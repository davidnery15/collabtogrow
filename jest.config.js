module.exports = {
  transform: {
    '^.+\\.(js|jsx)?$': '<rootDir>/node_modules/babel-jest',
  },
  transformIgnorePatterns: [
    // "<rootDir>/node_modules/*" // Defaul
    '!<rootDir>/node_modules/@cobuildlab/*',
  ],
  // modulePaths:[]
};

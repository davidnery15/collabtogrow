# Contribution guidelines

## Code Conventions

1) At a View level, 8base objects will be managed as they are retrieved from a graphql query, and the Actions layer this object should be converted to the `create`, `connect`, `disconnect` and `reconnect` format. 

Example: Let's suppose we have the entity `Alliance` who has a field `owner` that is a relationship with some other entity, then:

At a View level we use:

```javascript 1.6
alliance = {
  id: '',
  owner: {
    id: '',
    name: ''
  }
}
```

And not:

```javascript 1.6
alliance = {
  id: '',
  owner: ''
}
```

And, at an action level, we transform the object to create or update operations:

```javascript 1.6
alliance = {
  id: '',
  owner: {
    connect: {
      id: '',
    }
  }
}
```

### Use cases:

- When selecting an object from a `<Select />` or `<SelectInput />` make sure that you set as `value` for the options the complete object to be selected and not just the `id` 


2) On the `*View` use a `data` property on the state to hold every piece of information that belongs to an 8base entity:

Do this:

```javascript 1.6
this.state = {
  data: {
    initiativeData: {...},
    fundingRequestData: {...},
    // More data that is an entity
  },
    loading: true,
  step: 0
  // Every other data related to a View state
}
```

And not this:

```javascript 1.6
this.state = {
  initiativeData: {...},
  fundingRequestData: {...}
  loading: true,
  step: 0
  // Every other data related to a View state
}
```

const { addWebpackAlias, override } = require('customize-cra');

module.exports = override(
  addWebpackAlias({
    echarts$: 'echarts/dist/echarts-en.min',
    echarts: 'echarts/dist',
  }),
);

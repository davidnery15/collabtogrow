import { META_ALLIANCE_SELECTED } from '.';
import { isValidString } from './validators';
import sessionStore, { NEW_SESSION_EVENT } from './SessionStore';
import { ALLIANCE_ADMINISTRATOR, ALLIANCE_CREATOR, ALLIANCE_SER } from './roles';
import { ALLIANCE_APPROVAL_PENDING } from './status';

/**
 * Extracts the Role of a User on an Alliance, null if it does not have one
 * @param {User} user The User
 * @param {Alliance} alliance The Alliance
 */
export const getRoleOnAlliance = (user, alliance) => {
  // console.log('roles', user, alliance);
  for (let i = 0; i < user.companyUserRelation.items.length; i++) {
    const companyUser = user.companyUserRelation.items[i];
    for (let j = 0; j < companyUser.allianceUserCompanyUserRelation.items.length; j++) {
      const allianceUser = companyUser.allianceUserCompanyUserRelation.items[j];
      if (allianceUser.alliance.id === alliance.id) {
        return allianceUser.role;
      }
    }
  }
  return null;
};

/**
 * Extracts the Alliance of a Logged User
 * @param {User} user The User
 * @deprecated use sessionStore.getState(SESSION_ALLIANCE)
 */
export const getActiveAllianceId = (user) => {
  const metaList = user.metaRelation.items;
  const metaAlliance = metaList.find((meta) => meta.name === META_ALLIANCE_SELECTED);
  if (!metaAlliance && isValidString(metaAlliance.value)) return null;
  return metaAlliance.value;
};

/**
 * Extracts the company of a User on an Alliance, null if it does not have one
 * @param {User} user The User
 * @param {Alliance} alliance The Alliance
 */
export const getCompanyOnAlliance = (user, alliance) => {
  for (let i = 0; i < user.companyUserRelation.items.length; i++) {
    const companyUser = user.companyUserRelation.items[i];
    for (let j = 0; j < companyUser.allianceUserCompanyUserRelation.items.length; j++) {
      const allianceUser = companyUser.allianceUserCompanyUserRelation.items[j];
      if (allianceUser.alliance.id === alliance.id) {
        return companyUser.company;
      }
    }
  }
  return null;
};

/**
 * Return the currency on the selected alliance, or null if there is no selected alliance
 * @return {null}
 */
export const getCurrencyOnSession = () => {
  const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
  return selectedAlliance === null ? null : selectedAlliance.currency;
};

/**
 * Concat the clientCompany and partnerCompany users and returns the result
 * validate if there is no client or partner company
 * @param  {object} clientCompany  [description]
 * @param  {object} partnerCompany [description]
 * @return {array}  members of both companies
 */
export const concatClientAndPartnerUsers = (clientCompany, partnerCompany) => {
  const clientUsers =
    clientCompany && clientCompany.companyUserRelation
      ? clientCompany.companyUserRelation.items.map((item) => item.user)
      : [];
  const partnerUsers =
    partnerCompany && partnerCompany.companyUserRelation
      ? partnerCompany.companyUserRelation.items.map((item) => item.user)
      : [];
  const users = clientUsers.concat(partnerUsers);

  return users;
};

/**
 * Returns true if the User has the role or any the roles
 * in the provided Alliance
 * @param {User} user The user
 * @param {Alliance} alliance The Alliance
 * @param {String | String[]} role Role name or array or Role names
 */
export const userHasAnyRoleInAlliance = (user, alliance, role) => {
  const userRole = getRoleOnAlliance(user, alliance);
  if (!userRole) return false;

  if (Array.isArray(role)) {
    return role.includes(userRole.name);
  }
  return role === userRole.name;
};

/**
 * Returns true if the User has the role
 * of Alliance Administrator or Alliance SER in the Alliance
 * @param {User} user The user
 * @param {Alliance} alliance The Alliance
 */
export const isUserAdminOrSERInAlliance = (user, alliance) => {
  return userHasAnyRoleInAlliance(user, alliance, [ALLIANCE_ADMINISTRATOR, ALLIANCE_SER]);
};

/**
 * Returns true if the User has the role
 * of Alliance CREATOR in the Alliance
 * @param {User} user The user
 * @param {Alliance} alliance The Alliance
 */
export const isUserCreatorInAlliance = (user, alliance) => {
  return userHasAnyRoleInAlliance(user, alliance, [ALLIANCE_CREATOR]);
};

/**
 * Returns true if the User has the role
 * of Alliance SER in the provided Alliance
 * @param {User} user The user
 * @param {Alliance} alliance The Alliance
 */
export const isUserAllianceSER = (user, alliance) => {
  return userHasAnyRoleInAlliance(user, alliance, ALLIANCE_SER);
};

/**
 * Returns true if the User has the role
 * of Alliance Administrator in the provided Alliance
 * @param {User} user The user
 * @param {Alliance} alliance The Alliance
 */
export const isUserAllianceAdmin = (user, alliance) => {
  return userHasAnyRoleInAlliance(user, alliance, ALLIANCE_ADMINISTRATOR);
};

/**
 * Return true if the User's company's approval or rejection
 * of the Alliance is pending
 * @param {User} user The user
 * @param {Alliance} alliance The Alliance
 */
export const isUserPendingApprovalOfAlliance = (user, alliance) => {
  const { allianceApprovalRelation } = alliance;
  const userCompany = getCompanyOnAlliance(user, alliance);

  const userCompanyIsPendingResponse = allianceApprovalRelation.items.find(
    (approval) =>
      userCompany.id === approval.company.id && approval.status === ALLIANCE_APPROVAL_PENDING,
  );

  return !!userCompanyIsPendingResponse;
};
/**
 *
 * @param user
 * @param alliance
 * @returns {*}
 */
export const isUserFromClientCompany = (user, alliance) => {
  const companyId = alliance.clientCompany.id;
  const { companyUserRelation } = user;
  return !!companyUserRelation.items.find((item) => item.company.id === companyId);
};

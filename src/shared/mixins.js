import { error } from '@cobuildlab/pure-logger';
import * as toast from 'components/toast/Toast';
import { canLoadPageOnScroll } from './utils';

/**
 * The onChange method for forms
 */
export function onChangeMixin(name, value) {
  const data = this.state.data;
  data[name] = value;
  this.setState({ data });
}

/**
 * The onChange data method for forms
 */
export function onChangeDataMixin(name, value) {
  const { data } = this.state;
  data[name] = value;
  this.setState({ data });
}

/**
 * The onError method for View, shows the error on a Toast
 */
export function onErrorMixin(err) {
  error(err);
  if (Array.isArray(err.arr)) {
    toast.errors(err.arr);
  } else {
    toast.error(err.message);
  }
  this.setState({ loading: false, loadingPage: false });
}

/**
 * this on Change BusinessCase Data Mixin
 * @param name
 * @param value
 */
export function onChangeBusinessCaseDataMixin(name, value) {
  const { data } = this.state;
  data.businessCaseData[name] = value;
  this.setState({ data });
}

/**
 * Fetch the items next page on scroll event
 * if the canLoadPageOnScroll returns true
 * @param  {event} event       onScroll event
 * @param  {array} items       the array of items
 * @param  {function}  fetchItemsAction the method to load the items
 */
export function onListScrollMixin(event, items, fetchItemsAction) {
  const { count, loadingPage, search, page, filter, itemFilter } = this.state;
  const canLoadPage = canLoadPageOnScroll(event, items, count, loadingPage);

  if (canLoadPage) {
    this.setState({ loadingPage: true }, () => {
      const nextPage = page + 1;
      fetchItemsAction(search, nextPage, 20, filter, itemFilter);
    });
  }
}

import React from 'react';
import { AsyncContent } from '@8base/boost';
import { withApollo } from 'react-apollo';
import { Redirect } from 'react-router-dom';
import { SESSION_ERROR, APOLLO_CLIENT, NEW_SESSION_EVENT } from '../SessionStore';
import sessionStore from '../SessionStore';
import { createUserWithToken, fetchSession } from '../../modules/auth/auth.actions';
import Flux from '@cobuildlab/flux-state';
import PropTypes from 'prop-types';
import { initializeUser, updateMeta } from '../../modules/dashboard/dashboard-actions';
import { META_STEP_NAME } from '../../shared';
import {
  handleAllianceInvitationLink,
  handleAllianceMemberInvitationLink,
} from '../../shared/session-utils';
import { log } from '@cobuildlab/pure-logger';
import { NUMBER_OF_WIZARD_SCREENS } from '../../modules/wizard';
import { withLogout, withAuth } from '@8base/app-provider';
import { withRouter } from 'react-router-dom';
import View from '@cobuildlab/react-flux-state';

/**
 * User Session component
 */
class Session extends View {
  constructor(props) {
    super(props);
    this.state = {
      apolloAcquired: false,
      sessionAcquired: false,
      redirectUri: '',
    };

    this.handleAllianceInvitationLink = handleAllianceInvitationLink.bind(this);
    this.handleAllianceMemberInvitationLink = handleAllianceMemberInvitationLink.bind(this);
  }

  async componentDidMount() {
    const {
      auth: { isAuthorized },
    } = this.props;
    if (!isAuthorized) {
      setTimeout(() => this.props.logout(), 5000);
    }
    this.subscribe(sessionStore, SESSION_ERROR, async (e) => {
      log(`SESSION_ERROR:message`, e.message);
      log(`SESSION_ERROR:message`, e.message.lastIndexOf('User is absent'));
      // Maybe is a new User, if so, we will create him on 8base
      if (e.message.lastIndexOf('User is absent') !== -1) {
        log(`SESSION_ERROR:user_is_absent:1`);
        const {
          auth: { authState },
        } = this.props;
        const { token } = authState;
        log(`SESSION_ERROR:user_is_absent:2`);
        await createUserWithToken(token);
        log(`SESSION_ERROR:user_is_absent:3`);
        return fetchSession();
      } else {
        log(`SESSION_ERROR:else:`, e.message);
        setTimeout(() => this.props.logout(), 5000);
      }
    });

    this.subscribe(sessionStore, APOLLO_CLIENT, async () => {
      this.setState({ apolloAcquired: true }, () => {
        fetchSession();
      });
    });
    this.subscribe(sessionStore, NEW_SESSION_EVENT, async (session) => {
      log('NEW_SESSION_EVENT', session);
      const { user, allianceInvitationsList, allianceMemberInvitationsList } = session;
      const metaList = user.metaRelation.items;
      const companies = user.companyUserRelation.items;
      const { company, role } = companies.length ? companies[0] : {};
      const {
        history: {
          location: { pathname },
        },
      } = this.props;

      let step = metaList.find((meta) => meta.name === META_STEP_NAME);
      log('STEP', step);

      // Is a New USER
      if (step === undefined) {
        const stepValue = 0;
        await initializeUser(stepValue);
        return await fetchSession();
      }

      const stepValue = parseInt(step.value);

      // the user clicked accept-alliance-invitation email link
      if (pathname.includes('accept-alliance-invitation')) {
        try {
          await this.handleAllianceInvitationLink(user, company, stepValue, role);
        } catch (e) {
          return log('accept-alliance-invitation: ', e.message);
        }
      }

      // the user clicked accept-alliance-member-invitation email link
      if (pathname.includes('accept-alliance-member-invitation')) {
        try {
          await this.handleAllianceMemberInvitationLink(user, company, stepValue);
        } catch (e) {
          return log('accept-alliance-member-invitation: ', e.message);
        }
      }
      // Is an Existing User
      // If is over the Wizard, we don't do anything
      // If not, we check for invitations to see if we need to Skip the Wizard
      if (stepValue < NUMBER_OF_WIZARD_SCREENS + 1) {
        // skip the wizard if the user has an alliance member invitation
        const hasAllianceMemberInvitations = allianceMemberInvitationsList.count > 0;
        // skip the wizard if the user has a company and allianceInvitations
        const hasACompanyAndAllianceInvitations = company && allianceInvitationsList.count > 0;

        if (hasACompanyAndAllianceInvitations || hasAllianceMemberInvitations) {
          if (hasAllianceMemberInvitations) {
            // redirect to user profile if not going to accept the allianceMember invitation
            const redirectUri = pathname.includes('accept-alliance-member-invitation')
              ? ''
              : '/settings/user-profile';
            this.setState({ redirectUri });
          }

          await updateMeta(META_STEP_NAME, NUMBER_OF_WIZARD_SCREENS + 1);
          return await fetchSession();
        }
      }

      /*
      remove redirectUri, we dont need it anymore, this avoids user
      redirected to redirectUri after logout and login again on same tab
       */
      localStorage.removeItem('redirectUri');

      this.setState({ sessionAcquired: true });
    });
    // To avoid the dispatch cycle when redirected here
    // this.props.logout();
    setTimeout(() => Flux.dispatchEvent(APOLLO_CLIENT, this.props.client), 1000);
  }

  render() {
    const { history } = this.props;
    const { apolloAcquired, sessionAcquired, redirectUri } = this.state;
    if (apolloAcquired === false || sessionAcquired === false)
      return <AsyncContent loading={true} stretch />;
    const { user } = sessionStore.getState(NEW_SESSION_EVENT);
    const metaList = user.metaRelation.items;
    const step = metaList.find((meta) => meta.name === META_STEP_NAME);
    log('RENDER STEP: ', step);
    if (step === undefined)
      throw new Error(
        `User data must have a Meta value named: ${META_STEP_NAME} Maybe the User hasn't been properly created`,
      );
    if (parseInt(step.value) < NUMBER_OF_WIZARD_SCREENS - 1) {
      if (history.location.pathname.indexOf('/wizard/') === -1)
        return <Redirect to={`/wizard/${step.value}`} />;
    }
    // redirect to redirectUri avoiding infinite cycle
    if (redirectUri && redirectUri !== history.location.pathname) {
      const redirectUriCopy = redirectUri;
      this.setState({ redirectUri: '' });
      return <Redirect to={redirectUriCopy} />;
    }
    return this.props.children;
  }
}

Session.propTypes = {
  client: PropTypes.any,
  logOut: PropTypes.func,
  children: PropTypes.any,
};

export default withRouter(withAuth(withLogout(withApollo(Session))));

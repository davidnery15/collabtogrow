import React from 'react';
import { Grid, Card, Button, Paragraph, Text } from '@8base/boost';
// import { EmptyCardFooter } from '../../modules/wizard/components/CardFooter';
import PropTypes from 'prop-types';

const MessageScreen = (props) => {
  const {
    icon,
    mainMessage,
    mainButtonAction,
    mainButtonText,
    secondaryButtonAction,
    secondaryButtonText,
  } = props;
  return (
    <React.Fragment>
      <Card.Body style={{ paddingRight: '35px' }}>
        <Grid.Layout style={{ height: 500 }} columns={'1fr 1fr 1fr'}>
          <Grid.Box />
          <Grid.Box style={{ width: '100%' }}>
            <div style={{ textAlign: 'center', margin: 'auto' }}>
              <Paragraph
                style={{
                  opacity: '0.9',
                  color: '#323C47',
                  fontSize: 16,
                  fontWeight: 600,
                }}>
                {mainMessage}
              </Paragraph>
              <img style={{ width: 212, margin: '26px 0' }} src={icon} alt="CollabToGrow" />
              <Button
                style={{ float: 'none', margin: '28px auto 5px auto' }}
                type="button"
                onClick={mainButtonAction}
                text={mainButtonText}
              />
              <Text color="TEXT_SECONDARY">
                {/* TODO: Alan que se vea como un boton secundario */}
                <Button
                  style={{ float: 'none', margin: '28px auto 5px auto' }}
                  onClick={secondaryButtonAction}
                  text={secondaryButtonText}
                />
              </Text>
            </div>
          </Grid.Box>
          <Grid.Box />
        </Grid.Layout>
      </Card.Body>
    </React.Fragment>
  );
};

MessageScreen.propTypes = {
  headerMessage: PropTypes.string.isRequired,
  secondaryButtonText: PropTypes.string,
  mainButtonText: PropTypes.string.isRequired,
  mainMessage: PropTypes.string.isRequired,
  mainButtonAction: PropTypes.func.isRequired,
  secondaryButtonAction: PropTypes.func,
  icon: PropTypes.any.isRequired,
};

export default MessageScreen;

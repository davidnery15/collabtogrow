import { Menu as BoostMenu } from '@8base/boost';
import styled from 'styled-components';

export const Menu = styled(BoostMenu)`
  min-width: 148px;
`;

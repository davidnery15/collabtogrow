import React from 'react';
import { PropTypes } from 'prop-types';
import styled from '@emotion/styled';
import { Progress } from '@8base/boost';

const StyledProgress = styled(Progress)`
  background-color: 'black';
  color: #fcbd00;
`;

const ProgressDashboard = ({ onChange, label, value, disabled, currency }) => {
  const _value = value ? value : 0;
  const _currency = currency ? currency : {};

  return (
    <StyledProgress>
      <Progress style={{ color: 'aquamarine!important' }} />
    </StyledProgress>
  );
};

export { ProgressDashboard };

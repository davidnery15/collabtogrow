import React from 'react';
import PropTypes from 'prop-types';
import { Input, Icon } from '@8base/boost';

const SearchBox = ({ value, onChange, placeholder }) => {
  return (
    <Input
      value={value}
      onChange={onChange}
      rightIcon={<Icon name="Search" size="sm" color="GRAY4" />}
      placeholder={placeholder}
    />
  );
};

SearchBox.defaultProps = {
  placeholder: 'Search...',
  value: '',
};

SearchBox.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string,
  placeholder: PropTypes.string,
};

export { SearchBox };

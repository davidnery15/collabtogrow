import React from 'react';
import { SelectField, Dialog, Grid, Button, Label } from '@8base/boost';
import PropTypes from 'prop-types';
import { onChangeMixin } from '../mixins';

class UserRoleDialog extends React.Component {
  constructor(props) {
    super(props);
    const user = this.props.user;
    const role = this.props.role;
    this.state = {
      data: {
        user: user === null ? null : user.id,
        role: role === null ? null : role.id,
      },
    };
    this.onChange = onChangeMixin.bind(this);
  }

  onAccept = () => {
    const user = this.state.data.user;
    const role = this.state.data.role;
    if (user === '') return this.props.onError('Invalid User');
    if (role === '') return this.props.onError('Invalid Role');
    const data = {
      user: this.props.users.find((_user) => _user.id === user),
      role: this.props.roles.find((_role) => _role.id === role),
    };
    this.props.onAccept(data);
  };

  render() {
    const { onCancel, users, roles } = this.props;
    const { user, role } = this.state.data;

    return (
      <Dialog size="sm" isOpen={this.props.isOpen}>
        <Dialog.Header title="User Role association" onClose={onCancel} />
        <Dialog.Body scrollable>
          <Grid.Layout gap="sm" stretch>
            <Grid.Box>
              <Label>Select User</Label>
              <SelectField
                label="User"
                input={{
                  name: 'user',
                  value: user,
                  onChange: (value) => this.onChange('user', value),
                }}
                meta={{}}
                placeholder="Select"
                options={users.map((user) => {
                  return { label: `${user.firstName} ${user.lastName}`, value: user.id };
                })}
              />
              <Label>Select Role</Label>
              <SelectField
                label="Role"
                input={{
                  name: 'role',
                  value: role,
                  onChange: (value) => this.onChange('role', value),
                }}
                meta={{}}
                placeholder="Select"
                options={roles.map((role) => {
                  return { label: role.name, value: role.id };
                })}
              />
            </Grid.Box>
          </Grid.Layout>
        </Dialog.Body>
        <Dialog.Footer>
          <Button color="neutral" variant="outlined" onClick={onCancel}>
            Cancel
          </Button>
          <Button color="red" onClick={this.onAccept} type="submit" text="Save" />
        </Dialog.Footer>
      </Dialog>
    );
  }
}

UserRoleDialog.propTypes = {
  onAccept: PropTypes.func.isRequired,
  onCancel: PropTypes.func,
  onError: PropTypes.func,
  users: PropTypes.array.isRequired,
  roles: PropTypes.array.isRequired,
  isOpen: PropTypes.bool.isRequired,
  user: PropTypes.string,
  role: PropTypes.string,
};

export default UserRoleDialog;

import React from 'react';
import { PropTypes } from 'prop-types';
import { Column } from '@8base/boost';
import styled from '@emotion/styled';

import { CurrencyFormat } from '../../shared/components/CurrencyFormat';
import { Overline1 } from '../../components/new-ui/font-style/Overline1';

const Input = styled.input`
  width: 100%;
  outline: none;
  padding-left: 1rem;
  padding-right: 2rem;
  background-color: ${(props) => (props.disabled ? '#f4f7f9' : '#FFFFFF')};
  color: #323c47;
  font-size: 1.4rem;
  font-weight: 400;
  height: 4rem;
  line-height: normal;
  transition: all 0.15s ease-in-out;
  border-color: gray;
  text-align: left;
  border: 1px solid #d0d7dd;
  border-radius: 5px;
  margin-top: -1rem;
`;

const StyledLabel = styled(Overline1)`
  margin-bottom: 14px !important;
`;

const CurrencyInputField = ({ onChange, label, value, disabled, currency }) => {
  const _value = value ? value : 0;
  const _currency = currency ? currency : {};

  return (
    <Column stretch>
      <StyledLabel>{label}</StyledLabel>
      <CurrencyFormat
        {..._currency}
        value={_value}
        disabled={disabled}
        onValueChange={(value) => {
          if (isNaN(value.floatValue)) onChange(0);
          else onChange(value.floatValue);
        }}
        customInput={Input}
      />
    </Column>
  );
};

CurrencyInputField.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.any.isRequired,
  onChange: PropTypes.func.isRequired,
  currency: PropTypes.object,
  disabled: PropTypes.bool,
};

CurrencyInputField.defaultProps = {
  currency: null,
  disabled: false,
};

export { CurrencyInputField };

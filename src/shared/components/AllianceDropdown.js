import React from 'react';
import { Dropdown, Menu, Text } from '@8base/boost';
import { PropTypes } from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const DropDownText = () => {
  return (
    <span style={{ transform: 'translateX(-25px)' }}>
      <Text>
        Select an Alliance <FontAwesomeIcon icon="caret-down" />
      </Text>
    </span>
  );
};

const AllianceDropdown = (props) => {
  const { selected, items, onSelect } = props;

  let selectedText = <Text style={{ color: 'lightgrey' }}>No Alliances to Select</Text>;

  if (items.length > 0) selectedText = <DropDownText />;

  if (selected != null)
    selectedText = (
      <>
        {' '}
        {selected} &nbsp; <FontAwesomeIcon icon="caret-down" />
      </>
    );
  return (
    <Dropdown defaultOpen={false}>
      <Dropdown.Head>{selectedText}</Dropdown.Head>
      <Dropdown.Body pin="center">
        {({ closeDropdown }) => (
          <Menu style={{ maxHeight: '400px', overflowY: 'auto' }}>
            {items.map((item, index) => (
              <Menu.Item
                key={index}
                onClick={() => {
                  onSelect(item, true);
                  closeDropdown();
                }}>
                {item.name}
              </Menu.Item>
            ))}
          </Menu>
        )}
      </Dropdown.Body>
    </Dropdown>
  );
};

AllianceDropdown.propTypes = {
  onSelect: PropTypes.func.isRequired,
  selected: PropTypes.string,
  items: PropTypes.array.isRequired,
};

AllianceDropdown.defaultProps = {
  selected: null,
};

export default AllianceDropdown;

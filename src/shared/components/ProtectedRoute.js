import React from 'react';
import * as R from 'ramda';
import { Route, Redirect } from 'react-router-dom';
import { withAuth } from '@8base/app-provider';
import sessionStore from '../SessionStore';
import { META_STEP_NAME } from '../';
import { NEW_SESSION_EVENT } from '../SessionStore';
import { NUMBER_OF_WIZARD_SCREENS } from '../../modules/wizard';
import PropTypes from 'prop-types';

const renderComponent = (props) => {
  const { render, children, component, ...rest } = props;

  let rendered = null;

  if (component) {
    rendered = React.createElement(component, { ...rest }, children);
  }

  if (render) {
    rendered = render({ ...rest, children });
  }

  if (typeof children === 'function') {
    rendered = children(rest);
  } else if (children) {
    rendered = children;
  } else if (!rendered) {
    throw new Error(
      'Error: must specify either a render prop, a render function as children, or a component prop.',
    );
  }

  return rendered;
};

class ProtectedRoute extends React.Component {
  renderRoute = () => {
    const {
      auth: { isAuthorized },
      ...rest
    } = this.props;

    if (isAuthorized) {
      // check wizard steps
      const user = sessionStore.getState(NEW_SESSION_EVENT).user;
      let metaList;
      let step;
      try {
        metaList = user.metaRelation.items;
        step = metaList.find((meta) => meta.name === META_STEP_NAME);
      } catch (e) {
        console.log('SetWizardStepError: ', e);
      }
      // redirect to wizard if there are missing steps
      if (step && parseInt(step) < NUMBER_OF_WIZARD_SCREENS - 1) {
        // dont redirect if you are already on wizard route
        if (!this.props.location.pathname.includes('/wizard')) {
          return <Redirect to={`/wizard/${step.value}`} />;
        }
      }

      return renderComponent(rest);
    }

    return <Redirect to={{ pathname: '/auth', state: { from: rest.location } }} />;
  };

  render() {
    const props = R.omit(['component', 'render'], this.props);

    return <Route {...props} render={this.renderRoute} />;
  }
}

ProtectedRoute.propTypes = {
  auth: PropTypes.any,
  location: PropTypes.any.isRequired,
};

export default withAuth(ProtectedRoute);

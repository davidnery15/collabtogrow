export const mockedAlliance = () => {
  return {
    selectedAlliance: { id: 1, currency: { symbol: '$', decimalSeparator: ',' } },
  };
};

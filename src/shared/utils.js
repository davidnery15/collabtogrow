import { curry, apply, clone } from 'ramda';
import { getRoleOnAlliance } from './alliance-utils';
import { ALLIANCE_ADMINISTRATOR, ALLIANCE_SER } from './roles';
import { META_SHOW_ON_BOARDING } from '.';
import BigInt from 'big-integer';

/**
 *
 * @param {The current State} state
 * @param {Object to update keys from} object
 * @returns a New State object
 * @deprecated Not to use, See Contributing guidelines
 */
export const updateStateFromObject = (state, object) => {
  const newState = {};
  Object.keys(state).forEach((key) => {
    const newValue = object[key];
    const oldValue = state[key];
    if (newValue === undefined) {
      newState[key] = oldValue;
      return;
    }
    if (newValue === null) {
      newState[key] = oldValue;
      return;
    }

    if (typeof newValue === 'object') {
      if (newValue.id) newState[key] = newValue.id;
      else newState[key] = newValue;
      return;
    }

    if (Array.isArray(newValue)) {
      const newValues = [];
      newValue.forEach((value) => {
        if (value === undefined) return;
        if (value === null) return;

        if (typeof value === 'object') {
          if (value.id) newValues.push(value.id);
          return;
        }
        newValues.push(value);
      });
      newState[key] = newValues;
      return;
    }

    newState[key] = newValue;
  });
  return newState;
};

/**
 * Helper to change Array keys to 8base 'reconnect' multiple references
 * WARNING: This function mutates the data
 * @param {data} object to mutate
 * @param {key} string the key of the property to mutate
 */
export const sanitize8BaseReconnects = (data, key) => {
  if (Array.isArray(data[key])) {
    data[key] = {
      reconnect: data[key].map((id) => {
        return { id: id };
      }),
    };
  } else delete data[key];
};

/**
 * Helper to change Array key objects to 8base 'reconnect' multiple references
 * WARNING: This function mutates the data
 * @param {data} object to mutate
 * @param {key} string the key of the property to mutate
 */
export const sanitize8BaseReconnectsFromObjects = (data, key) => {
  if (Array.isArray(data[key])) {
    data[key] = {
      reconnect: data[key].map(({ id }) => {
        return { id: id };
      }),
    };
  } else delete data[key];
};

/**
 * Helper to change Array keys to 8base 'connect' multiple references
 * WARNING: This function mutates the data
 * @param data
 * @param key
 */
export const sanitize8BaseReferences = (data, key) => {
  if (Array.isArray(data[key])) {
    data[key] = {
      connect: data[key].map((id) => {
        return { id: id };
      }),
    };
  } else delete data[key];
};

/**
 * Helper to change Array keys objects to 8base 'connect' multiple references
 * WARNING: This function mutates the data
 * @param data
 * @param key
 */
export const sanitize8BaseReferencesFromObjects = (data, key) => {
  if (Array.isArray(data[key])) {
    data[key] = {
      connect: data[key].map(({ id }) => {
        return { id: id };
      }),
    };
  } else delete data[key];
};

/**
 * Helper to change non null keys to 8base 'connect' reference
 * WARNING: This function mutates the data
 * @param {data} object to mutate
 * @param {key} string the key of the property to mutate
 */
export const sanitize8BaseReference = (data, key) => {
  if (data[key] !== null && data[key] !== '') data[key] = { connect: { id: data[key] } };
  else delete data[key];
};

/**
 * Helper function to non null objects to 8base 'connect' reference
 * WARNING: This function mutates the data
 * @param data
 * @param key
 */
export const sanitize8BaseReferenceFromObject = (data, key) => {
  if (data[key] !== null && data[key] !== '' && data[key] !== undefined) {
    if (data[key].id === undefined)
      throw new Error(
        `${key} property on 'data' does not have a field 'id' so is not possible to create the 'connect'`,
      );
    data[key] = { connect: { id: data[key].id } };
  } else delete data[key];
};

/**
 * Helper to change non null keys to 8base 'create' reference
 * WARNING: This function mutates the data
 * @param {data} object to mutate
 * @param {key} string the key of the property to mutate
 */
export const sanitize8BaseNewObject = (data, key) => {
  if (data[key] !== null && data[key] !== '') data[key] = { create: { id: data[key] } };
  else delete data[key];
};

/**
 * Helper to change non null keys to 8base 'create' reference for Documents
 * WARNING: This function mutates the data
 * @param {data} object to mutate
 * @param {key} string the key of the property to mutate
 */
export const sanitize8BaseDocumentCreate = (data, key) => {
  if (data[key] !== null && data[key] !== '') data[key] = { create: data[key] };
  else delete data[key];
};

/**
 * Helper to change non null keys to 8base 'create' reference for Documents List
 * WARNING: This function mutates the data
 * @param {data} object to mutate
 * @param {key} string the key of the property to mutate
 */
export const sanitize8BaseDocumentsCreate = (data, key) => {
  if (data[key] !== null && data[key] !== '') {
    data[key] = {
      create: data[key].map((document) => {
        return { ...document };
      }),
    };
  } else delete data[key];
};

/**
 * Helper to change non null keys to 8base 'create' reference for Documents
 * WARNING: This function mutates the data
 * @param {data} object to mutate
 * @param {key} string the key of the property to mutate
 */
export const sanitize8BaseDocumentUpdate = (data, key) => {
  if (data[key] !== null && data[key].downloadUrl === undefined) data[key] = { create: data[key] };
  else delete data[key];
};

/**
 * Helper to change non null keys to 8base 'create' reference for Documents Lists
 * WARNING: This function mutates the data
 * @param {data} object to mutate
 * @param {key} string the key of the property to mutate
 */
export const sanitize8BaseDocumentsUpdate = (data, key) => {
  if (data[key] !== null && data[key].length > 0) {
    const docsToCreate = data[key].filter((file) => file.downloadUrl === undefined);
    if (docsToCreate.length > 0) {
      data[key] = {
        create: docsToCreate.map((document) => {
          return { ...document };
        }),
      };
      return;
    }
  }
  delete data[key];
};

/**
 * Helper to change non null keys to 8base 'delete & create' reference for Documents Lists
 * WARNING: This function mutates the data
 * @param {data} object to mutate
 * @param {key} string the key of the property to mutate
 * @param {originalDocuments } List the documents originals
 */
export const sanitize8BaseDocumentsDeleteAndUpdate = (data, key, originalDocuments) => {
  if (data[key] === undefined || data[key] === null) {
    delete data[key];
    return;
  }

  const toBeDeleted = [],
    toBeCreated = [];

  data[key].forEach((document) => {
    if (document.id === undefined) toBeCreated.push(document);
  });

  originalDocuments.forEach((original) => {
    const originalDocument = data[key].find((file) => file.id === original.id);
    if (originalDocument === undefined) toBeDeleted.push({ id: original.id });
  });

  if (toBeCreated.length === 0 && toBeDeleted.length === 0) {
    delete data[key];
    return;
  }

  data[key] = {};

  if (toBeCreated.length > 0)
    data[key].create = toBeCreated.map((document) => {
      return { ...document };
    });

  if (toBeDeleted.length > 0) data[key].disconnect = toBeDeleted;
};

/**
 * Helper to remove null keys from objects
 * WARNING: This function mutates the data
 * @param {data} object to mutate
 * @param {key} string the key of the property to mutate
 */
export const sanitize8BaseNonNull = (data, key) => {
  if (data[key] === null) delete data[key];
};

/**
 * Helper to remove null or blank dates keys from objects
 * WARNING: This function mutates the data
 * @param {data} object to mutate
 * @param {key} string the key of the property to mutate
 */
export const sanitize8BaseDate = (data, key) => {
  if (data[key] === null || data[key] === '') delete data[key];
};

/**
 * Helper to truncate text to a given length and add ellipsis
 * @param {str} string original string to be truncated
 * @param {length} number (optional) length of new truncated string that will be returned
 * @returns The new truncated string with an ellipsis at the end
 */
export const truncateText = (str, length = 75) => {
  if (str.length > length) {
    return str.substring(0, length - 3) + '...';
  }
  return str;
};

/**
 * @deprecated // Use ./shared/validators isValidString
 * helper to check valid Strings: not null, not undefined, not blank
 * @returns true if the string is Valid, otherwise false
 * @param str
 */
export const isValidString = (str) => {
  if (str === undefined) return false;
  if (str === null) return false;
  return str !== '';
};

export const pct = (total, fraction) => {
  if (total === 0) return 0;
  return Math.ceil((fraction * 100) / total);
};

/**
 * Shuffles array in place. ES6 version
 * @param {Array} a items An array containing the items.
 * https://stackoverflow.com/questions/6274339/how-can-i-shuffle-an-array/6274381#6274381
 */
export const shuffle = (a) => {
  for (let i = a.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [a[i], a[j]] = [a[j], a[i]];
  }
  return a;
};

/* Set the redirectUri to use after login and store query string params
 * // TODO: add any auth route on the if coditional
 * // This wont override urls that includes '/invitations/accept' string
 */
export const setLoginRedirectUri = () => {
  const { pathname, search } = window.location;
  const routesToIgnore = ['/home', '/auth', '/wizard', '/company-management'];

  if (pathname === '/') return;
  for (const route of routesToIgnore) {
    // dont set redirectUri if its an ignored route
    if (pathname.includes(route)) return;
  }

  const redirectUri = localStorage.getItem('redirectUri');
  // don't override '/invitations/accept' routes
  if (!redirectUri || !redirectUri.includes('/invitations/accept')) {
    localStorage.setItem('redirectUri', pathname);
  }
  if (search) localStorage.setItem('search', search);
  console.log('redirectUri: ', pathname);
  console.log('search: ', search);
};

/**
 * Get invitation from search query stored with setLoginRedirectUri
 * @return {Invitation} the invitation from the acceptInvitation link
 */
export const getInvitationFromSearch = () => {
  const search = localStorage.getItem('search');
  const params = new URLSearchParams(search);
  const companyName = params.get('companyName');
  const allianceName = params.get('allianceName');
  const type = params.get('invitationType');
  const email = params.get('email');

  if (type) {
    const invitation = { type, email };
    if (type === 'company') invitation.name = companyName;
    else if (type === 'alliance' || type === 'alliance-member') invitation.name = allianceName;
    return invitation;
  }

  return null;
};

/**
 * Debounce
 *
 * @param {Boolean} immediate If true run `fn` at the start of the timeout
 * @param  timeMs {Number} Debounce timeout
 * @param  fn {Function} Function to debounce
 *
 * @return {Number} timeout
 * @example
 *
 *    const say = (x) => console.log(x);
 *    const debouncedSay = debounce_(1000, say, false)();
 *    debouncedSay("1");
 */
export const debounce = curry((timeMs, fn, immediate = false) => () => {
  let timeout;

  return (...args) => {
    const later = () => {
      timeout = null;

      if (!immediate) {
        apply(fn, args);
      }
    };

    const callNow = immediate && !timeout;

    clearTimeout(timeout);
    timeout = setTimeout(later, timeMs);

    if (callNow) {
      apply(fn, args);
    }

    return timeout;
  };
});

/**
 * Returns true is the scroll bottom is reached and the view can load more items
 * @param  {event} event The react node event
 * @param  {array} list  the list to check if list is complete
 * @param  {number} count total items from pagination
 * @param  {boolean} loadingPage if its already loading the page;
 * @return {boolean}
 */
export const canLoadPageOnScroll = (event, list, count, loadingPage) => {
  const { target } = event;
  const isListCompleted = count <= list.length;
  const distanceToBottom = target.clientHeight - (target.scrollHeight - target.scrollTop);
  const isBottomReached = distanceToBottom <= 10 && distanceToBottom >= -10;
  // console.log('isBottomReached:', isBottomReached);
  // console.log('isListCompleted:', isListCompleted, count, list.length);
  return isBottomReached && !isListCompleted && !loadingPage;
};

/**
 * Helper to selected  item Menu
 * @param {url} string
 * @param {compare} object
 */
export const checkUrl = (url, compare) => {
  let aux = false;
  if (typeof compare === 'object') {
    compare.forEach((value) => {
      if (url.pathname.search(value) >= 1) {
        aux = true;
      }
    });
  } else {
    if (url.pathname.search(compare) > 0) {
      aux = true;
    }
  }
  return aux;
};

/**
 * Save forms to local storage ignoring the fields send in propertiesToReset
 * and setting it to its default value from model, to ignore the Files from 8base
 * @param  {string} itemName
 * @param  {object} data
 * @param  {object} model
 * @param  {array}  propertiesToReset
 */
export const saveFormToSessionStorage = (itemName, data, model, propertiesToReset) => {
  const dataCopy = clone(data);
  for (const prop in dataCopy) {
    if (dataCopy.hasOwnProperty(prop)) {
      if (BigInt.isInstance(dataCopy[prop])) {
        dataCopy[prop] = dataCopy[prop].toString();
      }
      if (dataCopy[prop] && dataCopy[prop].fileId) {
        dataCopy[prop] = null;
      }
      if (propertiesToReset.includes(prop)) dataCopy[prop] = model[prop];
    }
  }

  sessionStorage.setItem(itemName, JSON.stringify(dataCopy));
};

export const b64toBlob = (dataURI, type) => {
  var byteString = atob(dataURI.split(',')[1]);
  var ab = new ArrayBuffer(byteString.length);
  var ia = new Uint8Array(ab);

  for (var i = 0; i < byteString.length; i++) {
    ia[i] = byteString.charCodeAt(i);
  }

  return new Blob([ab], { type: type });
};

export const downloadFile = (blob, tempLink, filename) => {
  let csvURL = window.URL.createObjectURL(blob);
  tempLink = document.createElement('a');
  tempLink.href = csvURL;
  tempLink.setAttribute('download', filename);
  tempLink.click();
};

export const ownerToString = (owner) => {
  if (!owner) return '';
  return `${owner.firstName} ${owner.lastName}`;
};

export const capitalize = (str) => str.charAt(0).toUpperCase() + str.slice(1);

export const exportToFile = (fileData, fileName, format) => {
  let newFile;
  let downloadLink;

  // NEW FILE
  newFile = new Blob([fileData], { type: format ? format : '' });

  // Download link
  downloadLink = document.createElement('a');

  // File name
  downloadLink.download = fileName;

  // We have to create a link to the file
  downloadLink.href = window.URL.createObjectURL(newFile);

  // Make sure that the link is not displayed
  downloadLink.style.display = 'none';

  // Add the link to your DOM
  document.body.appendChild(downloadLink);

  downloadLink.click();
};

/**
 * Compare if 2 emails are equal
 * @param  {String} email
 * @param  {String} emailToCompare
 * @return {Boolean}
 */
export const areEmailsEqual = (email, emailToCompare) => {
  if (!email || !emailToCompare) return false;
  return email.toLowerCase() === emailToCompare.toLowerCase();
};

export const canShowOnBoarding = (user, selectedAlliance, metas) => {
  const role = getRoleOnAlliance(user, selectedAlliance);
  let showOnBoarding = true;
  if (role) {
    if (role.name !== ALLIANCE_ADMINISTRATOR && role.name !== ALLIANCE_SER) {
      showOnBoarding = false;
    }
  }
  if (metas.length) {
    metas.forEach((meta) => {
      if (meta.name === META_SHOW_ON_BOARDING) {
        if (isValidString(meta.value)) showOnBoarding = false;
      }
    });
  }
  return showOnBoarding;
};
/**
 * get format document to base64
 * @param file
 * @return {File}
 */
export const documentFormatBase64 = (file: File) => {
  const reader = new FileReader();

  reader.addEventListener(
    'load',
    () => {
      file.base64 = reader.result;
    },
    false,
  );

  reader.readAsDataURL(file);

  return file;
};

/**
 *
 * @param year : date for YYYY or YYYY/MM/DD  ex: year : 2019 or 2019/12/31
 * @param fieldName: reference for use field in 8base
 * @param isDateTime: if the field is date/time or normal date
 */
export const filterForYear = (year, fieldName = 'createdAt', isDateTime = true) => {
  let gte;
  let lte;

  if (isDateTime) {
    gte = `${year}-01-01T00:00:00.000Z`;
    lte = `${year}-12-31T00:00:00.000Z`;
  } else {
    gte = `${year}-01-01`;
    lte = `${year}-12-31`;
  }

  const filter = {
    AND: [{ [fieldName]: { lte } }, { [fieldName]: { gte } }],
  };

  return filter;
};

/**
 * Transforms value into a String
 * @param {String | BigInt} value
 */
export const stringify8BaseBigInt = (value) => {
  if (!value) return '0';
  return value.toString();
};

/**
 * sanitize8BaseBigInt by using stringify8BaseBigInt, for BigInt fields
 * @param {Object} data
 * @param {String} fieldName the fieldName to sanitize to string
 */
export const sanitize8BaseBigInt = (data, fieldName) => {
  data[fieldName] = stringify8BaseBigInt(data[fieldName]);
};

/**
 * sanitize8BaseBigInts by using stringify8BaseBigInt, for BigInt arrays
 * @param {Object} data
 * @param {String} fieldName the fieldName to sanitize values to string
 */
export const sanitize8BaseBigInts = (data, fieldName) => {
  data[fieldName] = data[fieldName].map(stringify8BaseBigInt);
};

/**
 * Calculates The converted Value
 */
export const calculateValueBigInt = (unitQuantity, unitMonetizationFactor) => {
  const _unitQuantity = unitQuantity || 0;
  const _unitMonetizationFactor = unitMonetizationFactor || '0';

  return BigInt(parseInt(_unitQuantity))
    .multiply(_unitMonetizationFactor)
    .toString();
};

export const showMonths = (i) => {
  let months = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
  return months[i];
};

export const showQuantity = (i) => {
  let quantity = [7, 8, 9, 11, 12];
  return quantity.includes(i + 1);
};

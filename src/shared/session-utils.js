import { getInvitationFromSearch } from './utils';
import { updateMeta } from '../modules/dashboard/dashboard-actions';
import { NUMBER_OF_WIZARD_SCREENS } from '../modules/wizard';
import { COMPANY_ADMINISTRATOR } from './roles';
import { META_STEP_NAME } from './';
import { fetchSession } from '../modules/auth/auth.actions';

/**
 * Handle alliance invitation link on session component
 * // Must return on catch error on the session component
 * @param  {User}  user
 * @param  {Company}  company
 * @param  {META_STEP_NAME}  stepValue
 */
export async function handleAllianceInvitationLink(user, company, stepValue, role) {
  const invitation = getInvitationFromSearch();

  // logout if not my invitation
  if (invitation && user.email.toLowerCase() !== invitation.email.toLowerCase()) {
    this.props.logout();
    throw new Error(`Can't accept alliance invitation, not invitation owner`);
  }

  // remove invitation query strings
  localStorage.removeItem('search');

  // check if the user can accept invitation
  if (company && role && role.name === COMPANY_ADMINISTRATOR) {
    if (stepValue < NUMBER_OF_WIZARD_SCREENS + 1) {
      await updateMeta(META_STEP_NAME, NUMBER_OF_WIZARD_SCREENS + 1);
      await fetchSession();
      throw new Error(`Need to update metaStep before continue`);
    }
  }

  // otherwise continue to the accept-alliance-invitation route
}

/**
 * Handle alliance member invitation link on session component
 * // Must return on catch error on the session component
 * @param  {User}  user
 * @param  {Company}  company
 * @param  {META_STEP_NAME}  stepValue
 */
export async function handleAllianceMemberInvitationLink(user, company, stepValue) {
  const invitation = getInvitationFromSearch();

  // logout if not my invitation
  if (invitation && user.email.toLowerCase() !== invitation.email.toLowerCase()) {
    this.props.logout();
    throw new Error(`Can't accept company invitation, not invitation owner`);
  }

  // remove invitation query strings
  localStorage.removeItem('search');

  // check if the user can accept invitation
  if (!company) {
    if (stepValue < NUMBER_OF_WIZARD_SCREENS + 1) {
      await updateMeta(META_STEP_NAME, NUMBER_OF_WIZARD_SCREENS + 1);
      await fetchSession();
      throw new Error(`Need to update metaStep before continue`);
    }
  }

  // otherwise continue to the accept-company-invitation route
}

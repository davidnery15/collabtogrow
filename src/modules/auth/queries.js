import gql from 'graphql-tag';

export const AllianceMemberInvitationFragment = gql`
  fragment AllianceMemberInvitationFragment on AllianceMemberInvitation {
    id
    email
    status
    dateOfResponse
    role {
      id
      name
    }
    company {
      id
      name
    }
    alliance {
      id
      name
    }
  }
`;

export const UserFragment = gql`
  fragment UserFragment on User {
    email
    firstName
    id
    lastName
    weeklyEmailNotification
    avatar {
      id
      downloadUrl
    }
  }
`;

export const RoleFragment = gql`
  fragment RolesFragment on Role {
    id
    name
    description
  }
`;

export const AllianceInvitationFragment = gql`
  fragment AllianceInvitationFragment on AllianceInvitation {
    id
    email
    alliance {
      id
      name
      status
      owner {
        id
        lastName
        firstName
      }
      clientCompany {
        id
        name
      }
    }
    dateOfResponse
    createdAt
    createdBy {
      ...UserFragment
    }
    status
  }
  ${UserFragment}
`;

/**
 * Query the invitations
 * E.X. input:
 *
 {
  "allianceData": {
    "email": { "equals": "alacret@gmail.com"}
  },
  "allianceMemberInvitationFilter": {
    "email": { "equals": "alacret@gmail.com"}
  }
}
 *
 */

export const INVITATIONS_QUERY = gql`
  query allianceInvitations(
    $allianceData: AllianceInvitationFilter!
    $allianceMemberInvitationFilter: AllianceMemberInvitationFilter!
  ) {
    allianceInvitationsList(filter: $allianceData) {
      count
      items {
        ...AllianceInvitationFragment
      }
    }
    allianceMemberInvitationsList(filter: $allianceMemberInvitationFilter) {
      count
      items {
        ...AllianceMemberInvitationFragment
      }
    }
  }
  ${UserFragment}
  ${AllianceInvitationFragment}
  ${AllianceMemberInvitationFragment}
`;

export const SESSION_QUERY = gql`
  query(
    $id: ID
    $alliancesFilter: AllianceFilter!
    $allianceData: AllianceInvitationFilter!
    $allianceMemberInvitationFilter: AllianceMemberInvitationFilter!
    $CompanyFilter: CompanyFilter!
  ) {
    user(id: $id) {
      ...UserFragment
      metaRelation {
        count
        items {
          name
          value
        }
      }
      companyUserRelation {
        count
        items {
          id
          allianceUserCompanyUserRelation {
            count
            items {
              id
              alliance {
                id
              }
              role {
                id
                name
              }
            }
          }
          company {
            id
            name
            logoFile {
              id
              downloadUrl
            }
          }
          role {
            id
            name
          }
        }
      }
    }

    companiesList(filter: $CompanyFilter) {
      count
      items {
        id
        name
        logoFile {
          id
          downloadUrl
        }
      }
    }

    alliancesList(filter: $alliancesFilter) {
      count
      items {
        id
        name
        status
        createdAt
        clientCompany {
          id
          name
        }
        partnerCompany {
          id
          name
        }
        owner {
          ...UserFragment
        }
        currency {
          id
          thousandSeparator
          decimalSeparator
          symbol
          shortName
        }
      }
    }

    allianceInvitationsList(filter: $allianceData) {
      count
      items {
        ...AllianceInvitationFragment
      }
    }
    allianceMemberInvitationsList(filter: $allianceMemberInvitationFilter) {
      count
      items {
        ...AllianceMemberInvitationFragment
      }
    }
  }
  ${UserFragment}
  ${AllianceInvitationFragment}
  ${AllianceMemberInvitationFragment}
`;

export const USER_QUERY = gql`
  query User($id: ID) {
    user(id: $id) {
      ...UserFragment
      metaRelation {
        count
        items {
          name
          value
        }
      }
      companyUserRelation {
        count
        items {
          id
          allianceUserCompanyUserRelation {
            count
            items {
              id
              alliance {
                id
              }
              role {
                id
                name
              }
            }
          }
          company {
            id
            name
          }
          role {
            id
            name
          }
        }
      }
    }
  }
  ${UserFragment}
`;

export const USERS_AND_ROLES_QUERY = gql`
  query {
    usersList {
      count
      items {
        ...UserFragment
      }
    }
    rolesList {
      count
      items {
        ...RolesFragment
      }
    }
  }
  ${UserFragment}
  ${RoleFragment}
`;

export const CREATE_USER_QUERY = gql`
  mutation($user: UserCreateInput!, $authId: ID!) {
    userSignUpWithToken(user: $user, authProfileId: $authId) {
      id
      email
    }
  }
`;

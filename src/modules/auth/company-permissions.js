import { COMPANY_ADMINISTRATOR } from '../../shared/roles';

/**
 * Checks if a User can Edit a Company
 * @param {User} user The user
 * @param {Company} company The company
 */
export const canEditCompanyPermission = (user, company) => {
  const companyOwner = company.companyUserRelation.items.find(
    (companyInformation) => companyInformation.role.name === COMPANY_ADMINISTRATOR,
  );

  if (!companyOwner) return false;

  if (user.id === companyOwner.user.id) return true;

  return false;
};

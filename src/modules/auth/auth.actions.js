import {
  USER_QUERY,
  USERS_AND_ROLES_QUERY,
  INVITATIONS_QUERY,
  SESSION_QUERY,
  CREATE_USER_QUERY,
} from './queries';
import Flux from '@cobuildlab/flux-state';

import {
  SESSION_ERROR,
  INVITATIONS_EVENT,
  APOLLO_CLIENT,
  USER_AND_ROLES_EVENT,
  INVITATIONS_USERS_EVENT,
  NEW_SESSION_EVENT,
  NEW_USER_EVENT,
  AUTH_LOCK_LOGIN_EVENT,
} from '../../shared/SessionStore';
import sessionStore from '../../shared/SessionStore';
import { META_ALLIANCE_SELECTED } from '../../shared';
import * as jwtToken from 'jwt-decode';
import { log, error } from '@cobuildlab/pure-logger';
import { WebAuth0AuthClient } from '@8base/web-auth0-auth-client';
import Auth0Lock from 'auth0-lock';

const { REACT_APP_AUTH_PROVIDER_ID, REACT_APP_CLIENT_ID, REACT_APP_DOMAIN } = process.env;

export const auth0WebClient = new WebAuth0AuthClient({
  domain: REACT_APP_DOMAIN,
  clientId: REACT_APP_CLIENT_ID,
  redirectUri: `${window.location.origin}/auth/callback`,
  logoutRedirectUri: `${window.location.origin}/home`,
});

const lock = new Auth0Lock(REACT_APP_CLIENT_ID, REACT_APP_DOMAIN, {
  loginAfterSignUp: false,
  configurationBaseUrl: 'https://cdn.auth0.com',
  auth: {
    responseType: 'id_token',
    redirectUrl: `${window.location.origin}/home`,
  },
  theme: {
    logo: `${window.location.origin}/favicon.png`,
    primaryColor: '#3eb7f9',
  },
  languageDictionary: {
    title: 'Log In',
    success: {
      signUp: `Thanks for signing up. We've just send you an email to verify your account in order to log in.`,
    },
  },
});

/**
 * On Auth0Lock login event handler
 * @type {event}
 */
lock.on('authenticated', (credentials) => {
  Flux.dispatchEvent(AUTH_LOCK_LOGIN_EVENT, credentials);
  lock.hide();
});

/**
 * Show the Auth0 Lock Widget on login screen
 */
export const showAuth0LockLogin = () => {
  lock.show({ initialScreen: 'login' });
};

/**
 * Show the Auth0 Lock Widget on signUp screen
 */
export const showAuth0LockSignup = () => {
  lock.show({ initialScreen: 'signUp' });
};

/**
 * Fetches The logged user invitations, both Company and Alliance, and The User Data as well
 * @param
 * @return {Promise<void>}
 */
export const fetchUserWithInvitations = async () => {
  let response;
  try {
    response = {
      user: await fetchUser(),
      invitations: await fetchInvitations(),
    };
  } catch (e) {
    error('fetchUserWithInvitations', e);
    return Flux.dispatchEvent(SESSION_ERROR, e);
  }
  log('fetchUserWithInvitations', response);
  Flux.dispatchEvent(INVITATIONS_USERS_EVENT, response);
  return response;
};

/**
 * Filter invitations by alliance and by user's email
 * @param  {String} allianceId
 * @param  {string} email
 * @return {Filter} filter
 */
const invitationsFilter = (email, allianceId) => {
  const OR = [{ email: { equals: email } }];

  if (allianceId) OR.push({ alliance: { id: { equals: allianceId } } });

  const invitationsFilter = { OR };

  return invitationsFilter;
};

/**
 * Fetches The logged user invitations, both Company and Alliance, and Alliance
 * member
 * @return {Promise<void>}
 */
export const fetchInvitations = async () => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { user, selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
  const allianceId = selectedAlliance ? selectedAlliance.id : null;

  const filter = invitationsFilter(user.email, allianceId);

  const variables = {
    allianceMemberInvitationFilter: filter,
    allianceData: filter,
  };
  let response;
  try {
    response = await client.query({
      query: INVITATIONS_QUERY,
      fetchPolicy: 'network-only',
      variables,
    });
  } catch (e) {
    error('fetchInvitations', e);
    return Flux.dispatchEvent(SESSION_ERROR, e);
  }
  log('fetchInvitations', response.data);
  Flux.dispatchEvent(INVITATIONS_EVENT, response.data);
  return response.data;
};

/**
 * Fetches a User Profile by Id or the logged User if no Id provided
 * @param id
 * @return {Promise<void>}
 */
export const fetchUser = async (id: ?string): void => {
  const client = sessionStore.getState(APOLLO_CLIENT);

  let response;
  try {
    if (id === 0) {
      response = await client.query({ query: USER_QUERY, fetchPolicy: 'network-only' });
    } else {
      response = await client.query({
        query: USER_QUERY,
        fetchPolicy: 'network-only',
        variables: { id },
      });
    }
  } catch (e) {
    error('fetchUser', e);
    throw e;
  }
  log('fetchUser', response.data.user);
  return response.data.user;
};

export const createUserWithToken = async (token: string): void => {
  const { email } = jwtToken(token);

  // const client = new ApolloClient({
  //   uri: REACT_APP_8BASE_API_ENDPOINT,
  // });
  const client = sessionStore.getState(APOLLO_CLIENT);
  const data = {
    user: { email },
    authId: REACT_APP_AUTH_PROVIDER_ID,
  };

  let response;
  try {
    response = await client.mutate({
      mutation: CREATE_USER_QUERY,
      variables: data,
    });
  } catch (e) {
    error('createUserWithToken', e);
    return Flux.dispatchEvent(SESSION_ERROR, e);
  }
  log('createUserWithToken', response.data);
  Flux.dispatchEvent(NEW_USER_EVENT, response.data);
  return response.data;
};

/**
 * Fetches the logged User Profile with invitations and Approved Alliances
 * @return {Promise<void>}
 */
export const fetchSession = async (): void => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  let user = null;
  try {
    user = await fetchUser();
  } catch (e) {
    error('fetchSession', e);
    Flux.dispatchEvent(SESSION_ERROR, e);
    throw e;
  }

  let response;
  try {
    response = await client.query({
      query: SESSION_QUERY,
      fetchPolicy: 'network-only',
      variables: {
        id: user.id,
        allianceData: { email: { equals: user.email } },
        allianceMemberInvitationFilter: { email: { equals: user.email } },
        alliancesFilter: {
          allianceUserAllianceRelation: {
            some: { companyUser: { user: { id: { equals: user.id } } } },
          },
        },
        CompanyFilter: {
          companyUserRelation: {
            some: { user: { id: { equals: user.id } } },
          },
        },
      },
    });
  } catch (e) {
    error('fetchSession', e);
    return Flux.dispatchEvent(SESSION_ERROR, e);
  }
  let selectedAlliance = null;
  const metaAlliance =
    user.metaRelation.items.find((meta) => meta.name === META_ALLIANCE_SELECTED) || {};
  //
  const userAlliances = response.data.alliancesList.items;
  userAlliances.forEach((alliance) => {
    if (alliance.id === metaAlliance.value) selectedAlliance = alliance;
  });
  const session = { ...response.data, selectedAlliance, userAlliances };
  log('fetchSession', session);
  Flux.dispatchEvent(NEW_SESSION_EVENT, session);
  return session;
};

/**
 * Fetches all Application Users and Roles
 * @return {Promise<void>}
 */
export const fetchUsersAndRoles = async () => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  let response;
  try {
    response = await client.query({ query: USERS_AND_ROLES_QUERY });
  } catch (e) {
    error('fetchUsersAndRoles', e);
    return Flux.dispatchEvent(SESSION_ERROR, e);
  }
  log('fetchUsersAndRoles', response.data);
  Flux.dispatchEvent(USER_AND_ROLES_EVENT, response.data);
  return response.data;
};

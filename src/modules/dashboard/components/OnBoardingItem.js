import { Grid, Text, Paper } from '@8base/boost';
import React from 'react';
import { Link } from 'react-router-dom';
import { PropTypes } from 'prop-types';
import styled from '@emotion/styled';

const StyledPaper = styled(Paper)`
  &:not(:hover) {
    box-shadow: none;
  }
`;

const StyleCircle = {
  borderRadius: '100%',
  width: '27px',
  height: '27px',
  boxSizing: 'border-box',
  border: '2px solid #EAEDF5',
};

const StyleCircleCheck = {
  boxSizing: 'border-box',
  height: '27px',
  width: '27px',
  border: '2px solid #3EB7F9',
  backgroundColor: '#3EB7F9',
  boxShadow: '0 0 8px 0 rgba(147,158,167,0.35)',
  borderRadius: '100%',
  textAlign: 'center',
};

const StyleImage = {
  width: '15px',
  height: '23px',
};

const OnBoardingItem = ({ description, secondaryText, linkText, icon, href, checked = false }) => {
  return (
    <StyledPaper>
      <Grid.Layout
        gap={'xs'}
        columns="40px auto 40px"
        className={'onBoardingItem'}
        style={{ width: '100%' }}>
        <Grid.Box>
          <img width={'38px'} height={'44px'} alt={'profile'} src={icon} />
        </Grid.Box>
        <Grid.Box>
          <Link className="onboarding-name" to={href}>
            {linkText}
          </Link>
          <Text style={{ fontSize: '15px', marginBottom: '2px' }} disabled>
            {secondaryText}
          </Text>
          <Text className="onboarding-text">{description}</Text>
        </Grid.Box>
        <Grid.Box>
          {checked ? (
            <div style={StyleCircleCheck}>
              <img style={StyleImage} alt="Checked" src={require('../assets/check-mark.svg')} />
            </div>
          ) : (
            <div style={StyleCircle} />
          )}
        </Grid.Box>
      </Grid.Layout>
    </StyledPaper>
  );
};

OnBoardingItem.propTypes = {
  description: PropTypes.string.isRequired,
  secondaryText: PropTypes.string.isRequired,
  linkText: PropTypes.string.isRequired,
  icon: PropTypes.object.isRequired,
  href: PropTypes.string.isRequired,
  checked: PropTypes.bool,
};
OnBoardingItem.defaultProps = {
  checked: PropTypes.bool,
};

export { OnBoardingItem };

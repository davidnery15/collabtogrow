import React from 'react';
import {
  NoData,
  Column,
  Grid,
  Row,
  Select,
  Text,
  Progress,
  Card,
  Heading,
  Loader,
} from '@8base/boost';
import { onErrorMixin } from '../../../shared/mixins';
import View from '@cobuildlab/react-flux-state';
import { DASHBOARD_ERROR, DASHBOARD_ITEMS, dashboardStore } from '../dashboard-store';
import { fetchDashboardData, getChartOptions, getWeekForCalendar } from '../dashboard-actions';
import { getAMOItemType } from '../../management/amo-item';
import { withRouter } from 'react-router-dom';
import moment from 'moment';
import arrowToRight from '../../../images/icons/arrow-pointing-to-right.svg';
import ActiveItem from '../../active-items/ActiveItem';
import UserSmallAvatar from '../../../components/user/UserSmallAvatar';
import ItemsCard from '../../active-items/work-queue/components/ItemsCard';
import workQueueStore, {
  WORK_QUEUE_ERROR_EVENT,
  WORK_QUEUE_ITEMS_EVENT,
} from '../../active-items/work-queue/work-queue-store';
import {
  getUserActiveItems,
  getTasksForDate,
  getInitiativeActiveItems,
  filterActiveItemsByDueDate,
} from '../../active-items/active-items-utils';
import { fetchInitiativeList } from '../../management/initiative/initiative-actions';
import { fetchInitiativeActiveItems } from '../../active-items/initiative-active-items/initiative-active-items-actions';
import { fetchWorkQueueItems } from '../../active-items/work-queue/work-queue-actions';
import initiativeActiveItemsStore, {
  INITIATIVE_ACTIVE_ITEMS_ERROR_EVENT,
  INITIATIVE_ACTIVE_ITEMS_EVENT,
} from '../../active-items/initiative-active-items/initiative-active-items-store';
import sessionStore, { NEW_SESSION_EVENT } from '../../../shared/SessionStore';
import initiativeStore, {
  INITIATIVE_LIST_EVENT,
  INITIATIVE_ERROR_EVENT,
} from '../../management/initiative/initiative-store';
import WeekComponent from './WeekComponent';
import YesNoDialog from '../../../components/dialogs/YesNoDialog';
import nextStepStore, { NEXT_STEP_COMPLETE_EVENT } from '../../next-step/next-step-store';
import ideaStore, { IDEA_COMPLETED_EVENT } from '../../management/idea/idea-store';
import issueStore, { ISSUE_COMPLETED_EVENT } from '../../management/issue/issue-store';
import riskStore, { RISK_COMPLETED_EVENT } from '../../management/risk/risk-store';
import actionStore, { ACTION_COMPLETE_EVENT } from '../../management/action/action-store';
import fundingRequestStore, {
  FUNDING_REQUEST_COMPLETED_EVENT,
} from '../../management/funding-request/funding-request-store';
import * as toast from 'components/toast/Toast';
import PieChart from '../../../components/PieChart';
import Status from '../../../components/Status';

/**
 * Main screen of the user when it logs in
If the user hasn't completed the Sign Up, it redirects to the Wizard View.
 *
 * @param due
 * @param event
 * @param completeFunction
 * @param selectedItemToComplete
 * @param selectedDay
 * @param initiativeId
 * @param selectedInitiative
 * @param item
 */
class PrivateGraphsDashboard extends View {
  constructor(props) {
    super(props);
    this.user = null;
    this.state = {
      items: [],
      companyOnState: null,
      initiatives: [],
      allItems: 0,
      inProgresItems: 0,
      completedItems: 0,
      itemsToShow: [],
      loadingInitiativeData: true,
      selectedInitiative: null,
      tasksByDate: {},
      selectedDay: moment().format('YYYY-MM-DD'),
      loadingTasks: true,
      week: [],
      dateReference: moment().format('YYYY-MM-DD'),
      overDueItems: [],
      completeModalIsOpen: false,
      selectedItemToComplete: null,
      completeFunction: null,
    };
    this.user = sessionStore.getState(NEW_SESSION_EVENT).user;
    this.selectedAlliance = sessionStore.getState(NEW_SESSION_EVENT).selectedAlliance;
    this.onError = onErrorMixin.bind(this);
  }

  componentDidMount() {
    this.subscribe(workQueueStore, WORK_QUEUE_ERROR_EVENT, (e) => {
      this.onError(e);
      this.setState({ loadingTasks: false });
    });
    this.subscribe(dashboardStore, DASHBOARD_ERROR, this.onError);
    this.subscribe(initiativeActiveItemsStore, INITIATIVE_ACTIVE_ITEMS_ERROR_EVENT, this.onError);
    this.subscribe(initiativeStore, INITIATIVE_ERROR_EVENT, (e) => {
      this.onError(e);
      this.setState({ loadingInitiativeData: false });
    });
    this.subscribe(nextStepStore, NEXT_STEP_COMPLETE_EVENT, () => {
      toast.success('Next Step Successfully Completed');
      fetchWorkQueueItems();
      this.fetchInitiativeActiveItems();
    });
    this.subscribe(ideaStore, IDEA_COMPLETED_EVENT, () => {
      toast.success('Idea Successfully Completed');
      fetchWorkQueueItems();
      this.fetchInitiativeActiveItems();
    });
    this.subscribe(issueStore, ISSUE_COMPLETED_EVENT, () => {
      toast.success('Issue Successfully Completed');
      fetchWorkQueueItems();
      this.fetchInitiativeActiveItems();
    });
    this.subscribe(riskStore, RISK_COMPLETED_EVENT, () => {
      toast.success('Risk Successfully Completed');
      fetchWorkQueueItems();
      this.fetchInitiativeActiveItems();
    });
    this.subscribe(actionStore, ACTION_COMPLETE_EVENT, () => {
      toast.success('Action Successfully Completed');
      fetchWorkQueueItems();
      this.fetchInitiativeActiveItems();
    });
    this.subscribe(fundingRequestStore, FUNDING_REQUEST_COMPLETED_EVENT, () => {
      toast.success('Funding Request Successfully Completed');
      fetchWorkQueueItems();
      this.fetchInitiativeActiveItems();
    });

    this.subscribe(initiativeStore, INITIATIVE_LIST_EVENT, (data) => {
      const initiatives = data.initiativesList.items;
      const selectedInitiative = initiatives.length ? initiatives[0] : null;

      if (!selectedInitiative) {
        return this.setState({ loadingInitiativeData: false });
      }

      this.setState(
        {
          initiatives,
          selectedInitiative,
        },
        () => this.fetchInitiativeActiveItems(),
      );
    });

    this.subscribe(workQueueStore, WORK_QUEUE_ITEMS_EVENT, (state) => {
      const { items } = state.itemsList;
      const { items: initiatives } = state.initiativesList;
      const { allActiveItems } = getUserActiveItems(items, initiatives);

      const { overDueItems, dueThisWeekItems, notDueItems } = filterActiveItemsByDueDate(
        allActiveItems,
      );

      const notDueAndDueThisWeekActiveItems = dueThisWeekItems.concat(notDueItems);

      const { dateReference } = this.state;
      const week = getWeekForCalendar(dateReference);
      const tasksByDate = getTasksForDate(notDueAndDueThisWeekActiveItems);

      this.setState({
        loadingTasks: false,
        week,
        tasksByDate,
        overDueItems,
      });
    });

    this.subscribe(initiativeActiveItemsStore, INITIATIVE_ACTIVE_ITEMS_EVENT, (state) => {
      const { items } = state.itemsList;
      const {
        allActiveItems,
        inProgresActiveItems,
        completedActiveItems,
      } = getInitiativeActiveItems(items, []);

      const itemsToShow = [
        { count: allActiveItems.length, text: 'All Items' },
        { count: inProgresActiveItems.length, text: 'In Progress' },
        { count: completedActiveItems.length, text: 'Completed' },
      ];

      this.setState({
        loadingInitiativeData: false,
        allItems: allActiveItems.length,
        inProgresItems: inProgresActiveItems.length,
        completedItems: completedActiveItems.length,
        itemsToShow,
      });
    });

    this.subscribe(dashboardStore, DASHBOARD_ITEMS, (state) => {
      const {
        amoItems: {
          itemsList: { items },
        },
      } = state;
      this.setState({ items: items.map((item) => getAMOItemType(item)) });
    });

    fetchDashboardData();
    fetchWorkQueueItems();
    fetchInitiativeList('', 1, 1000);
  }

  fetchInitiativeActiveItems = () => {
    if (this.state.selectedInitiative) {
      fetchInitiativeActiveItems(this.state.selectedInitiative.id);
    } else this.setState({ loadingInitiativeData: false });
  };

  goToMyWorkQueue = () => {
    this.props.history.push('/active-items/my-work-queue');
  };

  goToAMOItems = () => {
    this.props.history.push('/management/amo-item');
  };

  goToDeals = (event) => {
    const stage = event.data.type;
    this.props.history.push(`/management/deal?q=${stage}`);
  };

  onClickDue = (due, item) => {
    this.props.history.push(`/management/amo-item?due=${due}&item=${item}`);
  };

  onClickChart = (event) => {
    let due = event.data.type;
    let item = event.data.itemType;
    this.props.history.push(`/management/amo-item?due=${due}&item=${item}`);
  };

  onSelectInitiative = (selectedInitiative) => {
    this.setState({ selectedInitiative, loadingInitiativeData: true }, () =>
      fetchInitiativeActiveItems(selectedInitiative.id),
    );
  };

  onClickInitiativeItems = (item, initiativeId) => {
    this.props.history.push(`/active-items/initiative/${initiativeId}`);
  };

  onSelectDay = (selectedDay) => {
    this.setState({ selectedDay });
  };

  onRightClick = () => {
    const dateReference = moment(this.state.dateReference).add(7, 'days');
    const week = getWeekForCalendar(dateReference);

    this.setState({
      week,
      dateReference,
    });
  };

  onLeftClick = () => {
    const dateReference = moment(this.state.dateReference).subtract(7, 'days');
    const week = getWeekForCalendar(dateReference);

    this.setState({
      week,
      dateReference,
    });
  };

  onClickComplete = (completeFunction, selectedItemToComplete) => {
    this.setState({
      completeModalIsOpen: true,
      completeFunction,
      selectedItemToComplete,
    });
  };

  onYesComplete = () => {
    const { completeFunction, selectedItemToComplete } = this.state;
    this.setState(
      {
        completeModalIsOpen: false,
        loadingTasks: true,
      },
      () => {
        completeFunction(selectedItemToComplete);
      },
    );
  };

  onCloseComplete = () => {
    this.setState({
      completeModalIsOpen: false,
    });
  };

  render() {
    const {
      items,
      selectedInitiative,
      initiatives,
      loadingInitiativeData,
      completedItems,
      allItems,
      itemsToShow,
      loadingTasks,
      tasksByDate,
      selectedDay,
      week,
      overDueItems,
      completeModalIsOpen,
    } = this.state;
    const { risksOptions, issuesOptions, actionsOptions } = getChartOptions(items);
    const onEvents = {
      click: this.onClickChart,
    };
    const selectedDayFormat = moment(selectedDay).format('dddd MMMM Do');
    const progressValue = Math.trunc((completedItems * 100) / allItems) || 0;
    // fromNowDate
    const momentSelectedDay = moment(selectedDay);
    const fromNowDate =
      moment().diff(momentSelectedDay, 'days') >= 1
        ? momentSelectedDay.fromNow()
        : momentSelectedDay.calendar().split(' ')[0];

    const noData = !tasksByDate[selectedDay] && overDueItems.length === 0 ? <NoData /> : null;

    return (
      <>
        <Grid.Layout gap={'lg'} columns={'auto'} areas={[['left', 'center', 'right']]}>
          <Grid.Box area="left">
            <PieChart
              title="Risks"
              itemName="RISK"
              paperPadding="md"
              onEvents={onEvents}
              data={risksOptions}
              onClickLegend={this.onClickDue}
            />
          </Grid.Box>
          <Grid.Box area="center">
            <PieChart
              title="Issues"
              itemName="ISSUE"
              paperPadding="md"
              onEvents={onEvents}
              data={issuesOptions}
              onClickLegend={this.onClickDue}
            />
          </Grid.Box>
          <Grid.Box area="right">
            <PieChart
              title="Actions"
              itemName="ACTION"
              paperPadding="md"
              onEvents={onEvents}
              data={actionsOptions}
              onClickLegend={this.onClickDue}
            />
          </Grid.Box>
        </Grid.Layout>

        <Grid.Layout
          style={{ padding: '24px 0' }}
          gap={'lg'}
          columns={'1.6fr 1.3fr'}
          areas={[['tasks', 'initiatives']]}>
          <Grid.Box area="tasks">
            <Card
              style={{
                maxHeight: '400px',
                minHeight: '400px',
              }}>
              <Card.Header>
                <Row growChildren alignItems="center" style={{ width: '100%' }}>
                  <Column alignItems="start">
                    <Heading type="h4" text={`Tasks for ${fromNowDate}, ${selectedDayFormat}`} />
                  </Column>
                  <Column alignItems="end">
                    <Row alignItems="center">
                      <Text
                        onClick={this.goToMyWorkQueue}
                        style={{ color: '#3EB7F9', cursor: 'pointer' }}>
                        View all tasks
                      </Text>
                      <img
                        onClick={this.goToMyWorkQueue}
                        style={{ width: '16px', cursor: 'pointer' }}
                        src={arrowToRight}
                        alt=""
                      />
                    </Row>
                  </Column>
                </Row>
              </Card.Header>
              <Card.Body>
                {!loadingTasks ? (
                  <>
                    <Row alignItems="center" justifyContent="center">
                      <WeekComponent
                        week={week}
                        onSelectDay={this.onSelectDay}
                        selectedDay={selectedDay}
                        onLeftClick={this.onLeftClick}
                        onRightClick={this.onRightClick}
                      />
                    </Row>
                    <Row offsetTop={'sm'} style={{ width: '100%' }}>
                      <Grid.Layout style={{ width: '100%' }}>
                        {overDueItems
                          ? overDueItems.map((item, i) => (
                              <ActiveItem
                                item={item}
                                key={i}
                                onClickComplete={this.onClickComplete}
                                color="#FCBD00"
                              />
                            ))
                          : null}
                        {tasksByDate[selectedDay]
                          ? tasksByDate[selectedDay].map((task, i) => (
                              <ActiveItem item={task} key={i} color="#FCBD00" />
                            ))
                          : null}

                        {noData}
                      </Grid.Layout>
                    </Row>
                  </>
                ) : (
                  <Loader stretch />
                )}
              </Card.Body>
            </Card>
          </Grid.Box>

          <Grid.Box area="initiatives">
            <Card
              style={{
                maxHeight: '400px',
                minHeight: '400px',
              }}>
              <Card.Header>
                <Row growChildren alignItems="center" style={{ width: '100%' }}>
                  <Column alignItems="start">
                    <Heading type="h4" text={'Initiative State'} />
                  </Column>
                  <Column alignItems="end">
                    <Select
                      onChange={(initiative) => this.onSelectInitiative(initiative)}
                      value={selectedInitiative}
                      options={initiatives.map((initiative) => {
                        return {
                          label: initiative.name,
                          value: initiative,
                        };
                      })}
                    />
                  </Column>
                </Row>
              </Card.Header>
              <Card.Body>
                {!loadingInitiativeData ? (
                  <>
                    {selectedInitiative ? (
                      <>
                        <Row growChildren>
                          <Column>
                            <Text weight="bold">{selectedInitiative.name}</Text>
                            <Status status={selectedInitiative.ragStatus} />
                            {/* <Progress
                              size="sm"
                              color={selectedInitiative.ragStatus}
                              value={progressValue}
                            /> */}
                            <UserSmallAvatar
                              owner={selectedInitiative.owner}
                              text="is the owner of this initiative"
                            />
                          </Column>
                        </Row>
                        <Row offsetTop="lg" gap="lg" growChildren>
                          {itemsToShow.map((item, i) => {
                            const { text, count } = item;

                            return (
                              <ItemsCard
                                key={i}
                                text={text}
                                width="50px"
                                count={count}
                                onClick={() =>
                                  this.onClickInitiativeItems(item, selectedInitiative.id)
                                }
                              />
                            );
                          })}
                        </Row>{' '}
                      </>
                    ) : (
                      <NoData />
                    )}
                  </>
                ) : (
                  <Loader stretch />
                )}
              </Card.Body>
            </Card>
          </Grid.Box>
        </Grid.Layout>
        <YesNoDialog
          isOpen={completeModalIsOpen}
          onYes={this.onYesComplete}
          onNo={this.onCloseComplete}
          onClose={this.onCloseComplete}
          text={'Are you sure you want to Mark this Item as Completed?'}
          title={'Mark Completed'}
        />
      </>
    );
  }
}

const GraphsDashboard = withRouter(PrivateGraphsDashboard);
export { GraphsDashboard };

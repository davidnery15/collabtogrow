import { Row, Text } from '@8base/boost';
import React from 'react';

export const EmptyDashboard = () => {
  return (
    <Row growChildren>
      <Text> {`You don't have Active Alliances yet!`}</Text>
    </Row>
  );
};

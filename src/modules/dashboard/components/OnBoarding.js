import { Grid, Paper } from '@8base/boost';
import React from 'react';
import { ActionButtonClose } from '../../../components/buttons/ActionButtonClose';
import { OnBoardingItem } from './OnBoardingItem';
import { Separator } from '../../../components/Separator';
import View from '@cobuildlab/react-flux-state';
import wizardStore, { USER_INFORMATION_EVENT } from '../../wizard/WizardStore';
import { fetchUserInformation } from '../../wizard/wizard.actions';
import { pct } from '../../../shared/utils';
import sessionStore, { NEW_SESSION_EVENT } from '../../../shared/SessionStore';
import { fetchAlliance } from '../../settings/alliance-management/alliance-actions';
import allianceStore, {
  ALLIANCE_DETAIL_EVENT,
  ALLIANCE_MEMBER_INVITATIONS_LIST_EVENT,
} from '../../settings/alliance-management/alliance-store';
import { ALLIANCE_INVITATION_ACCEPTED, ALLIANCE_IN_PROGRESS } from '../../../shared/status';
import { fetchInitiativeList } from '../../management/initiative/initiative-actions';
import initiativeStore, {
  INITIATIVE_LIST_EVENT,
} from '../../management/initiative/initiative-store';
import { META_SHOW_ON_BOARDING } from '../../../shared';
import { updateMeta } from '../dashboard-actions';
import { ALLIANCE_SER } from '../../../shared/roles';
import { fetchAllianceMemberInvitationsList } from '../../settings/alliance-management/alliance-actions';

export class OnBoarding extends View {
  constructor(props) {
    super(props);
    this.state = {
      userProfileCompleted: false,
      userProfilePct: 0,
      inviteSerCompleted: false,
      inviteSerText: '',
      partnerCompanyCompleted: false,
      partnerCompanyText: '',
      initiativesCompleted: false,
      initiativesText: '',
      hide: false,
      alliance: '',
    };
  }

  componentDidMount(): void {
    this.subscribe(wizardStore, USER_INFORMATION_EVENT, async (data) => {
      const userInformation = data.userInformationsList.items[0];
      const keys = Object.keys(userInformation);
      const total = keys.length;
      let filled = 0;
      keys.forEach((key) => {
        const value = userInformation[key];
        if (value !== null && value !== '') filled++;
      });
      const pctCompleted = pct(total, filled);
      this.setState({
        userProfileCompleted: pctCompleted > 85,
        userProfilePct: pctCompleted,
      });
    });
    this.subscribe(allianceStore, ALLIANCE_DETAIL_EVENT, (state) => {
      const { alliance } = state;

      // Partner Company
      const partnerCompanyCompleted = !!alliance.partnerCompany;
      const partnerCompanyText = alliance.partnerCompany
        ? `Partner: ${alliance.partnerCompany.name}`
        : `Invite a Partner`;
      this.setState({
        partnerCompanyCompleted,
        partnerCompanyText,
        alliance,
      });
    });

    this.subscribe(initiativeStore, INITIATIVE_LIST_EVENT, (state) => {
      const { count } = state.initiativesList;
      const initiativesCompleted = count > 0;
      const initiativesText =
        count === 1 ? `(1) Initiative created` : `(${count}) Initiatives created`;
      this.setState({ initiativesCompleted, initiativesText });
    });

    this.subscribe(allianceStore, ALLIANCE_MEMBER_INVITATIONS_LIST_EVENT, (state) => {
      const { allianceMemberInvitationsList } = state;

      const {
        user: {
          companyUserRelation: { items: company },
        },
      } = sessionStore.getState(NEW_SESSION_EVENT);

      let companyInfo = company.length !== 0 ? company[0] : null;

      let inviteSerCompleted = false;
      let invitesCounter = 0;

      if (companyInfo && allianceMemberInvitationsList.items.length !== 0) {
        allianceMemberInvitationsList.items.forEach((allianceUser) => {
          if (
            allianceUser.company.id === companyInfo.company.id &&
            allianceUser.role.name === ALLIANCE_SER &&
            allianceUser.status === ALLIANCE_INVITATION_ACCEPTED
          ) {
            inviteSerCompleted = true;
            invitesCounter++;
          }
        });
      }

      const inviteSerText =
        invitesCounter === 1 ? `(1) invitation sent` : `(${invitesCounter} invitations sent)`;

      this.setState({
        inviteSerText,
        inviteSerCompleted,
        invitesCounter,
      });
    });

    const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);

    if (selectedAlliance) {
      fetchAlliance(selectedAlliance.id);
      fetchAllianceMemberInvitationsList(selectedAlliance.id);
    }

    setTimeout(() => {
      fetchUserInformation();
      fetchInitiativeList('', 1, 1000);
    });
  }

  render() {
    //
    if (this.state.hide === true) return null;

    const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);

    // User Profile
    const { userProfileCompleted, userProfilePct } = this.state;

    // Business Case
    const allianceSubmitted = selectedAlliance.status !== ALLIANCE_IN_PROGRESS;
    const allianceSubmittedText = `Alliance: ${selectedAlliance.status}`;

    // Invite your Colleagues

    // Invite SER
    const { inviteSerCompleted, inviteSerText, alliance } = this.state;

    //Partner
    const { partnerCompanyText, partnerCompanyCompleted } = this.state;

    // initiatives
    const { initiativesCompleted, initiativesText } = this.state;

    const showDismiss = () => {
      if (
        [
          userProfileCompleted,
          allianceSubmitted,
          inviteSerCompleted,
          partnerCompanyCompleted,
          initiativesCompleted,
        ].filter((v) => v === true).length >= 2
      ) {
        return true;
      } else {
        return false;
      }
    };

    return (
      <Paper padding="none">
        <Grid.Layout inline padding={'sm'} columns="auto" areas={[['title', 'title', 'option']]}>
          <Grid.Box
            area="title"
            textAlign={'center'}
            alignItems={'start'}
            alignSelf={'center'}
            style={{ fontSize: '20px', padding: '16px 0 8px 24px' }}>
            Get Started
          </Grid.Box>
          <Grid.Box area="option" alignItems={'end'} textAlign={'right'} justifySelf={'end'}>
            {showDismiss() && (
              <ActionButtonClose
                onClick={() => {
                  this.setState({ hide: true });
                  updateMeta(META_SHOW_ON_BOARDING, 'any-value');
                }}
              />
            )}
          </Grid.Box>
        </Grid.Layout>
        <Separator />
        <Grid.Layout
          gap={'lg'}
          padding={'md'}
          columns="auto"
          areas={[
            ['left1', 'center1', 'right1'],
            ['left2', 'center2', 'right2'],
          ]}
          style={{ padding: '0 23px 6.5px' }}>
          <Grid.Box area="left1">
            <OnBoardingItem
              description={'Finish setting up your personal profile.'}
              secondaryText={`${userProfilePct}% completed`}
              linkText={'Complete Profile'}
              icon={require('../assets/complete-profile.svg')}
              href={'/settings/user-profile/'}
              checked={userProfileCompleted}
            />
          </Grid.Box>
          <Grid.Box area="center1">
            <OnBoardingItem
              description={
                "Don't forget to invite your SER. The SER will need to approve the business case."
              }
              secondaryText={inviteSerText}
              linkText={'Invite SER'}
              icon={require('../assets/invite-ser.svg')}
              href={`/settings/alliance-management/members/${alliance.id}`}
              checked={inviteSerCompleted}
            />
          </Grid.Box>
          <Grid.Box area="right1">
            <OnBoardingItem
              description={'Submit your Alliance for Approval.'}
              secondaryText={allianceSubmittedText}
              linkText={'SUBMIT ALLIANCE FOR APPROVAL'}
              icon={require('../assets/business-case.svg')}
              href={'/settings/alliances/'}
              checked={allianceSubmitted}
            />
          </Grid.Box>
          <Grid.Box area="left2">
            <OnBoardingItem
              description={
                'Invite your partner company to start collaborating on your business case.'
              }
              secondaryText={partnerCompanyText}
              linkText={'Invite partner company'}
              icon={require('../assets/invite-partner.svg')}
              href={'/settings/alliances/'}
              checked={partnerCompanyCompleted}
            />
          </Grid.Box>
          <Grid.Box area="center2">
            <OnBoardingItem
              description={'Create your first initiative.'}
              secondaryText={initiativesText}
              linkText={'Create Initiative'}
              icon={require('../assets/create-initiative.svg')}
              href={'/management/initiative/'}
              checked={initiativesCompleted}
            />
          </Grid.Box>
        </Grid.Layout>
      </Paper>
    );
  }
}

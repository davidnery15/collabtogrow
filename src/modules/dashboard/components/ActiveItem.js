import React from 'react';
import { Grid, Heading, Text, ButtonGroup, Button } from '@8base/boost';
import { PropTypes } from 'prop-types';
import { ColorRelatedItems } from '../../../shared/components/ColorRelatedItems';
import { withRouter } from 'react-router-dom';
import UserSmallAvatar from '../../../components/user/UserSmallAvatar';
import styled from 'styled-components';
import Moment from 'react-moment';
import moment from 'moment';

const BUTTONS_HEIGHT = 60;
const ButtonLeft = styled(Button)`
  cursor: default !important;
  height: ${BUTTONS_HEIGHT}px !important;
  padding: 0 20px;
`;
const ButtonRight = styled(Button)`
  cursor: default !important;
  background-color: #edf0f2 !important;
  height: ${BUTTONS_HEIGHT}px !important;
  min-width: 200px !important;
  max-width: 200px !important;
  width: 200px !important;
`;

const ActiveItem = ({ item, color }) => {
  const revisedDueDate = item.revisedDueDate ? (
    moment(item.revisedDueDate).format('MMM Do YY') === moment().format('MMM Do YY') ? (
      <Text weight="bold">Today</Text>
    ) : (
      <Moment format="MMMM Do, YYYY">{item.revisedDueDate}</Moment>
    )
  ) : (
    <Text disabled>Not set</Text>
  );

  // const onClickDetails = item.detailsUrl ? () => history.push(item.detailsUrl) : null;
  const isGreenDetail = item.detail !== 'Alliance Major Event';
  return (
    <Grid.Box stretch style={{ margin: 10, width: '100%' }}>
      <ButtonGroup>
        <ButtonLeft stretch color="neutral" variant="outlined">
          <ColorRelatedItems value={item} color={color} />
          <Grid.Layout stretch columns="auto 150px">
            <Grid.Box alignItems="start">
              <Heading type="h6">
                <Text weight={'bold'}>{item.title}</Text>
              </Heading>
              <UserSmallAvatar owner={item.owner} text={item.ownerText} src={item.src} />
            </Grid.Box>
            <Grid.Box alignItems="center" justifyContent="center">
              {/* <Dropdown defaultOpen={false}>
                <Dropdown.Head>
                  <Icon name="More" className="more-icon" />
                </Dropdown.Head>
                <Dropdown.Body> */}
              {/* {({ closeDropdown }) => ( */}
              {/* <Menu> */}
              {/* <Menu.Item onClick={(closeDropdown, onClickDetails)}>Details</Menu.Item> */}
              {item.detail ? (
                <div
                  style={{
                    padding: '5px',
                    background: isGreenDetail ? 'rgba(112, 212, 75, 0.2)' : 'rgba(252,189,0,0.2)',
                    border: `1px solid ${isGreenDetail ? '#70D44B' : '#FCBD00'}`,
                    borderRadius: '12px',
                  }}>
                  <Text
                    weight={'bold'}
                    style={{ fontSize: '13px', color: isGreenDetail ? '#70D44B' : '#FCBD00' }}>
                    {item.detail}
                  </Text>
                </div>
              ) : null}
              {/* </Menu> */}
              {/* )} */}
              {/* </Dropdown.Body>
              </Dropdown> */}
            </Grid.Box>
          </Grid.Layout>
        </ButtonLeft>
        <ButtonRight color="neutral" variant="outlined">
          {revisedDueDate}
        </ButtonRight>
      </ButtonGroup>
    </Grid.Box>
  );
};

ActiveItem.propTypes = {
  // showNextSteps: PropTypes.bool.isRequired,
  item: PropTypes.object.isRequired,
  // history: PropTypes.object.isRequired,
  // onClickComplete: PropTypes.func,
  color: PropTypes.string.isRequired,
};

ActiveItem.defaultProps = {
  onClickComplete: null,
};

export default withRouter(ActiveItem);

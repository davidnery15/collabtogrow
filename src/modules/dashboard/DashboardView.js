import React from 'react';
import sessionStore, {
  NEW_SESSION_EVENT,
  UPDATE_META_EVENT,
  SESSION_META_EVENT,
} from '../../shared/SessionStore';
import { EmptyDashboard } from './components/EmptyDashboard';
import { GraphsDashboard } from './components/GraphsDashboard';
import { OnBoarding } from './components/OnBoarding';
import Content from '../../components/Content';
import View from '@cobuildlab/react-flux-state';
import { META_SHOW_ON_BOARDING } from '../../shared';
import { canShowOnBoarding } from '../../shared/utils';
import { Loader } from '@8base/boost';
import { fetchMeta } from './dashboard-actions';

class DashboardView extends View {
  constructor(props) {
    super(props);
    this.state = {
      showOnBoarding: false,
      loading: true,
      meta: [],
    };
  }

  componentDidMount() {
    this.subscribe(sessionStore, UPDATE_META_EVENT, (data) => {
      if (data.name === META_SHOW_ON_BOARDING) {
        this.setState({ showOnBoarding: false });
      }
    });
    this.subscribe(sessionStore, SESSION_META_EVENT, (data) => {
      this.setState({
        loading: false,
        meta: data,
      });
    });
    fetchMeta();
  }

  render() {
    const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
    let content = <EmptyDashboard />;
    if (selectedAlliance !== undefined && selectedAlliance !== null) content = <GraphsDashboard />;
    let { showOnBoarding, loading, meta } = this.state;
    const user = sessionStore.getState(NEW_SESSION_EVENT).user;
    if (selectedAlliance) {
      showOnBoarding = canShowOnBoarding(user, selectedAlliance, meta);
    }

    return (
      <>
        {loading ? (
          <Loader stretch />
        ) : (
          <Content style={{ marginTop: '0px', paddingTop: '0px' }}>
            {showOnBoarding && (
              <>
                <OnBoarding />
                <br />
                <br />
              </>
            )}
            {content}
          </Content>
        )}
      </>
    );
  }
}

export default DashboardView;

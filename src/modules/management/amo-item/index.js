import { requestApprovalForAction } from '../action/action-actions';

/**
 * Return the [type, url, id] of the Item
 * @param {item} item The AMO Item
 */
export const getAMOItemType = (item) => {
  if (item.action !== null) {
    if (item.action.businessCase === null)
      return {
        type: 'Action',
        url: 'action-without-business-case',
        id: item.action.id,
        itemId: item.id,
        name: item.action.name,
        status: item.action.status,
        createdAt: item.action.createdAt,
        revisedDueDate: item.action.revisedDueDate,
        originalDueDate: item.action.originalDueDate,
        requestedBy: item.action.requestedBy
          ? `${item.action.requestedBy.firstName} ${item.action.requestedBy.lastName}`
          : null,
        initiatives: item.action.initiatives,
        approvalFunction: null,
        approvalItems: null,
      };
    return {
      type: 'Action',
      url: 'action',
      id: item.action.id,
      itemId: item.id,
      name: item.action.name,
      status: item.action.status,
      createdAt: item.action.createdAt,
      revisedDueDate: item.action.revisedDueDate,
      originalDueDate: item.action.originalDueDate,
      requestedBy: item.action.requestedBy
        ? `${item.action.requestedBy.firstName} ${item.action.requestedBy.lastName}`
        : null,
      initiatives: item.action.initiatives,
      approvalFunction: requestApprovalForAction,
      approvalItems: item.action.actionApprovalRelation.items,
    };
  }
  if (item.issue !== null)
    return {
      type: 'Issue',
      url: 'issue',
      id: item.issue.id,
      itemId: item.id,
      name: item.issue.name,
      status: item.issue.status,
      createdAt: item.issue.createdAt,
      revisedDueDate: item.issue.revisedDueDate,
      originalDueDate: item.issue.originalDueDate,
      initiatives: item.issue.initiatives,
      approvalFunction: null,
      approvalItems: null,
      createdBy: item.issue.createdBy
        ? `${item.issue.createdBy.firstName} ${item.issue.createdBy.lastName}`
        : null,
    };

  if (item.decision !== null) {
    return {
      type: 'Decision',
      url: 'decision',
      id: item.decision.id,
      itemId: item.id,
      name: item.decision.name,
      status: item.decision.status,
      createdAt: item.decision.createdAt,
      revisedDueDate: '',
      initiatives: item.decision.initiatives,
      approvalItems: null,
      createdBy: item.decision.createdBy
        ? `${item.decision.createdBy.firstName} ${item.decision.createdBy.lastName}`
        : null,
    };
  }
  if (item.risk !== null)
    return {
      type: 'Risk',
      url: 'risk',
      id: item.risk.id,
      itemId: item.id,
      name: item.risk.name,
      status: item.risk.status,
      createdAt: item.risk.createdAt,
      revisedDueDate: item.risk.revisedDueDate,
      originalDueDate: item.risk.originalDueDate,
      initiatives: item.risk.initiatives,
      approvalFunction: null,
      approvalItems: null,
      createdBy: item.risk.createdBy
        ? `${item.risk.createdBy.firstName} ${item.risk.createdBy.lastName}`
        : null,
    };
  return null;
};

export const DECISION_UNIT_TYPE = ['PMO', 'CLIENT REFERENCE', 'OTHER'];

import {
  AMO_ITEM_APPROVED,
  AMO_ITEM_SUBMITTED_FOR_APPROVAL,
  ALLIANCE_APPROVED,
  AMO_ITEM_COMPLETED,
  RISK_COMPLETED,
  ACTION_COMPLETED,
  ACTION_SUBMITTED_FOR_APPROVAL,
  ACTION_APPROVED,
  ISSUE_COMPLETED,
  DECISION_COMPLETED,
} from '../../../shared/status';
import {
  isUserAdminOrSERInAlliance,
  isUserCreatorInAlliance,
} from '../../../shared/alliance-utils';
import { initiativesApprovedValidator } from '../initiative/initiative-validators';
import { isAllianceCompleted } from '../../settings/alliance-management/alliance-permissions';

/**
 * Checks if a User can Edit An AMO Item
 * @param {User} user The user
 * @param {Alliance} alliance The Alliance
 * @param {AMOItem} item The item
 */
export const canEditAMOItem = (user, item, alliance) => {
  if (isAllianceCompleted(alliance)) return false;

  if (isUserAdminOrSERInAlliance(user, alliance)) return true;

  if (
    [
      RISK_COMPLETED,
      ACTION_COMPLETED,
      ACTION_SUBMITTED_FOR_APPROVAL,
      ACTION_APPROVED,
      ISSUE_COMPLETED,
      DECISION_COMPLETED,
    ].includes(item.status)
  )
    return false;

  if (isUserCreatorInAlliance(user, alliance)) return true;

  return false;
};

/**
 * Checks if a User can Delete A AMO Item
 * @param {User} user The user
 * @param {Alliance} alliance The Alliance
 * @param {AMOItem} item The item
 */
export const canDeleteAMOItem = (user, item, alliance) => {
  if (isAllianceCompleted(alliance)) return false;

  if (item.status === AMO_ITEM_APPROVED) return false;

  return isUserAdminOrSERInAlliance(user, alliance) || isUserCreatorInAlliance(user, alliance);
};

/**
 * Checks if a User can Create AMO Items on an Alliance Alliance
 * @param {User} user The user
 * @param {Alliance} alliance The Alliance
 */
export const canCreateAMOItem = (user, alliance) => {
  if (isAllianceCompleted(alliance)) return false;

  return isUserAdminOrSERInAlliance(user, alliance) || isUserCreatorInAlliance(user, alliance);
};

/**
 * Checks if a User can Submit For approval a AMO item
 * Used for Am item list, use the specific item permission on the
 * submitForApproval action
 * @param {User} user The user
 * @param {AMOItem} item The amo item
 * @param {Alliance} alliance The Alliance
 */
export const canSubmitForApprovalAMOItem = (user, item, alliance) => {
  if (isAllianceCompleted(alliance)) return false;

  if (alliance.status !== ALLIANCE_APPROVED) return false;

  if (
    item.status === AMO_ITEM_APPROVED ||
    item.status === AMO_ITEM_COMPLETED ||
    item.status === AMO_ITEM_SUBMITTED_FOR_APPROVAL
  )
    return false;

  try {
    initiativesApprovedValidator(item.initiatives);
  } catch (e) {
    return false;
  }

  return isUserAdminOrSERInAlliance(user, alliance);
};

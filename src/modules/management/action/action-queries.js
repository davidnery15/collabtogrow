import gql from 'graphql-tag';
import { UserFragment } from '../../auth/queries';
import { BusinessCaseFragment } from '../../document-management/business-case/businessCase.queries';
import { CommentFragment } from '../../comment/comment-queries';

const ActionFragment = gql`
  fragment ActionFragment on Action {
    id
    name
    description
    status
    originalDueDate
    assignedDate
    revisedDueDate
    budgetUtilized
    requestedDate
    createdAt
    requestedBy {
      ...UserFragment
    }
    unitType
    unitQuantity
    unitValueDescription
    unitMonetizationFactor
    requestedBy {
      ...UserFragment
    }
    createdBy {
      firstName
      lastName
      id
    }
    assignedTo {
      ...UserFragment
    }
    initiatives {
      items {
        id
        name
        status
        baselineStartDate
        baselineEndDate
        owner {
          ...UserFragment
        }
      }
    }
    nextSteps {
      items {
        id
        dueDate
        description
        status
        assignedTo {
          ...UserFragment
        }
      }
    }
    businessCase {
      ...BusinessCaseFragment
    }
    documents {
      items {
        id
        downloadUrl
        filename
        shareUrl
      }
    }
    itemActionRelation {
      id
    }
    source {
      id
      name
    }

    actionApprovalRelation {
      items {
        dateOfResponse
        status
        approvedBy {
          ...UserFragment
        }
        company {
          id
          name
        }
      }
    }
  }
  ${UserFragment}
  ${BusinessCaseFragment}
`;

/**
 * Create
 */
export const ACTION_CREATE_MUTATION = gql`
  mutation($data: ActionCreateInput!) {
    actionCreate(data: $data) {
      id
    }
  }
`;

/**
 * Retrieve
 */
export const ACTION_DETAIL_QUERY = gql`
  query($id: ID!) {
    action(id: $id) {
      ...ActionFragment
    }
  }
  ${ActionFragment}
`;

/**
 * Update
 */
export const ACTION_UPDATE_MUTATION = gql`
  mutation($data: ActionUpdateInput!) {
    actionUpdate(data: $data) {
      id
      name
      description
      status
    }
  }
`;

export const ActionApprovalFragment = gql`
  fragment ActionApprovalFragment on ActionApproval {
    id
    action {
      id
      name
    }
    company {
      id
      name
    }
    status
    dateOfResponse
  }
`;

export const ACTION_APPROVALS_LIST_QUERY = gql`
  query($data: ActionApprovalFilter) {
    actionApprovalsList(filter: $data) {
      count
      items {
        ...ActionApprovalFragment
      }
    }
  }
  ${ActionApprovalFragment}
`;

/**
 * Update an Action Approval
 */
export const ACTION_APPROVAL_UPDATE_MUTATION = gql`
  mutation($approval: ActionApprovalUpdateInput!) {
    actionApprovalUpdate(data: $approval) {
      id
    }
  }
`;
/**
 * Update an Action Approval and an Action
 */
export const ACTION_APPROVAL_MUTATION = gql`
  mutation($action: ActionUpdateInput!, $approval: ActionApprovalUpdateInput!) {
    actionUpdate(data: $action) {
      id
    }
    actionApprovalUpdate(data: $approval) {
      id
    }
  }
`;
/**
 * Query for the Action Comments
 * @type {*|*}
 */
export const ACTION_COMMENTS_QUERY = gql`
  query($id: ID!) {
    action(id: $id) {
      id
      comments {
        items {
          ...CommentFragment
        }
        count
      }
    }
  }
  ${CommentFragment}
`;

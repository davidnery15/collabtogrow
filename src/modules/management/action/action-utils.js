import { getCompanyOnAlliance } from '../../../shared/alliance-utils';
import { ACTION_APPROVAL_PENDING } from '../../../shared/status';

/**
 * Return true if the User's company's approval or rejection
 * of the Action is pending
 * @param {User} user The user
 * @param {Alliance} alliance The Alliance
 * @param {Action} action The Action
 */

export const isUserPendingActionApproval = (user, alliance, action) => {
  const { actionApprovalRelation } = action;
  const userCompany = getCompanyOnAlliance(user, alliance);

  const userCompanyIsPendingResponse = actionApprovalRelation.items.find(
    (approval) =>
      userCompany.id === approval.company.id && approval.status === ACTION_APPROVAL_PENDING,
  );

  return !!userCompanyIsPendingResponse;
};

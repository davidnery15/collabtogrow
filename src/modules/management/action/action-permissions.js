import {
  isUserAdminOrSERInAlliance,
  isUserCreatorInAlliance,
} from '../../../shared/alliance-utils';
import {
  ACTION_APPROVED,
  ACTION_SUBMITTED_FOR_APPROVAL,
  ACTION_COMPLETED,
  ALLIANCE_APPROVED,
} from '../../../shared/status';
import { initiativesApprovedValidator } from '../initiative/initiative-validators';
import { isUserPendingActionApproval } from './action-utils';
import { isAllianceCompleted } from '../../settings/alliance-management/alliance-permissions';

/**
 * Checks if a User can Edit an Action.
 *
 * @param {User} user - The user.
 * @param action
 * @param {Action} alliance - The Alliance.
 */
export const canEditAction = (user, action, alliance) => {
  if (isAllianceCompleted(alliance)) return false;

  if (action.status === ACTION_APPROVED) return false;
  if (action.status === ACTION_SUBMITTED_FOR_APPROVAL) return false;
  if (action.status === ACTION_COMPLETED) return false;
  if (isUserAdminOrSERInAlliance(user, alliance)) return true;
  if (isUserCreatorInAlliance(user, alliance)) return true;

  return false;
};

/**
 * Checks if a User can Approve an action.
 *
 * @param {object} user - User.
 * @param {object} action - Action.
 * @param {object} alliance - Alliance.
 * @returns {boolean} Validate Options.
 */
export const canApproveAction = (user, action, alliance) => {
  if (isAllianceCompleted(alliance)) return false;

  if (action.status !== ACTION_SUBMITTED_FOR_APPROVAL) return false;

  if (!isUserAdminOrSERInAlliance(user, alliance)) return false;

  return isUserPendingActionApproval(user, alliance, action);
};

/**
 * Checks if a User can Reject an action.
 *
 * @param {object} user - User.
 * @param {object} action - Action.
 * @param {object} alliance - Alliance.
 * @returns {boolean} Validate Options.
 */
export const canRejectAction = (user, action, alliance) => {
  if (isAllianceCompleted(alliance)) return false;

  if (action.status !== ACTION_SUBMITTED_FOR_APPROVAL) return false;

  if (!isUserAdminOrSERInAlliance(user, alliance)) return false;

  return isUserPendingActionApproval(user, alliance, action);
};

/**
 * Checks if a User can Submit For approval an action.
 *
 * @param {object} user - User.
 * @param {object} action - Action.
 * @param {object} alliance - Alliance.
 * @returns {boolean} Validate Options.
 */
export const canSubmitForApprovalAction = (user, action, alliance) => {
  if (isAllianceCompleted(alliance)) return false;

  if (alliance.status !== ALLIANCE_APPROVED) return false;
  if (action.status === ACTION_APPROVED || action.status === ACTION_COMPLETED) return false;

  try {
    initiativesApprovedValidator(action.initiatives);
  } catch (e) {
    return false;
  }

  return isUserAdminOrSERInAlliance(user, alliance);
};
/**
 * Checks if a User can Complete an action.
 *
 * @param {object} user - User.
 * @param {object} action - Action.
 * @param {object} alliance - Alliance.
 * @returns {boolean} Validate Options.
 */
export const canCompletedAction = (user, action, alliance) => {
  if (isAllianceCompleted(alliance)) return false;

  if (action.status === ACTION_COMPLETED) return false;

  if (action.assignedTo && action.assignedTo.id === user.id) return true;

  return isUserAdminOrSERInAlliance(user, alliance);
};
/**
 * Checks if a User can restore an action.
 *
 * @param {object} user - User.
 * @param {object} action - Action.
 * @param {object} alliance - Alliance.
 * @returns {boolean} Validate Options.
 */
export const canRestoreAction = (user, action, alliance) => {
  if (isAllianceCompleted(alliance)) return false;

  if (action.status !== ACTION_COMPLETED) return false;

  if (action.assignedTo && action.assignedTo.id === user.id) return true;

  return isUserAdminOrSERInAlliance(user, alliance);
};

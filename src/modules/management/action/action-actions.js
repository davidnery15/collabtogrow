import {
  ACTION_CREATE_EVENT,
  ACTION_ERROR_EVENT,
  ACTION_UPDATE_EVENT,
  ACTION_REJECT_EVENT,
  ACTION_DETAIL_EVENT,
  ACTION_COMPLETE_EVENT,
  ACTION_SUBMIT_FOR_APPROVAL_EVENT,
  ACTION_RESTORE_EVENT,
} from './action-store';
import Flux from '@cobuildlab/flux-state';
import { IntegrityError } from '../../../shared/errors';
import { validateActionData, actionValidator } from './action-validators';
import sessionStore, { APOLLO_CLIENT, NEW_SESSION_EVENT } from '../../../shared/SessionStore';
import {
  ACTION_CREATE_MUTATION,
  ACTION_DETAIL_QUERY,
  ACTION_UPDATE_MUTATION,
  ACTION_APPROVALS_LIST_QUERY,
  ACTION_APPROVAL_UPDATE_MUTATION,
  ACTION_APPROVAL_MUTATION,
  ACTION_COMMENTS_QUERY,
} from './action-queries';
import { log, error } from '@cobuildlab/pure-logger';
import {
  sanitize8BaseDate,
  sanitize8BaseDocumentsCreate,
  sanitize8BaseDocumentCreate,
  sanitize8BaseReferencesFromObjects,
  sanitize8BaseReferenceFromObject,
  sanitize8BaseReconnectsFromObjects,
  sanitize8BaseDocumentsDeleteAndUpdate,
  sanitize8BaseBigInt,
} from '../../../shared/utils';
import { getActiveAllianceId, getCompanyOnAlliance } from '../../../shared/alliance-utils';
import {
  sanitizeRecommendedSolutions,
  updateBusinessCase,
} from '../../document-management/business-case/businessCases.actions';
import {
  ACTION_APPROVAL_APPROVED,
  ACTION_APPROVAL_REJECTED,
  ACTION_APPROVED,
  ACTION_OPEN,
  ACTION_SUBMITTED_FOR_APPROVAL,
  ACTION_COMPLETED,
  ACTION_IN_PROGRESS,
} from '../../../shared/status';
import {
  COMMENT_CREATE_EVENT,
  COMMENT_ERROR_EVENT,
  COMMENT_REQUEST_EVENT,
  COMMENTS_EVENT,
} from '../../comment/comment-store';
import { ACTION_TYPE } from '../../../shared/item-types';
import { COMMENTS_CREATE_MUTATION } from '../../comment/comment-queries';
import { fetchAlliance } from '../../settings/alliance-management/alliance-actions';
import { businessCaseValidator } from '../../document-management/business-case/business-case-validators';
import {
  canCompletedAction,
  canApproveAction,
  canRejectAction,
  canSubmitForApprovalAction,
  canRestoreAction,
} from './action-permissions';
import {
  sanitizeNextStepsCreate,
  nextStepsToBeCreated,
  updateNextSteps,
  deleteNextSteps,
  completeItemsNextSteps,
  restoreItemsNextSteps,
} from '../../../modules/next-step/next-step-actions.js';
import * as R from 'ramda';
import { RELATED_ITEM_UPDATE_MUTATION } from '../../related-item/related-item-queries';
import { FUNDING_REQUEST_ERROR_EVENT } from '../funding-request/funding-request-store';
/**
 * Notifies a Request for Comments for an Action.
 */
export const openComments = ({ id: actionId }) => {
  Flux.dispatchEvent(COMMENT_REQUEST_EVENT, { type: ACTION_TYPE, id: actionId });
};

/**
 * Creates a New Action.
 *
 * @param {object}action - Action.
 * @param {object}businessCase - BusinessCase.
 * @param {Array}relatedItems - RelatedItems.
 * @param {object}initiatives - Initiatives.
 * @returns {Promise<void>} Return promise.
 */
export const createAction = async (action, businessCase, relatedItems, initiatives) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
  delete action.id;
  delete businessCase.id;
  delete action.createdAt;

  const allianceId = selectedAlliance.id;

  if (!allianceId)
    return Flux.dispatchEvent(
      ACTION_ERROR_EVENT,
      new IntegrityError('Must have an Active Alliance'),
    );

  try {
    validateActionData(action, initiatives, businessCase, true, selectedAlliance);
  } catch (e) {
    error('createAction', e);
    return Flux.dispatchEvent(ACTION_ERROR_EVENT, e);
  }

  action.initiatives = initiatives;
  sanitize8BaseReferencesFromObjects(action, 'initiatives');
  sanitize8BaseReferenceFromObject(action, 'source');
  sanitize8BaseReferenceFromObject(action, 'assignedTo');
  sanitize8BaseReferenceFromObject(action, 'requestedBy');
  sanitize8BaseDocumentsCreate(action, 'documents');
  sanitize8BaseDate(action, 'assignedDate');
  sanitize8BaseDate(action, 'originalDueDate');
  sanitize8BaseDate(action, 'revisedDueDate');
  sanitize8BaseDate(action, 'requestDate');
  sanitize8BaseBigInt(action, 'budgetUtilized');
  sanitize8BaseBigInt(action, 'unitMonetizationFactor');

  sanitizeNextStepsCreate(action);
  sanitizeRecommendedSolutions(businessCase);
  sanitize8BaseDocumentCreate(businessCase, 'document');
  action.businessCase = { create: businessCase };

  action.itemActionRelation = {
    create: {
      alliance: { connect: { id: allianceId } },
      itemsRelated: {
        connect: relatedItems.map((item) => {
          return { id: item.itemId };
        }),
      },
    },
  };

  let result;
  try {
    result = await client.mutate({
      mutation: ACTION_CREATE_MUTATION,
      variables: { data: action },
    });
  } catch (e) {
    error('createAction', e);
    return Flux.dispatchEvent(ACTION_ERROR_EVENT, e);
  }

  return Flux.dispatchEvent(ACTION_CREATE_EVENT, result);
};

/**
 * Creates a New Action
- Create the Action.
 *
 * @param {object}action - Action.
 * @param {object}relatedItems - RelatedItems.
 * @param {object}initiatives - Initiatives.
 * @returns {Promise<void>} Return promise.
 */
export const createActionWithOutBusinessCase = async (action, relatedItems, initiatives) => {
  console.log('createActionWithOutBusinessCase:Debug', action, relatedItems, initiatives);
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
  const allianceId = selectedAlliance.id;

  action.initiatives = initiatives.map((i) => i.id);

  delete action.id;
  delete action.createdAt;
  delete action.owner;
  if (!allianceId)
    return Flux.dispatchEvent(
      ACTION_ERROR_EVENT,
      new IntegrityError('Must have an Active Alliance'),
    );

  try {
    validateActionData(action, initiatives, undefined, false, selectedAlliance);
  } catch (e) {
    error('createAction', e);
    return Flux.dispatchEvent(ACTION_ERROR_EVENT, new IntegrityError(e.message));
  }

  action.initiatives = initiatives;

  sanitize8BaseDocumentsCreate(action, 'documents');
  sanitize8BaseReferenceFromObject(action, 'assignedTo');
  sanitize8BaseReferencesFromObjects(action, 'initiatives');
  sanitize8BaseReferenceFromObject(action, 'requestedBy');
  sanitize8BaseReferenceFromObject(action, 'source');
  sanitize8BaseDate(action, 'assignedDate');
  sanitize8BaseDate(action, 'originalDueDate');
  sanitize8BaseDate(action, 'revisedDueDate');
  sanitize8BaseDate(action, 'requestDate');
  sanitize8BaseBigInt(action, 'budgetUtilized');
  sanitize8BaseBigInt(action, 'unitMonetizationFactor');

  sanitizeNextStepsCreate(action);
  action.status = ACTION_OPEN;
  action.itemActionRelation = {
    create: {
      alliance: { connect: { id: allianceId } },
      itemsRelated: {
        connect: relatedItems.map((item) => {
          return { id: item.itemId };
        }),
      },
    },
  };

  let result;
  try {
    result = await client.mutate({
      mutation: ACTION_CREATE_MUTATION,
      variables: { data: action },
    });
  } catch (e) {
    error('createAction', e);
    return Flux.dispatchEvent(ACTION_ERROR_EVENT, e);
  }

  return Flux.dispatchEvent(ACTION_CREATE_EVENT, result);
};

/**
 * Fetch the Action by id of the Current Alliance.
 *
 * @param {string}id - Id.
 * @returns {Promise<void>} Return promise.
 */
export const fetchAction = async (id) => {
  const client = sessionStore.getState(APOLLO_CLIENT);

  let response;
  try {
    response = await client.query({
      query: ACTION_DETAIL_QUERY,
      fetchPolicy: 'network-only',
      variables: { id },
    });
  } catch (e) {
    error('fetchAction', e);
    throw e;
  }

  return response.data;
};

/**
 * Edit an Action
- Create the Action.
 *
 * @param {object}action - Action.
 * @param {object}businessCaseData - BusinessCaseData.
 * @param {Array}originalRecommendedSolutions - OriginalRecommendedSolutions.
 * @param {Array}originalNextSteps - OriginalNextSteps.
 * @param {Array}relatedItems - RelatedItems.
 * @param {object}initiatives - Initiatives.
 * @param {object}originalDocuments - OriginalDocuments.
 * @returns {Promise<void>} Return promise.
 */
export const updateAction = async (
  action,
  businessCaseData,
  originalRecommendedSolutions,
  originalNextSteps,
  relatedItems,
  initiatives,
  originalDocuments,
) => {
  console.log('updateAction', action, businessCaseData, originalRecommendedSolutions, relatedItems);
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);

  const itemId = action.itemId;

  delete action.createdAt;
  delete action.itemId;
  delete action.createdBy;
  delete businessCaseData.recommendedSolutionsRelation;
  delete businessCaseData.createdAt;
  delete businessCaseData.__typename;
  delete businessCaseData.owner;
  delete businessCaseData.recommendedSolutionsRelation;
  delete businessCaseData.createdAt;
  delete businessCaseData.__typename;
  delete action.itemActionRelation;
  delete action.actionApprovalRelation;
  delete action.__typename;
  delete action.businessCase;

  try {
    validateActionData(action, initiatives, businessCaseData, true, selectedAlliance);
  } catch (e) {
    error('updateAction', e);
    return Flux.dispatchEvent(ACTION_ERROR_EVENT, new IntegrityError(e.message));
  }

  action.initiatives = initiatives;

  sanitize8BaseReconnectsFromObjects(action, 'initiatives');
  sanitize8BaseReferenceFromObject(action, 'source');
  sanitize8BaseReferenceFromObject(action, 'assignedTo');
  sanitize8BaseReferenceFromObject(action, 'requestedBy');
  sanitize8BaseDate(action, 'assignedDate');
  sanitize8BaseDate(action, 'originalDueDate');
  sanitize8BaseDate(action, 'revisedDueDate');
  sanitize8BaseDate(action, 'requestDate');
  sanitize8BaseBigInt(action, 'budgetUtilized');
  sanitize8BaseBigInt(action, 'unitMonetizationFactor');
  sanitize8BaseDocumentsDeleteAndUpdate(action, 'documents', originalDocuments);

  // nextSteps to be created
  const nextSteps = R.clone(action.nextSteps);
  const toBeCreated = nextStepsToBeCreated(nextSteps);
  action.nextSteps = toBeCreated;
  sanitizeNextStepsCreate(action);

  // update and delete nextSteps
  await deleteNextSteps(nextSteps, originalNextSteps);
  await updateNextSteps(nextSteps, originalNextSteps);

  let response;
  try {
    response = await client.mutate({
      mutation: ACTION_UPDATE_MUTATION,
      variables: { data: action },
    });
  } catch (e) {
    error('updateAction', e);
    return Flux.dispatchEvent(ACTION_ERROR_EVENT, e);
  }

  try {
    await updateBusinessCase(businessCaseData, originalRecommendedSolutions);
  } catch (e) {
    console.error('updateAction', e);
    return Flux.dispatchEvent(ACTION_ERROR_EVENT, e);
  }

  const data = {
    id: itemId,
    itemsRelated: {
      reconnect: relatedItems.map((i) => {
        return { id: i.itemId };
      }),
    },
  };
  console.log('actionData:update:relatedItems:', data);
  try {
    await client.mutate({
      mutation: RELATED_ITEM_UPDATE_MUTATION,
      variables: { data },
    });
  } catch (e) {
    console.error('updateAction:updateRelated Items', e, action);
    return Flux.dispatchEvent(FUNDING_REQUEST_ERROR_EVENT, e);
  }

  Flux.dispatchEvent(ACTION_UPDATE_EVENT, response);
  return response.data;
};

/**
 * Edit an Action
- Create the Action.
 *
 * @param {object}action - Action.
 * @param {Array}originalNextSteps - OriginalNextSteps.
 * @param {Array}relatedItems - RelatedItems.
 * @param {object}initiatives - Initiatives.
 * @param {object}originalDocuments - OriginalDocuments.
 * @returns {Promise<void>} Return promise.
 */
export const updateActionWithOutBusinessCase = async (
  action,
  originalNextSteps,
  relatedItems,
  initiatives,
  originalDocuments,
) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);

  const itemId = action.itemId;

  delete action.createdAt;
  delete action.itemId;
  delete action.itemActionRelation;
  delete action.actionApprovalRelation;
  delete action.__typename;
  delete action.requestedBy;
  delete action.createdBy;

  try {
    validateActionData(action, initiatives, undefined, false, selectedAlliance);
  } catch (e) {
    error('updateActionWithOutBusinessCase', e);
    return Flux.dispatchEvent(ACTION_ERROR_EVENT, new IntegrityError(e.message));
  }

  action.initiatives = initiatives;

  sanitize8BaseReferenceFromObject(action, 'assignedTo');
  sanitize8BaseReconnectsFromObjects(action, 'initiatives');
  sanitize8BaseReferenceFromObject(action, 'requestedBy');
  sanitize8BaseReferenceFromObject(action, 'source');
  sanitize8BaseDate(action, 'assignedDate');
  sanitize8BaseDate(action, 'originalDueDate');
  sanitize8BaseDate(action, 'revisedDueDate');
  sanitize8BaseDate(action, 'requestDate');
  sanitize8BaseBigInt(action, 'budgetUtilized');
  sanitize8BaseBigInt(action, 'unitMonetizationFactor');
  sanitize8BaseDocumentsDeleteAndUpdate(action, 'documents', originalDocuments);

  // nextSteps to be created
  const nextSteps = R.clone(action.nextSteps);
  const toBeCreated = nextStepsToBeCreated(nextSteps, originalNextSteps);
  action.nextSteps = toBeCreated;
  sanitizeNextStepsCreate(action);

  // update and delete nextSteps
  await deleteNextSteps(nextSteps, originalNextSteps);
  await updateNextSteps(nextSteps, originalNextSteps);

  let response;
  try {
    response = await client.mutate({
      mutation: ACTION_UPDATE_MUTATION,
      variables: { data: action },
    });
  } catch (e) {
    error('updateActionWithOutBusinessCase', e);
    return Flux.dispatchEvent(ACTION_ERROR_EVENT, e);
  }

  const data = {
    id: itemId,
    itemsRelated: {
      reconnect: relatedItems.map((i) => {
        return { id: i.itemId };
      }),
    },
  };
  console.log('fundingRequestData:update:relatedItems:', data);
  try {
    await client.mutate({
      mutation: RELATED_ITEM_UPDATE_MUTATION,
      variables: { data },
    });
  } catch (e) {
    console.error('updateFundingRequest:updateRelated Items', e, action);
    return Flux.dispatchEvent(FUNDING_REQUEST_ERROR_EVENT, e);
  }

  Flux.dispatchEvent(ACTION_UPDATE_EVENT, response);
  return response.data;
};

/**
 * Submit For Approval an Action.
 *
 * @param {object}amoItem - The Action.
 * @returns {Promise<void>} Return promise.
 */

export const requestApprovalForAction = async (amoItem) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { user, selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
  const actionId = amoItem.action.id;
  const { action } = await fetchAction(actionId);
  const activeAllianceId = selectedAlliance.id;

  if (!canSubmitForApprovalAction(user, action, selectedAlliance))
    return Flux.dispatchEvent(
      ACTION_ERROR_EVENT,
      new IntegrityError('You do not have permission to perform this action'),
    );

  log('requestApprovalForAction', 'toValidate', action);
  // set initiatives & nextSteps for validator
  action.initiatives = action.initiatives.items;
  action.nextSteps = action.nextSteps.items;
  try {
    actionValidator(action, selectedAlliance);
    businessCaseValidator(action.businessCase);
  } catch (e) {
    console.error('requestApprovalForAction', e);
    return Flux.dispatchEvent(ACTION_ERROR_EVENT, e);
  }

  const { alliance } = await fetchAlliance(activeAllianceId);
  log('requestApprovalForAction', 'alliance', alliance);
  const data = {
    id: action.id,
    status: ACTION_SUBMITTED_FOR_APPROVAL,
    actionApprovalRelation: {
      create: [
        { company: { connect: { id: alliance.clientCompany.id } } },
        { company: { connect: { id: alliance.partnerCompany.id } } },
      ],
    },
  };

  let response;
  try {
    response = await client.mutate({
      mutation: ACTION_UPDATE_MUTATION,
      variables: { data },
    });
  } catch (e) {
    console.error('requestApprovalForAction', e, amoItem);
    return Flux.dispatchEvent(ACTION_ERROR_EVENT, e);
  }
  Flux.dispatchEvent(ACTION_SUBMIT_FOR_APPROVAL_EVENT, response.data);
  return response.data;
};

/**
 * Approve Action.
 *
 * @param {object}action - Action.
 * @returns {Promise<void>} Return promise.

 */
export const approveAction = async (action) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { user } = sessionStore.getState(NEW_SESSION_EVENT);
  const actionId = action.id;
  const allianceId = getActiveAllianceId(user);
  const company = getCompanyOnAlliance(user, { id: allianceId });

  if (!canApproveAction(user, action, { id: allianceId }))
    return Flux.dispatchEvent(
      ACTION_ERROR_EVENT,
      new IntegrityError(`Permission Denied. Can't approve this Action`),
    );

  // We fetch the approvals to see what's gonna happen
  const { actionApprovalsList } = await fetchActionApprovals(actionId);
  if (actionApprovalsList.count < 2) {
    return Flux.dispatchEvent(
      ACTION_ERROR_EVENT,
      new IntegrityError('Action must have have at least 2 request for approvals'),
    );
  }

  let approvalsCounter = 0;
  let approvalId = null;
  actionApprovalsList.items.forEach((approval) => {
    if (approval.status === ACTION_APPROVAL_APPROVED) approvalsCounter++;
    if (approval.company.id === company.id) approvalId = approval.id;
  });
  if (approvalId === null) {
    return Flux.dispatchEvent(
      ACTION_ERROR_EVENT,
      new IntegrityError("You can't approve this Action, your Company does not belong here."),
    );
  }

  // Update the status of the Aproval
  const mutation = {
    mutation: ACTION_APPROVAL_UPDATE_MUTATION,
    variables: {
      approval: {
        id: approvalId,
        status: ACTION_APPROVAL_APPROVED,
        dateOfResponse: new Date(),
        approvedBy: { connect: { id: user.id } },
      },
    },
  };
  // If there are more than 0 approvals: Update the status of the action
  if (approvalsCounter > 0) {
    mutation.mutation = ACTION_APPROVAL_MUTATION;
    mutation.variables.action = {
      id: actionId,
      status: ACTION_APPROVED,
    };
  }

  let response;
  try {
    response = await client.mutate(mutation);
  } catch (e) {
    error('approveFundingRequest', e);
    return Flux.dispatchEvent(ACTION_ERROR_EVENT, e);
  }
  log('approveFundingRequest', response);
  Flux.dispatchEvent(ACTION_UPDATE_EVENT, response.data);
  return response.data;
};

/**
 * Reject Action.
 *
 * @param {object}action - Action.
 * @returns {Promise<void>} Return promise.
 */

export const rejectAction = async (action) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { user } = sessionStore.getState(NEW_SESSION_EVENT);
  const actionId = action.id;
  const allianceId = getActiveAllianceId(user);
  const company = getCompanyOnAlliance(user, { id: allianceId });

  if (!canRejectAction(user, action, { id: allianceId }))
    return Flux.dispatchEvent(
      ACTION_ERROR_EVENT,
      new IntegrityError(`Permission Denied. Can't Reject this Action`),
    );

  // We fetch the approvals to see what's gonna happen
  const { actionApprovalsList } = await fetchActionApprovals(actionId);
  if (actionApprovalsList.count < 2) {
    return Flux.dispatchEvent(
      ACTION_ERROR_EVENT,
      new IntegrityError('Action must have have at least 2 request for approvals'),
    );
  }

  let approvalId = null;
  let alliedCompanyApprovalId = null;
  actionApprovalsList.items.forEach((approval) => {
    if (approval.company.id === company.id) {
      approvalId = approval.id;
    } else {
      alliedCompanyApprovalId = approval.id;
    }
  });
  if (approvalId === null) {
    return Flux.dispatchEvent(
      ACTION_ERROR_EVENT,
      new IntegrityError("You can't reject this Action, your Company does not belong here."),
    );
  }

  // Update the status of the initiativeApproval
  const userCompanyMutation = {
    mutation: ACTION_APPROVAL_MUTATION,
    variables: {
      approval: {
        id: approvalId,
        status: ACTION_APPROVAL_REJECTED,
        dateOfResponse: new Date(),
        approvedBy: { connect: { id: user.id } },
      },
      action: {
        id: actionId,
        status: ACTION_APPROVAL_REJECTED,
      },
    },
  };

  const alliedCompanyMutation = {
    mutation: ACTION_APPROVAL_UPDATE_MUTATION,
    variables: {
      approval: {
        id: alliedCompanyApprovalId,
        status: ACTION_APPROVAL_REJECTED,
        dateOfResponse: new Date(),
      },
    },
  };

  let response;
  try {
    response = await client.mutate(userCompanyMutation);
    await client.mutate(alliedCompanyMutation);
  } catch (e) {
    error('rejectAction', e);
    return Flux.dispatchEvent(ACTION_ERROR_EVENT, e);
  }
  log('rejectAction', response);
  Flux.dispatchEvent(ACTION_REJECT_EVENT, response.data);
  return response.data;
};

/**
 * Fetches the Actions approvals for the User.
 *
 * @param {string} actionId - Action Id.
 * @returns {Promise<void>} Return promise.
 */
export const fetchActionApprovals = async (actionId) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const data = { action: { id: { equals: actionId } } };

  let response;
  try {
    response = await client.query({
      query: ACTION_APPROVALS_LIST_QUERY,
      variables: { data },
      fetchPolicy: 'network-only',
    });
  } catch (e) {
    error('fetchActionApprovals', e);
    return Flux.dispatchEvent(ACTION_ERROR_EVENT, e);
  }
  log('fetchActionApprovals', response);
  return response.data;
};

/**
 * Fetches the Action Comments.
 *
 * @param {string}actionId - Action Id.
 * @returns {Promise<void>} Return promise.
 */
export const fetchActionComments = async (actionId) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const data = { id: actionId };
  const { user } = sessionStore.getState(NEW_SESSION_EVENT);

  let response;
  try {
    response = await client.query({
      query: ACTION_COMMENTS_QUERY,
      variables: data,
      fetchPolicy: 'network-only',
    });
  } catch (e) {
    error('fetchActionComments', e);
    return Flux.dispatchEvent(COMMENT_ERROR_EVENT, e);
  }
  log('fetchActionComments', response);
  response.data.action.comments.userId = user.id;
  Flux.dispatchEvent(COMMENTS_EVENT, response.data.action.comments);
  return response.data.action.comments;
};

/**
 * Create a comment on an Action.
 *
 * @param {string}actionId - Action Id.
 * @param {object}comment - Comment.
 * @returns {Promise<void>} Return promise.
 */
export const createActionComment = async (actionId, comment) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const data = {
    comment,
    actionCommentsRelation: { connect: { id: actionId } },
  };

  let response;
  try {
    response = await client.mutate({
      mutation: COMMENTS_CREATE_MUTATION,
      variables: { data },
    });
  } catch (e) {
    error('createActionComment', e);
    return Flux.dispatchEvent(COMMENT_ERROR_EVENT, e);
  }
  log('createActionComment', response);
  Flux.dispatchEvent(COMMENT_CREATE_EVENT, response.data);
  return response.data;
};

/**
 * Restore a Action.
 *
 * @param {object}actionData - Action.
 * @returns {Promise<void>} Return promise.
 */
export const restoreAction = async (actionData) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { selectedAlliance, user } = sessionStore.getState(NEW_SESSION_EVENT);
  const { action } = await fetchAction(actionData.id);

  if (!canRestoreAction(user, action, selectedAlliance))
    return Flux.dispatchEvent(
      ACTION_ERROR_EVENT,
      new IntegrityError('You do not have permission to perform this action'),
    );

  let response;
  try {
    response = await client.mutate({
      mutation: ACTION_UPDATE_MUTATION,
      variables: {
        data: {
          id: action.id,
          status: ACTION_IN_PROGRESS,
        },
      },
    });
  } catch (e) {
    error('restoredAction', e);
    return Flux.dispatchEvent(ACTION_ERROR_EVENT, e);
  }

  Flux.dispatchEvent(ACTION_RESTORE_EVENT, response);
  return response.data;
};

/**
 * Restore a Action with out business case.
 *
 * @param {object}actionData - Action.
 * @returns {Promise<void|*>} Return promise.
 */
export const restoreActionWithOutBusinessCase = async (actionData) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { selectedAlliance, user } = sessionStore.getState(NEW_SESSION_EVENT);
  const { action } = await fetchAction(actionData.id);

  if (!canRestoreAction(user, action, selectedAlliance))
    return Flux.dispatchEvent(
      ACTION_ERROR_EVENT,
      new IntegrityError('You do not have permission to perform this action'),
    );

  let response;
  try {
    response = await client.mutate({
      mutation: ACTION_UPDATE_MUTATION,
      variables: {
        data: {
          id: action.id,
          status: ACTION_OPEN,
        },
      },
    });
  } catch (e) {
    error('restoredAction', e);
    return Flux.dispatchEvent(ACTION_ERROR_EVENT, e);
  }


  Flux.dispatchEvent(ACTION_RESTORE_EVENT, response);
  return response.data;
};

/**
 * Close a Action.
 *
 * @param {object}actionData - Action.
 * @returns {Promise<void>} Return promise.
 */
export const completedAction = async (actionData) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { selectedAlliance, user } = sessionStore.getState(NEW_SESSION_EVENT);
  const { action } = await fetchAction(actionData.id);

  if (!canCompletedAction(user, action, selectedAlliance))
    return Flux.dispatchEvent(
      ACTION_ERROR_EVENT,
      new IntegrityError('You do not have permission to perform this action'),
    );

  let response;
  try {
    response = await client.mutate({
      mutation: ACTION_UPDATE_MUTATION,
      variables: {
        data: {
          id: action.id,
          status: ACTION_COMPLETED,
        },
      },
    });
  } catch (e) {
    error('completedAction', e);
    return Flux.dispatchEvent(ACTION_ERROR_EVENT, e);
  }

  try {
    await completeItemsNextSteps(action);
  } catch (e) {
    log('completeItemsNextStepsError', e);
  }

  Flux.dispatchEvent(ACTION_COMPLETE_EVENT, response);
  return response.data;
};

/**
 * This function call action data and dispatch event.
 *
 * @param {string}id - Id.
 * @returns {Promise<void>} Return promise.
 */
export const fetchActionDetail = async (id) => {
  let response;
  try {
    response = await fetchAction(id);
  } catch (e) {
    error('fetchActionDetail', e);
    return Flux.dispatchEvent(ACTION_ERROR_EVENT, e);
  }

  Flux.dispatchEvent(ACTION_DETAIL_EVENT, response);
  console.log('fetchActionDetail', response);
  return response;
};

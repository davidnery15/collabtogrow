import React from 'react';
import PropTypes from 'prop-types';
import { Loader, Card, Heading, Switch, Grid } from '@8base/boost';
import { CreateViewCardBody } from '../../../components/new-ui/card/CreateViewCardBody';
import ActionForm from './components/ActionForm';
import {
  onChangeBusinessCaseDataMixin,
  onChangeDataMixin,
  onErrorMixin,
} from '../../../shared/mixins';
import * as R from 'ramda';
import ActionModel, { ACTION_DOCUMENTS } from './action-model';
import { withRouter } from 'react-router-dom';
import actionStore, { ACTION_CREATE_EVENT, ACTION_ERROR_EVENT } from './action-store';
import * as toast from 'components/toast/Toast';
import View from '@cobuildlab/react-flux-state';
import { createAction } from './action-actions';
import allianceStore, {
  ALLIANCE_LIST_MEMBERS_EVENT,
} from '../../settings/alliance-management/alliance-store';
import { fetchCurrentAllianceMembersAction } from '../../settings/alliance-management/alliance-actions';
import { fetchInitiativeList } from '../initiative/initiative-actions';
import initiativeStore, { INITIATIVE_LIST_EVENT } from '../initiative/initiative-store';
import BusinessCaseModel from '../../document-management/business-case/BusinessCase.model';
import sessionStore, { NEW_SESSION_EVENT } from '../../../shared/SessionStore';
import BusinessCaseForm from '../../document-management/business-case/components/BusinessCaseForm';
import SubHeader from '../../../components/SubHeader';
import BusinessCaseDetailTable from '../../document-management/business-case/components/BusinessCaseDetailTable';
import ActionDetailTable from './components/ActionDetailTable';
import { getCurrencyOnSession } from '../../../shared/alliance-utils';
import { ActionButton } from '../../../components/buttons/ActionButton';
import { ActionButtonClose } from '../../../components/buttons/ActionButtonClose';
import { TransparentButton } from '../../../components/buttons/TransparentButton';
import { FormSteps } from '../../../components/dots/FormSteps';
import { actionValidator } from './action-validators';
import { businessCaseValidator } from '../../document-management/business-case/business-case-validators';
import { saveFormToSessionStorage } from 'shared/utils';
import { initiativesItemValidator } from '../initiative/initiative-validators';
import RelatedItemForm from '../../related-item/components/RelatedItemForm';
import { RelatedItemsDetailTable } from '../../related-item/components/RelatedItemsDetailTable';
import { InitiativeListTable } from '../initiative/components/InitiativeListTable';
import { CardFooter } from '../../../components/new-ui/card/CardFooter';
import { LeftProgressSection } from '../../../components/new-ui/LeftProgressSection';
import { SCREENS_ACTIONS } from '../screenView';
import { BoxCard } from '../../../components/new-ui/div/BoxCard';

export const ACTION_DATA_STORE = 'actionCreateView';

/**
 * Create Action
 */
class ActionCreateView extends View {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        actionData: R.clone(ActionModel),
        businessCaseData: R.clone(BusinessCaseModel),
        relatedItems: [],
        initiatives: [],
      },
      clientCompany: null,
      partnerCompany: null,
      initiativesList: [],
      step: 0,
      loading: true,
    };
    this.onChangeData = onChangeDataMixin.bind(this);
    this.onError = onErrorMixin.bind(this);
    this.onChangeBusinessCaseData = onChangeBusinessCaseDataMixin.bind(this);
  }

  onChangeActionData = (name, value) => {
    const { data } = this.state;
    data.actionData[name] = value;
    if (name === 'originalDueDate') {
      data.actionData.revisedDueDate = value;
    }
    this.setState({ data });
    const model = R.clone(ActionModel);
    saveFormToSessionStorage(ACTION_DATA_STORE, data.actionData, model, ['documents']);
  };

  componentDidMount = () => {
    const actionData = JSON.parse(sessionStorage.getItem(ACTION_DATA_STORE));
    this.subscribe(actionStore, ACTION_ERROR_EVENT, this.onError);
    this.subscribe(allianceStore, ALLIANCE_LIST_MEMBERS_EVENT, (state) => {
      this.setState({
        clientCompany: state.clientCompany,
        partnerCompany: state.partnerCompany,
      });
    });
    this.subscribe(actionStore, ACTION_CREATE_EVENT, (state) => {
      sessionStorage.removeItem(ACTION_DATA_STORE);
      toast.success('Action Successfully Created');
      this.props.history.push(`/management/amo-item`);
    });
    this.subscribe(initiativeStore, INITIATIVE_LIST_EVENT, (state) => {
      const initiativesList = state.initiativesList.items;
      this.setState({
        loading: false,
        initiativesList,
      });
    });

    // set actionData from sessionStorage
    if (actionData) {
      const { data } = this.state;
      data.actionData = actionData;
      this.setState({ data });
    }

    fetchInitiativeList('', 1, 1000);
    fetchCurrentAllianceMembersAction();
  };

  componentWillUnmount() {
    super.componentWillUnmount();
    localStorage.removeItem(ACTION_DOCUMENTS);
  }

  onSubmit = () => {
    this.setState({ loading: true }, () => {
      const actionData = R.clone(this.state.data.actionData);
      const businessCaseData = R.clone(this.state.data.businessCaseData);
      const relatedItems = R.clone(this.state.data.relatedItems);
      const initiatives = R.clone(this.state.data.initiatives);
      createAction(actionData, businessCaseData, relatedItems, initiatives);
    });
  };

  onActionStepChange = (nextStep) => {
    const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
    const actionData = R.clone(this.state.data.actionData);
    try {
      actionValidator(actionData, selectedAlliance);
    } catch (e) {
      return this.onError(e);
    }
    this.onScreen(nextStep);
  };

  onBusinessCaseStepChange = (nextStep) => {
    const businessCaseData = R.clone(this.state.data.businessCaseData);
    try {
      businessCaseValidator(businessCaseData);
    } catch (e) {
      return this.onError(e);
    }
    this.onScreen(nextStep);
  };

  onRelatedItemsStepChange = (step) => {
    const initiatives = R.clone(this.state.data.initiatives);
    try {
      initiativesItemValidator(initiatives);
    } catch (e) {
      return this.onError(e);
    }
    this.onScreen(step);
  };

  onScreen = (step) => {
    this.setState({ step });
  };

  render() {
    const { initiativesList, clientCompany, partnerCompany, step, loading } = this.state;
    const { actionData, businessCaseData, relatedItems, initiatives } = this.state.data;
    const { user, selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
    const companyId = user.companyUserRelation.items[0].company.id;
    const { history } = this.props;
    const currency = getCurrencyOnSession();
    let content = <Loader stretch />;
    let footer = <></>;

    if (!loading && step === 0) {
      content = (
        <ActionForm
          data={actionData}
          onChange={this.onChangeActionData}
          clientCompany={clientCompany}
          partnerCompany={partnerCompany}
          myCompanyId={companyId}
          currency={currency}
          user={user}
          selectedAlliance={selectedAlliance}
        />
      );
      footer = (
        <CardFooter>
          <ActionButton onClick={() => this.onActionStepChange(1)} text={'Next'} />
        </CardFooter>
      );
    }

    if (!loading && step === 1) {
      content = (
        <BusinessCaseForm
          data={businessCaseData}
          onChange={this.onChangeBusinessCaseData}
          currency={currency}
        />
      );
      footer = (
        <CardFooter>
          <ActionButton onClick={() => this.onBusinessCaseStepChange(2)} text={'Next'} />
          <TransparentButton onClick={() => this.onScreen(0)} text={'Previous'} />
        </CardFooter>
      );
    }

    if (!loading && step === 2) {
      content = (
        <RelatedItemForm
          relatedItems={relatedItems}
          initiatives={initiativesList}
          onChange={(key, value) => {
            this.onChangeData(key, value);
          }}
          selectedInitiatives={initiatives}
        />
      );

      footer = (
        <CardFooter>
          <ActionButton onClick={() => this.onRelatedItemsStepChange(3)} text={'Next'} />
          <TransparentButton onClick={() => this.onScreen(1)} text={'Previous'} />
        </CardFooter>
      );
    }
    if (!loading && step === 3) {
      const actionDetails = R.clone(actionData);

      actionDetails.documents = { items: actionData.documents };
      actionDetails.nextSteps = { items: actionData.nextSteps };

      content = (
        <>
          <ActionDetailTable data={actionDetails} currency={currency} />
          <SubHeader text="Business Case" status={actionData.status} />
          <BusinessCaseDetailTable data={businessCaseData} currency={currency} />
          <InitiativeListTable initiatives={initiatives} />
          <RelatedItemsDetailTable relatedItems={relatedItems} />
        </>
      );
      footer = (
        <CardFooter>
          <ActionButton onClick={() => this.onSubmit()} text={'Create Action'} />
          <TransparentButton onClick={() => this.onScreen(2)} text={'Previous'} />
        </CardFooter>
      );
    }

    return (
      <React.Fragment>
        <Card.Header>
          <Grid.Layout
            columns="200px auto 200px"
            areas={[['left', 'center', 'right']]}
            style={{ width: '100%' }}>
            <Grid.Box area="left">
              <Heading type="h4" text="Create Action" />
            </Grid.Box>
            <Grid.Box area="center">
              <FormSteps totalSteps={4} step={step} />
            </Grid.Box>
            <Grid.Box area="right">
              <Switch
                label={'Business Case'}
                value
                onChange={() => {
                  history.push(`/management/action-without-business-case/create`);
                }}
              />
            </Grid.Box>
          </Grid.Layout>
          <ActionButtonClose onClick={history.goBack} />
        </Card.Header>
        <CreateViewCardBody>
          <Grid.Layout columns="30% 70%" areas={[['left', 'right']]} style={{ width: '100%' }}>
            <Grid.Box area="left">
              <LeftProgressSection sections={SCREENS_ACTIONS} currentScreen={step} />
            </Grid.Box>
            <BoxCard>
              <Grid.Box area="right">{content}</Grid.Box>
            </BoxCard>
          </Grid.Layout>
        </CreateViewCardBody>
        {footer}
      </React.Fragment>
    );
  }
}

ActionCreateView.propTypes = {
  history: PropTypes.object.isRequired,
};

export default withRouter(ActionCreateView);

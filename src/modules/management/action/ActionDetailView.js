import React from 'react';
import View from '@cobuildlab/react-flux-state';
import { Card, Heading, Loader, Row } from '@8base/boost';
import ActionModel from './action-model';
import { onErrorMixin } from '../../../shared/mixins';
import * as R from 'ramda';
import sessionStore, { NEW_SESSION_EVENT } from '../../../shared/SessionStore';
import ActionDetailTable from './components/ActionDetailTable';
import {
  approveAction,
  rejectAction,
  openComments,
  completedAction,
  requestApprovalForAction,
  fetchActionDetail,
  restoreAction,
} from './action-actions';
import actionStore, {
  ACTION_DETAIL_EVENT,
  ACTION_ERROR_EVENT,
  ACTION_UPDATE_EVENT,
  ACTION_REJECT_EVENT,
  ACTION_COMPLETE_EVENT,
  ACTION_SUBMIT_FOR_APPROVAL_EVENT,
  ACTION_RESTORE_EVENT,
} from './action-store';
import * as toast from 'components/toast/Toast';
import PropTypes from 'prop-types';
import withAlliance from '../../../components/hoc/withAlliance';
import { withRouter } from 'react-router-dom';
import BusinessCaseModel from '../../document-management/business-case/BusinessCase.model';
import BusinessCaseDetailTable from '../../document-management/business-case/components/BusinessCaseDetailTable';
import {
  canApproveAction,
  canRejectAction,
  canCompletedAction,
  canRestoreAction,
} from './action-permissions';
import YesNoDialog from '../../../components/dialogs/YesNoDialog';
import { DangerButton } from '../../../components/buttons/DangerButton';
import {
  fetchRelatedItems,
  fetchRelatedItemsByItemId,
} from '../../related-item/related-item-actions';
import { getCurrencyOnSession } from '../../../shared/alliance-utils';
import { DetailViewCardBody } from '../../../components/card/DetailViewCardBody';
import { ActionButton } from '../../../components/buttons/ActionButton';
import AppovalDetailTable from '../../../components/tables/approvalTable/ApprovalDetailTable';
import { RelatedItemsDetailTable } from '../../related-item/components/RelatedItemsDetailTable';
import relatedItemStore, {
  RELATED_ITEMS_BY_ITEM_EVENT,
  RELATED_ITEMS_EVENT,
} from '../../related-item/related-item-store';
import { getItemByType } from '../../../shared/items-util';
import { TopButtons } from '../../../components/buttons/TopButtons';
import { RelatedItemsByItemDetailTable } from '../../related-item/components/RelatedItemsByItemDetailTable';
import { InitiativeListTable } from '../initiative/components/InitiativeListTable';
import { canSubmitForApprovalAMOItem } from '../amo-item/amo-item-permissions';
import { CardFooter } from '../../../components/new-ui/card/CardFooter';

/**
 * Action Detail View.
 *
 * @param action
 */
class ActionDetailView extends View {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        actionData: R.clone(ActionModel),
        businessCaseData: R.clone(BusinessCaseModel),
        relatedItems: [],
        initiatives: [],
        approvalData: [],
        relatedItemsByItem: [],
      },
      loading: true,
      approvalModalIsOpen: false,
      rejectModalIsOpen: false,
      completedModalIsOpen: false,
      submitForApprovalModelIsOpen: false,
      restoreModalIsOpen: false,
    };
    this.user = sessionStore.getState(NEW_SESSION_EVENT).user;
    this.onError = onErrorMixin.bind(this);
    this.selectedAlliance = sessionStore.getState(NEW_SESSION_EVENT).selectedAlliance;
  }

  componentDidMount = () => {
    const { match } = this.props;
    if (!match.params.id) return toast.error('Action ID missing');

    this.subscribe(actionStore, ACTION_ERROR_EVENT, this.onError);

    this.subscribe(actionStore, ACTION_DETAIL_EVENT, (state) => {
      const { action } = state;
      const { businessCase: businessCaseData } = action;
      const { actionApprovalRelation } = action;
      const { data } = this.state;
      businessCaseData.recommendedSolutions = businessCaseData.recommendedSolutionsRelation.items.map(
        (solution) => {
          delete solution.__typename;
          return solution;
        },
      );

      this.originalRecommendedSolutions = businessCaseData.recommendedSolutions.concat();

      const actionData = action;
      const approvalData = actionApprovalRelation.items.slice(-2);

      actionData.itemId = action.itemActionRelation.id;

      data.actionData = actionData;
      data.businessCaseData = businessCaseData;
      data.approvalData = approvalData;
      data.initiatives = actionData.initiatives.items;

      this.setState(
        {
          data,
        },
        () => fetchRelatedItems(actionData.itemId),
        fetchRelatedItemsByItemId(actionData.itemId),
      );
    });

    this.subscribe(actionStore, ACTION_UPDATE_EVENT, () => {
      fetchActionDetail(match.params.id);
      toast.success('Action Successfully Approved');
    });

    this.subscribe(actionStore, ACTION_SUBMIT_FOR_APPROVAL_EVENT, (action) => {
      fetchActionDetail(match.params.id);
      toast.success('Action Submitted For Approval!');
    });

    this.subscribe(actionStore, ACTION_REJECT_EVENT, () => {
      fetchActionDetail(match.params.id);
      toast.success('Action Successfully Rejected');
    });

    this.subscribe(relatedItemStore, RELATED_ITEMS_EVENT, (state) => {
      const items = state.item.itemsRelated.items.map((item) => getItemByType(item));
      const { data } = this.state;
      data.relatedItems = items;
      this.setState({
        data,
      });
    });

    this.subscribe(actionStore, ACTION_COMPLETE_EVENT, (state) => {
      fetchActionDetail(match.params.id);
      toast.success('Action Successfully Completed');
    });

    this.subscribe(actionStore, ACTION_RESTORE_EVENT, (state) => {
      fetchActionDetail(match.params.id);
      toast.success('Action Successfully Restored');
    });

    this.subscribe(relatedItemStore, RELATED_ITEMS_BY_ITEM_EVENT, (state) => {
      const {
        itemsList: { items: itemsRelated },
      } = state;
      const relatedItemsByItem = itemsRelated.map((item) => getItemByType(item));
      const { data } = this.state;

      data.relatedItemsByItem = relatedItemsByItem;
      this.setState({ data, loading: false });

      console.log('relatedItemsByItem', relatedItemsByItem);
    });

    fetchActionDetail(match.params.id);
  };

  approve = () => {
    this.setState({
      approvalModalIsOpen: true,
    });
  };

  onYes = () => {
    this.setState(
      {
        approvalModalIsOpen: false,
        loading: true,
      },
      () => {
        approveAction({ ...this.state.data.actionData });
      },
    );
  };

  onClose = () => {
    this.setState({
      approvalModalIsOpen: false,
    });
  };

  reject = () => {
    this.setState({
      rejectModalIsOpen: true,
    });
  };

  onYesReject = () => {
    this.setState(
      {
        rejectModalIsOpen: false,
        loading: true,
      },
      () => {
        rejectAction({ ...this.state.data.actionData });
      },
    );
  };

  onCloseReject = () => {
    this.setState({
      rejectModalIsOpen: false,
    });
  };

  onCloseSubmitForApprovalModal = () => {
    this.setState({
      submitForApprovalModalIsOpen: false,
    });
  };
  onSelectSubmitForApprovalModal = (action) => {
    this.setState({
      action,
      submitForApprovalModalIsOpen: true,
    });
  };

  onYesSubmitForApprovalModal = () => {
    this.setState(
      {
        submitForApprovalModalIsOpen: false,
        loading: true,
      },
      () => {
        const action = R.clone(this.state.data.actionData);
        requestApprovalForAction({ action });
      },
    );
  };

  completedModal = () => {
    this.setState({
      completedModalIsOpen: true,
    });
  };

  restoreModal = () => {
    this.setState({
      restoreModalIsOpen: true,
    });
  };

  completedModalClose = () => {
    this.setState({
      completedModalIsOpen: false,
    });
  };

  restoredModalClose = () => {
    this.setState({
      restoreModalIsOpen: false,
    });
  };

  onYesModal = () => {
    this.setState(
      {
        completedModalIsOpen: false,
        loading: true,
      },
      () => {
        const actionData = R.clone(this.state.data.actionData);
        completedAction(actionData);
      },
    );
  };

  onYesRestore = () => {
    this.setState(
      {
        restoreModalIsOpen: false,
        loading: true,
      },
      () => {
        const actionData = R.clone(this.state.data.actionData);
        restoreAction(actionData);
      },
    );
  };

  render() {
    const {
      loading,
      approvalModalIsOpen,
      rejectModalIsOpen,
      completedModalIsOpen,
      submitForApprovalModalIsOpen,
      restoreModalIsOpen,
    } = this.state;

    const {
      actionData,
      businessCaseData,
      approvalData,
      relatedItems,
      initiatives,
      relatedItemsByItem,
    } = this.state.data;
    const currency = getCurrencyOnSession();
    const { history } = this.props;
    let content = <Loader stretch />;
    let buttonsBottom = '';
    let buttonsTop = '';
    const alliance = this.selectedAlliance;

    if (!loading) {
      content = (
        <>
          <ActionDetailTable
            currency={currency}
            data={actionData}
            onClickEdit={() => history.push(`/management/action/edit/${actionData.id}`)}
          />
          <BusinessCaseDetailTable data={businessCaseData} currency={currency} />
          <AppovalDetailTable data={approvalData} />
          <InitiativeListTable initiatives={initiatives} />
          <RelatedItemsDetailTable relatedItems={relatedItems} />
          <RelatedItemsByItemDetailTable relatedItemsByItem={relatedItemsByItem} />
        </>
      );

      buttonsTop = (
        <>
          <Heading type="h4" text={actionData.name} />

          <TopButtons
            onClickClosed={history.goBack}
            onClickCollaborated={() => openComments(actionData)}
          />
        </>
      );

      buttonsBottom = (
        <Row justifyContent="end">
          {canSubmitForApprovalAMOItem(this.user, actionData, this.selectedAlliance) ? (
            <ActionButton
              text="Submit For Approval"
              fontAwesomeIcon="check"
              onClick={() => {
                this.onSelectSubmitForApprovalModal();
              }}
            />
          ) : null}
          <DangerButton
            text={'Reject'}
            fontAwesomeIcon={'times'}
            onClick={this.reject}
            disabled={
              !canRejectAction(this.user, actionData, {
                id: this.props.allianceId,
              })
            }
          />
          <ActionButton
            fontAwesomeIcon={'check'}
            onClick={this.approve}
            text={'Approve'}
            disabled={
              !canApproveAction(this.user, actionData, {
                id: this.props.allianceId,
              })
            }
          />
          {canCompletedAction(this.user, actionData, alliance) ? (
            <ActionButton
              text="Mark Completed"
              fontAwesomeIcon="clipboard-list"
              onClick={() => {
                this.completedModal();
              }}
            />
          ) : null}
          {canRestoreAction(this.user, actionData, alliance) ? (
            <ActionButton
              text="Restore"
              fontAwesomeIcon="clipboard-list"
              onClick={() => {
                this.restoreModal();
              }}
            />
          ) : null}
        </Row>
      );
    }

    return (
      <React.Fragment>
        <Card.Header>{buttonsTop}</Card.Header>
        <DetailViewCardBody>{content}</DetailViewCardBody>
        <CardFooter>{buttonsBottom}</CardFooter>
        <YesNoDialog
          title={'Approve Action'}
          onYes={this.onYes}
          onClose={this.onClose}
          onNo={this.onClose}
          text={'Are you sure you want to Approve the Action?'}
          isOpen={approvalModalIsOpen}
        />
        <YesNoDialog
          title={'Reject Action'}
          onYes={this.onYesReject}
          onClose={this.onCloseReject}
          onNo={this.onCloseReject}
          text={'Are you sure you want to Reject the Action?'}
          isOpen={rejectModalIsOpen}
        />
        <YesNoDialog
          title={'Complete Action'}
          onYes={this.onYesModal}
          onClose={this.completedModalClose}
          onNo={this.completedModalClose}
          text={'Are you sure you want to Mark the Action as Completed?'}
          isOpen={completedModalIsOpen}
        />

        <YesNoDialog
          title={'Restore Action'}
          onYes={this.onYesRestore}
          onClose={this.restoredModalClose}
          onNo={this.restoredModalClose}
          text={'Are you sure you want to Mark the Action as Restored?'}
          isOpen={restoreModalIsOpen}
        />
        <YesNoDialog
          title={'Submit For Approval'}
          onYes={this.onYesSubmitForApprovalModal}
          onClose={this.onCloseSubmitForApprovalModal}
          onNo={this.onCloseSubmitForApprovalModal}
          text={'Are you sure you want to Submit this Action For Approval?'}
          isOpen={submitForApprovalModalIsOpen}
        />
      </React.Fragment>
    );
  }
}

ActionDetailView.propTypes = {
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default withRouter(withAlliance(ActionDetailView));

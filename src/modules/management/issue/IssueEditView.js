import React from 'react';
import { Card, Heading, Loader, Grid } from '@8base/boost';
import { CreateViewCardBody } from '../../../components/new-ui/card/CreateViewCardBody';
import fundingRequestStore, {
  ISSUE_DETAIL_EVENT,
  ISSUE_ERROR_EVENT,
  ISSUE_UPDATE_EVENT,
} from './issue-store';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import * as toast from 'components/toast/Toast';
import * as R from 'ramda';
import View from '@cobuildlab/react-flux-state';
import { onChangeDataMixin, onErrorMixin } from '../../../shared/mixins';
import { IssueModel, ISSUE_DOCUMENTS } from './issue-model';
import IssueForm from './components/IssueForm';
import { openComments, updateIssue } from './issue-actions';
import { fetchIssueDetail } from './issue-actions';
import initiativeStore, { INITIATIVE_LIST_EVENT } from '../initiative/initiative-store';
import { fetchInitiativeList } from '../initiative/initiative-actions';
import allianceStore, {
  ALLIANCE_LIST_MEMBERS_EVENT,
} from '../../settings/alliance-management/alliance-store';
import { fetchCurrentAllianceMembersAction } from '../../settings/alliance-management/alliance-actions';
import { getCurrencyOnSession } from '../../../shared/alliance-utils';
import { ActionButton } from '../../../components/buttons/ActionButton';
import { fetchRelatedItems } from '../../related-item/related-item-actions';
import { sanitizeNextStepsToEdit } from 'modules/next-step/next-step-actions';
import sessionStore, { NEW_SESSION_EVENT } from '../../../shared/SessionStore';
import relatedItemStore, { RELATED_ITEMS_EVENT } from '../../related-item/related-item-store';
import { getItemByType } from '../../../shared/items-util';
import { issueValidator } from './issue-validators';
import RelatedItemForm from '../../related-item/components/RelatedItemForm';
import { TransparentButton } from '../../../components/buttons/TransparentButton';
import IssueDetailTable from './components/IssueDetailTable';
import { RelatedItemsDetailTable } from '../../related-item/components/RelatedItemsDetailTable';
import { initiativesItemValidator } from '../initiative/initiative-validators';
import { TopButtons } from '../../../components/buttons/TopButtons';
import { FormSteps } from '../../../components/dots/FormSteps';
import { InitiativeListTable } from '../initiative/components/InitiativeListTable';
import { BUSINESS_CASE_DOCUMENT } from '../../document-management/business-case/BusinessCase.model';
import { LeftProgressSection } from '../../../components/new-ui/LeftProgressSection';
import { SCREENS_ISSUE } from '../screenView';
import { BoxCard } from '../../../components/new-ui/div/BoxCard';
import { CardFooter } from '../../../components/new-ui/card/CardFooter';
import { ISSUE_COMPLETED } from '../../../shared/status';

class IssueEditView extends View {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        issueData: R.clone(IssueModel),
        relatedItems: [],
        initiatives: [],
      },
      loading: true,
      initiativesList: [],
      clientCompany: null,
      partnerCompany: null,
      step: 0,
    };
    this.onError = onErrorMixin.bind(this);
    this.originalNextSteps = [];
    this.onChangeData = onChangeDataMixin.bind(this);
    this.originalDocuments = [];
  }

  onChangeIssueData = (name, value) => {
    const { data } = this.state;
    data.issueData[name] = value;
    this.setState({ data });
  };

  componentDidMount() {
    this.subscribe(fundingRequestStore, ISSUE_ERROR_EVENT, this.onError);
    this.subscribe(allianceStore, ALLIANCE_LIST_MEMBERS_EVENT, (state) => {
      this.setState({
        clientCompany: state.clientCompany,
        partnerCompany: state.partnerCompany,
      });
    });
    this.subscribe(fundingRequestStore, ISSUE_DETAIL_EVENT, (state) => {
      const { issue: issueData } = state;
      const { data } = this.state;
      issueData.documents = issueData.documents.items;
      const initiatives = issueData.initiatives.items;

      issueData.itemId = issueData.itemIssueRelation.id;
      this.originalNextSteps = sanitizeNextStepsToEdit(issueData);
      localStorage.setItem(ISSUE_DOCUMENTS, JSON.stringify(issueData.documents));
      this.originalDocuments = issueData.documents.concat();

      data.issueData = issueData;
      data.initiatives = initiatives;

      this.setState(
        {
          data,
          loading: false,
        },
        () => {
          fetchRelatedItems(issueData.itemId);
        },
      );
    });
    this.subscribe(fundingRequestStore, ISSUE_UPDATE_EVENT, () => {
      toast.success('Issue Successfully Updated');
      this.props.history.goBack();
    });
    this.subscribe(initiativeStore, INITIATIVE_LIST_EVENT, (state) => {
      const initiativesList = state.initiativesList.items;
      this.setState(
        {
          initiativesList,
        },
        () => {
          const { match } = this.props;
          if (!match.params.id) return toast.error('Issue ID missing');
          fetchIssueDetail(match.params.id);
        },
      );
    });

    this.subscribe(relatedItemStore, RELATED_ITEMS_EVENT, (state) => {
      const items = state.item.itemsRelated.items.map((item) => getItemByType(item));
      const { data } = this.state;
      data.relatedItems = items;
      this.setState({ data, loading: false });
    });

    fetchInitiativeList('', 1, 1000);
    fetchCurrentAllianceMembersAction();
  }

  componentWillUnmount() {
    super.componentWillUnmount();
    localStorage.removeItem(ISSUE_DOCUMENTS);
    localStorage.removeItem(BUSINESS_CASE_DOCUMENT);
  }

  onSubmit = () => {
    this.setState({ loading: true }, () => {
      const issueData = R.clone(this.state.data.issueData);
      const relatedItems = R.clone(this.state.data.relatedItems);
      const initiatives = R.clone(this.state.data.initiatives);
      updateIssue(
        issueData,
        this.originalNextSteps,
        relatedItems,
        initiatives,
        this.originalDocuments,
      );
    });
  };

  onIssueStepChange = (step) => {
    const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
    const issueData = R.clone(this.state.data.issueData);
    try {
      issueValidator(issueData, selectedAlliance);
    } catch (e) {
      return this.onError(e);
    }
    this.onScreen(step);
  };

  onRelatedItemsStepChange = (step) => {
    const initiatives = R.clone(this.state.data.initiatives);
    try {
      initiativesItemValidator(initiatives);
    } catch (e) {
      return this.onError(e);
    }
    this.onScreen(step);
  };

  onScreen = (step) => {
    this.setState({ step });
  };

  render() {
    const { initiativesList, companyId, loading, clientCompany, partnerCompany, step } = this.state;

    const { issueData, relatedItems, initiatives } = this.state.data;
    const { user, selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
    const { history } = this.props;

    let content = <Loader stretch />;
    let footer = <></>;
    const currency = getCurrencyOnSession();
    let buttonsTop = '';

    if (issueData.status === ISSUE_COMPLETED) history.push(`/management/amo-item`);

    if (!loading && step === 0) {
      content = (
        <IssueForm
          data={issueData}
          onChange={this.onChangeIssueData}
          clientCompany={clientCompany}
          partnerCompany={partnerCompany}
          myCompanyId={companyId}
          currency={currency}
          user={user}
          selectedAlliance={selectedAlliance}
        />
      );
      footer = (
        <CardFooter>
          <ActionButton onClick={() => this.onIssueStepChange(1)} text={'Next'} />
        </CardFooter>
      );
    }

    if (!loading && step === 1) {
      const itemData = { id: issueData.id, type: issueData.__typename };

      content = (
        <RelatedItemForm
          relatedItems={relatedItems}
          selectedInitiatives={initiatives}
          initiatives={initiativesList}
          onChange={(key, value) => {
            this.onChangeData(key, value);
          }}
          itemData={itemData}
        />
      );

      footer = (
        <CardFooter>
          <ActionButton onClick={() => this.onRelatedItemsStepChange(2)} text={'Next'} />
          <TransparentButton onClick={() => this.onScreen(0)} text={'Previous'} />
        </CardFooter>
      );
    }

    if (!loading && step === 2) {
      const issueDetail = R.clone(issueData);
      issueDetail.documents = { items: issueDetail.documents };
      issueDetail.nextSteps = { items: issueDetail.nextSteps };

      content = (
        <>
          <IssueDetailTable data={issueDetail} currency={currency} />
          <InitiativeListTable initiatives={initiatives} />

          <RelatedItemsDetailTable relatedItems={relatedItems} />
        </>
      );

      footer = (
        <CardFooter>
          <ActionButton onClick={this.onSubmit} text={'Edit Issue'} />
          <TransparentButton onClick={() => this.onScreen(1)} text={'Previous'} />
        </CardFooter>
      );
    }

    if (!loading)
      buttonsTop = (
        <TopButtons
          onClickClosed={history.goBack}
          onClickCollaborated={() => openComments(issueData)}
        />
      );

    return (
      <React.Fragment>
        <Card.Header>
          <Grid.Layout
            columns="364px auto 250px"
            areas={[['left', 'center', 'right']]}
            style={{ width: '100%' }}>
            <Grid.Box area="left">
              <Heading type="h4" text="Edit Issue" />
            </Grid.Box>
            <Grid.Box area="center">
              <FormSteps totalSteps={3} step={step} />
            </Grid.Box>
            <Grid.Box area="right" />
          </Grid.Layout>
          {buttonsTop}
        </Card.Header>
        <CreateViewCardBody>
          <Grid.Layout columns="30% 70%" areas={[['left', 'right']]} style={{ width: '100%' }}>
            <Grid.Box area="left">
              <LeftProgressSection sections={SCREENS_ISSUE} currentScreen={step} />
            </Grid.Box>
            <BoxCard>
              <Grid.Box area="right">{content}</Grid.Box>
            </BoxCard>
          </Grid.Layout>
        </CreateViewCardBody>
        {footer}
      </React.Fragment>
    );
  }
}

IssueEditView.propTypes = {
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default withRouter(IssueEditView);

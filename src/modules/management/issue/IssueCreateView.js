import React from 'react';
import { Card, Grid, Heading } from '@8base/boost';
import { CreateViewCardBody } from '../../../components/new-ui/card/CreateViewCardBody';
import IssueForm from './components/IssueForm';
import issueStore, { ISSUE_CREATE_EVENT, ISSUE_ERROR_EVENT } from './issue-store';
import * as toast from 'components/toast/Toast';
import { Loader } from '@8base/boost';
import { withRouter } from 'react-router-dom';
import { PropTypes } from 'prop-types';
import { IssueModel, ISSUE_DOCUMENTS } from './issue-model';
import View from '@cobuildlab/react-flux-state';
import { onChangeDataMixin, onErrorMixin } from '../../../shared/mixins';
import * as R from 'ramda';
import sessionStore, { NEW_SESSION_EVENT } from '../../../shared/SessionStore';
import { createIssue } from './issue-actions';
import { fetchInitiativeList } from '../initiative/initiative-actions';
import initiativeStore, { INITIATIVE_LIST_EVENT } from '../initiative/initiative-store';
import allianceStore, {
  ALLIANCE_LIST_MEMBERS_EVENT,
} from '../../settings/alliance-management/alliance-store';
import { fetchCurrentAllianceMembersAction } from '../../settings/alliance-management/alliance-actions';
import { getCurrencyOnSession } from '../../../shared/alliance-utils';
import { ActionButton } from '../../../components/buttons/ActionButton';
import { ActionButtonClose } from '../../../components/buttons/ActionButtonClose';
import { saveFormToSessionStorage } from 'shared/utils';
import { issueValidator } from './issue-validators';
import { initiativesItemValidator } from '../initiative/initiative-validators';
import RelatedItemForm from '../../related-item/components/RelatedItemForm';
import { TransparentButton } from '../../../components/buttons/TransparentButton';
import { RelatedItemsDetailTable } from '../../related-item/components/RelatedItemsDetailTable';
import IssueDetailTable from './components/IssueDetailTable';
import { FormSteps } from '../../../components/dots/FormSteps';
import { InitiativeListTable } from '../initiative/components/InitiativeListTable';
import { LeftProgressSection } from '../../../components/new-ui/LeftProgressSection';
import { SCREENS_ISSUE } from '../screenView';
import { BoxCard } from '../../../components/new-ui/div/BoxCard';
import { CardFooter } from '../../../components/new-ui/card/CardFooter';
const ISSUE_DATA_STORE = 'issueCreateView';

/**
 * Create Issue
 */
class IssueCreateView extends View {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        issueData: R.clone(IssueModel),
        relatedItems: [],
        initiatives: [],
      },
      loading: true,
      initiativesList: [],
      clientCompany: null,
      partnerCompany: null,
      userRole: true,
      step: 0,
    };
    this.onError = onErrorMixin.bind(this);
    this.onChangeData = onChangeDataMixin.bind(this);
  }

  onChangeIssueData = (name, value) => {
    const { data } = this.state;
    data.issueData[name] = value;
    if (name === 'originalDueDate') {
      data.issueData.revisedDueDate = value;
    }
    this.setState({ data });
    const model = R.clone(IssueModel);
    saveFormToSessionStorage(ISSUE_DATA_STORE, data.issueData, model, ['documents']);
  };

  componentDidMount() {
    const issueData = JSON.parse(sessionStorage.getItem(ISSUE_DATA_STORE));

    this.subscribe(issueStore, ISSUE_ERROR_EVENT, this.onError);
    this.subscribe(allianceStore, ALLIANCE_LIST_MEMBERS_EVENT, (state) => {
      this.setState({
        clientCompany: state.clientCompany,
        partnerCompany: state.partnerCompany,
      });
    });
    this.subscribe(issueStore, ISSUE_CREATE_EVENT, (state) => {
      sessionStorage.removeItem(ISSUE_DATA_STORE);
      toast.success('Issue Successfully Created');
      this.props.history.goBack();
    });
    this.subscribe(initiativeStore, INITIATIVE_LIST_EVENT, (state) => {
      const initiativesList = state.initiativesList.items;
      this.setState({
        loading: false,
        initiativesList,
      });
    });

    // set issueData from sessionStorage
    if (issueData) {
      const { data } = this.state;
      data.issueData = issueData;
      this.setState({ data });
    }

    fetchInitiativeList('', 1, 1000);
    fetchCurrentAllianceMembersAction();
  }

  componentWillUnmount() {
    super.componentWillUnmount();
    localStorage.removeItem(ISSUE_DOCUMENTS);
  }

  onSubmit = () => {
    this.setState({ loading: true }, () => {
      const issueData = R.clone(this.state.data.issueData);
      const relatedItems = R.clone(this.state.data.relatedItems);
      const initiatives = R.clone(this.state.data.initiatives);
      createIssue(issueData, relatedItems, initiatives);
    });
  };

  onIssueStepChange = (step) => {
    const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
    const issueData = R.clone(this.state.data.issueData);
    try {
      issueValidator(issueData, selectedAlliance);
    } catch (e) {
      return this.onError(e);
    }
    this.onScreen(step);
  };

  onScreen = (step) => {
    this.setState({ step });
  };

  onRelatedItemsStepChange = (step) => {
    const initiatives = R.clone(this.state.data.initiatives);
    try {
      initiativesItemValidator(initiatives);
    } catch (e) {
      return this.onError(e);
    }
    this.onScreen(step);
  };

  render() {
    const { initiativesList, loading, clientCompany, partnerCompany, step } = this.state;
    const { user, selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
    const { relatedItems, issueData, initiatives } = this.state.data;
    const { history } = this.props;

    const companyId = user.companyUserRelation.items[0].company.id;
    const currency = getCurrencyOnSession();

    let content = <Loader stretch />;
    let footer = <></>;

    if (!loading && step === 0) {
      content = (
        <IssueForm
          data={issueData}
          onChange={this.onChangeIssueData}
          clientCompany={clientCompany}
          partnerCompany={partnerCompany}
          myCompanyId={companyId}
          currency={currency}
          user={user}
          selectedAlliance={selectedAlliance}
        />
      );
      footer = (
        <CardFooter>
          <ActionButton onClick={() => this.onIssueStepChange(1)} text={'Next'} />
        </CardFooter>
      );
    }

    if (!loading && step === 1) {
      content = (
        <RelatedItemForm
          relatedItems={relatedItems}
          selectedInitiatives={initiatives}
          initiatives={initiativesList}
          onChange={(key, value) => {
            this.onChangeData(key, value);
          }}
        />
      );

      footer = (
        <CardFooter>
          <ActionButton onClick={() => this.onRelatedItemsStepChange(2)} text={'Next'} />
          <TransparentButton onClick={() => this.onScreen(0)} text={'Previous'} />
        </CardFooter>
      );
    }

    if (!loading && step === 2) {
      const issueDetail = R.clone(issueData);
      issueDetail.documents = { items: issueDetail.documents };
      issueDetail.nextSteps = { items: issueDetail.nextSteps };

      content = (
        <>
          <IssueDetailTable data={issueDetail} currency={currency} />
          <InitiativeListTable initiatives={initiatives} />

          <RelatedItemsDetailTable relatedItems={relatedItems} />
        </>
      );

      footer = (
        <CardFooter>
          <ActionButton onClick={this.onSubmit} text={'Create Issue'} />
          <TransparentButton onClick={() => this.onScreen(1)} text={'Previous'} />
        </CardFooter>
      );
    }

    return (
      <React.Fragment>
        <Card.Header>
          <Grid.Layout
            columns="200px auto 200px"
            areas={[['left', 'center', 'right']]}
            style={{ width: '100%' }}>
            <Grid.Box area="left">
              <Heading type="h4" text="Create Issue" />
            </Grid.Box>
            <Grid.Box area="center">
              <FormSteps totalSteps={3} step={step} />
            </Grid.Box>
            <Grid.Box area="right" />
          </Grid.Layout>
          <ActionButtonClose onClick={history.goBack} />
        </Card.Header>
        <CreateViewCardBody>
          <Grid.Layout columns="30% 70%" areas={[['left', 'right']]} style={{ width: '100%' }}>
            <Grid.Box area="left">
              <LeftProgressSection sections={SCREENS_ISSUE} currentScreen={step} />
            </Grid.Box>
            <BoxCard>
              <Grid.Box area="right">{content}</Grid.Box>
            </BoxCard>
          </Grid.Layout>
        </CreateViewCardBody>
        {footer}
      </React.Fragment>
    );
  }
}

IssueCreateView.propTypes = {
  history: PropTypes.object.isRequired,
};

export default withRouter(IssueCreateView);

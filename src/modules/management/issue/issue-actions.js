import sessionStore, { APOLLO_CLIENT, NEW_SESSION_EVENT } from '../../../shared/SessionStore';
import {
  sanitize8BaseDocumentsUpdate,
  sanitize8BaseReferenceFromObject,
  sanitize8BaseReconnectsFromObjects,
  sanitize8BaseReferencesFromObjects,
  sanitize8BaseDocumentsDeleteAndUpdate,
  sanitize8BaseBigInt,
} from '../../../shared/utils';
import Flux from '@cobuildlab/flux-state';
import { IntegrityError } from '../../../shared/errors';
import { ISSUE_COMPLETED, ISSUE_OPEN } from '../../../shared/status';
import { validateIssueData } from './issue-validators';
import { error, log } from '@cobuildlab/pure-logger';
import {
  ISSUE_CREATE_EVENT,
  ISSUE_ERROR_EVENT,
  ISSUE_DETAIL_EVENT,
  ISSUE_UPDATE_EVENT,
  ISSUE_COMPLETED_EVENT,
  ISSUE_RESTORE_EVENT,
} from './issue-store';
import {
  ISSUE_COMMENTS_QUERY,
  ISSUE_CREATE_MUTATION,
  ISSUE_DETAIL_QUERY,
  ISSUE_UPDATE_QUERY,
  ISSUE_UPDATE_MUTATION,
} from './issue-queries';
import {
  COMMENT_CREATE_EVENT,
  COMMENT_ERROR_EVENT,
  COMMENT_REQUEST_EVENT,
  COMMENTS_EVENT,
} from '../../comment/comment-store';
import { ISSUE_TYPE } from '../../../shared/item-types';
import { COMMENTS_CREATE_MUTATION } from '../../comment/comment-queries';
import { canCompletedIssue, canRestoreIssue } from './issue-permissions';
import {
  sanitizeNextStepsCreate,
  nextStepsToBeCreated,
  updateNextSteps,
  deleteNextSteps,
  completeItemsNextSteps,
} from 'modules/next-step/next-step-actions.js';
import * as R from 'ramda';
import { RELATED_ITEM_UPDATE_MUTATION } from '../../related-item/related-item-queries';

/**
 * Notifies a Request for Comments for a Issue.
 */
export const openComments = ({ id }) => {
  Flux.dispatchEvent(COMMENT_REQUEST_EVENT, { type: ISSUE_TYPE, id: id });
};

/**
 * Create Issue.
 *
 * @param {object} issue - Issue.
 * @param {Array} relatedItems - RelatedItems.
 * @param {object}initiatives - Initiatives.
 * @returns {Promise<void>} Return promise.
 */
export const createIssue = async (issue, relatedItems, initiatives) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  delete issue.id;
  const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
  const allianceId = selectedAlliance.id;

  if (!allianceId)
    return Flux.dispatchEvent(
      ISSUE_ERROR_EVENT,
      new IntegrityError('Must have an Active Alliance'),
    );

  try {
    validateIssueData(issue, initiatives, selectedAlliance);
  } catch (err) {
    console.error('createIssue', err);
    return Flux.dispatchEvent(ISSUE_ERROR_EVENT, err);
  }
  issue.initiatives = initiatives;
  // Transforming into 8base 'connect' relationships
  sanitize8BaseReferencesFromObjects(issue, 'initiatives');
  sanitize8BaseReferenceFromObject(issue, 'assignedTo');
  sanitize8BaseReferenceFromObject(issue, 'source');
  sanitize8BaseDocumentsUpdate(issue, 'documents');
  sanitize8BaseBigInt(issue, 'unitMonetizationFactor');
  sanitizeNextStepsCreate(issue);

  issue.itemIssueRelation = {
    create: {
      alliance: { connect: { id: allianceId } },
      itemsRelated: {
        connect: relatedItems.map((item) => {
          return { id: item.itemId };
        }),
      },
    },
  };

  let result;
  console.log('createIssue', issue);
  try {
    result = await client.mutate({
      mutation: ISSUE_CREATE_MUTATION,
      variables: { data: issue },
    });
  } catch (e) {
    console.error('createIssue', e);
    return Flux.dispatchEvent(ISSUE_ERROR_EVENT, e);
  }

  console.log('createIssue', result);
  return Flux.dispatchEvent(ISSUE_CREATE_EVENT, result);
};

/**
 * Create a comment on a Issue.
 *
 * @param {string}issueId - Issue Id.
 * @param {Array}comment - Comment.
 * @returns {Promise<*>} Return promise.
 */
export const createIssueComment = async (issueId, comment) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const data = {
    comment,
    issueCommentsRelation: { connect: { id: issueId } },
  };

  let response;
  try {
    response = await client.mutate({
      mutation: COMMENTS_CREATE_MUTATION,
      variables: { data },
    });
  } catch (e) {
    error('createIssueComment', e);
    return Flux.dispatchEvent(COMMENT_ERROR_EVENT, e);
  }
  log('createIssueComment', response);
  Flux.dispatchEvent(COMMENT_CREATE_EVENT, response.data);
  return response.data;
};

/**
 * Fetches the Issue Item Comments.
 *
 * @param {string}issueId - Issue.
 * @returns {Promise<void>} Return promise.
 */
export const fetchIssueComments = async (issueId) => {
  console.log('issueID', issueId);
  const client = sessionStore.getState(APOLLO_CLIENT);
  const data = { id: issueId };
  const { user } = sessionStore.getState(NEW_SESSION_EVENT);

  let response;
  try {
    response = await client.query({
      query: ISSUE_COMMENTS_QUERY,
      variables: data,
      fetchPolicy: 'network-only',
    });
  } catch (e) {
    error('fetchIssueComments', e);
    return Flux.dispatchEvent(COMMENT_ERROR_EVENT, e);
  }
  log('fetchIssueComments', response);
  response.data.issue.comments.userId = user.id;

  Flux.dispatchEvent(COMMENTS_EVENT, response.data.issue.comments);
  return response.data.issue.comments;
};

/**
 * Fetches the Issue Item.
 *
 * @param {string}issueId - Issue Id.
 * @returns {Promise<void>} Return promise.
 */
export const fetchIssue = async (issueId) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const data = { id: issueId };

  let response;
  try {
    response = await client.query({
      query: ISSUE_DETAIL_QUERY,
      variables: data,
      fetchPolicy: 'network-only',
    });
  } catch (e) {
    error('fetchIssue', e);
    throw e;
  }

  log('fetchIssue', response);
  return response.data;
};

/**
 * Fetches the Issue Item and dispatchEvent.
 *
 * @param {string}issueId - Issue Id.
 * @returns {Promise<void>} Return promise.
 */
export const fetchIssueDetail = async (issueId) => {
  let response;
  try {
    response = await fetchIssue(issueId);
  } catch (e) {
    return Flux.dispatchEvent(ISSUE_ERROR_EVENT, e);
  }

  Flux.dispatchEvent(ISSUE_DETAIL_EVENT, response);
  return response;
};

/**
 * Update a Issue.
 *
 * @param {object}issueData - Issue.
 * @param {Array}originalNextSteps - OriginalNextSteps.
 * @param {Array}relatedItems - RelatedItems.
 * @param {object}initiatives - Initiatives.
 * @param {object}originalDocuments - OriginalDocuments.
 * @returns {Promise<void>} Return promise.
 */
export const updateIssue = async (
  issueData,
  originalNextSteps,
  relatedItems,
  initiatives,
  originalDocuments,
) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);

  const itemId = issueData.itemIssueRelation.id;

  delete issueData.createdAt;
  delete issueData.itemId;
  delete issueData.itemIssueRelation;
  delete issueData.__typename;
  delete issueData.createdBy;

  log('issueData', issueData, relatedItems, initiatives, originalNextSteps);

  try {
    validateIssueData(issueData, initiatives, selectedAlliance);
  } catch (e) {
    console.error('Update Issue Validator', e);
    return Flux.dispatchEvent(ISSUE_ERROR_EVENT, e);
  }

  issueData.initiatives = initiatives;
  sanitize8BaseReconnectsFromObjects(issueData, 'initiatives');
  sanitize8BaseReferenceFromObject(issueData, 'requestedBy');
  sanitize8BaseReferenceFromObject(issueData, 'assignedTo');
  sanitize8BaseReferenceFromObject(issueData, 'source');
  sanitize8BaseBigInt(issueData, 'unitMonetizationFactor');
  sanitize8BaseDocumentsDeleteAndUpdate(issueData, 'documents', originalDocuments);

  // nextSteps to be created
  const nextSteps = R.clone(issueData.nextSteps);
  const toBeCreated = nextStepsToBeCreated(nextSteps);
  issueData.nextSteps = toBeCreated;
  sanitizeNextStepsCreate(issueData);

  // update and delete nextSteps
  await deleteNextSteps(nextSteps, originalNextSteps);
  await updateNextSteps(nextSteps, originalNextSteps);

  let response;
  try {
    response = await client.mutate({
      mutation: ISSUE_UPDATE_QUERY,
      variables: { data: issueData },
    });
  } catch (e) {
    console.error('updateIssue', e, issueData);
    return Flux.dispatchEvent(ISSUE_ERROR_EVENT, e);
  }

  // Reconnect related items
  const relatedItemsData = {
    id: itemId,
    itemsRelated: {
      reconnect: relatedItems.map((i) => {
        return { id: i.itemId };
      }),
    },
  };

  log('issueData:update:relatedItems:', relatedItemsData);
  try {
    await client.mutate({
      mutation: RELATED_ITEM_UPDATE_MUTATION,
      variables: { data: relatedItemsData },
    });
  } catch (e) {
    error('issueData:updateRelated Items', e, issueData);
    return Flux.dispatchEvent(ISSUE_ERROR_EVENT, e);
  }

  Flux.dispatchEvent(ISSUE_UPDATE_EVENT, response.data);
  return response.data;
};

/**
 * Change status item the completed to open.
 *
 * @param {object}issueData - Issue.
 * @returns {Promise<void|*>} Return promise.
 */
export const completedIssue = async (issueData) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { user, selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
  const { issue } = await fetchIssue(issueData.id);

  if (!canCompletedIssue(user, issue, selectedAlliance))
    return Flux.dispatchEvent(
      ISSUE_ERROR_EVENT,
      new IntegrityError('You do not have permission to perform this action'),
    );

  let response;
  try {
    response = await client.mutate({
      mutation: ISSUE_UPDATE_MUTATION,
      variables: {
        data: {
          id: issue.id,
          status: ISSUE_COMPLETED,
        },
      },
    });
  } catch (e) {
    console.error('CompletedIssue', e, issue);
    return Flux.dispatchEvent(ISSUE_ERROR_EVENT, e);
  }

  try {
    await completeItemsNextSteps(issue);
  } catch (e) {
    log('completeItemsNextStepsError', e);
  }

  Flux.dispatchEvent(ISSUE_COMPLETED_EVENT, response.data);
  return response.data;
};

/**
 * User Restore Issue.
 *
 * @param {object} issueData - Issue.
 * @returns {Promise<void|*>} - Promise.
 */
export const restoreIssue = async (issueData) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { user, selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
  const { issue } = await fetchIssue(issueData.id);

  if (!canRestoreIssue(user, issue, selectedAlliance))
    return Flux.dispatchEvent(
      ISSUE_ERROR_EVENT,
      new IntegrityError('You do not have permission to perform this action'),
    );

  let response;
  try {
    response = await client.mutate({
      mutation: ISSUE_UPDATE_MUTATION,
      variables: {
        data: {
          id: issue.id,
          status: ISSUE_OPEN,
        },
      },
    });
  } catch (e) {
    console.error('restoredIssue', e, issue);
    return Flux.dispatchEvent(ISSUE_ERROR_EVENT, e);
  }

  Flux.dispatchEvent(ISSUE_RESTORE_EVENT, response.data);
  return response.data;
};

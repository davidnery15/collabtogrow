import moment from 'moment';

const currentDate = moment().format('YYYY-MM-DD');

export const FundingRequestModel = {
  id: null,
  name: '',
  description: '',
  documents: [],
  nextSteps: [],
  assignedTo: null,
  assignedDate: currentDate,
  requestedDate: currentDate,
  originalDueDate: null,
  revisedDueDate: null,
  requestedBy: null,
  budgetUtilized: '0',
  unitType: null,
  unitQuantity: 0,
  unitValueDescription: '',
  unitMonetizationFactor: '0',
  source: null,
};

export const FUNDING_REQUEST_DOCUMENTS = 'fundingRequestDocuments';

import sessionStore, { APOLLO_CLIENT, NEW_SESSION_EVENT } from '../../../shared/SessionStore';
import {
  sanitize8BaseDocumentCreate,
  sanitize8BaseDocumentsCreate,
  sanitize8BaseDocumentsDeleteAndUpdate,
  sanitize8BaseReconnects,
  sanitize8BaseReferenceFromObject,
  sanitize8BaseReferences,
  sanitize8BaseBigInt,
} from '../../../shared/utils';
import Flux from '@cobuildlab/flux-state';
import { IntegrityError } from '../../../shared/errors';
import { fundingRequestValidator, validateFundingRequestData } from './funding-request-validators';
import {
  sanitizeRecommendedSolutions,
  updateBusinessCase,
} from '../../document-management/business-case/businessCases.actions';
import { error, log } from '@cobuildlab/pure-logger';
import {
  fetchAlliance,
  fetchCurrentAllianceMembersAction,
} from '../../settings/alliance-management/alliance-actions';
import {
  FUNDING_REQUEST_COMPLETED_EVENT,
  FUNDING_REQUEST_CREATE_EVENT,
  FUNDING_REQUEST_DETAIL_EVENT,
  FUNDING_REQUEST_ERROR_EVENT,
  FUNDING_REQUEST_FORM_DATA_EVENT,
  FUNDING_REQUEST_REJECT_EVENT,
  FUNDING_REQUEST_UPDATE_EVENT,
  FUNDING_REQUEST_SUBMIT_FOR_APPROVAL_EVENT,
  FUNDING_REQUEST_RESTORE_EVENT,
} from './funding-request-store';
import { getActiveAllianceId, getCompanyOnAlliance } from '../../../shared/alliance-utils';
import {
  FUNDING_REQUEST_APPROVAL_MUTATION,
  FUNDING_REQUEST_APPROVAL_UPDATE_MUTATION,
  FUNDING_REQUEST_APPROVALS_LIST_QUERY,
  FUNDING_REQUEST_COMMENTS_QUERY,
  FUNDING_REQUEST_CREATE_MUTATION,
  FUNDING_REQUEST_DETAIL_QUERY,
  FUNDING_REQUEST_UPDATE_MUTATION,
  FUNDING_REQUEST_UPDATE_QUERY,
} from './funding-request-queries';
import {
  FUNDING_REQUEST_APPROVAL_APPROVED,
  FUNDING_REQUEST_APPROVAL_REJECTED,
  FUNDING_REQUEST_APPROVED,
  FUNDING_REQUEST_COMPLETED,
  FUNDING_REQUEST_IN_PROGRESS,
  FUNDING_REQUEST_SUBMITTED_FOR_APPROVAL,
} from '../../../shared/status';
import {
  COMMENT_CREATE_EVENT,
  COMMENT_ERROR_EVENT,
  COMMENT_REQUEST_EVENT,
  COMMENTS_EVENT,
} from '../../comment/comment-store';
import { FUNDING_REQUEST_TYPE } from '../../../shared/item-types';
import { COMMENTS_CREATE_MUTATION } from '../../comment/comment-queries';
import { INITIATIVE_ERROR_EVENT } from '../initiative/initiative-store';
import {
  canApproveFundingRequest,
  canCompletedFundingRequest,
  canRejectFundingRequest,
  canRestoreFundingRequest,
  canSubmitForApprovalFundingRequest,
} from './funding-request-permissions';
import { businessCaseValidator } from '../../document-management/business-case/business-case-validators';
import {
  deleteNextSteps,
  nextStepsToBeCreated,
  sanitizeNextStepsCreate,
  updateNextSteps,
  completeItemsNextSteps,
  restoreItemsNextSteps,
} from '../../../modules/next-step/next-step-actions.js';
import * as R from 'ramda';
import { hasActiveAllianceDecorator } from '../../../shared/decorators';
import { RELATED_ITEM_UPDATE_MUTATION } from '../../related-item/related-item-queries';

/**
 * Notifies a Request for Comments for a Funding Request.
 */
export const openComments = ({ id: fundingRequestId }) => {
  Flux.dispatchEvent(COMMENT_REQUEST_EVENT, { type: FUNDING_REQUEST_TYPE, id: fundingRequestId });
};

/**
 * Create a Funding Request.
 *
 * @param {object}fundingRequestData
 * @param {object}businessCaseData
 * @param {object}relatedItems
 * @param {object}initiatives
 */

/**
 *
 * Create a Funding Request.
 *
 * @param {object}fundingRequestData - Funding Request.
 * @param {object}businessCaseData - Business Case.
 * @param {Array}relatedItems - Related Items.
 * @param {object}initiatives - Initiatives.
 * @returns {Promise<void|*>} Return promise.
 */
export const createFundingRequest = async (
  fundingRequestData,
  businessCaseData,
  relatedItems,
  initiatives,
) => {
  log('createFundingRequest', fundingRequestData, businessCaseData, relatedItems);
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
  const allianceId = selectedAlliance.id;

  if (!allianceId)
    return Flux.dispatchEvent(
      FUNDING_REQUEST_ERROR_EVENT,
      new IntegrityError('Must have an Active Alliance'),
    );

  // Remove
  delete fundingRequestData.id;
  delete businessCaseData.id;
  fundingRequestData.initiatives = initiatives.map((i) => i.id);
  try {
    validateFundingRequestData(
      fundingRequestData,
      initiatives,
      businessCaseData,
      true,
      selectedAlliance,
    );
  } catch (err) {
    console.error('createFundingRequest', err);
    Flux.dispatchEvent(FUNDING_REQUEST_ERROR_EVENT, err);
    return err;
  }

  // Transforming into 8base 'connect' relationships
  sanitize8BaseDocumentsCreate(fundingRequestData, 'documents');
  sanitize8BaseReferenceFromObject(fundingRequestData, 'requestedBy');
  sanitize8BaseReferenceFromObject(fundingRequestData, 'assignedTo');
  sanitize8BaseReferenceFromObject(fundingRequestData, 'source');
  sanitize8BaseReferences(fundingRequestData, 'initiatives');
  sanitizeNextStepsCreate(fundingRequestData);
  sanitize8BaseBigInt(fundingRequestData, 'budgetUtilized');
  sanitize8BaseBigInt(fundingRequestData, 'unitMonetizationFactor');
  sanitizeRecommendedSolutions(businessCaseData);
  sanitize8BaseDocumentCreate(businessCaseData, 'document');

  fundingRequestData.businessCase = { create: businessCaseData };
  fundingRequestData.itemFundingRequestRelation = {
    create: {
      alliance: { connect: { id: allianceId } },
      itemsRelated: {
        connect: relatedItems.map((item) => {
          return { id: item.itemId };
        }),
      },
    },
  };
  log('createFundingRequest:fundingRequestData:', fundingRequestData);
  let result;
  try {
    result = await client.mutate({
      mutation: FUNDING_REQUEST_CREATE_MUTATION,
      variables: { data: fundingRequestData },
    });
  } catch (e) {
    console.error('createFundingRequest', e);
    return Flux.dispatchEvent(FUNDING_REQUEST_ERROR_EVENT, e);
  }

  Flux.dispatchEvent(FUNDING_REQUEST_CREATE_EVENT, result);
  return result;
};

/**
 * Retrieve the Data necessary to the Funding Request Form.
 *
 * @returns {Promise<void|*>} Return promise.
 */
export const fetchFundingRequestFormDataAction = async () => {
  log('fetchFundingRequestFormDataAction');
  let allianceMembers;
  try {
    allianceMembers = await fetchCurrentAllianceMembersAction();
  } catch (e) {
    error('fetchFundingRequestFormDataAction', e);
    return Flux.dispatchEvent(FUNDING_REQUEST_ERROR_EVENT, e);
  }
  const data = { ...allianceMembers };
  Flux.dispatchEvent(FUNDING_REQUEST_FORM_DATA_EVENT, data);
  return data;
};

/**
 * Create a comment on a Funding Request.
 *
 * @param {string}fundingRequestId - Funding Request Id.
 * @param {Array}comment - Comment.
 * @returns {Promise<void|*>} Return promise.
 */
export const createFundingRequestComment = async (fundingRequestId, comment) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const data = {
    comment,
    fundingRequestCommentsRelation: { connect: { id: fundingRequestId } },
  };

  let response;
  try {
    response = await client.mutate({
      mutation: COMMENTS_CREATE_MUTATION,
      variables: { data },
    });
  } catch (e) {
    error('createFundingRequestComment', e);
    return Flux.dispatchEvent(COMMENT_ERROR_EVENT, e);
  }
  log('createFundingRequestComment', response);
  Flux.dispatchEvent(COMMENT_CREATE_EVENT, response.data);
  return response.data;
};

/**
 * Fetches the funding Request Item Comments.
 *
 * @param {string}fundingRequestId - Funding Request Id.
 * @returns {Promise<void|*>} Return promise.
 */
export const fetchFundingRequestComments = async (fundingRequestId) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const data = { id: fundingRequestId };
  const { user } = sessionStore.getState(NEW_SESSION_EVENT);

  let response;
  try {
    response = await client.query({
      query: FUNDING_REQUEST_COMMENTS_QUERY,
      variables: data,
      fetchPolicy: 'network-only',
    });
  } catch (e) {
    error('fetchFundingRequestComments', e);
    return Flux.dispatchEvent(COMMENT_ERROR_EVENT, e);
  }
  log('fetchFundingRequestComments', response);
  response.data.fundingRequest.comments.userId = user.id;

  Flux.dispatchEvent(COMMENTS_EVENT, response.data.fundingRequest.comments);
  return response.data.fundingRequest.comments;
};

/**
 * Fetches the funding Request Item.
 *
 * @param {string}fundingRequestId - Funding Request Id.
 * @returns {Promise<void|*>} Return promise.
 */
export const fetchFundingRequest = async (fundingRequestId) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const data = { id: fundingRequestId };

  let response;
  try {
    response = await client.query({
      query: FUNDING_REQUEST_DETAIL_QUERY,
      variables: data,
      fetchPolicy: 'network-only',
    });
  } catch (e) {
    error('fetchFundingRequest', e);
    throw e;
  }

  log('fetchFundingRequest:response:', response.data);

  return response.data;
};

const _updateFundingRequest = async (
  fundingRequestData,
  businessCaseData,
  originalRecommendedSolutions,
  originalNextSteps,
  relatedItems,
  initiatives,
  originalDocuments,
) => {
  console.log(
    `_updateFundingRequest:`,
    fundingRequestData,
    businessCaseData,
    originalRecommendedSolutions,
    originalNextSteps,
    relatedItems,
    initiatives,
  );
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
  const itemId = fundingRequestData.itemFundingRequestRelation.id;

  // Remove
  delete fundingRequestData.itemFundingRequestRelation.id;
  delete fundingRequestData.itemFundingRequestRelation;
  delete fundingRequestData.createdBy;
  delete fundingRequestData.createdAt;
  delete fundingRequestData.fundingRequestApprovalRelation;
  delete fundingRequestData.status;
  delete fundingRequestData.__typename;
  delete businessCaseData.createdAt;
  delete businessCaseData.status;
  delete businessCaseData.recommendedSolutionsRelation;
  delete businessCaseData.__typename;
  delete businessCaseData.owner;
  fundingRequestData.initiatives = initiatives.map((i) => i.id);
  try {
    validateFundingRequestData(
      fundingRequestData,
      initiatives,
      businessCaseData,
      true,
      selectedAlliance,
    );
  } catch (err) {
    console.error('Update funding Request Validator', err);
    Flux.dispatchEvent(FUNDING_REQUEST_ERROR_EVENT, err);
    return err;
  }

  sanitize8BaseReferenceFromObject(fundingRequestData, 'requestedBy');
  sanitize8BaseReferenceFromObject(fundingRequestData, 'assignedTo');
  sanitize8BaseReferenceFromObject(fundingRequestData, 'source');
  sanitize8BaseReconnects(fundingRequestData, 'initiatives');
  sanitize8BaseBigInt(fundingRequestData, 'budgetUtilized');
  sanitize8BaseBigInt(fundingRequestData, 'unitMonetizationFactor');
  sanitize8BaseDocumentsDeleteAndUpdate(fundingRequestData, 'documents', originalDocuments);

  // nextSteps to be created
  const nextSteps = R.clone(fundingRequestData.nextSteps);
  fundingRequestData.nextSteps = nextStepsToBeCreated(nextSteps, originalNextSteps);
  sanitizeNextStepsCreate(fundingRequestData);

  // update and delete nextSteps
  await deleteNextSteps(nextSteps, originalNextSteps);
  await updateNextSteps(nextSteps, originalNextSteps);

  console.log('fundingRequestData:update:fundingRequestData:', fundingRequestData);
  // Updating Funding Request
  try {
    await client.mutate({
      mutation: FUNDING_REQUEST_UPDATE_QUERY,
      variables: { data: fundingRequestData },
    });
  } catch (e) {
    console.log('fundingRequestData:update:', fundingRequestData);
    return Flux.dispatchEvent(FUNDING_REQUEST_ERROR_EVENT, e);
  }

  // Updating Business Case
  console.log('fundingRequestData:update:businessCaseData:', businessCaseData);
  try {
    await updateBusinessCase(businessCaseData, originalRecommendedSolutions);
  } catch (e) {
    console.error('updateFundingRequest:updateBusinessCase', e);
    return Flux.dispatchEvent(FUNDING_REQUEST_ERROR_EVENT, e);
  }

  // Updating Related Items
  const data = {
    id: itemId,
    itemsRelated: {
      reconnect: relatedItems.map((i) => {
        return { id: i.itemId };
      }),
    },
  };
  console.log('fundingRequestData:update:relatedItems:', data);
  try {
    await client.mutate({
      mutation: RELATED_ITEM_UPDATE_MUTATION,
      variables: { data },
    });
  } catch (e) {
    console.error('updateFundingRequest:updateRelated Items', e, fundingRequestData);
    return Flux.dispatchEvent(FUNDING_REQUEST_ERROR_EVENT, e);
  }

  Flux.dispatchEvent(FUNDING_REQUEST_UPDATE_EVENT, {});
  return {};
};
export const updateFundingRequest = hasActiveAllianceDecorator(
  _updateFundingRequest,
  FUNDING_REQUEST_ERROR_EVENT,
);

/**
 * Submit For Approval a Funding Request.
 *
 * @param {object}investmentItem - Investment Item.
 * @returns {Promise<void|*>} Return promise.
 */
export const requestApprovalForFundingRequest = async (investmentItem) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { user, selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
  const fundingRequestId = investmentItem.fundingRequest.id;
  const { fundingRequest } = await fetchFundingRequest(fundingRequestId);
  const activeAllianceId = selectedAlliance.id;

  if (!canSubmitForApprovalFundingRequest(user, fundingRequest, selectedAlliance))
    return Flux.dispatchEvent(
      FUNDING_REQUEST_ERROR_EVENT,
      new IntegrityError('You do not have permission to perform this action'),
    );

  log('requestApprovalForFundingRequest', 'toValidate', fundingRequest);
  // set initiatives & nextSteps for validators
  fundingRequest.initiatives = fundingRequest.initiatives.items;
  fundingRequest.nextSteps = fundingRequest.nextSteps.items;
  try {
    fundingRequestValidator(fundingRequest, selectedAlliance);
    businessCaseValidator(fundingRequest.businessCase);
  } catch (e) {
    console.error('Update funding Request Validator', e);
    return Flux.dispatchEvent(FUNDING_REQUEST_ERROR_EVENT, e);
  }

  const { alliance } = await fetchAlliance(activeAllianceId);
  log('requestApprovalForFundingRequest', 'alliance', alliance);
  const data = {
    id: fundingRequest.id,
    status: FUNDING_REQUEST_SUBMITTED_FOR_APPROVAL,
    fundingRequestApprovalRelation: {
      create: [
        { company: { connect: { id: alliance.clientCompany.id } } },
        { company: { connect: { id: alliance.partnerCompany.id } } },
      ],
    },
  };

  let response;
  try {
    response = await client.mutate({
      mutation: FUNDING_REQUEST_UPDATE_MUTATION,
      variables: { data },
    });
  } catch (e) {
    console.error('requestApprovalForFundingRequest', e, investmentItem);
    return Flux.dispatchEvent(FUNDING_REQUEST_ERROR_EVENT, e);
  }
  Flux.dispatchEvent(FUNDING_REQUEST_SUBMIT_FOR_APPROVAL_EVENT, response.data);
  return response.data;
};

/**
 * Fetches the Funding Request approvals for the User.
 *
 * @param {string} fundingRequestId - Funding Request Id.
 * @returns {Promise<void|*>} Return promise.
 */
export const fetchFundingRequestApprovals = async (fundingRequestId) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const data = { fundingRequest: { id: { equals: fundingRequestId } } };

  let response;
  try {
    response = await client.query({
      query: FUNDING_REQUEST_APPROVALS_LIST_QUERY,
      variables: { data },
      fetchPolicy: 'network-only',
    });
  } catch (e) {
    error('fetchFundingRequestApprovals', e);
    return Flux.dispatchEvent(INITIATIVE_ERROR_EVENT, e);
  }
  log('fetchFundingRequestApprovals', response);
  return response.data;
};

/**
 * Approve Funding Request.
 *
 * @param {object}fundingRequest - Funding Request.
 * @returns {Promise<void|*>} Return promise.
 */
export const approveFundingRequest = async (fundingRequest) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { user, selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
  const fundingRequestId = fundingRequest.id;
  const allianceId = selectedAlliance.id;
  const company = getCompanyOnAlliance(user, { id: allianceId });

  if (!canApproveFundingRequest(user, fundingRequest, { id: allianceId }))
    return Flux.dispatchEvent(
      FUNDING_REQUEST_ERROR_EVENT,
      new IntegrityError(`Permission Denied. Can't approve this Funding Request`),
    );

  // We fetch the approvals to see what's gonna happen
  const { fundingRequestApprovalsList } = await fetchFundingRequestApprovals(fundingRequestId);
  if (fundingRequestApprovalsList.count < 2) {
    return Flux.dispatchEvent(
      FUNDING_REQUEST_ERROR_EVENT,
      new IntegrityError('Funding Request must have have at least 2 request for approvals'),
    );
  }

  let approvalsCounter = 0;
  let approvalId = null;
  fundingRequestApprovalsList.items.forEach((approval) => {
    if (approval.status === FUNDING_REQUEST_APPROVAL_APPROVED) approvalsCounter++;
    if (approval.company.id === company.id) approvalId = approval.id;
  });
  if (approvalId === null) {
    return Flux.dispatchEvent(
      FUNDING_REQUEST_ERROR_EVENT,
      new IntegrityError(
        "You can't approve this Funding Request, your Company does not belong here.",
      ),
    );
  }

  // Update the status of the Approval
  const mutation = {
    mutation: FUNDING_REQUEST_APPROVAL_UPDATE_MUTATION,
    variables: {
      approval: {
        id: approvalId,
        status: FUNDING_REQUEST_APPROVAL_APPROVED,
        dateOfResponse: new Date(),
        approvedBy: { connect: { id: user.id } },
      },
    },
  };
  // If there are more than 0 approvals: Update the status of the fundingRequest
  if (approvalsCounter > 0) {
    mutation.mutation = FUNDING_REQUEST_APPROVAL_MUTATION;
    mutation.variables.fundingRequest = {
      id: fundingRequestId,
      status: FUNDING_REQUEST_APPROVED,
    };
  }

  let response;
  try {
    response = await client.mutate(mutation);
  } catch (e) {
    error('approveFundingRequest', e);
    return Flux.dispatchEvent(FUNDING_REQUEST_ERROR_EVENT, e);
  }
  log('approveFundingRequest', response);
  Flux.dispatchEvent(FUNDING_REQUEST_UPDATE_EVENT, response.data);
  return response.data;
};

/**
 * Reject Funding Request.
 *
 * @param {string} fundingRequest - Funding Request.
 * @returns {Promise<void|*>} Return promise.
 */
export const rejectFundingRequest = async (fundingRequest) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const user = sessionStore.getState(NEW_SESSION_EVENT).user;
  const fundingRequestId = fundingRequest.id;
  const allianceId = getActiveAllianceId(user);
  const company = getCompanyOnAlliance(user, { id: allianceId });

  if (!canRejectFundingRequest(user, fundingRequest, { id: allianceId }))
    return Flux.dispatchEvent(
      FUNDING_REQUEST_ERROR_EVENT,
      new IntegrityError(`Permission Denied. Can't reject this Funding Request`),
    );

  // We fetch the approvals to see what's gonna happen
  const { fundingRequestApprovalsList } = await fetchFundingRequestApprovals(fundingRequestId);
  if (fundingRequestApprovalsList.count < 2) {
    return Flux.dispatchEvent(
      FUNDING_REQUEST_ERROR_EVENT,
      new IntegrityError('Funding Request must have have at least 2 request for approvals'),
    );
  }

  let approvalId = null;
  let alliedCompanyApprovalId = null;
  fundingRequestApprovalsList.items.forEach((approval) => {
    if (approval.company.id === company.id) {
      approvalId = approval.id;
    } else {
      alliedCompanyApprovalId = approval.id;
    }
  });
  if (approvalId === null) {
    return Flux.dispatchEvent(
      FUNDING_REQUEST_ERROR_EVENT,
      new IntegrityError(
        "You can't reject this Funding Request, your Company does not belong here.",
      ),
    );
  }

  // Update the status of the Approval
  const userCompanyMutation = {
    mutation: FUNDING_REQUEST_APPROVAL_MUTATION,
    variables: {
      approval: {
        id: approvalId,
        status: FUNDING_REQUEST_APPROVAL_REJECTED,
        dateOfResponse: new Date(),
        approvedBy: { connect: { id: user.id } },
      },
      fundingRequest: {
        id: fundingRequestId,
        status: FUNDING_REQUEST_APPROVAL_REJECTED,
      },
    },
  };

  const alliedCompanyMutation = {
    mutation: FUNDING_REQUEST_APPROVAL_UPDATE_MUTATION,
    variables: {
      approval: {
        id: alliedCompanyApprovalId,
        status: FUNDING_REQUEST_APPROVAL_REJECTED,
        dateOfResponse: new Date(),
      },
    },
  };

  let response;
  try {
    response = await client.mutate(userCompanyMutation);
    await client.mutate(alliedCompanyMutation);
  } catch (e) {
    error('rejectFundingRequest', e);
    return Flux.dispatchEvent(FUNDING_REQUEST_ERROR_EVENT, e);
  }
  log('rejectFundingRequest', response);
  Flux.dispatchEvent(FUNDING_REQUEST_REJECT_EVENT, response.data);
  return response.data;
};

/**
 * Completed a Funding Request.
 *
 * @param {object}fundingRequestData - Funding Request.
 * @returns {Promise<void|*>} Return promise.
 */
export const completedFundingRequest = async (fundingRequestData) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { selectedAlliance, user } = sessionStore.getState(NEW_SESSION_EVENT);
  const { fundingRequest } = await fetchFundingRequest(fundingRequestData.id);
  let response;

  if (!canCompletedFundingRequest(user, fundingRequest, selectedAlliance))
    return Flux.dispatchEvent(
      FUNDING_REQUEST_ERROR_EVENT,
      new IntegrityError('You do not have permission to perform this action'),
    );

  try {
    response = await client.mutate({
      mutation: FUNDING_REQUEST_UPDATE_QUERY,
      variables: {
        data: {
          id: fundingRequest.id,
          status: FUNDING_REQUEST_COMPLETED,
        },
      },
    });
  } catch (e) {
    error('completedFundingRequest', e);
    return Flux.dispatchEvent(FUNDING_REQUEST_ERROR_EVENT, e);
  }

  try {
    await completeItemsNextSteps(fundingRequest);
  } catch (e) {
    log('completeItemsNextStepsError', e);
  }

  Flux.dispatchEvent(FUNDING_REQUEST_COMPLETED_EVENT, response);
  return response.data;
};

/**
 * Restore Funding Request.
 *
 * @param {object}fundingRequestData - Funding Request.
 * @returns {Promise<void|*>} Return promise.
 */
export const restoreFundingRequest = async (fundingRequestData) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { selectedAlliance, user } = sessionStore.getState(NEW_SESSION_EVENT);
  const { fundingRequest } = await fetchFundingRequest(fundingRequestData.id);
  let response;

  if (!canRestoreFundingRequest(user, fundingRequest, selectedAlliance))
    return Flux.dispatchEvent(
      FUNDING_REQUEST_ERROR_EVENT,
      new IntegrityError('You do not have permission to perform this action'),
    );

  try {
    response = await client.mutate({
      mutation: FUNDING_REQUEST_UPDATE_QUERY,
      variables: {
        data: {
          id: fundingRequest.id,
          status: FUNDING_REQUEST_IN_PROGRESS,
        },
      },
    });
  } catch (e) {
    error('completedFundingRequest', e);
    return Flux.dispatchEvent(FUNDING_REQUEST_ERROR_EVENT, e);
  }

  Flux.dispatchEvent(FUNDING_REQUEST_RESTORE_EVENT, response);
  return response.data;
};

/**
 * This function call funding request data and dispatch event.
 *
 * @param {string}id - Id.
 * @returns {Promise<*>} Return promise.
 */
export const fetchFundingRequestDetail = async (id) => {
  let response;
  try {
    response = await fetchFundingRequest(id);
  } catch (e) {
    return Flux.dispatchEvent(FUNDING_REQUEST_ERROR_EVENT, e);
  }

  Flux.dispatchEvent(FUNDING_REQUEST_DETAIL_EVENT, response);

  return response;
};

import React from 'react';
import { Card, Heading, Loader, Grid } from '@8base/boost';
import { CreateViewCardBody } from '../../../components/new-ui/card/CreateViewCardBody';
import fundingRequestStore, {
  FUNDING_REQUEST_DETAIL_EVENT,
  FUNDING_REQUEST_ERROR_EVENT,
  FUNDING_REQUEST_UPDATE_EVENT,
  FUNDING_REQUEST_FORM_DATA_EVENT,
} from './funding-request-store';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import * as toast from '../../../components/toast/Toast';
import * as R from 'ramda';
import View from '@cobuildlab/react-flux-state';
import { onChangeDataMixin, onErrorMixin } from '../../../shared/mixins';
import { FundingRequestModel, FUNDING_REQUEST_DOCUMENTS } from './funding-request-model';
import FundingRequestForm from './components/FundingRequestForm';
import {
  fetchFundingRequestDetail,
  openComments,
  updateFundingRequest,
} from './funding-request-action';
import BusinessCaseModel, {
  BUSINESS_CASE_DOCUMENT,
} from '../../document-management/business-case/BusinessCase.model';
import BusinessCaseForm from '../../document-management/business-case/components/BusinessCaseForm';
import { fetchFundingRequestFormDataAction } from './funding-request-action';
import initiativeStore, { INITIATIVE_LIST_EVENT } from '../initiative/initiative-store';
import { fetchInitiativeList } from '../initiative/initiative-actions';
import FundingRequestDetailTable from './components/FundingRequestDetailTable';
import BusinessCaseDetailTable from '../../document-management/business-case/components/BusinessCaseDetailTable';
import { getCurrencyOnSession } from '../../../shared/alliance-utils';
import { ActionButton } from '../../../components/buttons/ActionButton';
import { FormSteps } from '../../../components/dots/FormSteps';
import { TransparentButton } from '../../../components/buttons/TransparentButton';
import { fundingRequestValidator } from './funding-request-validators';
import { businessCaseValidator } from '../../document-management/business-case/business-case-validators';
import { fetchRelatedItems } from '../../related-item/related-item-actions';
import { sanitizeNextStepsToEdit } from '../../../modules/next-step/next-step-actions';
import { fetchCurrentAllianceMembersAction } from '../../settings/alliance-management/alliance-actions';
import sessionStore, { NEW_SESSION_EVENT } from '../../../shared/SessionStore';
import { sanitizeRecommendedSolutionsToEdit } from '../../document-management/business-case/businessCases.actions';
import RelatedItemForm from '../../related-item/components/RelatedItemForm';
import relatedItemStore, { RELATED_ITEMS_EVENT } from '../../related-item/related-item-store';
import { getItemByType } from '../../../shared/items-util';
import { TopButtons } from '../../../components/buttons/TopButtons';
import { RelatedItemsDetailTable } from '../../related-item/components/RelatedItemsDetailTable';
import { initiativesItemValidator } from '../initiative/initiative-validators';
import { InitiativeListTable } from '../initiative/components/InitiativeListTable';
import { LeftProgressSection } from '../../../components/new-ui/LeftProgressSection';
import { SCREENS_FUNDING } from '../screenView';
import { BoxCard } from '../../../components/new-ui/div/BoxCard';
import { CardFooter } from '../../../components/new-ui/card/CardFooter';
import { FUNDING_REQUEST_COMPLETED } from '../../../shared/status';

class FundingRequestEditView extends View {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        fundingRequestData: R.clone(FundingRequestModel),
        businessCaseData: R.clone(BusinessCaseModel),
        relatedItems: [],
        initiatives: [],
      },
      loading: true,
      step: 0,
      initiativesList: [],
      clientCompany: null,
      partnerCompany: null,
      userRole: false,
    };
    this.onError = onErrorMixin.bind(this);
    this.onChangeData = onChangeDataMixin.bind(this);
    this.originalRecommendedSolutions = [];
    this.originalNextSteps = [];
    this.originalDocuments = [];
  }

  onChangeFundingRequestData = (name, value) => {
    const { data } = this.state;
    data.fundingRequestData[name] = value;
    this.setState({ data });
  };

  onChangeBusinessCaseData = (name, value) => {
    const { data } = this.state;
    data.businessCaseData[name] = value;
    this.setState({ data });
  };

  componentDidMount() {
    const { match } = this.props;
    if (!match.params.id) return toast.error('Funding Request ID missing');

    this.subscribe(fundingRequestStore, FUNDING_REQUEST_ERROR_EVENT, this.onError);
    this.subscribe(fundingRequestStore, FUNDING_REQUEST_FORM_DATA_EVENT, (state) => {
      this.setState({
        clientCompany: state.clientCompany,
        partnerCompany: state.partnerCompany,
      });
    });
    this.subscribe(fundingRequestStore, FUNDING_REQUEST_DETAIL_EVENT, (state) => {
      const { fundingRequest: fundingRequestData } = state;
      const businessCaseData = R.clone(fundingRequestData.businessCase);
      delete fundingRequestData.businessCase;

      this.originalRecommendedSolutions = sanitizeRecommendedSolutionsToEdit(businessCaseData);
      this.originalNextSteps = sanitizeNextStepsToEdit(fundingRequestData);
      fundingRequestData.documents = fundingRequestData.documents.items;
      fundingRequestData.initiatives = fundingRequestData.initiatives.items;

      localStorage.setItem(FUNDING_REQUEST_DOCUMENTS, JSON.stringify(fundingRequestData.documents));
      this.originalDocuments = fundingRequestData.documents.concat();

      const { data } = this.state;
      data.initiatives = fundingRequestData.initiatives;
      data.fundingRequestData = fundingRequestData;
      data.businessCaseData = businessCaseData;

      this.setState({ data }, () => {
        fetchRelatedItems(fundingRequestData.itemFundingRequestRelation.id);
      });
    });

    this.subscribe(relatedItemStore, RELATED_ITEMS_EVENT, (state) => {
      const items = state.item.itemsRelated.items.map((item) => getItemByType(item));
      const { data } = this.state;
      data.relatedItems = items;
      this.setState({ data, loading: false });
    });

    this.subscribe(fundingRequestStore, FUNDING_REQUEST_UPDATE_EVENT, () => {
      toast.success('Funding Request Successfully Updated');
      this.props.history.goBack();
    });

    this.subscribe(initiativeStore, INITIATIVE_LIST_EVENT, (state) => {
      const initiativesList = state.initiativesList.items;
      this.setState({
        initiativesList,
      });
    });

    // Apollo may be capable to make just one call
    fetchFundingRequestDetail(match.params.id);
    fetchInitiativeList('', 1, 1000);
    fetchFundingRequestFormDataAction();
    fetchCurrentAllianceMembersAction();
  }

  componentWillUnmount() {
    super.componentWillUnmount();
    localStorage.removeItem(FUNDING_REQUEST_DOCUMENTS);
    localStorage.removeItem(BUSINESS_CASE_DOCUMENT);
  }

  onSubmit = () => {
    this.setState({ loading: true }, () => {
      updateFundingRequest(
        R.clone(this.state.data.fundingRequestData),
        R.clone(this.state.data.businessCaseData),
        this.originalRecommendedSolutions,
        this.originalNextSteps,
        R.clone(this.state.data.relatedItems),
        R.clone(this.state.data.initiatives),
        this.originalDocuments,
      );
    });
  };

  onFundingRequestStepChange = (nextStep) => {
    const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
    const fundingRequestData = R.clone(this.state.data.fundingRequestData);
    try {
      fundingRequestValidator(fundingRequestData, selectedAlliance);
    } catch (e) {
      return this.onError(e);
    }
    this.onScreen(nextStep);
  };

  onBusinessCaseStepChange = (nextStep) => {
    const businessCaseData = R.clone(this.state.data.businessCaseData);
    try {
      businessCaseValidator(businessCaseData);
    } catch (e) {
      return this.onError(e);
    }
    this.onScreen(nextStep);
  };

  onRelatedItemsStepChange = (step) => {
    const initiatives = R.clone(this.state.data.initiatives);
    try {
      initiativesItemValidator(initiatives);
    } catch (e) {
      return this.onError(e);
    }
    this.onScreen(step);
  };

  onScreen = (step) => {
    console.log(`DEBUG:`, this.state);
    this.setState({ step });
  };

  render() {
    const {
      initiativesList,
      companyId,
      loading,
      step,
      clientCompany,
      partnerCompany,
      data,
    } = this.state;
    const { initiatives, businessCaseData, fundingRequestData, relatedItems } = data;
    const { user, selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
    const { history } = this.props;
    const currency = getCurrencyOnSession();
    let content = <Loader stretch />;
    let footer = <></>;
    let buttonsTop = '';
    if (fundingRequestData.status === FUNDING_REQUEST_COMPLETED)
      history.push(`/management/investment-item/`);
    if (!loading && step === 0) {
      content = (
        <FundingRequestForm
          data={this.state.data.fundingRequestData}
          onChange={this.onChangeFundingRequestData}
          myCompanyId={companyId}
          initiativesList={initiativesList}
          clientCompany={clientCompany}
          partnerCompany={partnerCompany}
          currency={currency}
          user={user}
          selectedAlliance={selectedAlliance}
        />
      );
      footer = (
        <CardFooter>
          <ActionButton onClick={() => this.onFundingRequestStepChange(1)} text={'Next'} />
        </CardFooter>
      );
    }

    if (!loading && step === 1) {
      content = (
        <BusinessCaseForm
          data={businessCaseData}
          onChange={this.onChangeBusinessCaseData}
          currency={currency}
        />
      );
      footer = (
        <CardFooter>
          <ActionButton onClick={() => this.onBusinessCaseStepChange(2)} text={'Next'} />
          <TransparentButton onClick={() => this.onScreen(0)} text={'Previous'} />
        </CardFooter>
      );
    }

    if (!loading && step === 2) {
      const itemData = { id: fundingRequestData.id, type: fundingRequestData.__typename };

      content = (
        <RelatedItemForm
          relatedItems={relatedItems}
          initiatives={initiativesList}
          onChange={(key, value) => {
            this.onChangeData(key, value);
          }}
          selectedInitiatives={initiatives}
          itemData={itemData}
        />
      );

      footer = (
        <CardFooter>
          <ActionButton onClick={() => this.onRelatedItemsStepChange(3)} text={'Next'} />
          <TransparentButton onClick={() => this.onScreen(1)} text={'Previous'} />
        </CardFooter>
      );
    }

    if (!loading && step === 3) {
      const fundingRequestDataDetail = R.clone(fundingRequestData);
      fundingRequestDataDetail.documents = { items: fundingRequestDataDetail.documents };
      fundingRequestDataDetail.nextSteps = { items: fundingRequestDataDetail.nextSteps };

      content = (
        <>
          <FundingRequestDetailTable data={fundingRequestDataDetail} currency={currency} />
          <BusinessCaseDetailTable data={businessCaseData} currency={currency} />
          <InitiativeListTable initiatives={initiatives} />
          <RelatedItemsDetailTable relatedItems={relatedItems} />
        </>
      );
      footer = (
        <CardFooter>
          <ActionButton onClick={this.onSubmit} text={'Update Funding Request'} />
          <TransparentButton onClick={() => this.onScreen(2)} text={'Previous'} />
        </CardFooter>
      );
    }

    if (!loading)
      buttonsTop = (
        <TopButtons
          onClickClosed={() => this.props.history.goBack()}
          onClickCollaborated={() => openComments(this.state.data.fundingRequestData)}
        />
      );

    return (
      <React.Fragment>
        <Card.Header>
          <Grid.Layout
            columns="200px auto 200px"
            areas={[['left', 'center', 'right']]}
            style={{ width: '100%' }}>
            <Grid.Box area="left">
              <Heading type="h4" text="Edit Funding Request" />
            </Grid.Box>
            <Grid.Box area="center">
              <FormSteps totalSteps={4} step={this.state.step} />
            </Grid.Box>
            <Grid.Box area="right" />
          </Grid.Layout>
          {buttonsTop}
        </Card.Header>
        <CreateViewCardBody>
          <Grid.Layout columns="30% 70%" areas={[['left', 'right']]} style={{ width: '100%' }}>
            <Grid.Box area="left">
              <LeftProgressSection sections={SCREENS_FUNDING} currentScreen={step} />
            </Grid.Box>
            <BoxCard>
              <Grid.Box area="right">{content}</Grid.Box>
            </BoxCard>
          </Grid.Layout>
        </CreateViewCardBody>
        {footer}
      </React.Fragment>
    );
  }
}

FundingRequestEditView.propTypes = {
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default withRouter(FundingRequestEditView);

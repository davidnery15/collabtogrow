import React from 'react';
import PropTypes from 'prop-types';
import DetailValue from '../../../../components/DetailValue';
import DetailDateValue from '../../../../components/DetailDateValue';
import { CurrencyTextField } from '../../../../shared/components/CurrencyTextField';
import NextStepsDetail from '../../../../modules/next-step/components/NextStepsDetail';
import UserDetailValue from '../../../../components/UserDetailValue';
import SourceDetailValue from '../../../../components/SourceDetailValue';
import DocumentsFileComponent from '../../../../components/inputs/DocumentsFileComponent';
import { FUNDING_REQUEST_DOCUMENTS } from '../funding-request-model';
import { calculateValueBigInt } from '../../../../shared/utils';
import { HorizontalLine } from '../../../../components/new-ui/text/HorizontalLine';
import { HorizontalLineText } from '../../../../components/text/HorizontalLineText';
import { BoderDetailView } from '../../../../components/new-ui/div/BorderDetailView';
import { Grid } from '@8base/boost';
import { TablePosition } from '../../../../components/new-ui/div/TablePosition';
import { TableDetail } from '../../../../components/new-ui/table/TableDetail';
import { HeaderText } from '../../../../components/new-ui/text/HeaderText';
import { EditButton } from '../../../../components/new-ui/buttons/EditButton';
import { canEditFundingRequest } from '../funding-request-permissions';
import sessionStore, { NEW_SESSION_EVENT } from '../../../../shared/SessionStore';
import { ThTitlePosition } from '../../../../components/new-ui/div/ThTitlePosition';

/**
 * Detail View Table For The Funding Request Detail Table.
 *
 * @param props
 */
const FundingRequestDetailTable = (props) => {
  const { data, currency, onClickEdit } = props;
  const {
    status,
    name,
    requestedBy,
    revisedDueDate,
    description,
    documents,
    assignedTo,
    nextSteps,
    assignedDate,
    originalDueDate,
    budgetUtilized,
    unitType,
    unitQuantity,
    unitValueDescription,
    unitMonetizationFactor,
    source,
  } = data;

  const { user, selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);

  return (
    <>
      <HorizontalLine>
        <HorizontalLineText>FUNDING REQUEST</HorizontalLineText>
      </HorizontalLine>
      <>
        <BoderDetailView>
          <Grid.Layout
            columns="auto 100px"
            areas={[['left', 'right']]}
            style={{ width: '100%', height: '100%' }}>
            <Grid.Box area="left">
              <div style={{ position: 'absolute', top: '21px', left: '25px' }}>
                <HeaderText>FUNDING REQUEST</HeaderText>
              </div>
            </Grid.Box>
            <Grid.Box area="right" justifyContent={'center'}>
              {canEditFundingRequest(user, data, selectedAlliance) ? (
                <EditButton onClick={onClickEdit} text="Edit" />
              ) : null}
            </Grid.Box>
          </Grid.Layout>
        </BoderDetailView>
        <TablePosition>
          <TableDetail>
            <tbody>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>STATE</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <span> {status} </span>
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>NEXT STEPS</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <NextStepsDetail nextSteps={nextSteps} assignedTo={assignedTo} />
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>NAME</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <DetailValue text={name} />
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>DESCRIPTION</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <DetailValue text={description} />
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>ASSIGNED TO</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <UserDetailValue user={assignedTo} />
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>ASSIGNED DATE</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <DetailDateValue date={assignedDate} />
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>ORIGINAL DUE DATE</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <DetailDateValue date={originalDueDate} />
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>REQUESTED BY</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <UserDetailValue user={requestedBy} />
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>REVISED DUE DATE</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <DetailDateValue date={revisedDueDate} />
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>SOURCE</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <SourceDetailValue source={source} />
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>BUDGET UTILIZED</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <CurrencyTextField value={budgetUtilized} currency={currency} />
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>UNIT TYPE</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <DetailValue text={unitType} />
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>UNIT VALUE</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <DetailValue text={unitQuantity} />
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>UNIT VALUE DESCRIPTION</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <DetailValue text={unitValueDescription} />
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>UNIT MONETIZACION FACTOR</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <CurrencyTextField value={unitMonetizationFactor} currency={currency} />
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>CALCULATED VALUE</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <CurrencyTextField
                    value={calculateValueBigInt(unitQuantity, unitMonetizationFactor)}
                  />
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>DOCUMENTS</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <DocumentsFileComponent data={documents} localKey={FUNDING_REQUEST_DOCUMENTS} />
                </td>
              </tr>
            </tbody>
          </TableDetail>
        </TablePosition>
      </>
    </>
  );
};

FundingRequestDetailTable.defaultProps = {
  onClickEdit: null, // null for form previews
};

FundingRequestDetailTable.propTypes = {
  data: PropTypes.object.isRequired,
  currency: PropTypes.object.isRequired,
  onClickEdit: PropTypes.func,
};

export default FundingRequestDetailTable;

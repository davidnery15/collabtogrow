import React from 'react';
import PropTypes from 'prop-types';
import DetailValue from '../../../../components/DetailValue';
import DetailDateValue from '../../../../components/DetailDateValue';
import { CurrencyTextField } from '../../../../shared/components/CurrencyTextField';
import NextStepsDetail from '../../../../modules/next-step/components/NextStepsDetail';
import UserDetailValue from '../../../../components/UserDetailValue';
import DocumentsFileComponent from '../../../../components/inputs/DocumentsFileComponent';
import { RISK_DOCUMENTS } from '../risk-model';
import { calculateValueBigInt } from '../../../../shared/utils';
import { HorizontalLine } from '../../../../components/new-ui/text/HorizontalLine';
import { HorizontalLineText } from '../../../../components/text/HorizontalLineText';
import { BoderDetailView } from '../../../../components/new-ui/div/BorderDetailView';
import { Grid } from '@8base/boost';
import { TablePosition } from '../../../../components/new-ui/div/TablePosition';
import { TableDetail } from '../../../../components/new-ui/table/TableDetail';
import { HeaderText } from '../../../../components/new-ui/text/HeaderText';
import { EditButton } from '../../../../components/new-ui/buttons/EditButton';
import { canEditRisk } from '../risk-permissions';
import sessionStore, { NEW_SESSION_EVENT } from '../../../../shared/SessionStore';
import { ThTitlePosition } from '../../../../components/new-ui/div/ThTitlePosition';

/**
 * Detail View Table For The Risk Detail Table
 */
const RiskDetailTable = (props) => {
  const { data, currency, onClickEdit } = props;
  const {
    name,
    description,
    status,
    assignedDate,
    nextSteps,
    assignedTo,
    originalDueDate,
    documents,
    createdAt,
    unitType,
    unitQuantity,
    unitValueDescription,
    unitMonetizationFactor,
  } = data;
  const { user, selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
  return (
    <>
      <HorizontalLine>
        <HorizontalLineText>RISK</HorizontalLineText>
      </HorizontalLine>
      <>
        <BoderDetailView>
          <Grid.Layout
            columns="auto 100px"
            areas={[['left', 'right']]}
            style={{ width: '100%', height: '100%' }}>
            <Grid.Box area="left">
              <div style={{ position: 'absolute', top: '21px', left: '25px' }}>
                <HeaderText>RISK</HeaderText>
              </div>
            </Grid.Box>
            <Grid.Box area="right" justifyContent={'center'}>
              {canEditRisk(user, data, selectedAlliance) ? (
                <EditButton onClick={onClickEdit} text="Edit" />
              ) : null}
            </Grid.Box>
          </Grid.Layout>
        </BoderDetailView>
        <TablePosition>
          <TableDetail>
            <tbody>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>STATE</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <span> {status} </span>
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>NEXT STEPS</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <NextStepsDetail nextSteps={nextSteps} assignedTo={assignedTo} />
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>NAME</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <DetailValue text={name} />
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>DESCRIPTION</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <DetailValue text={description} />
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>DOCUMENTS</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <DocumentsFileComponent data={documents} localKey={RISK_DOCUMENTS} />
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>ORIGINAL DUE DATE</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <DetailDateValue date={originalDueDate} />
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>CREATED AT</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <DetailDateValue date={createdAt} />
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>ASSIGNED TO</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <UserDetailValue user={assignedTo} />
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>ASSIGNED DATE</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <DetailDateValue date={assignedDate} />
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>UNIT TYPE</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <DetailValue text={unitType} />
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>UNIT QUANTITY</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <DetailValue text={unitQuantity} />
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>UNIT VALUE DESCRIPTION</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <DetailValue text={unitValueDescription} />
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>UNIT MONETIZATION FACTOR</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <CurrencyTextField value={unitMonetizationFactor} currency={currency} />
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>CALCULATED VALUE</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <CurrencyTextField
                    value={calculateValueBigInt(unitQuantity, unitMonetizationFactor)}
                  />
                </td>
              </tr>
            </tbody>
          </TableDetail>
        </TablePosition>
      </>
    </>
  );
};

RiskDetailTable.defaultProps = {
  onClickEdit: null, // null for form previews
};

RiskDetailTable.propTypes = {
  data: PropTypes.object.isRequired,
  currency: PropTypes.object.isRequired,
  onClickEdit: PropTypes.func,
};

export default RiskDetailTable;

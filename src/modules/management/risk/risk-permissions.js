import { RISK_CLOSED, RISK_COMPLETED } from '../../../shared/status';
import {
  isUserAdminOrSERInAlliance,
  isUserCreatorInAlliance,
} from '../../../shared/alliance-utils';
import { isAllianceCompleted } from '../../settings/alliance-management/alliance-permissions';

/**
 * Checks if a User can Edit a Risk.
 *
 * @param {object}user - User.
 * @param {object}risk - Risks.
 * @param {object}alliance - Alliance.
 * @returns {boolean} Validate options.
 */
export const canEditRisk = (user, risk, alliance): boolean => {
  if (isAllianceCompleted(alliance)) return false;
  if (risk.status === RISK_CLOSED) return false;
  if (risk.status === RISK_COMPLETED) return false;
  if (isUserAdminOrSERInAlliance(user, alliance)) return true;
  if (isUserCreatorInAlliance(user, alliance)) return true;

  return false;
};

/**
 * Checks if a User can Complete a Risk.
 *
 * @param {object}user - User.
 * @param {object}risk - Risks.
 * @param {object}alliance - Alliance.
 * @returns {boolean} Validate options.
 */
export const canCompletedRisk = (user, risk, alliance) => {
  if (isAllianceCompleted(alliance)) return false;

  if (risk.status === RISK_COMPLETED) return false;

  if (risk.assignedTo && risk.assignedTo.id === user.id) return true;

  return isUserAdminOrSERInAlliance(user, alliance);
};

/**
 * Checks if a User can restore a Risk.
 *
 * @param {object}user - User.
 * @param {object}risk - Risks.
 * @param {object}alliance - Alliance.
 * @returns {boolean} Validate options.
 */
export const canRestoreRisk = (user, risk, alliance) => {
  if (isAllianceCompleted(alliance)) return false;

  if (risk.status !== RISK_COMPLETED) return false;

  if (risk.assignedTo && risk.assignedTo.id === user.id) return true;

  return isUserAdminOrSERInAlliance(user, alliance);
};

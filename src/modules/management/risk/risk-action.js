import sessionStore, { APOLLO_CLIENT, NEW_SESSION_EVENT } from '../../../shared/SessionStore';
import {
  sanitize8BaseDocumentsCreate,
  sanitize8BaseReferenceFromObject,
  sanitize8BaseReferencesFromObjects,
  sanitize8BaseReconnectsFromObjects,
  sanitize8BaseDocumentsDeleteAndUpdate,
  sanitize8BaseBigInt,
} from '../../../shared/utils';
import Flux from '@cobuildlab/flux-state';
import { IntegrityError } from '../../../shared/errors';
import { validateRiskData } from './risk-validators';
import { error, log } from '@cobuildlab/pure-logger';
import {
  RISK_CREATE_EVENT,
  RISK_ERROR_EVENT,
  RISK_DETAIL_EVENT,
  RISK_UPDATE_EVENT,
  RISK_COMPLETED_EVENT,
  RISK_RESTORE_EVENT,
} from './risk-store';
import {
  RISK_COMMENTS_QUERY,
  RISK_CREATE_MUTATION,
  RISK_DETAIL_QUERY,
  RISK_UPDATE_MUTATION,
} from './risk-queries';
import {
  COMMENT_CREATE_EVENT,
  COMMENT_ERROR_EVENT,
  COMMENT_REQUEST_EVENT,
  COMMENTS_EVENT,
} from '../../comment/comment-store';
import { RISK_TYPE } from '../../../shared/item-types';
import { COMMENTS_CREATE_MUTATION } from '../../comment/comment-queries';
import { RISK_COMPLETED, RISK_OPEN } from '../../../shared/status';
import { RELATED_ITEM_UPDATE_MUTATION } from '../../related-item/related-item-queries';
import { canCompletedRisk, canRestoreRisk } from './risk-permissions';
import {
  sanitizeNextStepsCreate,
  nextStepsToBeCreated,
  updateNextSteps,
  deleteNextSteps,
  completeItemsNextSteps,
  restoreItemsNextSteps,
} from 'modules/next-step/next-step-actions.js';
import * as R from 'ramda';

/**
 * Notifies a Request for Comments for a Risk.
 */
export const openComments = ({ id }) => {
  Flux.dispatchEvent(COMMENT_REQUEST_EVENT, { type: RISK_TYPE, id: id });
};

/**
 * Create a Risk.
 *
 * @param {object}risk - Risks.
 * @param {Array}relatedItems - RelatedItems.
 * @param {object}initiatives - Initiatives.
 * @returns {Promise<void>} Return promise.
 */
export const createRisk = async (risk, relatedItems, initiatives) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
  const allianceId = selectedAlliance.id;

  delete risk.createdAt;
  delete risk.status;
  delete risk.id;

  if (!allianceId)
    return Flux.dispatchEvent(RISK_ERROR_EVENT, new IntegrityError('Must have an Active Alliance'));

  log('RiskData: ', risk, relatedItems, initiatives);
  try {
    validateRiskData(risk, initiatives, selectedAlliance);
  } catch (err) {
    error('createRisk', err);
    return Flux.dispatchEvent(RISK_ERROR_EVENT, err);
  }

  // Transforming into 8base 'connect' relationships
  risk.initiatives = initiatives;
  sanitize8BaseDocumentsCreate(risk, 'documents');
  sanitize8BaseReferencesFromObjects(risk, 'initiatives');
  sanitize8BaseReferenceFromObject(risk, 'assignedTo');
  sanitize8BaseReferenceFromObject(risk, 'source');
  sanitize8BaseBigInt(risk, 'unitMonetizationFactor');
  sanitizeNextStepsCreate(risk);

  risk.itemRiskRelation = {
    create: {
      alliance: { connect: { id: allianceId } },
      itemsRelated: {
        connect: relatedItems.map((item) => {
          return { id: item.itemId };
        }),
      },
    },
  };

  let result;
  log('createRisk', risk);
  try {
    result = await client.mutate({
      mutation: RISK_CREATE_MUTATION,
      variables: { data: risk },
    });
  } catch (e) {
    error('createRisk', e);
    return Flux.dispatchEvent(RISK_ERROR_EVENT, e);
  }
  log('createRisk', result);

  return Flux.dispatchEvent(RISK_CREATE_EVENT, result);
};

/**
 * Create a comment on a Risk.
 *
 * @param {string}riskId - Risk Id.
 * @param {Array}comment - Comment.
 * @returns {Promise<*>} Return promise.
 */
export const createRiskComment = async (riskId, comment) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const data = {
    comment,
    riskCommentsRelation: { connect: { id: riskId } },
  };

  let response;
  try {
    response = await client.mutate({
      mutation: COMMENTS_CREATE_MUTATION,
      variables: { data },
    });
  } catch (e) {
    error('createRiskComment', e);
    return Flux.dispatchEvent(COMMENT_ERROR_EVENT, e);
  }
  log('createRiskComment', response);
  Flux.dispatchEvent(COMMENT_CREATE_EVENT, response.data);
  return response.data;
};

/**
 * Fetches the Risk Item Comments.
 *
 * @param {string}riskId - Risk Id.
 * @returns {Promise<void>} Return promise.
 */
export const fetchRiskComments = async (riskId) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const data = { id: riskId };
  const { user } = sessionStore.getState(NEW_SESSION_EVENT);
  let response;
  try {
    response = await client.query({
      query: RISK_COMMENTS_QUERY,
      variables: data,
      fetchPolicy: 'network-only',
    });
  } catch (e) {
    error('fetchRiskComments', e);
    return Flux.dispatchEvent(COMMENT_ERROR_EVENT, e);
  }
  log('fetchRiskComments', response);

  response.data.risk.comments.userId = user.id;
  Flux.dispatchEvent(COMMENTS_EVENT, response.data.risk.comments);
  return response.data.risk.comments;
};

/**
 * Fetches the Risk Item.
 *
 * @param {string}riskId - Risk Id.
 * @returns {Promise<void>} Return promise.
 */
export const fetchRisk = async (riskId) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const data = { id: riskId };

  let response;
  try {
    response = await client.query({
      query: RISK_DETAIL_QUERY,
      variables: data,
      fetchPolicy: 'network-only',
    });
  } catch (e) {
    error('fetchRisk', e);
    throw e;
  }
  log('fetchRisk', response);
  return response.data;
};

/**
 * Fetches the Risk Item and dispatchEvent.
 *
 * @param {string} riskId - Risk Id.
 * @returns {Promise<void>} Return promise.
 */
export const fetchRiskDetail = async (riskId) => {
  let response;
  try {
    response = await fetchRisk(riskId);
  } catch (e) {
    return Flux.dispatchEvent(RISK_ERROR_EVENT, e);
  }

  Flux.dispatchEvent(RISK_DETAIL_EVENT, response);
  return response;
};

/**
 * Update a Risk.
 *
 * @param {object} riskData - Risk.
 * @param {Array}relatedItems - RelatedItems.
 * @param {object}initiatives - Initiatives.
 * @param {Array}originalNextSteps - OriginalNextSteps.
 * @param {Array}originalDocuments - OriginalDocuments.
 * @returns {Promise<void|*>} Return promise.
 */
export const updateRisk = async (
  riskData,
  relatedItems,
  initiatives,
  originalNextSteps,
  originalDocuments,
) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const itemId = riskData.itemRiskRelation.id;
  const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);

  delete riskData.itemRiskRelation;
  delete riskData.__typename;
  delete riskData.createdAt;
  delete riskData.createdBy;
  delete riskData.itemId;

  log('riskUpdate', riskData, relatedItems, initiatives, originalNextSteps);
  try {
    validateRiskData(riskData, initiatives, selectedAlliance);
  } catch (e) {
    error('Update Risk Validator', e);
    return Flux.dispatchEvent(RISK_ERROR_EVENT, e);
  }

  riskData.initiatives = initiatives;
  sanitize8BaseReconnectsFromObjects(riskData, 'initiatives');
  sanitize8BaseDocumentsDeleteAndUpdate(riskData, 'documents', originalDocuments);
  sanitize8BaseReferenceFromObject(riskData, 'assignedTo');
  sanitize8BaseBigInt(riskData, 'unitMonetizationFactor');
  sanitize8BaseReferenceFromObject(riskData, 'source');

  // nextSteps to be created
  const nextSteps = R.clone(riskData.nextSteps);
  const toBeCreated = nextStepsToBeCreated(nextSteps);
  riskData.nextSteps = toBeCreated;
  sanitizeNextStepsCreate(riskData);

  // update and delete nextSteps
  await deleteNextSteps(nextSteps, originalNextSteps);
  await updateNextSteps(nextSteps, originalNextSteps);

  let response;
  try {
    response = await client.mutate({
      mutation: RISK_UPDATE_MUTATION,
      variables: { data: riskData },
    });
  } catch (e) {
    error('updateRisk', e, riskData);
    return Flux.dispatchEvent(RISK_ERROR_EVENT, e);
  }

  // Reconnect related items
  const relatedItemsData = {
    id: itemId,
    itemsRelated: {
      reconnect: relatedItems.map((i) => {
        return { id: i.itemId };
      }),
    },
  };

  log('risktData:update:relatedItems:', relatedItemsData);
  try {
    await client.mutate({
      mutation: RELATED_ITEM_UPDATE_MUTATION,
      variables: { data: relatedItemsData },
    });
  } catch (e) {
    error('updateRisk:updateRelated Items', e, riskData);
    return Flux.dispatchEvent(RISK_ERROR_EVENT, e);
  }

  Flux.dispatchEvent(RISK_UPDATE_EVENT, response.data);
  return response.data;
};

/**
 * Change item status the open to completed.
 *
 * @param {object} riskData - Risk.
 * @returns {Promise<void|*>} Return promise.
 */
export const completedRisk = async (riskData) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { selectedAlliance, user } = sessionStore.getState(NEW_SESSION_EVENT);
  const { risk } = await fetchRisk(riskData.id);

  if (!canCompletedRisk(user, risk, selectedAlliance))
    return Flux.dispatchEvent(
      RISK_ERROR_EVENT,
      new IntegrityError('You do not have permission to perform this action'),
    );

  let response;
  try {
    response = await client.mutate({
      mutation: RISK_UPDATE_MUTATION,
      variables: {
        data: {
          id: risk.id,
          status: RISK_COMPLETED,
        },
      },
    });
  } catch (e) {
    console.error('completedRisk', e, risk);
    return Flux.dispatchEvent(RISK_ERROR_EVENT, e);
  }

  try {
    await completeItemsNextSteps(risk);
  } catch (e) {
    log('completeItemsNextStepsError', e);
  }

  Flux.dispatchEvent(RISK_COMPLETED_EVENT, response.data);
  return response.data;
};

/**
 * Restore risk item, change status the completed to open.
 *
 * @param {object} riskData - Risk.
 * @returns {Promise<void|*>} Return Promise.
 */
export const restoreRisk = async (riskData) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { selectedAlliance, user } = sessionStore.getState(NEW_SESSION_EVENT);
  const { risk } = await fetchRisk(riskData.id);

  if (!canRestoreRisk(user, risk, selectedAlliance))
    return Flux.dispatchEvent(
      RISK_ERROR_EVENT,
      new IntegrityError('You do not have permission to perform this action'),
    );

  let response;
  try {
    response = await client.mutate({
      mutation: RISK_UPDATE_MUTATION,
      variables: {
        data: {
          id: risk.id,
          status: RISK_OPEN,
        },
      },
    });
  } catch (e) {
    console.error('restoredRisk', e, risk);
    return Flux.dispatchEvent(RISK_ERROR_EVENT, e);
  }

  Flux.dispatchEvent(RISK_RESTORE_EVENT, response.data);
  return response.data;
};

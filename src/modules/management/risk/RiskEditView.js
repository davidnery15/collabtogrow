import React from 'react';
import { Card, Heading, Loader, Grid } from '@8base/boost';
import { CreateViewCardBody } from '../../../components/new-ui/card/CreateViewCardBody';
import riskStore, { RISK_DETAIL_EVENT, RISK_ERROR_EVENT, RISK_UPDATE_EVENT } from './risk-store';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import * as toast from 'components/toast/Toast';
import * as R from 'ramda';
import View from '@cobuildlab/react-flux-state';
import { RISK_DOCUMENTS, RiskModel } from './risk-model';
import RiskForm from './components/RiskForm';
import { openComments, updateRisk } from './risk-action';
import { fetchRiskDetail } from './risk-action';
import initiativeStore, { INITIATIVE_LIST_EVENT } from '../initiative/initiative-store';
import { fetchInitiativeList } from '../initiative/initiative-actions';
import allianceStore, {
  ALLIANCE_LIST_MEMBERS_EVENT,
} from '../../settings/alliance-management/alliance-store';
import { fetchCurrentAllianceMembersAction } from '../../settings/alliance-management/alliance-actions';
import { getCurrencyOnSession } from '../../../shared/alliance-utils';
import { ActionButton } from '../../../components/buttons/ActionButton';
import { TopButtons } from '../../../components/buttons/TopButtons';
import { sanitizeNextStepsToEdit } from 'modules/next-step/next-step-actions';
import sessionStore, { NEW_SESSION_EVENT } from '../../../shared/SessionStore';
import { riskValidator } from './risk-validators';
import { onChangeDataMixin, onErrorMixin } from '../../../shared/mixins';
import { initiativesItemValidator } from '../initiative/initiative-validators';
import { TransparentButton } from '../../../components/buttons/TransparentButton';
import RelatedItemForm from '../../related-item/components/RelatedItemForm';
import RiskDetailTable from './components/RiskDetailTable';
import { RelatedItemsDetailTable } from '../../related-item/components/RelatedItemsDetailTable';
import { fetchRelatedItems } from '../../related-item/related-item-actions';
import relatedItemStore, {
  RELATED_ITEMS_EVENT,
  RELATED_ITEM_ERROR_EVENT,
} from '../../related-item/related-item-store';
import { FormSteps } from '../../../components/dots/FormSteps';
import { getItemByType } from '../../../shared/items-util';
import { InitiativeListTable } from '../initiative/components/InitiativeListTable';
import { BUSINESS_CASE_DOCUMENT } from '../../document-management/business-case/BusinessCase.model';
import { LeftProgressSection } from '../../../components/new-ui/LeftProgressSection';
import { SCREENS_RISK } from '../screenView';
import { BoxCard } from '../../../components/new-ui/div/BoxCard';
import { RISK_COMPLETED } from '../../../shared/status';

class RiskEditView extends View {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        riskData: R.clone(RiskModel),
        relatedItems: [],
        initiativesList: [],
      },
      step: 0,
      loading: true,
      clientCompany: null,
      partnerCompany: null,
      initiativesList: [],
    };
    this.onError = onErrorMixin.bind(this);
    this.onChangeData = onChangeDataMixin.bind(this);
    this.originalNextSteps = [];
    this.originalDocuments = [];
  }

  onChangeRiskData = (name, value) => {
    const { data } = this.state;
    data.riskData[name] = value;
    this.setState({ data });
  };

  componentDidMount() {
    const { match } = this.props;
    if (!match.params.id) return toast.error('Risk ID missing');

    this.subscribe(riskStore, RISK_ERROR_EVENT, this.onError);
    this.subscribe(relatedItemStore, RELATED_ITEM_ERROR_EVENT, this.onError);

    this.subscribe(allianceStore, ALLIANCE_LIST_MEMBERS_EVENT, (state) => {
      this.setState({
        clientCompany: state.clientCompany,
        partnerCompany: state.partnerCompany,
      });
    });
    this.subscribe(riskStore, RISK_DETAIL_EVENT, (state) => {
      const { risk } = state;
      const { data } = this.state;
      const riskData = R.clone(risk);
      const initiatives = riskData.initiatives.items;

      riskData.documents = riskData.documents.items;
      this.originalDocuments = riskData.documents.concat();
      this.originalNextSteps = sanitizeNextStepsToEdit(riskData);
      localStorage.setItem(RISK_DOCUMENTS, JSON.stringify(riskData.documents));

      data.riskData = riskData;
      data.initiatives = initiatives;

      this.setState({ data }, () => fetchRelatedItems(riskData.itemRiskRelation.id));
    });
    this.subscribe(relatedItemStore, RELATED_ITEMS_EVENT, (state) => {
      const items = state.item.itemsRelated.items.map((item) => getItemByType(item));
      const { data } = this.state;

      data.relatedItems = items;

      this.setState({ data, loading: false });
    });
    this.subscribe(riskStore, RISK_UPDATE_EVENT, () => {
      toast.success('Risk Successfully Updated');
      this.props.history.goBack();
    });
    this.subscribe(initiativeStore, INITIATIVE_LIST_EVENT, (state) => {
      this.setState({
        initiativesList: state.initiativesList.items,
      });
    });

    fetchRiskDetail(match.params.id);
    fetchInitiativeList('', 1, 1000);
    fetchCurrentAllianceMembersAction();
  }

  componentWillUnmount() {
    super.componentWillUnmount();
    localStorage.removeItem(RISK_DOCUMENTS);
    localStorage.removeItem(BUSINESS_CASE_DOCUMENT);
  }

  onRiskStepChange = (step) => {
    const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
    const riskData = R.clone(this.state.data.riskData);
    try {
      riskValidator(riskData, selectedAlliance);
    } catch (e) {
      return this.onError(e);
    }
    this.onScreen(step);
  };

  onRelatedItemsStepChange = (step) => {
    const initiatives = R.clone(this.state.data.initiatives);
    try {
      initiativesItemValidator(initiatives);
    } catch (e) {
      return this.onError(e);
    }
    this.onScreen(step);
  };

  onScreen = (step) => {
    this.setState({ step });
  };

  onSubmit = () => {
    this.setState({ loading: true }, () => {
      const riskData = R.clone(this.state.data.riskData);
      const relatedItems = R.clone(this.state.data.relatedItems);
      const initiatives = R.clone(this.state.data.initiatives);
      updateRisk(
        riskData,
        relatedItems,
        initiatives,
        this.originalNextSteps,
        this.originalDocuments,
      );
    });
  };

  render() {
    const { initiativesList, loading, clientCompany, partnerCompany, data, step } = this.state;
    const { riskData, initiatives, relatedItems } = data;
    const { history } = this.props;
    const { user, selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);

    let content = <Loader stretch />;
    let footer = <></>;
    let buttonsTop = '';
    const currency = getCurrencyOnSession();
    if (riskData.status === RISK_COMPLETED) history.push(`/management/amo-item`);

    if (!loading && step === 0) {
      content = (
        <RiskForm
          data={riskData}
          onChange={this.onChangeRiskData}
          clientCompany={clientCompany}
          partnerCompany={partnerCompany}
          currency={currency}
          user={user}
          selectedAlliance={selectedAlliance}
        />
      );
      footer = (
        <Card.Footer>
          <ActionButton onClick={() => this.onRiskStepChange(1)} text={'Next'} />
        </Card.Footer>
      );
    }

    if (!loading && step === 1) {
      const itemData = { id: riskData.id, type: riskData.__typename };

      content = (
        <RelatedItemForm
          relatedItems={relatedItems}
          selectedInitiatives={initiatives}
          initiatives={initiativesList}
          onChange={(key, value) => {
            this.onChangeData(key, value);
          }}
          itemData={itemData}
        />
      );

      footer = (
        <Card.Footer>
          <ActionButton onClick={() => this.onRelatedItemsStepChange(2)} text={'Next'} />
          <TransparentButton onClick={() => this.onScreen(0)} text={'Previous'} />
        </Card.Footer>
      );
    }

    if (!loading && step === 2) {
      const riskDetail = R.clone(riskData);
      riskDetail.documents = { items: riskDetail.documents };
      riskDetail.initiatives = { items: riskDetail.initiatives };
      riskDetail.nextSteps = { items: riskDetail.nextSteps };

      content = (
        <>
          <RiskDetailTable data={riskDetail} currency={currency} />
          <InitiativeListTable initiatives={initiatives} />
          <RelatedItemsDetailTable relatedItems={relatedItems} />
        </>
      );

      footer = (
        <Card.Footer>
          <ActionButton onClick={this.onSubmit} text={'Update Risk'} />
          <TransparentButton onClick={() => this.onScreen(1)} text={'Previous'} />
        </Card.Footer>
      );
    }

    if (!loading)
      buttonsTop = (
        <TopButtons
          onClickClosed={history.goBack}
          onClickCollaborated={() => openComments(riskData)}
        />
      );

    return (
      <React.Fragment>
        <Card.Header>
          <Grid.Layout
            columns="200px auto 200px"
            areas={[['left', 'center', 'right']]}
            style={{ width: '100%' }}>
            <Grid.Box area="left">
              <Heading type="h4" text="Edit Risk" />
            </Grid.Box>
            <Grid.Box area="center">
              <FormSteps totalSteps={3} step={step} />
            </Grid.Box>
            <Grid.Box area="right" />
          </Grid.Layout>
          {buttonsTop}
        </Card.Header>
        <CreateViewCardBody>
          <Grid.Layout columns="30% 70%" areas={[['left', 'right']]} style={{ width: '100%' }}>
            <Grid.Box area="left">
              <LeftProgressSection sections={SCREENS_RISK} currentScreen={step} />
            </Grid.Box>
            <BoxCard>
              <Grid.Box area="right">{content}</Grid.Box>
            </BoxCard>
          </Grid.Layout>
        </CreateViewCardBody>
        {footer}
      </React.Fragment>
    );
  }
}

RiskEditView.propTypes = {
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default withRouter(RiskEditView);

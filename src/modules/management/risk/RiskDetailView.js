import React from 'react';
import { Card, Heading, Loader, Row } from '@8base/boost';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import * as toast from 'components/toast/Toast';
import * as R from 'ramda';
import View from '@cobuildlab/react-flux-state';
import { onErrorMixin } from '../../../shared/mixins';
import { Risk } from '@cobuildlab/collabtogrow-models';
import { fetchRiskDetail, completedRisk, restoreRisk } from './risk-action';
import sessionStore, { NEW_SESSION_EVENT } from '../../../shared/SessionStore';
import { onChangeMixin } from '../../../shared/mixins';
import withAlliance from '../../../components/hoc/withAlliance';
import RiskDetailTable from './components/RiskDetailTable';
import { openComments } from './risk-action';
import riskStore, {
  RISK_ERROR_EVENT,
  RISK_DETAIL_EVENT,
  RISK_COMPLETED_EVENT,
  RISK_RESTORE_EVENT,
} from './risk-store';
import { canCompletedRisk, canRestoreRisk } from './risk-permissions';
import YesNoDialog from '../../../components/dialogs/YesNoDialog';
import { DetailViewCardBody } from '../../../components/card/DetailViewCardBody';
import { RelatedItemsDetailTable } from '../../related-item/components/RelatedItemsDetailTable';
import { RelatedItemsByItemDetailTable } from '../../related-item/components/RelatedItemsByItemDetailTable';
import relatedItemStore, {
  RELATED_ITEMS_EVENT,
  RELATED_ITEM_ERROR_EVENT,
  RELATED_ITEMS_BY_ITEM_EVENT,
} from '../../related-item/related-item-store';
import { getItemByType } from '../../../shared/items-util';
import { TopButtons } from '../../../components/buttons/TopButtons';
import { ActionButton } from '../../../components/buttons/ActionButton';
import {
  fetchRelatedItems,
  fetchRelatedItemsByItemId,
} from '../../related-item/related-item-actions';
import { getCurrencyOnSession } from '../../../shared/alliance-utils';
import { RISK_DOCUMENTS } from './risk-model';
import { InitiativeListTable } from '../initiative/components/InitiativeListTable';

/**
 * Funding Request Detail View.
 */
class RiskDetailView extends View {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        riskData: Risk.clone(),
        relatedItems: [],
        initiatives: [],
        relatedItemsByItem: [],
      },
      fundingRequest: null,
      loading: true,
      approvalModalIsOpen: false,
      completedModalIsOpen: false,
      restoreModalIsOpen: false,
    };
    this.onChange = onChangeMixin.bind(this);
    this.onError = onErrorMixin.bind(this);
    this.user = sessionStore.getState(NEW_SESSION_EVENT).user;
    this.selectedAlliance = sessionStore.getState(NEW_SESSION_EVENT).selectedAlliance;
  }

  componentDidMount() {
    const { match } = this.props;

    this.subscribe(riskStore, RISK_ERROR_EVENT, this.onError);
    this.subscribe(relatedItemStore, RELATED_ITEM_ERROR_EVENT, this.onError);

    this.subscribe(riskStore, RISK_DETAIL_EVENT, (state) => {
      const { risk } = state;
      const { data } = this.state;
      const riskData = R.clone(risk);
      const initiatives = R.clone(risk.initiatives.items);
      localStorage.setItem(RISK_DOCUMENTS, JSON.stringify(riskData.documents.items));

      data.riskData = riskData;
      data.initiatives = initiatives;

      this.setState(
        { data },
        () => fetchRelatedItems(riskData.itemRiskRelation.id),
        fetchRelatedItemsByItemId(riskData.itemRiskRelation.id),
      );
    });

    this.subscribe(relatedItemStore, RELATED_ITEMS_EVENT, (state) => {
      const items = state.item.itemsRelated.items.map((item) => getItemByType(item));
      const { data } = this.state;

      data.relatedItems = items;
      this.setState({ data });
    });

    this.subscribe(relatedItemStore, RELATED_ITEMS_BY_ITEM_EVENT, (state) => {
      const {
        itemsList: { items: itemsRelated },
      } = state;
      const relatedItemsByItem = itemsRelated.map((item) => getItemByType(item));
      const { data } = this.state;
      console.log('relatedItemsByItemFetch', relatedItemsByItem);
      console.log('relatedItemsByItemFetchState', state);

      data.relatedItemsByItem = relatedItemsByItem;
      this.setState({ data, loading: false });
    });

    this.subscribe(riskStore, RISK_COMPLETED_EVENT, (state) => {
      fetchRiskDetail(match.params.id);
      toast.success('Risk Successfully Completed');
    });

    this.subscribe(riskStore, RISK_RESTORE_EVENT, (state) => {
      fetchRiskDetail(match.params.id);
      toast.success('Risk Successfully Restored');
    });

    if (!match.params.id) return toast.error('Risk ID missing');
    fetchRiskDetail(match.params.id);
  }

  componentWillUnmount() {
    super.componentWillUnmount();
    localStorage.removeItem(RISK_DOCUMENTS);
  }

  completeModal = () => {
    this.setState({
      completedModalIsOpen: true,
    });
  };

  onYes = () => {
    this.setState(
      {
        completedModalIsOpen: false,
        loading: true,
      },
      () => {
        const riskData = R.clone(this.state.data.riskData);
        completedRisk(riskData);
      },
    );
  };

  onYesRestore = () => {
    this.setState(
      {
        restoreModalIsOpen: false,
        loading: true,
      },
      () => {
        const riskData = R.clone(this.state.data.riskData);
        restoreRisk(riskData);
      },
    );
  };

  onCloseModalCompleted = () => {
    this.setState({
      completedModalIsOpen: false,
    });
  };

  onCloseModalRestore = () => {
    this.setState({
      restoreModalIsOpen: false,
    });
  };

  restoreModal = () => {
    this.setState({
      restoreModalIsOpen: true,
    });
  };
  render() {
    const { data, loading, completedModalIsOpen, restoreModalIsOpen } = this.state;
    const { riskData, initiatives, relatedItems, relatedItemsByItem } = data;
    const { history } = this.props;

    let content = <Loader stretch />;
    let buttonsBottom = '';
    let buttonsTop = '';
    const alliance = this.selectedAlliance;
    const currency = getCurrencyOnSession();

    const _canCompletedRisk = canCompletedRisk(this.user, riskData, alliance);

    if (!loading) {
      content = (
        <>
          <RiskDetailTable
            data={riskData}
            currency={currency}
            onClickEdit={() => history.push(`/management/risk/edit/${riskData.id}`)}
          />
          <InitiativeListTable initiatives={initiatives} />
          <RelatedItemsDetailTable relatedItems={relatedItems} />
          <RelatedItemsByItemDetailTable relatedItemsByItem={relatedItemsByItem} />
        </>
      );

      buttonsTop = (
        <>
          <Heading type="h4" text={riskData.name} />

          <TopButtons
            onClickClosed={history.goBack}
            onClickCollaborated={() => openComments(riskData)}
          />
        </>
      );

      buttonsBottom = (
        <Row justifyContent="end">
          {_canCompletedRisk ? (
            <ActionButton
              text="Mark Completed"
              fontAwesomeIcon="clipboard-list"
              onClick={() => {
                this.completeModal();
              }}
            />
          ) : null}

          {canRestoreRisk(this.user, riskData, alliance) ? (
            <ActionButton
              text="Restore"
              fontAwesomeIcon="clipboard-list"
              onClick={() => {
                this.restoreModal();
              }}
            />
          ) : null}
        </Row>
      );
    }

    return (
      <React.Fragment>
        <Card.Header>{buttonsTop}</Card.Header>
        <DetailViewCardBody>{content}</DetailViewCardBody>
        <Card.Footer>{buttonsBottom}</Card.Footer>
        <YesNoDialog
          title={'Complete Risk'}
          onYes={this.onYes}
          onCloseModalCompleted={this.onCloseModalCompleted}
          onNo={this.onCloseModalCompleted}
          text={'Are you sure you want to Mark the Risk as Completed?'}
          isOpen={completedModalIsOpen}
        />

        <YesNoDialog
          title={'Restore Risk'}
          onYes={this.onYesRestore}
          onCloseModalCompleted={this.onCloseModalRestore}
          onNo={this.onCloseModalRestore}
          text={'Are you sure you want to Mark the Risk as Restored?'}
          isOpen={restoreModalIsOpen}
        />
      </React.Fragment>
    );
  }
}

RiskDetailView.propTypes = {
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default withRouter(withAlliance(RiskDetailView));

import { ValidationError } from '../../../shared/errors';
import {
  isValidString,
  setErrorIfDateIsBeforeAllianceStart,
  isValidBigInt,
} from '../../../shared/validators';
import moment from 'moment';
import { RiskModel } from './risk-model';
import { nextStepsValidator } from '../../../modules/next-step/next-step-validators';
import { initiativesItemValidator } from '../initiative/initiative-validators';

/**
 * Validate that the Item has all the fields necessaries
 * @param {RiskModel} data  - The Risk data to be reviewed
 * @param allianceDetails
 */
export const riskValidator = (data: RiskModel, allianceDetails) => {
  let errorMessages = [];

  if (!isValidString(data.name)) errorMessages.push('The Risk must have a valid Name');

  if (!isValidString(data.description))
    errorMessages.push('The Risk must have a valid Description');

  if (Array.isArray(data.nextSteps) && data.nextSteps.length > 0) {
    try {
      nextStepsValidator(data.nextSteps);
    } catch (e) {
      errorMessages.push(e.message);
    }
  } else errorMessages.push('The Risk must have valid Next Steps');

  if (!data.assignedTo) errorMessages.push('The Risk must have a valid Assignee');

  if (!moment(data.assignedDate).isValid()) {
    errorMessages.push('The Risk must have a valid Assignee Date');
  } else {
    setErrorIfDateIsBeforeAllianceStart(
      data.assignedDate,
      allianceDetails,
      'Assignee Date',
      errorMessages,
    );
  }

  if (!moment(data.originalDueDate).isValid()) {
    errorMessages.push('The Risk must have a valid Original Due Date');
  } else {
    setErrorIfDateIsBeforeAllianceStart(
      data.originalDueDate,
      allianceDetails,
      'Original Due Date',
      errorMessages,
    );
  }

  if (moment(data.revisedDueDate).isValid()) {
    setErrorIfDateIsBeforeAllianceStart(
      data.revisedDueDate,
      allianceDetails,
      'Revised Due Date',
      errorMessages,
    );
  }

  if (!isValidString(data.unitType, false, true))
    errorMessages.push('The Risk must have a valid Unit Type');

  if (!(parseFloat(data.unitQuantity) >= 0)) errorMessages.push('The Risk must have a valid Unit');

  if (!isValidString(data.unitValueDescription, true, true))
    errorMessages.push('The Risk must have a valid Unit Description');

  if (!isValidBigInt(data.unitMonetizationFactor))
    errorMessages.push('The Risk must have a valid Unit Monetization Factor');

  if (errorMessages.length !== 0) throw new ValidationError(errorMessages);
};

export const validateRiskData = (riskData, initiativesData, allianceDetails) => {
  riskValidator(riskData, allianceDetails);
  initiativesItemValidator(initiativesData);
};

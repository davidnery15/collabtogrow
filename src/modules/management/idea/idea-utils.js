import { getCompanyOnAlliance } from '../../../shared/alliance-utils';
import { IDEA_APPROVAL_PENDING } from '../../../shared/status';

/**
 * Return true if the User's company's approval or rejection
 * of the Idea is pending
 * @param {User} user The user
 * @param {Alliance} alliance The Alliance
 * @param {Idea} idea The Idea
 */

export const isUserPendingIdeaApproval = (user, alliance, idea) => {
  const { ideaApprovalRelation } = idea;
  const userCompany = getCompanyOnAlliance(user, alliance);

  const userCompanyIsPendingResponse = ideaApprovalRelation.items.find(
    (approval) =>
      userCompany.id === approval.company.id && approval.status === IDEA_APPROVAL_PENDING,
  );

  return !!userCompanyIsPendingResponse;
};

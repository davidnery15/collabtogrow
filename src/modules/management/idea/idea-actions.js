import {
  IDEA_CREATE_EVENT,
  IDEA_ERROR_EVENT,
  IDEA_UPDATE_EVENT,
  IDEA_REJECT_EVENT,
  IDEA_DETAIL_EVENT,
  IDEA_LIST_EVENT,
  IDEA_DELETE_EVENT,
  IDEA_COMPLETED_EVENT,
  IDEA_SUBMIT_FOR_APPROVAL_EVENT,
  IDEA_RESTORE_EVENT,
} from './idea-store';
import Flux from '@cobuildlab/flux-state';
import { IntegrityError } from '../../../shared/errors';
import { ideaValidator } from './idea-validators';
import { businessCaseValidator } from '../../document-management/business-case/business-case-validators';
import { initiativesItemValidator } from '../initiative/initiative-validators';
import sessionStore, { APOLLO_CLIENT, NEW_SESSION_EVENT } from '../../../shared/SessionStore';
import {
  IDEA_CREATE_MUTATION,
  IDEA_DETAIL_QUERY,
  IDEA_UPDATE_MUTATION,
  IDEA_APPROVALS_LIST_QUERY,
  IDEA_APPROVAL_UPDATE_MUTATION,
  IDEA_APPROVAL_MUTATION,
  IDEA_COMMENTS_QUERY,
  IDEA_LIST_QUERY,
  IDEA_DELETE_MUTATION,
} from './idea-queries';
import { log, error } from '@cobuildlab/pure-logger';
import {
  sanitize8BaseReferenceFromObject,
  sanitize8BaseReferencesFromObjects,
  sanitize8BaseReconnectsFromObjects,
  sanitize8BaseDate,
  sanitize8BaseDocumentsCreate,
  sanitize8BaseDocumentCreate,
  sanitize8BaseDocumentsDeleteAndUpdate,
  sanitize8BaseBigInt,
  filterForYear,
} from '../../../shared/utils';
import { getActiveAllianceId, getCompanyOnAlliance } from '../../../shared/alliance-utils';
import {
  sanitizeRecommendedSolutions,
  updateBusinessCase,
} from '../../document-management/business-case/businessCases.actions';
import { INITIATIVE_ERROR_EVENT } from '../initiative/initiative-store';
import {
  IDEA_COMPLETED,
  IDEA_APPROVAL_APPROVED,
  IDEA_APPROVAL_REJECTED,
  IDEA_APPROVED,
  IDEA_REJECTED,
  IDEA_SUBMITTED_FOR_APPROVAL,
  IDEA_IN_PROGRESS,
} from '../../../shared/status';
import {
  canApproveIdea,
  canRejectIdea,
  canCompletedIdea,
  canSubmitForApprovalIdea,
  canRestoreIdea,
} from './idea-permissions';
import {
  COMMENT_CREATE_EVENT,
  COMMENT_ERROR_EVENT,
  COMMENT_REQUEST_EVENT,
  COMMENTS_EVENT,
} from '../../comment/comment-store';
import { IDEA_TYPE } from '../../../shared/item-types';
import { COMMENTS_CREATE_MUTATION } from '../../comment/comment-queries';
import { fetchAlliance } from '../../settings/alliance-management/alliance-actions';
import { businessCaseSearchFilterOR } from '../../document-management/business-case/businessCases.actions';
import {
  sanitizeNextStepsCreate,
  nextStepsToBeCreated,
  updateNextSteps,
  deleteNextSteps,
  completeItemsNextSteps,
  restoreItemsNextSteps,
} from 'modules/next-step/next-step-actions.js';
import { RELATED_ITEM_UPDATE_MUTATION } from '../../related-item/related-item-queries';
import { INITIATIVE_CREATE_MUTATION } from '../initiative/initiative-queries';
import * as R from 'ramda';

/**
 * Creates a filter object and search by a string
on string properties of the idea.
 *
 * @param {string} [search=''] - The string to search.
 * @param {string} allianceId -  The allianceId to filter.
 * @param status
 * @param filterYear
 */
const ideaFilter = (allianceId, search = '', status, filterYear = null) => {
  const filter = {
    alliance: { id: { equals: allianceId } },
    idea: {
      id: { not_equals: null },
      OR: [
        {
          name: {
            contains: search,
          },
        },
        {
          description: {
            contains: search,
          },
        },
        {
          requestedBy: {
            firstName: { contains: search },
          },
        },
        {
          requestedBy: {
            lastName: { contains: search },
          },
        },
        {
          requestedBy: {
            email: { contains: search },
          },
        },
        {
          assignedTo: {
            firstName: { contains: search },
          },
        },
        {
          assignedTo: {
            lastName: { contains: search },
          },
        },
        {
          assignedTo: {
            email: { contains: search },
          },
        },
      ].concat(businessCaseSearchFilterOR(search)),
    },
  };

  if (filterYear) {
    if (filterYear) {
      const filterForYearData = filterForYear(
        filterYear.year,
        filterYear.fieldName,
        filterYear.isDateTime,
      );

      if (filterYear.fieldName === 'dateOfResponse') {
        filter.idea.ideaApprovalRelation = {
          some: {
            AND: filterForYearData.AND,
          },
        };
      } else {
        filter.idea.AND = filterForYearData.AND;
      }
    }
  }

  if (status) {
    filter.idea.status = { equals: status };
  }

  return filter;
};

/**
 * Notifies a Request for Comments for an Idea.
 */
export const openComments = ({ id: ideaId }) => {
  Flux.dispatchEvent(COMMENT_REQUEST_EVENT, { type: IDEA_TYPE, id: ideaId });
};

/**
 * Creates a New Idea
- TODO: Sets the Owner to the current logged User
- Create the Idea.
 *
 * @returns {Promise<void>}
 * @param idea
 * @param businessCase
 * @param relatedItems
 * @param initiatives
 */
export const createIdea = async (idea, businessCase, relatedItems, initiatives) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { user, selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
  const allianceId = getActiveAllianceId(user);

  delete idea.id;
  delete businessCase.id;

  if (!allianceId)
    return Flux.dispatchEvent(IDEA_ERROR_EVENT, new IntegrityError('Must have an Active Alliance'));

  log('IdeaData: ', idea, businessCase, relatedItems, initiatives);
  try {
    ideaValidator(idea, selectedAlliance);
    initiativesItemValidator(initiatives);
    businessCaseValidator(businessCase);
  } catch (e) {
    error('createIdea', e);
    return Flux.dispatchEvent(IDEA_ERROR_EVENT, e);
  }

  idea.initiatives = initiatives;
  sanitize8BaseReferencesFromObjects(idea, 'initiatives');
  sanitize8BaseDocumentsCreate(idea, 'documents');
  sanitize8BaseReferenceFromObject(idea, 'assignedTo');
  sanitize8BaseReferenceFromObject(idea, 'source');
  sanitize8BaseReferenceFromObject(idea, 'requestedBy');
  sanitize8BaseBigInt(idea, 'budgetUtilized');
  sanitize8BaseBigInt(idea, 'unitMonetizationFactor');

  sanitize8BaseDate(idea, 'assignedDate');
  sanitize8BaseDate(idea, 'originalDueDate');
  sanitize8BaseDate(idea, 'revisedDueDate');
  sanitize8BaseDate(idea, 'requestedDate');

  sanitizeNextStepsCreate(idea);
  sanitizeRecommendedSolutions(businessCase);
  sanitize8BaseDocumentCreate(businessCase, 'document');
  idea.businessCase = { create: businessCase };
  idea.itemIdeaRelation = {
    create: {
      alliance: { connect: { id: allianceId } },
      itemsRelated: {
        connect: relatedItems.map((item) => {
          return { id: item.itemId };
        }),
      },
    },
  };

  let result;
  try {
    result = await client.mutate({
      mutation: IDEA_CREATE_MUTATION,
      variables: { data: idea },
    });
  } catch (e) {
    error('createIdea', e);
    return Flux.dispatchEvent(IDEA_ERROR_EVENT, e);
  }

  return Flux.dispatchEvent(IDEA_CREATE_EVENT, result);
};

/**
 * Fetch the Idea by id of the Current Alliance.
 *
 * @returns {Promise<void>}
 * @param id
 */
export const fetchIdea = async (id) => {
  const client = sessionStore.getState(APOLLO_CLIENT);

  let response;
  try {
    response = await client.query({
      query: IDEA_DETAIL_QUERY,
      fetchPolicy: 'network-only',
      variables: { id },
    });
  } catch (e) {
    error('fetchIdea', e);
    throw e;
  }
  log('fetchIdea', response);
  return response.data;
};

/**
 * Edit an Idea
- Create the Idea.
 *
 * @returns {Promise<void>}
 * @param idea
 * @param businessCaseData
 * @param relatedItems
 * @param initiatives
 * @param originalRecommendedSolutions
 * @param originalNextSteps
 * @param originalDocuments
 */
export const updateIdea = async (
  idea,
  businessCaseData,
  relatedItems,
  initiatives,
  originalRecommendedSolutions,
  originalNextSteps,
  originalDocuments,
) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
  const itemId = idea.itemIdeaRelation.id;

  delete idea.__typename;
  delete idea.status;
  delete idea.createdAt;
  delete idea.createdBy;
  delete idea.businessCase;
  delete idea.itemIdeaRelation;
  delete idea.ideaApprovalRelation;
  delete businessCaseData.__typename;
  delete businessCaseData.owner;
  delete businessCaseData.status;
  delete businessCaseData.createdAt;
  delete businessCaseData.createdBy;
  delete businessCaseData.recommendedSolutionsRelation;

  log('UpdateIdea: ', idea, businessCaseData, relatedItems, initiatives);
  try {
    ideaValidator(idea, selectedAlliance);
    initiativesItemValidator(initiatives);
    businessCaseValidator(businessCaseData);
  } catch (e) {
    error('createIdea', e);
    return Flux.dispatchEvent(IDEA_ERROR_EVENT, e);
  }

  idea.initiatives = initiatives;

  sanitize8BaseReconnectsFromObjects(idea, 'initiatives');
  sanitize8BaseReferenceFromObject(idea, 'assignedTo');
  sanitize8BaseReferenceFromObject(idea, 'source');
  sanitize8BaseReferenceFromObject(idea, 'requestedBy');
  sanitize8BaseBigInt(idea, 'budgetUtilized');
  sanitize8BaseBigInt(idea, 'unitMonetizationFactor');
  sanitize8BaseDate(idea, 'assignedDate');
  sanitize8BaseDate(idea, 'originalDueDate');
  sanitize8BaseDate(idea, 'revisedDueDate');
  sanitize8BaseDate(idea, 'requestedDate');
  sanitize8BaseDocumentsDeleteAndUpdate(idea, 'documents', originalDocuments);

  // nextSteps to be created
  const nextSteps = R.clone(idea.nextSteps);
  const toBeCreated = nextStepsToBeCreated(nextSteps);
  idea.nextSteps = toBeCreated;
  sanitizeNextStepsCreate(idea);

  // update and delete nextSteps
  await deleteNextSteps(nextSteps, originalNextSteps);
  await updateNextSteps(nextSteps, originalNextSteps);

  let response;
  try {
    response = await client.mutate({
      mutation: IDEA_UPDATE_MUTATION,
      variables: { data: idea },
    });
  } catch (e) {
    error('updateIdea', e);
    return Flux.dispatchEvent(IDEA_ERROR_EVENT, e);
  }

  try {
    await updateBusinessCase(businessCaseData, originalRecommendedSolutions);
  } catch (e) {
    console.error('updateIdea', e);
    return Flux.dispatchEvent(IDEA_ERROR_EVENT, e);
  }

  // Reconnect related items
  const relatedItemsData = {
    id: itemId,
    itemsRelated: {
      reconnect: relatedItems.map((i) => {
        return { id: i.itemId };
      }),
    },
  };

  log('IdeaData:update:relatedItems:', relatedItemsData);
  try {
    await client.mutate({
      mutation: RELATED_ITEM_UPDATE_MUTATION,
      variables: { data: relatedItemsData },
    });
  } catch (e) {
    error('updateIdea:updateRelated Items', e, idea);
    return Flux.dispatchEvent(IDEA_ERROR_EVENT, e);
  }

  Flux.dispatchEvent(IDEA_UPDATE_EVENT, response);
  return response.data;
};

/**
 * Submit For Approval an Idea.
 *
 * @param amoItem - The Idea.
 */
export const requestApprovalForIdea = async (amoItem) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { user, selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
  const ideaId = amoItem.idea.id;
  const { idea } = await fetchIdea(ideaId);
  const activeAllianceId = selectedAlliance.id;

  if (!canSubmitForApprovalIdea(user, idea, selectedAlliance))
    return Flux.dispatchEvent(
      IDEA_ERROR_EVENT,
      new IntegrityError('You do not have permission to perform this action'),
    );

  log('requestApprovalForIdea', 'toValidate', idea);
  // set initiatives & nextSteps for validators
  idea.initiatives = idea.initiatives.items;
  idea.nextSteps = idea.nextSteps.items;
  try {
    ideaValidator(idea, selectedAlliance);
    businessCaseValidator(idea.businessCase);
  } catch (e) {
    console.error('requestApprovalForIdea', e);
    return Flux.dispatchEvent(IDEA_ERROR_EVENT, e);
  }

  const { alliance } = await fetchAlliance(activeAllianceId);
  log('requestApprovalForIdea', 'alliance', alliance);
  const data = {
    id: idea.id,
    status: IDEA_SUBMITTED_FOR_APPROVAL,
    ideaApprovalRelation: {
      create: [
        { company: { connect: { id: alliance.clientCompany.id } } },
        { company: { connect: { id: alliance.partnerCompany.id } } },
      ],
    },
  };

  let response;
  try {
    response = await client.mutate({
      mutation: IDEA_UPDATE_MUTATION,
      variables: { data },
    });
  } catch (e) {
    console.error('requestApprovalForIdea', e, amoItem);
    return Flux.dispatchEvent(IDEA_ERROR_EVENT, e);
  }
  Flux.dispatchEvent(IDEA_SUBMIT_FOR_APPROVAL_EVENT, response.data);
  return response.data;
};

/**
 * Approve Idea.
 *
 * @param idea
 * @param {string} Idea
 * @returns {Promise}
 */
export const approveIdea = async (idea) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const user = sessionStore.getState(NEW_SESSION_EVENT).user;
  const ideaId = idea.id;
  const allianceId = getActiveAllianceId(user);
  const company = getCompanyOnAlliance(user, { id: allianceId });

  if (!canApproveIdea(user, idea, { id: allianceId }))
    return Flux.dispatchEvent(
      IDEA_ERROR_EVENT,
      new IntegrityError(`Permission Denied. Can't approve this Idea`),
    );

  // We fetch the approvals to see what's gonna happen
  const { ideaApprovalsList } = await fetchIdeaApprovals(ideaId);
  if (ideaApprovalsList.count < 2) {
    return Flux.dispatchEvent(
      IDEA_ERROR_EVENT,
      new IntegrityError('Idea must have have at least 2 request for approvals'),
    );
  }

  let approvalsCounter = 0;
  let approvalId = null;
  ideaApprovalsList.items.forEach((approval) => {
    if (approval.status === IDEA_APPROVAL_APPROVED) approvalsCounter++;
    if (approval.company.id === company.id) approvalId = approval.id;
  });
  if (approvalId === null) {
    return Flux.dispatchEvent(
      IDEA_ERROR_EVENT,
      new IntegrityError("You can't approve this Idea, your Company does not belong here."),
    );
  }

  // Update the status of the Aproval
  const mutation = {
    mutation: IDEA_APPROVAL_UPDATE_MUTATION,
    variables: {
      approval: {
        id: approvalId,
        status: IDEA_APPROVAL_APPROVED,
        dateOfResponse: new Date(),
        approvedBy: { connect: { id: user.id } },
      },
    },
  };
  // If there are more than 0 approvals: Update the status of the idea
  if (approvalsCounter > 0) {
    mutation.mutation = IDEA_APPROVAL_MUTATION;
    mutation.variables.idea = {
      id: ideaId,
      status: IDEA_APPROVED,
    };

    try {
      await createInitiativeOfApprovedIdea(ideaId);
    } catch (e) {
      error('createInitiativeOfApprovedIdeaError', e);
      return Flux.dispatchEvent(IDEA_ERROR_EVENT, e);
    }
  }

  let response;
  try {
    response = await client.mutate(mutation);
  } catch (e) {
    error('approveFundingRequest', e);
    return Flux.dispatchEvent(IDEA_ERROR_EVENT, e);
  }
  log('approveFundingRequest', response);
  Flux.dispatchEvent(IDEA_UPDATE_EVENT, response.data);
  return response.data;
};

/**
 * Creates an initiative from an idea.
 *
 * @param  {string}  ideaId
 */
const createInitiativeOfApprovedIdea = async (ideaId) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const user = sessionStore.getState(NEW_SESSION_EVENT).user;
  const allianceId = getActiveAllianceId(user);
  const {
    idea: {
      name,
      description,
      businessCase,
      budgetUtilized,
      revisedDueDate,
      requestedBy,
      requestedDate,
    },
  } = await fetchIdea(ideaId);

  // TODO: copy documents
  const initiative = {
    name,
    description,
    budgetUtilized,
    owner: requestedBy,
    baselineStartDate: revisedDueDate,
    baselineEndDate: revisedDueDate,
    forecastedEndDate: revisedDueDate,
    alliance: { id: allianceId },
    requestedBy,
    requestedDate,
  };
  const businessCaseData = R.clone(businessCase);
  delete businessCaseData.id;
  delete businessCaseData.createdAt;
  delete businessCaseData.__typename;
  delete businessCaseData.document;

  // sanitize initiative
  sanitize8BaseDate(initiative, 'baselinestartDate');
  sanitize8BaseDate(initiative, 'baselineEndDate');
  sanitize8BaseDate(initiative, 'forecastedEndDate');
  sanitize8BaseReferenceFromObject(initiative, 'alliance');
  sanitize8BaseReferenceFromObject(initiative, 'requestedBy');
  sanitize8BaseReferenceFromObject(initiative, 'owner');
  sanitize8BaseDate(initiative, 'requestedDate');
  // sanitize businessCase
  businessCaseData.recommendedSolutionsRelation = {
    create: businessCaseData.recommendedSolutionsRelation.items.map(({ description }) => {
      return { description };
    }),
  };
  initiative.businessCase = {
    create: businessCaseData,
  };

  console.log('createInitiativeOfApprovedIdea: ', initiative);
  let response;
  try {
    response = await client.mutate({
      mutation: INITIATIVE_CREATE_MUTATION,
      variables: { data: initiative },
    });
  } catch (e) {
    error('createInitiativeOfApprovedIdea', e);
    throw e;
  }
  console.log('createInitiativeOfApprovedIdeaResponse: ', response);

  return response;
};

/**
 * Reject Idea.
 *
 * @param idea
 * @param {string} Idea
 * @returns {Promise}
 */
export const rejectIdea = async (idea) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const user = sessionStore.getState(NEW_SESSION_EVENT).user;
  const ideaId = idea.id;
  const allianceId = getActiveAllianceId(user);
  const company = getCompanyOnAlliance(user, { id: allianceId });

  if (!canRejectIdea(user, idea, { id: allianceId }))
    return Flux.dispatchEvent(
      IDEA_ERROR_EVENT,
      new IntegrityError(`Permission Denied. Can't reject this Idea`),
    );

  // We fetch the approvals to see what's gonna happen
  const { ideaApprovalsList } = await fetchIdeaApprovals(ideaId);
  if (ideaApprovalsList.count < 2) {
    return Flux.dispatchEvent(
      IDEA_ERROR_EVENT,
      new IntegrityError('Idea must have have at least 2 request for approvals'),
    );
  }

  let approvalId = null;
  let alliedCompanyApprovalId = null;
  ideaApprovalsList.items.forEach((approval) => {
    if (approval.company.id === company.id) {
      approvalId = approval.id;
    } else {
      alliedCompanyApprovalId = approval.id;
    }
  });
  if (approvalId === null) {
    return Flux.dispatchEvent(
      IDEA_ERROR_EVENT,
      new IntegrityError("You can't reject this Idea, your Company does not belong here."),
    );
  }

  // Update the status of the Approval
  const userCompanyMutation = {
    mutation: IDEA_APPROVAL_MUTATION,
    variables: {
      approval: {
        id: approvalId,
        status: IDEA_APPROVAL_REJECTED,
        dateOfResponse: new Date(),
        approvedBy: { connect: { id: user.id } },
      },
      idea: {
        id: ideaId,
        status: IDEA_REJECTED,
      },
    },
  };

  const alliedCompanyMutation = {
    mutation: IDEA_APPROVAL_UPDATE_MUTATION,
    variables: {
      approval: {
        id: alliedCompanyApprovalId,
        status: IDEA_APPROVAL_REJECTED,
        dateOfResponse: new Date(),
      },
    },
  };

  let response;
  try {
    response = await client.mutate(userCompanyMutation);
    await client.mutate(alliedCompanyMutation);
  } catch (e) {
    error('rejectIdea', e);
    return Flux.dispatchEvent(IDEA_ERROR_EVENT, e);
  }
  log('rejectIdea', response);
  Flux.dispatchEvent(IDEA_REJECT_EVENT, response.data);
  return response.data;
};

/**
 * Fetches the Ideas approvals for the User.
 * @param ideaId
 * @param {string} Idea Id
 * @return {Promise}
 */
export const fetchIdeaApprovals = async (ideaId) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const data = { idea: { id: { equals: ideaId } } };

  let response;
  try {
    response = await client.query({
      query: IDEA_APPROVALS_LIST_QUERY,
      variables: { data },
      fetchPolicy: 'network-only',
    });
  } catch (e) {
    error('fetchIdeaApprovals', e);
    return Flux.dispatchEvent(INITIATIVE_ERROR_EVENT, e);
  }
  log('fetchIdeaApprovals', response);
  return response.data;
};

/**
 * Fetches the Idea Comments.
 *
 * @returns {Promise<void>}
 * @param ideaId
 */
export const fetchIdeaComments = async (ideaId) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const data = { id: ideaId };
  const { user } = sessionStore.getState(NEW_SESSION_EVENT);

  let response;
  try {
    response = await client.query({
      query: IDEA_COMMENTS_QUERY,
      variables: data,
      fetchPolicy: 'network-only',
    });
  } catch (e) {
    error('fetchIdeaComments', e);
    return Flux.dispatchEvent(COMMENT_ERROR_EVENT, e);
  }
  log('fetchIdeaComments', response);
  response.data.idea.comments.userId = user.id;

  Flux.dispatchEvent(COMMENTS_EVENT, response.data.idea.comments);
  return response.data.idea.comments;
};

/**
 * Create a comment on an Idea.
 *
 * @param ideaId
 * @param comment
 * @returns {Promise<*>}
 */
export const createIdeaComment = async (ideaId, comment) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const data = {
    comment,
    ideaCommentsRelation: { connect: { id: ideaId } },
  };

  let response;
  try {
    response = await client.mutate({
      mutation: COMMENTS_CREATE_MUTATION,
      variables: { data },
    });
  } catch (e) {
    error('createIdeaComment', e);
    return Flux.dispatchEvent(COMMENT_ERROR_EVENT, e);
  }
  log('createIdeaComment', response);
  Flux.dispatchEvent(COMMENT_CREATE_EVENT, response.data);
  return response.data;
};

/**
 * Fetches the Ideas of the Current Alliance.
 *
 * @returns {Promise<void>}
 * @param search
 * @param page
 * @param first
 * @param status
 * @param filterYear
 */
export const fetchIdeas = async (search = '', page = 1, first = 20, status, filterYear = null) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const user = sessionStore.getState(NEW_SESSION_EVENT).user;
  const allianceId = getActiveAllianceId(user);
  const skip = (page - 1) * first;

  if (!allianceId)
    return Flux.dispatchEvent(IDEA_ERROR_EVENT, new IntegrityError('An Alliance must be selected'));

  const filter = ideaFilter(allianceId, search, status, filterYear);

  let response;
  try {
    response = await client.query({
      query: IDEA_LIST_QUERY,
      variables: { data: filter, skip, first },
      fetchPolicy: 'network-only',
    });
    response.data.page = page;
    response.data.first = first;
    response.data.search = search;
  } catch (e) {
    error('fetchIdeas', e);
    Flux.dispatchEvent(IDEA_ERROR_EVENT, e);
    throw e;
  }

  log('fetchIdeas', response);
  Flux.dispatchEvent(IDEA_LIST_EVENT, response.data);

  return response.data;
};

/**
 * Delete an idea.
 *
 * @param idea
 * @returns {Promise<*>}
 */
export const deleteIdea = async (idea) => {
  const client = sessionStore.getState(APOLLO_CLIENT);

  let response;
  try {
    response = await client.mutate({
      mutation: IDEA_DELETE_MUTATION,
      variables: { data: { id: idea.id, force: true } },
    });
  } catch (e) {
    error('deleteIdea', e);
    return Flux.dispatchEvent(IDEA_ERROR_EVENT, e);
  }
  log('deleteIdea', response);
  Flux.dispatchEvent(IDEA_DELETE_EVENT, response.data);
  return response.data;
};

/**
 * Completed a Idea.
 *
 * @returns {Promise}
 * @param ideaData
 */
export const completedIdea = async (ideaData) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { selectedAlliance, user } = sessionStore.getState(NEW_SESSION_EVENT);
  const { idea } = await fetchIdea(ideaData.id);

  let response;
  if (!canCompletedIdea(user, idea, selectedAlliance))
    return Flux.dispatchEvent(
      IDEA_ERROR_EVENT,
      new IntegrityError('You do not have permission to perform this action'),
    );

  try {
    response = await client.mutate({
      mutation: IDEA_UPDATE_MUTATION,
      variables: {
        data: {
          id: idea.id,
          status: IDEA_COMPLETED,
        },
      },
    });
  } catch (e) {
    error('completedIdea', e);
    return Flux.dispatchEvent(IDEA_ERROR_EVENT, e);
  }

  try {
    await completeItemsNextSteps(idea);
  } catch (e) {
    log('completeItemsNextStepsError', e);
  }

  Flux.dispatchEvent(IDEA_COMPLETED_EVENT, response);
  return response.data;
};

/**
 * Restore a Idea.
 *
 * @param ideaData
 * @returns {Promise<void|*>}
 */
export const restoreIdea = async (ideaData) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { selectedAlliance, user } = sessionStore.getState(NEW_SESSION_EVENT);
  const { idea } = await fetchIdea(ideaData.id);

  let response;
  if (!canRestoreIdea(user, idea, selectedAlliance))
    return Flux.dispatchEvent(
      IDEA_ERROR_EVENT,
      new IntegrityError('You do not have permission to perform this action'),
    );

  try {
    response = await client.mutate({
      mutation: IDEA_UPDATE_MUTATION,
      variables: {
        data: {
          id: idea.id,
          status: IDEA_IN_PROGRESS,
        },
      },
    });
  } catch (e) {
    error('restoreIdea', e);
    return Flux.dispatchEvent(IDEA_ERROR_EVENT, e);
  }

  Flux.dispatchEvent(IDEA_RESTORE_EVENT, response);
  return response.data;
};

/**
 * This function call idea data and dispatch event.
 *
 * @param id
 * @returns {Promise<*>}
 */
export const fetchIdeaDetail = async (id) => {
  let response;
  try {
    response = await fetchIdea(id);
  } catch (e) {
    return Flux.dispatchEvent(IDEA_ERROR_EVENT, response);
  }

  Flux.dispatchEvent(IDEA_DETAIL_EVENT, response);
  return response;
};

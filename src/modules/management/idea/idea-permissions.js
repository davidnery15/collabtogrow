import {
  isUserAdminOrSERInAlliance,
  isUserCreatorInAlliance,
} from '../../../shared/alliance-utils';
import {
  IDEA_COMPLETED,
  IDEA_APPROVED,
  IDEA_SUBMITTED_FOR_APPROVAL,
  ALLIANCE_APPROVED,
} from '../../../shared/status';
import { initiativesApprovedValidator } from '../initiative/initiative-validators';
import { isUserPendingIdeaApproval } from './idea-utils';
import { isAllianceCompleted } from '../../settings/alliance-management/alliance-permissions';

/**
 * Checks if a User can Edit a Idea.
 *
 * @param {object}user - User.
 * @param {object}idea - Idea.
 * @param {object}alliance - Alliance.
 * @returns {boolean} Validate Options.
 */
export const canEditIdea = (user, idea, alliance): boolean => {
  if (isAllianceCompleted(alliance)) return false;

  if (idea.status === IDEA_COMPLETED) return false;

  if (isUserAdminOrSERInAlliance(user, alliance)) return true;

  if (idea.status === IDEA_APPROVED) return false;

  if (idea.status === IDEA_SUBMITTED_FOR_APPROVAL) return false;

  if (isUserCreatorInAlliance(user, alliance)) return true;

  return false;
};

/**
 * Checks if a User can Approve a Idea.
 *
 * @param {object}user - User.
 * @param {object}idea - Idea.
 * @param {object}alliance - Alliance.
 * @returns {boolean} Validate Options.
 */
export const canApproveIdea = (user, idea, alliance) => {
  if (isAllianceCompleted(alliance)) return false;

  if (idea.status !== IDEA_SUBMITTED_FOR_APPROVAL) return false;

  if (!isUserAdminOrSERInAlliance(user, alliance)) return false;

  return isUserPendingIdeaApproval(user, alliance, idea);
};

/**
 * Checks if a User can Reject a Idea.
 *
 * @param {object}user - User.
 * @param {object}idea - Idea.
 * @param {object}alliance - Alliance.
 * @returns {boolean} Validate Options.
 */
export const canRejectIdea = (user, idea, alliance) => {
  if (isAllianceCompleted(alliance)) return false;

  if (idea.status !== IDEA_SUBMITTED_FOR_APPROVAL) return false;

  if (idea.status !== IDEA_SUBMITTED_FOR_APPROVAL) return false;

  if (!isUserAdminOrSERInAlliance(user, alliance)) return false;

  return isUserPendingIdeaApproval(user, alliance, idea);
};

/**
 * Checks if a User can Submit for Approval a Idea.
 *
 * @param {object}user - User.
 * @param {object}idea - Idea.
 * @param {object}alliance - Alliance.
 * @returns {boolean} Validate Options.
 */
export const canSubmitForApprovalIdea = (user, idea, alliance) => {
  if (isAllianceCompleted(alliance)) return false;

  if (alliance.status !== ALLIANCE_APPROVED) return false;

  if (
    idea.status === IDEA_APPROVED ||
    idea.status === IDEA_COMPLETED ||
    idea.status === IDEA_SUBMITTED_FOR_APPROVAL
  )
    return false;

  try {
    initiativesApprovedValidator(idea.initiatives);
  } catch (e) {
    return false;
  }

  return isUserAdminOrSERInAlliance(user, alliance);
};

/**
 * Checks if a User can Create a Idea.
 *
 * @param {object}user - User.
 * @param {object}alliance - Alliance.
 * @returns {boolean} Validate Options.
 */
export const canCreateIdea = (user, alliance) => {
  if (isAllianceCompleted(alliance)) return false;

  if (isUserAdminOrSERInAlliance(user, alliance)) return true;

  if (isUserCreatorInAlliance(user, alliance)) return true;

  return false;
};

/**
 * Checks if a User can Deleted a Idea.
 *
 * @param {object}user - User.
 * @param {object}idea - Idea.
 * @param {object}alliance - Alliance.
 * @returns {boolean} Validate Options.
 */
export const canDeleteIdea = (user, idea, alliance) => {
  if (isAllianceCompleted(alliance)) return false;

  if (idea.status === IDEA_SUBMITTED_FOR_APPROVAL) return false;
  if (idea.status === IDEA_APPROVED) return false;

  return isUserAdminOrSERInAlliance(user, alliance);
};

/**
 * Checks if a User can Completed a Idea.
 *
 * @param {object}user - User.
 * @param {object}idea - Idea.
 * @param {object}alliance - Alliance.
 * @returns {boolean} Validate Options.
 */
export const canCompletedIdea = (user, idea, alliance) => {
  if (isAllianceCompleted(alliance)) return false;

  if (idea.status === IDEA_COMPLETED) return false;

  if (idea.assignedTo && idea.assignedTo.id === user.id) return true;

  return isUserAdminOrSERInAlliance(user, alliance);
};

/**
 * Checks if a User can Restore a Idea.
 *
 * @param {object}user - User.
 * @param {object}idea - Idea.
 * @param {object}alliance - Alliance.
 * @returns {boolean} Validate Options.
 */
export const canRestoreIdea = (user, idea, alliance) => {
  if (isAllianceCompleted(alliance)) return false;

  if (idea.status !== IDEA_COMPLETED) return false;

  if (idea.assignedTo && idea.assignedTo.id === user.id) return true;

  return isUserAdminOrSERInAlliance(user, alliance);
};

import React from 'react';
import PropTypes from 'prop-types';
import DetailValue from '../../../../components/DetailValue';
import DetailDateValue from '../../../../components/DetailDateValue';
import { CurrencyTextField } from '../../../../shared/components/CurrencyTextField';
import NextStepsDetail from '../../../../modules/next-step/components/NextStepsDetail';
import UserDetailValue from '../../../../components/UserDetailValue';
import DocumentsFileComponent from '../../../../components/inputs/DocumentsFileComponent';
import { IDEA_DOCUMENTS } from '../idea-model';
import { calculateValueBigInt } from '../../../../shared/utils';
import { HorizontalLine } from '../../../../components/new-ui/text/HorizontalLine';
import { HorizontalLineText } from '../../../../components/text/HorizontalLineText';
import { BoderDetailView } from '../../../../components/new-ui/div/BorderDetailView';
import { Grid } from '@8base/boost';
import { TablePosition } from '../../../../components/new-ui/div/TablePosition';
import { TableDetail } from '../../../../components/new-ui/table/TableDetail';
import { ThTitlePosition } from '../../../../components/new-ui/div/ThTitlePosition';
import { HeaderText } from '../../../../components/new-ui/text/HeaderText';
import { EditButton } from '../../../../components/new-ui/buttons/EditButton';
import sessionStore, { NEW_SESSION_EVENT } from '../../../../shared/SessionStore';
import { canEditIdea } from '../idea-permissions';

/**
 * Detail View Table For The Idea Entity
 */
const IdeaDetailTable = (props) => {
  const { data, currency, onClickEdit } = props;
  const {
    status,
    name,
    requestedBy,
    revisedDueDate,
    description,
    documents,
    budgetUtilized,
    assignedTo,
    initiatives,
    nextSteps,
    createdAt,
    assignedDate,
    originalDueDate,
    unitType,
    unitQuantity,
    unitValueDescription,
    unitMonetizationFactor,
  } = data;
  const { user, selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);

  return (
    <>
      <HorizontalLine>
        <HorizontalLineText>IDEA</HorizontalLineText>
      </HorizontalLine>
      <>
        <BoderDetailView>
          <Grid.Layout
            columns="auto 100px"
            areas={[['left', 'right']]}
            style={{ width: '100%', height: '100%' }}>
            <Grid.Box area="left">
              <div style={{ position: 'absolute', top: '21px', left: '25px' }}>
                <HeaderText>IDEA</HeaderText>
              </div>
            </Grid.Box>
            <Grid.Box area="right" justifyContent={'center'}>
              {canEditIdea(user, data, selectedAlliance) ? (
                <EditButton onClick={onClickEdit} text="Edit" />
              ) : null}
            </Grid.Box>
          </Grid.Layout>
        </BoderDetailView>
        <TablePosition>
          <TableDetail>
            <tbody>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>STATE</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <span> {status} </span>
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>NEXT STEPS</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <NextStepsDetail nextSteps={nextSteps} assignedTo={assignedTo} />
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>NAME</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <DetailValue text={name} />
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>DESCRIPTION</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <DetailValue text={description} />
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>ASSIGNED TO</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <UserDetailValue user={assignedTo} />
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>CREATED AT</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <DetailDateValue date={createdAt} />
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>ASSIGNED DATE</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <DetailDateValue date={assignedDate} />
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>ORIGINAL DUE DATE</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <DetailDateValue date={originalDueDate} />
                </td>
              </tr>
              <th>
                <ThTitlePosition>
                  <span>REQUESTED BY</span>
                </ThTitlePosition>
              </th>
              <td>
                <UserDetailValue user={requestedBy} />
              </td>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>REVISED DUE DATE</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <DetailDateValue date={revisedDueDate} />
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>BUDGED UTILIZED</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <CurrencyTextField value={budgetUtilized} currency={currency} />
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>UNIT TYPE</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <DetailValue text={unitType} />
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>UNIT QUANTITY</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <DetailValue text={unitQuantity} />
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>UNIT VALUE DESCRIPTION</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <DetailValue text={unitValueDescription} />
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>UNIT MONETIZATION FACTOR</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <CurrencyTextField value={unitMonetizationFactor} currency={currency} />
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>CALCULATED VALUE</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <CurrencyTextField
                    value={calculateValueBigInt(unitQuantity, unitMonetizationFactor)}
                    currency={currency}
                  />
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>DOCUMENTS</span>
                  </ThTitlePosition>
                </th>
                <td>
                  <DocumentsFileComponent data={documents} localKey={IDEA_DOCUMENTS} />
                </td>
              </tr>
              <tr>
                <th>
                  <ThTitlePosition>
                    <span>INITIATIVES</span>
                  </ThTitlePosition>
                </th>
                <td>
                  {initiatives.items.map((initiative, i) => (
                    <React.Fragment key={i}>
                      <span>{initiative.name}</span>
                      <br />
                    </React.Fragment>
                  ))}
                </td>
              </tr>
            </tbody>
          </TableDetail>
        </TablePosition>
      </>
    </>
  );
};

IdeaDetailTable.defaultProps = {
  onClickEdit: null, // null for form previews
};

IdeaDetailTable.propTypes = {
  data: PropTypes.object.isRequired,
  currency: PropTypes.object.isRequired,
  onClickEdit: PropTypes.func,
};

export default IdeaDetailTable;

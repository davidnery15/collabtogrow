import React from 'react';
import { Card, Grid, Heading, Loader } from '@8base/boost';
import { CreateViewCardBody } from '../../../components/new-ui/card/CreateViewCardBody';
import InitiativeForm from './components/InitiativeForm';
import initiativeStore, {
  INITIATIVE_CREATE_EVENT,
  INITIATIVE_ERROR_EVENT,
} from './initiative-store';
import * as toast from '../../../components/toast/Toast';
import { createInitiative } from './initiative-actions';
import { withRouter } from 'react-router-dom';
import { PropTypes } from 'prop-types';
import InititativeModel, {
  INITIATIVE_DATA_STORE,
  INITIATIVE_CURRENT_DASHBOARD_DOCUMENTS,
  INITIATIVE_DOCUMENTS,
} from './initiative-model';
import View from '@cobuildlab/react-flux-state';
import { onChangeDataMixin, onErrorMixin } from '../../../shared/mixins';
import * as R from 'ramda';
import BusinessCaseForm from '../../document-management/business-case/components/BusinessCaseForm';
import BusinessCaseModel from '../../document-management/business-case/BusinessCase.model';
import InitiativeDetailTable from './components/InitiativeDetailTable';
import SubHeader from '../../../components/SubHeader';
import BusinessCaseDetailTable from '../../document-management/business-case/components/BusinessCaseDetailTable';
import { getCurrencyOnSession } from '../../../shared/alliance-utils';
import { ActionButton } from '../../../components/buttons/ActionButton';
import { FormSteps } from '../../../components/dots/FormSteps';
import { ActionButtonClose } from '../../../components/buttons/ActionButtonClose';
import { TransparentButton } from '../../../components/buttons/TransparentButton';
import { initiativeValidator } from './initiative-validators';
import { businessCaseValidator } from '../../document-management/business-case/business-case-validators';
import allianceStore, {
  ALLIANCE_LIST_MEMBERS_EVENT,
} from '../../settings/alliance-management/alliance-store';
import { fetchCurrentAllianceMembersAction } from '../../settings/alliance-management/alliance-actions';
import sessionStore, { NEW_SESSION_EVENT } from '../../../shared/SessionStore';
import { saveFormToSessionStorage } from '../../../shared/utils';
import RelatedItemForm from '../../related-item/components/RelatedItemForm';
import { RelatedItemsDetailTable } from '../../related-item/components/RelatedItemsDetailTable';
import { LeftProgressSection } from '../../../components/new-ui/LeftProgressSection';
import { SCREENS_INITIATIVE } from '../screenView';
import { BoxCard } from '../../../components/new-ui/div/BoxCard';
import { CardFooter } from '../../../components/new-ui/card/CardFooter';

class InitiativeCreateView extends View {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        initiativeData: R.clone(InititativeModel),
        businessCaseData: R.clone(BusinessCaseModel),
        relatedItems: [],
        initiatives: [],
      },
      clientCompany: null,
      partnerCompany: null,
      loading: true,
      step: 0,
      initiativesList: [],
    };
    this.onError = onErrorMixin.bind(this);
    this.onChangeData = onChangeDataMixin.bind(this);
  }

  onChangeInitiativeData = (name, value) => {
    const { data } = this.state;
    data.initiativeData[name] = value;
    this.setState({ data });
    const model = R.clone(InititativeModel);

    saveFormToSessionStorage(INITIATIVE_DATA_STORE, data.initiativeData, model, [
      'currentDashboardFile',
      'documentsFile',
    ]);
  };

  onChangeBusinessCaseData = (name, value) => {
    const { data } = this.state;
    data.businessCaseData[name] = value;
    this.setState({ data });
  };

  componentDidMount() {
    const { user } = sessionStore.getState(NEW_SESSION_EVENT);
    const initiativeData = JSON.parse(sessionStorage.getItem(INITIATIVE_DATA_STORE));

    this.subscribe(initiativeStore, INITIATIVE_ERROR_EVENT, this.onError);
    this.subscribe(allianceStore, ALLIANCE_LIST_MEMBERS_EVENT, (state) => {
      this.setState({
        loading: false,
        clientCompany: state.clientCompany,
        partnerCompany: state.partnerCompany,
      });
    });
    this.subscribe(initiativeStore, INITIATIVE_CREATE_EVENT, (state) => {
      sessionStorage.removeItem(INITIATIVE_DATA_STORE);
      toast.success('Initiative Successfully Created');
      this.props.history.goBack();
    });

    // set initiativeData from sessionStorage
    if (initiativeData) {
      const { data } = this.state;
      data.initiativeData = initiativeData;
      this.setState({ data });
    } else {
      // owner default to current user but editable
      this.onChangeInitiativeData('owner', user);
    }

    fetchCurrentAllianceMembersAction();
  }

  componentWillUnmount() {
    super.componentWillUnmount();
    localStorage.removeItem(INITIATIVE_DOCUMENTS);
    localStorage.removeItem(INITIATIVE_CURRENT_DASHBOARD_DOCUMENTS);
  }

  onSubmit = () => {
    this.setState({ loading: true }, () => {
      const initiativeData = R.clone(this.state.data.initiativeData);
      const businessCaseData = R.clone(this.state.data.businessCaseData);
      const relatedItems = R.clone(this.state.data.relatedItems);
      createInitiative(initiativeData, businessCaseData, relatedItems);
    });
  };

  onInitiativeStepChange = (step) => {
    const initiativeData = R.clone(this.state.data.initiativeData);
    try {
      initiativeValidator(initiativeData);
    } catch (e) {
      return this.onError(e);
    }
    this.onScreen(step);
  };

  onBusinessCaseStepChange = (step) => {
    const businessCaseData = R.clone(this.state.data.businessCaseData);
    try {
      businessCaseValidator(businessCaseData);
    } catch (e) {
      return this.onError(e);
    }
    this.onScreen(step);
  };

  onScreen = (step) => {
    this.setState({ step });
  };

  render() {
    const { data, step, loading, clientCompany, partnerCompany, initiativesList } = this.state;
    const { initiativeData, businessCaseData, relatedItems, initiatives } = data;
    const { history } = this.props;
    const currency = getCurrencyOnSession();
    let content = <Loader stretch />;
    let footer = <></>;
    //let headerText = '';

    if (!loading && step === 0) {
      // headerText = 'Initiative Information';
      content = (
        <InitiativeForm
          data={initiativeData}
          clientCompany={clientCompany}
          partnerCompany={partnerCompany}
          onChange={this.onChangeInitiativeData}
        />
      );
      footer = (
        <CardFooter>
          <ActionButton onClick={() => this.onInitiativeStepChange(1)} text="Next" />
        </CardFooter>
      );
    }

    if (!loading && step === 1) {
      //  headerText = 'Business Case Information';
      content = (
        <BusinessCaseForm
          data={businessCaseData}
          onChange={this.onChangeBusinessCaseData}
          currency={currency}
        />
      );
      footer = (
        <CardFooter>
          <ActionButton onClick={() => this.onBusinessCaseStepChange(2)} text="Next" />
          <TransparentButton onClick={() => this.onScreen(0)} text="Previous" />
        </CardFooter>
      );
    }

    if (!loading && step === 2) {
      //headerText = 'Related Items';

      content = (
        <RelatedItemForm
          relatedItems={relatedItems}
          initiatives={initiativesList}
          onChange={(key, value) => {
            this.onChangeData(key, value);
          }}
          selectedInitiatives={initiatives}
          allowedDealOption={false}
        />
      );

      footer = (
        <CardFooter>
          <ActionButton onClick={() => this.onScreen(3)} text={'Next'} />
          <TransparentButton onClick={() => this.onScreen(1)} text={'Previous'} />
        </CardFooter>
      );
    }

    if (!loading && step === 3) {
      const initiativeDetail = R.clone(initiativeData);
      //headerText = 'Preview Create Initiative';

      content = (
        <>
          <InitiativeDetailTable data={initiativeDetail} currency={currency} />
          <SubHeader text="Business Case" status={initiativeDetail.status} />
          <BusinessCaseDetailTable data={businessCaseData} currency={currency} />
          <RelatedItemsDetailTable relatedItems={relatedItems} />
        </>
      );
      footer = (
        <CardFooter>
          <ActionButton onClick={() => this.onSubmit()} text="Create Initiative" />
          <TransparentButton onClick={() => this.onScreen(2)} text="Previous" />
        </CardFooter>
      );
    }

    return (
      <React.Fragment>
        <Card.Header>
          <Grid.Layout
            columns="200px auto 200px"
            areas={[['left', 'center', 'right']]}
            style={{ width: '100%' }}>
            <Grid.Box area="left">
              <Heading type="h4" text="Create Initiative" />
            </Grid.Box>
            <Grid.Box area="center">
              <FormSteps totalSteps={4} step={step} />
            </Grid.Box>
            <Grid.Box area="right" />
          </Grid.Layout>
          <ActionButtonClose onClick={history.goBack} />
        </Card.Header>
        <CreateViewCardBody>
          <Grid.Layout columns="30% 70%" areas={[['left', 'right']]} style={{ width: '100%' }}>
            <Grid.Box area="left">
              <LeftProgressSection sections={SCREENS_INITIATIVE} currentScreen={step} />
            </Grid.Box>
            <BoxCard>
              <Grid.Box area="right">{content}</Grid.Box>
            </BoxCard>
          </Grid.Layout>
        </CreateViewCardBody>
        {footer}
      </React.Fragment>
    );
  }
}

InitiativeCreateView.propTypes = {
  history: PropTypes.object.isRequired,
};

export default withRouter(InitiativeCreateView);

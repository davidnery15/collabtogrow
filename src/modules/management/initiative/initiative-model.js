import moment from 'moment';

const currentDate = moment().format('YYYY-MM-DD');

export default {
  id: null,
  name: '',
  description: '',
  currentDashboardFile: null,
  documentsFile: [],
  businessCase: null,
  owner: null,
  alliance: null,
  baselineStartDate: '',
  baselineEndDate: '',
  forecastedEndDate: null,
  requestedDate: currentDate,
  requestedBy: null,
  ragStatus: null,
};

export const INITIATIVE_DATA_STORE = 'initiativeCreateView';
export const INITIATIVE_CURRENT_DASHBOARD_DOCUMENTS = 'initiativeCurrentDashboardDocuments';
export const INITIATIVE_DOCUMENTS = 'initiativeDocuments';

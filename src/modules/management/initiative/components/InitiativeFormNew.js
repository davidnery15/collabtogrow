import React, { Component } from 'react';
import { InputField, TextAreaField, Label, DateInputField, Row, Column } from '@8base/boost';
import PropTypes from 'prop-types';
import { onErrorMixin } from '../../../../shared/mixins';
import FileInputComponent from '../../../../components/new-ui/inputs/FileInputComponentNew';
import { CurrencyInputField } from '../../../../shared/components/CurrencyInputField';
import { SelectInputById } from '../../../../components/forms/SelectInputById';
import { concatClientAndPartnerUsers } from 'shared/alliance-utils';
import { INITIATIVE_DOCUMENTS, INITIATIVE_CURRENT_DASHBOARD_DOCUMENTS } from '../initiative-model';
import YesNoDialog from '../../../../components/dialogs/YesNoDialog';
import { HorizontalLineText } from '../../../../components/new-ui/text/HorizontalLineText';
import { BorderForm } from '../../../../components/new-ui/div/BorderForm';
import { ColumnLeft } from '../../../../components/new-ui/ColumnLeft';
import { ColumnRight } from '../../../../components/new-ui/ColumnRight';
import '../../../../components/new-ui/forms.css';

/**
 * The Form for the Initiative entity
 */
class InitiativeForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deleteDocumentModalIsOpen: false,
      deleteDocumentPos: null,
    };
    this.onClickDelete = this.onClickDelete.bind(this);
    this.onChangeDocuments = this.onChangeDocuments.bind(this);
    this.nameInput = null;
  }

  onClickDelete = (pos, name) => {
    this.nameInput = name;
    this.setState({
      deleteDocumentModalIsOpen: true,
      deleteDocumentPos: pos,
    });
  };

  onYes = () => {
    this.setState(
      {
        deleteDocumentModalIsOpen: false,
      },
      () => {
        this.onDeleteDocuments(this.nameInput, this.state.deleteDocumentPos);
      },
    );
  };

  onClose = () => {
    this.setState({
      deleteDocumentModalIsOpen: false,
    });
  };

  onChangeDocuments(name, value) {
    const initiativeData = this.props.data;

    if (name === 'documentsFile') {
      for (let aux of value) {
        initiativeData[name].push(aux);
      }
    } else {
      initiativeData[name] = value;
    }

    this.props.onChange(name, initiativeData[name]);
  }

  onDeleteDocuments = (name, pos) => {
    const initiativeData = this.props.data;

    if (name === 'documentsFile') {
      initiativeData[name].splice(pos, 1);
      localStorage.setItem(INITIATIVE_DOCUMENTS, JSON.stringify(initiativeData[name]));
    } else if (name === 'currentDashboardFile') {
      initiativeData[name] = null;
      localStorage.setItem(
        INITIATIVE_CURRENT_DASHBOARD_DOCUMENTS,
        JSON.stringify(initiativeData[name]),
      );
    }
    this.setState({ deleteDocumentPos: null });
    this.props.onChange(name, initiativeData[name]);
  };

  render() {
    const { deleteDocumentModalIsOpen } = this.state;
    const { onChange, clientCompany, partnerCompany, currency, headerText } = this.props;
    const {
      name,
      description,
      budgetUtilized,
      currentDashboardFile,
      documentsFile,
      baselineStartDate,
      baselineEndDate,
      forecastedEndDate,
      requestedBy,
      requestedDate,
      owner,
    } = this.props.data;
    // concat client and partner users
    const users = concatClientAndPartnerUsers(clientCompany, partnerCompany);

    return (
      <>
        {/* General */}
        <BorderForm>
          <HorizontalLineText text={headerText} />
          <Row>
            <ColumnLeft text="General" />
            <ColumnRight>
              <InputField
                label="Name"
                placeholder={'Name'}
                input={{
                  name: 'name',
                  value: name,
                  onChange: (value, e) => onChange(e.target.name, value),
                }}
              />
              <TextAreaField
                stretch
                rows={7}
                label="Description"
                placeholder={'Description'}
                input={{
                  name: 'description',
                  value: description,
                  onChange: (value) => onChange('description', value),
                }}
              />
              <SelectInputById
                stretch
                label="Owner"
                input={{
                  name: 'owner',
                  value: owner,
                  onChange: (value) => onChange('owner', value),
                }}
                meta={{}}
                placeholder="Select"
                options={users.map((user) => {
                  return {
                    label: `${user.firstName} ${user.lastName}`,
                    value: user,
                  };
                })}
              />
            </ColumnRight>
          </Row>
        </BorderForm>

        {/* Dates */}
        <BorderForm>
          <Row>
            <ColumnLeft text="Dates" />
            <ColumnRight>
              <Row growChildren gap="lg">
                <Column>
                  <DateInputField
                    label="Requested Date"
                    placeholder="MM/DD/YYYY"
                    input={{
                      name: 'requestedDate',
                      value: requestedDate,
                      onChange: (value) => onChange('requestedDate', value),
                    }}
                  />
                  <DateInputField
                    // style={{ width: '100%', marginBottom: '10px' }}
                    label="Baseline Start Date"
                    placeholder="MM/DD/YYYY"
                    input={{
                      value: baselineStartDate,
                      onChange: (value) => onChange('baselineStartDate', value),
                    }}
                  />
                  <DateInputField
                    // style={{ width: '100%', marginBottom: '10px' }}
                    label="Forecasted End Date"
                    placeholder="MM/DD/YYYY"
                    input={{
                      name: 'forecastedEndDate',
                      value: forecastedEndDate,
                      onChange: (value) => onChange('forecastedEndDate', value),
                    }}
                  />
                </Column>
                <Column>
                  <SelectInputById
                    label="Requested By"
                    // style={{ width: '100%', marginBottom: '10px' }}
                    input={{
                      name: 'requestedBy',
                      value: requestedBy,
                      onChange: (value) => onChange('requestedBy', value),
                    }}
                    meta={{}}
                    placeholder="Select"
                    options={users.map((user) => {
                      return {
                        label: `${user.firstName} ${user.lastName}`,
                        value: user,
                      };
                    })}
                  />
                  <DateInputField
                    // style={{ width: '100%', marginBottom: '10px' }}
                    label="Baseline End Date"
                    placeholder="MM/DD/YYYY"
                    input={{
                      name: 'baselineEndDate',
                      value: baselineEndDate,
                      onChange: (value) => onChange('baselineEndDate', value),
                    }}
                  />
                  <CurrencyInputField
                    // style={{ width: '100%', marginBottom: '10px' }}
                    currency={currency}
                    label={'Budget Utilized'}
                    value={budgetUtilized}
                    onChange={(val) => {
                      onChange('budgetUtilized', val);
                    }}
                  />
                </Column>
              </Row>
            </ColumnRight>
          </Row>
        </BorderForm>

        {/* Documents */}
        <BorderForm>
          <HorizontalLineText text={'DOCUMENTS'} />
          <Row growChildren gap="lg">
            <Column>
              <Label kind="secondary" style={{ textAlign: 'left' }}>
                Status Report File (Optional)
              </Label>
              <FileInputComponent
                onChange={this.onChangeDocuments}
                nameInput={'currentDashboardFile'}
                field={currentDashboardFile}
                maxFiles={1}
                localKey={INITIATIVE_CURRENT_DASHBOARD_DOCUMENTS}
                text={'Add Document'}
                onClickDelete={(pos) => this.onClickDelete(pos, 'currentDashboardFile')}
              />
            </Column>
            <Column>
              <Label kind="secondary" style={{ textAlign: 'left' }}>
                Documents File (Optional)
              </Label>
              <FileInputComponent
                onChange={this.onChangeDocuments}
                nameInput={'documentsFile'}
                field={documentsFile}
                maxFiles={5}
                localKey={INITIATIVE_DOCUMENTS}
                text={'Add Document'}
                onClickDelete={(pos) => this.onClickDelete(pos, 'documentsFile')}
              />
            </Column>
          </Row>
        </BorderForm>

        <YesNoDialog
          isOpen={deleteDocumentModalIsOpen}
          onYes={this.onYes}
          onNo={this.onClose}
          onClose={this.onClose}
          text={'Are you sure you want to Delete this Document?'}
          title={'Delete Document'}
        />
      </>
    );
  }
}

Object.assign(InitiativeForm.prototype, onErrorMixin);

InitiativeForm.propTypes = {
  data: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  currency: PropTypes.object.isRequired,
  clientCompany: PropTypes.object.isRequired,
  partnerCompany: PropTypes.object.isRequired,
  headerText: PropTypes.string.isRequired,
};

export default InitiativeForm;

import React from 'react';
import PropTypes from 'prop-types';
import DetailValue from '../../../../components/DetailValue';
import DetailDateValue from '../../../../components/DetailDateValue';
import Status from '../../../../components/Status';
import { CurrencyTextField } from '../../../../shared/components/CurrencyTextField';
import UserDetailValue from 'components/UserDetailValue';
import DocumentsFileComponent from '../../../../components/inputs/DocumentsFileComponent';
import { INITIATIVE_DOCUMENTS, INITIATIVE_CURRENT_DASHBOARD_DOCUMENTS } from '../initiative-model';
import { HorizontalLineText } from '../../../../components/new-ui/text/HorizontalLineText';
import { HeaderText } from '../../../../components/new-ui/text/HeaderText';
import { FlexTopDiv } from '../../../../components/new-ui/div/FlexTopDiv';
import { EditButton } from '../../../../components/new-ui/buttons/EditButton';
import styled from '@emotion/styled';

const StyledTable = styled('table')({
  'font-family': 'Trebuchet MS',
  borderCollapse: 'collapse',
  width: '70% !important',
  margin: '0 auto',
  'text-align': 'left',
  border: '1px solid rgb(241, 241, 241)',
  'padding-bottom': '10px',
});

const Th = styled('th')({
  border: '1px solid rgb(241, 241, 241)',
  padding: '10px 25px!important',
  height: '1.78 %',
  width: '7.85 %',
  color: '#7E828B!important',
  'font-family': 'Poppins',
  'font-size': '12px',
  'font-weight': 'bold',
  'letter-spacing': '0.5px',
  'line-height': '16px',
  backgroundColor: '#edf0f2',
});

const Td = styled('td')({
  height: '6%',
  width: '73.19%',
  color: '#323C47',
  'font-family': 'Poppins',
  'font-size': '13px',
  'line-height': '18px',
});

/**
 * Detail View Table For The Initiative Entity
 */
class InitiativeDetailTable extends React.Component {
  render() {
    const { data, currency, headerText, onClickEdit } = this.props;
    const {
      name,
      status,
      description,
      createdAt,
      budgetUtilized,
      documentsFile,
      currentDashboardFile,
      baselineStartDate,
      baselineEndDate,
      forecastedEndDate,
      owner,
      requestedBy,
      requestedDate,
    } = data;

    return (
      <>
        <div>
          <HorizontalLineText text={headerText} />
        </div>
        <FlexTopDiv>
          <HeaderText> CREATE INITIATIVE </HeaderText>
          {onClickEdit ? <EditButton onClick={onClickEdit} text="Edit" /> : null}
        </FlexTopDiv>
        <StyledTable className="details">
          <tbody>
            <tr>
              <Th>Status</Th>
              <Td className="spacing-td">
                <Status status={status} />
              </Td>
            </tr>
            <tr>
              <Th>Name</Th>
              <Td>
                <DetailValue text={name} />
              </Td>
            </tr>
            <tr>
              <Th>Description</Th>
              <Td>
                <DetailValue text={description} />
              </Td>
            </tr>
            <tr>
              <Th>Owner</Th>
              <Td>
                <UserDetailValue user={owner} />
              </Td>
            </tr>
            <tr>
              <Th>Documents</Th>
              <Td>
                <DocumentsFileComponent data={documentsFile} localKey={INITIATIVE_DOCUMENTS} />
              </Td>
            </tr>
            <tr>
              <Th>Status Report File</Th>
              <Td>
                <DocumentsFileComponent
                  data={currentDashboardFile}
                  localKey={INITIATIVE_CURRENT_DASHBOARD_DOCUMENTS}
                />
              </Td>
            </tr>
            <tr>
              <Th>Requested Date</Th>
              <Td>
                <DetailDateValue date={requestedDate} />
              </Td>
            </tr>
            <tr>
              <Th>Requested By</Th>
              <Td>
                <UserDetailValue user={requestedBy} />
              </Td>
            </tr>
            <tr>
              <Th>Baseline Start Date</Th>
              <Td>
                <DetailDateValue date={baselineStartDate} />
              </Td>
            </tr>
            <tr>
              <Th>Baseline End Date</Th>
              <Td>
                <DetailDateValue date={baselineEndDate} />
              </Td>
            </tr>
            <tr>
              <Th>Forecasted End Date</Th>
              <Td>
                <DetailDateValue date={forecastedEndDate} />
              </Td>
            </tr>
            <tr>
              <Th>Craeted at</Th>
              <Td>
                <DetailDateValue date={createdAt} />
              </Td>
            </tr>
            <tr>
              <Th>Budget Utilized</Th>
              <Td>
                <CurrencyTextField value={budgetUtilized} currency={currency} />
              </Td>
            </tr>
          </tbody>
        </StyledTable>
      </>
    );
  }
}

InitiativeDetailTable.defaultProps = {
  onClickEdit: null, // null for form previews
};

InitiativeDetailTable.propTypes = {
  data: PropTypes.object.isRequired,
  currency: PropTypes.object.isRequired,
  headerText: PropTypes.string.isRequired,
  onClickEdit: PropTypes.func,
};

export default InitiativeDetailTable;

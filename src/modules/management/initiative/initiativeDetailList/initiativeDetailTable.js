import React from 'react';
import PropTypes from 'prop-types';
import { InitiativeDetailRow } from './component/initiativeDetailRow';
class InitiativeDetailTable extends React.Component {
  render() {
    const { initiatives } = this.props;
    let content = null;
    console.log('initiatives', initiatives);

    if (initiatives.items.length) {
      content = (
        <>
          {initiatives.items.map((initiative, i) => (
            <InitiativeDetailRow data={initiative} key={i} />
          ))}
        </>
      );
    }

    return (
      <>
        <table className="details">
          <tbody>{content}</tbody>
        </table>
      </>
    );
  }
}

InitiativeDetailTable.propTypes = {
  initiatives: PropTypes.array.isRequired,
};

export default InitiativeDetailTable;

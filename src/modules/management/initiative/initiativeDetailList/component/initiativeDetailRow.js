import React from 'react';
import { Text } from '@8base/boost';
import { PropTypes } from 'prop-types';
import styled from '@emotion/styled';
import UserSmallAvatar from '../../../../../components/user/UserSmallAvatar';
import moment from 'moment/moment';
import {
  TRANSPARENT_COLOR,
  GREEN_COLOR,
  YELLOW_COLOR,
  RED_COLOR,
} from '../../../../../shared/colors';
import Moment from 'react-moment/dist/index';

const StyledDate = styled('div')`
  border-left: 1px solid #d0d7dd;
  border-right: 1px solid #d0d7dd;
  height: 100%;
  padding: 20px 10px;
  max-width: 20%;
  min-width: 20%;
  text-align: center;
`;

const StyledUserSmallAvatar = styled('div')`
  width: 80%;
  padding: 10px;
`;

const InitiativeDetailRow = (props) => {
  const { data } = props;

  let color = TRANSPARENT_COLOR;
  const { owner, name } = data;

  const baselineEndDate = data.baselineEndDate ? (
    <Moment format="MMMM Do, YYYY">{data.baselineEndDate}</Moment>
  ) : (
    <Text>Not set</Text>
  );

  if (baselineEndDate) {
    if (moment().isSameOrAfter(moment(baselineEndDate))) color = RED_COLOR;
    else {
      if (moment().diff(moment(baselineEndDate), 'weeks') === 0) color = YELLOW_COLOR;

      if (moment().diff(moment(baselineEndDate), 'weeks') < 0) color = GREEN_COLOR;
    }
  } else {
    color = RED_COLOR;
  }
  const StyledContainer = styled('div')`
    border: 1px solid #d0d7dd;
    width: 100%;
    position: relative;
    height: 60px;
    border-radius: 0.5rem;
    border-left: 8px solid ${color};
    margin: 5px 0;
    display: flex;
  `;

  return (
    <StyledContainer>
      <StyledUserSmallAvatar>
        <Text text={name} />
        <UserSmallAvatar owner={owner} />
      </StyledUserSmallAvatar>
      <StyledDate>{baselineEndDate}</StyledDate>
    </StyledContainer>
  );
};

InitiativeDetailRow.propTypes = {
  data: PropTypes.object.isRequired,
};

export { InitiativeDetailRow };

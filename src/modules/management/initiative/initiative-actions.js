import sessionStore, { APOLLO_CLIENT, NEW_SESSION_EVENT } from '../../../shared/SessionStore';
import Flux from '@cobuildlab/flux-state';
import {
  INITIATIVE_ERROR_EVENT,
  INITIATIVE_LIST_EVENT,
  INITIATIVE_FULL_LIST_EVENT,
  INITIATIVE_CREATE_EVENT,
  INITIATIVE_DETAIL_EVENT,
  INITIATIVE_UPDATE_EVENT,
  INITIATIVE_REJECT_EVENT,
  INITIATIVE_DELETE_EVENT,
  INITIATIVE_COMPLETED_EVENT,
  INITIATIVE_SUBMIT_FOR_APPOVAL_EVENT,
  INITIATIVE_RESTORE_EVENT,
} from './initiative-store';
import {
  INITIATIVE_LIST_QUERY,
  INITIATIVE_FULL_LIST_QUERY,
  INITIATIVE_CREATE_MUTATION,
  INITIATIVE_UPDATE_MUTATION,
  INITIATIVE_DETAIL_QUERY,
  INITIATIVE_APPROVALS_LIST_QUERY,
  INITIATIVE_INITIATIVE_APPROVAL_MUTATION,
  INITIATIVE_APPROVAL_UPDATE_MUTATION,
  INITIATIVE_DELETE_MUTATION,
  INITIATIVE_COMMENTS_QUERY,
} from './initiative-queries';
import {
  COMMENT_CREATE_EVENT,
  COMMENT_ERROR_EVENT,
  COMMENT_REQUEST_EVENT,
  COMMENTS_EVENT,
} from '../../comment/comment-store';
import { COMMENTS_CREATE_MUTATION } from '../../comment/comment-queries';
import { INITIATIVE_TYPE } from '../../../shared/item-types';
import { initiativeValidator } from './initiative-validators';
import { IntegrityError } from '../../../shared/errors';
import { log, error } from '@cobuildlab/pure-logger';
import {
  sanitize8BaseDocumentCreate,
  sanitize8BaseDocumentsCreate,
  sanitize8BaseDate,
  sanitize8BaseDocumentUpdate,
  sanitize8BaseDocumentsDeleteAndUpdate,
  sanitize8BaseReferenceFromObject,
  sanitize8BaseReference,
  isValidString,
} from '../../../shared/utils';
import {
  sanitizeRecommendedSolutions,
  updateBusinessCase,
  businessCaseSearchFilterOR,
} from '../../document-management/business-case/businessCases.actions';
import { getCompanyOnAlliance, getActiveAllianceId } from '../../../shared/alliance-utils';
import {
  canEditInitiative,
  canApproveInitiative,
  canRejectInitiative,
  canSubmitForApprovalInitiative,
  canDeleteInitiative,
  canCompletedInitiative,
  canRestoreInitiative,
} from './initiative-permissions';
import {
  INITIATIVE_APPROVAL_APPROVED,
  INITIATIVE_APPROVAL_REJECTED,
  INITIATIVE_APPROVED,
  INITIATIVE_REJECTED,
  INITIATIVE_COMPLETED,
  INITIATIVE_IN_PROGRESS,
} from '../../../shared/status';
import { businessCaseValidator } from '../../document-management/business-case/business-case-validators';
import { getInitiativeItemByType } from '../../../shared/initiative-item-util';
import { filterForYear } from '../../../shared/utils';

/**
 * Creates a filter object and search by a string
on string properties of the initiative.
 *
 * @param {string} [search=''] - The string to search.
 * @param {string} allianceId -  The allianceId to filter.
 * @param status
 * @param filterYear
 * @param {object} year
 * @returns {object}             The filter object.
 */
const initiativeFilter = (allianceId, search = '', status = '', filterYear = null) => {
  const filter = {
    alliance: { id: { equals: allianceId } },
    OR: [
      {
        name: {
          contains: search,
        },
      },
      {
        description: {
          contains: search,
        },
      },
      {
        owner: {
          firstName: { contains: search },
        },
      },
      {
        owner: {
          lastName: { contains: search },
        },
      },
      {
        owner: {
          email: { contains: search },
        },
      },
    ].concat(businessCaseSearchFilterOR(search)),
  };

  if (filterYear) {
    const filterForYearData = filterForYear(
      filterYear.year,
      filterYear.fieldName,
      filterYear.isDateTime,
    );

    if (filterYear.fieldName === 'dateOfResponse') {
      filter.initiativeApprovalInitiativeRelation = {
        some: {
          AND: filterForYearData.AND,
        },
      };
    } else {
      filter.AND = filterForYearData.AND;
    }
  }

  if (status) {
    filter.status = { equals: status };
  }

  return filter;
};

/**
 * Fetches the Initiative list.
 *
 * @param {string} [search=''] - Search by name.
 * @param page
 * @param first
 * @param status
 * @param filterYear
 * @returns {Promise}
 */
export const fetchInitiativeList = async (
  search = '',
  page = 1,
  first = 20,
  status,
  filterYear = null,
) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const user = sessionStore.getState(NEW_SESSION_EVENT).user;
  const allianceId = getActiveAllianceId(user);
  const skip = (page - 1) * first;

  log('allianceId', allianceId);
  if (!isValidString(allianceId))
    return Flux.dispatchEvent(
      INITIATIVE_ERROR_EVENT,
      new IntegrityError('An Alliance must be selected'),
    );

  const filter = initiativeFilter(allianceId, search, status, filterYear);

  let response;
  try {
    response = await client.query({
      query: INITIATIVE_LIST_QUERY,
      fetchPolicy: 'network-only',
      variables: { data: filter, skip, first },
    });
    response.data.page = page;
    response.data.first = first;
    response.data.search = search;
  } catch (e) {
    error('fetchInitiativeList', e);
    Flux.dispatchEvent(INITIATIVE_ERROR_EVENT, e);
    throw e;
  }

  log('fetchInitiativeList', response);
  Flux.dispatchEvent(INITIATIVE_LIST_EVENT, response.data);
  return response.data;
};

/**
 * Fetches a list of all Initiatives with a minimum data
 * for use as options in the Select component.
 */

export const fetchInitiativeFullList = async (search = '') => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const user = sessionStore.getState(NEW_SESSION_EVENT).user;
  const allianceId = getActiveAllianceId(user);

  log('allianceId', allianceId);
  if (!isValidString(allianceId))
    return Flux.dispatchEvent(
      INITIATIVE_ERROR_EVENT,
      new IntegrityError('An Alliance must be selected'),
    );

  const filter = initiativeFilter(allianceId, search);

  let response;
  try {
    response = await client.query({
      query: INITIATIVE_FULL_LIST_QUERY,
      fetchPolicy: 'network-only',
      variables: { data: filter },
    });
  } catch (e) {
    error('fetchAllInitiativeList', e);
    return Flux.dispatchEvent(INITIATIVE_ERROR_EVENT, e);
  }

  log('fetchAllInitiativeList', response);
  Flux.dispatchEvent(INITIATIVE_FULL_LIST_EVENT, response.data);
  return response.data;
};

/**
 * Fetches the Initiative list where
 * - status is equal to Approved.
 */
export const fetchInitiativeListApproved = async () => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const user = sessionStore.getState(NEW_SESSION_EVENT).user;
  const allianceId = getActiveAllianceId(user);

  log('allianceId', allianceId);
  if (!isValidString(allianceId))
    return Flux.dispatchEvent(
      INITIATIVE_ERROR_EVENT,
      new IntegrityError('An Alliance must be selected'),
    );
  const allianceFilter = {
    alliance: { id: { equals: allianceId } },
    status: { equals: INITIATIVE_APPROVED },
  };

  let response;
  try {
    response = await client.query({
      query: INITIATIVE_LIST_QUERY,
      fetchPolicy: 'network-only',
      variables: { data: allianceFilter },
    });
  } catch (e) {
    error('fetchInitiativeList', e);
    return Flux.dispatchEvent(INITIATIVE_ERROR_EVENT, e);
  }

  log('fetchInitiativeList', response);
  Flux.dispatchEvent(INITIATIVE_LIST_EVENT, response.data);
  return response.data;
};

/**
 * Creates a New Initiative
- Sets the Owner to the current logged User.
 *
 * @returns {Promise}
 * @param initiativeData
 * @param businessCaseData
 * @param relatedItems
 */
export const createInitiative = async (initiativeData, businessCaseData, relatedItems) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const user = sessionStore.getState(NEW_SESSION_EVENT).user;
  const allianceId = getActiveAllianceId(user);

  console.log('relatedItems', relatedItems);

  if (!isValidString(allianceId))
    return Flux.dispatchEvent(
      INITIATIVE_ERROR_EVENT,
      new IntegrityError('An Alliance must be selected'),
    );

  log('createInitiative', initiativeData, businessCaseData, relatedItems);
  try {
    initiativeValidator(initiativeData);
    businessCaseValidator(businessCaseData);
  } catch (e) {
    error('validateInitiativeForDraft', e);
    return Flux.dispatchEvent(INITIATIVE_ERROR_EVENT, e);
  }

  const initiativeRelatedItems = sanitizeConnectInitiativeItemRelation(relatedItems);
  // add connect fields
  initiativeData.alliance = allianceId;
  businessCaseData.owner = user.id;
  // Sanitize 8base initiativeData fields
  sanitize8BaseReference(initiativeData, 'alliance');
  sanitize8BaseReferenceFromObject(initiativeData, 'owner');
  sanitize8BaseReferenceFromObject(initiativeData, 'requestedBy');
  sanitize8BaseDocumentCreate(initiativeData, 'currentDashboardFile');
  sanitize8BaseDocumentsCreate(initiativeData, 'documentsFile');
  sanitize8BaseDate(initiativeData, 'baselineStartDate');
  sanitize8BaseDate(initiativeData, 'baselineEndDate');
  sanitize8BaseDate(initiativeData, 'forecastedEndDate');
  sanitize8BaseDate(initiativeData, 'requestedDate');
  // Sanitize 8base businessCaseData fields
  sanitize8BaseReference(businessCaseData, 'owner');
  sanitize8BaseDocumentCreate(businessCaseData, 'document');
  sanitizeRecommendedSolutions(businessCaseData);
  initiativeData.businessCase = {
    create: businessCaseData,
  };

  const data = {
    ...initiativeData,
    ...initiativeRelatedItems,
  };

  // Delete Unnecessary fields
  delete data.id;
  delete businessCaseData.id;
  let response;
  try {
    response = await client.mutate({
      mutation: INITIATIVE_CREATE_MUTATION,
      variables: { data: data },
    });
  } catch (e) {
    error('createInitiative', e);
    return Flux.dispatchEvent(INITIATIVE_ERROR_EVENT, e);
  }
  Flux.dispatchEvent(INITIATIVE_CREATE_EVENT, response.data);
  return response.data;
};

/**
 * This method is to ensure retro compatibility with previous versions of the Initiative.
 *
 * @param initiativeId
 * @returns {Promise<void>}
 */
export const createItemRelationForInitiative = async (initiativeId) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const user = sessionStore.getState(NEW_SESSION_EVENT).user;
  const allianceId = getActiveAllianceId(user);
  const data = {
    itemInitiativeRelation: { create: { alliance: { connect: { id: allianceId } } } },
  };
  let response;
  try {
    response = await client.mutate({
      mutation: INITIATIVE_UPDATE_MUTATION,
      variables: { data },
    });
  } catch (e) {
    error('updateInitiative', e);
    return Flux.dispatchEvent(INITIATIVE_ERROR_EVENT, e);
  }
  log('updateInitiative', response);
};

/**
 * Updates an Initiative.
 *
 * @returns {Promise}
 * @param initiativeData
 * @param businessCaseData
 * @param relatedItems
 * @param originalRecommendedSolutions
 * @param originalDocuments
 */
export const updateInitiative = async (
  initiativeData,
  businessCaseData,
  relatedItems,
  originalRecommendedSolutions,
  originalDocuments,
) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const user = sessionStore.getState(NEW_SESSION_EVENT).user;
  const allianceId = getActiveAllianceId(user);

  log(
    'updateInitiative',
    initiativeData,
    businessCaseData,
    originalRecommendedSolutions,
    originalDocuments,
  );

  if (!canEditInitiative(user, initiativeData, { id: allianceId }))
    return Flux.dispatchEvent(
      INITIATIVE_ERROR_EVENT,
      new IntegrityError(`Permission Denied. Can't Edit this Initiative`),
    );

  delete initiativeData.itemInitiativeRelation;
  delete initiativeData.__typename;
  delete initiativeData.status;
  delete initiativeData.createdAt;
  delete initiativeData.createdBy;
  delete initiativeData.alliance;
  delete initiativeData.businessCase;
  delete initiativeData.initiativeApprovalInitiativeRelation;
  delete businessCaseData.__typename;
  delete businessCaseData.owner;
  delete businessCaseData.status;
  delete businessCaseData.createdAt;
  delete businessCaseData.createdBy;
  delete businessCaseData.recommendedSolutionsRelation;

  try {
    initiativeValidator(initiativeData);
    businessCaseValidator(businessCaseData);
  } catch (e) {
    error('validateInitiative', e);
    return Flux.dispatchEvent(INITIATIVE_ERROR_EVENT, e);
  }

  // Sanitize 8base initiativeData fields
  sanitize8BaseDocumentUpdate(initiativeData, 'currentDashboardFile');
  sanitize8BaseDocumentsDeleteAndUpdate(initiativeData, 'documentsFile', originalDocuments);
  sanitize8BaseDate(initiativeData, 'baselineStartDate');
  sanitize8BaseDate(initiativeData, 'baselineEndDate');
  sanitize8BaseDate(initiativeData, 'forecastedEndDate');
  sanitize8BaseDate(initiativeData, 'requestedDate');
  sanitize8BaseReferenceFromObject(initiativeData, 'requestedBy');
  sanitize8BaseReferenceFromObject(initiativeData, 'businessCase');
  sanitize8BaseReferenceFromObject(initiativeData, 'owner');

  const initiativeRelatedItems = sanitizeReconnectInitiativeItemRelation(relatedItems);

  const data = {
    ...initiativeData,
    ...initiativeRelatedItems,
  };

  console.log('data', data);

  let response;
  try {
    response = await client.mutate({
      mutation: INITIATIVE_UPDATE_MUTATION,
      variables: { data: data },
    });
  } catch (e) {
    error('updateInitiative', e);
    return Flux.dispatchEvent(INITIATIVE_ERROR_EVENT, e);
  }
  log('updateInitiative', response);

  try {
    await updateBusinessCase(businessCaseData, originalRecommendedSolutions);
  } catch (e) {
    error('updateInitiative', e, initiativeData);
    return Flux.dispatchEvent(INITIATIVE_ERROR_EVENT, e);
  }

  Flux.dispatchEvent(INITIATIVE_UPDATE_EVENT, response.data);
  return response.data;
};

/**
 * Fetch a single Initiative.
 *
 * @param id - Initiative id.
 *  @returns {Promise}
 */
export const fetchInitiative = async (id) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  let response;
  try {
    response = await client.query({
      query: INITIATIVE_DETAIL_QUERY,
      variables: { id },
      fetchPolicy: 'network-only',
    });
  } catch (e) {
    error('fetchInitiative', e);
    return Flux.dispatchEvent(INITIATIVE_ERROR_EVENT, e);
  }
  log('fetchInitiative', response);
  Flux.dispatchEvent(INITIATIVE_DETAIL_EVENT, response.data);
  return response.data;
};

/**
 * Request approval for a initiative.
 *
 * @param  {object}  initiative - Initiative.
 * @returns {Promise}
 */
export const requestApprovalForInitiative = async (initiative) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { user, selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);

  if (!canSubmitForApprovalInitiative(user, initiative, selectedAlliance))
    return Flux.dispatchEvent(
      INITIATIVE_ERROR_EVENT,
      new IntegrityError(`Permission Denied. Can't submit this Initiative for approval`),
    );

  log('requestApprovalForInitiative data', initiative);
  try {
    initiativeValidator(initiative);
    businessCaseValidator(initiative.businessCase);
  } catch (e) {
    error('requestApprovalForInitiative', e);
    return Flux.dispatchEvent(INITIATIVE_ERROR_EVENT, e);
  }

  const data = {
    id: initiative.id,
    status: 'SUBMITTED FOR APPROVAL',
    initiativeApprovalInitiativeRelation: {
      create: [
        { company: { connect: { id: initiative.alliance.clientCompany.id } } },
        { company: { connect: { id: initiative.alliance.partnerCompany.id } } },
      ],
    },
  };

  let response;
  try {
    response = await client.mutate({
      mutation: INITIATIVE_UPDATE_MUTATION,
      variables: { data },
    });
  } catch (e) {
    error('requestApprovalForInitiative', e);
    return Flux.dispatchEvent(INITIATIVE_ERROR_EVENT, e);
  }
  log('requestApprovalForInitiative', response);
  Flux.dispatchEvent(INITIATIVE_SUBMIT_FOR_APPOVAL_EVENT, response.data);
  return response.data;
};

/**
 * Fetches the Initiative approvals for the User.
 *
 * @param  {string}  initiativeId - InitiativeId.
 * @returns {Promise}
 */
export const fetchInitiativeApprovals = async (initiativeId) => {
  const client = sessionStore.getState(APOLLO_CLIENT);

  const data = { initiative: { id: { equals: initiativeId } } };

  let response;
  try {
    response = await client.query({
      query: INITIATIVE_APPROVALS_LIST_QUERY,
      variables: { data },
      fetchPolicy: 'network-only',
    });
  } catch (e) {
    error('fetchInitiativeApprovals', e);
    return Flux.dispatchEvent(INITIATIVE_ERROR_EVENT, e);
  }
  log('fetchInitiativeApprovals', response);
  return response.data;
};

/**
 * Approve and Initiative.
 *
 * @param initiative
 * @param {string} initiativeId - InitiativeId.
 * @returns {Promise}
 */
export const approveInitiative = async (initiative) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const user = sessionStore.getState(NEW_SESSION_EVENT).user;
  const initiativeId = initiative.id;
  const allianceId = getActiveAllianceId(user);
  const company = getCompanyOnAlliance(user, { id: allianceId });

  if (!canApproveInitiative(user, initiative, { id: allianceId }))
    return Flux.dispatchEvent(
      INITIATIVE_ERROR_EVENT,
      new IntegrityError(`Permission Denied. Can't approve this Initiative`),
    );

  // We fetch the approvals to see what's gonna happend
  const { initiativeApprovalsList } = await fetchInitiativeApprovals(initiativeId);
  if (initiativeApprovalsList.count < 2) {
    return Flux.dispatchEvent(
      INITIATIVE_ERROR_EVENT,
      new IntegrityError('Initiative must have have at least 2 request for approvals'),
    );
  }

  let approvalsCounter = 0;
  let approvalId = null;
  initiativeApprovalsList.items.forEach((approval) => {
    if (approval.status === INITIATIVE_APPROVAL_APPROVED) approvalsCounter++;
    if (approval.company.id === company.id) approvalId = approval.id;
  });
  if (approvalId === null) {
    return Flux.dispatchEvent(
      INITIATIVE_ERROR_EVENT,
      new IntegrityError("You can't approve this Initiative, your Company does not belong here."),
    );
  }

  // Update the status of the initiativeApproval
  const mutation = {
    mutation: INITIATIVE_APPROVAL_UPDATE_MUTATION,
    variables: {
      approval: {
        id: approvalId,
        status: INITIATIVE_APPROVAL_APPROVED,
        dateOfResponse: new Date(),
        approvedBy: { connect: { id: user.id } },
      },
    },
  };
  // If there are more than 0 approvals: Update the status of the initiative
  if (approvalsCounter > 0) {
    mutation.mutation = INITIATIVE_INITIATIVE_APPROVAL_MUTATION;
    mutation.variables.initiative = {
      id: initiativeId,
      status: INITIATIVE_APPROVED,
    };
  }

  let response;
  try {
    response = await client.mutate(mutation);
  } catch (e) {
    error('approveInitiative', e);
    return Flux.dispatchEvent(INITIATIVE_ERROR_EVENT, e);
  }
  log('approveInitiative', response);
  Flux.dispatchEvent(INITIATIVE_UPDATE_EVENT, response.data);
  return response.data;
};

/**
 * Reject and Initiative.
 *
 * @param initiative
 * @param {string} initiativeId - InitiativeId.
 * @returns {Promise}
 */
export const rejectInitiative = async (initiative) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const user = sessionStore.getState(NEW_SESSION_EVENT).user;
  const initiativeId = initiative.id;
  const allianceId = getActiveAllianceId(user);
  const company = getCompanyOnAlliance(user, { id: allianceId });

  if (!canRejectInitiative(user, initiative, { id: allianceId }))
    return Flux.dispatchEvent(
      INITIATIVE_ERROR_EVENT,
      new IntegrityError(`Permission Denied. Can't reject this Initiative`),
    );

  // We fetch the approvals to see what's gonna happend
  const { initiativeApprovalsList } = await fetchInitiativeApprovals(initiativeId);
  if (initiativeApprovalsList.count < 2) {
    return Flux.dispatchEvent(
      INITIATIVE_ERROR_EVENT,
      new IntegrityError('Initiative must have have at least 2 request for approvals'),
    );
  }

  let alliedCompanyApprovalId = null;
  let approvalId = null;
  initiativeApprovalsList.items.forEach((approval) => {
    if (approval.company.id === company.id) {
      approvalId = approval.id;
    } else {
      alliedCompanyApprovalId = approval.id;
    }
  });
  if (approvalId === null) {
    return Flux.dispatchEvent(
      INITIATIVE_ERROR_EVENT,
      new IntegrityError("You can't reject this Initiative, your Company does not belong here."),
    );
  }

  // Update the status of the initiativeApproval
  const userCompanyMutation = {
    mutation: INITIATIVE_INITIATIVE_APPROVAL_MUTATION,
    variables: {
      approval: {
        id: approvalId,
        status: INITIATIVE_APPROVAL_REJECTED,
        dateOfResponse: new Date(),
        approvedBy: { connect: { id: user.id } },
      },
      initiative: {
        id: initiativeId,
        status: INITIATIVE_REJECTED,
      },
    },
  };

  const alliedCompanyMutation = {
    mutation: INITIATIVE_APPROVAL_UPDATE_MUTATION,
    variables: {
      approval: {
        id: alliedCompanyApprovalId,
        status: INITIATIVE_APPROVAL_REJECTED,
        dateOfResponse: new Date(),
      },
    },
  };

  let response;
  try {
    response = await client.mutate(userCompanyMutation);
    await client.mutate(alliedCompanyMutation);
  } catch (e) {
    error('rejectInitiative', e);
    return Flux.dispatchEvent(INITIATIVE_ERROR_EVENT, e);
  }
  log('rejectInitiative', response);
  Flux.dispatchEvent(INITIATIVE_REJECT_EVENT, response.data);
  return response.data;
};

/**
 * Delete an Initiative.
 *
 * @param initiative
 * @param {string} initiativeId - The id of the Initiative to be deleted.
 * @returns {Promise}
 */
export const deleteInitiative = async (initiative) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const user = sessionStore.getState(NEW_SESSION_EVENT).user;
  const allianceId = getActiveAllianceId(user);
  const initiativeId = initiative.id;

  if (!canDeleteInitiative(user, initiative, { id: allianceId }))
    return Flux.dispatchEvent(
      INITIATIVE_ERROR_EVENT,
      new IntegrityError(`Permission Denied. Can't Delete this Initiative`),
    );

  let response;
  try {
    response = await client.mutate({
      mutation: INITIATIVE_DELETE_MUTATION,
      variables: { data: { id: initiativeId, force: true } },
    });
  } catch (e) {
    error('deleteInitiative', e);
    return Flux.dispatchEvent(INITIATIVE_ERROR_EVENT, e);
  }
  log('deleteInitiative', response);
  Flux.dispatchEvent(INITIATIVE_DELETE_EVENT, response.data);
  return response.data;
};

/**
 * Close a Initiative.
 *
 * @returns {Promise}
 * @param initiativeData
 */
export const completedInitiative = async (initiativeData) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { selectedAlliance, user } = sessionStore.getState(NEW_SESSION_EVENT);

  if (!canCompletedInitiative(user, initiativeData, selectedAlliance))
    return Flux.dispatchEvent(
      INITIATIVE_ERROR_EVENT,
      new IntegrityError('You do not have permission to perform this action'),
    );

  let response;
  try {
    response = await client.mutate({
      mutation: INITIATIVE_UPDATE_MUTATION,
      variables: {
        data: {
          id: initiativeData.id,
          status: INITIATIVE_COMPLETED,
        },
      },
    });
  } catch (e) {
    error('completedInitiative', e);
    return Flux.dispatchEvent(INITIATIVE_ERROR_EVENT, e);
  }

  Flux.dispatchEvent(INITIATIVE_COMPLETED_EVENT, response);
  return response.data;
};

/**
 * Notifies a Request for Comments for a Initiative.
 */
export const openComments = ({ id: initiativeId }) => {
  Flux.dispatchEvent(COMMENT_REQUEST_EVENT, { type: INITIATIVE_TYPE, id: initiativeId });
};

/**
 * Create a comment on a Initiative.
 *
 * @param initiativeId
 * @param comment
 * @returns {Promise<*>}
 */
export const createInitiativeComment = async (initiativeId, comment) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const data = {
    comment,
    initiativeCommentsRelation: { connect: { id: initiativeId } },
  };

  let response;
  try {
    response = await client.mutate({
      mutation: COMMENTS_CREATE_MUTATION,
      variables: { data },
    });
  } catch (e) {
    error('createInitiativeComment', e);
    return Flux.dispatchEvent(COMMENT_ERROR_EVENT, e);
  }
  log('createInitiativeComment', response);
  Flux.dispatchEvent(COMMENT_CREATE_EVENT, response.data);
  return response.data;
};

/**
 * Fetches the Initiative Item Comments.
 *
 * @returns {Promise<void>}
 * @param initiativeId
 */
export const fetchInitiativeComments = async (initiativeId) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const data = { id: initiativeId };
  const { user } = sessionStore.getState(NEW_SESSION_EVENT);

  let response;
  try {
    response = await client.query({
      query: INITIATIVE_COMMENTS_QUERY,
      variables: data,
      fetchPolicy: 'network-only',
    });
  } catch (e) {
    error('fetchInitiativeComments', e);
    return Flux.dispatchEvent(COMMENT_ERROR_EVENT, e);
  }
  log('fetchInitiativeComments', response);

  response.data.initiative.comments.userId = user.id;

  Flux.dispatchEvent(COMMENTS_EVENT, response.data.initiative.comments);
  return response.data.initiative.comments;
};

export const getInitiativeRelationItem = (initiative) => {
  let data = [];
  const {
    actionInitiativesRelation,
    contributionInitiativesRelation,
    decisionInitiativesRelation,
    fundingRequestInitiativesRelation,
    ideaInitiativesRelation,
    issueInitiativesRelation,
    riskInitiativesRelation,
  } = initiative;

  if (actionInitiativesRelation.count !== 0) {
    actionInitiativesRelation.items.map((item) => data.push(getInitiativeItemByType(item)));
  }

  if (contributionInitiativesRelation.count !== 0) {
    contributionInitiativesRelation.items.map((item) => data.push(getInitiativeItemByType(item)));
  }

  if (decisionInitiativesRelation.count !== 0) {
    decisionInitiativesRelation.items.map((item) => data.push(getInitiativeItemByType(item)));
  }

  if (fundingRequestInitiativesRelation.count !== 0) {
    fundingRequestInitiativesRelation.items.map((item) => data.push(getInitiativeItemByType(item)));
  }

  if (ideaInitiativesRelation.count !== 0) {
    ideaInitiativesRelation.items.map((item) => data.push(getInitiativeItemByType(item)));
  }

  if (issueInitiativesRelation.count !== 0) {
    issueInitiativesRelation.items.map((item) => data.push(getInitiativeItemByType(item)));
  }

  if (riskInitiativesRelation.count !== 0) {
    riskInitiativesRelation.items.map((item) => data.push(getInitiativeItemByType(item)));
  }

  return data;
};

export const sanitizeConnectInitiativeItemRelation = (relatedItems) => {
  const relations = {
    issueInitiativesRelation: { connect: [] },
    ideaInitiativesRelation: { connect: [] },
    actionInitiativesRelation: { connect: [] },
    riskInitiativesRelation: { connect: [] },
    decisionInitiativesRelation: { connect: [] },
    contributionInitiativesRelation: { connect: [] },
    fundingRequestInitiativesRelation: { connect: [] },
  };
  relatedItems.forEach((item) => {
    const id = { id: item.id };
    if (item.type === 'Issue') {
      relations.issueInitiativesRelation.connect.push(id);
    }

    if (item.type === 'Contribution') {
      relations.contributionInitiativesRelation.connect.push(id);
    }

    if (item.type === 'Action') {
      relations.actionInitiativesRelation.connect.push(id);
    }

    if (item.type === 'Idea') {
      relations.ideaInitiativesRelation.connect.push(id);
    }

    if (item.type === 'Decision') {
      relations.decisionInitiativesRelation.connect.push(id);
    }

    if (item.type === 'Risk') {
      relations.riskInitiativesRelation.connect.push(id);
    }
    if (item.type === 'Funding Request') {
      relations.fundingRequestInitiativesRelation.connect.push(id);
    }
  });

  return relations;
};

export const sanitizeReconnectInitiativeItemRelation = (relatedItems) => {
  const relations = {
    issueInitiativesRelation: { reconnect: [] },
    ideaInitiativesRelation: { reconnect: [] },
    actionInitiativesRelation: { reconnect: [] },
    riskInitiativesRelation: { reconnect: [] },
    decisionInitiativesRelation: { reconnect: [] },
    contributionInitiativesRelation: { reconnect: [] },
    fundingRequestInitiativesRelation: { reconnect: [] },
  };
  relatedItems.forEach((item) => {
    const id = { id: item.id };
    if (item.type === 'Issue') {
      relations.issueInitiativesRelation.reconnect.push(id);
    }

    if (item.type === 'Contribution') {
      relations.contributionInitiativesRelation.reconnect.push(id);
    }

    if (item.type === 'Action') {
      relations.actionInitiativesRelation.reconnect.push(id);
    }

    if (item.type === 'Idea') {
      relations.ideaInitiativesRelation.reconnect.push(id);
    }

    if (item.type === 'Decision') {
      relations.decisionInitiativesRelation.reconnect.push(id);
    }

    if (item.type === 'Risk') {
      relations.riskInitiativesRelation.reconnect.push(id);
    }

    if (item.type === 'Funding Request') {
      relations.fundingRequestInitiativesRelation.reconnect.push(id);
    }
  });

  return relations;
};
/**
 * Restore Initiative.
 *
 * @param {object}initiativeData - Initiative.
 * @returns {Promise<void|*>} Return promise.
 */
export const restoreInitiative = async (initiativeData) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { selectedAlliance, user } = sessionStore.getState(NEW_SESSION_EVENT);

  if (!canRestoreInitiative(user, initiativeData, selectedAlliance))
    return Flux.dispatchEvent(
      INITIATIVE_ERROR_EVENT,
      new IntegrityError('You do not have permission to perform this action'),
    );

  let response;
  try {
    response = await client.mutate({
      mutation: INITIATIVE_UPDATE_MUTATION,
      variables: {
        data: {
          id: initiativeData.id,
          status: INITIATIVE_IN_PROGRESS,
        },
      },
    });
  } catch (e) {
    error('restoreInitiative', e);
    return Flux.dispatchEvent(INITIATIVE_ERROR_EVENT, e);
  }

  Flux.dispatchEvent(INITIATIVE_RESTORE_EVENT, response);
  return response.data;
};

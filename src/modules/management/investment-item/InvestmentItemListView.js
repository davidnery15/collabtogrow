import React from 'react';
import { Card, Table, Dropdown, Menu, Heading, Icon, Grid, Loader } from '@8base/boost';
import { DropdownBodyOnTable } from 'components/dropdown/DropdownBodyOnTable';
import { ListCardBody } from 'components/card/ListCardBody';
import * as toast from 'components/toast/Toast';
import investmentItemStore, {
  INVESTMENT_ITEM_LIST_EVENT,
  INVESTMENT_ITEM_ERROR_EVENT,
  INVESTMENT_ITEM_DELETE_EVENT,
} from './investment-item-store';
import { Link } from 'react-router-dom';
// import * as R from 'ramda';
import { fetchInvestmentItems, deleteInvestmentItem } from './investment-item-actions';
import View from '@cobuildlab/react-flux-state';
import YesNoDialog from '../../../components/dialogs/YesNoDialog';
import { PropTypes } from 'prop-types';
import sessionStore, { NEW_SESSION_EVENT } from '../../../shared/SessionStore';
import Status from '../../../components/Status';
import { onErrorMixin, onListScrollMixin } from '../../../shared/mixins';
import { getInvestmentItemType } from './index';
import OptionsDialog from '../../../components/dialogs/OptionsDialog';
import { withRouter } from 'react-router-dom';
import withAlliance from '../../../components/hoc/withAlliance';
import fundingRequestStore, {
  FUNDING_REQUEST_ERROR_EVENT,
  FUNDING_REQUEST_SUBMIT_FOR_APPROVAL_EVENT,
} from '../funding-request/funding-request-store';
import {
  canCreateInvestmentItem,
  canDeleteInvestmentItem,
  canEditInvestmentItem,
  canSubmitForApprovalInvestmentItem,
} from './investment-item-permissions';
import contributionStore, {
  CONTRIBUTION_UPDATE_EVENT,
  CONTRIBUTION_ERROR_EVENT,
} from '../contribution/contribution-store';
import { ActionButtonListView } from '../../../components/buttons/ActionButtonListView';
import SearchInput from '../../../components/inputs/SearchInput';
import { debounce } from '../../../shared/utils';
import DetailDateValue from '../../../components/DetailDateValue';
import ItemFilter from '../../../components/ItemFilter';
import { INVESTMENT_ITEM_STATUS_LABELS } from '../../../shared/status';
import { INVESTMENT_ITEMS_LABELS } from '../../../shared/items-util';
import { DateText } from '../../../components/DateText';
import { Body } from '../../../components/new-ui/font-style/Body';

/**
 * List All the Investment Items.
 *
 * @param item
 * @param value
 * @param approvalFunction
 */
class InvestmentItemListView extends View {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      loading: true,
      loadingPage: false,
      item: null,
      deleteModalIsOpen: false,
      createModalIsOpen: false,
      submitForApprovalModalIsOpen: false,
      approvalFunction: null,
      search: '',
      page: 1,
      count: 0,
      filter: null,
      itemFilter: null,
    };
    this.user = sessionStore.getState(NEW_SESSION_EVENT).user;
    this.selectedAlliance = sessionStore.getState(NEW_SESSION_EVENT).selectedAlliance;
    this.onError = onErrorMixin.bind(this);
    this.onListScroll = onListScrollMixin.bind(this);
    this.searchWithDebounce = debounce(300, this.searchWithDebounce)();
  }

  componentDidMount() {
    this.subscribe(investmentItemStore, INVESTMENT_ITEM_ERROR_EVENT, this.onError);
    this.subscribe(investmentItemStore, INVESTMENT_ITEM_LIST_EVENT, (state) => {
      const { page } = state;
      const { items: newItems, count } = state.itemsList;
      const { loadingPage, items: oldItemsList } = this.state;
      const items = loadingPage ? oldItemsList.concat(newItems) : newItems;
      this.setState({
        items,
        count,
        page,
        loading: false,
        loadingPage: false,
      });
    });
    this.subscribe(investmentItemStore, INVESTMENT_ITEM_DELETE_EVENT, () => {
      const { search, filter, itemFilter } = this.state;
      toast.success('Investment Item Deleted!');
      fetchInvestmentItems(search, 1, 20, filter, itemFilter);
    });
    this.subscribe(fundingRequestStore, FUNDING_REQUEST_SUBMIT_FOR_APPROVAL_EVENT, () => {
      const { search, filter, itemFilter } = this.state;
      toast.success('Funding Request Submitted For Approval!');
      fetchInvestmentItems(search, 1, 20, filter, itemFilter);
    });
    this.subscribe(fundingRequestStore, FUNDING_REQUEST_ERROR_EVENT, this.onError);
    this.subscribe(contributionStore, CONTRIBUTION_UPDATE_EVENT, () => {
      const { search, filter, itemFilter } = this.state;
      toast.success('Contribution Submitted For Approval!');
      fetchInvestmentItems(search, 1, 20, filter, itemFilter);
    });
    this.subscribe(contributionStore, CONTRIBUTION_ERROR_EVENT, this.onError);

    fetchInvestmentItems();
  }
  onSelectForDelete = (item) => {
    this.setState({
      item,
      deleteModalIsOpen: true,
    });
  };

  onSelectForSubmitForApproval = (item, approvalFunction) => {
    this.setState({
      item,
      submitForApprovalModalIsOpen: true,
      approvalFunction,
    });
  };

  onYes = () => {
    this.setState(
      {
        deleteModalIsOpen: false,
        loading: true,
      },
      () => deleteInvestmentItem(this.state.item),
    );
  };

  onYesSubmitForApproval = () => {
    this.setState(
      {
        submitForApprovalModalIsOpen: false,
        loading: true,
      },
      () => this.state.approvalFunction(this.state.item),
    );
  };

  onClose = () => {
    this.setState({
      deleteModalIsOpen: false,
      submitForApprovalModalIsOpen: false,
    });
  };

  onSearchChange = (value) => {
    this.setState({ search: value });
    this.searchWithDebounce(value);
  };

  searchWithDebounce = (value) => {
    const { filter, itemFilter } = this.state;
    this.setState({ loading: true }, () => {
      fetchInvestmentItems(value, 1, 20, filter, itemFilter);
    });
  };
  statusFilter = (value) => {
    const { search, itemFilter } = this.state;
    this.setState(
      {
        loading: true,
        filter: value,
      },
      () => {
        fetchInvestmentItems(search, 1, 20, value, itemFilter);
      },
    );
  };
  itemFilter = (value) => {
    const { search, filter } = this.state;
    this.setState(
      {
        loading: true,
        itemFilter: value,
      },
      () => {
        fetchInvestmentItems(search, 1, 20, filter, value);
      },
    );
  };

  render() {
    const {
      deleteModalIsOpen,
      createModalIsOpen,
      items,
      submitForApprovalModalIsOpen,
      search,
      loading,
      loadingPage,
      filter,
      itemFilter,
    } = this.state;
    const { history } = this.props;
    const alliance = this.selectedAlliance;

    return (
      <div className="items-card">
        <Card.Header>
          <Grid.Layout
            columns="200px auto 200px 200px"
            areas={[['left', 'center', 'right', 'right2']]}
            style={{ width: '100%' }}>
            <Grid.Box justifySelf="flex-start" area="left">
              <Heading type="h4" text="Investment Items" />
            </Grid.Box>
            <Grid.Box justifySelf="center" area="center" className="list-box">
              <SearchInput className="search-input" value={search} onChange={this.onSearchChange} />
            </Grid.Box>
            <Grid.Box area="right" className="list-box">
              <ItemFilter
                onChange={this.statusFilter}
                value={filter}
                options={INVESTMENT_ITEM_STATUS_LABELS}
                placeholder="Filter By Status"
              />
            </Grid.Box>
            <Grid.Box area="right2" className="list-box">
              <ItemFilter
                onChange={this.itemFilter}
                value={itemFilter}
                options={INVESTMENT_ITEMS_LABELS}
                placeholder="Filter By Item"
              />
            </Grid.Box>
            <Grid.Box justifySelf="flex-end" />
          </Grid.Layout>
        </Card.Header>
        <ListCardBody className="items-table">
          <Table>
            <Table.Header columns="230px 230px 230px 230px 230px 230px 230px 230px">
              <Table.HeaderCell className="name-column">Item Type</Table.HeaderCell>
              <Table.HeaderCell>STATE</Table.HeaderCell>
              <Table.HeaderCell>Name</Table.HeaderCell>
              <Table.HeaderCell>Due Date</Table.HeaderCell>
              <Table.HeaderCell>Requested By</Table.HeaderCell>
              <Table.HeaderCell>Source</Table.HeaderCell>
              <Table.HeaderCell>Options</Table.HeaderCell>
              <Table.HeaderCell>
                Approvals <br />
                CLIENT / PARTNER{' '}
              </Table.HeaderCell>
            </Table.Header>
            <Table.Body
              onScroll={(event) => this.onListScroll(event, items, fetchInvestmentItems)}
              loading={loading}
              data={items}
              className="card-body-list">
              {(item, index) => {
                const investmentItem = getInvestmentItemType(item);
                const {
                  id,
                  url,
                  name,
                  revisedDueDate,
                  requestedBy,
                  approvalFunction,
                  status,
                  type,
                  approvalItems,
                  createdBy,
                  source,
                } = investmentItem;

                const isLast = index === items.length - 1;
                const pageLoader = isLast && loadingPage ? <Loader stretch /> : null;
                const _canSubmitForApprovalInvestmentItem = canSubmitForApprovalInvestmentItem(
                  this.user,
                  investmentItem,
                  this.selectedAlliance,
                );
                const user = requestedBy ? requestedBy : createdBy;
                const sourceName = source ? source.name : '';

                return (
                  <>
                    <Table.BodyRow
                      columns="230px 230px 210px 230px 240px 230px 230px 400px"
                      key={id}>
                      <Table.BodyCell>
                        <Link className="item-name" to={`/management/${url}/${id}/`}>
                          {type}
                        </Link>
                      </Table.BodyCell>
                      <Table.BodyCell>
                        <span> {status} </span>
                      </Table.BodyCell>
                      <Table.BodyCell className="">
                        <Link className="item-name" to={`/management/${url}/${id}/`}>
                          {name}
                        </Link>
                      </Table.BodyCell>
                      <Table.BodyCell>
                        <Body>
                          <DateText date={revisedDueDate} />
                        </Body>
                      </Table.BodyCell>
                      <Table.BodyCell>
                        <Body>
                          <span className="owner-name">{user}</span>
                        </Body>
                      </Table.BodyCell>
                      <Table.BodyCell>
                        <Body>
                          <span className="owner-name">{sourceName}</span>
                        </Body>
                      </Table.BodyCell>
                      <Table.BodyCell>
                        <Dropdown defaultOpen={false}>
                          <Dropdown.Head>
                            <Icon name="More" className="more-icon" />
                          </Dropdown.Head>
                          <DropdownBodyOnTable>
                            {({ closeDropdown }) => (
                              <Menu>
                                <Menu.Item
                                  onClick={() => {
                                    closeDropdown();
                                    history.push(`/management/${url}/${id}/`);
                                  }}>
                                  Details
                                </Menu.Item>
                                {canEditInvestmentItem(this.user, investmentItem, alliance) ? (
                                  <Menu.Item
                                    onClick={() => {
                                      closeDropdown();
                                      history.push(`/management/${url}/edit/${id}/`);
                                    }}>
                                    Edit
                                  </Menu.Item>
                                ) : (
                                  ''
                                )}
                                {canDeleteInvestmentItem(this.user, investmentItem, alliance) ? (
                                  <Menu.Item
                                    onClick={() => {
                                      closeDropdown();
                                      this.onSelectForDelete(item);
                                    }}>
                                    Delete
                                  </Menu.Item>
                                ) : (
                                  ''
                                )}
                                {!approvalFunction ||
                                !_canSubmitForApprovalInvestmentItem ? null : (
                                    <Menu.Item
                                      onClick={() => {
                                        closeDropdown();
                                        this.onSelectForSubmitForApproval(item, approvalFunction);
                                      }}>
                                    Submit For Approval
                                    </Menu.Item>
                                  )}
                              </Menu>
                            )}
                          </DropdownBodyOnTable>
                        </Dropdown>
                      </Table.BodyCell>
                      <Table.BodyCell>
                        {approvalItems !== null
                          ? approvalItems.slice(-2).map((approval, i) => (
                            <div key={i}>
                              <Status status={approval.status} />
                              <DetailDateValue date={approval.dateOfResponse} />
                            </div>
                          ))
                          : ''}
                      </Table.BodyCell>
                    </Table.BodyRow>
                    {pageLoader}
                  </>
                );
              }}
            </Table.Body>
          </Table>
        </ListCardBody>
        {canCreateInvestmentItem(this.user, alliance) ? (
          <ActionButtonListView
            onClick={() => this.setState({ createModalIsOpen: true })}
            text="Create Item"
          />
        ) : (
          ''
        )}
        <YesNoDialog
          isOpen={deleteModalIsOpen}
          onYes={this.onYes}
          onNo={this.onClose}
          onClose={this.onClose}
          text={'Are you sure you want to Delete this Item?'}
          title={'Delete Item'}
        />
        <YesNoDialog
          isOpen={submitForApprovalModalIsOpen}
          onYes={this.onYesSubmitForApproval}
          onNo={this.onClose}
          onClose={this.onClose}
          text={'Are you sure you want to Submit For Approval this Item?'}
          title={'Submit For Approval'}
        />
        <OptionsDialog
          isOpen={createModalIsOpen}
          onClose={() => this.setState({ createModalIsOpen: false })}
          text={'Select an Item Type'}
          options={[
            {
              text: 'Funding Request',
              action: () => {
                this.props.history.push(`/management/funding-request/create`);
              },
            },
            {
              text: 'Contribution',
              action: () => {
                this.props.history.push(`/management/contribution/create`);
              },
            },
          ]}
          title={'Create Investment Item'}
        />
      </div>
    );
  }
}

InvestmentItemListView.propTypes = {
  history: PropTypes.object.isRequired,
};

export default withRouter(withAlliance(InvestmentItemListView));

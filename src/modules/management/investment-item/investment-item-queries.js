import gql from 'graphql-tag';
import { UserFragment } from '../../auth/queries';

/**
 * List Query
 * Example:
 * {
    "data": {
      "alliance": {
        "id": {
          "equals": "cjouj32uc000h01rxv4nevfin"
        }.
      }.
    }.
  }.
 */
export const INVESTMENT_ITEM_LIST_QUERY = gql`
  query($data: ItemFilter, $skip: Int, $first: Int) {
    itemsList(filter: $data, skip: $skip, first: $first) {
      count
      items {
        id
        createdAt
        contribution {
          id
          name
          description
          status
          unitType
          unitQuantity
          unitMonetizationFactor
          calculatedValue
          unitValueDescription
          contributionDate
          source {
            id
            name
          }
          initiatives {
            items {
              id
              name
              status
            }
          }
          createdBy {
            ...UserFragment
          }
        }
        fundingRequest {
          id
          name
          description
          status
          revisedDueDate
          source {
            id
            name
          }
          requestedBy {
            ...UserFragment
          }
          initiatives {
            items {
              id
              name
              status
            }
          }
          fundingRequestApprovalRelation {
            items {
              dateOfResponse
              status
            }
          }
        }
      }
    }
  }
  ${UserFragment}
`;

/**
 * Delete.
 */
export const INVESTMENT_ITEM_DELETE_MUTATION = gql`
  mutation($data: ItemDeleteInput!) {
    itemDelete(data: $data) {
      success
    }
  }
`;

import {
  INVESTMENT_ITEM_APPROVED,
  INVESTMENT_ITEM_SUBMITTED_FOR_APPROVAL,
  ALLIANCE_APPROVED,
  INVESTMENT_ITEM_COMPLETED,
  CONTRIBUTION_COMPLETED,
  FUNDING_REQUEST_APPROVED,
  FUNDING_REQUEST_COMPLETED,
  FUNDING_REQUEST_SUBMITTED_FOR_APPROVAL,
} from '../../../shared/status';
import {
  isUserAdminOrSERInAlliance,
  isUserCreatorInAlliance,
} from '../../../shared/alliance-utils';
import { initiativesApprovedValidator } from '../initiative/initiative-validators';
import { isAllianceCompleted } from '../../settings/alliance-management/alliance-permissions';

/**
 * Checks if a User can Edit An Investment Item
 * @param {User} user The user
 * @param {Alliance} alliance The Alliance
 * @param {InvestmentItem} item The item
 */
export const canEditInvestmentItem = (user, item, alliance) => {
  if (isAllianceCompleted(alliance)) return false;

  if (isUserAdminOrSERInAlliance(user, alliance)) return true;

  if (
    [
      CONTRIBUTION_COMPLETED,
      FUNDING_REQUEST_APPROVED,
      FUNDING_REQUEST_COMPLETED,
      FUNDING_REQUEST_SUBMITTED_FOR_APPROVAL,
    ].includes(item.status)
  )
    return false;

  if (isUserCreatorInAlliance(user, alliance)) return true;

  return false;
};

/**
 * Checks if a User can Delete A Investment Item
 * @param {User} user The user
 * @param {Alliance} alliance The Alliance
 * @param {InvestmentItem} item The item
 */
export const canDeleteInvestmentItem = (user, item, alliance) => {
  if (isAllianceCompleted(alliance)) return false;

  if (item.status === INVESTMENT_ITEM_APPROVED) return false;

  return isUserAdminOrSERInAlliance(user, alliance);
};

/**
 * Checks if a User can Create Investment Items on an Alliance Alliance
 * @param {User} user The user
 * @param {Alliance} alliance The Alliance
 */
export const canCreateInvestmentItem = (user, alliance) => {
  if (isAllianceCompleted(alliance)) return false;

  return isUserAdminOrSERInAlliance(user, alliance) || isUserCreatorInAlliance(user, alliance);
};

/**
 * Checks if a User can Submit For approval a InvestmentItem
 * @param {User} user The user
 * @param {Alliance} alliance The Alliance
 * @param {InvestmentItem} investmentItem
 */
export const canSubmitForApprovalInvestmentItem = (user, investmentItem, alliance) => {
  if (isAllianceCompleted(alliance)) return false;

  if (alliance.status !== ALLIANCE_APPROVED) return false;
  if (
    investmentItem.status === INVESTMENT_ITEM_APPROVED ||
    investmentItem.status === INVESTMENT_ITEM_COMPLETED ||
    investmentItem.status === INVESTMENT_ITEM_SUBMITTED_FOR_APPROVAL
  )
    return false;

  try {
    initiativesApprovedValidator(investmentItem.initiatives);
  } catch (e) {
    return false;
  }

  return isUserAdminOrSERInAlliance(user, alliance);
};

import sessionStore, { APOLLO_CLIENT, NEW_SESSION_EVENT } from '../../../../shared/SessionStore';
import { createMockClient } from 'mock-apollo-client';
import { DEAL_DATA_CREATE_MUTATION, DEAL_SOURCE_ID_QUERY } from '../deal-queries';
import { createDeal } from '../deal-actions';
import dealStore, { DEAL_ERROR_EVENT } from '../deal-store';
import { mockedAlliance } from '../../../../shared/tests';

describe('creates deals', () => {
  const otherCompanyDeals = [{ dealSourceId: 'dealSourceId1' }, { dealSourceId: 'dealSourceId1' }];
  // Mocking Apollo Client
  const mockClient = createMockClient();
  mockClient.setRequestHandler(DEAL_DATA_CREATE_MUTATION, () =>
    Promise.resolve({ data: { dealDatumCreate: { id: 1 } } }),
  );
  mockClient.setRequestHandler(DEAL_SOURCE_ID_QUERY, () =>
    Promise.resolve({ data: { dealDataList: { items: otherCompanyDeals } } }),
  );

  // Mocking the Session Store
  const newSessionEvent = mockedAlliance();
  sessionStore.getState = jest.fn((key) => {
    return {
      [APOLLO_CLIENT]: () => mockClient,
      [NEW_SESSION_EVENT]: newSessionEvent,
    }[key];
  });

  test('fails deal source Id ', () => {
    //Data Input
    const deal = { company: { id: 'companyID' }, dealSourceId: 'dealSourceId1' };
    const relatedItems = [];
    const otherCompany = null;

    dealStore.subscribe(DEAL_ERROR_EVENT, (error) => {
      expect(error.message).toBe('The Deal Source Id already exists');
    });

    createDeal(deal, relatedItems, otherCompany);

    // expect(() => {
    //   ;
    // }).toThrow();
  });

  jest.clearAllMocks();
});

module.exports = {};

import React from 'react';
import { Card, Grid, Heading, Loader } from '@8base/boost';
import { CreateViewCardBody } from '../../../components/new-ui/card/CreateViewCardBody';
import dealStore, { DEAL_DETAIL_EVENT, DEAL_ERROR_EVENT, DEAL_UPDATE_EVENT } from './deal-store';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import * as toast from '../../../components/toast/Toast';
import * as R from 'ramda';
import View from '@cobuildlab/react-flux-state';
import { onChangeDataMixin, onErrorMixin } from '../../../shared/mixins';
import { DealData } from './deal-model';
import DealForm from './components/DealForm';
import { fetchDealData, openComments, updateDeal } from './deal-actions';
import { getCurrencyOnSession } from '../../../shared/alliance-utils';
import { ActionButton } from '../../../components/buttons/ActionButton';
import { fetchRelatedItems } from '../../related-item/related-item-actions';
import allianceStore, {
  ALLIANCE_LIST_MEMBERS_EVENT,
} from '../../settings/alliance-management/alliance-store';
import { fetchCurrentAllianceMembersAction } from '../../settings/alliance-management/alliance-actions';
import { concatClientAndPartnerUsers } from '../../../shared/alliance-utils';
import relatedItemStore, { RELATED_ITEMS_EVENT } from '../../related-item/related-item-store';
import { getItemByType } from '../../../shared/items-util';
import { TransparentButton } from '../../../components/buttons/TransparentButton';
import DealDetailTable from './components/DealDetailTable';
import { FormSteps } from '../../../components/dots/FormSteps';
import { ActionButtonClose } from '../../../components/buttons/ActionButtonClose';
import { dealValidator } from './deal-validators';
import RelatedItemForm from '../../related-item/components/RelatedItemForm';
import { RelatedItemsDetailTable } from '../../related-item/components/RelatedItemsDetailTable';
import sessionStore, { NEW_SESSION_EVENT } from '../../../shared/SessionStore';
import { LeftProgressSection } from '../../../components/new-ui/LeftProgressSection';
import { SCREENS_DEALS } from '../screenView';
import { BoxCard } from '../../../components/new-ui/div/BoxCard';
import { TransparentButtonSvg } from '../../../components/buttons/TransparentButtonSvg';
import collaborateIcon from '../../../images/icons/collab-chat-icon.svg';
import stageMappingStore, {
  STAGE_MAPPING_DETAIL_EVENT,
} from '../../settings/stage-mapping/stage-mapping-store';
import { fetchDealStage } from '../../settings/stage-mapping/stage-mapping-action';

class DealEditView extends View {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        dealData: R.clone(DealData),
        relatedItems: [],
        relatedDeals: [],
        initiatives: [],
        originalDealAssociatedId: null,
      },
      loading: true,
      allianceMembersOptions: [],
      step: 0,
      initiativesList: [],
      otherCompany: null,
      dealStagesOptions: [],
    };
    this.onError = onErrorMixin.bind(this);
    this.onChangeData = onChangeDataMixin.bind(this);
    this.user = sessionStore.getState(NEW_SESSION_EVENT).user;
  }

  onChangeDealData = (name, value) => {
    const { data } = this.state;
    data.dealData[name] = value;
    this.setState({ data });
  };

  componentDidMount() {
    const { match } = this.props;

    this.subscribe(dealStore, DEAL_ERROR_EVENT, this.onError);

    this.subscribe(dealStore, DEAL_DETAIL_EVENT, (state) => {
      const { dealDatum } = state;
      dealDatum.owner = dealDatum.owner === null ? null : dealDatum.owner.id;
      dealDatum.itemId = dealDatum.itemDealDataRelation.id;
      const { data } = this.state;
      data.dealData = dealDatum;
      data.originalDealAssociatedId = R.clone(data.dealData.associatedDealId);
      this.setState(
        {
          data,
        },
        () => fetchRelatedItems(dealDatum.itemId),
      );
    });

    this.subscribe(dealStore, DEAL_UPDATE_EVENT, () => {
      toast.success('Deal Successfully Updated');
      this.props.history.goBack();
    });

    this.subscribe(relatedItemStore, RELATED_ITEMS_EVENT, (state) => {
      const items = state.item.itemsRelated.items.map((item) => getItemByType(item));
      const { data } = this.state;

      data.relatedItems = items;
      this.setState({ data, loading: false });
    });

    this.subscribe(stageMappingStore, STAGE_MAPPING_DETAIL_EVENT, (state) => {
      const { dealStagesList } = state;
      const { data } = this.state;
      const dealStagesOptions = dealStagesList.items.map((dealStage) => {
        return {
          label: `${dealStage.name}`,
          value: `${dealStage.name}`,
        };
      });

      const dealStage = dealStagesOptions.find(
        (dealStage) => dealStage.label === data.dealData.stage,
      );
      data.dealData.stage = dealStage ? dealStage.value : null;

      this.setState({
        dealStagesOptions,
        data,
      });
    });

    this.subscribe(allianceStore, ALLIANCE_LIST_MEMBERS_EVENT, (state) => {
      const { clientCompany, partnerCompany } = state;
      const members = concatClientAndPartnerUsers(clientCompany, partnerCompany);
      const { data } = this.state;
      let otherCompany;
      if (data.dealData.company.id === clientCompany.id) {
        // if company deal is client company the other company is partner company
        otherCompany = partnerCompany;
      } else {
        otherCompany = clientCompany;
      }

      this.setState({
        data,
        otherCompany: otherCompany,
        allianceMembersOptions: members.map((member) => {
          return {
            label: `${member.firstName} ${member.lastName}`,
            value: member.id,
          };
        }),
      });
    });

    if (!match.params.id) return toast.error('Deal ID missing');

    fetchDealStage();
    fetchDealData(match.params.id);
    fetchCurrentAllianceMembersAction();
  }

  onSubmit = () => {
    this.setState({ loading: true }, () => {
      const dealData = R.clone(this.state.data.dealData);
      const relatedItems = R.clone(this.state.data.relatedItems);
      const originalAssociatedDealId = R.clone(this.state.data.originalDealAssociatedId);
      const dealStagesOptions = R.clone(this.state.dealStagesOptions);
      const dealStageName = dealStagesOptions.find(
        (dealStage) => dealStage.value === dealData.stage,
      );
      dealData.stage = dealStageName ? dealStageName.label : null;
      updateDeal(dealData, relatedItems, this.state.otherCompany, originalAssociatedDealId);
    });
  };

  onDealStepChange = (step) => {
    const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
    const dealData = R.clone(this.state.data.dealData);
    try {
      dealValidator(dealData, selectedAlliance);
    } catch (e) {
      return this.onError(e);
    }

    this.onScreen(step);
  };

  onScreen = (step) => {
    this.setState({ step });
  };

  render() {
    const {
      loading,
      allianceMembersOptions,
      data,
      step,
      initiativesList,
      dealStagesOptions,
    } = this.state;
    const { dealData, relatedItems, relatedDeals, initiatives } = data;
    const { history } = this.props;
    let content = <Loader stretch />;
    let footer = <></>;
    let buttonsTop = <></>;
    const currency = getCurrencyOnSession();

    if (!loading && step === 0) {
      content = (
        <DealForm
          dealData={dealData}
          onChange={this.onChangeDealData}
          currency={currency}
          allianceMembersOptions={allianceMembersOptions}
          dealStagesOptions={dealStagesOptions}
        />
      );
      footer = (
        <Card.Footer>
          <ActionButton onClick={() => this.onDealStepChange(1)} text={'Next'} />
        </Card.Footer>
      );
    }

    if (!loading && step === 1) {
      const itemData = { id: dealData.id, type: dealData.__typename };

      content = (
        <RelatedItemForm
          relatedItems={relatedItems}
          initiatives={initiativesList}
          onChange={(key, value) => {
            this.onChangeData(key, value);
          }}
          selectedInitiatives={initiatives}
          allowedDealOption={true}
          itemData={itemData}
        />
      );

      footer = (
        <Card.Footer>
          <ActionButton onClick={() => this.onScreen(2)} text={'Next'} />
          <TransparentButton onClick={() => this.onScreen(0)} text={'Previous'} />
        </Card.Footer>
      );
    }

    if (!loading && step === 2) {
      const dealDetail = R.clone(dealData);
      const dealStageSelected = dealStagesOptions.find(
        (dealStage) => dealStage.value === dealDetail.stage,
      );
      dealDetail.stage = dealStageSelected ? dealStageSelected.label : null;
      content = (
        <>
          <DealDetailTable dealData={dealDetail} currency={currency} relatedDeals={relatedDeals} />
          <RelatedItemsDetailTable relatedItems={relatedItems} />
        </>
      );
      footer = (
        <Card.Footer>
          <ActionButton onClick={() => this.onSubmit()} text="Edit Deal" />
          <TransparentButton onClick={() => this.onScreen(1)} text="Previous" />
        </Card.Footer>
      );
    }

    if (!loading)
      buttonsTop = (
        <>
          <div className="company-icons">
            <TransparentButtonSvg
              iconSvg={collaborateIcon}
              onClick={() => openComments(dealData)}
            />
            <ActionButtonClose onClick={history.goBack} />
          </div>
        </>
      );

    return (
      <React.Fragment>
        <Card.Header>
          <Grid.Layout
            columns="200px auto 200px"
            areas={[['left', 'center', 'right']]}
            style={{ width: '100%' }}>
            <Grid.Box area="left">
              <Heading type="h4" text="Edit Deal" />
            </Grid.Box>
            <Grid.Box area="center">
              <FormSteps totalSteps={3} step={step} />
            </Grid.Box>
            <Grid.Box area="right" />
          </Grid.Layout>
          {buttonsTop}
        </Card.Header>
        <CreateViewCardBody>
          <Grid.Layout columns="30% 70%" areas={[['left', 'right']]} style={{ width: '100%' }}>
            <Grid.Box area="left">
              <LeftProgressSection sections={SCREENS_DEALS} currentScreen={step} />
            </Grid.Box>
            <BoxCard>
              <Grid.Box area="right">{content}</Grid.Box>
            </BoxCard>
          </Grid.Layout>
        </CreateViewCardBody>
        {footer}
      </React.Fragment>
    );
  }
}

DealEditView.propTypes = {
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default withRouter(DealEditView);

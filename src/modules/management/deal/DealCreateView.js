import React from 'react';
import { Card, Grid, Heading, Loader } from '@8base/boost';
import { CreateViewCardBody } from '../../../components/new-ui/card/CreateViewCardBody';
import DealForm from './components/DealForm';
import * as toast from '../../../components/toast/Toast';
import { withRouter } from 'react-router-dom';
import { PropTypes } from 'prop-types';
import View from '@cobuildlab/react-flux-state';
import { onChangeDataMixin, onErrorMixin } from '../../../shared/mixins';
import * as R from 'ramda';
import allianceStore, {
  ALLIANCE_LIST_MEMBERS_EVENT,
} from '../../settings/alliance-management/alliance-store';
import { fetchCurrentAllianceMembersAction } from '../../settings/alliance-management/alliance-actions';
import { getCurrencyOnSession } from '../../../shared/alliance-utils';
import { ActionButton } from '../../../components/buttons/ActionButton';
import dealStore, { DEAL_CREATE_EVENT, DEAL_ERROR_EVENT } from './deal-store';
import { createDeal, openComments } from './deal-actions';
import { DealData } from './deal-model';
import { concatClientAndPartnerUsers } from '../../../shared/alliance-utils';
import { TransparentButton } from '../../../components/buttons/TransparentButton';
import { dealValidator } from './deal-validators';
import { FormSteps } from '../../../components/dots/FormSteps';
import DealDetailTable from './components/DealDetailTable';
import RelatedItemForm from '../../related-item/components/RelatedItemForm';
import { RelatedItemsDetailTable } from '../../related-item/components/RelatedItemsDetailTable';
import sessionStore, { NEW_SESSION_EVENT } from '../../../shared/SessionStore';
import { LeftProgressSection } from '../../../components/new-ui/LeftProgressSection';
import { SCREENS_DEALS } from '../screenView';
import { BoxCard } from '../../../components/new-ui/div/BoxCard';
import { TransparentButtonSvg } from '../../../components/buttons/TransparentButtonSvg';
import collaborateIcon from '../../../images/icons/collab-chat-icon.svg';
import { ActionButtonClose } from '../../../components/buttons/ActionButtonClose';
import stageMappingStore, {
  STAGE_MAPPING_DETAIL_EVENT,
} from '../../settings/stage-mapping/stage-mapping-store';
import { fetchDealStage } from '../../settings/stage-mapping/stage-mapping-action';

const DEAL_DATA_STORE = 'dealCreateView';

class DealCreateView extends View {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        dealData: R.clone(DealData),
        relatedItems: [],
        relatedDeals: [],
        initiatives: [],
      },
      allianceMembersOptions: [],
      otherCompany: null,
      loading: true,
      step: 0,
      initiativesList: [],
      dealStagesOptions: [],
    };
    this.onChangeData = onChangeDataMixin.bind(this);
    this.onError = onErrorMixin.bind(this);
    this.user = sessionStore.getState(NEW_SESSION_EVENT).user;
  }

  onChangeDealData = (name, value) => {
    const { data } = this.state;
    data.dealData[name] = value;
    this.setState({ data });
    sessionStorage.setItem(DEAL_DATA_STORE, JSON.stringify(data));
  };

  componentDidMount() {
    const data = JSON.parse(sessionStorage.getItem(DEAL_DATA_STORE));

    this.subscribe(dealStore, DEAL_ERROR_EVENT, this.onError);
    this.subscribe(dealStore, DEAL_CREATE_EVENT, () => {
      sessionStorage.removeItem(DEAL_DATA_STORE);
      toast.success('Deal Successfully Created');
      this.props.history.goBack();
    });

    // set data from sessionStorage
    if (data) {
      this.setState({ data });
    }

    this.subscribe(stageMappingStore, STAGE_MAPPING_DETAIL_EVENT, (state) => {
      const { dealStagesList } = state;
      const dealStagesOptions = dealStagesList.items.map((dealStage) => {
        return {
          label: `${dealStage.name}`,
          value: `${dealStage.name}`,
        };
      });
      this.setState({
        dealStagesOptions,
      });
    });

    this.subscribe(allianceStore, ALLIANCE_LIST_MEMBERS_EVENT, (state) => {
      const { clientCompany, partnerCompany } = state;
      const members = concatClientAndPartnerUsers(clientCompany, partnerCompany);
      const { data } = this.state;

      const belongsToTheClientCompany = clientCompany.companyUserRelation.items.find(
        (item) => item.user.id === this.user.id,
      );

      const belongsToThePartnerCompany = partnerCompany
        ? partnerCompany.companyUserRelation.items.find((item) => item.user.id === this.user.id)
        : null;
      //data.dealData.company = state.clientCompany.companyUserRelation.items[0].company;

      data.dealData.company = belongsToTheClientCompany
        ? belongsToTheClientCompany.company
        : belongsToThePartnerCompany.company;
      console.log('ALLIANCE_LIST_MEMBERS_EVENT:members', state);
      this.setState({
        data,
        loading: false,
        otherCompany: partnerCompany !== null ? partnerCompany : null,
        allianceMembersOptions: members.map((member) => {
          return {
            label: `${member.firstName} ${member.lastName}`,
            value: member.id,
          };
        }),
      });
    });

    fetchDealStage();
    fetchCurrentAllianceMembersAction();
  }

  onSubmit = () => {
    this.setState({ loading: true }, () => {
      const dealData = R.clone(this.state.data.dealData);
      const relatedItems = R.clone(this.state.data.relatedItems);
      createDeal(dealData, relatedItems, this.state.otherCompany);
    });
  };

  onScreen = (step) => {
    this.setState({ step });
  };

  onDealStepChange = (step) => {
    const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
    const dealData = R.clone(this.state.data.dealData);
    try {
      dealValidator(dealData, selectedAlliance);
    } catch (e) {
      return this.onError(e);
    }
    this.onScreen(step);
  };

  render() {
    const {
      loading,
      allianceMembersOptions,
      data,
      step,
      initiativesList,
      dealStagesOptions,
    } = this.state;
    const { dealData, relatedItems, relatedDeals, initiatives } = data;
    const { history } = this.props;

    const currency = getCurrencyOnSession();
    let content = <Loader stretch />;
    let footer = <></>;
    let buttonsTop = <></>;

    if (!loading && step === 0) {
      content = (
        <DealForm
          dealData={dealData}
          onChange={this.onChangeDealData}
          currency={currency}
          allianceMembersOptions={allianceMembersOptions}
          dealStagesOptions={dealStagesOptions}
        />
      );
      footer = (
        <Card.Footer>
          <ActionButton onClick={() => this.onDealStepChange(1)} text={'Next'} />
        </Card.Footer>
      );
    }

    if (!loading && step === 1) {
      content = (
        <RelatedItemForm
          relatedItems={relatedItems}
          initiatives={initiativesList}
          onChange={(key, value) => {
            this.onChangeData(key, value);
          }}
          selectedInitiatives={initiatives}
        />
      );

      footer = (
        <Card.Footer>
          <ActionButton onClick={() => this.onScreen(2)} text={'Next'} />
          <TransparentButton onClick={() => this.onScreen(0)} text={'Previous'} />
        </Card.Footer>
      );
    }

    if (!loading && step === 2) {
      const dealDetail = R.clone(dealData);
      content = (
        <>
          <DealDetailTable dealData={dealDetail} currency={currency} relatedDeals={relatedDeals} />
          <RelatedItemsDetailTable relatedItems={relatedItems} />
        </>
      );
      footer = (
        <Card.Footer>
          <ActionButton onClick={() => this.onSubmit()} text="Create Deal" />
          <TransparentButton onClick={() => this.onScreen(1)} text="Previous" />
        </Card.Footer>
      );
    }

    if (!loading)
      buttonsTop = (
        <>
          <div className="company-icons">
            <TransparentButtonSvg
              iconSvg={collaborateIcon}
              onClick={() => openComments(dealData)}
            />
            <ActionButtonClose onClick={history.goBack} />
          </div>
        </>
      );

    return (
      <React.Fragment>
        <Card.Header>
          <Grid.Layout
            columns="200px auto 200px"
            areas={[['left', 'center', 'right']]}
            style={{ width: '100%' }}>
            <Grid.Box area="left">
              <Heading type="h4" text="Create Deal" />
            </Grid.Box>
            <Grid.Box area="center">
              <FormSteps totalSteps={3} step={step} />
            </Grid.Box>
            <Grid.Box area="right" />
          </Grid.Layout>
          {buttonsTop}
        </Card.Header>
        <CreateViewCardBody>
          <Grid.Layout columns="30% 70%" areas={[['left', 'right']]} style={{ width: '100%' }}>
            <Grid.Box area="left">
              <LeftProgressSection sections={SCREENS_DEALS} currentScreen={step} />
            </Grid.Box>
            <BoxCard>
              <Grid.Box area="right">{content}</Grid.Box>
            </BoxCard>
          </Grid.Layout>
        </CreateViewCardBody>
        {footer}
      </React.Fragment>
    );
  }
}

DealCreateView.propTypes = {
  history: PropTypes.object.isRequired,
};

export default withRouter(DealCreateView);

import sessionStore, { APOLLO_CLIENT, NEW_SESSION_EVENT } from '../../../shared/SessionStore';
import Flux from '@cobuildlab/flux-state';
import {
  DEAL_COMPLETED_EVENT,
  DEAL_CREATE_EVENT,
  DEAL_DELETE_EVENT,
  DEAL_DELETE_LIST_EVENT,
  DEAL_DETAIL_EVENT,
  DEAL_ERROR_EVENT,
  DEAL_IMPORTED_EVENT,
  DEAL_IMPORTED_HEADER_ERROR_EVENT,
  DEAL_IMPORTED_VALID_EVENT,
  DEAL_LIST_EVENT,
  ALL_DEAL_DATA_LIST_EVENT,
  DEAL_MONTHLY_SNAPSHOT_LIST_EVENT,
  DEAL_UPDATE_EVENT,
  DEAL_STAGES_EVENT,
  RELATED_DEAL_LIST_EVENT,
  DEAL_IMPORTED_ERROR_EVENT,
  DEAL_COMPANIES_EVENT,
} from './deal-store';
import {
  DEAL_COMMENTS_QUERY,
  DEAL_DATA_CREATE_MUTATION,
  DEAL_DATA_DELETE_MUTATION,
  DEAL_DATA_DETAIL_QUERY,
  DEAL_DATA_ID_LIST,
  DEAL_DATA_LIST_QUERY,
  DEAL_DATA_LIST_SLIM_QUERY,
  DEAL_MONTHLY_SNAPSHOT_LIST_QUERY,
  DEAL_DATA_UPDATE_MUTATION,
  DEAL_LIST_QUERY,
  ALL_DEAL_DATA_LIST_QUERY,
  DEAL_LIST_SLIM_QUERY,
  DEAL_SOURCE_ID_QUERY,
  RELATED_DEALS_LIST_QUERY,
  DEAL_STAGE_LIST_QUERY,
  DEAL_COMPANIES_LIST_QUERY,
} from './deal-queries';
import { IntegrityError } from '../../../shared/errors';
import { error, log } from '@cobuildlab/pure-logger';
import { filterForYear, isValidString, sanitize8BaseReference } from '../../../shared/utils';
import { canCompletedDeal, canDeleteDeal } from './deal-permissions';
import { DEAL_COMPLETED, DEAL_OPEN } from '../../../shared/status';
import * as Papa from 'papaparse';
import { DEAL_TYPE } from '../../../shared/item-types';
import {
  COMMENT_CREATE_EVENT,
  COMMENT_ERROR_EVENT,
  COMMENT_REQUEST_EVENT,
  COMMENTS_EVENT,
} from '../../comment/comment-store';
import { COMMENTS_CREATE_MUTATION } from '../../comment/comment-queries';
import {
  dealImportedValidator,
  dealValidator,
  dealImportedHeaderValidate,
  dealSourceIdValidator,
  createAssociatedDealIdValidator,
  updateAssociatedDealIdValidator,
  dealMappingValidator,
} from './deal-validators';
import moment from 'moment';
import currency from 'currency.js';
import { RELATED_ITEM_UPDATE_MUTATION } from '../../related-item/related-item-queries';
import { RISK_ERROR_EVENT } from '../risk/risk-store';
import { hasActiveAllianceDecorator } from '../../../shared/decorators';
import * as R from 'ramda';
import { getCurrencyOnSession } from '../../../shared/alliance-utils';
import { Currency } from './deal-model';
import { fetchStageMapping } from '../../settings/stage-mapping/stage-mapping-action';
import { replaceDealStage } from './deal-utils';

/**
 * Returns the monthly data for deals Data.
 *
 * @returns {Promise<Array<DealMonthlySnapshot>|void>} Promise - Promise.
 * @private
 */
const _fetchDealMonthlySnapShots = async () => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
  const currentYear = new Date().getFullYear();
  const filter = {
    alliance: { id: { equals: selectedAlliance.id } },
    year: { in: [currentYear, currentYear - 1] },
  };

  let response;
  try {
    response = await client.query({
      query: DEAL_MONTHLY_SNAPSHOT_LIST_QUERY,
      fetchPolicy: 'network-only',
      variables: { data: filter },
    });
  } catch (e) {
    error('fetchDealMonthlySnapShots', e);
    return Flux.dispatchEvent(DEAL_ERROR_EVENT, e);
  }
  log('fetchDealMonthlySnapShots', response);
  Flux.dispatchEvent(
    DEAL_MONTHLY_SNAPSHOT_LIST_EVENT,
    response.data.dealMonthlySnapshotsList.items,
  );
  return response.data.dealMonthlySnapshotsList.items;
};

export const fetchDealMonthlySnapShots = hasActiveAllianceDecorator(
  _fetchDealMonthlySnapShots,
  DEAL_ERROR_EVENT,
);

/**
 * Creates a filter object and search by a string
on string properties of the deal.
 *
 * @param {string} allianceId -  The allianceId to filter.
 * @param {string} search - The string to search.
 * @param {string} stage - Stage.
 * @param {string}companyId
 * @param {string}filterYear
 * @returns {object}             The filter object.
 */
const dealFilter = (allianceId, search = '', stage, companyId, filterYear = null) => {
  const filter = {
    itemDealDataRelation: { alliance: { id: { equals: allianceId } } },
    status: { equals: DEAL_OPEN },
    OR: [
      { name: { contains: search } },
      { owner: { firstName: { contains: search } } },
      { owner: { lastName: { contains: search } } },
      {
        owner: {
          email: { contains: search },
        },
      },
      {
        description: { contains: search },
      },
      {
        customerName: { contains: search },
      },
      {},
    ],
  };

  if (filterYear) {
    const filterForYearData = filterForYear(
      filterYear.year,
      filterYear.fieldName,
      filterYear.isDateTime,
    );

    filter.AND = filterForYearData.AND;
  }

  if (stage) {
    filter.stage = { equals: stage };
  }
  if (companyId) {
    filter.company = { id: { equals: companyId } };
  }

  return filter;
};
/**
 * Creates a filter object and filters the deals associated to the selected deal by dealSourceId or dealId.
 *
 * @param {string} [search=''] - The string to search.
 * @param {string}sourceId
 * @param {string}dealId
 * @param {string} allianceId -  The allianceId to filter.
 * @returns {object}             The filter object.
 */
const associatedDealFilter = (allianceId, sourceId, dealId) => {
  const filter = {
    itemDealDataRelation: { alliance: { id: { equals: allianceId } } },
    status: { equals: DEAL_OPEN },
    OR: [
      {
        associatedDealId: { equals: sourceId },
      },
      {
        AND: [
          {
            associatedDealId: { equals: dealId },
          },
        ],
      },
    ],
  };

  return filter;
};

/**
 * Create a comment on a Deal.
 *
 * @param {string}dealId
 * @param {string}comment
 * @returns {Promise<*>}
 */
export const createDealComment = async (dealId, comment) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const data = {
    comment,
    dealDataCommentsRelation: { connect: { id: dealId } },
  };
  let response;
  try {
    response = await client.mutate({
      mutation: COMMENTS_CREATE_MUTATION,
      variables: { data },
    });
  } catch (e) {
    error('createDealComment', e);
    Flux.dispatchEvent(COMMENT_ERROR_EVENT, e);
    throw e;
  }
  log('createDealComment', response);
  Flux.dispatchEvent(COMMENT_CREATE_EVENT, response.data);
  return response.data;
};
/**
 * Fetches the Deal Comments.
 *
 * @param {string}dealId - Deal Id.
 * @returns {Promise<void>} Promise - Promise.
 */
export const fetchDealComments = async (dealId) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const data = { id: dealId };
  const { user } = sessionStore.getState(NEW_SESSION_EVENT);

  let response;
  try {
    response = await client.query({
      query: DEAL_COMMENTS_QUERY,
      variables: data,
      fetchPolicy: 'network-only',
    });
  } catch (e) {
    error('fetchDealComments', e);
    return Flux.dispatchEvent(COMMENT_ERROR_EVENT, e);
  }
  log('fetchDealComments', response);

  response.data.dealDatum.comments.userId = user.id;
  Flux.dispatchEvent(COMMENTS_EVENT, response.data.dealDatum.comments);
  return response.data.dealDatum.comments;
};

/**
 * Notifies a Request for Comments for a Decision.
 */
export const openComments = ({ id: dealId }) => {
  Flux.dispatchEvent(COMMENT_REQUEST_EVENT, { type: DEAL_TYPE, id: dealId });
};

/**
 * Fetches the Deal list.
 *
 * @deprecated Use fetchDealsData instead.
 * @param {string}search - Search.
 * @param {number}page - Page.
 * @param {number}first - First.
 * @param {boolean}slim - Slim.
 * @returns {Promise<void|*>}Promise.
 */
export const fetchDeals = async (search = '', page = 1, first = 20, slim = false) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
  const skip = (page - 1) * first;
  if (!selectedAlliance || !isValidString(selectedAlliance.id))
    return Flux.dispatchEvent(DEAL_ERROR_EVENT, new IntegrityError('An Alliance must be selected'));

  const filter = dealFilter(selectedAlliance.id, search);

  let response;
  try {
    response = await client.query({
      query: slim ? DEAL_LIST_SLIM_QUERY : DEAL_LIST_QUERY,
      fetchPolicy: 'network-only',
      variables: { data: filter, skip, first },
    });
    response.data.page = page;
    response.data.first = first;
    response.data.search = search;
  } catch (e) {
    error('fetchDeals', e);
    return Flux.dispatchEvent(DEAL_ERROR_EVENT, e);
  }

  log('fetchDeals', response);
  Flux.dispatchEvent(DEAL_LIST_EVENT, response.data);
  return response.data;
};
/**
 * Fetches the Stage of all the Deals and filter the repeated elements.
 *
 * @returns {Promise<void>}Promise.
 */
export const fetchDealsStages = async () => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
  if (!selectedAlliance || !isValidString(selectedAlliance.id))
    return Flux.dispatchEvent(DEAL_ERROR_EVENT, new IntegrityError('An Alliance must be selected'));

  const filter = {
    itemDealDataRelation: { alliance: { id: { equals: selectedAlliance.id } } },
  };

  let response;
  try {
    response = await client.query({
      query: DEAL_STAGE_LIST_QUERY,
      fetchPolicy: 'network-only',
      variables: { data: filter },
    });
  } catch (e) {
    error('fetchDealsData', e);
    return Flux.dispatchEvent(DEAL_ERROR_EVENT, e);
  }

  // Repeated Stages Filter
  const stages = [...new Set(response.data.dealDataList.items.map((deal) => deal.stage))];
  stages.unshift('');
  log('fetchDealsData', response);
  Flux.dispatchEvent(DEAL_STAGES_EVENT, stages);
  console.log(stages);
  return stages;
};
/**
 * Fetch companies.
 *
 * @returns {Promise<void>}Promise.
 */
export const fetchDealsCompanies = async () => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
  if (!selectedAlliance || !isValidString(selectedAlliance.id))
    return Flux.dispatchEvent(DEAL_ERROR_EVENT, new IntegrityError('An Alliance must be selected'));

  const filter = {
    itemDealDataRelation: { alliance: { id: { equals: selectedAlliance.id } } },
  };

  let response;
  try {
    response = await client.query({
      query: DEAL_COMPANIES_LIST_QUERY,
      fetchPolicy: 'network-only',
      variables: { data: filter },
    });
  } catch (e) {
    error('fetchDealsData', e);
    return Flux.dispatchEvent(DEAL_ERROR_EVENT, e);
  }

  // Repeated Stages Filter
  const companies = [...new Set(response.data.dealDataList.items.map((deal) => deal.company))];
  companies.unshift({ id: '', name: 'Combined Companies' });
  Flux.dispatchEvent(DEAL_COMPANIES_EVENT, companies);
  return companies;
};

/**
 * Fetches the Deals Data List.
 *
 * @param {string}search - Search.
 * @param {number}page - Page.
 * @param {number}first - First.
 * @param {boolean}slim - Slim.
 * @param {string}stage - Stage.
 * @param {string}companyId - Company Id.
 * @returns {Promise<void>}Promise.
 */
export const fetchDealsData = async (
  search = '',
  page = 1,
  first = 20,
  slim = false,
  stage,
  companyId,
) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
  const skip = (page - 1) * first;
  if (!selectedAlliance || !isValidString(selectedAlliance.id))
    return Flux.dispatchEvent(DEAL_ERROR_EVENT, new IntegrityError('An Alliance must be selected'));

  const filter = dealFilter(selectedAlliance.id, search, stage, companyId);

  let response;
  try {
    response = await client.query({
      query: slim ? DEAL_DATA_LIST_SLIM_QUERY : DEAL_DATA_LIST_QUERY,
      fetchPolicy: 'network-only',
      variables: { data: filter, skip, first },
    });
    response.data.page = page;
    response.data.first = first;
    response.data.search = search;
  } catch (e) {
    error('fetchDealsData', e);
    return Flux.dispatchEvent(DEAL_ERROR_EVENT, e);
  }

  log('fetchDealsData', response);
  Flux.dispatchEvent(DEAL_LIST_EVENT, response.data);
  return response.data;
};
/**
 * Fetches the Associated Deals list.
 *
 * @param {boolean}slim - Slim.
 * @param {string}dealSourceId - Deal Source Id.
 * @param {string}dealId - Deal Id.
 * @returns {Promise<void>}Promise.
 */
export const fetchAssociatedDeals = async (slim = false, dealSourceId, dealId) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
  if (!selectedAlliance || !isValidString(selectedAlliance.id))
    return Flux.dispatchEvent(DEAL_ERROR_EVENT, new IntegrityError('An Alliance must be selected'));

  const filter = associatedDealFilter(selectedAlliance.id, dealSourceId, dealId, '');

  let response;
  try {
    response = await client.query({
      query: slim ? DEAL_DATA_LIST_SLIM_QUERY : DEAL_DATA_LIST_QUERY,
      fetchPolicy: 'network-only',
      variables: { data: filter },
    });
  } catch (e) {
    error('fetchDealsData', e);
    return Flux.dispatchEvent(DEAL_ERROR_EVENT, e);
  }

  log('fetchDealsData', response);
  Flux.dispatchEvent(DEAL_LIST_EVENT, response.data);
  return response.data;
};
/**
 * Fetch deal data by year.
 *
 * @param {string}search - Search.
 * @param {string}filterYear - Filter Year.
 * @returns {Promise<void|*>} Promise.
 */
export const fetchAllDealsData = async (search = '', filterYear = null) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
  if (!selectedAlliance || !isValidString(selectedAlliance.id))
    return Flux.dispatchEvent(DEAL_ERROR_EVENT, new IntegrityError('An Alliance must be selected'));

  const filter = dealFilter(selectedAlliance.id, search, null, null, filterYear);

  let response;
  try {
    response = await client.query({
      query: ALL_DEAL_DATA_LIST_QUERY,
      fetchPolicy: 'network-only',
      variables: { data: filter },
    });
  } catch (e) {
    error('fetchDealsData', e);
    Flux.dispatchEvent(DEAL_ERROR_EVENT, e);
    throw e;
  }

  log('fetchDealsData', response);
  Flux.dispatchEvent(ALL_DEAL_DATA_LIST_EVENT, response.data);
  return response.data;
};

/**
 * Fetches the Deals Id for this Alliance.
 *
 * @returns {Promise<void>}Promise.
 * @private
 */
const _fetchDealsId = async () => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
  if (!selectedAlliance || !isValidString(selectedAlliance.id))
    return Flux.dispatchEvent(DEAL_ERROR_EVENT, new IntegrityError('An Alliance must be selected'));

  const filter = { itemDealDataRelation: { alliance: { id: { equals: selectedAlliance.id } } } };
  let response;
  try {
    response = await client.query({
      query: DEAL_DATA_ID_LIST,
      fetchPolicy: 'network-only',
      variables: { data: filter },
    });
  } catch (e) {
    error('_fetchDealsId', e);
    throw e;
  }
  log('_fetchDealsId', response.data.dealDataList.items);
  return response.data.dealDataList.items;
};

/**
 * Fetch Related Deals.
 *
 * @param {object}deal - Deal.
 * @returns {Promise<Array|void|*>}Promise.
 */
export const fetchRelatedDeals = async (deal) => {
  if (!isValidString(deal.associatedDealId)) {
    Flux.dispatchEvent(RELATED_DEAL_LIST_EVENT, []);
    return [];
  }
  const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
  if (!selectedAlliance || !isValidString(selectedAlliance.id))
    return Flux.dispatchEvent(DEAL_ERROR_EVENT, new IntegrityError('An Alliance must be selected'));

  const client = sessionStore.getState(APOLLO_CLIENT);
  const filter = {
    dealSourceId: { equals: deal.associatedDealId },
    itemDealDataRelation: { alliance: { id: { equals: selectedAlliance.id } } },
  };

  let response;
  try {
    response = await client.query({
      query: RELATED_DEALS_LIST_QUERY,
      fetchPolicy: 'network-only',
      variables: { data: filter },
    });
  } catch (e) {
    error('fetchRelatedDeals', e);
    return Flux.dispatchEvent(DEAL_ERROR_EVENT, e);
  }

  log('fetchRelatedDeals', response);
  Flux.dispatchEvent(RELATED_DEAL_LIST_EVENT, response.data.dealDataList.items);
  return response.data.dealDataList.items;
};

/**
 * Fetches a list of DealSourceIds for an specific company.
 *
 * @param {number}companyId - Company Id.
 * @returns {Promise<void>}Promise.
 */
const fetchDealsSourceId = async (companyId: number) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);

  const filter = {
    company: { id: { equals: companyId } },
    itemDealDataRelation: { alliance: { id: { equals: selectedAlliance.id } } },
  };
  console.log('fetchDealsSourceId:company', companyId);
  console.log('fetchDealsSourceId:alliance', selectedAlliance.id);
  let response;
  try {
    response = await client.query({
      query: DEAL_SOURCE_ID_QUERY,
      fetchPolicy: 'network-only',
      variables: { data: filter },
    });
  } catch (e) {
    error('fetchDealsSourceId', e);
    return Flux.dispatchEvent(DEAL_ERROR_EVENT, e);
  }
  log('fetchDealsSourceId', response.data.dealDataList.items);
  return response.data.dealDataList.items;
};

/**
 * Fetches the Deal list with the Deal Source Id of only the Deals that can be related to other deals via the associatedDealId relation.
 *
 * @param {number}companyId - Company Id.
 * @returns {Promise<void>}Promise.
 */
export const fetchAvailableDealsSourceId = async (companyId: number) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
  const filter = {
    company: { id: { equals: companyId } },
    itemDealDataRelation: { alliance: { id: { equals: selectedAlliance.id } } },
    AND: {
      OR: [{ associatedDealId: { equals: '' } }, { associatedDealId: { equals: null } }],
    },
  };

  let response;
  try {
    response = await client.query({
      query: DEAL_SOURCE_ID_QUERY,
      fetchPolicy: 'network-only',
      variables: { data: filter },
    });
  } catch (e) {
    error('fetchDealsSourceId', e);
    return Flux.dispatchEvent(DEAL_ERROR_EVENT, e);
  }
  log('fetchDealsSourceId', response.data.dealDataList.items);
  return response.data.dealDataList.items;
};

/**
 * Pairs columns name positions with Row position into an Object.
 *
 * @param {object}csvRow - Object.
 * @param {object}columns - Columns.
 * @returns Paired object: {name: 'a', address: 'b'...}.
 */

const pairDealCsvData = (csvRow, columns) => {
  const obj = {};
  for (let i = 0, j = columns.length; i < j; i++) {
    const column = columns[i];
    if (column === null) continue;
    const { value, header } = column;
    if (value !== null && value !== undefined) obj[value] = csvRow[header];
  }
  return obj;
};

/**
 * Creates a Deal Data with the Deal Data.
 *
 * @param {object}client - Client.
 * @param {object}dealData - Deal Data.
 * @returns {Promise<*>}Promise.
 * @private
 */
const _createDeal = async (client, dealData) => {
  let response;
  try {
    response = await client.mutate({
      mutation: DEAL_DATA_CREATE_MUTATION,
      variables: { data: dealData },
    });
  } catch (e) {
    error('_createDeal', e, dealData);
    throw e;
  }
  return response;
};

/**
 *
 * @param {object}client - Client.
 * @param {object}dealData - Deal Data.
 * @returns {Promise<*>}Promise.
 * @private
 */
const _updateDealData = async (client, dealData) => {
  let response;
  try {
    response = await client.mutate({
      mutation: DEAL_DATA_UPDATE_MUTATION,
      variables: { data: dealData },
    });
  } catch (e) {
    error('_updateDealData', e);
    throw e;
  }
  return response;
};

/**
 * Format a date to YYYY-MM-DD.
 *
 * @param {string}date - Date.
 * @returns {string}String.
 * @private
 */
const _changeFormatDate = (date) => {
  return moment(new Date(date)).format('YYYY-MM-DD');
};

/**
 * Normalize amount field.
 *
 * @param {object}dealData - Deal Data.
 * @param {string}currencyOnAlliance - : The alliance Currency.
 * @returns {*}Object.
 */
export const sanitizeDealDataFormatAmount = (dealData, currencyOnAlliance: Currency) => {
  const currencyOptions = {
    symbol: currencyOnAlliance.symbol,
    decimal: currencyOnAlliance.decimalSeparator,
    separator: currencyOnAlliance.decimalSeparator,
    precision: 0,
  };
  if (dealData.amount !== undefined) {
    dealData.amount = String(currency(dealData.amount, currencyOptions).value);
  }

  return dealData;
};

/**
 * Format the dates of the Deal to valid dates.
 *
 * @param {object}dealData - Deal Data.
 * @returns {*}Object.
 */
const sanitizeDealDataFormatDate = (dealData) => {
  if (moment(dealData.closeDate).isValid() && dealData.closeDate !== undefined) {
    dealData.closeDate = _changeFormatDate(dealData.closeDate);
  }
  if (moment(dealData.lastActivityDate).isValid() && dealData.lastActivityDate !== undefined) {
    dealData.lastActivityDate = _changeFormatDate(dealData.lastActivityDate);
  }

  if (moment(dealData.lastContactedDate).isValid() && dealData.lastContactedDate !== undefined) {
    dealData.lastContactedDate = _changeFormatDate(dealData.lastContactedDate);
  }

  if (moment(dealData.lastModifiedDate).isValid() && dealData.lastModifiedDate !== undefined) {
    dealData.lastModifiedDate = _changeFormatDate(dealData.lastModifiedDate);
  }

  if (moment(dealData.nextActivityDate).isValid() && dealData.nextActivityDate !== undefined) {
    dealData.nextActivityDate = _changeFormatDate(dealData.nextActivityDate);
  }

  if (moment(dealData.createdDate).isValid() && dealData.createdDate !== undefined) {
    dealData.createdDate = _changeFormatDate(dealData.createdDate);
  }
  return dealData;
};

/**
 * Creates the new deals from the csv data.
 *
 * @param {object} dealOwners - Deal Owners.
 * @param {Array} csvData - Csv Data.
 * @param {Array} columns - Columns.
 * @param {object} company - The company who is importing the deals.
 * @param {Array} otherCompany - Ohter Company.
 * @returns {Promise<void|*>} Promise.
 */
export const createDealFromCSV = async (dealOwners, csvData, columns, company, otherCompany) => {
  log(`createDealFromCSV:`, dealOwners, csvData, columns, company);

  const client = sessionStore.getState(APOLLO_CLIENT);
  const currency = getCurrencyOnSession();
  const {
    selectedAlliance: { id: allianceId },
  } = sessionStore.getState(NEW_SESSION_EVENT);

  if (!isValidString(allianceId))
    return Flux.dispatchEvent(DEAL_ERROR_EVENT, new IntegrityError('An Alliance must be selected'));

  const dealsSourceIdListOnTheSystem = await fetchDealsSourceId(company.id);
  log(`createDealFromCSV:dealsSourceIdListL`, dealsSourceIdListOnTheSystem);
  const dealsSourceIdMap = {};
  dealsSourceIdListOnTheSystem.forEach(
    (item) => (dealsSourceIdMap[item.dealSourceId] = { id: item.id, owner: item.owner }),
  );
  const mappingList = await fetchStageMapping(company.id);
  const _csvData = sanitizeCsvData(csvData, columns, currency);
  console.log('createDealFromCSV:_csvData', _csvData);
  let otherCompanyDealsSourceIds = otherCompany ? await fetchDealsSourceId(otherCompany.id) : [];

  let operations = [];
  for (let i = 0, j = _csvData.length; i < j; i++) {
    // Parsing the Deal into a valid object
    let dataObject = _csvData[i];
    let dataObjectCreate;
    let dealReference = null;
    if (dataObject.dealSourceId !== undefined) {
      const _dealReference = dealsSourceIdMap[dataObject.dealSourceId];
      if (_dealReference !== undefined) dealReference = _dealReference;
    }
    const dealDataOwnerId = dealOwners[i];

    if (dataObject.stage) {
      console.log('replaceDealStage:dataObject.stage', dataObject.stage);
      dataObject.stage = replaceDealStage(mappingList, dataObject.stage);
    }

    if (dealReference === null) {
      // CREATE NEW DEAL
      const data = {
        itemDealDataRelation: { create: { alliance: { connect: { id: allianceId } } } },
        ...dataObject,
        company: { connect: { id: company.id } },
        owner: dealDataOwnerId ? { connect: { id: dealDataOwnerId } } : null,
      };
      //operations.push();
      console.log(`createDealFromCSV:for:create`, data);
      dataObjectCreate = await _createDeal(client, data);
      operations.push(i);
    } else {
      // UPDATE Existing Deal
      //Update Deal Data
      const data = {
        id: dealReference.id,
        ...dataObject,
      };

      if (dealReference.owner) {
        // if (dealDataOwnerId) data.owner = { reconnect: { id: dealReference.owner.id } };
        if (dealDataOwnerId) data.owner = { reconnect: { id: dealDataOwnerId } };
        else data.owner = { disconnect: { id: dealReference.owner.id } };
      } else {
        // if (dealDataOwnerId) data.owner = { connect: { id: dealReference.owner.id } };
        if (dealDataOwnerId) data.owner = { connect: { id: dealDataOwnerId } };
      }
      console.log(`createDealFromCSV:for:update`, data);
      await _updateDealData(client, data);

      //operations.push(_updateDealData(client, data));
      operations.push(i);
    }

    // If the deal has associatedDealId we update that deal represented by the associatedDealId
    // and put his associatedDealId as our dealSourceId
    if (dataObject.associatedDealId) {
      const _dealAssociated = otherCompanyDealsSourceIds.find(
        (item) => item.dealSourceId === dataObject.associatedDealId,
      );

      if (_dealAssociated) {
        _dealAssociated.associatedDealId = dataObject.dealSourceId
          ? dataObject.dealSourceId
          : dataObjectCreate.data.dealDatumCreate.id;
        delete _dealAssociated.owner;
        delete _dealAssociated.__typename;
        console.log('_dealAssociated', _dealAssociated);
        //We remove any previous association. Maybe we are updating the associatedDealId
        /*
        await removeDealAssociatedId(dataObject, client, R.clone(otherCompanyDealsSourceIds));
*/
        await _updateDealData(client, _dealAssociated);
      }
    } else {
      // We check if our dealSourceID exists as associatedDealId to break any previous associations
      await removeDealAssociatedId(dataObject, client, R.clone(otherCompanyDealsSourceIds));
    }

    if (operations.length === 10) {
      Flux.dispatchEvent(DEAL_CREATE_EVENT, { operations: operations.length });
      operations = [];
    }
  }

  Flux.dispatchEvent(DEAL_CREATE_EVENT, { operations: operations.length });

  // await Promise.all(operations);
};

/**
 * Fetch a single Deal.
 *
 * @param {string}id - Deal id.
 *  @returns {Promise}Promise.
 */
export const fetchDealData = async (id) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  let response;
  try {
    response = await client.query({
      query: DEAL_DATA_DETAIL_QUERY,
      variables: { id },
      fetchPolicy: 'network-only',
    });
  } catch (e) {
    error('fetchDealData', e);
    return Flux.dispatchEvent(DEAL_ERROR_EVENT, e);
  }
  log('fetchDealData', response);
  Flux.dispatchEvent(DEAL_DETAIL_EVENT, response.data);
  return response.data;
};

/**
 * Convert csv data into Deal Objects for 8base.
 *
 * @param {Array}csvData - Csv Data.
 * @param {Array}columns - Columns.
 * @param {string}currencyOnAlliance - Currency.
 * @returns {[]}Promise.
 */
export const sanitizeCsvData = (csvData, columns, currencyOnAlliance: Currency) => {
  const data = [];
  const currentDate = moment().format('YYYY-MM-DD');

  for (let i = 0, j = csvData.length; i < j; i++) {
    const csvRow = csvData[i][0];
    // Parsing the Deal into a valid object
    let dataObject = pairDealCsvData(csvRow, columns);
    dataObject = sanitizeDealDataFormatDate(dataObject);
    dataObject = sanitizeDealDataFormatAmount(dataObject, currencyOnAlliance);
    dataObject.createdDate = dataObject.createdDate ? dataObject.createdDate : currentDate;
    data.push(dataObject);
  }

  data.forEach((item) => {
    if (!item.associatedDealId) {
      const _dealAssociated = data.find((dt) => dt.associatedDealId === item.dealSourceId);
      if (_dealAssociated) {
        item.associatedDealId = _dealAssociated.dealSourceId;
      }
    }
  });
  return data;
};

/**
 * Delete an Deal.
 *
 * @param  {object}deal - The id of the Deal to be deleted.
 * @param  {object} otherCompany - Other Company.
 * @returns {Promise}Promise.
 */
export const deleteDeal = async (deal, otherCompany) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const user = sessionStore.getState(NEW_SESSION_EVENT).user;
  const selectedAlliance = sessionStore.getState(NEW_SESSION_EVENT).selectedAlliance;
  const allianceId = selectedAlliance.id;
  const dealId = deal.id;

  if (!canDeleteDeal(user, deal, { id: allianceId }))
    return Flux.dispatchEvent(
      DEAL_ERROR_EVENT,
      new IntegrityError(`Permission Denied. Can't Delete this Deal`),
    );

  const otherCompanyDealsSourceIdList = await fetchDealsSourceId(otherCompany.id);
  await removeDealAssociatedId(deal, client, R.clone(otherCompanyDealsSourceIdList));

  let response;
  try {
    response = await client.mutate({
      mutation: DEAL_DATA_DELETE_MUTATION,
      variables: { data: { id: dealId, force: true } },
    });
  } catch (e) {
    error('deleteDeal', e);
    return Flux.dispatchEvent(DEAL_ERROR_EVENT, e);
  }
  log('deleteDeal', response);
  Flux.dispatchEvent(DEAL_DELETE_EVENT, response.data);
  return response.data;
};

/**
 * Close a Deal.
 *
 * @param {object}user - User.
 * @param {object}dealData - Deal Data.
 * @param {object}alliance - Alliance.
 * @returns {Promise}Promise.
 */
export const completedDeal = async (user, dealData, alliance) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  let response;

  if (!canCompletedDeal(user, dealData, alliance))
    return Flux.dispatchEvent(
      DEAL_ERROR_EVENT,
      new IntegrityError('You do not have permission to perform this action'),
    );

  try {
    response = await client.mutate({
      mutation: DEAL_DATA_UPDATE_MUTATION,
      variables: {
        data: {
          id: dealData.id,
          status: DEAL_COMPLETED,
        },
      },
    });
  } catch (e) {
    error('completedDeal', e);
    return Flux.dispatchEvent(DEAL_ERROR_EVENT, e);
  }

  Flux.dispatchEvent(DEAL_COMPLETED_EVENT, response.data);
  return response.data;
};

/**
 * Parse CSV files for import process.
 *
 * @param {object}dealsCompany -  The company to associated the Deals.
 * @param {object}otherCompany -  The other Company Id. It may be null.
 * @param {object}files - Files.
 * @returns {Promise<void>}Promise.
 */
export const parseCSV = async (dealsCompany: string, otherCompany: string, files: File[]) => {
  if (dealsCompany === null || dealsCompany === undefined)
    return Flux.dispatchEvent(
      DEAL_ERROR_EVENT,
      new IntegrityError(
        'Please select A Company to assign to the the deals that are going to be imported.',
      ),
    );

  let fileCounter = 0;
  const data = [];
  let headers = null;

  files.forEach((file) => {
    Papa.parse(file, {
      header: true,
      skipEmptyLines: true,
      step: (results) => {
        const cleanedData = results.data.filter((row) => {
          const keys = Object.keys(row);
          for (let i = 0, j = keys.length; i < j; i++) if (isValidString(row[keys[i]])) return true;
          return false;
        });
        data.push(cleanedData);
        if (headers === null && results.meta.fields) headers = results.meta.fields;
      },
      complete: () => {
        if (++fileCounter === files.length)
          Flux.dispatchEvent(DEAL_IMPORTED_EVENT, { data, headers, dealsCompany, otherCompany });
      },
      error: (error) => {
        console.log('parseCSV:Error:', error);
        Flux.dispatchEvent(DEAL_ERROR_EVENT, error);
      },
    });
  });
};

/**
 * Creates a New Deal.
 *
 * @param {object}deal - Deal.
 * @param {Array}relatedItems - Related Items.
 * @param {Array}otherCompany - Other Company.
 * @returns {Promise<void>}Promise.
 */
export const createDeal = async (deal, relatedItems, otherCompany) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
  const allianceId = selectedAlliance.id;

  if (!allianceId)
    return Flux.dispatchEvent(DEAL_ERROR_EVENT, new IntegrityError('Must have an Active Alliance'));

  deal = sanitizeDealDataFormatAmount(deal, selectedAlliance.currency);
  log(`createDeal:deal`, deal);

  const companyId = deal.company.id;

  const dealsSourceIdList = await fetchDealsSourceId(companyId);
  log(`createDeal:dealsSourceIdList`, dealsSourceIdList);
  const dealsSourceIdMap = {};
  dealsSourceIdList.forEach(
    (item) =>
      (dealsSourceIdMap[item.dealSourceId] = {
        id: item.id,
        owner: item.owner,
        associatedDealId: item.associatedDealId,
      }),
  );

  if (deal.dealSourceId) {
    try {
      dealSourceIdValidator(deal.dealSourceId, dealsSourceIdList);
    } catch (e) {
      return Flux.dispatchEvent(DEAL_ERROR_EVENT, new IntegrityError(e.message));
    }
  }

  if (deal.associatedDealId && otherCompany === null)
    return Flux.dispatchEvent(
      DEAL_ERROR_EVENT,
      new IntegrityError(
        `You can't associate a Deal to another Deal. You only have one company in your alliance`,
      ),
    );

  if (deal.associatedDealId === deal.dealSourceId && deal.associatedDealId && deal.dealSourceId)
    return Flux.dispatchEvent(
      DEAL_ERROR_EVENT,
      new IntegrityError(`You can't associate with your self!`),
    );

  let otherCompanyDealsSourceIds = [];

  if (deal.associatedDealId) {
    // validate if source id exists
    otherCompanyDealsSourceIds = await fetchDealsSourceId(otherCompany.id);
    try {
      createAssociatedDealIdValidator(
        deal.associatedDealId,
        dealsSourceIdList,
        otherCompanyDealsSourceIds,
      );
    } catch (e) {
      return Flux.dispatchEvent(DEAL_ERROR_EVENT, new IntegrityError(e.message));
    }
  }

  try {
    dealValidator(deal, selectedAlliance);
  } catch (err) {
    error('createDeal', err);
    return Flux.dispatchEvent(DEAL_ERROR_EVENT, err);
  }
  sanitize8BaseReference(deal, 'owner');
  deal.company = { connect: { id: deal.company.id } };
  deal.itemDealDataRelation = {
    create: {
      alliance: { connect: { id: allianceId } },
      itemsRelated: {
        connect: relatedItems.map((item) => {
          return { id: item.itemId };
        }),
      },
    },
  };
  let result;
  try {
    result = await client.mutate({
      mutation: DEAL_DATA_CREATE_MUTATION,
      variables: { data: deal },
    });
  } catch (e) {
    error('createDeal', e);
    return Flux.dispatchEvent(DEAL_ERROR_EVENT, e);
  }

  if (deal.associatedDealId) {
    deal.id = result.data.dealDatumCreate.id;
    await updateDealAssociatedId(deal, client, R.clone(otherCompanyDealsSourceIds));
  }

  log('createDeal', result);
  return Flux.dispatchEvent(DEAL_CREATE_EVENT, result);
};

/**
 * Before update changes delete fields that are not necessary and change type of value of amount
 * and numberOfSalesActivities to String.
 *
 * @param {object}deal - Deal.
 * @returns {*}Object.
 */
const sanitizeUpdateDealData = (deal) => {
  sanitize8BaseReference(deal, 'owner');
  delete deal.itemDealDataRelation;
  delete deal.__typename;
  delete deal.company;
  delete deal.createdAt;
  delete deal.createdBy;
  delete deal.updatedAt;
  return deal;
};

/**
 * Update Deal.
 *
 * @param {object}deal - Deal.
 * @param {Array}relatedItems - Related Items.
 * @param {Array}otherCompany - Other Company.
 * @param {Array}originalAssociatedDealId - Original Associated Deal id.
 * @returns {Promise<void|*>}Promise.
 */
export const updateDeal = async (deal, relatedItems, otherCompany, originalAssociatedDealId) => {
  console.log(`updateDeal`, deal);
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);

  deal = sanitizeDealDataFormatAmount(deal, selectedAlliance.currency);

  const companyId = deal.company.id;
  const dealsSourceIdList = await fetchDealsSourceId(companyId);
  log(`updateDeal:dealsSourceIdList`, dealsSourceIdList);
  const dealsSourceIdMap = {};
  dealsSourceIdList.forEach(
    (item) =>
      (dealsSourceIdMap[item.dealSourceId] = {
        id: item.id,
        owner: item.owner,
        associatedDealId: item.associatedDealId,
      }),
  );

  if (deal.associatedDealId && otherCompany === null)
    return Flux.dispatchEvent(
      DEAL_ERROR_EVENT,
      new IntegrityError(
        `You can't associate a Deal to another Deal. You only have one company in your alliance`,
      ),
    );

  if (deal.associatedDealId === deal.dealSourceId)
    return Flux.dispatchEvent(
      DEAL_ERROR_EVENT,
      new IntegrityError(`You can't associate with your self!`),
    );
  let otherCompanyDealsSourceIds = otherCompany ? await fetchDealsSourceId(otherCompany.id) : [];
  console.log('updateDeal:otherCompanyDealsSourceIds', otherCompanyDealsSourceIds);
  if (deal.associatedDealId) {
    try {
      //validate if deal source id exists
      updateAssociatedDealIdValidator(
        deal.dealSourceId,
        deal.associatedDealId,
        dealsSourceIdList,
        otherCompanyDealsSourceIds,
      );
    } catch (e) {
      return Flux.dispatchEvent(DEAL_ERROR_EVENT, new IntegrityError(e.message));
    }
  }

  const { itemId } = deal;
  delete deal.itemId;

  try {
    dealValidator(deal, selectedAlliance);
  } catch (e) {
    error('updateDeal', e);
    return Flux.dispatchEvent(DEAL_ERROR_EVENT, new IntegrityError(e.message));
  }

  sanitizeUpdateDealData(deal);

  let response;
  try {
    response = await client.mutate({
      mutation: DEAL_DATA_UPDATE_MUTATION,
      variables: { data: deal },
    });
  } catch (e) {
    error('updateDeal', e);
    return Flux.dispatchEvent(DEAL_ERROR_EVENT, e);
  }

  // Reconnect related items
  const relatedItemsData = {
    id: itemId,
    itemsRelated: {
      reconnect: relatedItems.map((i) => {
        return { id: i.itemId };
      }),
    },
  };

  console.log('DEBUG:originalAssociatedDealId', originalAssociatedDealId);

  if (originalAssociatedDealId === deal.associatedDealId) {
    Flux.dispatchEvent(DEAL_UPDATE_EVENT, response);
    return response.data;
  }

  if (deal.associatedDealId) {
    await updateDealAssociatedId(deal, client, R.clone(otherCompanyDealsSourceIds));
    await removeDealAssociatedId(deal, client, R.clone(otherCompanyDealsSourceIds));
  } else {
    await removeDealAssociatedId(deal, client, R.clone(otherCompanyDealsSourceIds));
  }

  log('dealData:update:relatedItems:', relatedItemsData);
  try {
    await client.mutate({
      mutation: RELATED_ITEM_UPDATE_MUTATION,
      variables: { data: relatedItemsData },
    });
  } catch (e) {
    error('dealData:updateRelated Items', e, deal);
    return Flux.dispatchEvent(RISK_ERROR_EVENT, e);
  }

  Flux.dispatchEvent(DEAL_UPDATE_EVENT, response);
  return response.data;
};

/**
 * Search deal associated for update associatedDealId field.
 *
 * @param {object}deal - Deal.
 * @param {object}client - Client.
 * @param {Array}dealsSourceIdList - Deals Source.
 * @returns {Promise<void|*>}Promise.
 */
export const updateDealAssociatedId = async (deal, client, dealsSourceIdList) => {
  const dealAssociated = dealsSourceIdList.find(
    (item) => item.dealSourceId === deal.associatedDealId,
  );

  dealAssociated.associatedDealId = deal.dealSourceId;
  delete dealAssociated.__typename;
  delete dealAssociated.owner;

  try {
    await client.mutate({
      mutation: DEAL_DATA_UPDATE_MUTATION,
      variables: { data: dealAssociated },
    });
  } catch (e) {
    error('updateDealAssociatedId:updateDealAssociated', e);
    return Flux.dispatchEvent(DEAL_ERROR_EVENT, e);
  }

  return dealAssociated;
};

/**
 * If other deals is associated with dealAssociated o deal reset that value.
 *
 * @param {object}deal - Deal.
 * @param {object}client - Client.
 * @param {object}otherCompanyDealsSourceIds - Other Company.
 * @returns {Promise<void>}Promise.
 */
export const removeDealAssociatedId = async (deal, client, otherCompanyDealsSourceIds) => {
  let dealAssociated = otherCompanyDealsSourceIds.find(
    (item) => item.associatedDealId === deal.dealSourceId,
  );

  if (dealAssociated) {
    dealAssociated.associatedDealId = null;
    delete dealAssociated.__typename;
    delete dealAssociated.owner;
    try {
      await client.mutate({
        mutation: DEAL_DATA_UPDATE_MUTATION,
        variables: { data: dealAssociated },
      });
    } catch (e) {
      error('removeDealAssociatedId:updateDealAssociated', e);
      return Flux.dispatchEvent(DEAL_ERROR_EVENT, e);
    }
  }

  /*  dealAssociated = otherCompanyDealsSourceIds.find(
    (item) => item.associatedDealId === deal.associatedDealId,
  );
  console.log('removeDealAssociatedId:updateDealAssociated', dealAssociated);

  if (dealAssociated) {
    dealAssociated.associatedDealId = null;
    delete dealAssociated.__typename;
    delete dealAssociated.owner;
    try {
      await client.mutate({
        mutation: DEAL_DATA_UPDATE_MUTATION,
        variables: { data: dealAssociated },
      });
    } catch (e) {
      error('removeDealAssociatedId:updateDealAssociated', e);
      return Flux.dispatchEvent(DEAL_ERROR_EVENT, e);
    }
  }*/
};

/**
 * Validate every field.
 *
 * @param {Array}headers - Headers selected for User.
 * @param {Array}dataCSV -  Data Imported.
 * @param {object}company - Company importing the deals.
 * @param {object}otherCompany - Company not importing the deals.
 * @param {string}currency - The currency of the alliance.
 * @returns {Promise<void>}Promise.
 */

export const checkDataCvs = async (
  headers: Array,
  dataCSV: Array,
  company,
  otherCompany,
  currency,
) => {
  const labelList = headers.filter((header) => header !== null);
  const associatedDealIdLabel = labelList.find((item) => item.value === 'associatedDealId');
  const dealSourceIdLabel = labelList.find((item) => item.value === 'dealSourceId');

  let response = {};
  const mappingList = await fetchStageMapping(company.id);
  const availableDealsSourceIds = await fetchAvailableDealsSourceId(company.id);
  const availableDealsSourceIdsFromOtherCompany = otherCompany
    ? await fetchAvailableDealsSourceId(otherCompany.id)
    : [];

  log(
    `checkDataCvs:dealsSourceIdList`,
    availableDealsSourceIds,
    availableDealsSourceIdsFromOtherCompany,
  );

  // validate Deal Imported Data
  try {
    dealImportedHeaderValidate(labelList);
  } catch (e) {
    return Flux.dispatchEvent(DEAL_IMPORTED_ERROR_EVENT, e);
  }

  // validate Deal Imported Header if repeat
  for (let i = 0, j = labelList.length; i < j; i++) {
    const label = labelList.filter((lab) => labelList[i].value === lab.value);
    if (label.length > 1) {
      const response = getKeyHeaderArray(headers, labelList[i]);
      console.log('dealDataHeaderError', response);
      return Flux.dispatchEvent(DEAL_IMPORTED_HEADER_ERROR_EVENT, response);
    }
  }

  for (let i = 0, j = labelList.length; i < j; i++) {
    const position = labelList[i].header;
    const key = labelList[i].value;
    for (let k = 0, h = dataCSV.length; k < h; k++) {
      const dealRow = dataCSV[k];
      const value = dealRow[0][position];
      console.log('LabelList:key', key);
      try {
        dealImportedValidator(
          key,
          value,
          availableDealsSourceIds,
          availableDealsSourceIdsFromOtherCompany,
          dataCSV,
          associatedDealIdLabel,
          dealSourceIdLabel,
          currency,
        );

        if (key === 'stage') {
          dealMappingValidator(mappingList, value);
        }
      } catch (e) {
        const keys = Object.keys(dealRow[0]);
        keys.forEach((i, index) => {
          if (i === position) {
            response.indexColumn = index;
          }
        });
        response.error = e.message;
        response.indexRow = k;
        console.log('dealDataImportedError', response);
        return Flux.dispatchEvent(DEAL_ERROR_EVENT, response);
      }
    }
  }
  response = true;
  return Flux.dispatchEvent(DEAL_IMPORTED_VALID_EVENT, response);
};

const getKeyHeaderArray = (dataHeaders, header) => {
  const arrayKeys = [];
  for (let i = 0, j = dataHeaders.length; i < j; i++) {
    if (dataHeaders[i] && dataHeaders[i].value === header.value) {
      arrayKeys.push(i);
    }
  }
  return arrayKeys;
};

/**
 * Delete the Deals List.
 *
 * @returns {Promise<void>}
 */
export const deleteDealList = async () => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const dealsId = await _fetchDealsId();
  const operations = [];

  for (let i = 0, j = dealsId.length; i < j; i++) {
    const dealId = dealsId[i].id;
    let operation;
    try {
      operation = client.mutate({
        mutation: DEAL_DATA_DELETE_MUTATION,
        variables: { data: { id: dealId, force: true } },
      });
    } catch (e) {
      error('deleteDeal', e);
      continue;
    }
    operations.push(operation);
  }
  await Promise.all(operations);
  return Flux.dispatchEvent(DEAL_DELETE_LIST_EVENT, {});
};

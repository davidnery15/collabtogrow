import React from 'react';
import { Card, Heading, Label, Loader } from '@8base/boost';
import dealStore, {
  DEAL_CREATE_EVENT,
  DEAL_ERROR_EVENT,
  DEAL_IMPORTED_EVENT,
  DEAL_IMPORTED_VALID_EVENT,
  DEAL_IMPORTED_HEADER_ERROR_EVENT,
  DEAL_IMPORTED_ERROR_EVENT,
} from './deal-store';
import * as toast from 'components/toast/Toast';
import { withRouter } from 'react-router-dom';
import { PropTypes } from 'prop-types';
import { DEAL_FIELDS } from './deal-model';
import View from '@cobuildlab/react-flux-state';
import { onErrorMixin } from '../../../shared/mixins';
import Header from '../../../components/Header';
import { createDealFromCSV } from './deal-actions';
import { DealHeader } from './components/DealHeader';
import { ActionButton } from '../../../components/buttons/ActionButton';
import { Redirect } from 'react-router-dom';
import { checkDataCvs } from './deal-actions';
import * as R from 'ramda';
import { fetchCurrentAllianceMembersAction } from '../../settings/alliance-management/alliance-actions';
import allianceStore, {
  ALLIANCE_LIST_MEMBERS_EVENT,
} from '../../settings/alliance-management/alliance-store';
import { DealDataCSVRow } from './components/DealDataCSVRow';
import { getCurrencyOnSession } from '../../../shared/alliance-utils';

/**
 * Import Deal
 */
class DealImportedView extends View {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      allianceMembersOptions: [],
      data: [],
      dealOwner: [],
      dealsCreated: 0,
      dealImportedValid: false,
      dealImportedError: {},
      dealImportedHeaderError: {},
    };
    this.onError = onErrorMixin.bind(this);
    const state = dealStore.getState(DEAL_IMPORTED_EVENT);

    if (state) {
      const { data, headers, dealsCompany, otherCompany } = state;
      this.data = data.filter((d) => d.length !== 0);
      console.log('DealImportedView:DEBUG', state);
      this.headers = headers;
      this.company = dealsCompany;
      this.otherCompany = otherCompany;
      this.state.data = new Array(this.headers.length).fill(null);
      this.state.dealOwner = new Array(this.data.length).fill(null);
      this.dealsCreated = 0;
    }
    this.dealFieldsOptions = DEAL_FIELDS.map(({ id, label }) => {
      return { label, value: id };
    });
  }

  componentDidMount() {
    this.subscribe(dealStore, DEAL_ERROR_EVENT, (state) => {
      const { dealImportedError } = this.state;
      const dealImportedHeaderError = {};

      dealImportedError.indexRow = state.indexRow;
      dealImportedError.indexColumn = state.indexColumn;

      this.setState({ dealImportedError, dealImportedHeaderError });

      toast.error(state.error);
    });

    this.subscribe(dealStore, DEAL_IMPORTED_HEADER_ERROR_EVENT, (state) => {
      const { dealImportedHeaderError } = this.state;
      const dealImportedError = {};
      dealImportedHeaderError.rows = state;

      this.setState({ dealImportedHeaderError, dealImportedError });
      toast.error(`Deal imported can't repeat header fields`);
    });

    this.subscribe(dealStore, DEAL_IMPORTED_VALID_EVENT, (state) => {
      if (state) {
        this.setState({ loading: true }, () => {
          createDealFromCSV(
            this.state.dealOwner,
            this.data,
            this.state.data,
            this.company,
            this.otherCompany,
          );
        });
      }
    });

    this.subscribe(dealStore, DEAL_IMPORTED_ERROR_EVENT, this.onError);

    this.subscribe(dealStore, DEAL_CREATE_EVENT, (state) => {
      const { operations } = state;
      this.setState({ dealsCreated: this.state.dealsCreated + operations });
      if (this.state.dealsCreated >= this.data.length) {
        toast.success('Deals Successfully Imported');
        this.props.history.push('/management/deal');
      }
    });

    this.subscribe(allianceStore, ALLIANCE_LIST_MEMBERS_EVENT, (state) => {
      const { clientCompany, partnerCompany } = state;
      const clientCompanyMembers = clientCompany.companyUserRelation.items;
      const partnerCompanyMembers = partnerCompany ? partnerCompany.companyUserRelation.items : [];
      const allianceMembers = clientCompanyMembers.concat(partnerCompanyMembers);
      this.setState({
        loading: false,
        allianceMembersOptions: allianceMembers.map((member) => {
          return {
            label: `${member.user.firstName} ${member.user.lastName}`,
            value: member.user.id,
          };
        }),
      });
    });

    fetchCurrentAllianceMembersAction();
  }

  onSubmit = () => {
    const headers = R.clone(this.state.data);
    const dataCvs = R.clone(this.data);
    checkDataCvs(headers, dataCvs, this.company, this.otherCompany, getCurrencyOnSession());
  };

  render() {
    const {
      loading,
      data,
      dealsCreated,
      dealImportedError,
      dealImportedHeaderError,
      allianceMembersOptions,
      dealOwner,
    } = this.state;

    if (data.length === 0) {
      return <Redirect to={'/management/deal/import'} />;
    }

    const dataCvs = this.data;

    let content = (
      <>
        <div style={{ height: '80%' }}>
          <Loader stretch />
          <Label>This may take up to a minute... </Label>
          {dealsCreated > 1 ? <Label>{`${dealsCreated} deals Imported!`}</Label> : null}
        </div>
      </>
    );
    let footer = <></>;
    let headerText = '';

    if (!loading) {
      headerText = 'Deals Information';
      const headers = this.headers;

      content = (
        // <Scrollable>
        <table>
          <thead>
            <tr>
              <th>Alliance Deal Owner</th>
              {this.headers.map((header, i) => {
                // Error Management
                let addClassError = false;
                if (dealImportedHeaderError.rows !== undefined) {
                  const indexError = dealImportedHeaderError.rows.find((ind) => ind === i);
                  if (indexError !== undefined) {
                    addClassError = true;
                  }
                }

                return (
                  <th key={i}>
                    <DealHeader
                      options={this.dealFieldsOptions}
                      data={data}
                      header={header}
                      index={i}
                      addClassError={addClassError}
                    />
                  </th>
                );
              })}
            </tr>
            <tr>
              <th className={'table-header-cell'} />
              {this.headers.map((header, i) => {
                return (
                  <th key={i} className={'table-header-cell'}>
                    {header}
                  </th>
                );
              })}
            </tr>
          </thead>

          <tbody>
            {this.data.map((row, i) => {
              const columns = headers.map((header) => row[0][header]);
              return (
                <DealDataCSVRow
                  key={i}
                  index={i}
                  columns={columns}
                  dataCvs={dataCvs}
                  dealImportedError={dealImportedError}
                  allianceMembersOptions={allianceMembersOptions}
                  dealOwner={dealOwner}
                />
              );
            })}
          </tbody>
        </table>
        // {/* </Scrollable> */}
      );
      footer = (
        <Card.Footer>
          <ActionButton text={'Save'} onClick={this.onSubmit} />
        </Card.Footer>
      );
    }

    return (
      <React.Fragment>
        <Card.Header>
          <Heading type="h4" text={`Import Deals of: ${this.company.name}`} />
        </Card.Header>
        <Card.Body
          scrollable
          borderRadius="all"
          style={{ padding: 20, textAlign: 'center' }}
          className="card-body card-body-deals-import">
          <Header text={headerText} />
          {content}
        </Card.Body>
        {footer}
      </React.Fragment>
    );
  }
}

DealImportedView.propTypes = {
  history: PropTypes.object.isRequired,
};

export default withRouter(DealImportedView);

import React from 'react';
import { InputField, DateInputField, Column, SelectField } from '@8base/boost';
import PropTypes from 'prop-types';

import { CurrencyInputField } from '../../../../shared/components/CurrencyInputField';

/**
 * The Form for the Action
 */
class DealColumnForm extends React.Component {
  render() {
    const { onChange, currency, company, allianceMembersOptions } = this.props;

    const {
      customerName,
      name,
      amount,
      description,
      closeDate,
      lastActivityDate,
      lastContactedDate,
      lastModifiedDate,
      nextActivityDate,
      numberOfSalesActivities,
      salesPerson,
      type,
      wonReason,
      stage,
      lostReason,
      nextActivityDescription,
      numberOfTimesContacted,
      line,
      owner,
      dealSourceId,
    } = this.props.company;

    const companyName = company.company.name;
    return (
      <>
        <Column alignItems="stretch">
          <h2>{companyName}</h2>
          <InputField
            stretch
            label="Customer Name"
            input={{
              name: 'customerName',
              value: customerName,
              onChange: (value, e) => onChange(e.target.name, value),
            }}
          />
          <InputField
            stretch
            label="Name"
            input={{
              name: 'name',
              value: name,
              onChange: (value, e) => onChange(e.target.name, value),
            }}
          />
          <InputField
            stretch
            label="Description"
            input={{
              name: 'description',
              value: description,
              onChange: (value, e) => onChange(e.target.name, value),
            }}
          />
          <CurrencyInputField
            currency={currency}
            label={`Amount`}
            value={amount}
            onChange={(val) => {
              onChange('amount', val);
            }}
          />
          <DateInputField
            style={{ width: '50%' }}
            label="Closed Date"
            input={{
              name: 'closeDate',
              value: closeDate,
              onChange: (value) => onChange('closeDate', value),
            }}
          />

          <DateInputField
            style={{ width: '50%' }}
            label="Last Activity Date"
            input={{
              name: 'lastActivityDate',
              value: lastActivityDate,
              onChange: (value) => onChange('lastActivityDate', value),
            }}
          />

          <DateInputField
            style={{ width: '50%' }}
            label="Last Contacted Date"
            input={{
              name: 'lastContactedDate',
              value: lastContactedDate,
              onChange: (value) => onChange('lastContactedDate', value),
            }}
          />

          <DateInputField
            style={{ width: '50%' }}
            label="Last Modified Date"
            input={{
              name: 'lastModifiedDate',
              value: lastModifiedDate,
              onChange: (value) => onChange('lastModifiedDate', value),
            }}
          />

          <DateInputField
            style={{ width: '50%' }}
            label="Next Activity Date	"
            input={{
              name: 'nextActivityDate',
              value: nextActivityDate,
              onChange: (value) => onChange('nextActivityDate', value),
            }}
          />

          <InputField
            stretch
            label="Number Of Sales Activities"
            input={{
              name: 'numberOfSalesActivities',
              value: numberOfSalesActivities,
              onChange: (value, e) => onChange(e.target.name, value),
            }}
          />

          <InputField
            stretch
            label="Sales Person"
            input={{
              name: 'salesPerson',
              value: salesPerson,
              onChange: (value, e) => onChange(e.target.name, value),
            }}
          />

          <InputField
            stretch
            label="Type"
            input={{
              name: 'type',
              value: type,
              onChange: (value, e) => onChange(e.target.name, value),
            }}
          />

          <InputField
            stretch
            label="Won Reason"
            input={{
              name: 'wonReason',
              value: wonReason,
              onChange: (value, e) => onChange(e.target.name, value),
            }}
          />

          <InputField
            stretch
            label="Stage"
            input={{
              name: 'stage',
              value: stage,
              onChange: (value, e) => onChange(e.target.name, value),
            }}
          />

          <InputField
            stretch
            label="Next Activity Description"
            input={{
              name: 'nextActivityDescription',
              value: nextActivityDescription,
              onChange: (value, e) => onChange(e.target.name, value),
            }}
          />

          <InputField
            stretch
            label="Lost Reason"
            input={{
              name: 'lostReason',
              value: lostReason,
              onChange: (value, e) => onChange(e.target.name, value),
            }}
          />

          <InputField
            stretch
            label="Number of time contacted"
            input={{
              name: 'numberOfTimesContacted',
              value: numberOfTimesContacted,
              onChange: (value, e) => onChange(e.target.name, value),
            }}
          />
          <InputField
            stretch
            label="Line"
            input={{
              name: 'line',
              value: line,
              onChange: (value, e) => onChange(e.target.name, value),
            }}
          />
          <SelectField
            label="Deal Owner"
            input={{
              name: 'Owner',
              value: owner,
              onChange: (value) => onChange('owner', value),
            }}
            meta={{}}
            placeholder="Select a Deal Owner"
            options={allianceMembersOptions}
          />
          <InputField
            stretch
            label="Deal Source Id"
            input={{
              name: 'dealSourceId',
              value: dealSourceId,
              onChange: (value, e) => onChange(e.target.name, value),
            }}
          />
        </Column>
      </>
    );
  }
}

DealColumnForm.propTypes = {
  currency: PropTypes.object.isRequired,
  company: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  allianceMembersOptions: PropTypes.array.isRequired,
};

export default DealColumnForm;

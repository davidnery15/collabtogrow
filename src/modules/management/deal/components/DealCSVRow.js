import React from 'react';
import { PropTypes } from 'prop-types';
import { Select, Grid } from '@8base/boost';
import DealDataInput from './DealDataInput';

// eslint-disable-next-line react/prop-types
const CustomOption = ({ children, data, ...rest }) => {
  return (
    <Select.components.Option {...rest}>
      <div className={'table-select'}>
        <Grid.Layout columns={'60px auto'} padding={'xs'}>
          <Grid.Box>
            <div className={'table-header-cell gray-border'}>ID</div>
          </Grid.Box>
          <Grid.Box>{data.dealSourceId}</Grid.Box>
          <Grid.Box>
            <div className={'table-header-cell'}>Name:</div>
          </Grid.Box>
          <Grid.Box>{data.label}</Grid.Box>
        </Grid.Layout>
      </div>
    </Select.components.Option>
  );
};

// eslint-disable-next-line react/prop-types
const SingleValue = ({ children, data, ...rest }) => {
  return (
    <Select.components.SingleValue {...rest}>{data.dealSourceId}</Select.components.SingleValue>
  );
};

class DealCSVRow extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      dealValue: null,
      ownerValue: null,
    };
  }

  onChangeOwner = (value) => {
    this.setState({ ownerValue: value }, () => {
      this.props.dealOwner[this.props.index] = value;
    });
  };

  render() {
    const { columns, index, dealImportedError, data, onChangeDeal } = this.props;
    const indexError = dealImportedError.indexRow === index ? dealImportedError.indexColumn : null;

    return (
      <tr>
        <td>
          <Select
            style={{ width: '250px', margin: '0.5em' }}
            placeholder="Select a Deal"
            options={this.props.options}
            components={{ Option: CustomOption, SingleValue }}
            clearable
            value={data[index]}
            onChange={(value) => onChangeDeal(value, index)}
          />
        </td>
        <td>
          <Select
            style={{ width: '250px', margin: '0.5em' }}
            placeholder="Select a Owner"
            options={this.props.allianceMembersOptions}
            clearable
            value={this.state.ownerValue}
            onChange={this.onChangeOwner}
          />
        </td>
        {columns.map((col, i) => {
          let addClassError = false;
          if (indexError === i) {
            addClassError = true;
          }

          return (
            <td key={i}>
              <DealDataInput
                indexRow={this.props.index}
                index={i}
                columns={this.props.columns}
                value={col}
                dataCvs={this.props.dataCvs}
                addClassError={addClassError}
              />
            </td>
          );
        })}
      </tr>
    );
  }
}

DealCSVRow.propTypes = {
  index: PropTypes.number.isRequired,
  columns: PropTypes.array.isRequired,
  options: PropTypes.array.isRequired,
  data: PropTypes.array.isRequired,
  dataCvs: PropTypes.array.isRequired,
  dealImportedError: PropTypes.object.isRequired,
  allianceMembersOptions: PropTypes.array.isRequired,
  dealOwner: PropTypes.array.isRequired,
  onChangeDeal: PropTypes.func.isRequired,
};

export { DealCSVRow };

import { DEAL_CLOSED, DEAL_COMPLETED } from '../../../shared/status';
import {
  isUserAdminOrSERInAlliance,
  isUserCreatorInAlliance,
} from '../../../shared/alliance-utils';
import { isAllianceCompleted } from '../../settings/alliance-management/alliance-permissions';

/**
 * Checks if a User can Edit A Deal
 * @param {User} user The user
 * @param {Alliance} alliance The Alliance
 * @param {Deal} deal The Deal
 */
export const canEditDeal = (user, deal, alliance): boolean => {
  if (!deal || !deal.status || !user || !alliance) return false;

  if (isAllianceCompleted(alliance)) return false;

  if (isUserAdminOrSERInAlliance(user, alliance)) return true;

  if (deal.status === DEAL_CLOSED) return false;
  if (deal.status === DEAL_COMPLETED) return false;

  if (isUserCreatorInAlliance(user, alliance)) return true;

  return false;
};

/**
 * Checks if a User can Delete A Deal
 * @param {User} user The user
 * @param {Alliance} alliance The Alliance
 * @param {Deal} deal The Deal
 */
export const canDeleteDeal = (user, deal, alliance) => {
  if (isAllianceCompleted(alliance)) return false;

  if (deal.status === DEAL_CLOSED) return false;

  return isUserAdminOrSERInAlliance(user, alliance);
};

/**
 * Checks if a User can Close a Deal
 * @param {User} user The user
 * @param {Alliance} alliance The Alliance
 * @param {Deal} deal The Deal
 */
export const canCompletedDeal = (user, deal, alliance) => {
  if (isAllianceCompleted(alliance)) return false;

  if (deal.status === DEAL_COMPLETED) return false;

  return isUserAdminOrSERInAlliance(user, alliance);
};

/**
 *
 * @param user
 * @param alliance
 * @returns {boolean}
 */
export const canCreateDeal = (user, alliance) => {
  if (isAllianceCompleted(alliance)) return false;

  if (isUserAdminOrSERInAlliance(user, alliance)) return true;

  if (isUserCreatorInAlliance(user, alliance)) return true;

  return false;
};

export const canBulkDeals = (alliance) => {
  return !isAllianceCompleted(alliance);
};

export const canImportCSV = (alliance) => {
  return !isAllianceCompleted(alliance);
};

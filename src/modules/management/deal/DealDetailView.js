import React from 'react';
import View from '@cobuildlab/react-flux-state';
import { Button, Card, Heading, Loader } from '@8base/boost';
import { onErrorMixin } from '../../../shared/mixins';
import * as R from 'ramda';
import sessionStore, { NEW_SESSION_EVENT } from '../../../shared/SessionStore';
import DealDetailTable from './components/DealDetailTable';
import {
  openComments,
  completedDeal,
  fetchDealData,
  fetchRelatedDeals,
  fetchAssociatedDeals,
} from './deal-actions';
import dealStore, {
  DEAL_DETAIL_EVENT,
  DEAL_ERROR_EVENT,
  DEAL_COMPLETED_EVENT,
  RELATED_DEAL_LIST_EVENT,
  DEAL_LIST_EVENT,
} from './deal-store';
import * as toast from '../../../components/toast/Toast';
import PropTypes from 'prop-types';
import withAlliance from '../../../components/hoc/withAlliance';
import { withRouter } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { canCompletedDeal } from './deal-permissions';
import YesNoDialog from '../../../components/dialogs/YesNoDialog';
import {
  fetchRelatedItems,
  fetchRelatedItemsByItemId,
} from '../../related-item/related-item-actions';
import { DealData } from './deal-model';
import { getCurrencyOnSession } from '../../../shared/alliance-utils';
import { DetailViewCardBody } from '../../../components/card/DetailViewCardBody';

import { DEAL_COMPLETED } from '../../../shared/status';
import { RelatedItemsByItemDetailTable } from '../../related-item/components/RelatedItemsByItemDetailTable';
import relatedItemStore, {
  RELATED_ITEMS_BY_ITEM_EVENT,
  RELATED_ITEMS_EVENT,
} from '../../related-item/related-item-store';
import { getItemByType } from '../../../shared/items-util';
import { TopButtons } from '../../../components/buttons/TopButtons';
import { RelatedItemsDetailTable } from '../../related-item/components/RelatedItemsDetailTable';
import { AssociatedItemsTable } from '../../related-item/components/AssociatedItemsTable';

/**
 * Deal Detail View
 */
class DealDetailView extends View {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        dealData: R.clone(DealData),
        relatedDeals: [],
        relatedItems: [],
        associatedItems: [],
        relatedItemsByItem: [],
      },
      loading: true,
      completedModalIsOpen: false,
    };
    this.user = sessionStore.getState(NEW_SESSION_EVENT).user;
    this.onError = onErrorMixin.bind(this);
    this.selectedAlliance = sessionStore.getState(NEW_SESSION_EVENT).selectedAlliance;
  }

  componentDidMount = () => {
    this.subscribe(dealStore, DEAL_ERROR_EVENT, this.onError);
    this.subscribe(dealStore, DEAL_DETAIL_EVENT, ({ dealDatum }) => {
      const dealData = { itemId: dealDatum.itemDealDataRelation.id, ...dealDatum };
      const { data } = this.state;

      data.dealData = dealData;
      this.setState(
        {
          data,
          loading: false,
        },
        () => setTimeout(() => fetchRelatedDeals(dealData), 0),
        fetchRelatedItems(dealData.itemId),
        fetchRelatedItemsByItemId(dealData.itemId),
        fetchAssociatedDeals(false, dealData.dealSourceId, match.params.id),
      );
    });

    this.subscribe(dealStore, RELATED_DEAL_LIST_EVENT, (deals) => {
      const { data } = this.state;
      data.relatedDeals = deals;
      this.setState({ data });
    });

    this.subscribe(dealStore, DEAL_COMPLETED_EVENT, () => {
      const { data } = this.state;
      data.dealData.status = DEAL_COMPLETED;
      this.setState({
        data,
        loading: false,
      });
      toast.success('Deal Successfully Closed');
    });

    this.subscribe(relatedItemStore, RELATED_ITEMS_EVENT, (state) => {
      const items = state.item.itemsRelated.items.map((item) => getItemByType(item));
      const { data } = this.state;

      data.relatedItems = items;
      this.setState({ data });
    });

    this.subscribe(relatedItemStore, RELATED_ITEMS_BY_ITEM_EVENT, (state) => {
      const {
        itemsList: { items: itemsRelated },
      } = state;
      const relatedItemsByItem = itemsRelated.map((item) => getItemByType(item));
      const { data } = this.state;

      data.relatedItemsByItem = relatedItemsByItem;
      this.setState({ data, loading: false });
    });
    this.subscribe(dealStore, DEAL_LIST_EVENT, (items) => {
      const { data } = this.state;
      data.associatedItems = items.dealDataList.items;
      this.setState({
        data,
      });
    });

    const { match } = this.props;
    if (!match.params.id) return toast.error('Deal ID missing');
    fetchDealData(match.params.id);
  };

  completedModal = () => {
    this.setState({
      completedModalIsOpen: true,
    });
  };

  onYesModal = () => {
    this.setState(
      {
        completedModalIsOpen: false,
        loading: true,
      },
      () => {
        const dealData = R.clone(this.state.data.dealData);
        completedDeal(this.user, dealData, { id: this.props.allianceId });
      },
    );
  };

  onCompletedModal = () => {
    this.setState({
      completedModalIsOpen: false,
    });
  };

  render() {
    const { loading, completedModalIsOpen, data } = this.state;
    const { dealData, relatedItems, relatedItemsByItem, associatedItems } = data;
    const { history } = this.props;
    let content = <Loader stretch />;
    let buttonsBottom = '';
    let buttonsTop = '';
    const currency = getCurrencyOnSession();
    const alliance = this.selectedAlliance;

    if (!loading) {
      content = (
        <>
          <DealDetailTable
            currency={currency}
            dealData={dealData}
            onClickEdit={() => history.push(`/management/deal/edit/${dealData.id}`)}
          />
          <AssociatedItemsTable associatedItems={associatedItems} />
          <RelatedItemsDetailTable relatedItems={relatedItems} />
          <RelatedItemsByItemDetailTable relatedItemsByItem={relatedItemsByItem} />
        </>
      );

      buttonsTop = (
        <>
          <Heading type="h4" text={dealData.name} />

          <TopButtons
            onClickClosed={history.goBack}
            onClickCollaborated={() => openComments(dealData)}
          />
        </>
      );

      buttonsBottom = (
        <div className="positionBottonRight">
          {canCompletedDeal(this.user, dealData, alliance) ? (
            <Button
              onClick={() => {
                this.completedModal();
              }}>
              <FontAwesomeIcon icon="clipboard-list" />
              &nbsp; Mark Completed
            </Button>
          ) : null}
        </div>
      );
    }

    return (
      <React.Fragment>
        <Card.Header>{buttonsTop}</Card.Header>
        <DetailViewCardBody>{content}</DetailViewCardBody>
        <Card.Footer>{buttonsBottom}</Card.Footer>
        <YesNoDialog
          title={'Complete Deal'}
          onYes={this.onYesModal}
          onClose={this.onClose}
          onNo={this.onCompletedModal}
          text={'Are you sure you want to Mark the Deal as Completed?'}
          isOpen={completedModalIsOpen}
        />
      </React.Fragment>
    );
  }
}

DealDetailView.propTypes = {
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default withRouter(withAlliance(DealDetailView));

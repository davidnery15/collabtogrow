import { DEAL_OPEN } from '../../../shared/status';
import moment from 'moment';

const currentDate = moment().format('YYYY-MM-DD');

/**
 * @deprecated
 * @type {{owner: null, name: string, id: null, status: string}}
 */
export const Deal = {
  id: null,
  name: '',
  owner: null,
  status: DEAL_OPEN,
};

export const DealData = {
  status: DEAL_OPEN,
  customerName: '',
  amount: '0',
  company: {},
  closeDate: currentDate,
  createdDate: currentDate,
  lastActivityDate: currentDate,
  lastContactedDate: currentDate,
  lastModifiedDate: currentDate,
  nextActivityDate: currentDate,
  numberOfSalesActivities: '',
  salesPerson: '',
  type: '',
  wonReason: '',
  stage: '',
  lostReason: '',
  numberOfTimesContacted: '',
  nextActivityDescription: '',
  line: '',
  dealSourceId: '',
  associatedDealId: '',
  partnerRegistrationId: '',
  dealReferencable: '',
  owner: null,
  description: '',
};

export type Currency = {
  __typename?: 'Currency',
  id?: string,
  symbol?: string,
  thousandSeparator?: string,
  decimalSeparator?: string,
  name?: string,
};

export const DEAL_FIELDS = [
  { id: 'customerName', label: 'Customer Name' },
  { id: 'amount', label: 'Amount' },
  { id: 'dealSourceId', label: 'Deal Source ID' },
  { id: 'associatedDealId', label: 'Associated Deal ID' },
  { id: 'partnerRegistrationId', label: 'Partner Registration ID' },
  { id: 'closeDate', label: 'Close Date' },
  { id: 'lostReason', label: 'Lost Reason' },
  { id: 'wonReason', label: 'Won Reason' },
  { id: 'createdDate', label: 'Created Date' },
  { id: 'name', label: 'Deal Name' },
  { id: 'description', label: 'Deal Description' },
  { id: 'stage', label: 'Deal Stage' },
  { id: 'type', label: 'Deal Type' },
  { id: 'salesPerson', label: 'Sales Person' },
  { id: 'lastActivityDate', label: 'Last Activity Date' },
  { id: 'lastContactedDate', label: 'Last Contacted Date' },
  { id: 'lastModifiedDate', label: 'Last Modified Date' },
  { id: 'nextActivityDate', label: 'Next Activity Date' },
  { id: 'nextActivityDescription', label: 'Next Activity Description' },
  { id: 'numberOfSalesActivities', label: 'Number of Sales Activities' },
  { id: 'numberOfTimesContacted', label: 'Number of Times Contacted' },
  { id: 'line', label: 'Product Line' },
  { id: 'dealReferencable', label: 'Deal Referencable (Y / N)' },
];

/**
 * DEAL STAGES
 * @type {String}
 */
export const DEAL_STAGE_QUALIFICATION = 'Qualification';
export const DEAL_STAGE_NEEDS_ANALYSIS = 'Needs Analysis';
export const DEAL_STAGE_PROPOSAL_CREATION = 'Proposal Creation';
export const DEAL_STAGE_PROPOSAL_PRESENTATION = 'Proposal Presentation';
export const DEAL_STAGE_NEGOTIATION = 'Negotiation';
export const DEAL_STAGE_CLOSED_WON = 'Closed Won';
export const DEAL_STAGE_CLOSED_LOST = 'Closed Lost';
export const DEAL_STAGES_MAP = {
  [DEAL_STAGE_QUALIFICATION]: 10,
  [DEAL_STAGE_NEEDS_ANALYSIS]: 20,
  [DEAL_STAGE_PROPOSAL_CREATION]: 30,
  [DEAL_STAGE_PROPOSAL_PRESENTATION]: 50,
  [DEAL_STAGE_NEGOTIATION]: 90,
  [DEAL_STAGE_CLOSED_WON]: 100,
  [DEAL_STAGE_CLOSED_LOST]: 0,
};
export const DEAL_ACTIVE_STAGES = [
  DEAL_STAGE_QUALIFICATION,
  DEAL_STAGE_NEEDS_ANALYSIS,
  DEAL_STAGE_PROPOSAL_CREATION,
  DEAL_STAGE_PROPOSAL_PRESENTATION,
  DEAL_STAGE_NEGOTIATION,
];
export const PROPOSAL_ISSUED_STAGES = [
  DEAL_STAGE_PROPOSAL_PRESENTATION,
  DEAL_STAGE_NEGOTIATION,
  DEAL_STAGE_CLOSED_WON,
  DEAL_STAGE_CLOSED_LOST,
];

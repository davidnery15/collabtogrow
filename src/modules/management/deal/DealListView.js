import React from 'react';
import { Card, Heading, Grid, Switch } from '@8base/boost';
import { DealCardBody } from 'components/card/DealCardBody';
import dealStore, {
  DEAL_LIST_EVENT,
  DEAL_ERROR_EVENT,
  DEAL_DELETE_EVENT,
  DEAL_DELETE_LIST_EVENT,
  DEAL_STAGES_EVENT,
  DEAL_COMPANIES_EVENT,
} from './deal-store';

import {
  deleteDeal,
  deleteDealList,
  fetchDealsData,
  fetchDealsStages,
  fetchDealsCompanies,
} from './deal-actions';
import sessionStore, { NEW_SESSION_EVENT } from '../../../shared/SessionStore';
import { canCreateDeal, canBulkDeals, canImportCSV } from './deal-permissions';
import withAlliance from '../../../components/hoc/withAlliance';
import View from '@cobuildlab/react-flux-state';
import { PropTypes } from 'prop-types';
import YesNoDialog from '../../../components/dialogs/YesNoDialog';
import { onErrorMixin, onListScrollMixin } from '../../../shared/mixins';
import { withRouter } from 'react-router-dom';
import * as toast from '../../../components/toast/Toast';
import { TransparentButtonSvg } from '../../../components/buttons/TransparentButtonSvg';
import { ActionButtonListView } from '../../../components/buttons/ActionButtonListView';
import importCSV from '../../../images/icons/import-csv.svg';
import SearchInput from '../../../components/inputs/SearchInput';
import { debounce } from '../../../shared/utils';
import { getCurrencyOnSession } from '../../../shared/alliance-utils';
import DealTable from './components/DealTable';
import DealFullTable from './components/DealFullTable';
import ItemFilter from '../../../components/ItemFilter';

/**
 * List All the Deal Items.
 *
 * @param value
 * @param deal
 */
class DealListView extends View {
  constructor(props) {
    super(props);
    this.state = {
      dealsList: [],
      deal: {},
      loading: true,
      loadingPage: false,
      search: '',
      page: 1,
      count: 0,
      deleteModalIsOpen: false,
      bulkDeleteModalIsOpen: false,
      isFullTable: false,
      filter: null,
      stages: [],
      itemFilter: null,
      companiesOptions: [],
    };
    this.user = sessionStore.getState(NEW_SESSION_EVENT).user;
    this.onError = onErrorMixin.bind(this);
    this.onListScroll = onListScrollMixin.bind(this);
    this.searchWithDebounce = debounce(300, this.searchWithDebounce)();
    this.selectedAlliance = sessionStore.getState(NEW_SESSION_EVENT).selectedAlliance;
  }

  componentDidMount() {
    this.subscribe(dealStore, DEAL_ERROR_EVENT, this.onError);
    this.subscribe(dealStore, DEAL_LIST_EVENT, (state) => {
      const { page } = state;
      const { items, count } = state.dealDataList;
      const { loadingPage, dealsList: oldDealsList } = this.state;
      const dealsList = loadingPage ? oldDealsList.concat(items) : items;

      this.setState({
        dealsList,
        loading: false,
        loadingPage: false,
        page,
        count,
      });
    });

    this.subscribe(dealStore, DEAL_STAGES_EVENT, (stages) => {
      this.setState({
        stages,
      });
      this.subscribe(dealStore, DEAL_COMPANIES_EVENT, (companiesOptions) => {
        this.setState({
          companiesOptions,
        });
      });
    });
    this.subscribe(dealStore, DEAL_DELETE_EVENT, () => {
      const { search, filter, itemFilter } = this.state;
      toast.success('Deal Deleted!');
      fetchDealsData(search, 1, 20, false, filter, itemFilter);
    });

    this.subscribe(dealStore, DEAL_DELETE_LIST_EVENT, () => {
      const { search, filter, itemFilter } = this.state;
      toast.success('Deals List Deleted!');
      fetchDealsData(search, 1, 20, false, filter, itemFilter);
    });

    const searchParams = new URLSearchParams(this.props.location.search);
    const urlStage = searchParams.get('stage');
    const urlCompnay = searchParams.get('company');
    if (urlStage && urlCompnay) {
      const company = urlCompnay === 'null' ? '' : urlCompnay;
      this.setState(
        {
          filter: urlStage,
          itemFilter: company,
        },
        () => fetchDealsData('', 1, 20, false, urlStage, company),
      );
    } else {
      fetchDealsData();
    }
    fetchDealsStages();
    fetchDealsCompanies();
  }

  goToImportDeals = () => {
    this.props.history.push('/management/deal/import');
  };

  onSelectForDelete = (deal) => {
    this.setState({
      deal,
      deleteModalIsOpen: true,
    });
  };

  onYes = () => {
    const { companiesOptions } = this.state;
    const { deal } = this.state;
    const otherCompany = companiesOptions.find(
      (company) => company.id !== deal.company.id && company.id,
    );
    this.setState(
      {
        deleteModalIsOpen: false,
        loading: true,
      },
      () => deleteDeal(this.state.deal, otherCompany),
    );
  };

  onClose = () => {
    this.setState({
      deleteModalIsOpen: false,
    });
  };

  onBulkDelete = (deal) => {
    this.setState({
      bulkDeleteModalIsOpen: true,
    });
  };

  onYesBulkDelete = () => {
    this.setState(
      {
        bulkDeleteModalIsOpen: false,
        loading: true,
      },
      () => {
        const { dealsList } = this.state;
        deleteDealList(dealsList);
      },
    );
  };

  onCloseBulkDelete = () => {
    this.setState({
      bulkDeleteModalIsOpen: false,
    });
  };

  onSearchChange = (value) => {
    this.setState({ search: value });
    this.searchWithDebounce(value);
  };

  searchWithDebounce = (value) => {
    const { filter, itemFilter } = this.state;
    this.setState({ loading: true }, () => {
      fetchDealsData(value, 1, 20, false, filter, itemFilter);
    });
  };
  stageFilter = (value) => {
    const { search, itemFilter } = this.state;
    this.setState(
      {
        loading: true,
        filter: value,
      },
      () => {
        fetchDealsData(search, 1, 20, false, value, itemFilter);
      },
    );
  };
  companyFilter = (value) => {
    const { search, filter } = this.state;
    this.setState(
      {
        loading: true,
        itemFilter: value,
      },
      () => {
        fetchDealsData(search, 1, 20, false, filter, value);
      },
    );
  };

  render() {
    const {
      dealsList,
      deleteModalIsOpen,
      bulkDeleteModalIsOpen,
      search,
      loadingPage,
      loading,
      isFullTable,
      filter,
      stages,
      itemFilter,
      companiesOptions,
    } = this.state;
    const { history } = this.props;
    const currency = getCurrencyOnSession();
    const alliance = this.selectedAlliance;

    if (filter !== null && stages.indexOf(filter) === -1) {
      stages.push(filter);
    }

    return (
      <div className="items-card">
        <Card.Header>
          <Grid.Layout
            columns="7fr 9fr 9fr 9fr 3fr 4fr 20fr"
            areas={[['left', 'left2', 'center', 'right', 'right-1', 'right-2', 'right-3']]}
            style={{ width: '100%' }}>
            <Grid.Box justifySelf="flex-start" area="left" className="list-box">
              <Heading type="h4" text="Deals" />
            </Grid.Box>
            <Grid.Box area="left2" className="list-box">
              <SearchInput value={search} onChange={this.onSearchChange} />
            </Grid.Box>
            <Grid.Box area="center" className="list-box">
              <ItemFilter
                onChange={this.companyFilter}
                value={itemFilter}
                options={companiesOptions.map((company) => {
                  return { label: company.name, value: company.id };
                })}
                placeholder="Companies"
              />
            </Grid.Box>
            <Grid.Box area="right" className="list-box">
              <ItemFilter
                onChange={this.stageFilter}
                value={filter}
                options={stages.map((stage) => {
                  return { label: stage, value: stage };
                })}
                placeholder="Filter By Stage"
              />
            </Grid.Box>

            <Grid.Box area="right-1" className="list-box">
              {canBulkDeals(alliance) ? (
                <TransparentButtonSvg
                  onClick={this.onBulkDelete}
                  iconSvg={importCSV}
                  text="Bulk Delete"
                />
              ) : null}
            </Grid.Box>
            <Grid.Box area="right-2" className="list-box">
              {canImportCSV(alliance) ? (
                <TransparentButtonSvg
                  onClick={this.goToImportDeals}
                  iconSvg={importCSV}
                  text="Import CSV"
                />
              ) : null}
            </Grid.Box>

            <Grid.Box area="right-3">
              <Switch
                label={'Show All Columns'}
                value={isFullTable}
                onChange={() => {
                  this.setState({
                    isFullTable: !isFullTable,
                  });
                }}
              />
            </Grid.Box>
          </Grid.Layout>
        </Card.Header>
        <DealCardBody
          className="items-table"
          onScroll={(event) => this.onListScroll(event, dealsList, fetchDealsData)}>
          {isFullTable ? (
            <DealFullTable
              dealsList={dealsList}
              loadingPage={loadingPage}
              loading={loading}
              history={history}
              currency={currency}
              alliance={alliance}
              user={this.user}
              onSelectForDelete={this.onSelectForDelete}
              onListScroll={this.onListScroll}
            />
          ) : (
            <DealTable
              dealsList={dealsList}
              loadingPage={loadingPage}
              loading={loading}
              history={history}
              currency={currency}
              alliance={alliance}
              user={this.user}
              onSelectForDelete={this.onSelectForDelete}
              onListScroll={this.onListScroll}
            />
          )}
          {canCreateDeal(this.user, alliance) ? (
            <ActionButtonListView
              onClick={() => this.props.history.push('/management/deal/create')}
              text="Create Deals"
            />
          ) : null}
        </DealCardBody>
        <YesNoDialog
          isOpen={deleteModalIsOpen}
          onYes={this.onYes}
          onNo={this.onClose}
          onClose={this.onClose}
          text={'Are you sure you want to Delete this Deal?'}
          title={'Delete Deal'}
        />
        <YesNoDialog
          isOpen={bulkDeleteModalIsOpen}
          onYes={this.onYesBulkDelete}
          onNo={this.onCloseBulkDelete}
          onClose={this.onCloseBulkDelete}
          text={'Are you sure you want to Delete all Deals?'}
          title={'Bulk delete'}
        />
      </div>
    );
  }
}

DealListView.propTypes = {
  history: PropTypes.object.isRequired,
};

export default withRouter(withAlliance(DealListView));

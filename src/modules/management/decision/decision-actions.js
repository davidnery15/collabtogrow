import {
  DECISION_CREATE_EVENT,
  DECISION_ERROR_EVENT,
  DECISION_UPDATE_EVENT,
  DECISION_DETAIL_EVENT,
  DECISION_COMPLETED_EVENT,
  DECISION_RESTORE_EVENT,
} from './decision-store';
import Flux from '@cobuildlab/flux-state';
import { IntegrityError } from '../../../shared/errors';
import { validateDecisionData } from './decision-validators';
import sessionStore, { APOLLO_CLIENT, NEW_SESSION_EVENT } from '../../../shared/SessionStore';
import {
  DECISION_CREATE_MUTATION,
  DECISION_DETAIL_QUERY,
  DECISION_UPDATE_MUTATION,
  DECISION_COMMENTS_QUERY,
} from './decision-queries';
import { log, error } from '@cobuildlab/pure-logger';
import {
  sanitize8BaseReferencesFromObjects,
  sanitize8BaseReferenceFromObject,
  sanitize8BaseReconnectsFromObjects,
  sanitize8BaseDocumentsCreate,
  sanitize8BaseDocumentsDeleteAndUpdate,
  sanitize8BaseBigInt,
} from '../../../shared/utils';
import { getActiveAllianceId } from '../../../shared/alliance-utils';

import { INITIATIVE_ERROR_EVENT } from '../initiative/initiative-store';
import { DECISION_OPEN, DECISION_COMPLETED } from '../../../shared/status';
import {
  COMMENT_CREATE_EVENT,
  COMMENT_ERROR_EVENT,
  COMMENT_REQUEST_EVENT,
  COMMENTS_EVENT,
} from '../../comment/comment-store';
import { DECISION_TYPE } from '../../../shared/item-types';
import { COMMENTS_CREATE_MUTATION } from '../../comment/comment-queries';
import { canCompletedDecision, canRestoreDecision } from './decision-permissions';
import { RELATED_ITEM_UPDATE_MUTATION } from '../../related-item/related-item-queries';

/**
 * Notifies a Request for Comments for a Decision.
 */
export const openComments = ({ id: decisionId }) => {
  Flux.dispatchEvent(COMMENT_REQUEST_EVENT, { type: DECISION_TYPE, id: decisionId });
};

/**
 * Creates a New Decision.
 *
 * @param {object}decision - Decision.
 * @param {object}relatedItems - RelatedItems.
 * @param {Array}initiatives - Initiatives.
 * @returns {Promise<void>} Return promise.
 */
export const createDecision = async (decision, relatedItems, initiatives) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { user } = sessionStore.getState(NEW_SESSION_EVENT);
  const allianceId = getActiveAllianceId(user);

  delete decision.id;

  if (!allianceId)
    return Flux.dispatchEvent(
      DECISION_ERROR_EVENT,
      new IntegrityError('Must have an Active Alliance'),
    );

  try {
    validateDecisionData(decision, initiatives, true);
  } catch (e) {
    error('createDecision', e);
    return Flux.dispatchEvent(DECISION_ERROR_EVENT, e);
  }

  decision.initiatives = initiatives;
  sanitize8BaseDocumentsCreate(decision, 'documents');
  sanitize8BaseReferencesFromObjects(decision, 'initiatives');
  sanitize8BaseReferenceFromObject(decision, 'source');
  sanitize8BaseBigInt(decision, 'unitMonetizationFactor');
  decision.status = DECISION_OPEN;
  decision.itemDecisionRelation = {
    create: {
      alliance: { connect: { id: allianceId } },
      itemsRelated: {
        connect: relatedItems.map((item) => {
          return { id: item.itemId };
        }),
      },
    },
  };

  let result;
  try {
    result = await client.mutate({
      mutation: DECISION_CREATE_MUTATION,
      variables: { data: decision },
    });
  } catch (e) {
    error('createDecision', e);
    return Flux.dispatchEvent(DECISION_ERROR_EVENT, e);
  }

  return Flux.dispatchEvent(DECISION_CREATE_EVENT, result);
};

/**
 * Fetch the Decision by id of the Current Alliance.
 *
 * @param {string}id - Id.
 * @returns {Promise<void>} Return promise.
 */
export const fetchDecision = async (id) => {
  const client = sessionStore.getState(APOLLO_CLIENT);

  let response;
  try {
    response = await client.query({
      query: DECISION_DETAIL_QUERY,
      fetchPolicy: 'network-only',
      variables: { id },
    });
  } catch (e) {
    error('fetchDecision', e);
    return Flux.dispatchEvent(DECISION_ERROR_EVENT, e);
  }
  log('fetchDecision', response);
  Flux.dispatchEvent(DECISION_DETAIL_EVENT, response.data);
  return response.data;
};

/**
 * Edit a Decision.
 *
 * @param {object}decision - Decision.
 * @param {Array}relatedItems - RelatedItems.
 * @param {object}initiatives - Initiatives.
 * @param {Array}originalDocuments - OriginalDocuments.
 * @returns {Promise<void>} Return promise.
 */
export const updateDecision = async (decision, relatedItems, initiatives, originalDocuments) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const itemId = decision.itemDecisionRelation.id;

  delete decision.__typename;
  delete decision.status;
  delete decision.createdAt;
  delete decision.createdBy;
  delete decision.itemDecisionRelation;

  try {
    validateDecisionData(decision, initiatives, true);
  } catch (e) {
    error('updateDecision', e);
    return Flux.dispatchEvent(DECISION_ERROR_EVENT, e);
  }

  decision.initiatives = initiatives;
  sanitize8BaseReconnectsFromObjects(decision, 'initiatives');
  sanitize8BaseReferenceFromObject(decision, 'source');
  sanitize8BaseDocumentsDeleteAndUpdate(decision, 'documents', originalDocuments);
  sanitize8BaseBigInt(decision, 'unitMonetizationFactor');

  let response;
  try {
    response = await client.mutate({
      mutation: DECISION_UPDATE_MUTATION,
      variables: { data: decision },
    });
  } catch (e) {
    error('updateDecision', e);
    return Flux.dispatchEvent(DECISION_ERROR_EVENT, e);
  }

  // Reconnect related items
  const relatedItemsData = {
    id: itemId,
    itemsRelated: {
      reconnect: relatedItems.map((i) => {
        return { id: i.itemId };
      }),
    },
  };

  log('decisionData:update:relatedItems:', relatedItemsData);
  try {
    await client.mutate({
      mutation: RELATED_ITEM_UPDATE_MUTATION,
      variables: { data: relatedItemsData },
    });
  } catch (e) {
    error('updateDecision:updateRelated Items', e, decision);
    return Flux.dispatchEvent(DECISION_ERROR_EVENT, e);
  }

  Flux.dispatchEvent(DECISION_UPDATE_EVENT, response);
  return response.data;
};

/**
 * Fetches the Decision Comments.
 *
 * @param {string}decisionId - Id.
 * @returns {Promise<void>} Return promise.
 */
export const fetchDecisionComments = async (decisionId) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const data = { id: decisionId };
  const { user } = sessionStore.getState(NEW_SESSION_EVENT);

  let response;
  try {
    response = await client.query({
      query: DECISION_COMMENTS_QUERY,
      variables: data,
      fetchPolicy: 'network-only',
    });
  } catch (e) {
    error('fetchDecisionComments', e);
    return Flux.dispatchEvent(COMMENT_ERROR_EVENT, e);
  }
  log('fetchDecisionComments', response);
  response.data.decision.comments.userId = user.id;

  Flux.dispatchEvent(COMMENTS_EVENT, response.data.decision.comments);
  return response.data.decision.comments;
};

/**
 * Create a comment on a Decision.
 *
 * @param {string}decisionId - Id.
 * @param {Array}comment - Comment.
 * @returns {Promise<*>} Return promise.
 */
export const createDecisionComment = async (decisionId, comment) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const data = {
    comment,
    decisionCommentsRelation: { connect: { id: decisionId } },
  };

  let response;
  try {
    response = await client.mutate({
      mutation: COMMENTS_CREATE_MUTATION,
      variables: { data },
    });
  } catch (e) {
    error('createDecisionComment', e);
    return Flux.dispatchEvent(COMMENT_ERROR_EVENT, e);
  }
  log('createDecisionComment', response);
  Flux.dispatchEvent(COMMENT_CREATE_EVENT, response.data);
  return response.data;
};

/**
 * Change status item the completed to open.
 *
 * @param {object}decisionData - Decision.
 * @returns {Promise} Return promise.
 */
export const completedDecision = async (decisionData) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { selectedAlliance, user } = sessionStore.getState(NEW_SESSION_EVENT);

  if (!canCompletedDecision(user, decisionData, selectedAlliance))
    return Flux.dispatchEvent(
      INITIATIVE_ERROR_EVENT,
      new IntegrityError('You do not have permission to perform this action'),
    );

  let response;
  try {
    response = await client.mutate({
      mutation: DECISION_UPDATE_MUTATION,
      variables: {
        data: {
          id: decisionData.id,
          status: DECISION_COMPLETED,
        },
      },
    });
  } catch (e) {
    error('completedDecision', e);
    return Flux.dispatchEvent(DECISION_ERROR_EVENT, e);
  }

  Flux.dispatchEvent(DECISION_COMPLETED_EVENT, response);
  return response.data;
};

/**
 * Restore status the completed to open.
 *
 * @param {object}decisionData - Decision.
 * @returns {Promise<void|*>} Return Promise.
 */
export const restoreDecision = async (decisionData) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { selectedAlliance, user } = sessionStore.getState(NEW_SESSION_EVENT);

  if (!canRestoreDecision(user, decisionData, selectedAlliance))
    return Flux.dispatchEvent(
      INITIATIVE_ERROR_EVENT,
      new IntegrityError('You do not have permission to perform this action'),
    );

  let response;
  try {
    response = await client.mutate({
      mutation: DECISION_UPDATE_MUTATION,
      variables: {
        data: {
          id: decisionData.id,
          status: DECISION_OPEN,
        },
      },
    });
  } catch (e) {
    error('restoredDecision', e);
    return Flux.dispatchEvent(DECISION_ERROR_EVENT, e);
  }

  Flux.dispatchEvent(DECISION_RESTORE_EVENT, response);
  return response.data;
};

import React from 'react';
import PropTypes from 'prop-types';
import { Loader, Card, Heading, Grid } from '@8base/boost';
import { CreateViewCardBody } from '../../../components/new-ui/card/CreateViewCardBody';
import DecisionForm from './components/DecisionForm';
import { onErrorMixin, onChangeDataMixin } from '../../../shared/mixins';
import * as R from 'ramda';
import DecisionModel, { DECISION_DOCUMENTS } from './decision-model';
import { withRouter } from 'react-router-dom';
import decisionStore, { DECISION_CREATE_EVENT, DECISION_ERROR_EVENT } from './decision-store';
import allianceStore, {
  ALLIANCE_LIST_MEMBERS_EVENT,
} from '../../settings/alliance-management/alliance-store';
import { fetchCurrentAllianceMembersAction } from '../../settings/alliance-management/alliance-actions';
import * as toast from 'components/toast/Toast';
import View from '@cobuildlab/react-flux-state';
import { createDecision } from './decision-actions';
import { fetchInitiativeList } from '../initiative/initiative-actions';
import initiativeStore, { INITIATIVE_LIST_EVENT } from '../initiative/initiative-store';
import SubHeader from '../../../components/SubHeader';
import DecisionDetailTable from './components/DecisionDetailTable';
import { getCurrencyOnSession } from '../../../shared/alliance-utils';
import { ActionButton } from '../../../components/buttons/ActionButton';
import { FormSteps } from '../../../components/dots/FormSteps';
import { ActionButtonClose } from '../../../components/buttons/ActionButtonClose';
import { TransparentButton } from '../../../components/buttons/TransparentButton';
import { decisionValidator } from './decision-validators';
import { saveFormToSessionStorage } from 'shared/utils';
import RelatedItemForm from '../../related-item/components/RelatedItemForm';
import { initiativesItemValidator } from '../initiative/initiative-validators';
import { RelatedItemsDetailTable } from '../../related-item/components/RelatedItemsDetailTable';
import { InitiativeListTable } from '../initiative/components/InitiativeListTable';
import { LeftProgressSection } from '../../../components/new-ui/LeftProgressSection';
import { SCREENS_DECISION } from '../screenView';
import { BoxCard } from '../../../components/new-ui/div/BoxCard';
import { CardFooter } from '../../../components/new-ui/card/CardFooter';

export const DECISION_DATA_STORE = 'decisionCreateView';

/**
 * Create Decision
 */
class DecisionCreateView extends View {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        decisionData: R.clone(DecisionModel),
        initiatives: [],
        relatedItems: [],
      },
      initiativesList: [],
      clientCompany: null,
      partnerCompany: null,
      step: 0,
      loading: true,
    };
    this.onError = onErrorMixin.bind(this);
    this.onChangeData = onChangeDataMixin.bind(this);
  }

  onChangeDecisionData = (name, value) => {
    const { data } = this.state;
    data.decisionData[name] = value;
    this.setState({ data });
    const model = R.clone(DecisionModel);
    saveFormToSessionStorage(DECISION_DATA_STORE, data.decisionData, model, ['documents']);
  };

  componentDidMount = () => {
    const decisionData = JSON.parse(sessionStorage.getItem(DECISION_DATA_STORE));

    this.subscribe(decisionStore, DECISION_ERROR_EVENT, this.onError);
    this.subscribe(allianceStore, ALLIANCE_LIST_MEMBERS_EVENT, (state) => {
      this.setState({
        clientCompany: state.clientCompany,
        partnerCompany: state.partnerCompany,
      });
    });
    this.subscribe(decisionStore, DECISION_CREATE_EVENT, (state) => {
      sessionStorage.removeItem(DECISION_DATA_STORE);
      toast.success('Decision Successfully Created');
      this.props.history.push(`/management/amo-item`);
    });
    this.subscribe(initiativeStore, INITIATIVE_LIST_EVENT, (state) => {
      this.setState({
        loading: false,
        initiativesList: state.initiativesList.items,
      });
    });

    // set decisionData from sessionStorage
    if (decisionData) {
      const { data } = this.state;
      data.decisionData = decisionData;
      this.setState({ data });
    }

    fetchInitiativeList('', 1, 1000);
    fetchCurrentAllianceMembersAction();
  };

  componentWillUnmount() {
    super.componentWillUnmount();
    localStorage.removeItem(DECISION_DOCUMENTS);
  }

  onSubmit = () => {
    this.setState({ loading: true }, () => {
      const decisionData = R.clone(this.state.data.decisionData);
      const relatedItems = R.clone(this.state.data.relatedItems);
      const initiatives = R.clone(this.state.data.initiatives);
      createDecision(decisionData, relatedItems, initiatives);
    });
  };

  onDecisionStepChange = (nextStep) => {
    const decisionData = R.clone(this.state.data.decisionData);
    try {
      decisionValidator(decisionData);
    } catch (e) {
      return this.onError(e);
    }
    this.onScreen(nextStep);
  };

  onRelatedItemsStepChange = (step) => {
    const initiatives = R.clone(this.state.data.initiatives);
    try {
      initiativesItemValidator(initiatives);
    } catch (e) {
      return this.onError(e);
    }
    this.onScreen(step);
  };

  onScreen = (step) => {
    this.setState({ step });
  };

  render() {
    const { data, initiativesList, step, loading, clientCompany, partnerCompany } = this.state;
    const { decisionData, relatedItems, initiatives } = data;
    const { history } = this.props;

    const currency = getCurrencyOnSession();
    let content = <Loader stretch />;
    let footer = <></>;

    if (!loading && step === 0) {
      content = (
        <DecisionForm
          data={decisionData}
          onChange={this.onChangeDecisionData}
          clientCompany={clientCompany}
          partnerCompany={partnerCompany}
          currency={currency}
        />
      );
      footer = (
        <CardFooter>
          <ActionButton onClick={() => this.onDecisionStepChange(1)} text={'Next'} />
        </CardFooter>
      );
    }

    if (!loading && step === 1) {
      content = (
        <RelatedItemForm
          selectedInitiatives={initiatives}
          relatedItems={relatedItems}
          initiatives={initiativesList}
          onChange={(key, value) => {
            this.onChangeData(key, value);
          }}
        />
      );

      footer = (
        <CardFooter>
          <ActionButton onClick={() => this.onRelatedItemsStepChange(2)} text={'Next'} />
          <TransparentButton onClick={() => this.onScreen(0)} text={'Previous'} />
        </CardFooter>
      );
    }

    if (!loading && step === 2) {
      const decisionDetail = R.clone(decisionData);

      decisionDetail.initiatives = { items: initiatives };
      decisionDetail.documents = { items: decisionDetail.documents };

      content = (
        <>
          <DecisionDetailTable data={decisionDetail} currency={currency} />
          <SubHeader text="Business Case" status={decisionDetail.status} />
          <InitiativeListTable initiatives={initiatives} />
          <RelatedItemsDetailTable relatedItems={relatedItems} />
        </>
      );
      footer = (
        <CardFooter>
          <ActionButton onClick={() => this.onSubmit()} text="Create Decision" />
          <TransparentButton onClick={() => this.onScreen(1)} text="Previous" />
        </CardFooter>
      );
    }

    return (
      <React.Fragment>
        <Card.Header>
          <Grid.Layout
            columns="200px auto 200px"
            areas={[['left', 'center', 'right']]}
            style={{ width: '100%' }}>
            <Grid.Box area="left">
              <Heading type="h4" text="Create Decision" />
            </Grid.Box>
            <Grid.Box area="center">
              <FormSteps totalSteps={3} step={step} />
            </Grid.Box>
          </Grid.Layout>
          <ActionButtonClose onClick={history.goBack} />
        </Card.Header>
        <CreateViewCardBody>
          <Grid.Layout columns="30% 70%" areas={[['left', 'right']]} style={{ width: '100%' }}>
            <Grid.Box area="left">
              <LeftProgressSection sections={SCREENS_DECISION} currentScreen={step} />
            </Grid.Box>
            <BoxCard>
              <Grid.Box area="right">{content}</Grid.Box>
            </BoxCard>
          </Grid.Layout>
        </CreateViewCardBody>
        {footer}
      </React.Fragment>
    );
  }
}

DecisionCreateView.propTypes = {
  history: PropTypes.object.isRequired,
};

export default withRouter(DecisionCreateView);

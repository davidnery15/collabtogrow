import React from 'react';
import PropTypes from 'prop-types';
import { Loader, Card, Heading, Grid } from '@8base/boost';
import { CreateViewCardBody } from '../../../components/new-ui/card/CreateViewCardBody';
import DecisionForm from './components/DecisionForm';
import { onErrorMixin, onChangeDataMixin } from '../../../shared/mixins';
import * as R from 'ramda';
import DecisionModel, { DECISION_DOCUMENTS } from './decision-model';
import { withRouter } from 'react-router-dom';
import decisionStore, {
  DECISION_ERROR_EVENT,
  DECISION_DETAIL_EVENT,
  DECISION_UPDATE_EVENT,
} from './decision-store';
import allianceStore, {
  ALLIANCE_LIST_MEMBERS_EVENT,
} from '../../settings/alliance-management/alliance-store';
import { fetchCurrentAllianceMembersAction } from '../../settings/alliance-management/alliance-actions';
import * as toast from 'components/toast/Toast';
import View from '@cobuildlab/react-flux-state';
import { fetchDecision, openComments, updateDecision } from './decision-actions';
import initiativeStore, { INITIATIVE_LIST_EVENT } from '../initiative/initiative-store';
import { fetchInitiativeList } from '../initiative/initiative-actions';
import DecisionDetailTable from './components/DecisionDetailTable';
import { getCurrencyOnSession } from '../../../shared/alliance-utils';
import { ActionButton } from '../../../components/buttons/ActionButton';
import { FormSteps } from '../../../components/dots/FormSteps';
import { TransparentButton } from '../../../components/buttons/TransparentButton';
import { decisionValidator } from './decision-validators';
import { businessCaseValidator } from '../../document-management/business-case/business-case-validators';
import { TopButtons } from '../../../components/buttons/TopButtons';
import RelatedItemForm from '../../related-item/components/RelatedItemForm';
import { initiativesItemValidator } from '../initiative/initiative-validators';
import { RelatedItemsDetailTable } from '../../related-item/components/RelatedItemsDetailTable';
import relatedItemStore, {
  RELATED_ITEMS_EVENT,
  RELATED_ITEM_ERROR_EVENT,
} from '../../related-item/related-item-store';
import { fetchRelatedItems } from '../../related-item/related-item-actions';
import { getItemByType } from '../../../shared/items-util';
import { InitiativeListTable } from '../initiative/components/InitiativeListTable';
import { BUSINESS_CASE_DOCUMENT } from '../../document-management/business-case/BusinessCase.model';
import { LeftProgressSection } from '../../../components/new-ui/LeftProgressSection';
import { SCREENS_DECISION } from '../screenView';
import { BoxCard } from '../../../components/new-ui/div/BoxCard';
import { CardFooter } from '../../../components/new-ui/card/CardFooter';
import { DECISION_COMPLETED } from '../../../shared/status';

// Edit Decision
class DecisionEditView extends View {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        decisionData: R.clone(DecisionModel),
        initiatives: [],
        relatedItems: [],
      },
      initiativesList: [],
      clientCompany: null,
      partnerCompany: null,
      step: 0,
      loading: true,
    };
    this.onError = onErrorMixin.bind(this);
    this.onChangeData = onChangeDataMixin.bind(this);
    this.originalRecommendedSolutions = [];
    this.originalDocuments = [];
  }

  onChangeDecisionData = (name, value) => {
    const { data } = this.state;
    data.decisionData[name] = value;
    this.setState({ data });
  };

  componentDidMount = () => {
    const { match } = this.props;
    if (!match.params.id) return toast.error('Decision ID missing');

    this.subscribe(decisionStore, DECISION_ERROR_EVENT, this.onError);
    this.subscribe(relatedItemStore, RELATED_ITEM_ERROR_EVENT, this.onError);

    this.subscribe(allianceStore, ALLIANCE_LIST_MEMBERS_EVENT, (state) => {
      this.setState({
        clientCompany: state.clientCompany,
        partnerCompany: state.partnerCompany,
      });
    });
    this.subscribe(decisionStore, DECISION_DETAIL_EVENT, (state) => {
      const { decision: decisionData } = state;
      const initiatives = decisionData.initiatives.items;
      const { data } = this.state;

      decisionData.documents = decisionData.documents.items;
      localStorage.setItem(DECISION_DOCUMENTS, JSON.stringify(decisionData.documents));
      this.originalDocuments = decisionData.documents.concat();

      data.decisionData = decisionData;
      data.initiatives = initiatives;

      this.setState({ data }, () => fetchRelatedItems(decisionData.itemDecisionRelation.id));
    });

    this.subscribe(relatedItemStore, RELATED_ITEMS_EVENT, (state) => {
      const items = state.item.itemsRelated.items.map((item) => getItemByType(item));
      const { data } = this.state;

      data.relatedItems = items;
      this.setState({ data, loading: false });
    });

    this.subscribe(initiativeStore, INITIATIVE_LIST_EVENT, (state) => {
      this.setState({
        initiativesList: state.initiativesList.items,
      });
    });
    this.subscribe(decisionStore, DECISION_UPDATE_EVENT, (state) => {
      toast.success(`Decision Successfully Updated`);
      this.props.history.goBack();
    });

    fetchInitiativeList('', 1, 1000);
    fetchCurrentAllianceMembersAction();
    fetchDecision(match.params.id);
  };

  componentWillUnmount() {
    super.componentWillUnmount();
    localStorage.removeItem(DECISION_DOCUMENTS);
    localStorage.removeItem(BUSINESS_CASE_DOCUMENT);
  }

  onSubmit = () => {
    this.setState({ loading: true }, () => {
      const decisionData = R.clone(this.state.data.decisionData);
      const relatedItems = R.clone(this.state.data.relatedItems);
      const initiatives = R.clone(this.state.data.initiatives);
      updateDecision(
        decisionData,
        relatedItems,
        initiatives,
        this.originalRecommendedSolutions,
        this.originalDocuments,
      );
    });
  };

  onDecisionStepChange = (nextStep) => {
    const decisionData = R.clone(this.state.data.decisionData);
    try {
      decisionValidator(decisionData);
    } catch (e) {
      return this.onError(e);
    }
    this.onScreen(nextStep);
  };

  onBusinessCaseStepChange = (nextStep) => {
    const businessCaseData = R.clone(this.state.data.businessCaseData);
    try {
      businessCaseValidator(businessCaseData);
    } catch (e) {
      return this.onError(e);
    }
    this.onScreen(nextStep);
  };

  onRelatedItemsStepChange = (step) => {
    const initiatives = R.clone(this.state.data.initiatives);
    try {
      initiativesItemValidator(initiatives);
    } catch (e) {
      return this.onError(e);
    }
    this.onScreen(step);
  };

  onScreen = (step) => {
    this.setState({ step });
  };

  render() {
    const { data, initiativesList, loading, step, clientCompany, partnerCompany } = this.state;
    const { decisionData, relatedItems, initiatives } = data;
    const { history } = this.props;

    const currency = getCurrencyOnSession();
    let content = <Loader stretch />;
    let footer = <></>;
    let buttonsTop = <></>;
    if (decisionData.status === DECISION_COMPLETED) history.push(`/management/amo-item`);

    if (!loading && step === 0) {
      content = (
        <DecisionForm
          data={decisionData}
          onChange={this.onChangeDecisionData}
          clientCompany={clientCompany}
          partnerCompany={partnerCompany}
          currency={currency}
        />
      );
      footer = (
        <CardFooter>
          <ActionButton onClick={() => this.onDecisionStepChange(1)} text={'Next'} />
        </CardFooter>
      );
    }

    if (!loading && step === 1) {
      const itemData = { id: decisionData.id, type: decisionData.__typename };

      content = (
        <RelatedItemForm
          selectedInitiatives={initiatives}
          relatedItems={relatedItems}
          initiatives={initiativesList}
          onChange={(key, value) => {
            this.onChangeData(key, value);
          }}
          itemData={itemData}
        />
      );

      footer = (
        <CardFooter>
          <ActionButton onClick={() => this.onRelatedItemsStepChange(2)} text={'Next'} />
          <TransparentButton onClick={() => this.onScreen(0)} text={'Previous'} />
        </CardFooter>
      );
    }

    if (!loading && step === 2) {
      const decisionDetail = R.clone(decisionData);
      decisionDetail.initiatives = { items: initiatives };
      decisionDetail.documents = { items: decisionDetail.documents };

      content = (
        <>
          <DecisionDetailTable data={decisionDetail} currency={currency} />
          <InitiativeListTable initiatives={initiatives} />
          <RelatedItemsDetailTable relatedItems={relatedItems} />
        </>
      );
      footer = (
        <CardFooter>
          <ActionButton onClick={() => this.onSubmit()} text={'Update Decision'} />
          <TransparentButton onClick={() => this.onScreen(1)} text={'Previous'} />
        </CardFooter>
      );
    }

    if (!loading)
      buttonsTop = (
        <TopButtons
          onClickClosed={history.goBack}
          onClickCollaborated={() => openComments(decisionData)}
        />
      );

    return (
      <React.Fragment>
        <Card.Header>
          <Grid.Layout
            columns="300px auto 350px"
            areas={[['left', 'center', 'right']]}
            style={{ width: '100%' }}>
            <Grid.Box area="left">
              <Heading type="h4" text="Edit Decision" />
            </Grid.Box>
            <Grid.Box area="center">
              <FormSteps totalSteps={3} step={step} />
            </Grid.Box>
            <Grid.Box area="right" />
          </Grid.Layout>
          {buttonsTop}
        </Card.Header>
        <CreateViewCardBody>
          <Grid.Layout columns="30% 70%" areas={[['left', 'right']]} style={{ width: '100%' }}>
            <Grid.Box area="left">
              <LeftProgressSection sections={SCREENS_DECISION} currentScreen={step} />
            </Grid.Box>
            <BoxCard>
              <Grid.Box area="right">{content}</Grid.Box>
            </BoxCard>
          </Grid.Layout>
        </CreateViewCardBody>
        {footer}
      </React.Fragment>
    );
  }
}

DecisionEditView.propTypes = {
  history: PropTypes.object.isRequired,
};

export default withRouter(DecisionEditView);

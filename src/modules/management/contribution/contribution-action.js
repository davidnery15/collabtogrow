import sessionStore, { APOLLO_CLIENT, NEW_SESSION_EVENT } from '../../../shared/SessionStore';
import {
  sanitize8BaseDocumentsCreate,
  sanitize8BaseDocumentsDeleteAndUpdate,
  sanitize8BaseReferencesFromObjects,
  sanitize8BaseReferenceFromObject,
  sanitize8BaseReconnectsFromObjects,
  sanitize8BaseBigInt,
} from '../../../shared/utils';
import Flux from '@cobuildlab/flux-state';
import { IntegrityError } from '../../../shared/errors';
import { validateContributionData } from './contribution-validators';
import { error, log } from '@cobuildlab/pure-logger';
import {
  CONTRIBUTION_CREATE_EVENT,
  CONTRIBUTION_ERROR_EVENT,
  CONTRIBUTION_DETAIL_EVENT,
  CONTRIBUTION_UPDATE_EVENT,
  CONTRIBUTION_COMPLETED_EVENT,
  CONTRIBUTION_RESTORE_EVENT,
} from './contribution-store';
import {
  CONTRIBUTION_COMMENTS_QUERY,
  CONTRIBUTION_CREATE_MUTATION,
  CONTRIBUTION_DETAIL_QUERY,
  CONTRIBUTION_UPDATE_QUERY,
  CONTRIBUTION_CLOSE_MUTATION,
} from './contribution-queries';
import { CONTRIBUTION_COMPLETED, CONTRIBUTION_OPEN } from '../../../shared/status';
import {
  COMMENT_CREATE_EVENT,
  COMMENT_ERROR_EVENT,
  COMMENT_REQUEST_EVENT,
  COMMENTS_EVENT,
} from '../../comment/comment-store';
import { CONTRIBUTION_TYPE } from '../../../shared/item-types';
import { COMMENTS_CREATE_MUTATION } from '../../comment/comment-queries';
import { canCompletedContribution, canRestoreContribution } from './contribution-permissions';
import { RELATED_ITEM_UPDATE_MUTATION } from '../../related-item/related-item-queries';
import { FUNDING_REQUEST_ERROR_EVENT } from '../funding-request/funding-request-store';

/**
 * Notifies a Request for Comments for a Contribution.
 */
export const openComments = ({ id }) => {
  Flux.dispatchEvent(COMMENT_REQUEST_EVENT, { type: CONTRIBUTION_TYPE, id: id });
};

/**
 * Create a Contribution.
 *
 * @param contribution
 * @param relatedItems
 * @param initiatives
 */
export const createContribution = async (contribution, relatedItems, initiatives) => {
  log('createContribution', contribution, relatedItems, initiatives);
  const client = sessionStore.getState(APOLLO_CLIENT);

  delete contribution.id;

  const {
    selectedAlliance: { id: allianceId },
  } = sessionStore.getState(NEW_SESSION_EVENT);

  if (!allianceId)
    return Flux.dispatchEvent(
      CONTRIBUTION_ERROR_EVENT,
      new IntegrityError('Must have an Active Alliance'),
    );

  try {
    validateContributionData(contribution, initiatives);
  } catch (err) {
    error('createContribution', err);
    return Flux.dispatchEvent(CONTRIBUTION_ERROR_EVENT, err);
  }

  contribution.initiatives = initiatives;
  // Transforming into 8base 'connect' relationships
  sanitize8BaseDocumentsCreate(contribution, 'documents');
  sanitize8BaseReferencesFromObjects(contribution, 'initiatives');
  sanitize8BaseReferenceFromObject(contribution, 'source');
  sanitize8BaseBigInt(contribution, 'unitMonetizationFactor');

  contribution.status = CONTRIBUTION_OPEN;
  contribution.itemContributionRelation = {
    create: {
      alliance: { connect: { id: allianceId } },
      itemsRelated: {
        connect: relatedItems.map((item) => {
          return { id: item.itemId };
        }),
      },
    },
  };

  let result;
  log('createContributionWithOutBusinessCase', contribution);
  try {
    result = await client.mutate({
      mutation: CONTRIBUTION_CREATE_MUTATION,
      variables: { data: contribution },
    });
  } catch (e) {
    error('createContribution', e);
    return Flux.dispatchEvent(CONTRIBUTION_ERROR_EVENT, e);
  }

  log('createContribution', result);
  return Flux.dispatchEvent(CONTRIBUTION_CREATE_EVENT, result);
};

/**
 * Create a comment on a Contribution.
 *
 * @param contributionId
 * @param comment
 * @returns {Promise<*>}
 */
export const createContributionComment = async (contributionId, comment) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const data = {
    comment,
    contributionCommentsRelation: { connect: { id: contributionId } },
  };

  let response;
  try {
    response = await client.mutate({
      mutation: COMMENTS_CREATE_MUTATION,
      variables: { data },
    });
  } catch (e) {
    error('createContributionComment', e);
    return Flux.dispatchEvent(COMMENT_ERROR_EVENT, e);
  }
  log('createContributionComment', response);
  Flux.dispatchEvent(COMMENT_CREATE_EVENT, response.data);
  return response.data;
};

/**
 * Fetches the Contribution Item Comments.
 *
 * @returns {Promise<void>}
 * @param contributionId
 */
export const fetchContributionComments = async (contributionId) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const data = { id: contributionId };
  const { user } = sessionStore.getState(NEW_SESSION_EVENT);

  let response;
  try {
    response = await client.query({
      query: CONTRIBUTION_COMMENTS_QUERY,
      variables: data,
      fetchPolicy: 'network-only',
    });
  } catch (e) {
    error('fetchContributionComments', e);
    return Flux.dispatchEvent(COMMENT_ERROR_EVENT, e);
  }
  log('fetchContributionComments', response);
  response.data.contribution.comments.userId = user.id;

  Flux.dispatchEvent(COMMENTS_EVENT, response.data.contribution.comments);
  return response.data.contribution.comments;
};

/**
 * Fetches the Contribution Item.
 *
 * @returns {Promise<void>}
 * @param contributionId
 */
export const fetchContribution = async (contributionId) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const data = { id: contributionId };

  let response;
  try {
    response = await client.query({
      query: CONTRIBUTION_DETAIL_QUERY,
      variables: data,
      fetchPolicy: 'network-only',
    });
  } catch (e) {
    error('fetchContribution', e);
    return Flux.dispatchEvent(CONTRIBUTION_ERROR_EVENT, e);
  }
  log('fetchContribution', response);
  Flux.dispatchEvent(CONTRIBUTION_DETAIL_EVENT, response.data);
  return response.data;
};

/**
 * Update a Contribution.
 *
 * @param contributionData
 * @param relatedItems
 * @param initiatives
 * @param originalDocuments
 */
export const updateContribution = async (
  contributionData,
  relatedItems,
  initiatives,
  originalDocuments,
) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const itemId = contributionData.itemId;
  console.log(
    'updateContribution',
    contributionData,
    initiatives,

    relatedItems,
  );

  delete contributionData.itemId;
  delete contributionData.owner;
  delete contributionData.itemContributionRelation;
  delete contributionData.contributionApprovalContributionRelation;
  delete contributionData.__typename;

  try {
    validateContributionData(contributionData, initiatives);
  } catch (e) {
    error('updateContributionWithOutBusinessCase Validator', e);
    return Flux.dispatchEvent(CONTRIBUTION_ERROR_EVENT, e);
  }
  contributionData.initiatives = initiatives;

  sanitize8BaseReconnectsFromObjects(contributionData, 'initiatives');
  sanitize8BaseReferenceFromObject(contributionData, 'source');
  sanitize8BaseDocumentsDeleteAndUpdate(contributionData, 'documents', originalDocuments);
  sanitize8BaseBigInt(contributionData, 'unitMonetizationFactor');

  let response;
  try {
    response = await client.mutate({
      mutation: CONTRIBUTION_UPDATE_QUERY,
      variables: { data: contributionData },
    });
  } catch (e) {
    error('updateContributionWithOutBusinessCase', e, contributionData);
    return Flux.dispatchEvent(CONTRIBUTION_ERROR_EVENT, e);
  }

  const data = {
    id: itemId,
    itemsRelated: {
      reconnect: relatedItems.map((i) => {
        return { id: i.itemId };
      }),
    },
  };
  console.log('fundingRequestData:update:relatedItems:', data);
  try {
    await client.mutate({
      mutation: RELATED_ITEM_UPDATE_MUTATION,
      variables: { data },
    });
  } catch (e) {
    console.error('updateFundingRequest:updateRelated Items', e, contributionData);
    return Flux.dispatchEvent(FUNDING_REQUEST_ERROR_EVENT, e);
  }

  Flux.dispatchEvent(CONTRIBUTION_UPDATE_EVENT, response.data);

  return response.data;
};

/**
 * Close a Contribution.
 *
 * @param contributionData
 */
export const completedContribution = async (contributionData) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { selectedAlliance, user } = sessionStore.getState(NEW_SESSION_EVENT);

  if (!canCompletedContribution(user, contributionData, selectedAlliance))
    return Flux.dispatchEvent(
      CONTRIBUTION_ERROR_EVENT,
      new IntegrityError('You do not have permission to perform this action'),
    );

  let response;
  try {
    response = await client.mutate({
      mutation: CONTRIBUTION_CLOSE_MUTATION,
      variables: {
        data: {
          id: contributionData.id,
          status: CONTRIBUTION_COMPLETED,
        },
      },
    });
  } catch (e) {
    console.error('updateContribution', e, contributionData);
    return Flux.dispatchEvent(CONTRIBUTION_ERROR_EVENT, e);
  }

  Flux.dispatchEvent(CONTRIBUTION_COMPLETED_EVENT, response.data);
  return response.data;
};

/**
 * Restore contribution.
 *
 * @param {object}contributionData - Contribution.
 * @returns {Promise<void|*>} Return promise.
 */
export const restoreContribution = async (contributionData) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { selectedAlliance, user } = sessionStore.getState(NEW_SESSION_EVENT);

  if (!canRestoreContribution(user, contributionData, selectedAlliance))
    return Flux.dispatchEvent(
      CONTRIBUTION_ERROR_EVENT,
      new IntegrityError('You do not have permission to perform this action'),
    );

  let response;
  try {
    response = await client.mutate({
      mutation: CONTRIBUTION_CLOSE_MUTATION,
      variables: {
        data: {
          id: contributionData.id,
          status: CONTRIBUTION_OPEN,
        },
      },
    });
  } catch (e) {
    console.error('updateContribution', e, contributionData);
    return Flux.dispatchEvent(CONTRIBUTION_ERROR_EVENT, e);
  }

  Flux.dispatchEvent(CONTRIBUTION_RESTORE_EVENT, response.data);
  return response.data;
};

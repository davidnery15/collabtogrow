import React from 'react';
import { Card, Heading, Grid } from '@8base/boost';
import { CreateViewCardBody } from '../../../components/new-ui/card/CreateViewCardBody';
import ContributionForm from './components/ContributionForm';
import contributionStore, {
  CONTRIBUTION_CREATE_EVENT,
  CONTRIBUTION_ERROR_EVENT,
} from './contribution-store';
import * as toast from 'components/toast/Toast';
import { Loader } from '@8base/boost';
import { withRouter } from 'react-router-dom';
import { PropTypes } from 'prop-types';
import { ContributionModel, CONTRIBUTION_DOCUMENTS } from './contribution-model';
import { onChangeDataMixin, onErrorMixin } from '../../../shared/mixins';
import View from '@cobuildlab/react-flux-state';
import * as R from 'ramda';
import sessionStore, { NEW_SESSION_EVENT } from '../../../shared/SessionStore';
import { createContribution } from './contribution-action';
import { fetchInitiativeList } from '../initiative/initiative-actions';
import initiativeStore, { INITIATIVE_LIST_EVENT } from '../initiative/initiative-store';
import allianceStore, {
  ALLIANCE_LIST_MEMBERS_EVENT,
} from '../../settings/alliance-management/alliance-store';
import { fetchCurrentAllianceMembersAction } from '../../settings/alliance-management/alliance-actions';
import { getCurrencyOnSession } from '../../../shared/alliance-utils';
import { ActionButton } from '../../../components/buttons/ActionButton';
import { ActionButtonClose } from '../../../components/buttons/ActionButtonClose';
import { saveFormToSessionStorage } from 'shared/utils';
import { contributionValidator } from './contribution-validators';
import RelatedItemForm from '../../related-item/components/RelatedItemForm';
import { TransparentButton } from '../../../components/buttons/TransparentButton';
import ContributionDetailTable from './components/ContributionDetailTable';
import { RelatedItemsDetailTable } from '../../related-item/components/RelatedItemsDetailTable';
import { FormSteps } from '../../../components/dots/FormSteps';
import { initiativesItemValidator } from '../initiative/initiative-validators';
import { LeftProgressSection } from '../../../components/new-ui/LeftProgressSection';
import { SCREENS_CONTRIBUTION } from '../screenView';
import { BoxCard } from '../../../components/new-ui/div/BoxCard';
import { CardFooter } from '../../../components/new-ui/card/CardFooter';
export const CONTRIBUTION_DATA_STORE = 'contributionCreateView';

/**
 * Create Contribution
 */
class ContributionCreateView extends View {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        contributionData: R.clone(ContributionModel),
        relatedItems: [],
        initiatives: [],
      },
      loading: true,
      initiativesList: [],
      clientCompany: null,
      partnerCompany: null,
      step: 0,
    };
    this.onError = onErrorMixin.bind(this);
    this.onChangeData = onChangeDataMixin.bind(this);
  }

  onChangeContributionData = (name, value) => {
    const { data } = this.state;
    data.contributionData[name] = value;
    this.setState({ data });
    const model = R.clone(ContributionModel);
    saveFormToSessionStorage(CONTRIBUTION_DATA_STORE, data.contributionData, model, ['documents']);
  };

  componentDidMount() {
    const contributionData = JSON.parse(sessionStorage.getItem(CONTRIBUTION_DATA_STORE));
    this.subscribe(contributionStore, CONTRIBUTION_ERROR_EVENT, this.onError);
    this.subscribe(allianceStore, ALLIANCE_LIST_MEMBERS_EVENT, (state) => {
      this.setState({
        clientCompany: state.clientCompany,
        partnerCompany: state.partnerCompany,
      });
    });
    this.subscribe(contributionStore, CONTRIBUTION_CREATE_EVENT, (state) => {
      sessionStorage.removeItem(CONTRIBUTION_DATA_STORE);
      toast.success('Contribution Successfully Created');
      this.props.history.push(`/management/investment-item`);
    });
    this.subscribe(initiativeStore, INITIATIVE_LIST_EVENT, (state) => {
      const initiativesList = state.initiativesList.items;
      this.setState({
        loading: false,
        initiativesList,
      });
    });

    // set contributionData from sessionStorage
    if (contributionData) {
      const { data } = this.state;
      data.contributionData = contributionData;
      this.setState({ data });
    }

    fetchInitiativeList('', 1, 1000);
    fetchCurrentAllianceMembersAction();
  }

  componentWillUnmount() {
    super.componentWillUnmount();
    localStorage.removeItem(CONTRIBUTION_DOCUMENTS);
  }

  onContributionStepChange = (nextStep) => {
    const contributionData = R.clone(this.state.data.contributionData);
    try {
      contributionValidator(contributionData);
    } catch (e) {
      return this.onError(e);
    }
    this.onScreen(nextStep);
  };

  onScreen = (step) => {
    this.setState({ step });
  };

  onRelatedItemsStepChange = (step) => {
    const initiatives = R.clone(this.state.data.initiatives);
    try {
      initiativesItemValidator(initiatives);
    } catch (e) {
      return this.onError(e);
    }
    this.onScreen(step);
  };

  onSubmit = () => {
    this.setState({ loading: true }, () => {
      const contributionData = R.clone(this.state.data.contributionData);
      const initiatives = R.clone(this.state.data.initiatives);
      const relatedItems = R.clone(this.state.data.relatedItems);
      createContribution(contributionData, relatedItems, initiatives);
    });
  };

  render() {
    const { initiativesList, loading, clientCompany, partnerCompany, step } = this.state;
    const { user } = sessionStore.getState(NEW_SESSION_EVENT);
    const { contributionData, relatedItems, initiatives } = this.state.data;
    const { history } = this.props;
    const companyId = user.companyUserRelation.items[0].company.id;
    const currency = getCurrencyOnSession();
    let content = <Loader stretch />;
    let footer = <></>;

    if (!loading && step === 0) {
      content = (
        <ContributionForm
          data={contributionData}
          onChange={this.onChangeContributionData}
          myCompanyId={companyId}
          initiativesList={initiativesList}
          clientCompany={clientCompany}
          partnerCompany={partnerCompany}
          currency={currency}
        />
      );
      footer = (
        <CardFooter>
          <ActionButton onClick={() => this.onContributionStepChange(1)} text="Next" />
        </CardFooter>
      );
    }

    if (!loading && step === 1) {
      content = (
        <RelatedItemForm
          relatedItems={relatedItems}
          initiatives={initiativesList}
          onChange={(key, value) => {
            this.onChangeData(key, value);
          }}
          selectedInitiatives={initiatives}
          allowedDealOption={true}
        />
      );

      footer = (
        <CardFooter>
          <ActionButton onClick={() => this.onRelatedItemsStepChange(2)} text={'Next'} />
          <TransparentButton onClick={() => this.onScreen(0)} text={'Previous'} />
        </CardFooter>
      );
    }

    if (!loading && step === 2) {
      const contributionDetail = R.clone(contributionData);
      contributionDetail.documents = { items: contributionDetail.documents };
      content = (
        <>
          <ContributionDetailTable data={contributionDetail} currency={currency} />
          <RelatedItemsDetailTable relatedItems={relatedItems} initiatives={initiatives} />
        </>
      );
      footer = (
        <CardFooter>
          <ActionButton onClick={this.onSubmit} text={'Create Contribution'} />
          <TransparentButton onClick={() => this.onScreen(1)} text={'Previous'} />
        </CardFooter>
      );
    }

    return (
      <React.Fragment>
        <Card.Header>
          <Grid.Layout
            columns="200px auto 200px"
            areas={[['left', 'center', 'right']]}
            style={{ width: '100%' }}>
            <Grid.Box area="left">
              <Heading type="h4" text="Create Contribution" />
            </Grid.Box>
            <Grid.Box area="center">
              <FormSteps totalSteps={3} step={step} />
            </Grid.Box>
            <Grid.Box area="right"></Grid.Box>
          </Grid.Layout>
          <div style={{ paddingLeft: '20px', marginRight: '0px' }}>
            <ActionButtonClose onClick={history.goBack} />
          </div>
        </Card.Header>
        <CreateViewCardBody>
          <Grid.Layout columns="30% 70%" areas={[['left', 'right']]} style={{ width: '100%' }}>
            <Grid.Box area="left">
              <LeftProgressSection sections={SCREENS_CONTRIBUTION} currentScreen={step} />
            </Grid.Box>
            <BoxCard>
              <Grid.Box area="right">{content}</Grid.Box>
            </BoxCard>
          </Grid.Layout>
        </CreateViewCardBody>
        {footer}
      </React.Fragment>
    );
  }
}

ContributionCreateView.propTypes = {
  history: PropTypes.object.isRequired,
};

export default withRouter(ContributionCreateView);

import React from 'react';
import {
  InputField,
  Row,
  TextAreaField,
  Label,
  Column,
  SelectField,
  DateInputField,
} from '@8base/boost';
import PropTypes from 'prop-types';
import { onErrorMixin } from '../../../../shared/mixins';
import { UNIT_TYPES } from '../../../../shared/item-types';
import { CurrencyInputField } from '../../../../shared/components/CurrencyInputField';
import FileInputComponent from '../../../../components/inputs/FileInputComponent';
import { CONTRIBUTION_DOCUMENTS } from '../contribution-model';
import { SelectInputById } from '../../../../components/forms/SelectInputById';
import YesNoDialog from '../../../../components/dialogs/YesNoDialog';
import { HorizontalLineText } from '../../../../components/new-ui/text/HorizontalLineText';
import { HorizontalLine } from '../../../../components/new-ui/text/HorizontalLine';
import { GroupInputs } from '../../../../components/new-ui/inputs/GroupInputs';
import { RowForm } from '../../../../components/new-ui/div/RowForm';

/**
 * The Form for the Funding Request
 */
class ContributionForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      deleteDocumentModalIsOpen: false,
      deleteDocumentPos: null,
    };
    this.onClickDelete = this.onClickDelete.bind(this);
    this.onChangeDocuments = this.onChangeDocuments.bind(this);
  }

  onClickDelete = (pos) => {
    this.setState({
      deleteDocumentModalIsOpen: true,
      deleteDocumentPos: pos,
    });
  };

  onYes = () => {
    this.setState(
      {
        deleteDocumentModalIsOpen: false,
      },
      () => {
        this.onDeleteDocuments('documents', this.state.deleteDocumentPos);
      },
    );
  };

  onClose = () => {
    this.setState({
      deleteDocumentModalIsOpen: false,
    });
  };

  onChangeDocuments(name, value) {
    const contributionData = this.props.data;
    for (let aux of value) {
      contributionData[name].push(aux);
    }
    this.props.onChange('documents', contributionData[name]);
  }

  onDeleteDocuments = (name, pos) => {
    const contributionData = this.props.data;
    contributionData[name].splice(pos, 1);
    let file = JSON.parse(localStorage.getItem(CONTRIBUTION_DOCUMENTS));
    file.splice(pos, 1);
    localStorage.setItem(CONTRIBUTION_DOCUMENTS, JSON.stringify(file));
    this.setState({ deleteDocumentPos: null });
    this.props.onChange('documents', contributionData[name]);
  };

  render() {
    const { deleteDocumentModalIsOpen } = this.state;
    const { onChange, clientCompany, partnerCompany, currency } = this.props;
    const {
      name,
      description,
      documents,
      unitType,
      unitQuantity,
      unitValueDescription,
      unitMonetizationFactor,
      source,
      contributionDate,
    } = this.props.data;
    // company sources
    const companySources = [];
    if (clientCompany) {
      companySources.push({ label: clientCompany.name, value: clientCompany });
    }
    if (partnerCompany) {
      companySources.push({ label: partnerCompany.name, value: partnerCompany });
    }
    return (
      <>
        <HorizontalLine>
          <HorizontalLineText text={'CONTRIBUTION'} />
        </HorizontalLine>
        <GroupInputs text={'General'}>
          <Row growChildren gap="lg">
            <Column alignItems="stretch">
              <InputField
                stretch
                label="Name"
                input={{
                  name: 'name',
                  value: name,
                  onChange: (value, e) => onChange(e.target.name, value),
                }}
              />
              <TextAreaField
                style={{ width: '100%', height: 152 }}
                label="Description"
                input={{
                  name: 'description',
                  value: description,
                  onChange: (value) => onChange('description', value),
                }}
              />
            </Column>
          </Row>
        </GroupInputs>

        <GroupInputs text={'Dates'}>
          <Row growChildren gap="lg">
            <Column style={{ width: '35%' }}>
              <DateInputField
                label="Contribution Date"
                input={{
                  name: 'contributionDate',
                  value: contributionDate,
                  onChange: (value) => onChange('contributionDate', value),
                }}
              />
            </Column>
            <Column style={{ width: '65%' }} />
          </Row>
        </GroupInputs>
        <GroupInputs text={'Funding Type'} show={true}>
          <RowForm growChildren gap="lg">
            <Column style={{ width: '100%' }}>
              <SelectInputById
                label="Select Source"
                input={{
                  name: 'source',
                  value: source,
                  onChange: (value) => onChange('source', value),
                }}
                meta={{}}
                placeholder="Select"
                options={companySources}
              />
            </Column>
          </RowForm>
          <RowForm growChildren gap="lg">
            <Column style={{ width: '35%' }}>
              <InputField
                placeholder={'Unit Quantity'}
                label={`Unit Quantity`}
                type={'number'}
                input={{
                  value: unitQuantity,
                  onChange: (val) => {
                    onChange('unitQuantity', val);
                  },
                }}
              />
            </Column>
            <Column style={{ width: '65%', 'margin-left': '23px' }}>
              <SelectField
                label="Unit Type"
                input={{
                  name: 'unitType',
                  value: unitType,
                  onChange: (value) => onChange('unitType', value),
                }}
                meta={{}}
                placeholder="Select unit Type"
                options={UNIT_TYPES.map((item) => {
                  return {
                    label: `${item}`,
                    value: item,
                  };
                })}
              />
            </Column>
          </RowForm>
          <RowForm>
            <Column style={{ width: '100%' }}>
              <InputField
                stretch
                label="Unit Value Description"
                placeholder={'Description'}
                input={{
                  name: 'unitValueDescription',
                  value: unitValueDescription,
                  onChange: (value, e) => onChange(e.target.name, value),
                }}
              />
              <CurrencyInputField
                stretch
                value={unitMonetizationFactor}
                onChange={(val) => {
                  onChange('unitMonetizationFactor', val);
                }}
                label={`Unit Monetization Factor`}
                currency={currency}
              />
            </Column>
          </RowForm>
        </GroupInputs>
        <HorizontalLine>
          <HorizontalLineText text={'DOCUMENTS'} />
        </HorizontalLine>
        <Row style={{ 'padding-bottom': '15px' }}>
          <Column alignItems="">
            <div style={{ 'margin-left': '20px' }}>
              <Label kind="secondary" style={{ textAlign: 'left' }}>
                Documents (Optional)
              </Label>
              <FileInputComponent
                onChange={this.onChangeDocuments}
                nameInput={'documents'}
                field={documents}
                localKey={CONTRIBUTION_DOCUMENTS}
                maxFiles={5}
                text={'Upload Documents'}
                onClickDelete={this.onClickDelete}
              />
            </div>
          </Column>
        </Row>
        <YesNoDialog
          isOpen={deleteDocumentModalIsOpen}
          onYes={this.onYes}
          onNo={this.onClose}
          onClose={this.onClose}
          text={'Are you sure you want to Delete this Document?'}
          title={'Delete Document'}
        />
      </>
    );
  }
}

Object.assign(ContributionForm.prototype, onErrorMixin);

ContributionForm.propTypes = {
  data: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  clientCompany: PropTypes.object.isRequired,
  partnerCompany: PropTypes.object.isRequired,
  currency: PropTypes.object,
};

ContributionForm.defaultProps = {
  onClickDelete: null,
  currency: null,
};

export default ContributionForm;

import Flux from '@cobuildlab/flux-state';
import sessionStore, { APOLLO_CLIENT, NEW_SESSION_EVENT } from '../../shared/SessionStore';
import { error, log } from '@cobuildlab/pure-logger';
import { ITEMS_LIST_QUERY } from '../related-item/related-item-queries';
import {
  ACTIVE_ITEMS_ERROR_EVENT,
  ACTIVE_ITEMS_REQUEST_EVENT,
  ACTIVE_ITEMS_EVENT,
} from './active-items-store';
// import {} from '../../shared/status';
import { getActiveAllianceId } from '../../shared/alliance-utils';

/**
 * Filter items with next steps
 * @param  {String} allianceId  the allianceId to filter
 * @return {Object}             the filter object
 */
const itemsFilter = (allianceId, search = '') => {
  const riskFilter = {
    risk: {
      id: { not_equals: null },
    },
  };

  const issueFilter = {
    issue: {
      id: { not_equals: null },
    },
  };

  const ideaFilter = {
    idea: {
      id: { not_equals: null },
    },
  };

  const fundingRequestFilter = {
    fundingRequest: {
      id: { not_equals: null },
    },
  };

  const actionFilter = {
    action: {
      id: { not_equals: null },
    },
  };

  const decisionFilter = {
    decision: {
      id: { not_equals: null },
    },
  };

  const contributionFilter = {
    contribution: {
      id: { not_equals: null },
    },
  };

  const filter = {
    alliance: { id: { equals: allianceId } },
    OR: [
      riskFilter,
      issueFilter,
      ideaFilter,
      fundingRequestFilter,
      actionFilter,
      decisionFilter,
      contributionFilter,
    ],
  };

  return filter;
};

/**
 * Fetch Active items
 * @return {Promise<void>}
 */
export const fetchActiveItems = async () => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const user = sessionStore.getState(NEW_SESSION_EVENT).user;
  const allianceId = getActiveAllianceId(user);
  const filter = itemsFilter(allianceId);

  let response;
  try {
    response = await client.query({
      query: ITEMS_LIST_QUERY,
      variables: { data: filter },
      fetchPolicy: 'network-only',
    });
  } catch (e) {
    error('fetchActiveItems', e);
    return Flux.dispatchEvent(ACTIVE_ITEMS_ERROR_EVENT, e);
  }
  log('fetchActiveItems', response.data);
  Flux.dispatchEvent(ACTIVE_ITEMS_EVENT, response.data);
  return response.data;
};

/**
 * Dispatch a Request for seen the related Items
 * @param itemId
 */
export const openActiveItems = (itemId) => {
  Flux.dispatchEvent(ACTIVE_ITEMS_REQUEST_EVENT, { itemId });
};

import Flux from '@cobuildlab/flux-state';
import sessionStore, { APOLLO_CLIENT, NEW_SESSION_EVENT } from 'shared/SessionStore';
import { error, log } from '@cobuildlab/pure-logger';
import { INITIATIVE_ACTIVE_ITEMS_LIST_QUERY } from './initiative-active-items-queries';
import {
  INITIATIVE_ACTIVE_ITEMS_ERROR_EVENT,
  INITIATIVE_ACTIVE_ITEMS_EVENT,
} from './initiative-active-items-store';
import { getActiveAllianceId } from 'shared/alliance-utils';

/**
 * Filter active item, items and nextSteps of the selected initiative.
 *
 * @param  {string} allianceId -  The allianceId to filter.
 * @param  {string} initiativeId -  The initiativeId to filter.
 * @returns {object}             the filter object
 */
const initiativeActiveItemsFilter = (allianceId, initiativeId) => {
  const riskFilter = {
    risk: {
      initiatives: { some: { id: { equals: initiativeId } } },
      id: { not_equals: null },
    },
  };

  const issueFilter = {
    issue: {
      initiatives: { some: { id: { equals: initiativeId } } },
      id: { not_equals: null },
    },
  };

  const ideaFilter = {
    idea: {
      initiatives: { some: { id: { equals: initiativeId } } },
      id: { not_equals: null },
    },
  };

  const fundingRequestFilter = {
    fundingRequest: {
      initiatives: { some: { id: { equals: initiativeId } } },
      id: { not_equals: null },
    },
  };

  const actionFilter = {
    action: {
      initiatives: { some: { id: { equals: initiativeId } } },
      id: { not_equals: null },
    },
  };

  const itemFilter = {
    alliance: { id: { equals: allianceId } },
    OR: [riskFilter, issueFilter, ideaFilter, fundingRequestFilter, actionFilter],
  };

  return { itemFilter };
};

/**
 * Fetch initiative active items.
 *
 * @returns {Promise<void>}
 * @param initiativeId
 */
export const fetchInitiativeActiveItems = async (initiativeId) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { user } = sessionStore.getState(NEW_SESSION_EVENT);
  const allianceId = getActiveAllianceId(user);
  const { itemFilter } = initiativeActiveItemsFilter(allianceId, initiativeId);

  let response;
  try {
    response = await client.query({
      query: INITIATIVE_ACTIVE_ITEMS_LIST_QUERY,
      variables: { itemFilter },
      fetchPolicy: 'network-only',
    });
  } catch (e) {
    error('fetchInitiativeActiveItems', e);
    return Flux.dispatchEvent(INITIATIVE_ACTIVE_ITEMS_ERROR_EVENT, e);
  }
  log('fetchInitiativeActiveItems', response.data);
  Flux.dispatchEvent(INITIATIVE_ACTIVE_ITEMS_EVENT, response.data);
  return response.data;
};

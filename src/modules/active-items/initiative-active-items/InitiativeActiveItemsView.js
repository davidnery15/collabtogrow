import React, { Fragment } from 'react';
import { Card, Heading, Row, Loader, Progress, Column, Text, Grid } from '@8base/boost';
import sessionStore, { NEW_SESSION_EVENT } from '../../../shared/SessionStore';
import { withRouter } from 'react-router-dom';
import { ViewCardBody } from '../../../components/card/ViewCardBody';
import { PropTypes } from 'prop-types';
import View from '@cobuildlab/react-flux-state';
import { onErrorMixin } from 'shared/mixins';
import initiativeActiveItemsStore, {
  INITIATIVE_ACTIVE_ITEMS_ERROR_EVENT,
  INITIATIVE_ACTIVE_ITEMS_EVENT,
} from './initiative-active-items-store';
import * as toast from '../../../components/toast/Toast';
import { fetchInitiativeActiveItems } from './initiative-active-items-actions';
import { getInitiativeActiveItems, filterActiveItemsByDueDate } from '../active-items-utils';
import UserSmallAvatar from '../../../components/user/UserSmallAvatar';
import HorizontalLineText from '../work-queue/components/HorizontalLineText';
import Hr from 'components/text/Hr';
import ActiveItem from '../ActiveItem';
import YesNoDialog from '../../../components/dialogs/YesNoDialog';
import { fetchInitiative } from '../../management/initiative/initiative-actions';
import initiativeStore, {
  INITIATIVE_DETAIL_EVENT,
  INITIATIVE_ERROR_EVENT,
} from '../../management/initiative/initiative-store';
import nextStepStore, {
  NEXT_STEP_COMPLETE_EVENT,
  NEXT_STEP_ERROR_EVENT,
} from '../../next-step/next-step-store';
import ideaStore, {
  IDEA_COMPLETED_EVENT,
  IDEA_ERROR_EVENT,
} from '../../management/idea/idea-store';
import issueStore, {
  ISSUE_COMPLETED_EVENT,
  ISSUE_ERROR_EVENT,
} from '../../management/issue/issue-store';
import riskStore, {
  RISK_COMPLETED_EVENT,
  RISK_ERROR_EVENT,
} from '../../management/risk/risk-store';
import actionStore, {
  ACTION_COMPLETE_EVENT,
  ACTION_ERROR_EVENT,
} from '../../management/action/action-store';
import fundingRequestStore, {
  FUNDING_REQUEST_COMPLETED_EVENT,
  FUNDING_REQUEST_ERROR_EVENT,
} from '../../management/funding-request/funding-request-store';
import { getChartOptions } from '../../dashboard/dashboard-actions';
import PieChart from '../../../components/PieChart';
import Status from '../../../components/Status';

/**
 * The items to show on the view, to show the state items acording to the
 * selected item to show.
 *
 * @type {Array}
 */
const ITEMS_TO_SHOW = [
  { stateName: 'allItems', text: 'All Items' },
  { stateName: 'inProgresItems', text: 'In Progress' },
  { stateName: 'completedItems', text: 'Completed' },
];

/**
 * The filtered items to collapse on the view, other filtered items will remain
 * hidden.
 *
 * @type {Array}
 */
const FILTERED_ITEMS_TO_SHOW = [
  { stateName: 'overDueItems', text: 'Over Due' },
  { stateName: 'dueThisWeekItems', text: 'Due This Week' },
  { stateName: 'notDueItems', text: 'Not Due' },
  { stateName: 'completedDueItems', text: 'Completed' },
];

class InitiativeActiveItemsView extends View {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      completeFunction: null,
      selectedItemToComplete: null,
      completeModalIsOpen: false,
      itemStateToShow: ITEMS_TO_SHOW[0].stateName,
      initiative: null,
      allItems: [],
      inProgresItems: [],
      completedItems: [],
      /*
      activeItems filtered by due date
       */
      overDueItems: [],
      overDueItemsIsOpen: true,
      dueThisWeekItems: [],
      dueThisWeekItemsIsOpen: false,
      notDueItems: [],
      notDueItemsIsOpen: false,
      completedDueItems: [],
      completedDueItemsIsOpen: false,
      /*
      activeItems filtered by due date
       */
    };
    this.onError = onErrorMixin.bind(this);
    this.user = sessionStore.getState(NEW_SESSION_EVENT).user;
    this.selectedAlliance = sessionStore.getState(NEW_SESSION_EVENT).selectedAlliance;
  }

  componentDidMount() {
    const { match } = this.props;

    this.subscribe(initiativeActiveItemsStore, INITIATIVE_ACTIVE_ITEMS_ERROR_EVENT, this.onError);
    this.subscribe(initiativeStore, INITIATIVE_ERROR_EVENT, this.onError);
    this.subscribe(nextStepStore, NEXT_STEP_ERROR_EVENT, this.onError);
    this.subscribe(ideaStore, IDEA_ERROR_EVENT, this.onError);
    this.subscribe(issueStore, ISSUE_ERROR_EVENT, this.onError);
    this.subscribe(riskStore, RISK_ERROR_EVENT, this.onError);
    this.subscribe(actionStore, ACTION_ERROR_EVENT, this.onError);
    this.subscribe(fundingRequestStore, FUNDING_REQUEST_ERROR_EVENT, this.onError);

    this.subscribe(initiativeStore, INITIATIVE_DETAIL_EVENT, ({ initiative }) => {
      this.setState({ initiative });
    });
    this.subscribe(nextStepStore, NEXT_STEP_COMPLETE_EVENT, () => {
      toast.success('Next Step Successfully Completed');
      fetchInitiativeActiveItems(match.params.id);
    });
    this.subscribe(ideaStore, IDEA_COMPLETED_EVENT, () => {
      toast.success('Idea Successfully Completed');
      fetchInitiativeActiveItems(match.params.id);
    });
    this.subscribe(issueStore, ISSUE_COMPLETED_EVENT, () => {
      toast.success('Issue Successfully Completed');
      fetchInitiativeActiveItems(match.params.id);
    });
    this.subscribe(riskStore, RISK_COMPLETED_EVENT, () => {
      toast.success('Risk Successfully Completed');
      fetchInitiativeActiveItems(match.params.id);
    });
    this.subscribe(actionStore, ACTION_COMPLETE_EVENT, () => {
      toast.success('Action Successfully Completed');
      fetchInitiativeActiveItems(match.params.id);
    });
    this.subscribe(fundingRequestStore, FUNDING_REQUEST_COMPLETED_EVENT, () => {
      toast.success('Funding Request Successfully Completed');
      fetchInitiativeActiveItems(match.params.id);
    });

    this.subscribe(initiativeActiveItemsStore, INITIATIVE_ACTIVE_ITEMS_EVENT, (state) => {
      const { items } = state.itemsList;
      const {
        allActiveItems: allItems,
        inProgresActiveItems: inProgresItems,
        completedActiveItems: completedItems,
      } = getInitiativeActiveItems(items, []);

      const {
        overDueItems,
        dueThisWeekItems,
        notDueItems,
        completedDueItems,
      } = filterActiveItemsByDueDate(allItems);

      this.setState({
        loading: false,
        allItems,
        inProgresItems,
        completedItems,
        overDueItems,
        dueThisWeekItems,
        notDueItems,
        completedDueItems,
        itemStateToShow: ITEMS_TO_SHOW[0].stateName,
      });
    });

    if (!match.params.id) return toast.error('Initiative ID missing');
    fetchInitiativeActiveItems(match.params.id);
    fetchInitiative(match.params.id);
  }

  setItemsToShow = (itemStateToShow) => {
    if (itemStateToShow === this.state.itemStateToShow) return;

    const {
      overDueItems,
      dueThisWeekItems,
      notDueItems,
      completedDueItems,
    } = filterActiveItemsByDueDate(this.state[itemStateToShow]);

    this.setState({
      itemStateToShow: itemStateToShow,
      overDueItems,
      dueThisWeekItems,
      notDueItems,
      completedDueItems,
    });
  };

  toggleFilteredItems = (filteredItemsStateName) => {
    const stateName = `${filteredItemsStateName}IsOpen`;
    this.setState({ [stateName]: !this.state[stateName] });
  };

  onClickComplete = (completeFunction, selectedItemToComplete) => {
    this.setState({
      completeModalIsOpen: true,
      completeFunction,
      selectedItemToComplete,
    });
  };

  onYesComplete = () => {
    const { completeFunction, selectedItemToComplete } = this.state;
    this.setState(
      {
        completeModalIsOpen: false,
        loading: true,
      },
      () => {
        completeFunction(selectedItemToComplete);
      },
    );
  };

  onCloseComplete = () => {
    this.setState({
      completeModalIsOpen: false,
    });
  };

  onClickChart = (event) => {
    let due = event.data.type;
    let item = event.data.itemType;
    this.props.history.push(`/management/amo-item?due=${due}&item=${item}`);
  };

  onClickDue = (due, item) => {
    this.props.history.push(`/management/amo-item?due=${due}&item=${item}`);
  };

  render() {
    const { loading, completedItems, allItems, completeModalIsOpen, initiative } = this.state;
    const progressValue = Math.trunc((completedItems.length * 100) / allItems.length) || 0;
    const onEvents = {
      click: this.onClickChart,
    };
    const { risksOptions, issuesOptions, actionsOptions } = getChartOptions(allItems);

    let content = <Loader stretch />;

    if (!loading) {
      content = (
        <>
          <Row growChildren gap="md" alignItems="center">
            <Column style={{ width: '100%' }}>
              {initiative ? (
                <>
                  <Text weight="bold">{initiative.name}</Text>
                  {/* <Progress color={initiative.ragStatus} size="sm" value={progressValue} /> */}
                  <Status status={initiative.ragStatus} />
                  <UserSmallAvatar
                    owner={initiative.owner}
                    text="is the owner of this initiative"
                  />
                </>
              ) : null}
            </Column>
          </Row>
          <Row growChildren gap="md" alignItems="center" style={{ marginTop: '30px' }}>
            <Column>
              <PieChart
                title="Risks"
                itemName="RISK"
                paperPadding="xs"
                onEvents={onEvents}
                data={risksOptions}
                onClickLegend={this.onClickDue}
              />
            </Column>
            <Column>
              <PieChart
                title="Issues"
                itemName="ISSUE"
                paperPadding="xs"
                onEvents={onEvents}
                data={issuesOptions}
                onClickLegend={this.onClickDue}
              />
            </Column>
            <Column>
              <PieChart
                title="Actions"
                itemName="ACTION"
                paperPadding="xs"
                onEvents={onEvents}
                data={actionsOptions}
                onClickLegend={this.onClickDue}
              />
            </Column>
          </Row>

          <Hr marginHorizontal={-24} />

          {FILTERED_ITEMS_TO_SHOW.map(({ stateName, text }, i) => {
            const filteredItems = this.state[stateName];
            const active = this.state[`${stateName}IsOpen`];
            const count = filteredItems.length;

            return (
              <Fragment key={i}>
                <HorizontalLineText
                  onClick={() => this.toggleFilteredItems(stateName)}
                  text={text}
                  active={active}
                  count={count}
                />
                {/* show the filtered items if isOpen */}
                {active && count ? (
                  <Grid.Layout padding="xl">
                    {filteredItems.map((activeItem, i) => {
                      return (
                        <ActiveItem
                          item={activeItem}
                          key={i}
                          onClickComplete={this.onClickComplete}
                          showNextSteps={true}
                        />
                      );
                    })}
                  </Grid.Layout>
                ) : null}
              </Fragment>
            );
          })}
        </>
      );
    }

    return (
      <Fragment>
        <Card.Header>
          <Heading type="h4" text={'Initiative Dashboard'} />
        </Card.Header>
        <ViewCardBody>{content}</ViewCardBody>
        <YesNoDialog
          isOpen={completeModalIsOpen}
          onYes={this.onYesComplete}
          onNo={this.onCloseComplete}
          onClose={this.onCloseComplete}
          text={'Are you sure you want to Mark this Item as Completed?'}
          title={'Mark Completed'}
        />
      </Fragment>
    );
  }
}

InitiativeActiveItemsView.propTypes = {
  history: PropTypes.object.isRequired,
};

export default withRouter(InitiativeActiveItemsView);

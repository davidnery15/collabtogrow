import React from 'react';
import './assets/home.css';
import { withRouter, Link } from 'react-router-dom';
import { Column, Row, Grid, Button } from '@8base/boost';
import { PropTypes } from 'prop-types';
import { withAuth } from '@8base/app-provider';
import IconSucceed from '../../images/wizard/succeed-icon.png';
import Logo from '../../images/logos/collabtogrow_main_logo.svg';
import SearchIcon from '../../images/icons/search.svg';
import FacebookIcon from '../../images/icons/facebook-logo.svg';
import InstagramIcon from '../../images/icons/ig.svg';
import TwitterIcon from '../../images/icons/twitter.svg';
import { Signup } from './components/Signup';
import ErrorDialog from '../../components/dialogs/ErrorDialog';
import { showAuth0LockLogin, showAuth0LockSignup } from '../auth/auth.actions';
import View from '@cobuildlab/react-flux-state';
import { onChangeMixin, onErrorMixin } from '../../shared/mixins';
import { getInvitationFromSearch } from '../../shared/utils';
import sessionStore, { SESSION_ERROR, AUTH_LOCK_LOGIN_EVENT } from '../../shared/SessionStore';
import * as jwtToken from 'jwt-decode';

/**
 * Home View
 * @param props
 * @return {*}
 * @constructor
 */
class HomeView extends View {
  constructor(props) {
    super(props);
    this.state = {
      signUpModalIsOpen: false,
      signUpModalIsSuccess: false,
      errorModalIsOpen: false,
      errorModalText: '',
      data: {
        email: '',
        password: '',
        firstName: '',
        lastName: '',
      },
      invitation: null,
      loading: false,
    };
    this.onError = onErrorMixin.bind(this);
    this.onChange = onChangeMixin.bind(this);
  }

  componentDidMount(): undefined {
    this.subscribe(sessionStore, SESSION_ERROR, this.onError);
    this.subscribe(sessionStore, AUTH_LOCK_LOGIN_EVENT, async (credentials) => {
      const { idToken } = credentials;
      this.verifyToken(idToken);
    });

    this.checkHashForToken();
    this.checkForInvitationParams();
    this.checkForVerifyEmailParams();
  }

  checkForVerifyEmailParams = () => {
    const { search } = this.props.location;
    const params = new URLSearchParams(search);
    const message = params.get('message');
    const success = params.get('success');

    const verifyMessage = 'Your email was verified. You can continue using the application.';
    if (message === verifyMessage && success === 'true') {
      this.setState({ signUpModalIsOpen: true, signUpModalIsSuccess: true });
    }
  };

  checkForInvitationParams = () => {
    const invitation = getInvitationFromSearch();
    if (invitation) {
      localStorage.removeItem('search');
      this.setState({ invitation, signUpModalIsOpen: true });
    }
  };

  verifyToken = async (idToken) => {
    console.log(`DEBUG:verifyToken`);
    const { email_verified } = jwtToken(idToken);

    if (!email_verified) {
      return this.setState({
        errorModalText: 'You must verify your email in order to log in',
        errorModalTitle: 'Login Error',
        errorModalIsOpen: true,
      });
    }

    // clear sessionStorage on login
    sessionStorage.clear();

    const { auth, history } = this.props;
    await auth.setAuthState({ token: idToken, idToken });
    const redirectUri = localStorage.getItem('redirectUri') || '/welcome';
    history.replace(redirectUri);
  };

  verifyParams = async (params) => {
    if (params.has('error')) {
      return this.setState({
        errorModalText: params.get('error_description'),
        errorModalTitle: params.get('error'),
        errorModalIsOpen: true,
      });
    }

    const idToken = params.get('id_token');
    this.verifyToken(idToken);
  };

  checkHashForToken = () => {
    const { location } = this.props;
    const paramsString = location.hash.replace('#', '?');
    const params = new URLSearchParams(paramsString);

    if (!params.has('id_token') && !params.has('error')) return;

    this.verifyParams(params);
  };

  onOpen = () => {
    this.setState({ signUpModalIsOpen: true, signUpModalIsSuccess: false });
  };

  onSignUp = () => {
    this.setState({ signUpModalIsOpen: false });
    showAuth0LockSignup();
  };

  onLogin = () => {
    this.setState({ signUpModalIsOpen: false });
    showAuth0LockLogin();
  };

  onCancel = () => {
    const { invitation } = this.state;
    if (invitation) {
      localStorage.removeItem('redirectUri');
      localStorage.removeItem('search');
    }

    this.setState({
      signUpModalIsOpen: false,
      signUpModalIsSuccess: false,
      invitation: null,
    });
  };

  onCloseErrorModal = () => {
    this.setState({ errorModalIsOpen: false });
  };

  render() {
    const {
      signUpModalIsOpen,
      loading,
      data,
      signUpModalIsSuccess,
      errorModalIsOpen,
      errorModalTitle,
      errorModalText,
      invitation,
    } = this.state;

    // if (isAuthorized) {
    //   const returnTo = window.location.pathname + window.location.search;
    //   this.props.auth.logout(returnTo);
    //   return null;
    // }

    return (
      <div>
        <nav className="home-nav">
          <div className="home-nav-container">
            <Row justifyContent="start">
              <Row style={{ width: '100%' }}>
                <div>
                  <img src={Logo} width="200" alt="logo" />
                </div>
                <Row className="home-nav-right">
                  <a href=" " className="home-link-nav" style={{ paddingTop: '12px' }}>
                    {' '}
                    <img src={SearchIcon} alt="Search" width="15" />{' '}
                  </a>

                  <a href=" " className="home-link-nav">
                    About
                  </a>
                  <a href=" " className="home-link-nav">
                    Services
                  </a>
                  <a href=" " className="home-link-nav">
                    Insights
                  </a>
                  <a href=" " className="home-link-nav">
                    Resources
                  </a>
                  <Button className="home-buttom-nav" variant="outlined" onClick={this.onOpen}>
                    Sign Up
                  </Button>
                  <Button
                    className="home-buttom-nav-login"
                    variant="outlined"
                    onClick={showAuth0LockLogin}>
                    Login
                  </Button>
                </Row>
              </Row>
            </Row>
          </div>
        </nav>
        <section className="home-section-bg" />
        <section className="home-section-container">
          <Grid.Layout inline columns="80vh 20vh" className="home-body-section">
            <Grid.Box className="home-column">
              <h3>Imagine. Align.</h3>
              <h1 className={'home-view'}>Succeed.</h1>
              <p className={'home-view'}>
                collabtogrow is a Boston-based technology-enabled managed services and advisory firm
                that specializes in improving the performance of strategic alliances. Leveraging our
                proven framework of industry-leading practices, standards, and tools, we help our
                clients unlock the growth potential of collaborative relationships.
              </p>
              <br />
              <p className={'home-view'}>
                collabtogrow is a Boston-based technology-enabled managed services and advisory firm
                that specializes in improving the performance of strategic alliances. Leveraging our
                proven framework of industry-leading practices, standards, and tools, we help our
                clients unlock the growth potential of collaborative relationships.
              </p>
            </Grid.Box>
            <Grid.Box>
              <img src={IconSucceed} width="125" alt="logo" />
            </Grid.Box>
          </Grid.Layout>
          <Row justifyContent="center">
            <div>
              <Button className="home-body-buttom">Our Services</Button>
            </div>
            <div>
              <Button className="home-body-buttom">Contact Us</Button>
            </div>
          </Row>
        </section>
        <section className="home-section-video">
          <Row>
            <Column className="home-section-container">
              <h3 className="home-view home-title-video">HEAR FROM OUR CO-FOUNDERS: </h3>
              <iframe
                src="https://player.vimeo.com/video/311295865?title=0&byline=0&portrait=0"
                title="Collabgrow"
                width="1040"
                height="585"
                frameBorder="0"
                allow="autoplay; fullscreen"
                allowFullScreen
              />
            </Column>
          </Row>
        </section>
        <section className="home-section-footer">
          <Row>
            <Column className="home-section-container">
              <Grid.Layout inline columns="30vh 30vh 30vh 10vh">
                <Grid.Box>
                  <h3 className="home-view home-title-footer">© 2019 Collabtogrow</h3>
                  <p className="home-subtitle-footer">info@collabtogrow.com</p>
                </Grid.Box>
                <Grid.Box>
                  <Column>
                    <Link to="/home" className="home-link-footer">
                      About Collabgrow
                    </Link>
                  </Column>
                  <Column>
                    <Link to="/home" className="home-link-footer">
                      Meet our Team
                    </Link>
                  </Column>
                  <Column>
                    <Link to="/home" className="home-link-footer">
                      Alliance Services
                    </Link>
                  </Column>
                  <Column>
                    <Link to="/home" className="home-link-footer">
                      Alliance Platform
                    </Link>
                  </Column>
                </Grid.Box>
                <Grid.Box>
                  <Column>
                    <Link to="/home" className="home-link-footer">
                      Questionnaire
                    </Link>
                  </Column>
                  <Column>
                    <Link to="/home" className="home-link-footer">
                      Managing Your Alliance
                    </Link>
                  </Column>
                  <Column>
                    <Link to="/home" className="home-link-footer">
                      Frequently Asked Questions
                    </Link>
                  </Column>
                  <Column>
                    <Link to="/home" className="home-link-footer">
                      Contact Us
                    </Link>
                  </Column>
                </Grid.Box>
                <Grid.Box>
                  <Row>
                    <a
                      href="https://twitter.com/collabtogrow"
                      className="home-link-redes"
                      target="blank">
                      <img src={TwitterIcon} alt="Twitter" width="15" />
                    </a>
                    <a
                      href="http://instagram.com/collabtogrow"
                      className="home-link-redes"
                      target="blank">
                      <img src={InstagramIcon} alt="Instagram" width="15" />
                    </a>
                    <a
                      href="https://www.facebook.com/collabtogrow"
                      className="home-link-redes"
                      target="blank">
                      <img src={FacebookIcon} alt="Facebook" width="15" />
                    </a>
                  </Row>
                </Grid.Box>
              </Grid.Layout>
            </Column>
          </Row>
        </section>
        <Signup
          isOpen={signUpModalIsOpen}
          onChange={this.onChange}
          data={data}
          loading={loading}
          onCancel={this.onCancel}
          onSignUp={this.onSignUp}
          onLogin={this.onLogin}
          successState={signUpModalIsSuccess}
          invitation={invitation}
        />
        <ErrorDialog
          isOpen={errorModalIsOpen}
          onOk={this.onCloseErrorModal}
          title={errorModalTitle}
          text={errorModalText}
        />
      </div>
    );
  }
}

HomeView.propTypes = {
  history: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
};

export default withAuth(withRouter(HomeView));

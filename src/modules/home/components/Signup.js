import React from 'react';
import { Grid, Button, Dialog, Loader, Text, Row, Column, Paragraph, Link } from '@8base/boost';
import { PropTypes } from 'prop-types';
import IconSucceed from '../../../images/wizard/succeed-icon.png';
import logo from '../../../images/logos/collabtogrow_main_logo.svg';
import { ActionButton } from '../../../components/buttons/ActionButton';

/**
 * Home View
 * @param props
 * @return {*}
 * @constructor
 */
const Signup = (props) => {
  const { isOpen, loading, onCancel, onSignUp, onLogin, successState, invitation } = props;
  let headerContent = <Loader stretch />;
  if (!loading) headerContent = <img width={180} src={logo} alt={'Logo'} />;

  let text = `Sign up below to begin collaborating!`;
  if (invitation !== null) {
    if (invitation.type === 'company')
      text = (
        <Paragraph>
          {' '}
          You have been invited by <Text weight={'bold'}>{invitation.name}</Text> to join as Member
        </Paragraph>
      );
    else if (invitation.type === 'alliance')
      text = (
        <Paragraph>
          You have been invited to join the Alliance: <Text weight={'bold'}>{invitation.name}</Text>
        </Paragraph>
      );
    else if (invitation.type === 'alliance-member')
      text = (
        <Paragraph>
          You have been invited to be a member in the Alliance:{' '}
          <Text weight={'bold'}>{invitation.name}</Text>
        </Paragraph>
      );
    else text = `Ooops!`;
  }

  let content = (
    <>
      <Dialog.Body scrollable>
        <Grid.Layout gap="sm" stretch>
          <Grid.Box alignItems={'center'}>{headerContent}</Grid.Box>
          <Grid.Box justifyContent={'center'} alignItems={'center'}>
            <br />
            <Text weight="bold">Welcome to Collab to Grow!</Text>
            <br />
            <Paragraph>{text}</Paragraph>
            <br />
            <Link onClick={onLogin} text="Already have an account? Log In here" />
          </Grid.Box>
        </Grid.Layout>
      </Dialog.Body>
      <Dialog.Footer style={{ width: '360px' }}>
        <Button color="neutral" variant="outlined" onClick={onCancel}>
          {'Cancel'}
        </Button>
        <ActionButton onClick={onSignUp} text={'Sign Up'} />
      </Dialog.Footer>
    </>
  );

  if (successState)
    content = (
      <>
        <Dialog.Body scrollable>
          <Grid.Layout gap="sm" stretch>
            <Grid.Box alignItems={'center'}>{headerContent}</Grid.Box>
            <Grid.Box justifyContent={'center'} alignItems={'center'}>
              <br />
              <Text weight="bold">Begin Collaborating!</Text>
              <br />
              <Paragraph align={'center'}>
                You can now Login to the platform, continue below to get started.
              </Paragraph>
              <br />
            </Grid.Box>
            <Grid.Box alignItems={'center'}>
              <br />
              <img src={IconSucceed} alt={'Succed'} />
              <br />
              <br />
            </Grid.Box>
          </Grid.Layout>
          <Grid.Layout alignItems={'center'} stretch={false}>
            <Row alignItems={'center'} justifyContent={'center'}>
              <Column alignItems={'center'}>
                <ActionButton onClick={onLogin} text={'Get Started'} />
              </Column>
            </Row>
          </Grid.Layout>
        </Dialog.Body>
      </>
    );

  return (
    <Dialog size="sm" isOpen={isOpen}>
      {content}
    </Dialog>
  );
};

Signup.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  loading: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
  onCancel: PropTypes.func.isRequired,
  onSignUp: PropTypes.func.isRequired,
  onLogin: PropTypes.func.isRequired,
  successState: PropTypes.bool.isRequired,
  invitation: PropTypes.object,
};

export { Signup };

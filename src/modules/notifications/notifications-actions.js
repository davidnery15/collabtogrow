import sessionStore, { APOLLO_CLIENT } from 'shared/SessionStore';
import { error, log } from '@cobuildlab/pure-logger';
import Flux from '@cobuildlab/flux-state';
import { NEW_SESSION_EVENT } from '../../shared/SessionStore';
import { NOTIFICATIONS_LIST_QUERY, NOTIFICATIONS_UPDATE_MUTATION } from './notifications-queries';
import notificationsStore, {
  NOTIFICATIONS_ERROR_EVENT,
  NOTIFICATIONS_EVENT,
  NOTIFICATIONS_MARK_READ_EVENT,
  NOTIFICATIONS_REQUEST_EVENT,
} from './notifications-store';
import { CHAT_TOGGLE_EVENT } from '../chat/chat-store';
import { IntegrityError } from '../../shared/errors';

const notificationsFilter = (allianceId, userId) => {
  const riskFilter = {
    risk: {
      id: { not_equals: null },
      itemRiskRelation: {
        alliance: { id: { equals: allianceId } },
      },
    },
  };
  const issueFilter = {
    issue: {
      id: { not_equals: null },
      itemIssueRelation: {
        alliance: { id: { equals: allianceId } },
      },
    },
  };
  const ideaFilter = {
    idea: {
      id: { not_equals: null },
      itemIdeaRelation: {
        alliance: { id: { equals: allianceId } },
      },
    },
  };
  const fundingRequestFilter = {
    fundingRequest: {
      id: { not_equals: null },
      itemFundingRequestRelation: {
        alliance: { id: { equals: allianceId } },
      },
    },
  };
  const actionFilter = {
    action: {
      id: { not_equals: null },
      itemActionRelation: {
        alliance: { id: { equals: allianceId } },
      },
    },
  };
  const decisionFilter = {
    decision: {
      id: { not_equals: null },
      itemDecisionRelation: {
        alliance: { id: { equals: allianceId } },
      },
    },
  };
  const contributionFilter = {
    contribution: {
      id: { not_equals: null },
      itemContributionRelation: {
        alliance: { id: { equals: allianceId } },
      },
    },
  };
  const dealDataFilter = {
    dealData: {
      id: { not_equals: null },
      itemDealDataRelation: {
        alliance: { id: { equals: allianceId } },
      },
    },
  };
  const initiativesFilter = {
    initiative: {
      id: { not_equals: null },
      alliance: { id: { equals: allianceId } },
    },
  };
  const alliancesFilter = {
    alliance: {
      id: { not_equals: null },
    },
  };
  const nextStepFilter = {
    nextStep: {
      id: { not_equals: null },
      OR: [
        {
          riskNextStepsRelation: {
            id: { not_equals: null },
            itemRiskRelation: { alliance: { id: { equals: allianceId } } },
          },
        },
        {
          ideaNextStepsRelation: {
            id: { not_equals: null },
            itemIdeaRelation: { alliance: { id: { equals: allianceId } } },
          },
        },
        {
          fundingRequestNextStepsRelation: {
            id: { not_equals: null },
            itemFundingRequestRelation: { alliance: { id: { equals: allianceId } } },
          },
        },
        {
          actionNextStepsRelation: {
            id: { not_equals: null },
            itemActionRelation: { alliance: { id: { equals: allianceId } } },
          },
        },
        {
          issueNextStepsRelation: {
            id: { not_equals: null },
            itemIssueRelation: { alliance: { id: { equals: allianceId } } },
          },
        },
      ],
    },
  };

  return {
    isRead: { equals: false },
    owner: { id: { equals: userId } },
    OR: [
      riskFilter,
      issueFilter,
      ideaFilter,
      fundingRequestFilter,
      actionFilter,
      decisionFilter,
      contributionFilter,
      dealDataFilter,
      initiativesFilter,
      alliancesFilter,
      nextStepFilter,
    ],
  };
};

/**
 * Fetches the notifications.
 *
 * @returns {Promise<void|number|*>}
 * @param limit
 */
export const fetchNotifications = async (limit = 10) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { selectedAlliance, user } = sessionStore.getState(NEW_SESSION_EVENT);

  if (!selectedAlliance) {
    return setTimeout(() => {
      return Flux.dispatchEvent(
        NOTIFICATIONS_ERROR_EVENT,
        new IntegrityError('Permission Error. No Alliance Selected'),
      );
    });
  }

  const filter = notificationsFilter(selectedAlliance.id, user.id);
  let response;
  try {
    response = await client.query({
      query: NOTIFICATIONS_LIST_QUERY,
      variables: { data: filter, first: limit },
      fetchPolicy: 'network-only',
    });
  } catch (e) {
    error('fetchNotifications', e);
    return Flux.dispatchEvent(NOTIFICATIONS_ERROR_EVENT, e);
  }
  log('fetchNotifications', response.data.notificationsList.items);
  Flux.dispatchEvent(NOTIFICATIONS_EVENT, response.data.notificationsList.items);
  return response.data.notificationsList.items;
};

const _markAsRead = async (notificationId) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const data = { isRead: true, id: notificationId };
  let response;
  try {
    response = await client.mutate({
      mutation: NOTIFICATIONS_UPDATE_MUTATION,
      variables: { data },
    });
  } catch (e) {
    error('_markAsRead', e);
    throw e;
  }
  return response;
};

export const markAsRead = async (notificationId) => {
  let response;
  try {
    response = await _markAsRead(notificationId);
  } catch (e) {
    error('markAsRead', e);
    Flux.dispatchEvent(NOTIFICATIONS_ERROR_EVENT, e);
    throw e;
  }
  log('markAsRead', response);
  Flux.dispatchEvent(NOTIFICATIONS_MARK_READ_EVENT, response);
  return response;
};

export const markAllAsRead = async () => {
  const notifications = notificationsStore.getState(NOTIFICATIONS_EVENT);
  const promises = [];
  for (let i = 0, j = notifications.length; i < j; i++) {
    promises.push(_markAsRead(notifications[i].id));
  }

  try {
    await Promise.all(promises);
  } catch (e) {
    error('markAllAsRead', e);
    return Flux.dispatchEvent(NOTIFICATIONS_ERROR_EVENT, e);
  }
  log('markAllAsRead', {});
  Flux.dispatchEvent(NOTIFICATIONS_MARK_READ_EVENT, {});
};

export const openNotificationsSideBar = () => {
  Flux.dispatchEvent(NOTIFICATIONS_REQUEST_EVENT, null);
  Flux.dispatchEvent(CHAT_TOGGLE_EVENT, false);
};

import React, { Component } from 'react';
import { Card, Paragraph, Heading } from '@8base/boost';
import { withApollo } from 'react-apollo';

// Main View of Summary of Business Cases
class BusinessCases extends Component {
  constructor(props) {
    super(props);
    this.user = null;
  }

  // TODO: REVIEW & CLEAN STUFF BELOW:
  //
  // componentDidMount() {
  //   this.sessionErrorSubscription = sessionStore.subscribe(SESSION_ERROR, state => {
  //     toast.error(state.message);
  //   });
  //   this.initialMetaSubscription = sessionStore.subscribe(USER_CREATED_EVENT, state => {
  //     this.step = state.value;
  //     this.navigateToWizardIfNecesary();
  //   });
  //   this.metaSubscription = sessionStore.subscribe(SESSION_META_EVENT, (state: Array) => {
  //     const step = state.find(meta => meta.name === META_STEP_NAME);
  //     if (step === undefined)
  //       return this.createInitialMeta();
  //     this.step = step.value;
  //     this.navigateToWizardIfNecesary();
  //   });
  //   this.sessionSubscription = sessionStore.subscribe(SESSION_EVENT, state => {
  //     this.user = state;
  //     fetchMeta();
  //   });
  //   // We require the user to obtain the userId, don't know yet how to pull it from The eighBaseProvider
  //   // TODO: Pull data from user earlier
  //   this.apolloSubscription = sessionStore.subscribe(APOLLO_CLIENT, _ => {
  //     fetchUser();
  //   });
  //   Flux.dispatchEvent(APOLLO_CLIENT, this.props.client);
  // }

  // componentWillUnmount() {
  //   this.sessionSubscription.unsubscribe();
  //   this.metaSubscription.unsubscribe();
  //   this.apolloSubscription.unsubscribe();
  //   this.initialMetaSubscription.unsubscribe();
  // }

  render() {
    return (
      <React.Fragment>
        <Card.Header>
          <Heading type="h4" text="General" />
        </Card.Header>
        <Card.Body
          borderRadius="all"
          style={{ height: '100vh', textAlign: 'center', paddingTop: 66 }}>
          <Paragraph
            style={{
              opacity: '0.9',
              color: '#323C47',
              fontSize: 16,
              fontWeight: 600,
            }}>
            General Documents
          </Paragraph>
          <Paragraph
            style={{
              opacity: '0.9',
              color: '#323C47',
              fontSize: 13,
              fontWeight: 400,
            }}>
            Currently Under Development...
          </Paragraph>
        </Card.Body>
      </React.Fragment>
    );
  }
}

// TODO:
// BusinessCases.propTypes = {
//   client: PropTypes.object.isRequired
// };

export default withApollo(BusinessCases);

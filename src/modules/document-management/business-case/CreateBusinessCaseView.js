import React, { Component } from 'react';
import { Card, Heading, Button } from '@8base/boost';
import BusinessCaseForm from './components/BusinessCaseForm';
import businessCaseStore, {
  BUSINESS_CASE_CREATE_EVENT,
  BUSINESS_CASE_ERROR_EVENT,
} from './BusinessCase.store';
import * as toast from 'components/toast/Toast';
import { createBusinessCase } from './businessCases.actions';
import { Loader } from '@8base/boost';
import { withRouter } from 'react-router-dom';
import { PropTypes } from 'prop-types';
import BusinessCaseModel from './BusinessCase.model';
import * as R from 'ramda';

// Main View of Summary of Business Cases
class CreateBusinessCase extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: R.clone(BusinessCaseModel),
      loading: false,
    };
  }

  componentDidMount() {
    this.businessCaseErrorSubscription = businessCaseStore.subscribe(
      BUSINESS_CASE_ERROR_EVENT,
      (state) => {
        toast.error(state.message);
      },
    );
    this.businessCaseCreateSubscription = businessCaseStore.subscribe(
      BUSINESS_CASE_CREATE_EVENT,
      (state) => {
        toast.success('Business Case Successfully Created');
        this.props.history.goBack();
      },
    );
  }

  componentWillUnmount() {
    this.businessCaseErrorSubscription.unsubscribe();
    this.businessCaseCreateSubscription.unsubscribe();
  }

  onSubmit = () => {
    this.setState({ loading: true }, () => {
      const data = R.clone(this.state.data);
      delete data.loading;
      delete data.id;
      createBusinessCase(data);
    });
  };

  onChange = (name, value) => {
    const { data } = this.state;
    data[name] = value;
    this.setState({ data });
  };

  render() {
    let content = <BusinessCaseForm data={this.state.data} onChange={this.onChange} />;

    if (this.state.loading === true) content = <Loader stretch />;

    return (
      <React.Fragment>
        <Card.Header>
          <Heading type="h4" text="Create Business Case" />
        </Card.Header>
        <Card.Body borderRadius="all" style={{ padding: 20, textAlign: 'center' }}>
          {content}
        </Card.Body>
        <Card.Footer>
          <Button type="button" onClick={this.onSubmit} text="Create Business Case" />
        </Card.Footer>
      </React.Fragment>
    );
  }
}

// // TODO:
CreateBusinessCase.propTypes = {
  history: PropTypes.object.isRequired,
};

export default withRouter(CreateBusinessCase);

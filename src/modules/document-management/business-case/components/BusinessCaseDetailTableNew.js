import React from 'react';
import PropTypes from 'prop-types';
import DetailValue from '../../../../components/DetailValue';
import CostYearPrefix from '../../../../components/text/CostYearPrefix';
import { CurrencyTextField } from '../../../../shared/components/CurrencyTextField';
import { Row } from '@8base/boost';
import UserDetailValue from 'components/UserDetailValue';
import DocumentsFileComponent from '../../../../components/inputs/DocumentsFileComponent';
import { BUSINESS_CASE_DOCUMENT } from '../BusinessCase.model';
import { FlexTopDiv } from '../../../../components/new-ui/div/FlexTopDiv';
import { HeaderText } from '../../../../components/new-ui/text/HeaderText';
import '../../../../components/new-ui/forms.css';
import styled from '@emotion/styled';

const StyledTable = styled('table')({
  'font-family': 'Trebuchet MS',
  borderCollapse: 'collapse',
  width: '70% !important',
  margin: '0 auto',
  'text-align': 'left',
  border: '1px solid rgb(241, 241, 241)',
  'padding-bottom': '10px',
});

const Th = styled('th')({
  border: '1px solid rgb(241, 241, 241)',
  padding: '10px 25px!important',
  height: '1.78 %',
  width: '7.85 %',
  color: '#7E828B!important',
  'font-family': 'Poppins',
  'font-size': '12px',
  'font-weight': 'bold',
  'letter-spacing': '0.5px',
  'line-height': '16px',
  backgroundColor: '#edf0f2',
});

const Td = styled('td')({
  height: '6%',
  width: '73.19%',
  color: '#323C47',
  'font-family': 'Poppins',
  'font-size': '13px',
  'line-height': '18px',
});

/**
 * Detail View Table For Business Case
 */
class BusinessCaseDetailTable extends React.Component {
  render() {
    const { currency } = this.props;
    const {
      backgroundSummary,
      visionMissionStrategy,
      marketAnalysis,
      salesMarketingStrategy,
      expectedCosts,
      expectedRevenues,
      anticipatedCosts,
      risks,
      document,
      owner,
      /* recommendedSolutions,
      document, */
    } = this.props.data;
    return (
      <>
        <FlexTopDiv>
          <HeaderText> BUSINESS CASE </HeaderText>
        </FlexTopDiv>
        <StyledTable className="details">
          <tbody>
            <tr>
              <Th>Background Summary</Th>
              <Td>
                <DetailValue text={backgroundSummary} />
              </Td>
            </tr>
            <tr>
              <Th>Vision, Mission, Strategy</Th>
              <Td>
                <DetailValue text={visionMissionStrategy} />
              </Td>
            </tr>
            <tr>
              <Th>Market Analysis</Th>
              <Td>
                <DetailValue text={marketAnalysis} />
              </Td>
            </tr>
            <tr>
              <Th>Sales Market Stratgey</Th>
              <Td>
                <DetailValue text={salesMarketingStrategy} />
              </Td>
            </tr>
            <tr>
              <Th>Risks</Th>
              <Td>
                <DetailValue text={risks} />
              </Td>
            </tr>
            <tr>
              <Th>Anticipated Contributions</Th>
              <Td>
                {anticipatedCosts.map((value, index) => (
                  <Row key={index}>
                    <CostYearPrefix index={index} />
                    <CurrencyTextField value={value} currency={currency} />
                  </Row>
                ))}
              </Td>
            </tr>
            <tr>
              <Th>Expected Costs Avoidance</Th>
              <Td>
                {expectedCosts.map((value, index) => (
                  <Row key={index}>
                    <CostYearPrefix index={index} />
                    <CurrencyTextField value={value} currency={currency} />
                  </Row>
                ))}
              </Td>
            </tr>
            <tr>
              <Th>Expected Bookings</Th>
              <Td>
                {expectedRevenues.map((value, index) => (
                  <Row key={index}>
                    <CostYearPrefix index={index} />
                    <CurrencyTextField value={value} currency={currency} />
                  </Row>
                ))}
              </Td>
            </tr>
            <tr>
              <Th>Owner</Th>
              <Td>
                <UserDetailValue user={owner} />
              </Td>
            </tr>
            <tr>
              <Th>Document</Th>
              <Td>
                <DocumentsFileComponent data={document} localKey={BUSINESS_CASE_DOCUMENT} />
              </Td>
            </tr>
          </tbody>
        </StyledTable>
      </>
    );
  }
}

BusinessCaseDetailTable.propTypes = {
  data: PropTypes.object.isRequired,
  currency: PropTypes.object.isRequired,
};

export default BusinessCaseDetailTable;

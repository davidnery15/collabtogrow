import React, { Component } from 'react';
import { Card, Heading } from '@8base/boost';
import businessCaseStore, {
  BUSINESS_CASE_UPDATE_EVENT,
  BUSINESS_CASE_DETAIL_EVENT,
  BUSINESS_CASE_ERROR_EVENT,
} from './BusinessCase.store';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import BusinessCaseDetailTable from './components/BusinessCaseDetailTable';
import * as toast from 'components/toast/Toast';
import { fetchBusinessCase, updateBusinessCase } from './businessCases.actions';
import BusinessCaseModel from './BusinessCase.model';
import { Loader } from '@8base/boost';
import { updateStateFromObject } from '../../../shared/utils';
import { Button } from '@8base/boost';
import * as R from 'ramda';

// Main View of Summary of Business Cases
class DetailBusinessCase extends Component {
  constructor(props) {
    super(props);
    this.state = {
      BusinessCaseModel: R.clone(BusinessCaseModel),
      id: null,
      loading: true,
    };
    this.originalRecommendedSolutions = [];
  }

  // TODO: REVIEW & CLEAN STUFF BELOW:
  componentDidMount() {
    this.businessCaseErrorSubscription = businessCaseStore.subscribe(
      BUSINESS_CASE_ERROR_EVENT,
      (state) => {
        toast.error(state.message);
      },
    );
    this.businessCaseDetailSubscription = businessCaseStore.subscribe(
      BUSINESS_CASE_DETAIL_EVENT,
      (state) => {
        state.businessCase.recommendedSolutions = state.businessCase.recommendedSolutionsRelation.items.map(
          (solution) => {
            delete solution.__typename;
            return solution;
          },
        );
        this.originalRecommendedSolutions = state.businessCase.recommendedSolutions.concat();
        const newState = updateStateFromObject(this.state, state.businessCase);
        this.setState({
          ...newState,
          loading: false,
        });
      },
    );
    this.businessCaseUpdateSubscription = businessCaseStore.subscribe(
      BUSINESS_CASE_UPDATE_EVENT,
      (state) => {
        toast.success('Business Case Successfully Created');
        this.props.history.goBack();
      },
    );
    const { match } = this.props;
    if (!match.params.id) return toast.error('Business Case ID missing');

    fetchBusinessCase(match.params.id);
  }

  componentWillUnmount() {
    this.businessCaseErrorSubscription.unsubscribe();
    this.businessCaseDetailSubscription.unsubscribe();
    this.businessCaseUpdateSubscription.unsubscribe();
  }

  onSubmit = () => {
    this.setState({ loading: true }, () => {
      const data = R.clone(this.state);
      delete data.loading;
      updateBusinessCase(data, this.originalRecommendedSolutions);
    });
  };

  onChange = (name, value) => {
    this.setState({ [name]: value });
  };

  render() {
    const { loading } = this.state;

    let content = <BusinessCaseDetailTable isDisabled data={this.state} onChange={this.onChange} />;
    if (loading === true) content = <Loader stretch />;

    return (
      <React.Fragment>
        <Card.Header>
          <Heading type="h4" text="Business Case" />
        </Card.Header>
        <Card.Body borderRadius="all" style={{ padding: 0, textAlign: 'center' }}>
          {content}
        </Card.Body>
        <Card.Footer>
          <Button
            type="button"
            onClick={() =>
              this.props.history.push(`/document-management/business-cases/edit/${this.state.id}`)
            }
            text="Edit Business Case"
          />
          <Button
            type="button"
            onClick={() => this.props.history.push(`/document-management/business-cases/`)}
            text="Done"
          />
        </Card.Footer>
      </React.Fragment>
    );
  }
}

DetailBusinessCase.propTypes = {
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default withRouter(DetailBusinessCase);

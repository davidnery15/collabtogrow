import React, { Component } from 'react';
import { Card, Table, Dropdown, Menu, Button, Heading, Icon } from '@8base/boost';
import { DropdownBodyOnTable } from 'components/dropdown/DropdownBodyOnTable';
import * as toast from 'components/toast/Toast';
import businessCaseStore, {
  BUSINESS_CASE_LIST_EVENT,
  BUSINESS_CASE_ERROR_EVENT,
  BUSINESS_CASE_DELETE_EVENT,
} from './BusinessCase.store';
import { Link } from 'react-router-dom';
import * as R from 'ramda';
import { fetchBusinessCases, deleteBusinessCase } from './businessCases.actions';
import YesNoDialog from '../../../components/dialogs/YesNoDialog';
import { PropTypes } from 'prop-types';
import Moment from 'react-moment';
import Status from '../../../components/Status';
/**
 * List My Business Cases
 */

class ListBusinessCase extends Component {
  constructor(props) {
    super(props);
    this.state = {
      businessCases: [],
      loading: true,
      businessCaseId: 0,
      deleteModalIsOpen: false,
    };
  }

  componentDidMount() {
    this.businessCaseErrorSubscription = businessCaseStore.subscribe(
      BUSINESS_CASE_ERROR_EVENT,
      (state) => {
        toast.error(state.message);
        this.setState({
          loading: false,
        });
      },
    );
    this.businessCaseListSubscription = businessCaseStore.subscribe(
      BUSINESS_CASE_LIST_EVENT,
      (state) => {
        this.setState({
          businessCases: state.businessCasesList.items,
          loading: false,
        });
      },
    );
    this.businessCaseDeleteSubscription = businessCaseStore.subscribe(
      BUSINESS_CASE_DELETE_EVENT,
      () => fetchBusinessCases(),
    );
    fetchBusinessCases();
  }

  componentWillUnmount() {
    this.businessCaseErrorSubscription.unsubscribe();
    this.businessCaseListSubscription.unsubscribe();
  }

  onSelectForDelete = (id) => {
    this.setState({
      businessCaseId: id,
      deleteModalIsOpen: true,
    });
  };

  onYes = () => {
    deleteBusinessCase(this.state.businessCaseId).then(() =>
      this.setState({
        deleteModalIsOpen: false,
      }),
    );
  };

  onClose = () => {
    this.setState({
      deleteModalIsOpen: false,
    });
  };

  render() {
    const { deleteModalIsOpen } = this.state;
    return (
      <React.Fragment>
        <Card.Header>
          <Heading type="h4" text="Business Cases" />
        </Card.Header>
        <Card.Body borderRadius="all" style={{ padding: 0, textAlign: 'center' }}>
          <Table>
            <Table.Header columns="2fr 2fr 3fr 3fr 200px">
              <Table.HeaderCell>Code</Table.HeaderCell>
              <Table.HeaderCell>Status</Table.HeaderCell>
              <Table.HeaderCell>Date Created</Table.HeaderCell>
              <Table.HeaderCell>Created By</Table.HeaderCell>
            </Table.Header>
            <Table.Body loading={this.state.loading} data={this.state.businessCases}>
              {(businessCase) => (
                <Table.BodyRow
                  // style={{ cursor: 'pointer' }}
                  columns="2fr 2fr 3fr 3fr 200px"
                  key={businessCase.id}
                  // onClick={() => {
                  //   this.props.history.push(
                  //     `/document-management/business-cases/${businessCase.id}`,
                  //   );
                  // }}
                >
                  <Table.BodyCell>
                    {R.pathOr(
                      <span style={{ color: 'lightgrey' }}>Not Available</span>,
                      ['code'],
                      businessCase,
                    )}
                  </Table.BodyCell>
                  <Table.BodyCell>
                    <Status
                      status={R.pathOr(
                        <span style={{ color: 'lightgrey' }}>No Status</span>,
                        ['status'],
                        businessCase,
                      )}
                    />
                  </Table.BodyCell>
                  <Table.BodyCell>
                    <Moment format="MMMM Do, YYYY">
                      {R.pathOr(
                        <span style={{ color: 'lightgrey' }}>Not Available</span>,
                        ['createdAt'],
                        businessCase,
                      )}
                    </Moment>
                  </Table.BodyCell>
                  <Table.BodyCell>
                    {R.pathOr('', ['creator', 'firstName'], businessCase) +
                      ' ' +
                      R.pathOr('', ['creator', 'lastName'], businessCase)}
                  </Table.BodyCell>
                  <Table.BodyCell>
                    <Dropdown defaultOpen={false}>
                      <Dropdown.Head>
                        <Icon name="More" className="more-icon" />
                      </Dropdown.Head>
                      <DropdownBodyOnTable>
                        {({ closeDropdown }) => (
                          <Menu>
                            <Menu.Item
                              onClick={() => {
                                this.props.history.push(
                                  `/document-management/business-cases/${businessCase.id}`,
                                );
                              }}>
                              Details
                            </Menu.Item>
                            <Menu.Item
                              onClick={() => {
                                this.props.history.push(
                                  `/document-management/business-cases/edit/${businessCase.id}`,
                                );
                              }}>
                              Edit
                            </Menu.Item>
                            <Menu.Item
                              onClick={() => {
                                closeDropdown();
                                this.onSelectForDelete(businessCase.id);
                              }}>
                              Delete
                            </Menu.Item>
                          </Menu>
                        )}
                      </DropdownBodyOnTable>
                    </Dropdown>
                  </Table.BodyCell>
                </Table.BodyRow>
              )}
            </Table.Body>
            <Table.Footer justifyContent="center">
              <Link to="/document-management/business-cases/create/">
                <Button>Create Business Case</Button>
              </Link>
            </Table.Footer>
          </Table>
        </Card.Body>
        <YesNoDialog
          isOpen={deleteModalIsOpen}
          onYes={this.onYes}
          onNo={this.onClose}
          onClose={this.onClose}
          text={'Are you sure you want to Delete this Business Case?'}
          title={'Delete Business Case'}
        />
      </React.Fragment>
    );
  }
}

// // TODO:
ListBusinessCase.propTypes = { history: PropTypes.object.isRequired };

export default ListBusinessCase;

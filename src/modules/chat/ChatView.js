import '@cobuildlab/8base-chat/dist/8base-chat.css';

import React from 'react';
import * as R from 'ramda';
import PropTypes from 'prop-types';
import { withAuth } from '@8base/app-provider';
import ChatPlugin from '@cobuildlab/8base-chat';
import View from '@cobuildlab/react-flux-state';

import sessionStore, { NEW_SESSION_EVENT } from '../../shared/SessionStore';
import chatStore, { CHAT_TOGGLE_EVENT } from './chat-store';
import { toggleChatSideBar, onChannelsUnreadsChange } from './chat-actions';

class ChatView extends View {
  constructor(props) {
    super(props);

    this.state = {
      isSidebarOpen: false,
    };
  }

  componentDidMount() {
    this.chatToggleSubscription = this.subscribe(chatStore, CHAT_TOGGLE_EVENT, this.onChatToggle);
  }

  onChatToggle = (isSidebarOpen) => {
    this.setState({
      isSidebarOpen,
    });
  };

  getSectionsConfig = R.memoizeWith(R.identity, (selectedAllianceId) => ({
    channels: !!selectedAllianceId,
    contacts: true,
    dm: true,
  }));

  render() {
    const { auth } = this.props;
    const { selectedAlliance, user } = sessionStore.getState(NEW_SESSION_EVENT);

    const company = user.companyUserRelation.items[0] && user.companyUserRelation.items[0].company;
    const allianceId = selectedAlliance ? selectedAlliance.id : '';
    const sections = this.getSectionsConfig(allianceId);

    if (!company) {
      return null;
    }

    return (
      <ChatPlugin
        uri={process.env.REACT_APP_8BASE_API_ENDPOINT}
        workspaceId={process.env.REACT_APP_8BASE_WORKSPACE_ID}
        allianceId={allianceId}
        companyId={company.id}
        currentUser={user}
        authToken={auth.authState.token}
        sections={sections}
        isSidebarVisible={this.state.isSidebarOpen}
        onChangeSidebar={toggleChatSideBar}
        onChannelsUnreadsChange={onChannelsUnreadsChange}
        top="60px"
      />
    );
  }
}

ChatView.propTypes = {
  auth: PropTypes.object.isRequired,
};

export default withAuth(ChatView);

import Flux from '@cobuildlab/flux-state';
import chatStore, { CHAT_TOGGLE_EVENT, CHAT_UNREADS_EVENT } from './chat-store';
import { NOTIFICATIONS_REQUEST_EVENT } from '../notifications/notifications-store';

export const toggleChatSideBar = (newValue) => {
  const isSidebarOpen = chatStore.getState(CHAT_TOGGLE_EVENT);
  Flux.dispatchEvent(
    CHAT_TOGGLE_EVENT,
    typeof newValue !== 'undefined' ? newValue : !isSidebarOpen,
  );
  Flux.dispatchEvent(NOTIFICATIONS_REQUEST_EVENT, false);
};

export const onChannelsUnreadsChange = (channelMembers) => {
  setTimeout(() => {
    Flux.dispatchEvent(
      CHAT_UNREADS_EVENT,
      channelMembers.reduce((acc, item) => acc + item.hasUnreads, 0),
    );
  }, 0);
};

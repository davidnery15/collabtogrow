import moment from 'moment';

const currentDate = moment().format('YYYY-MM-DD');

export const RelatedItemModel = {
  approvalFunction: null,
  checked: '',
  createdAt: currentDate,
  id: null,
  itemId: null,
  name: '',
  nextSteps: null,
  originalDueDate: currentDate,
  owner: '',
  ownerObject: null,
  revisedDueDate: currentDate,
  status: '',
  type: '',
  url: '',
};

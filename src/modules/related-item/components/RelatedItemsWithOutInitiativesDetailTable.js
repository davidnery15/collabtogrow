import React from 'react';
import PropTypes from 'prop-types';
import DetailValue from '../../../components/DetailValue';
import DetailDateValue from '../../../components/DetailDateValue';
import Status from '../../../components/Status';
import UserDetailValue from '../../../components/UserDetailValue';
import { HorizontalLineText } from '../../../components/text/HorizontalLineText';
import H4 from '../../../components/text/H4';
import { Row } from '@8base/boost';
import Hr from '../../../components/text/Hr';

const RelatedItemsWithOutInitiativesDetailTable = (props) => {
  const { relatedItems } = props;
  let itemsComponent = 'No items selected';
  if (relatedItems.length > 0) {
    itemsComponent = (
      <table className="details">
        <tbody>
          <tr>
            <th>Name</th>
            <th>Owner</th>
            <th>Type</th>
            <th>Due Date</th>
            <th>Status</th>
          </tr>
          {relatedItems.map((item, i) => {
            const owner = item.ownerObject;
            return (
              <tr key={i}>
                <td>
                  <DetailValue text={item.name} />
                </td>
                <td>
                  <UserDetailValue user={owner} />
                </td>
                <td>
                  <DetailValue text={item.type} />
                </td>
                <td>
                  <DetailDateValue date={item.originalDueDate} />
                </td>
                <td>
                  <Status status={item.status} />
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }

  return (
    <>
      <HorizontalLineText>Related Items</HorizontalLineText>
      <Row>
        <H4 text={'Items'} />
      </Row>
      <Hr marginHorizontal={-24} />
      {itemsComponent}
    </>
  );
};

RelatedItemsWithOutInitiativesDetailTable.propTypes = {
  relatedItems: PropTypes.object.isRequired,
};

export { RelatedItemsWithOutInitiativesDetailTable };

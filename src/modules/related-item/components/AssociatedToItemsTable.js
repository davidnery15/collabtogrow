import React from 'react';
import PropTypes from 'prop-types';
import DetailValue from '../../../components/DetailValue';
import Status from '../../../components/Status';
import { HorizontalLineText } from '../../../components/text/HorizontalLineText';
import { Table } from '@8base/boost';
import { Link } from 'react-router-dom';
import Moment from 'react-moment';
import styled from '@emotion/styled';
import { ListCardBody } from 'components/card/ListCardBody';

const StyledListCardBody = styled(ListCardBody)`
  min-height: 20vh !important;
`;

const AssociatedToItemsTable = (props) => {
  const { associatedItems } = props;
  console.log(associatedItems);

  let itemsComponent = 'No Associated';

  if (associatedItems.length > 0) {
    itemsComponent = (
      <StyledListCardBody>
        <table className="details">
          <Table.Header className="justify-center-column" columns="3fr 3fr 3fr 3fr 3fr">
            <Table.HeaderCell className="name-column">Name</Table.HeaderCell>
            <Table.HeaderCell>Owner</Table.HeaderCell>
            <Table.HeaderCell>Type</Table.HeaderCell>
            <Table.HeaderCell>Due Date</Table.HeaderCell>
            <Table.HeaderCell>Status</Table.HeaderCell>
          </Table.Header>
          <Table.Body data={associatedItems} className="card-body-list-details">
            {(item, index) => {
              const { id, name, originalDueDate, type, status, owner } = item;
              const username = owner ? `${owner.firstName} ${owner.lastName}` : owner;
              return (
                <>
                  <Table.BodyRow
                    columns="3fr 3fr 3fr 3fr 3fr"
                    key={index}
                    className={'justify-center-column'}>
                    <Table.BodyCell className="justify-center-column">
                      <Link className="item-name" to={`/management/deal/${id}/`}>
                        {name}
                      </Link>
                    </Table.BodyCell>
                    <Table.BodyCell>
                      <DetailValue text={username} />
                    </Table.BodyCell>
                    <Table.BodyCell>
                      <DetailValue text={type} />
                    </Table.BodyCell>
                    <Table.BodyCell>
                      <Moment format="MMMM Do, YYYY">{originalDueDate}</Moment>
                    </Table.BodyCell>
                    <Table.BodyCell>
                      <Status status={status} />
                    </Table.BodyCell>
                  </Table.BodyRow>
                </>
              );
            }}
          </Table.Body>
        </table>
      </StyledListCardBody>
    );
  }

  return (
    <>
      <HorizontalLineText>Associated To Deals</HorizontalLineText>
      {itemsComponent}
    </>
  );
};

AssociatedToItemsTable.propTypes = {
  associatedItems: PropTypes.object.isRequired,
};

export { AssociatedToItemsTable };

import React from 'react';
import { Column, Row, Table } from '@8base/boost';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Header from '../../../components/Header';
import { RelatedItemRow } from './relatedItemRow';
import { ActionButton } from '../../../components/buttons/ActionButton';
import RelatedIdemDialog from './RelatedItemDialog';
import YesNoDialog from '../../../components/dialogs/YesNoDialog';

const StyledTable = styled('table')`
  width: 100%;
  text-align: center;
`;

class RelatedItemWithOutInitiativeForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      relatedItemDialogIsOpen: false,
      relatedItemDeleteDialogIsOpen: false,
      item: null,
    };
  }

  onChangeRelatedItems = (items) => {
    console.log(`DEBUG:onChangeRelatedItems:`, items);
    this.props.onChange('relatedItems', this.props.relatedItems.concat(items));
  };

  openRelatedItemDialog = () => {
    this.setState({ relatedItemDialogIsOpen: true });
  };

  closeRelatedItemDialog = () => {
    this.setState({ relatedItemDialogIsOpen: false });
  };

  openRelatedItemDeleteDialog = (i) => {
    const { relatedItems } = this.props;
    const item = relatedItems[i];
    this.setState({
      item,
      relatedItemDeleteDialogIsOpen: true,
    });
  };

  closeRelatedItemDeleteDialog = () => {
    this.setState({ item: null, relatedItemDeleteDialogIsOpen: false });
  };

  onDeleteRelatedItem = () => {
    let { relatedItems } = this.props;
    const { item } = this.state;
    let newRelatedItems = relatedItems.filter((relatedItem) => relatedItem.id !== item.id);
    this.setState(
      {
        relatedItemDeleteDialogIsOpen: false,
      },
      () => {
        this.props.onChange('relatedItems', newRelatedItems);
      },
    );
  };

  render() {
    const { relatedItems } = this.props;
    const { relatedItemDialogIsOpen, relatedItemDeleteDialogIsOpen } = this.state;
    const filteredIds = relatedItems.map((item) => item.itemId);

    return (
      <>
        <Row growChildren gap="lg">
          <Column stretch>
            <Header text={'Items'} />
            <StyledTable>
              <Table.Body data={relatedItems}>
                {relatedItems.map((item, i) => (
                  <RelatedItemRow
                    data={item}
                    key={i}
                    index={i}
                    onClose={this.openRelatedItemDeleteDialog}
                  />
                ))}
              </Table.Body>
              <Table.Footer justifyContent="center">
                <ActionButton text={'Add Relation Item'} onClick={this.openRelatedItemDialog} />
              </Table.Footer>
            </StyledTable>
          </Column>
          <RelatedIdemDialog
            filteredIds={filteredIds}
            onClose={this.closeRelatedItemDialog}
            onRelatedItems={this.onChangeRelatedItems}
            isOpen={relatedItemDialogIsOpen}
          />
          <YesNoDialog
            onYes={this.onDeleteRelatedItem}
            onNo={this.closeRelatedItemDeleteDialog}
            title={'Delete Related Item'}
            text={'Do you really want to Delete this relationship?'}
            isOpen={relatedItemDeleteDialogIsOpen}
          />
        </Row>
      </>
    );
  }
}

RelatedItemWithOutInitiativeForm.propTypes = {
  relatedItems: PropTypes.array.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default RelatedItemWithOutInitiativeForm;

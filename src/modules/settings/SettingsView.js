import React from 'react';
import { Card, Grid, SecondaryNavigation } from '@8base/boost';
import { Route, Switch, Redirect } from 'react-router-dom';
import './css/settings.css';
/* Alliance */
import AllianceCreateView from './alliance-management/AllianceCreateView';
import AllianceEditView from './alliance-management/AllianceEditView';
import AllianceDetailView from './alliance-management/AllianceDetailView';
import AllianceListView from './alliance-management/AllianceListView';
import AllianceMembersView from './alliance-management/AllianceMembersView';
/* Company */
import CreateCompany from './company-management/CompanyCreateView';
import MyCompanyView from './company-management/MyCompanyView';
import EditCompany from './company-management/CompanyEditView';
import DetailCompany from './company-management/CompanyDetailView';
import CompanyMembersView from './company-management/CompanyMembersView';
/* User */
import './css/settings.css';
import sessionStore, { NEW_SESSION_EVENT } from '../../shared/SessionStore';
import UserProfileView from './user-profile/UserProfileView';
import { PropTypes } from 'prop-types';
import { LinkItem } from '../../components/link/LinkItem';
import { COMPANY_ADMINISTRATOR } from '../../shared/roles';
import CompanyListView from './company-management/CompanyListView';
/*Stage Mapping*/
import StageMapping from './stage-mapping/StageMappingView';

const SettingsView = (props) => {
  const { user } = sessionStore.getState(NEW_SESSION_EVENT);
  const { items } = user.companyUserRelation;
  const role = items.length !== 0 ? items[0].role.name : null;
  let companyLabel = 'Company Information';

  if (role === COMPANY_ADMINISTRATOR) {
    companyLabel = 'Company Management';
  }

  const validated = user.companyUserRelation.count > 0;
  const { location } = props;
  console.log('SettingsView', location);
  return (
    <Grid.Layout className="minHeightCard" columns="215px 1fr">
      <Grid.Box>
        <SecondaryNavigation>
          <LinkItem
            to={'/settings/company-management/my-company'}
            label={companyLabel}
            location={location}
            url={'company-management'}
          />
          {validated && (
            <LinkItem
              to={'/settings/alliance-management'}
              label={'Alliances'}
              location={location}
              url={'alliance-management'}
            />
          )}
          <LinkItem
            to={'/settings/user-profile'}
            label={'User Profile'}
            location={location}
            url={'user-profile'}
          />
          <LinkItem
            to={'/settings/stage-mapping'}
            label={'Stage Mapping'}
            location={location}
            url={'stage-mapping'}
          />
        </SecondaryNavigation>
      </Grid.Box>
      <Grid.Box>
        <Card stretch>
          {/* TODO: Why "exact" or not "exact"? */}
          <Switch>
            <Route path="/settings/alliance-management/create" component={AllianceCreateView} />
            <Route path="/settings/alliance-management/edit/:id" component={AllianceEditView} />
            <Route
              path="/settings/alliance-management/members/:id"
              component={AllianceMembersView}
            />
            <Route path="/settings/alliance-management/:id" component={AllianceDetailView} />
            <Route path="/settings/alliance-management" component={AllianceListView} />
            {/* Company Management */}
            <Route path="/settings/company-management/create" component={CreateCompany} />
            <Route path="/settings/company-management/my-company" component={MyCompanyView} />
            <Route path="/settings/company-management/company-list" component={CompanyListView} />
            <Route path="/settings/company-management/edit/:id" component={EditCompany} />
            <Route path="/settings/company-management/members/:id" component={CompanyMembersView} />
            <Route path="/settings/company-management/:id" component={DetailCompany} />
            {/*Stage Mapping*/}
            <Route path="/settings/stage-mapping" component={StageMapping} />
            {/** User Profile */}
            <Route exact path="/settings/user-profile/" component={UserProfileView} />
            {validated ? (
              <Redirect to="/settings/alliance-management" />
            ) : (
              <Redirect to="/settings/company-management/my-company" />
            )}
          </Switch>
        </Card>
      </Grid.Box>
    </Grid.Layout>
  );
};

SettingsView.propTypes = {
  location: PropTypes.any.isRequired,
};

export default SettingsView;

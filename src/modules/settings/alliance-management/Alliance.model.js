import { allianceKPIsYearModel } from './allianceKPIs/allianceKPIs-model';

export default {
  id: null,
  name: '',
  description: '',
  clientCompany: null,
  partnerCompany: null,
  currency: null,
  policyGuidelinesFile: null,
  organizationalChartFile: null,
  documents: [],
  invitationEmail: '',
  businessCase: { name: '' },
  allianceKPIs: allianceKPIsYearModel(),
  effectiveDate: null,
};

export const ALLIANCE_ORGANIZATIONAL_CHART_FILE = 'allianceOrganizationalChartFile';
export const ALLIANCE_POLICY_GUIDELINES_FILE = 'alliancePolicyGuidelinesFile';
export const ALLIANCE_DOCUMENTS_FILE = 'allianceDocuments';

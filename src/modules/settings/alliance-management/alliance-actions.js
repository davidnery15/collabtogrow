import {
  ALLIANCE_UPDATE_MUTATION,
  ALLIANCE_LIST_QUERY,
  ALLIANCE_SMALL_LIST_QUERY,
  ALLIANCE_DETAIL_QUERY,
  ALLIANCE_CREATE_MUTATION,
  ALLIANCE_FORM_DATA_QUERY,
  ALLIANCE_DELETE_MUTATION,
  ALLIANCE_APPROVALS_LIST_QUERY,
  ALLIANCE_APPROVAL_MUTATION,
  ALLIANCE_APPROVAL_UPDATE_MUTATION,
  ALLIANCE_MEMBERS_QUERY,
  ALLIANCE_USER_UPDATE_MUTATION,
  ALLIANCE_MEMBERS_WITH_ROLE_QUERY,
  CREATE_ALLIANCE_USER_MUTATION,
  CREATE_ALLIANCE_MEMBER_INVITATION_MUTATION,
  DELETE_ALLIANCE_USER_MUTATION,
  ALLIANCE_COMMENTS_QUERY,
  ALLIANCE_REACTIVATE_QUERY,
  CREATE_ALLIANCE_DELETED_MUTATION,
  ALLIANCE_DELETED_LIST_QUERY,
  DELETED_ALLIANCE_DELETED_MUTATION,
} from './alliance-queries';
import {
  COMMENT_CREATE_EVENT,
  COMMENT_ERROR_EVENT,
  COMMENT_REQUEST_EVENT,
  COMMENTS_EVENT,
} from '../../comment/comment-store';
import { COMMENTS_CREATE_MUTATION } from '../../comment/comment-queries';
import { ALLIANCE_TYPE } from '../../../shared/item-types';
import Flux from '@cobuildlab/flux-state';
import {
  ALLIANCE_LIST_EVENT,
  ALLIANCE_APPROVED_EVENT,
  ALLIANCE_ERROR_EVENT,
  ALLIANCE_CREATE_EVENT,
  ALLIANCE_DETAIL_EVENT,
  ALLIANCE_UPDATE_EVENT,
  ALLIANCE_REJECT_EVENT,
  ALLIANCE_FORM_DATA_EVENT,
  ALLIANCE_DELETE_EVENT,
  CREATE_ALLIANCE_MEMBER_EVENT,
  INVITE_ALLIANCE_MEMBER_EVENT,
  // ALLIANCE_ROLE_CHANGE_EVENT,
  ALLIANCE_DETAIL_LIST_MEMBERS_EVENT,
  ALLIANCE_LIST_MEMBERS_EVENT,
  ALLIANCE_ROLE_CHANGE_EVENT,
  ALLIANCE_LIST_MEMBERS_WITH_ROLES_EVENT,
  DELETE_ALLIANCE_USER_EVENT,
  ALLIANCE_COMPLETED_EVENT,
  ALLIANCE_REACTIVATE_EVENT,
  ALLIANCE_DELETED_EVENT,
  ALLIANCE_DELETED_LIST_EVENT,
  CANCEL_ALLIANCE_DELETED_EVENT,
  ALLIANCE_SUBMIT_FOR_APPROVAL_EVENT,
  ALLIANCE_RESTORE_EVENT,
  ALLIANCE_MEMBER_INVITATIONS_LIST_EVENT,
} from './alliance-store';
import sessionStore, { APOLLO_CLIENT, NEW_SESSION_EVENT } from '../../../shared/SessionStore';
import { IntegrityError } from '../../../shared/errors';
import { META_ALLIANCE_SELECTED } from '../../../shared';
import {
  sanitize8BaseReference,
  sanitize8BaseReferenceFromObject,
  sanitize8BaseDocumentCreate,
  sanitize8BaseDocumentsCreate,
  sanitize8BaseDocumentUpdate,
  sanitize8BaseDocumentsDeleteAndUpdate,
} from '../../../shared/utils';
import { isValidString } from '../../../shared/validators';
import {
  createAllianceValidator,
  createAllianceMemberInvitationValidator,
  createAllianceMemberValidator,
  requestApprovalAllianceValidator,
  validateAllianceMemberInvitation,
} from './alliance-validators';
import { businessCaseValidator } from '../../document-management/business-case/business-case-validators';
import { fetchRoles } from '../../dashboard/dashboard-actions';
import {
  sanitizeRecommendedSolutions,
  updateBusinessCase,
  businessCaseSearchFilterOR,
} from '../../document-management/business-case/businessCases.actions';
import { fetchUser } from '../../auth/auth.actions';
import {
  ALLIANCE_SUBMITTED_FOR_APPROVAL,
  ALLIANCE_APPROVAL_PENDING,
  ALLIANCE_APPROVAL_APPROVED,
  ALLIANCE_APPROVAL_REJECTED,
  ALLIANCE_APPROVED,
  ALLIANCE_REJECTED,
  ALLIANCE_COMPLETED,
  INITIATIVE_APPROVED,
  ALLIANCE_IN_PROGRESS,
} from '../../../shared/status';
import { ALLIANCE_ADMINISTRATOR, ALLIANCE_SER, COMPANY_ADMINISTRATOR } from '../../../shared/roles';
import { addPortfolioOwnersAsAllianceCollaborator } from '../invitations/invitations.actions';
import { log, error } from '@cobuildlab/pure-logger';
import { INITIATIVE_CREATE_MUTATION } from '../../management/initiative/initiative-queries';
import { ACTION_CREATE_MUTATION } from '../../management/action/action-queries';
import moment from 'moment';
import { ALLIANCE_MEMBER_INVITATIONS_QUERY } from '../invitations/invitations.queries';
import { sanitizeAllianceKPIsCreate } from './allianceKPIs/allianceKPIs-actions';
import * as R from 'ramda';
import {
  allianceKPIsToBeCreated,
  deleteAllianceKPIs,
  updateAllianceKPIs,
  sanitizeAllianceKPIs,
} from './allianceKPIs/allianceKPIs-actions';

/**
 * Creates a filter object and search by a string
 on string properties of the alliance.
 *
 * @param {string} [search=''] - The string to search.
 * @param {string} userId -      The userId to filter.
 * @param status
 * @returns {object}             The filter object.
 */
const allianceFilter = (userId, search = '', status) => {
  const filter = {
    allianceUserAllianceRelation: {
      some: { companyUser: { user: { id: { equals: userId } } } },
    },
    OR: [
      {
        name: {
          contains: search,
        },
      },
      {
        description: {
          contains: search,
        },
      },
      {
        owner: {
          firstName: { contains: search },
        },
      },
      {
        owner: {
          lastName: { contains: search },
        },
      },
      {
        owner: {
          email: { contains: search },
        },
      },
    ].concat(businessCaseSearchFilterOR(search)),
  };
  if (status) {
    filter.status = { equals: status };
  }

  return filter;
};

/**
 * Approve and Alliance.
 *
 * @param {Alliance} alliance
 */
export const approveAlliance = async (alliance) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const user = sessionStore.getState(NEW_SESSION_EVENT).user;
  // We fetch the user again just in case the Role has changed
  const {
    user: { companyUserRelation },
  } = sessionStore.getState(NEW_SESSION_EVENT);

  // We fetch the approvals to see what's gonna happen
  const { allianceApprovalsList } = await fetchAllianceApprovals(alliance);
  if (allianceApprovalsList.count < 2) {
    return Flux.dispatchEvent(
      ALLIANCE_ERROR_EVENT,
      new IntegrityError('Alliance must have have at least 2 request for approvals'),
    );
  }

  let approvalsCounter = 0;
  let approvalId = null;
  const userCompanyIds = companyUserRelation.items.map((item) => item.company.id);

  allianceApprovalsList.items.forEach((approval) => {
    if (approval.status === ALLIANCE_APPROVED) approvalsCounter++;

    if (userCompanyIds.includes(approval.company.id)) approvalId = approval.id;
  });
  if (approvalId === null) {
    return Flux.dispatchEvent(
      ALLIANCE_ERROR_EVENT,
      new IntegrityError("You can't approve this Alliance, your Company does not belong here."),
    );
  }

  //
  const mutation = {
    mutation: ALLIANCE_APPROVAL_UPDATE_MUTATION,
    variables: {
      approval: {
        id: approvalId,
        status: ALLIANCE_APPROVAL_APPROVED,
        dateOfResponse: new Date(),
        approvedBy: { connect: { id: user.id } },
      },
    },
  };

  if (approvalsCounter > 0) {
    mutation.mutation = ALLIANCE_APPROVAL_MUTATION;
    mutation.variables.alliance = {
      id: alliance.id,
      status: ALLIANCE_APPROVED,
    };
  }

  let response;
  try {
    response = await client.mutate(mutation);
  } catch (e) {
    error('approveAlliance', e);
    return Flux.dispatchEvent(ALLIANCE_ERROR_EVENT, e);
  }
  log('approveAlliance', response);
  Flux.dispatchEvent(ALLIANCE_UPDATE_EVENT, response.data);
  return response.data;
};

/**
 * Reject and Alliance.
 *
 * @param alliance - Alliance.
 */
export const rejectAlliance = async (alliance) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  // We fetch the user again just in case the Role has changed
  const { companyUserRelation } = await fetchUser();

  // We fetch the approvals to see what's gonna happen
  const { allianceApprovalsList } = await fetchAllianceApprovals(alliance);
  if (allianceApprovalsList.count < 2) {
    return Flux.dispatchEvent(
      ALLIANCE_ERROR_EVENT,
      new IntegrityError('Alliance must have have at least 2 request for approvals'),
    );
  }

  let approvalId = null;
  let alliedCompanyIndex = null;
  const userCompanyIds = companyUserRelation.items.map((item) => item.company.id);

  allianceApprovalsList.items.forEach((approval, index) => {
    if (userCompanyIds.includes(approval.company.id)) {
      approvalId = approval.id;
    } else {
      alliedCompanyIndex = index;
    }
  });

  if (approvalId === null) {
    return Flux.dispatchEvent(
      ALLIANCE_ERROR_EVENT,
      new IntegrityError("You can't reject this Alliance, your Company does not belong here."),
    );
  }

  const userCompanyMutation = {
    mutation: ALLIANCE_APPROVAL_MUTATION,
    variables: {
      approval: {
        id: approvalId,
        status: ALLIANCE_APPROVAL_REJECTED,
        dateOfResponse: new Date(),
      },
      alliance: {
        id: alliance.id,
        status: ALLIANCE_REJECTED,
      },
    },
  };

  // We also set the partners approval to REJECTED
  const alliedCompanyMutation = {
    mutation: ALLIANCE_APPROVAL_UPDATE_MUTATION,
    variables: {
      approval: {
        id: allianceApprovalsList.items[alliedCompanyIndex].id,
        status: ALLIANCE_APPROVAL_REJECTED,
        dateOfResponse: new Date(),
      },
    },
  };

  let response;
  try {
    // Set the Alliance and the User's company approval status to REJECTED
    response = await client.mutate(userCompanyMutation);
    // Also set the Allied company's approval status to REJECTED
    await client.mutate(alliedCompanyMutation);
  } catch (e) {
    error('rejectAlliance', e);
    return Flux.dispatchEvent(ALLIANCE_ERROR_EVENT, e);
  }
  log('rejectAlliance', response);
  Flux.dispatchEvent(ALLIANCE_REJECT_EVENT, response.data);
  return response.data;
};

/**
 * Request an Approval for an Alliance.
 *
 * @param alliance - The id of the Alliance to be deleted.
 */
export const requestApprovalForAlliance = async (alliance) => {
  const client = sessionStore.getState(APOLLO_CLIENT);

  try {
    requestApprovalAllianceValidator(alliance, alliance.businessCase);
  } catch (e) {
    error('requestApprovalForAlliance', e);
    return Flux.dispatchEvent(ALLIANCE_ERROR_EVENT, e);
  }

  const data = {
    id: alliance.id,
    status: 'SUBMITTED FOR APPROVAL',
    allianceApprovalRelation: {
      create: [
        { company: { connect: { id: alliance.clientCompany.id } } },
        { company: { connect: { id: alliance.partnerCompany.id } } },
      ],
    },
  };

  let response;
  try {
    response = await client.mutate({
      mutation: ALLIANCE_UPDATE_MUTATION,
      variables: { data },
    });
  } catch (e) {
    error('requestApprovalForAlliance', e);
    return Flux.dispatchEvent(ALLIANCE_ERROR_EVENT, e);
  }
  log('requestApprovalForAlliance', response);
  Flux.dispatchEvent(ALLIANCE_SUBMIT_FOR_APPROVAL_EVENT, response.data);
  return response.data;
};

/**
 * Delete an Alliance.
 *
 * @param alliance - The alliance to be deleted.
 */
export const deleteAlliance = async (alliance) => {
  const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
  const client = sessionStore.getState(APOLLO_CLIENT);

  if (selectedAlliance.id === alliance.id) {
    return Flux.dispatchEvent(
      ALLIANCE_ERROR_EVENT,
      new IntegrityError(`You can't delete the selected Alliance`),
    );
  }

  let response;
  try {
    response = await client.mutate({
      mutation: ALLIANCE_DELETE_MUTATION,
      variables: { data: { id: alliance.id, force: true } },
    });
  } catch (e) {
    error('deleteAlliance', e);
    return Flux.dispatchEvent(ALLIANCE_ERROR_EVENT, e);
  }
  log('deleteAlliance', response);
  Flux.dispatchEvent(ALLIANCE_DELETE_EVENT, response.data);
  return response.data;
};

/**
 * Update an Alliance.
 *
 * @param allianceData - The Updated Alliance.
 * @param businessCaseData - The Updated BusinessCase.
 * @param originalRecommendedSolutions - The Original Recommended Solutions.
 * @param originalAllianceKPIs
 * @param originalDocuments
 */
export const updateAlliance = async (
  allianceData,
  businessCaseData,
  originalRecommendedSolutions,
  originalAllianceKPIs,
  originalDocuments,
) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  delete allianceData.owner; // Unnecessary Update, The user owner will always be the same

  try {
    createAllianceValidator(allianceData);
    businessCaseValidator(businessCaseData);
  } catch (e) {
    error('UpdateAlliance Validator', e);
    return Flux.dispatchEvent(ALLIANCE_ERROR_EVENT, e);
  }

  log('UpdateAlliance AllianceData', allianceData);
  log('UpdateAlliance businessCaseData', businessCaseData);
  log('UpdateAlliance originalRecommendedSolutions', originalRecommendedSolutions);

  // Invitation Email
  if (isValidString(allianceData.invitationEmail)) {
    allianceData.allianceInvitationRelation = {
      create: {
        email: allianceData.invitationEmail,
      },
    };
  }
  delete allianceData.invitationEmail;

  sanitize8BaseReferenceFromObject(allianceData, 'currency');
  sanitize8BaseReferenceFromObject(allianceData, 'clientCompany');
  sanitize8BaseReferenceFromObject(allianceData, 'partnerCompany');
  sanitize8BaseReference(allianceData, 'businessCase');
  sanitize8BaseDocumentUpdate(allianceData, 'policyGuidelinesFile');
  sanitize8BaseDocumentUpdate(allianceData, 'organizationalChartFile');
  sanitize8BaseDocumentsDeleteAndUpdate(allianceData, 'documents', originalDocuments);

  // AllianceKPIs to be created
  sanitizeAllianceKPIs(allianceData);
  const allianceKPIs = R.clone(allianceData.allianceKPIs);
  const kpisToBeCreated = allianceKPIsToBeCreated(allianceKPIs);
  allianceData.allianceKPIAllianceRelation = { create: kpisToBeCreated };
  delete allianceData.allianceKPIs;

  // update and delete allianceKPIs
  let deletedKPIs;
  let updatedKPIs;
  try {
    deletedKPIs = await deleteAllianceKPIs(allianceKPIs, originalAllianceKPIs);
    updatedKPIs = await updateAllianceKPIs(allianceKPIs, originalAllianceKPIs);
  } catch (e) {
    error('updateDeleteAllianceKPIs', e);
    return Flux.dispatchEvent(ALLIANCE_ERROR_EVENT, e);
  }

  if (
    deletedKPIs.length ||
    updatedKPIs.length ||
    (kpisToBeCreated.length && allianceData.status !== ALLIANCE_COMPLETED)
  ) {
    if (allianceData.status === ALLIANCE_APPROVED) {
      allianceData.status = ALLIANCE_SUBMITTED_FOR_APPROVAL;
    }

    try {
      await resetApprovalsAsPending(allianceData);
    } catch (e) {
      return Flux.dispatchEvent(ALLIANCE_ERROR_EVENT, e);
    }
  }

  let response;
  try {
    response = await client.mutate({
      mutation: ALLIANCE_UPDATE_MUTATION,
      variables: { data: allianceData },
    });
  } catch (e) {
    console.log('updateAllianceError', e, allianceData);
    return Flux.dispatchEvent(ALLIANCE_ERROR_EVENT, e);
  }
  log('updateAlliance', allianceData, response);

  try {
    await updateBusinessCase(businessCaseData, originalRecommendedSolutions);
  } catch (e) {
    error('updateAlliance', e, allianceData);
    return Flux.dispatchEvent(ALLIANCE_ERROR_EVENT, e);
  }

  Flux.dispatchEvent(ALLIANCE_UPDATE_EVENT, response.data);
  return response.data;
};

/**
 * Reset alliance approvals to PENDING status
 * To use on updateAlliance when the alliance need to be reApproved
 * // Must set the alliance to SUBMITTED_FOR_APPROVAL after using this.
 *
 * @param  {Alliance}  alliance
 * @returns {Promise}
 */
export const resetApprovalsAsPending = async (alliance) => {
  log('resetApprovalsAsPending', alliance);
  const client = sessionStore.getState(APOLLO_CLIENT);

  if (alliance.status === ALLIANCE_COMPLETED) {
    throw new IntegrityError(`A Completed Alliance can't be submitted for approval`);
  }

  const { allianceApprovalsList } = await fetchAllianceApprovals(alliance);

  const updateApprovalPromise = async (approval) => {
    let response;
    try {
      response = await client.mutate({
        mutation: ALLIANCE_APPROVAL_UPDATE_MUTATION,
        variables: {
          approval: {
            id: approval.id,
            status: ALLIANCE_APPROVAL_PENDING,
            dateOfResponse: null,
            approvedBy: null,
          },
        },
      });
    } catch (e) {
      throw e;
    }

    return response;
  };

  let responses;
  try {
    responses = await Promise.all(allianceApprovalsList.items.map(updateApprovalPromise));
  } catch (e) {
    error('resetApprovalsAsPending', e);
    throw e;
  }

  return responses;
};

/**
 * Fetch a single Alliance.
 *
 * @returns {Promise<void>}
 * @param id
 */
export const fetchAlliance = async (id) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  let response;
  try {
    response = await client.query({
      query: ALLIANCE_DETAIL_QUERY,
      variables: { id },
      fetchPolicy: 'network-only',
    });
  } catch (e) {
    error('fetchAlliance', e);
    Flux.dispatchEvent(ALLIANCE_ERROR_EVENT, e);
    throw e;
  }
  log('fetchAlliance', response);

  if (!response.data.alliance) {
    log('wtf');
    return Flux.dispatchEvent(
      ALLIANCE_ERROR_EVENT,
      new IntegrityError(`Alliance can not be found`),
    );
  }

  Flux.dispatchEvent(ALLIANCE_DETAIL_EVENT, response.data);
  return response.data;
};

/**
 * Fetches the Alliances for the User
 - Queries the Alliances in which I'm Owner.
 *
 * @returns {Promise<void>}
 * @param alliance
 */
export const fetchAllianceApprovals = async (alliance) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const data = { alliance: { id: { equals: alliance.id } } };

  let response;
  try {
    response = await client.query({
      query: ALLIANCE_APPROVALS_LIST_QUERY,
      variables: { data },
      fetchPolicy: 'network-only',
    });
  } catch (e) {
    error('fetchAllianceApprovals', e);
    return Flux.dispatchEvent(ALLIANCE_ERROR_EVENT, e);
  }
  log('fetchAllianceApprovals', response);
  // Flux.dispatchEvent(ALLIANCE_LIST_EVENT, response.data);
  return response.data;
};

/**
 * Fetches the Alliances for the User
 - Queries the Alliances in which I'm Owner.
 *
 * @returns {Promise<void>}
 * @param search
 * @param page
 * @param first
 * @param status
 */
export const fetchAlliances = async (search = '', page = 1, first = 20, status) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const user = sessionStore.getState(NEW_SESSION_EVENT).user;
  const skip = (page - 1) * first;
  const filter = allianceFilter(user.id, search, status);

  let response;
  try {
    response = await client.query({
      query: ALLIANCE_LIST_QUERY,
      variables: { data: filter, skip, first },
      fetchPolicy: 'network-only',
    });
    response.data.page = page;
    response.data.first = first;
    response.data.search = search;
  } catch (e) {
    error('fetchAlliances', e);
    return Flux.dispatchEvent(ALLIANCE_ERROR_EVENT, e);
  }
  log('fetchAlliances', response);
  Flux.dispatchEvent(ALLIANCE_LIST_EVENT, response.data);
  return response.data;
};

/**
 * Fetches the Approved Alliances for the User
 * - Queries the Alliances in which I'm Owner.
 *
 * @returns {Promise<void>}
 */
export const fetchApprovedAlliances = async () => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { user } = sessionStore.getState(NEW_SESSION_EVENT);
  const data = {
    status: { equals: ALLIANCE_APPROVED },
    allianceUserAllianceRelation: {
      some: { companyUser: { user: { id: { equals: user.id } } } },
    },
  };

  let response;
  try {
    response = await client.query({
      query: ALLIANCE_SMALL_LIST_QUERY,
      variables: { data },
      fetchPolicy: 'network-only',
    });
  } catch (e) {
    error('fetchApprovedAlliances', e);
    return Flux.dispatchEvent(ALLIANCE_ERROR_EVENT, e);
  }
  log('fetchApprovedAlliances', response);
  Flux.dispatchEvent(ALLIANCE_APPROVED_EVENT, response.data);
  return response.data;
};

/**
 * Creates a New Alliance
 - Sets the Owner to the current logged User
 - Sets the company Client as the Company who the logged User is Owner if Any
 - Creates an Invitation for the Alliance if invitationEmail is set.
 *
 * @returns {Promise<void>}
 * @param alliance
 * @param businessCase
 * @param companyRelation
 */
export const createAlliance = async (alliance, businessCase, companyRelation) => {
  log('createAlliance:', alliance, businessCase, companyRelation);
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { user } = sessionStore.getState(NEW_SESSION_EVENT);
  const { company, role } = companyRelation;

  delete alliance.id;
  delete businessCase.id;

  log('Company & role: ', company, role);
  if (role.name !== COMPANY_ADMINISTRATOR)
    return Flux.dispatchEvent(
      ALLIANCE_ERROR_EVENT,
      new IntegrityError('You need to own a Company to Create an Alliance'),
    );

  try {
    createAllianceValidator(alliance);
    businessCaseValidator(businessCase);
  } catch (e) {
    return Flux.dispatchEvent(ALLIANCE_ERROR_EVENT, e);
  }

  const roles = await fetchRoles();
  const allianceAdministratorRole = roles.find((role) => role.name === ALLIANCE_ADMINISTRATOR);

  // Invitation Email
  const invitationEmail = alliance.invitationEmail;
  delete alliance.invitationEmail;
  sanitize8BaseReferenceFromObject(alliance, 'clientCompany');
  log('createAlliance:sanitized:', alliance);
  const companyId = alliance.clientCompany.connect.id;

  // AllianceKPIs
  sanitizeAllianceKPIsCreate(alliance);
  // I'm the Owner of this alliance
  alliance.owner = { connect: { id: user.id } };
  alliance.allianceUserAllianceRelation = {
    create: [
      {
        companyUser: {
          connect: {
            id: companyRelation.id,
          },
        },
        role: {
          connect: {
            id: allianceAdministratorRole.id,
          },
        },
      },
    ],
  };

  sanitize8BaseReferenceFromObject(alliance, 'currency');
  sanitize8BaseDocumentCreate(alliance, 'policyGuidelinesFile');
  sanitize8BaseDocumentCreate(alliance, 'organizationalChartFile');
  sanitize8BaseDocumentsCreate(alliance, 'documents');

  // Business Case data
  businessCase.owner = { connect: { id: user.id } };
  sanitize8BaseDocumentCreate(businessCase, 'document');
  sanitizeRecommendedSolutions(businessCase);
  alliance.businessCase = {
    create: businessCase,
  };

  let response;
  try {
    response = await client.mutate({
      mutation: ALLIANCE_CREATE_MUTATION,
      variables: { data: alliance },
    });
  } catch (e) {
    error('createAlliance', e);
    return Flux.dispatchEvent(ALLIANCE_ERROR_EVENT, e);
  }
  log('createAlliance', response);

  // If there is a invitationEmail create the allianceInvitationRelation
  const allianceId = response.data.allianceCreate.id;
  if (isValidString(invitationEmail)) {
    const allianceData = {
      id: allianceId,
      allianceInvitationRelation: {
        create: {
          email: invitationEmail,
        },
      },
    };

    try {
      await client.mutate({
        mutation: ALLIANCE_UPDATE_MUTATION,
        variables: { data: allianceData },
      });
    } catch (e) {
      error('createAllianceInvitation', e, allianceData);
      return Flux.dispatchEvent(ALLIANCE_ERROR_EVENT, e);
    }
  }

  try {
    await createDefaultInitiativeAndActions(allianceId);
  } catch (e) {
    error('createDefaultInitiativeAndActions', e);
    return Flux.dispatchEvent(ALLIANCE_ERROR_EVENT, e);
  }

  try {
    await addPortfolioOwnersAsAllianceCollaborator(allianceId, companyId, roles);
  } catch (e) {
    error('addPortfolioOwnersAsAllianceCollaborator', e);
    return Flux.dispatchEvent(ALLIANCE_ERROR_EVENT, e);
  }

  Flux.dispatchEvent(ALLIANCE_CREATE_EVENT, response.data);
  return response.data;
};

/**
 * Used after alliance creation to create a default initiative a 6 actions.
 *
 * @returns {Promise<void>}
 * @param allianceId
 */
export const createDefaultInitiativeAndActions = async (allianceId) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const user = sessionStore.getState(NEW_SESSION_EVENT).user;

  const businessCaseData = {
    owner: user.id,
    expectedCosts: ['0'],
    anticipatedCosts: ['0'],
    expectedRevenues: ['0'],
  };
  sanitize8BaseReference(businessCaseData, 'owner');
  const initiativeData = {
    name: 'Alliance Setup Initiative',
    alliance: allianceId,
    owner: user.id,
    businessCase: { create: businessCaseData },
    status: INITIATIVE_APPROVED,
  };
  sanitize8BaseReference(initiativeData, 'owner');
  sanitize8BaseReference(initiativeData, 'alliance');

  log('createInitiativeForAlliance', initiativeData);
  let response;
  try {
    response = await client.mutate({
      mutation: INITIATIVE_CREATE_MUTATION,
      variables: { data: initiativeData },
    });
  } catch (e) {
    error('createInitiativeForAlliance', e);
    throw e;
  }
  log('createInitiativeForAlliance', response);
  const { initiativeCreate } = response.data;

  const actionsToCreate = [
    {
      name: 'Complete Your User Profile',
    },
    {
      name: 'Invite Your Colleagues',
    },
    {
      name: 'Invite Your Partner Company',
    },
    {
      name: 'Invite SER',
    },
    {
      name: 'Create Your Business Case',
    },
    {
      name: 'Submit your Alliance for Approval',
    },
  ];

  // action's originalDueDate
  const originalDueDate = moment()
    .add(8, 'days')
    .format('YYYY-MM-DD');
  const currentDate = moment().format('YYYY-MM-DD');

  const createActionPromise = async ({ name }) => {
    const action = {
      name,
      budgetUtilized: '0',
      unitMonetizationFactor: '0',
      owner: { connect: { id: user.id } },
      assignedTo: { connect: { id: user.id } },
      requestedBy: { connect: { id: user.id } },
      requestedDate: currentDate,
      assignedDate: currentDate,
      initiatives: { connect: [{ id: initiativeCreate.id }] },
      revisedDueDate: originalDueDate,
      originalDueDate,
      itemActionRelation: {
        create: {
          alliance: { connect: { id: allianceId } },
        },
      },
    };

    log('createActionForAlliance', action);
    let _response;
    try {
      _response = await client.mutate({
        mutation: ACTION_CREATE_MUTATION,
        variables: { data: action },
      });
      log('createActionForAlliance', _response);
    } catch (e) {
      error('createAction', e);
      throw e;
    }

    return _response;
  };

  const responses = await Promise.all(actionsToCreate.map(createActionPromise));

  log('createActionsForAlliance', responses);
  return responses;
};

/**
 * Fetches the Alliances Form Data.
 *
 * @returns {Promise<void>}
 */
export const fetchAllianceFormData = async () => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  // const user = sessionStore.getState(NEW_SESSION_EVENT).user;
  // const data = {user :{id: { equals: user.id}}};
  let response;
  try {
    response = await client.query({
      query: ALLIANCE_FORM_DATA_QUERY,
      fetchPolicy: 'network-only',
    });
  } catch (e) {
    error('fetchAllianceFormData', e);
    return Flux.dispatchEvent(ALLIANCE_ERROR_EVENT, e);
  }
  log('fetchAllianceFormData', response);
  Flux.dispatchEvent(ALLIANCE_FORM_DATA_EVENT, response.data);
  return response.data;
};

/**
 * Fetches the Current Alliance Members.
 *
 * @returns {Promise<void>}
 */
export const fetchCurrentAllianceMembersAction = async () => {
  log('fetchCurrentAllianceMembersAction');
  const user = sessionStore.getState(NEW_SESSION_EVENT).user;
  const metaList = user.metaRelation.items;
  const metaAlliance = metaList.find((meta) => meta.name === META_ALLIANCE_SELECTED);

  if (!metaAlliance && !isValidString(metaAlliance.value)) {
    error('fetchCurrentAllianceMembersAction', 'Must have an Alliance');
    return Flux.dispatchEvent(
      ALLIANCE_ERROR_EVENT,
      new IntegrityError('Must have an Active Alliance'),
    );
  }

  const response = await fetchAllianceMembersAction(metaAlliance.value);

  Flux.dispatchEvent(ALLIANCE_LIST_MEMBERS_EVENT, response);
  return response;
};

/**
 * Fetches the Alliance Members of specific alliance from my Company.
 *
 * @returns {Promise<void>}
 * @param allianceId
 */
export const fetchAllianceDetailMembersAction = async (allianceId) => {
  let response;
  try {
    response = await fetchAllianceMembersAction(allianceId);
  } catch (e) {
    return Flux.dispatchEvent(ALLIANCE_ERROR_EVENT, e);
  }

  Flux.dispatchEvent(ALLIANCE_DETAIL_LIST_MEMBERS_EVENT, response);
  return response;
};

/**
 * Fetches the Alliance Members from my Company.
 *
 * @returns {Promise<void>}
 * @param allianceId
 */
export const fetchAllianceMembersAction = async (allianceId) => {
  const client = sessionStore.getState(APOLLO_CLIENT);

  let response;
  try {
    response = await client.query({
      query: ALLIANCE_MEMBERS_QUERY,
      variables: { allianceId },
      fetchPolicy: 'network-only',
    });
  } catch (e) {
    error('fetchAllianceMembersAction', e);
    throw e;
  }

  if (!response.data.alliance || !response.data.alliance.allianceUserAllianceRelation) {
    throw new IntegrityError('Alliance Members can not be found');
  }

  return response.data.alliance;
};

/**
 * Change the role of a member.
 *
 * @param memberId
 * @param newRole
 *  @returns {Promise<void>}
 */
export const updateMemberRoleAction = async (memberId, newRole) => {
  const client = sessionStore.getState(APOLLO_CLIENT);

  const data = {};
  data.id = memberId;
  data.role = {
    connect: { id: newRole },
  };

  let response;

  try {
    response = await client.mutate({
      mutation: ALLIANCE_USER_UPDATE_MUTATION,
      variables: { data },
    });
  } catch (e) {
    error('updateMemberRoleAction', e);
    return Flux.dispatchEvent(ALLIANCE_ERROR_EVENT, e);
  }
  Flux.dispatchEvent(ALLIANCE_ROLE_CHANGE_EVENT, response.data);
  return response.data;
};

/**
 * Fetches the Alliance Members with their alliance roles.
 *
 * @returns {Promise<void>}
 * @param allianceId
 */
export const fetchAllianceMembersWithRoleAction = async (allianceId) => {
  const client = sessionStore.getState(APOLLO_CLIENT);

  const data = {
    alliance: {
      id: {
        equals: allianceId,
      },
    },
  };

  let response;
  try {
    response = await client.query({
      query: ALLIANCE_MEMBERS_WITH_ROLE_QUERY,
      variables: { data },
      fetchPolicy: 'network-only',
    });
  } catch (e) {
    error('fetchAllianceMembersWithRoleAction', e);
    return Flux.dispatchEvent(ALLIANCE_ERROR_EVENT, e);
  }

  log('fetchAllianceMembersWithRoleAction', response.data);
  Flux.dispatchEvent(ALLIANCE_LIST_MEMBERS_WITH_ROLES_EVENT, response.data);
  return response.data.alliance;
};

/**
 * Create the User Invitation.
 *
 * @param companyUserId
 * @param roleId
 * @param allianceId
 * @returns {Promise<void>}
 */
export const createAllianceMember = async (companyUserId, roleId, allianceId) => {
  const client = sessionStore.getState(APOLLO_CLIENT);

  try {
    createAllianceMemberValidator(companyUserId, roleId);
  } catch (err) {
    error('createAllianceMember', err);
    return Flux.dispatchEvent(ALLIANCE_ERROR_EVENT, err);
  }

  const data = {
    alliance: {
      connect: { id: allianceId },
    },
    role: {
      connect: { id: roleId },
    },
    companyUser: {
      connect: { id: companyUserId },
    },
  };

  log('createAllianceMember', data);

  let result;

  try {
    result = await client.mutate({
      mutation: CREATE_ALLIANCE_USER_MUTATION,
      variables: { data },
    });
  } catch (e) {
    error('createAllianceMember', e);
    return Flux.dispatchEvent(ALLIANCE_ERROR_EVENT, e);
  }

  log('createAllianceMember', result);

  Flux.dispatchEvent(CREATE_ALLIANCE_MEMBER_EVENT, result);

  return result;
};

/**
 * Delete an Alliance member.
 *
 * @param  {Member}   member -   The member to be deleted.
 */
export const deleteAllianceMember = async (member) => {
  const client = sessionStore.getState(APOLLO_CLIENT);

  let response;
  try {
    response = await client.mutate({
      mutation: DELETE_ALLIANCE_USER_MUTATION,
      variables: { data: { id: member.id, force: true } },
    });
  } catch (e) {
    error('deleteAllianceMember', e);
    return Flux.dispatchEvent(ALLIANCE_ERROR_EVENT, e);
  }
  log('deleteAllianceMember', response);
  Flux.dispatchEvent(DELETE_ALLIANCE_USER_EVENT, response.data);
  return response.data;
};

/**
 * Mark an alliance as completed.
 *
 * @param {Alliance} alliance
 * @returns {Promise<void>}
 */
export const completedAlliance = async (alliance) => {
  const client = sessionStore.getState(APOLLO_CLIENT);

  let response;
  try {
    response = await client.mutate({
      mutation: ALLIANCE_UPDATE_MUTATION,
      variables: {
        data: {
          id: alliance.id,
          status: ALLIANCE_COMPLETED,
        },
      },
    });
  } catch (e) {
    error('fetchApprovedAlliances', e);
    return Flux.dispatchEvent(ALLIANCE_ERROR_EVENT, e);
  }
  log('fetchApprovedAlliances', response);
  Flux.dispatchEvent(ALLIANCE_COMPLETED_EVENT, response.data);
  return response.data;
};

/**
 * Restore Alliance.
 *
 * @param {object}alliance - Alliance.
 * @returns {Promise<void|*>} Return promise.
 */
export const restoreAlliance = async (alliance) => {
  const client = sessionStore.getState(APOLLO_CLIENT);

  let response;
  try {
    response = await client.mutate({
      mutation: ALLIANCE_UPDATE_MUTATION,
      variables: {
        data: {
          id: alliance.id,
          status: ALLIANCE_IN_PROGRESS,
        },
      },
    });
  } catch (e) {
    error('fetchApprovedAlliances', e);
    return Flux.dispatchEvent(ALLIANCE_ERROR_EVENT, e);
  }
  log('fetchApprovedAlliances', response);
  Flux.dispatchEvent(ALLIANCE_RESTORE_EVENT, response.data);
  return response.data;
};

/**
 * Notifies a Request for Comments for a Alliance.
 */
export const openComments = ({ id: allianceId }) => {
  Flux.dispatchEvent(COMMENT_REQUEST_EVENT, { type: ALLIANCE_TYPE, id: allianceId });
};

/**
 * Create a comment on a Alliance.
 *
 * @param allianceId
 * @param comment
 * @returns {Promise<*>}
 */
export const createAllianceComment = async (allianceId, comment) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const data = {
    comment,
    allianceCommentsRelation: { connect: { id: allianceId } },
  };

  let response;
  try {
    response = await client.mutate({
      mutation: COMMENTS_CREATE_MUTATION,
      variables: { data },
    });
  } catch (e) {
    error('createAllianceComment', e);
    return Flux.dispatchEvent(COMMENT_ERROR_EVENT, e);
  }
  log('createAllianceComment', response);
  Flux.dispatchEvent(COMMENT_CREATE_EVENT, response.data);
  return response.data;
};

/**
 * Fetches the Alliance Item Comments.
 *
 * @returns {Promise<void>}
 * @param allianceId
 */
export const fetchAllianceComments = async (allianceId) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const data = { id: allianceId };
  const { user } = sessionStore.getState(NEW_SESSION_EVENT);

  let response;
  try {
    response = await client.query({
      query: ALLIANCE_COMMENTS_QUERY,
      variables: data,
      fetchPolicy: 'network-only',
    });
  } catch (e) {
    error('fetchAllianceComments', e);
    return Flux.dispatchEvent(COMMENT_ERROR_EVENT, e);
  }
  log('fetchAllianceComments', response);
  response.data.alliance.comments.userId = user.id;
  Flux.dispatchEvent(COMMENTS_EVENT, response.data.alliance.comments);
  return response.data.alliance.comments;
};

/**
 * Validate if user role is Alliance Administrator or Ser.
 *
 * @param allianceList
 * @returns {boolean}
 */
export const isAdminSerRole = (allianceList) => {
  const { user } = sessionStore.getState(NEW_SESSION_EVENT);

  const userInfo =
    allianceList.length !== 0
      ? allianceList.filter((item) => item.companyUser.user.id === user.id)
      : null;
  console.log('AllianceRole', userInfo[0].role.name);
  if (userInfo)
    return (
      userInfo[0].role.name === ALLIANCE_SER || userInfo[0].role.name === ALLIANCE_ADMINISTRATOR
    );

  return false;
};

/**
 * Create the AllianceMemberInvitation.
 *
 * @param email
 * @param roleId
 * @param allianceId
 * @returns {Promise<void>}
 */
export const createAllianceMemberInvitation = async (email, roleId, allianceId) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { user } = sessionStore.getState(NEW_SESSION_EVENT);
  const { company } = user.companyUserRelation.items[0];
  const companyId = company.id;

  try {
    createAllianceMemberInvitationValidator(email, roleId, allianceId);
  } catch (err) {
    error('createAllianceMemberInvitation', err);
    return Flux.dispatchEvent(ALLIANCE_ERROR_EVENT, err);
  }
  const { allianceMemberInvitationsList } = await fetchAllianceMemberInvitations(email, allianceId);

  try {
    validateAllianceMemberInvitation(allianceMemberInvitationsList);
  } catch (err) {
    error('validateAllianceMemberInvitation', err);
    return Flux.dispatchEvent(ALLIANCE_ERROR_EVENT, err);
  }
  const data = {
    email,
    alliance: {
      connect: { id: allianceId },
    },
    company: {
      connect: { id: companyId },
    },
    role: {
      connect: { id: roleId },
    },
  };
  log('createAllianceMemberInvitation', data);

  let result;
  try {
    result = await client.mutate({
      mutation: CREATE_ALLIANCE_MEMBER_INVITATION_MUTATION,
      variables: { data },
    });
  } catch (e) {
    error('createAllianceMemberInvitation', e);
    return Flux.dispatchEvent(ALLIANCE_ERROR_EVENT, e);
  }
  log('createAllianceMembeInvitationr', result);

  Flux.dispatchEvent(INVITE_ALLIANCE_MEMBER_EVENT, result);
  return result;
};

/**
 * Filter allianceMemberInvitations by email and alliance id.
 *
 * @param email
 * @param allianceId
 * @returns {Promise<void|*>}
 */
export const fetchAllianceMemberInvitations = async (email, allianceId) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const filter = {
    email: { equals: email },
    alliance: { id: { equals: allianceId } },
  };

  let result;

  try {
    result = await client.mutate({
      mutation: ALLIANCE_MEMBER_INVITATIONS_QUERY,
      variables: { filter },
    });
  } catch (e) {
    error('validateAllianceMemberInvitation', e);
    return Flux.dispatchEvent(ALLIANCE_ERROR_EVENT, e);
  }

  log('validateAllianceMemberInvitation', result);

  return result.data;
};

export const reactivateAlliance = async (allianceId) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const data = { id: allianceId, status: ALLIANCE_APPROVED };

  let result;

  try {
    result = await client.mutate({
      mutation: ALLIANCE_REACTIVATE_QUERY,
      variables: { data },
    });
  } catch (e) {
    error('reactivateAlliance', e);
    return Flux.dispatchEvent(ALLIANCE_ERROR_EVENT, e);
  }
  Flux.dispatchEvent(ALLIANCE_REACTIVATE_EVENT, result.data.allianceUpdate);

  log('reactivateAlliance', result);

  return result.data;
};
/**
 * Create alliance deleted.
 *
 * @param alliance
 * @returns {Promise<void|*>}
 */
export const createAllianceDeleted = async (alliance) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const data = {
    alliance: {
      connect: {
        id: alliance.id,
      },
    },
  };
  let response;
  try {
    response = await client.mutate({
      mutation: CREATE_ALLIANCE_DELETED_MUTATION,
      variables: { data },
    });
  } catch (e) {
    error('createAllianceDeleted', e);
    return Flux.dispatchEvent(ALLIANCE_ERROR_EVENT, e);
  }
  log('createAllianceDeleted', response);
  Flux.dispatchEvent(ALLIANCE_DELETED_EVENT, response.data);
  return response.data;
};
/**
 * Filter allianceDeletedList by alliance id.
 *
 * @param allianceId
 * @returns {Promise<void|*>}
 */
export const fetchAllianceDeletedList = async (allianceId) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const filter = {
    alliance: { id: { equals: allianceId } },
  };
  let response;

  try {
    response = await client.mutate({
      mutation: ALLIANCE_DELETED_LIST_QUERY,
      variables: { filter },
    });
  } catch (e) {
    error('allianceDeletedList', e);
    return Flux.dispatchEvent(ALLIANCE_ERROR_EVENT, e);
  }
  log('allianceDeletedList', response);
  Flux.dispatchEvent(ALLIANCE_DELETED_LIST_EVENT, response.data);
  return response.data;
};

/**
 * Dalete Alliance Deleted.
 *
 * @param allianceDeletedData
 * @returns {Promise<void|*>}
 */
export const deleteAllianceDeleted = async (allianceDeletedData) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const data = {
    id: allianceDeletedData.id,
  };
  let response;
  try {
    response = await client.mutate({
      mutation: DELETED_ALLIANCE_DELETED_MUTATION,
      variables: { data },
    });
  } catch (e) {
    error('deleteAllianceDeleted', e);
    return Flux.dispatchEvent(ALLIANCE_ERROR_EVENT, e);
  }
  log('deleteAllianceDeleted', response);
  Flux.dispatchEvent(CANCEL_ALLIANCE_DELETED_EVENT, response.data);
  return response.data;
};
/**
 * Filter by alliance Id and get alliance member invitation list.
 *
 * @param allianceId
 * @returns {Promise<void|*>}
 */
export const fetchAllianceMemberInvitationsList = async (allianceId) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const filter = {
    alliance: { id: { equals: allianceId } },
  };

  let result;

  try {
    result = await client.mutate({
      mutation: ALLIANCE_MEMBER_INVITATIONS_QUERY,
      variables: { filter },
    });
  } catch (e) {
    error('validateAllianceMemberInvitation', e);
    return Flux.dispatchEvent(ALLIANCE_ERROR_EVENT, e);
  }

  log('validateAllianceMemberInvitation', result);
  Flux.dispatchEvent(ALLIANCE_MEMBER_INVITATIONS_LIST_EVENT, result.data);

  return result.data;
};

import { ALLIANCE_SER, COMPANY_ADMINISTRATOR } from '../../../shared/roles';
import {
  getRoleOnAlliance,
  isUserAdminOrSERInAlliance,
  isUserAllianceAdmin,
  isUserFromClientCompany,
  isUserPendingApprovalOfAlliance,
} from '../../../shared/alliance-utils';

import {
  ALLIANCE_APPROVED,
  ALLIANCE_COMPLETED,
  ALLIANCE_SUBMITTED_FOR_APPROVAL,
} from '../../../shared/status';

/**
 * Checks if a User can Create a Alliance.
 *
 * @param {User} user - The user.
 * @param {Alliance} alliance - The Alliance.
 */

export const canCreateAlliance = (user) => {
  const userCompanyRoles = user.companyUserRelation.items.map((company) => company.role.name);

  return userCompanyRoles.includes(COMPANY_ADMINISTRATOR);
};

/**
 * Checks if a User can Completed a Alliance.
 *
 * @param {User} user - The user.
 * @param {Alliance} alliance - The Alliance.
 */

export const canCompletedAlliance = (user, alliance) => {
  if (isAllianceCompleted(alliance)) return false;

  // Only Alliance Admins or SER can Mark as Completed
  return isUserAdminOrSERInAlliance(user, alliance);
};

/**
 * Checks if a User can Restore a Alliance.
 *
 * @param {object} user - The user.
 * @param {object} alliance - The Alliance.
 * @returns {boolean}
 */
export const canRestoreAlliance = (user, alliance) => {
  if (!isAllianceCompleted(alliance)) return false;

  // Only Alliance Admins or SER can Mark as Completed
  return isUserAdminOrSERInAlliance(user, alliance);
};

export const canApproveAlliance = (user, alliance) => {
  if (isAllianceCompleted(alliance)) return false;

  // The Alliance must be Submitted for Approval
  if (alliance.status !== ALLIANCE_SUBMITTED_FOR_APPROVAL) return false;

  if (!isUserPendingApprovalOfAlliance(user, alliance)) return false;

  // Only Alliance Admins or SER can Approve an Alliance
  return isUserAdminOrSERInAlliance(user, alliance);
};

export const canChangeRole = (user, alliance) => {
  if (isAllianceCompleted(alliance)) return false;

  return isUserAllianceAdmin(user, alliance);
};

/**
 * Check if alliance complete and user is SER.
 *
 * @param user
 * @param alliance
 * @returns {boolean}
 */
export const canReactivateAlliance = (user, alliance) => {
  if (!isAllianceCompleted(alliance)) return false;

  const userRole = getRoleOnAlliance(user, alliance);
  if (!isUserFromClientCompany(user, alliance)) return false;
  return userRole.name === ALLIANCE_SER;
};

export const isAllianceCompleted = (alliance) => {
  return alliance.status === ALLIANCE_COMPLETED;
};

export const canDeleteAlliance = (user, alliance) => {
  if (!isAllianceCompleted(alliance)) return false;

  const userRole = getRoleOnAlliance(user, alliance);
  if (!isUserFromClientCompany(user, alliance)) return false;
  return userRole.name === ALLIANCE_SER;
};

export const canManageMembers = (user, alliance) => {
  if (isAllianceCompleted(alliance)) return false;

  return isUserAdminOrSERInAlliance(user, alliance);
};

/**
 * Checks if a User can Submit For Approval a Alliance.
 *
 * @param {User} user - The user.
 * @param {Alliance} alliance - The Alliance.
 */
export const canSubmitForApprovalAlliancePermission = (user, alliance) => {
  if (alliance.status === ALLIANCE_COMPLETED) return false;
  if (alliance.status === ALLIANCE_APPROVED) return false;
  if (alliance.status === ALLIANCE_SUBMITTED_FOR_APPROVAL) return false;

  // Only Alliance Admins or SER can Submit an Alliance for Approval
  return isUserAdminOrSERInAlliance(user, alliance);
};

/**
 * Checks if a User can Edit an Alliance.
 *
 * @param {User} user - The user.
 * @param {Alliance} alliance - The Alliance.
 */
export const canEditAlliancePermission = (user, alliance) => {
  if (isAllianceCompleted(alliance)) return false;

  return isUserAdminOrSERInAlliance(user, alliance);

  // if (alliance.status === ALLIANCE_APPROVED) return false;
  // if (alliance.status === ALLIANCE_SUBMITTED_FOR_APPROVAL) return false;

  // other new roles can edit here
};

/**
 * Checks if a User can Delete an Alliance.
 *
 * @param {User} user - The user.
 * @param {Alliance} alliance - The Alliance.
 */
export const canDeleteAlliancePermission = (user, alliance) => {
  if (isAllianceCompleted(alliance)) return false;

  if (alliance.status === ALLIANCE_APPROVED) return false;
  if (alliance.status === ALLIANCE_SUBMITTED_FOR_APPROVAL) return false;

  // Only Alliance Admins or SER can Delete the Alliance
  return isUserAdminOrSERInAlliance(user, alliance);
};

/**
 * Checks if a User can Delete an Alliance member.
 *
 * @param {User} user - The user.
 * @param {Alliance} alliance - The Alliance.
 */
export const canDeleteAllianceMemberPermission = (user, alliance) => {
  if (isAllianceCompleted(alliance)) return false;
  // Only Alliance Admins or SER can Remove an Alliance member
  return isUserAdminOrSERInAlliance(user, alliance);
};

import React from 'react';
import { InputField, Row, Heading, TextAreaField, Label } from '@8base/boost';
import PropTypes from 'prop-types';
import RecommendedSolutionDialog from './Dialog';
import FileInputComponent from '../../../../components/inputs/FileInputComponent';
const ALLIANCE_BUSINESS_DOCUMENT = 'allianceBusinessDocument';

class BusinessCaseForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      recommendedSolutionDialogOpen: false,
      recommendedSolutionPosition: -1,
      recommendedSolutionDescription: '',
    };
  }

  onAcceptRecommendedSolution = () => {
    console.log(this.state, this.props.data.recommendedSolutions);
    const description = this.state.recommendedSolutionDescription;
    if (description === '') return;

    const pos = this.state.recommendedSolutionPosition;
    const currentSolutions = this.props.data.recommendedSolutions;
    if (pos === -1) {
      // A new one
      currentSolutions.push({ description });
    } else currentSolutions[pos].description = description;
    this.props.onChange('recommendedSolutions', currentSolutions);
    this.setState({ recommendedSolutionDialogOpen: false });
  };

  onCancelRecommendedSolution = () => {
    this.setState({
      recommendedSolutionDialogOpen: false,
      recommendedSolutionPosition: -1,
      recommendedSolutionDescription: '',
    });
  };

  onChangeRecommendedSolution = (_, value) => {
    this.setState({ recommendedSolutionDescription: value });
  };

  onOpenRecommendedSolutionDialog = () => {
    this.setState({
      recommendedSolutionDialogOpen: true,
      recommendedSolutionPosition: -1,
      recommendedSolutionDescription: '',
    });
  };

  onEditRecommendedSolutionDialog = (pos) => {
    this.setState({
      recommendedSolutionDialogOpen: true,
      recommendedSolutionPosition: pos,
      recommendedSolutionDescription: this.props.data.recommendedSolutions[pos].description,
    });
  };

  onDeleteRecommendedSolutionDialog = (pos) => {
    const currentSolutions = this.props.data.recommendedSolutions;
    currentSolutions.splice(pos, 1);
    this.props.onChange('recommendedSolutions', currentSolutions);
  };

  componentWillUnmount() {
    super.componentWillUnmount();
    localStorage.removeItem(ALLIANCE_BUSINESS_DOCUMENT);
  }

  render() {
    const { data, onChange } = this.props;
    const {
      backgroundSummary,
      visionMissionStrategy,
      marketAnalysis,
      salesMarketingStrategy,
      expectedCost,
      expectedRevenue,
      anticipatedCost,
      risks,
      name,
      document,
    } = data;

    return (
      <React.Fragment>
        <Row>
          <div style={{ width: '50%', paddingRight: 25 }}>
            <InputField
              disabled
              stretch
              label="Name"
              input={{
                name: 'name',
                value: name,
                onChange: (value, e) => onChange(e.target.name, value),
              }}
            />
            <TextAreaField
              style={{ width: '100%', height: 152 }}
              label="Background Summary"
              input={{
                name: 'backgroundSummary',
                value: backgroundSummary,
                onChange: (value) => onChange('backgroundSummary', value),
              }}
            />
            <TextAreaField
              style={{ width: '100%', height: 152 }}
              label="Vision, Mission and Strategy"
              input={{
                name: 'visionMissionStrategy',
                value: visionMissionStrategy,
                onChange: (value) => onChange('visionMissionStrategy', value),
              }}
            />
            <TextAreaField
              style={{ width: '100%', height: 152 }}
              label="Market Analysis"
              input={{
                name: 'marketAnalysis',
                value: marketAnalysis,
                onChange: (value) => onChange('marketAnalysis', value),
              }}
            />
            <TextAreaField
              style={{ width: '100%', height: 152 }}
              label="Sales and Marketing Strategy"
              input={{
                name: 'salesMarketingStrategy',
                value: salesMarketingStrategy,
                onChange: (value) => onChange('salesMarketingStrategy', value),
              }}
            />
          </div>
          <div>
            <TextAreaField
              style={{ width: '100%', height: 152 }}
              label="Risks"
              input={{
                name: 'risks',
                value: risks,
                onChange: (value) => onChange('risks', value),
              }}
            />
            <Heading type="h4" text="Recommended Solutions" />
            {/* <Table >
              <Table.Header columns="repeat(1, 1fr) 40px">
                <Table.HeaderCell>Description</Table.HeaderCell>
              </Table.Header>
              <Table.Body data={recommendedSolutions}>
                {(solution, i) => (
                  <Table.BodyRow columns="repeat(1, 1fr) 40px" key={i}>
                    <Table.BodyCell>{solution.description}</Table.BodyCell>
                    <Table.BodyCell>
                      <Dropdown defaultOpen={false}>
                        <Dropdown.Head>
                          <Icon name="More" color="LIGHT_GRAY2" />
                        </Dropdown.Head>
                        <Dropdown.Body pin="right">
                          {({ closeDropdown }) => (
                            <Menu >
                              <Menu.Item
                                onClick={() => {
                                  this.onEditRecommendedSolutionDialog(i);
                                  closeDropdown();
                                }}>
                                Edit
                              </Menu.Item>
                              <Menu.Item
                                onClick={() => {
                                  this.onDeleteRecommendedSolutionDialog(i);
                                  closeDropdown();
                                }}>
                                Delete
                              </Menu.Item>
                            </Menu >
                          )}
                        </Dropdown.Body>
                      </Dropdown>
                    </Table.BodyCell>
                  </Table.BodyRow>
                )}
              </Table.Body>
              <Table.Footer justifyContent="center">
                <Button onClick={this.onOpenRecommendedSolutionDialog}>
                  Create Recommended Solution
                </Button>
              </Table.Footer>
            </Table > */}
            <InputField
              style={{ width: '100%' }}
              label="Anticipated Contributions"
              input={{
                name: 'anticipatedCost',
                value: anticipatedCost,
                onChange: (value, e) => onChange(e.target.name, value),
              }}
            />
            <InputField
              label="Expected Cost"
              input={{
                name: 'expectedCost',
                value: expectedCost,
                onChange: (value, e) => onChange(e.target.name, value),
              }}
            />
            <InputField
              label="Expected Bookings"
              input={{
                name: 'expectedRevenue',
                value: expectedRevenue,
                onChange: (value, e) => onChange(e.target.name, value),
              }}
            />
            <Label> Document</Label>
            <FileInputComponent
              onChange={onChange}
              nameInput={'document'}
              field={document}
              maxFiles={1}
              localKey={ALLIANCE_BUSINESS_DOCUMENT}
              text={'Upload Documents'}
            />
          </div>
        </Row>
        <RecommendedSolutionDialog
          onAccept={this.onAcceptRecommendedSolution}
          onCancel={this.onCancelRecommendedSolution}
          onChange={this.onChangeRecommendedSolution}
          description={this.state.recommendedSolutionDescription}
          isOpen={this.state.recommendedSolutionDialogOpen}
        />
      </React.Fragment>
    );
  }
}

BusinessCaseForm.propTypes = {
  data: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default BusinessCaseForm;

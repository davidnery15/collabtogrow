import React from 'react';
import { TextAreaField, Dialog, Grid, Button } from '@8base/boost';
import PropTypes from 'prop-types';

class SampleDialog extends React.Component {
  render() {
    const { onChange, description, onAccept, onCancel } = this.props;
    return (
      <Dialog size="sm" isOpen={this.props.isOpen}>
        <Dialog.Header title="Recommended Solution" onClose={onCancel} />
        <Dialog.Body scrollable>
          <Grid.Layout gap="sm" stretch>
            <Grid.Box>
              <TextAreaField
                label="Description"
                stretch
                input={{
                  name: 'description',
                  value: description,
                  onChange: (value) => onChange('description', value),
                }}
              />
            </Grid.Box>
          </Grid.Layout>
        </Dialog.Body>
        <Dialog.Footer>
          <Button color="neutral" variant="outlined" onClick={onCancel}>
            Cancel
          </Button>
          <Button color="red" onClick={onAccept} type="submit" text="Save" />
        </Dialog.Footer>
      </Dialog>
    );
  }
}

SampleDialog.propTypes = {
  onAccept: PropTypes.func.isRequired,
  onCancel: PropTypes.func,
  onChange: PropTypes.func.isRequired,
  description: PropTypes.string,
  isOpen: PropTypes.bool.isRequired,
};

export default SampleDialog;

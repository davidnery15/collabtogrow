import React from 'react';
import { InputField, Row, TextAreaField, Label, Column, DateInputField } from '@8base/boost';
import PropTypes from 'prop-types';
import { onErrorMixin } from '../../../../shared/mixins';
// import { truncateText } from '../../../../shared/utils';
import '../../css/alliance.css';
import CenterParagraph from '../../../../components/CenterParagraph';
import FileInputComponent from '../../../../components/inputs/FileInputComponent';
import { SelectInputById } from '../../../../components/forms/SelectInputById';
import {
  ALLIANCE_ORGANIZATIONAL_CHART_FILE,
  ALLIANCE_POLICY_GUIDELINES_FILE,
  ALLIANCE_DOCUMENTS_FILE,
} from '../Alliance.model';
import YesNoDialog from '../../../../components/dialogs/YesNoDialog';
import { HorizontalLine } from '../../../../components/new-ui/text/HorizontalLine';
import { HorizontalLineText } from '../../../../components/new-ui/text/HorizontalLineText';
import { GroupInputs } from '../../../../components/new-ui/inputs/GroupInputs';

/**
 * The Form for the Alliance Entity.
 *
 * @param pos
 * @param name
 */
class AllianceForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      deleteDocumentModalIsOpen: false,
      deleteDocumentPos: null,
    };
    this.onClickDelete = this.onClickDelete.bind(this);
    this.onChangeDocuments = this.onChangeDocuments.bind(this);
    this.nameInput = null;
  }

  onClickDelete = (pos, name) => {
    this.nameInput = name;
    this.setState({
      deleteDocumentModalIsOpen: true,
      deleteDocumentPos: pos,
    });
  };

  onYes = () => {
    this.setState(
      {
        deleteDocumentModalIsOpen: false,
      },
      () => {
        this.onDeleteDocuments(this.nameInput, this.state.deleteDocumentPos);
      },
    );
  };

  onClose = () => {
    this.setState({
      deleteDocumentModalIsOpen: false,
    });
  };

  onChangeDocuments(name, value) {
    const allianceData = this.props.data;

    if (name === 'documents') {
      for (let aux of value) {
        allianceData[name].push(aux);
      }
    } else {
      allianceData[name] = value;
    }

    this.props.onChange(name, allianceData[name]);
  }

  onDeleteDocuments = (name, pos) => {
    const allianceData = this.props.data;

    if (name === 'organizationalChartFile') {
      allianceData[name] = null;
      localStorage.setItem(ALLIANCE_ORGANIZATIONAL_CHART_FILE, JSON.stringify(allianceData[name]));
    } else if (name === 'policyGuidelinesFile') {
      allianceData[name] = null;
      localStorage.setItem(ALLIANCE_POLICY_GUIDELINES_FILE, JSON.stringify(allianceData[name]));
    } else if (name === 'documents') {
      allianceData[name].splice(pos, 1);
      localStorage.setItem(ALLIANCE_DOCUMENTS_FILE, JSON.stringify(allianceData[name]));
    }

    this.setState({ deleteDocumentPos: null });
    this.props.onChange(name, allianceData[name]);
  };

  render() {
    const { deleteDocumentModalIsOpen } = this.state;
    const { onChange, invitations, currencies, companyUsers } = this.props;
    const {
      name,
      description,
      policyGuidelinesFile,
      organizationalChartFile,
      invitationEmail,
      currency,
      documents,
      effectiveDate,
      clientCompany,
    } = this.props.data;
    return (
      <>
        <HorizontalLine>
          <HorizontalLineText text={'ALLIANCE'} />
        </HorizontalLine>
        <GroupInputs text={'General'}>
          <Row growChildren gap="lg">
            <Column>
              <InputField
                stretch
                label="Name"
                placeholder={'Name'}
                input={{
                  name: 'name',
                  value: name,
                  onChange: (value, e) => onChange(e.target.name, value),
                }}
              />
              <TextAreaField
                style={{ width: '100%', height: 152 }}
                label="Description"
                placeholder={'Description'}
                input={{
                  name: 'description',
                  value: description,
                  onChange: (value) => onChange('description', value),
                }}
              />
            </Column>
          </Row>
        </GroupInputs>
        <GroupInputs text={'Date & Currency'}>
          <Row growChildren gap="lg">
            <Column style={{ width: '35%' }}>
              <DateInputField
                label="Effective Date"
                input={{
                  name: 'effectiveDate',
                  value: effectiveDate,
                  onChange: (value) => onChange('effectiveDate', value),
                }}
              />
            </Column>
            <Column style={{ width: '65%' }}>
              <SelectInputById
                label="Currency"
                input={{
                  name: 'currency',
                  value: currency,
                  onChange: (value) => onChange('currency', value),
                }}
                meta={{}}
                placeholder="Currency of your Alliance"
                options={currencies.map((currency) => {
                  return { label: currency.shortName, value: currency };
                })}
              />
            </Column>
          </Row>
        </GroupInputs>
        <GroupInputs text={'Companies'}>
          <Row growChildren gap="lg">
            <Column>
              {companyUsers ? (
                <SelectInputById
                  label="Company"
                  input={{
                    name: 'clientCompany',
                    value: clientCompany,
                    onChange: (value) => onChange('clientCompany', value),
                  }}
                  meta={{}}
                  placeholder="Select your Company"
                  options={companyUsers.map((companyRelation) => {
                    return {
                      label: companyRelation.company.name,
                      value: companyRelation.company,
                    };
                  })}
                />
              ) : null}
            </Column>
          </Row>
        </GroupInputs>

        <Row growChildren gap="lg">
          <Column>
            <HorizontalLine>
              <HorizontalLineText text={'Documents'} />
            </HorizontalLine>
            <Row style={{ 'padding-bottom': '15px' }}>
              <Column alignItems="">
                <div style={{ 'margin-left': '30px' }}>
                  <Label kind="secondary" style={{ textAlign: 'left' }}>
                    Organizational Chart File
                  </Label>
                  <FileInputComponent
                    onChange={this.onChangeDocuments}
                    nameInput={'organizationalChartFile'}
                    field={organizationalChartFile}
                    maxFiles={1}
                    localKey={ALLIANCE_ORGANIZATIONAL_CHART_FILE}
                    text={'Upload Documents'}
                    onClickDelete={(pos) => this.onClickDelete(pos, 'organizationalChartFile')}
                  />
                </div>
              </Column>

              <Column alignItems="">
                <div style={{ 'margin-left': '30px' }}>
                  <Label kind="secondary" style={{ textAlign: 'left' }}>
                    Policy Guidelines File
                  </Label>
                  <FileInputComponent
                    onChange={this.onChangeDocuments}
                    nameInput={'policyGuidelinesFile'}
                    field={policyGuidelinesFile}
                    maxFiles={1}
                    localKey={ALLIANCE_POLICY_GUIDELINES_FILE}
                    text={'Upload Documents'}
                    onClickDelete={(pos) => this.onClickDelete(pos, 'policyGuidelinesFile')}
                  />
                </div>
              </Column>
            </Row>
            <Row style={{ 'padding-bottom': '15px' }}>
              <Column alignItems="">
                <div style={{ 'margin-left': '30px' }}>
                  <Label kind="secondary" style={{ textAlign: 'left' }}>
                    Documents
                  </Label>
                  <FileInputComponent 
                    onChange={this.onChangeDocuments}
                    nameInput={'documents'}
                    field={documents}
                    maxFiles={5}
                    localKey={ALLIANCE_DOCUMENTS_FILE}
                    text={'Upload Documents'}
                    onClickDelete={(pos) => this.onClickDelete(pos, 'documents')}
                  />
                </div>
              </Column>
            </Row>
          </Column>
        </Row>

        <HorizontalLine>
          <HorizontalLineText text={'INVITATION'} />
        </HorizontalLine>

        <Row growChildren gap="lg" style={{ paddingBottom: 15, maxWidth: 650, margin: 'auto' }}>
          <CenterParagraph>Invite your Alliance Partner to begin collaborating</CenterParagraph>
          <InputField
            style={{ width: '100%' }}
            label="E-Mail"
            disabled={invitations && invitations.length}
            input={{
              name: 'invitationEmail',
              value: invitationEmail,
              onChange: (value, e) => onChange(e.target.name, value),
            }}
          />
        </Row>
        <YesNoDialog
          isOpen={deleteDocumentModalIsOpen}
          onYes={this.onYes}
          onNo={this.onClose}
          onClose={this.onClose}
          text={'Are you sure you want to Delete this Document?'}
          title={'Delete Document'}
        />
      </>
    );
  }
}

Object.assign(AllianceForm.prototype, onErrorMixin);

AllianceForm.defaultProps = {
  invitations: [],
  companyUsers: null, // null for edit view
  companyId: null, // null for edit view
};

AllianceForm.propTypes = {
  data: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  currencies: PropTypes.array.isRequired,
  invitations: PropTypes.array,
  companyUsers: PropTypes.array,
};
AllianceForm.defaultProps = {
  invitations: [],
};

export default AllianceForm;

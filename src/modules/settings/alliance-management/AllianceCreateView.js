import React from 'react';
import { Card, Grid, Heading } from '@8base/boost';
import { CreateViewCardBody } from '../../../components/new-ui/card/CreateViewCardBody';
import AllianceForm from './components/AllianceForm';
import AllianceKPIsForm from './allianceKPIs/components/AllianceKPIsForm';
import allianceStore, {
  ALLIANCE_CREATE_EVENT,
  ALLIANCE_ERROR_EVENT,
  ALLIANCE_FORM_DATA_EVENT,
} from './alliance-store';
import * as toast from 'components/toast/Toast';
import { createAlliance, fetchAllianceFormData } from './alliance-actions';
import { fetchSession } from '../../auth/auth.actions';
import { Loader } from '@8base/boost';
import { withRouter } from 'react-router-dom';
import { PropTypes } from 'prop-types';
import AllianceModel, {
  ALLIANCE_ORGANIZATIONAL_CHART_FILE,
  ALLIANCE_POLICY_GUIDELINES_FILE,
  ALLIANCE_DOCUMENTS_FILE,
} from './Alliance.model';
import View from '@cobuildlab/react-flux-state';
import { onErrorMixin } from '../../../shared/mixins';
import * as R from 'ramda';
import BusinessCaseForm from '../../document-management/business-case/components/BusinessCaseForm';
import BusinessCaseModel from '../../document-management/business-case/BusinessCase.model';
import AllianceDetailTable from './components/AllianceDetailTable';
import SubHeader from '../../../components/SubHeader';
import BusinessCaseDetailTable from '../../document-management/business-case/components/BusinessCaseDetailTable';
import { ActionButton } from '../../../components/buttons/ActionButton';
import { TransparentButton } from '../../../components/buttons/TransparentButton';
import { FormSteps } from '../../../components/dots/FormSteps';
import { ActionButtonClose } from '../../../components/buttons/ActionButtonClose';
import { createAllianceValidator } from './alliance-validators';
import { businessCaseValidator } from '../../document-management/business-case/business-case-validators';
import { saveFormToSessionStorage } from '../../../shared/utils';
import { getMyCompanies } from '../company-management/company.actions';
import moment from 'moment';
import { checkActiveYearsAndSetStateMixin } from './allianceKPIs/allianceKPIs-actions';
import { allianceKPIsValidator } from './allianceKPIs/allianceKPIs-validators';
import { LeftProgressSection } from '../../../components/new-ui/LeftProgressSection';
import { SCREENS_ALLIANCE } from '../../management/screenView';
import { BoxCard } from '../../../components/new-ui/div/BoxCard';
import { CardFooter } from '../../../components/new-ui/card/CardFooter';

const ALLIANCE_DATA_STORE = 'allianceCreateView';

// Create Alliance
class AllianceCreateView extends View {
  constructor(props) {
    super(props);
    this.state = {
      allianceData: R.clone(AllianceModel),
      businessCaseData: R.clone(BusinessCaseModel),
      selectedKPIYear: moment().year(),
      kpiYears: [moment().year()],
      loading: true,
      currencies: [],
      step: 0,
    };
    this.companyUsers = getMyCompanies();
    this.onError = onErrorMixin.bind(this);
    this.checkActiveYearsAndSetStateMixin = checkActiveYearsAndSetStateMixin.bind(this);
  }

  onChangeAllianceData = (name, value) => {
    const { allianceData, businessCaseData } = this.state;
    allianceData[name] = value;
    this.checkActiveYearsAndSetStateMixin(allianceData, businessCaseData);

    const model = R.clone(AllianceModel);
    saveFormToSessionStorage(ALLIANCE_DATA_STORE, allianceData, model, [
      'policyGuidelinesFile',
      'organizationalChartFile',
      'documents',
    ]);
  };

  onChangeBusinessCaseData = (name, value) => {
    const { businessCaseData, allianceData } = this.state;
    businessCaseData[name] = value;
    this.checkActiveYearsAndSetStateMixin(allianceData, businessCaseData);
  };

  componentDidMount() {
    const { businessCaseData } = this.state;
    const allianceData = JSON.parse(sessionStorage.getItem(ALLIANCE_DATA_STORE));

    this.subscribe(allianceStore, ALLIANCE_ERROR_EVENT, this.onError);
    this.subscribe(allianceStore, ALLIANCE_FORM_DATA_EVENT, (state) => {
      this.setState({
        loading: false,
        currencies: state.currenciesList.items,
      });
    });
    this.subscribe(allianceStore, ALLIANCE_CREATE_EVENT, async (state) => {
      sessionStorage.removeItem(ALLIANCE_DATA_STORE);
      await fetchSession();
      toast.success('Alliance Successfully Created');
      this.props.history.goBack();
    });

    // set allianceData from sessionStorage
    if (allianceData) {
      this.checkActiveYearsAndSetStateMixin(allianceData, businessCaseData);
    }

    if (this.companyUsers.length) {
      const { allianceData } = this.state;
      allianceData.clientCompany = this.companyUsers[0].company;
    }

    fetchAllianceFormData();
  }

  componentWillUnmount() {
    super.componentWillUnmount();
    localStorage.removeItem(ALLIANCE_POLICY_GUIDELINES_FILE);
    localStorage.removeItem(ALLIANCE_ORGANIZATIONAL_CHART_FILE);
    localStorage.removeItem(ALLIANCE_DOCUMENTS_FILE);
  }

  onKPIYearChange = (selectedKPIYear) => {
    this.setState({ selectedKPIYear });
  };

  onSubmit = () => {
    this.setState({ loading: true }, () => {
      const allianceData = R.clone(this.state.allianceData);
      const businessCaseData = R.clone(this.state.businessCaseData);
      const companyId = allianceData.clientCompany ? allianceData.clientCompany.id : null;
      const companySelected = this.companyUsers.find((companyRelation) => {
        return companyRelation.company.id === companyId;
      });
      createAlliance(allianceData, businessCaseData, companySelected);
    });
  };

  onAllianceStepChange = (step) => {
    const allianceData = R.clone(this.state.allianceData);
    try {
      createAllianceValidator(allianceData);
    } catch (e) {
      return this.onError(e);
    }
    this.onScreen(step);
  };

  onBusinessCaseStepChange = (step) => {
    const businessCaseData = R.clone(this.state.businessCaseData);
    try {
      businessCaseValidator(businessCaseData);
    } catch (e) {
      return this.onError(e);
    }
    this.onScreen(step);
  };

  onKPIsStepChange = (step) => {
    const alliance = R.clone(this.state.allianceData);
    const businessCase = R.clone(this.state.businessCaseData);
    try {
      allianceKPIsValidator(alliance, businessCase);
    } catch (e) {
      return this.onError(e);
    }
    this.onScreen(step);
  };

  onScreen = (step) => {
    this.setState({ step });
  };

  render() {
    const {
      step,
      loading,
      currencies,
      allianceData,
      businessCaseData,
      selectedKPIYear,
      kpiYears,
    } = this.state;
    const { currency } = allianceData;
    const { history } = this.props;

    let content = <Loader stretch />;
    let footer = <></>;

    if (!loading && step === 0) {
      content = (
        <AllianceForm
          data={allianceData}
          onChange={this.onChangeAllianceData}
          onChangeCompany={this.onChangeCompany}
          currencies={currencies}
          companyUsers={this.companyUsers}
        />
      );
      footer = (
        <CardFooter>
          <ActionButton onClick={() => this.onAllianceStepChange(1)} text="Next" />
        </CardFooter>
      );
    }

    if (!loading && step === 1) {
      content = (
        <BusinessCaseForm
          data={businessCaseData}
          onChange={this.onChangeBusinessCaseData}
          currency={currency}
        />
      );
      footer = (
        <CardFooter>
          <ActionButton onClick={() => this.onBusinessCaseStepChange(2)} text="Next" />
          <TransparentButton onClick={() => this.onScreen(0)} text={'Previous'} />
        </CardFooter>
      );
    }

    if (!loading && step === 2) {
      // headerText = 'Key Performance Indicator';
      content = (
        <AllianceKPIsForm
          alliance={allianceData}
          onChange={this.onChangeAllianceData}
          onYearChange={this.onKPIYearChange}
          selectedYear={selectedKPIYear}
          kpiYears={kpiYears}
        />
      );
      footer = (
        <CardFooter>
          <ActionButton onClick={() => this.onKPIsStepChange(3)} text="Next" />
          <TransparentButton onClick={() => this.onScreen(1)} text={'Previous'} />
        </CardFooter>
      );
    }

    if (!loading && step === 3) {
      const allianceDetail = R.clone(allianceData);
      allianceDetail.allianceKPIAllianceRelation = {
        items: allianceDetail.allianceKPIs,
      };

      content = (
        <>
          <AllianceDetailTable data={allianceDetail} />
          <SubHeader text="Business Case" status={allianceDetail.status} />
          <BusinessCaseDetailTable data={businessCaseData} />
        </>
      );
      footer = (
        <CardFooter>
          <ActionButton onClick={this.onSubmit} text="Create Alliance" />
          <TransparentButton onClick={() => this.onScreen(2)} text={'Previous'} />
        </CardFooter>
      );
    }

    return (
      <React.Fragment>
        <Card.Header>
          <Grid.Layout
            columns="200px auto 200px"
            areas={[['left', 'center', 'right']]}
            style={{ width: '100%' }}>
            <Grid.Box area="left">
              <Heading type="h4" text="Create Alliance" />
            </Grid.Box>
            <Grid.Box area="center">
              <FormSteps totalSteps={4} step={step} />
            </Grid.Box>
            <Grid.Box area="right" />
          </Grid.Layout>
          <ActionButtonClose onClick={history.goBack} />
        </Card.Header>
        <CreateViewCardBody>
          <Grid.Layout columns="30% 70%" areas={[['left', 'right']]} style={{ width: '100%' }}>
            <Grid.Box area="left">
              <LeftProgressSection sections={SCREENS_ALLIANCE} currentScreen={step} />
            </Grid.Box>
            <BoxCard>
              <Grid.Box area="right">{content}</Grid.Box>
            </BoxCard>
          </Grid.Layout>
        </CreateViewCardBody>
        {footer}
      </React.Fragment>
    );
  }
}

AllianceCreateView.propTypes = {
  history: PropTypes.object.isRequired,
};

export default withRouter(AllianceCreateView);

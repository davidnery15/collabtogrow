import React from 'react';
import { Card, Heading, Dropdown, Menu, Icon, Loader, Grid } from '@8base/boost';
import allianceStore, {
  ALLIANCE_UPDATE_EVENT,
  ALLIANCE_DETAIL_EVENT,
  ALLIANCE_ERROR_EVENT,
  ALLIANCE_FORM_DATA_EVENT,
} from './alliance-store';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import AllianceForm from './components/AllianceForm';
import * as toast from 'components/toast/Toast';
import {
  fetchAlliance,
  updateAlliance,
  fetchAllianceFormData,
  openComments,
} from './alliance-actions';
import { deleteAllianceInvitation } from '../invitations/invitations.actions';
import invitationStore, { ALLIANCE_INVITATION_ERROR_EVENT } from '../invitations/invitations.store';
import AllianceModel, {
  ALLIANCE_ORGANIZATIONAL_CHART_FILE,
  ALLIANCE_POLICY_GUIDELINES_FILE,
  ALLIANCE_DOCUMENTS_FILE,
} from './Alliance.model';
import { updateStateFromObject } from '../../../shared/utils';
import * as R from 'ramda';
import View from '@cobuildlab/react-flux-state';
import { onErrorMixin } from '../../../shared/mixins';
import BusinessCaseForm from '../../document-management/business-case/components/BusinessCaseForm';
import BusinessCaseModel, {
  BUSINESS_CASE_DOCUMENT,
} from '../../document-management/business-case/BusinessCase.model';
import AllianceInvitationTable from './components/AllianceInvitationTable';
import YesNoDialog from '../../../components/dialogs/YesNoDialog';
import { log } from '@cobuildlab/pure-logger';
import AllianceDetailTable from './components/AllianceDetailTable';
import BusinessCaseDetailTable from '../../document-management/business-case/components/BusinessCaseDetailTable';
import { TransparentButtonSvg } from '../../../components/buttons/TransparentButtonSvg';
import { ActionButtonClose } from '../../../components/buttons/ActionButtonClose';
import { ActionButton } from '../../../components/buttons/ActionButton';
import { TransparentButton } from '../../../components/buttons/TransparentButton';
import { FormSteps } from '../../../components/dots/FormSteps';
import { createAllianceValidator } from './alliance-validators';
import { businessCaseValidator } from '../../document-management/business-case/business-case-validators';
import collaborateIcon from '../../../images/icons/collab-chat-icon.svg';
import { fetchSession } from '../../auth/auth.actions';
import { sanitizeAllianceKPIsToEdit } from './allianceKPIs/allianceKPIs-actions';
import AllianceKPIsForm from './allianceKPIs/components/AllianceKPIsForm';
import moment from 'moment';
import { checkActiveYearsAndSetStateMixin } from './allianceKPIs/allianceKPIs-actions';
import { allianceKPIsValidator } from './allianceKPIs/allianceKPIs-validators';
import { LeftProgressSection } from '../../../components/new-ui/LeftProgressSection';
import { SCREENS_ALLIANCE } from '../../management/screenView';
import { BoxCard } from '../../../components/new-ui/div/BoxCard';
import { CreateViewCardBody } from '../../../components/new-ui/card/CreateViewCardBody';
import { CardFooter } from '../../../components/new-ui/card/CardFooter';
import { ALLIANCE_COMPLETED } from '../../../shared/status';

// Edit Alliance
class AllianceEditView extends View {
  constructor(props) {
    super(props);
    this.state = {
      allianceData: R.clone(AllianceModel),
      businessCaseData: R.clone(BusinessCaseModel),
      selectedKPIYear: moment().year(),
      kpiYears: [moment().year()],
      loading: true,
      companies: [],
      invitations: [],
      currencies: [],
      step: 0,
      yesNoDialogIsOpen: false,
      yesNoDialogTitle: '',
      yesNoDialogDescription: '',
    };

    this.onError = onErrorMixin.bind(this);
    this.checkActiveYearsAndSetStateMixin = checkActiveYearsAndSetStateMixin.bind(this);
    this.originalRecommendedSolutions = [];
    this.originalDocuments = [];
    this.originalAllianceKPIs = [];
  }

  onChangeAllianceData = (name, value) => {
    const { allianceData, businessCaseData } = this.state;
    allianceData[name] = value;
    this.checkActiveYearsAndSetStateMixin(allianceData, businessCaseData);
  };

  onChangeBusinessCaseData = (name, value) => {
    const { businessCaseData, allianceData } = this.state;
    businessCaseData[name] = value;
    this.checkActiveYearsAndSetStateMixin(allianceData, businessCaseData);
  };

  componentDidMount() {
    this.subscribe(allianceStore, ALLIANCE_ERROR_EVENT, this.onError);
    this.subscribe(invitationStore, ALLIANCE_INVITATION_ERROR_EVENT, this.onError);
    this.subscribe(allianceStore, ALLIANCE_FORM_DATA_EVENT, (state) => {
      this.setState(
        {
          currencies: state.currenciesList.items,
        },
        () => {
          const { match } = this.props;
          if (!match.params.id) return toast.error('Alliance ID missing');
          fetchAlliance(match.params.id);
        },
      );
    });

    this.subscribe(allianceStore, ALLIANCE_DETAIL_EVENT, (state) => {
      state.alliance.businessCase.recommendedSolutions = state.alliance.businessCase.recommendedSolutionsRelation.items.map(
        (solution) => {
          delete solution.__typename;
          return solution;
        },
      );
      this.originalRecommendedSolutions = state.alliance.businessCase.recommendedSolutions.concat();
      const businessCaseData = updateStateFromObject(
        this.state.businessCaseData,
        state.alliance.businessCase,
      );

      businessCaseData.owner = this.state.businessCaseData.owner;

      delete state.alliance.bussinesCase;
      const allianceData = updateStateFromObject(this.state.allianceData, state.alliance);
      allianceData.policyGuidelinesFile = state.alliance.policyGuidelinesFile;
      allianceData.allianceKPIs = state.alliance.allianceKPIAllianceRelation;
      allianceData.organizationalChartFile = state.alliance.organizationalChartFile;
      allianceData.owner = state.alliance.owner;
      allianceData.clientCompany = state.alliance.clientCompany;
      allianceData.partnerCompany = state.alliance.partnerCompany;
      allianceData.currency = state.alliance.currency;
      allianceData.documents = state.alliance.documents.items;
      allianceData.status = state.alliance.status;
      this.originalDocuments = R.clone(allianceData.documents);
      delete allianceData.alllianceForecastAllianceRelation;

      localStorage.setItem(
        ALLIANCE_POLICY_GUIDELINES_FILE,
        JSON.stringify(allianceData.policyGuidelinesFile),
      );
      localStorage.setItem(
        ALLIANCE_ORGANIZATIONAL_CHART_FILE,
        JSON.stringify(allianceData.organizationalChartFile),
      );
      localStorage.setItem(
        ALLIANCE_DOCUMENTS_FILE,
        JSON.stringify(allianceData.policyGuidelinesFile),
      );

      this.originalAllianceKPIs = sanitizeAllianceKPIsToEdit(allianceData);

      log(allianceData, businessCaseData);

      this.checkActiveYearsAndSetStateMixin(allianceData, businessCaseData);

      this.setState({
        invitations: state.alliance.allianceInvitationRelation.items,
        loading: false,
      });
    });

    this.subscribe(allianceStore, ALLIANCE_UPDATE_EVENT, async () => {
      await fetchSession();
      toast.success('Alliance Successfully Updated');
      this.props.history.goBack();
    });

    fetchAllianceFormData();
  }

  componentWillUnmount() {
    super.componentWillUnmount();
    localStorage.removeItem(ALLIANCE_POLICY_GUIDELINES_FILE);
    localStorage.removeItem(ALLIANCE_ORGANIZATIONAL_CHART_FILE);
    localStorage.removeItem(BUSINESS_CASE_DOCUMENT);
  }

  onKPIYearChange = (selectedKPIYear) => {
    this.setState({ selectedKPIYear });
  };

  onSubmit = () => {
    this.setState({ loading: true }, () => {
      const allianceData = R.clone(this.state.allianceData);
      const businessCaseData = R.clone(this.state.businessCaseData);
      updateAlliance(
        allianceData,
        businessCaseData,
        this.originalRecommendedSolutions,
        this.originalAllianceKPIs,
        this.originalDocuments,
      );
    });
  };

  onAllianceStepChange = (step) => {
    const allianceData = R.clone(this.state.allianceData);
    try {
      createAllianceValidator(allianceData);
    } catch (e) {
      return this.onError(e);
    }
    this.onScreen(step);
  };

  onBusinessCaseStepChange = (step) => {
    const businessCaseData = R.clone(this.state.businessCaseData);
    try {
      businessCaseValidator(businessCaseData);
    } catch (e) {
      return this.onError(e);
    }
    this.onScreen(step);
  };

  onKPIsStepChange = (step) => {
    const alliance = R.clone(this.state.allianceData);
    const businessCase = R.clone(this.state.businessCaseData);
    try {
      allianceKPIsValidator(alliance, businessCase);
    } catch (e) {
      return this.onError(e);
    }
    this.onScreen(step);
  };

  onScreen = (step) => {
    this.setState({ step });
  };

  onSelectForDelete = (invitation, name) => {
    this.setState({
      invitation,
      yesNoDialogIsOpen: true,
      yesNoDialogTitle: name,
      yesNoDialogDescription: 'Are you sure you want Delete this Invitation?',
    });
  };

  onYes = () => {
    const newInvitations = this.state.invitations.filter(
      (invitation) => invitation.id !== this.state.invitation.id,
    );
    this.setState(
      {
        invitations: newInvitations,
        yesNoDialogIsOpen: false,
      },
      () => {
        deleteAllianceInvitation(this.state.invitation);
      },
    );
  };

  onClose = () => {
    this.setState({
      yesNoDialogIsOpen: false,
    });
  };

  render() {
    const {
      currencies,
      loading,
      step,
      allianceData,
      invitations,
      businessCaseData,
      selectedKPIYear,
      kpiYears,
    } = this.state;
    const { yesNoDialogIsOpen, yesNoDialogTitle, yesNoDialogDescription } = this.state;
    const { history } = this.props;
    let content = <Loader stretch />;
    let footer = <></>;
    let buttonsTop = <></>;
    const { currency } = allianceData;

    if (allianceData.status === ALLIANCE_COMPLETED) history.push('/settings/alliance-management');
    if (!loading && step === 0) {
      content = (
        <>
          <AllianceForm
            data={allianceData}
            invitations={invitations}
            onChange={this.onChangeAllianceData}
            currencies={currencies}
          />
          <Heading
            type="h3"
            text="Sent Invitations"
            style={{
              margin: 'auto',
              fontSize: 17,
              position: 'relative',
              bottom: '-6px',
              backgroundColor: 'white',
              display: 'inline',
              padding: '0px 10px',
            }}
          />
          <div
            style={{
              border: `1px 0 0 0 solid #d0d7dd`,
              borderRadius: '5px',
              padding: 20,
              marginBottom: 10,
            }}>
            <AllianceInvitationTable invitations={invitations} loading={loading}>
              {(invitation) => (
                <Dropdown defaultOpen={false}>
                  <Dropdown.Head>
                    <Icon name="More" className="more-icon" />
                  </Dropdown.Head>
                  {/* TODO: Rejected in red text */}
                  <Dropdown.Body pin="right">
                    {({ closeDropdown }) => (
                      <Menu>
                        <Menu.Item
                          onClick={() => {
                            closeDropdown();
                            const name = R.pathOr('Unititled', ['alliance', 'name'], invitation);
                            this.onSelectForDelete(invitation, name);
                          }}>
                          Delete
                        </Menu.Item>
                      </Menu>
                    )}
                  </Dropdown.Body>
                </Dropdown>
              )}
            </AllianceInvitationTable>
          </div>
        </>
      );
      footer = (
        <CardFooter>
          <ActionButton type="button" onClick={() => this.onAllianceStepChange(1)} text="Next" />
        </CardFooter>
      );
    }

    if (!loading && step === 1) {
      content = (
        <BusinessCaseForm
          data={businessCaseData}
          onChange={this.onChangeBusinessCaseData}
          currency={currency}
        />
      );
      footer = (
        <CardFooter>
          <ActionButton onClick={() => this.onBusinessCaseStepChange(2)} text="Next" />
          <TransparentButton onClick={() => this.onScreen(0)} text={'Previous'} />
        </CardFooter>
      );
    }

    if (!loading && step === 2) {
      // headerText = 'Key Performance Indicator';
      content = (
        <AllianceKPIsForm
          alliance={allianceData}
          onChange={this.onChangeAllianceData}
          onYearChange={this.onKPIYearChange}
          selectedYear={selectedKPIYear}
          kpiYears={kpiYears}
        />
      );
      footer = (
        <CardFooter>
          <ActionButton onClick={() => this.onKPIsStepChange(3)} text="Next" />
          <TransparentButton onClick={() => this.onScreen(1)} text={'Previous'} />
        </CardFooter>
      );
    }

    if (!loading && step === 3) {
      const allianceDetail = R.clone(allianceData);
      allianceDetail.allianceKPIAllianceRelation = {
        items: allianceDetail.allianceKPIs,
      };

      content = (
        <>
          <AllianceDetailTable data={allianceDetail} currency={currency} />
          <BusinessCaseDetailTable data={businessCaseData} currency={currency} />
        </>
      );
      footer = (
        <CardFooter>
          <ActionButton onClick={this.onSubmit} text="Update Alliance" />
          <TransparentButton onClick={() => this.onScreen(2)} text={'Previous'} />
        </CardFooter>
      );
    }

    buttonsTop = (
      <div className={'button-top company-icons '}>
        <TransparentButtonSvg
          iconSvg={collaborateIcon}
          onClick={() => openComments(allianceData)}
        />
        <ActionButtonClose onClick={history.goBack} />
      </div>
    );

    return (
      <React.Fragment>
        <Card.Header>
          <Grid.Layout
            columns="200px auto 200px"
            areas={[['left', 'center', 'right']]}
            style={{ width: '100%' }}>
            <Grid.Box area="left">
              <Heading type="h4" text="Edit Alliance" />
            </Grid.Box>
            <Grid.Box area="center">
              <FormSteps totalSteps={4} step={step} />
            </Grid.Box>
            <Grid.Box area="right" />
            {buttonsTop}
          </Grid.Layout>
        </Card.Header>
        <CreateViewCardBody>
          <Grid.Layout columns="30% 70%" areas={[['left', 'right']]} style={{ width: '100%' }}>
            <Grid.Box area="left">
              <LeftProgressSection sections={SCREENS_ALLIANCE} currentScreen={step} />
            </Grid.Box>
            <BoxCard>
              <Grid.Box area="right">{content}</Grid.Box>
            </BoxCard>
          </Grid.Layout>
        </CreateViewCardBody>
        {footer}
        <YesNoDialog
          isOpen={yesNoDialogIsOpen}
          onYes={this.onYes}
          onNo={this.onClose}
          onClose={this.onClose}
          text={yesNoDialogDescription}
          title={yesNoDialogTitle}
        />
      </React.Fragment>
    );
  }
}

AllianceEditView.propTypes = {
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default withRouter(AllianceEditView);

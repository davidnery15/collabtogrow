import React from 'react';
import { Card, Heading, Row, Loader } from '@8base/boost';
import { ViewCardBody } from 'components/card/ViewCardBody';
import allianceStore, {
  ALLIANCE_UPDATE_EVENT,
  ALLIANCE_REJECT_EVENT,
  ALLIANCE_DETAIL_EVENT,
  ALLIANCE_ERROR_EVENT,
  ALLIANCE_COMPLETED_EVENT,
  ALLIANCE_DETAIL_LIST_MEMBERS_EVENT,
  ALLIANCE_REACTIVATE_EVENT,
  ALLIANCE_DELETED_EVENT,
  ALLIANCE_DELETED_LIST_EVENT,
  CANCEL_ALLIANCE_DELETED_EVENT,
  ALLIANCE_SUBMIT_FOR_APPROVAL_EVENT,
  ALLIANCE_RESTORE_EVENT,
} from './alliance-store';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import AllianceDetailTable from './components/AllianceDetailTable';
import BusinessCaseDetailTable from '../../document-management/business-case/components/BusinessCaseDetailTable';
import * as toast from 'components/toast/Toast';
import {
  fetchAlliance,
  approveAlliance,
  rejectAlliance,
  completedAlliance,
  openComments,
  fetchAllianceDetailMembersAction,
  reactivateAlliance,
  createAllianceDeleted,
  fetchAllianceDeletedList,
  deleteAllianceDeleted,
  requestApprovalForAlliance,
  restoreAlliance,
} from './alliance-actions';
import AllianceModel from './Alliance.model';
import * as R from 'ramda';
import View from '@cobuildlab/react-flux-state';
import { onChangeMixin, onErrorMixin } from '../../../shared/mixins';
import sessionStore, { NEW_SESSION_EVENT } from '../../../shared/SessionStore';
import YesNoDialog from '../../../components/dialogs/YesNoDialog';
import { ALLIANCE_SER } from '../../../shared/roles';
import {
  canCompletedAlliance,
  canApproveAlliance,
  canReactivateAlliance,
  canDeleteAlliance,
  canManageMembers,
  canSubmitForApprovalAlliancePermission,
  canRestoreAlliance,
} from './alliance-permissions';
import { ActionButtonClose } from '../../../components/buttons/ActionButtonClose';
import { ActionButton } from '../../../components/buttons/ActionButton';
import { OptionButton } from '../../../components/buttons/OptionButton';
import { DangerButton } from '../../../components/buttons/DangerButton';
import { TransparentButtonSvg } from '../../../components/buttons/TransparentButtonSvg';
import { TransparentButtonFontAwesome } from '../../../components/buttons/TransparentButtonFontAwesome';
import collaborateIcon from '../../../images/icons/collab-chat-icon.svg';
import AppovalDetailTable from '../../../components/tables/approvalTable/ApprovalDetailTable';
import AllianceUserRoleDetailTable from '../../../components/tables/allianceUserRoleTable/AllianceUserRoleDetailTable';
import { CardFooter } from '../../../components/new-ui/card/CardFooter';
import BusinessCaseModel from '../../document-management/business-case/BusinessCase.model';

/**
 * Alliance Detail View.
 *
 * @param alliance
 */
class AllianceDetailView extends View {
  constructor(props) {
    super(props);

    const allianceModel = R.clone(AllianceModel);
    allianceModel.businessCase = R.clone(BusinessCaseModel);

    this.state = {
      data: R.clone(allianceModel),
      companyUserRelation: null,
      loading: true,
      approvalModalIsOpen: false,
      rejectModalIsOpen: false,
      completedModalIsOpen: false,
      reactivateModalIsOpen: false,
      submitForApprovalModalIsOpen: false,
      approvalData: [],
      allianceSerList: [],
      canAllianceDeleted: false,
      canCancelAllianceDeleted: false,
      allianceDeletedData: [],
      submitForApprovalAlliance: false,
      restoreModalIsOpen: false,
    };
    this.onChange = onChangeMixin.bind(this);
    this.onError = onErrorMixin.bind(this);
    this.user = sessionStore.getState(NEW_SESSION_EVENT).user;
  }

  componentDidMount() {
    const { match } = this.props;
    if (!match.params.id) return toast.error('Alliance ID missing');

    this.subscribe(allianceStore, ALLIANCE_ERROR_EVENT, this.onError);

    this.subscribe(allianceStore, ALLIANCE_DETAIL_EVENT, (state) => {
      const { alliance } = state;
      const approvalData = alliance.allianceApprovalRelation.items.slice(-2);
      const submitForApprovalAlliance = alliance.allianceApprovalRelation.items.slice(-2);
      this.setState({
        data: alliance,
        approvalData,
        loading: false,
        submitForApprovalAlliance,
      });
    });
    this.subscribe(allianceStore, ALLIANCE_UPDATE_EVENT, () => {
      toast.success('Alliance Successfully Approved');
      window.location.reload();
    });
    this.subscribe(allianceStore, ALLIANCE_REACTIVATE_EVENT, (state) => {
      toast.success('Alliance Successfully reactivate');
      const { data } = this.state;
      data.status = state.status;
      this.setState({
        loading: false,
        data,
      });
    });
    this.subscribe(allianceStore, ALLIANCE_REJECT_EVENT, () => {
      toast.success('Alliance Successfully Rejected');
      this.props.history.goBack();
    });

    this.subscribe(allianceStore, ALLIANCE_COMPLETED_EVENT, (state) => {
      const { allianceUpdate } = state;
      fetchAlliance(allianceUpdate.id);
      toast.success('Alliance Successfully Complete');
    });

    this.subscribe(allianceStore, ALLIANCE_RESTORE_EVENT, (state) => {
      const { allianceUpdate } = state;
      fetchAlliance(allianceUpdate.id);
      toast.success('Alliance Successfully Restored');
    });

    this.subscribe(allianceStore, ALLIANCE_SUBMIT_FOR_APPROVAL_EVENT, (state) => {
      const { allianceUpdate } = state;
      fetchAlliance(allianceUpdate.id);
      toast.success('Alliance Submitted!');
    });

    this.subscribe(allianceStore, ALLIANCE_DELETED_EVENT, (state) => {
      toast.success('Alliance scheduled for deletion!');
      const allianceDeletedData = state.allianceDeletedCreate;
      this.setState({
        allianceDeletedData,
        canAllianceDeleted: false,
        canCancelAllianceDeleted: true,
        loading: false,
      });
    });

    this.subscribe(allianceStore, CANCEL_ALLIANCE_DELETED_EVENT, (state) => {
      const { success } = state.allianceDeletedDelete;
      const allianceDeletedData = [];
      const canAllianceDeleted = success;
      const canCancelAllianceDeleted = !success;
      toast.success('Cancel alliance deletion Successfully ');
      this.setState({
        allianceDeletedData,
        canAllianceDeleted,
        canCancelAllianceDeleted,
        loading: false,
      });
    });

    this.subscribe(allianceStore, ALLIANCE_DETAIL_LIST_MEMBERS_EVENT, (state) => {
      const { items } = state.allianceUserAllianceRelation;
      const allianceSerList =
        items.length !== 0 ? items.filter((item) => item.role.name === ALLIANCE_SER) : [];

      this.setState({
        allianceSerList,
      });
    });

    this.subscribe(allianceStore, ALLIANCE_DELETED_LIST_EVENT, (state) => {
      const { items } = state.allianceDeletedsList;
      const canAllianceDeleted = items.length === 0;
      const canCancelAllianceDeleted = items.length !== 0;
      const allianceDeletedData = items[0];

      this.setState({
        canAllianceDeleted,
        canCancelAllianceDeleted,
        allianceDeletedData,
      });
    });

    fetchAllianceDeletedList(match.params.id);
    fetchAlliance(match.params.id);
    fetchAllianceDetailMembersAction(match.params.id);
  }

  approve = () => {
    this.setState({
      approvalModalIsOpen: true,
    });
  };

  onYes = () => {
    this.setState(
      {
        approvalModalIsOpen: false,
        loading: true,
      },
      () => {
        approveAlliance({ ...this.state.data });
      },
    );
  };

  onClose = () => {
    this.setState({
      approvalModalIsOpen: false,
    });
  };

  reject = () => {
    this.setState({
      rejectModalIsOpen: true,
    });
  };

  onYesReject = () => {
    this.setState(
      {
        rejectModalIsOpen: false,
        loading: true,
      },
      () => {
        rejectAlliance({ ...this.state.data });
      },
    );
  };

  onCloseReject = () => {
    this.setState({
      rejectModalIsOpen: false,
    });
  };

  completeModal = () => {
    this.setState({
      completedModalIsOpen: true,
    });
  };

  restoreModal = () => {
    this.setState({
      restoreModalIsOpen: true,
    });
  };

  onYesModalCompleted = () => {
    this.setState(
      {
        restoreModalIsOpen: false,
        loading: true,
      },
      () => {
        const allianceData = R.clone(this.state.data);
        completedAlliance(allianceData);
      },
    );
  };

  onYesModalRestore = () => {
    this.setState(
      {
        completedModalIsOpen: false,
        loading: true,
      },
      () => {
        const allianceData = R.clone(this.state.data);
        restoreAlliance(allianceData);
      },
    );
  };

  onCloseModalCompleted = () => {
    this.setState({
      completedModalIsOpen: false,
    });
  };

  onCloseModalRestore = () => {
    this.setState({
      restoreModalIsOpen: false,
    });
  };

  onCloseModalReactivate = () => {
    this.setState({
      reactivateModalIsOpen: false,
    });
  };

  onYesModalReactivate = () => {
    this.setState(
      {
        reactivateModalIsOpen: false,
        loading: true,
      },
      () => {
        const allianceData = R.clone(this.state.data);
        reactivateAlliance(allianceData.id);
      },
    );
  };

  deleteAlliance = () => {
    const alliance = R.clone(this.state.data);
    this.setState(
      {
        loading: true,
      },
      () => createAllianceDeleted(alliance),
    );
  };

  recoverAlliance = () => {
    const allianceDeletedData = R.clone(this.state.allianceDeletedData);
    this.setState(
      {
        loading: true,
      },
      () => deleteAllianceDeleted(allianceDeletedData),
    );
  };

  reactivateModal = () => {
    this.setState({
      reactivateModalIsOpen: true,
    });
  };

  onSelectSubmitForApproval = (alliance) => {
    this.setState({
      alliance,
      submitForApprovalModalIsOpen: true,
    });
  };

  onYesModalSubmitForApproval = () => {
    this.setState(
      {
        submitForApprovalModalIsOpen: false,
        loading: true,
      },
      () => requestApprovalForAlliance(this.state.alliance),
    );
  };

  onCloseModalSubmitForApproval = () => {
    this.setState({
      submitForApprovalModalIsOpen: false,
    });
  };

  render() {
    const {
      loading,
      data: alliance,
      approvalModalIsOpen,
      rejectModalIsOpen,
      completedModalIsOpen,
      reactivateModalIsOpen,
      submitForApprovalModalIsOpen,
      approvalData,
      allianceSerList,
      canAllianceDeleted,
      canCancelAllianceDeleted,
      submitForApprovalAlliance,
      restoreModalIsOpen,
    } = this.state;

    let content = <Loader stretch />;
    let buttonsTop = '';
    let buttonsBottom = '';

    if (!loading) {
      const canApprove = canApproveAlliance(this.user, alliance);

      content = (
        <>
          <AllianceDetailTable
            data={alliance}
            onChange={this.onChange}
            currency={alliance.currency}
            onClickEdit={() =>
              this.props.history.push(`/settings/alliance-management/edit/${alliance.id}`)
            }
          />
          <BusinessCaseDetailTable data={alliance.businessCase} currency={alliance.currency} />
          <AllianceUserRoleDetailTable data={allianceSerList} />
          <AppovalDetailTable data={approvalData} />
        </>
      );
      buttonsTop = (
        <>
          <div className="company-icons">
            {canManageMembers(this.user, alliance) ? (
              <TransparentButtonFontAwesome
                text={'Manage Members'}
                fontAwesomeIcon={'users-cog'}
                onClick={() =>
                  this.props.history.push(`/settings/alliance-management/members/${alliance.id}`)
                }
              />
            ) : null}
            <TransparentButtonSvg
              iconSvg={collaborateIcon}
              text={'Collaborate'}
              onClick={() => openComments(alliance)}
            />
            <ActionButtonClose onClick={() => this.props.history.goBack()} />
          </div>
        </>
      );

      buttonsBottom = (
        <Row justifyContent="end">
          {canReactivateAlliance(this.user, alliance) && !canCancelAllianceDeleted ? (
            <OptionButton
              text="Reactivate Alliance"
              fontAwesomeIcon="clipboard-list"
              onClick={this.reactivateModal}
            />
          ) : null}

          {canDeleteAlliance(this.user, alliance) && canAllianceDeleted ? (
            <OptionButton text="Delete" fontAwesomeIcon="trash" onClick={this.deleteAlliance} />
          ) : null}

          {canDeleteAlliance(this.user, alliance) && canCancelAllianceDeleted ? (
            <OptionButton
              text="Recover"
              fontAwesomeIcon="trash-restore"
              onClick={this.recoverAlliance}
            />
          ) : null}
          {canSubmitForApprovalAlliancePermission(this.user, alliance) ? (
            <ActionButton
              text={'Submit For Approval'}
              fontAwesomeIcon={'check'}
              onClick={() => {
                this.onSelectSubmitForApproval(alliance);
              }}
              disabled={!submitForApprovalAlliance}
            />
          ) : (
            ''
          )}
          <DangerButton
            text={'Reject'}
            fontAwesomeIcon={'times'}
            onClick={this.reject}
            disabled={!canApprove}
          />
          <ActionButton
            fontAwesomeIcon={'check'}
            onClick={this.approve}
            text={'Approve'}
            disabled={!canApprove}
          />

          {canCompletedAlliance(this.user, alliance) ? (
            <ActionButton
              text="Mark Completed"
              fontAwesomeIcon="clipboard-list"
              onClick={() => {
                this.completeModal(alliance);
              }}
            />
          ) : null}

          {canRestoreAlliance(this.user, alliance) ? (
            <ActionButton
              text="Restore"
              fontAwesomeIcon="clipboard-list"
              onClick={() => {
                this.restoreModal(alliance);
              }}
            />
          ) : null}
        </Row>
      );
    }

    return (
      <React.Fragment>
        <Card.Header>
          <Heading type="h4" text={alliance.name} />
          {buttonsTop}
        </Card.Header>
        <ViewCardBody>{content}</ViewCardBody>
        <CardFooter>{buttonsBottom}</CardFooter>
        <YesNoDialog
          title={'Approve Alliance'}
          onYes={this.onYes}
          onClose={this.onClose}
          onNo={this.onClose}
          text={'Are you sure you want to Approve the Alliance?'}
          isOpen={approvalModalIsOpen}
        />
        <YesNoDialog
          title={'Reject Alliance'}
          onYes={this.onYesReject}
          onClose={this.onCloseReject}
          onNo={this.onCloseReject}
          text={'Are you sure you want to Reject the Alliance?'}
          isOpen={rejectModalIsOpen}
        />
        <YesNoDialog
          title={'Complete Alliance '}
          onYes={this.onYesModalCompleted}
          onCloseModalCompleted={this.onCloseModalCompleted}
          onNo={this.onCloseModalCompleted}
          text={'Are you sure you want to Mark the Alliance as Completed?'}
          isOpen={completedModalIsOpen}
        />

        <YesNoDialog
          title={'Restore Alliance '}
          onYes={this.onYesModalRestore}
          onCloseModalCompleted={this.onCloseModalRestore}
          onNo={this.onCloseModalRestore}
          text={'Are you sure you want to Restore this alliance?'}
          isOpen={restoreModalIsOpen}
        />

        <YesNoDialog
          title={'Reactivate Alliance'}
          onYes={this.onYesModalReactivate}
          onCloseModalCompleted={this.onCloseModalReactivate}
          onNo={this.onCloseModalReactivate}
          text={'Are you sure you want to Reactivate Alliance?'}
          isOpen={reactivateModalIsOpen}
        />
        <YesNoDialog
          isOpen={submitForApprovalModalIsOpen}
          onYes={this.onYesModalSubmitForApproval}
          onNo={this.onCloseModalSubmitForApproval}
          onClose={this.onCloseModalSubmitForApproval}
          text={'Are you sure you want to Submit this Alliance For Approval?'}
          title={'Submit For Approval'}
        />
      </React.Fragment>
    );
  }
}

AllianceDetailView.propTypes = {
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default withRouter(AllianceDetailView);

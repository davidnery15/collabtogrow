import { allianceKPIsYearModel } from './allianceKPIs-model';
import { log, error } from '@cobuildlab/pure-logger';
import sessionStore, { APOLLO_CLIENT } from '../../../../shared/SessionStore';
import { ALLIANCE_KPI_DELETE_MUTATION, ALLIANCE_KPI_UPDATE_MUTATION } from './allianceKPIs-queries';
import { sanitize8BaseBigInt, sanitize8BaseBigInts } from '../../../../shared/utils';
import * as R from 'ramda';
import moment from 'moment';

/**
 * To get the AllianceKPIs To Be deleted
 * @param  {array} AllianceKPIs
 * @param  {array} originalAllianceKPIs
 * @return {array}
 */
const allianceKPIsToBeDeleted = (allianceKPIs, originalAllianceKPIs) => {
  const toBeDeleted = [];

  originalAllianceKPIs.forEach((original) => {
    const _original = allianceKPIs.find((allianceKPIs) => original.id === allianceKPIs.id);
    if (_original === undefined) toBeDeleted.push(original);
  });

  log('allianceKPIsToBeDeleted', toBeDeleted);
  return toBeDeleted;
};

/**
 * To delete allianceKPIs on the Alliance Update actions
 * @param  {array} allianceKPIs
 * @param  {array} originalAllianceKPIs
 */
export const deleteAllianceKPIs = async (allianceKPIs, originalAllianceKPIs) => {
  log(`deleteAllianceKPIs:`, allianceKPIs, originalAllianceKPIs);
  const client = sessionStore.getState(APOLLO_CLIENT);
  const toBeDeleted = allianceKPIsToBeDeleted(allianceKPIs, originalAllianceKPIs);

  const deleteAllianceKPIPromise = async (allianceKPI) => {
    const data = { id: allianceKPI.id };

    let response;
    try {
      response = await client.mutate({
        mutation: ALLIANCE_KPI_DELETE_MUTATION,
        variables: { data },
      });
    } catch (e) {
      error('deleteAllianceKPIs', e, allianceKPI);
      throw e;
    }

    return response;
  };

  let responses;
  try {
    responses = await Promise.all(toBeDeleted.map(deleteAllianceKPIPromise));
  } catch (e) {
    throw e;
  }

  log('deleteAllianceKPIs', responses);
  return responses;
};

/**
 * To get the AllianceKPIs To Be updated
 * @param  {array} AllianceKPIs
 * @param  {array} originalAllianceKPIs
 * @return {array}
 */
const allianceKPIsToBeUpdated = (allianceKPIs, originalAllianceKPIs) => {
  const toBeUpdated = [];

  allianceKPIs.forEach((allianceKPI) => {
    if (allianceKPI.id) {
      const original = originalAllianceKPIs.find((original) => original.id === allianceKPI.id);
      if (!R.equals(original, allianceKPI)) {
        toBeUpdated.push(allianceKPI);
      }
    }
  });

  log('allianceKPIsTobeUpdated', toBeUpdated);
  return toBeUpdated;
};

/**
 * To Update allianceKPIs on alliance update action
 * @param  {Array} allianceKPIs
 * @param  {Array} originalAllianceKPIs
 */
export const updateAllianceKPIs = async (allianceKPIs, originalAllianceKPIs) => {
  log(`updateAllianceKPIs:`, allianceKPIs, originalAllianceKPIs);
  const client = sessionStore.getState(APOLLO_CLIENT);
  const toBeUpdated = allianceKPIsToBeUpdated(allianceKPIs, originalAllianceKPIs);

  const updateAllianceKPIPromise = async (data) => {
    let response;
    try {
      response = await client.mutate({
        mutation: ALLIANCE_KPI_UPDATE_MUTATION,
        variables: { data },
      });
    } catch (e) {
      error('updateAllianceKPIs', e, data);
      throw e;
    }

    return response;
  };

  let responses;
  try {
    responses = await Promise.all(toBeUpdated.map(updateAllianceKPIPromise));
  } catch (e) {
    throw e;
  }

  log('updateAllianceKPIs', responses);
  return responses;
};

/**
 * Prepare allianceKPIs for edit view
 * @param {Alliance} alliance to remove the unwanted allianceKPIs
 * properties
 * @return {Array}  to set the originalAllianceKPIs
 */
export const sanitizeAllianceKPIsToEdit = (alliance) => {
  const allianceKPIsSanitizeData = alliance.allianceKPIs.items.map((allianceKPI) => {
    delete allianceKPI.__typename;
    return allianceKPI;
  });

  alliance.allianceKPIs = R.clone(allianceKPIsSanitizeData);
  // set default allianceKPIs
  if (!alliance.allianceKPIs.length) {
    const date = alliance.effectiveDate || new Date();
    const year = moment(date).year();
    alliance.allianceKPIs = allianceKPIsYearModel(year);
  }
  return R.clone(allianceKPIsSanitizeData);
};

/**
 * To get the allianceKPIs To Be created on the alliance update action
 * @param  {array} allianceKPIs
 * @return {array}
 */
export const allianceKPIsToBeCreated = (allianceKPIs) => {
  const toBeCreated = [];

  allianceKPIs.forEach((allianceKPI) => {
    if (allianceKPI.id === undefined) toBeCreated.push(allianceKPI);
  });

  log('allianceKPIsToBeCreated', toBeCreated);
  return toBeCreated;
};

/**
 * sanitize AllianceKPIs to create
 * // WARNING: this function mutates the data
 * @param  {Alliance} alliance
 */
export const sanitizeAllianceKPIsCreate = (alliance) => {
  alliance.allianceKPIAllianceRelation = {
    create: alliance.allianceKPIs,
  };
  delete alliance.allianceKPIs;
};

/**
 * getKPIYears
 * @param  {Alliance} allianceData
 * @param  {BusinessCase} businesCaseData
 */
export const getKPIYears = (allianceData, businessCaseData) => {
  const { effectiveDate } = allianceData;
  const { anticipatedCosts, expectedCosts, expectedRevenues } = businessCaseData;

  const _effectiveDate = effectiveDate || new Date();
  const effectiveDateActualDiff = moment().year() - moment(_effectiveDate).year();
  const dateStart = moment(_effectiveDate);
  const kpiYears = [];
  // to get Max years
  const yearsOptions = [
    anticipatedCosts.length,
    expectedCosts.length,
    expectedRevenues.length,
    effectiveDateActualDiff + 1,
  ];
  // Max years for dateEnd & forecastingMaxYears
  const yearsCount = Math.max(...yearsOptions);
  // dateEnd for KPIs
  const dateEnd = moment(_effectiveDate).add('years', yearsCount - 1);

  // getKPIYears
  if (dateStart.year() >= dateEnd.year()) {
    kpiYears.push(dateStart.year());
  } else {
    while (dateStart.year() <= dateEnd.year()) {
      kpiYears.push(dateStart.year());
      dateStart.add(1, 'year');
    }
  }

  // getForecastYears
  const forecastYears = R.clone(kpiYears);
  // push one more year
  forecastYears.push(forecastYears[forecastYears.length - 1] + 1);

  return { kpiYears, forecastYears, yearsCount };
};

/**
 *  check ActiveYears And Set allianceData State Mixin
 *  Updates the selectedKPIYear &
 *  allianceKPIs based on effectiveDate
 * // You must bind this function on the constructor
 *
 * @param  {Alliance} allianceData
 * @param  {BusinessCase} businesCaseData
 */
export function checkActiveYearsAndSetStateMixin(allianceData, businessCaseData) {
  const { selectedKPIYear } = this.state;
  const { kpiYears } = getKPIYears(allianceData, businessCaseData);

  // add missing years
  for (const kpiYear of kpiYears) {
    if (!allianceData.allianceKPIs.some(({ year }) => kpiYear === year)) {
      allianceData.allianceKPIs.push.apply(
        allianceData.allianceKPIs,
        allianceKPIsYearModel(kpiYear),
      );
    }
  }
  // remove not included years
  allianceData.allianceKPIs = allianceData.allianceKPIs.filter(({ year }) =>
    kpiYears.includes(year),
  );
  // set selected years
  const _selectedKPIYear = kpiYears.includes(selectedKPIYear) ? selectedKPIYear : kpiYears[0];

  this.setState({
    selectedKPIYear: _selectedKPIYear,
    allianceData,
    businessCaseData,
    kpiYears,
  });
}

/**
 * sanitize Alliance KPIs
 * @param  {Alliance} alliance
 */
export const sanitizeAllianceKPIs = (alliance) => {
  alliance.allianceKPIs = alliance.allianceKPIs.map((kpi) => {
    sanitize8BaseBigInts(kpi, 'monthByMonth');
    sanitize8BaseBigInt(kpi, 'target');
    return kpi;
  });
};

import React, { Fragment } from 'react';
import { Row, Column, RadioGroupField, Label, Grid, SelectField } from '@8base/boost';
import PropTypes from 'prop-types';
import { onErrorMixin } from '../../../../../shared/mixins';
import { CurrencyInputField } from '../../../../../shared/components/CurrencyInputField';
import {
  TARGET_DISTRIBUTION_LABELS,
  MONTHS,
  MANUAL_TYPE,
  KPI_WITH_CURRENCY_TYPES,
} from '../allianceKPIs-model';
import { HorizontalLineText } from '../../../../../components/new-ui/text/HorizontalLineText';
import { ColumnLeft } from '../../../../../components/new-ui/ColumnLeft';
import { ColumnRight } from '../../../../../components/new-ui/ColumnRight';

/**
 * The Form for the Alliance Entity
 */

const AllianceKPIsForm = ({ alliance, onChange, onYearChange, selectedYear, kpiYears }) => {
  const { allianceKPIs, currency } = alliance;

  const yearSelect = (
    <Row justifyContent={'center'}>
      <Column>
        <SelectField
          style={{ maxWidth: '100px', minWidth: '100px' }}
          label="Year"
          input={{
            name: 'year',
            value: selectedYear,
            onChange: (value) => onYearChange(value),
          }}
          meta={{}}
          placeholder="Select the KPI year"
          options={kpiYears.map((year) => {
            return { label: year, value: year };
          })}
        />
      </Column>
    </Row>
  );

  const content = (
    <>
      {allianceKPIs
        .filter(({ year }) => year === selectedYear)
        .map((allianceKPI, i) => {
          const showMonthByMonth = allianceKPI.targetDistributionType === MANUAL_TYPE;
          const hasCurrency = KPI_WITH_CURRENCY_TYPES.includes(allianceKPI.type);
          const _currency = hasCurrency ? currency : { symbol: '' };
          // horizontalLineText section
          let kpiName = allianceKPI.type;
          let horizontalLineText = null;
          if (allianceKPI.type.includes('- Client')) {
            kpiName = allianceKPI.type.replace(' - Client', '');
            horizontalLineText = alliance.clientCompany ? alliance.clientCompany.name : 'Client';
          }
          if (allianceKPI.type.includes('- Partner')) {
            kpiName = '';
            horizontalLineText = alliance.partnerCompany ? alliance.partnerCompany.name : 'Partner';
          }

          return (
            <>
              <Row growChildren gap="lg" offsetTop="lg" key={i}>
                <ColumnLeft text={`${horizontalLineText ? kpiName : ''}`} />
                <ColumnRight />
              </Row>
              {horizontalLineText ? <HorizontalLineText text={horizontalLineText} /> : null}
              <Row growChildren gap="lg" offsetBottom="lg" key={i}>
                <ColumnLeft text={`${!horizontalLineText ? kpiName : ''}`} />
                <ColumnRight style={{ width: '60%' }} alignItems="start">
                  <Label>{`Target Distribution Type Year ${allianceKPI.year}`}</Label>
                  <RadioGroupField
                    direction={'row'}
                    options={TARGET_DISTRIBUTION_LABELS}
                    input={{
                      value: allianceKPI.targetDistributionType,
                      onChange: (value) => {
                        allianceKPI.targetDistributionType = value;
                        if (value === MANUAL_TYPE) {
                          if (!allianceKPI.monthByMonth || !allianceKPI.monthByMonth.length) {
                            allianceKPI.monthByMonth = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                          }
                        }
                        onChange('allianceKPIs', allianceKPIs);
                      },
                    }}
                  />

                  <Row>
                    <CurrencyInputField
                      currency={_currency}
                      label={'Target'}
                      value={allianceKPI.target}
                      onChange={(value) => {
                        allianceKPI.target = value;
                        onChange('allianceKPIs', allianceKPIs);
                      }}
                    />
                  </Row>

                  {showMonthByMonth ? (
                    <Row growChildren>
                      <Column>
                        {allianceKPI.monthByMonth.map((target, monthIndex) => {
                          const label = `${MONTHS[monthIndex]} ${allianceKPI.year}`;
                          const isPair = !(monthIndex % 2);

                          return (
                            <Fragment key={monthIndex}>
                              {isPair ? (
                                <CurrencyInputField
                                  currency={_currency}
                                  label={label}
                                  value={target}
                                  onChange={(value) => {
                                    allianceKPI.monthByMonth[monthIndex] = value;
                                    onChange('allianceKPIs', allianceKPIs);
                                  }}
                                />
                              ) : null}
                            </Fragment>
                          );
                        })}
                      </Column>
                      <Column>
                        {allianceKPI.monthByMonth.map((target, monthIndex) => {
                          const label = `${MONTHS[monthIndex]} ${allianceKPI.year}`;
                          const isNotPair = monthIndex % 2;

                          return (
                            <Fragment key={monthIndex}>
                              {isNotPair ? (
                                <CurrencyInputField
                                  currency={_currency}
                                  label={label}
                                  value={target}
                                  onChange={(value) => {
                                    allianceKPI.monthByMonth[monthIndex] = value;
                                    onChange('allianceKPIs', allianceKPIs);
                                  }}
                                />
                              ) : null}
                            </Fragment>
                          );
                        })}
                      </Column>
                    </Row>
                  ) : null}
                </ColumnRight>
              </Row>
            </>
          );
        })}
    </>
  );

  return (
    <Grid.Layout columns="1fr" areas={[['center']]} style={{ width: '100%' }}>
      <Grid.Box area="center">
        <HorizontalLineText text={'Key Performance Indicator'} />
        {yearSelect}
        {content}
      </Grid.Box>
    </Grid.Layout>
  );
};

Object.assign(AllianceKPIsForm.prototype, onErrorMixin);

AllianceKPIsForm.propTypes = {
  onChange: PropTypes.func.isRequired,
  onYearChange: PropTypes.func.isRequired,
  alliance: PropTypes.object.isRequired,
  selectedYear: PropTypes.number.isRequired,
  kpiYears: PropTypes.array.isRequired,
};

export default AllianceKPIsForm;

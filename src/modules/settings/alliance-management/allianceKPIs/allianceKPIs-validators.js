import { MANUAL_TYPE } from './allianceKPIs-model';
import { ValidationError } from '../../../../shared/errors';
import {
  getKPIName,
  getKPIFromList,
} from '../../../reports/balanced-scorecard/balanced-scorecard-utils';
import { getKPIYears } from './allianceKPIs-actions';
import {
  BOOKINGS_CLIENT_TYPE,
  BOOKINGS_PARTNER_TYPE,
  CONTRIBUTIONS_CLIENT_TYPE,
  CONTRIBUTIONS_PARTNER_TYPE,
} from './allianceKPIs-model';
import BigInt from 'big-integer';

/**
 * alliance KPIs Validator
 * @param  {Alliance} alliance
 * @param  {BusinessCase} businessCase
 */
export const allianceKPIsValidator = (alliance, businessCase) => {
  const { allianceKPIs } = alliance;
  const {
    expectedRevenues: expectedBookings,
    anticipatedCosts: anticipatedContributions,
  } = businessCase;
  const { kpiYears } = getKPIYears(alliance, businessCase);
  const errorMessages = [];

  allianceKPIs.forEach((kpi, i) => {
    const { monthByMonth, targetDistributionType, target, year, type } = kpi;
    if (targetDistributionType === MANUAL_TYPE) {
      const monthByMonthTotal = monthByMonth.reduce((a, b) => BigInt(a).add(b), BigInt(0));
      if (BigInt(target).notEquals(BigInt(monthByMonthTotal))) {
        const kpiName = getKPIName(alliance, type);
        errorMessages.push(
          `${kpiName} year: ${year} KPI Target and total month by month amount must be equal`,
        );
      }
    }
  });

  kpiYears.forEach((kpiYear, i) => {
    const expectedBookingsValue =
      expectedBookings[i] !== undefined ? BigInt(expectedBookings[i]) : null;
    const anticipatedContributionsValue =
      anticipatedContributions[i] !== undefined ? BigInt(anticipatedContributions[i]) : null;

    // targetBookings
    const clientBookings = getKPIFromList(allianceKPIs, BOOKINGS_CLIENT_TYPE, kpiYear);
    const partnerBookings = getKPIFromList(allianceKPIs, BOOKINGS_PARTNER_TYPE, kpiYear);
    const clientBookingsTarget = clientBookings ? clientBookings.target : BigInt(0);
    const partnerBookingsTarget = partnerBookings ? partnerBookings.target : BigInt(0);
    const targetBookings = BigInt(clientBookingsTarget).add(BigInt(partnerBookingsTarget));
    // targetContributions
    const clientContributions = getKPIFromList(allianceKPIs, CONTRIBUTIONS_CLIENT_TYPE, kpiYear);
    const partnerContributions = getKPIFromList(allianceKPIs, CONTRIBUTIONS_PARTNER_TYPE, kpiYear);
    const clientContributionsTarget = clientContributions ? clientContributions.target : BigInt(0);
    const partnerContributionsTarget = partnerContributions
      ? partnerContributions.target
      : BigInt(0);
    const targetContributions = BigInt(clientContributionsTarget).add(
      BigInt(partnerContributionsTarget),
    );

    // check if there is expectedBookingsValue for the current year
    if (expectedBookingsValue === null) {
      errorMessages.push(
        `You must add a Business Case's Expected Bookings for the Bookings KPI year: ${kpiYear}(${i +
          1})`,
      );
    } else if (BigInt(expectedBookingsValue).notEquals(targetBookings)) {
      errorMessages.push(
        `Bookings Client + Partner KPI's target year ${kpiYear}(${i +
          1}) must be equal to Business Case's Expected Bookings`,
      );
    }
    // check if there is anticipatedContributionsValue for the current year
    if (anticipatedContributionsValue === null) {
      errorMessages.push(
        `You must add a Business Case's Anticipated Contributions for the Contributions KPI year: ${kpiYear}(${i +
          1})`,
      );
    } else if (BigInt(anticipatedContributionsValue).notEquals(targetContributions)) {
      errorMessages.push(
        `Contributions Client + Partner KPI's target year ${kpiYear}(${i +
          1}) must be equal to Business Case's Anticipated Contributions`,
      );
    }
  });

  if (errorMessages.length) throw new ValidationError(errorMessages);
};

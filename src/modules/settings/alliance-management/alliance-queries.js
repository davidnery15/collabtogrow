import gql from 'graphql-tag';
import { UserFragment, AllianceInvitationFragment } from '../../auth/queries';
import { CompanyUserFragment } from '../company-management/Company.queries';
import { BusinessCaseFragment } from '../../document-management/business-case/businessCase.queries';
import { CommentFragment } from '../../comment/comment-queries';
import { AllianceKPIFragment } from './allianceKPIs/allianceKPIs-queries';
import { ForecastingYearFragment } from '../../reports/forecasting/forecasting-queries';

const AllianceApprovalFragment = gql`
  fragment AllianceApprovalFragment on AllianceApproval {
    id
    alliance {
      id
      name
    }
    company {
      id
      name
    }
    status
    dateOfResponse
  }
`;

const AllianceFragment = gql`
  fragment AllianceFragment on Alliance {
    id
    name
    description
    createdAt
    status
    effectiveDate
    clientCompany {
      id
      name
    }
    partnerCompany {
      id
      name
    }
    businessCase {
      ...BusinessCaseFragment
    }
    policyGuidelinesFile {
      id
      downloadUrl
      filename
    }
    organizationalChartFile {
      id
      downloadUrl
      filename
    }
    allianceInvitationRelation {
      count
      items {
        ...AllianceInvitationFragment
      }
    }
    owner {
      ...UserFragment
    }
    createdAt
    allianceApprovalRelation {
      count
      items {
        ...AllianceApprovalFragment
      }
    }
    currency {
      id
      thousandSeparator
      decimalSeparator
      symbol
      shortName
    }
    allianceApprovalRelation {
      items {
        dateOfResponse
        status
        approvedBy {
          ...UserFragment
        }
        company {
          id
          name
        }
      }
    }
    documents {
      items {
        id
        downloadUrl
        filename
      }
    }
    allianceKPIAllianceRelation {
      items {
        ...AllianceKPIFragment
      }
    }
    allianceForecastAllianceRelation {
      items {
        ...ForecastingYearFragment
      }
    }
  }
  ${UserFragment}
  ${BusinessCaseFragment}
  ${AllianceInvitationFragment}
  ${AllianceApprovalFragment}
  ${AllianceKPIFragment}
  ${ForecastingYearFragment}
`;

/**
 * List Query
 * Example:
 * {
    "data": {
      "user": {
        "id": {
          "equals": "cjouj32uc000h01rxv4nevfin"
        }.
      }.
    }.
  }.
 */
export const ALLIANCE_LIST_QUERY = gql`
  query($data: AllianceFilter, $skip: Int, $first: Int) {
    alliancesList(filter: $data, skip: $skip, first: $first) {
      count
      items {
        ...AllianceFragment
      }
    }
  }
  ${AllianceFragment}
`;

export const ALLIANCE_SMALL_LIST_QUERY = gql`
  query($data: AllianceFilter) {
    alliancesList(filter: $data) {
      count
      items {
        id
        name
        currency {
          id
          thousandSeparator
          decimalSeparator
          symbol
          shortName
        }
      }
    }
  }
`;

/**
 *
 *
 */
export const ALLIANCE_APPROVALS_LIST_QUERY = gql`
  query($data: AllianceApprovalFilter) {
    allianceApprovalsList(filter: $data) {
      count
      items {
        ...AllianceApprovalFragment
      }
    }
  }
  ${AllianceApprovalFragment}
`;

export const ALLIANCE_FORM_DATA_QUERY = gql`
  query {
    currenciesList {
      count
      items {
        id
        thousandSeparator
        decimalSeparator
        symbol
        shortName
      }
    }
  }
`;

/**
 * Alliance Information Detail Query
 * Example:
 {
    "id": "cjpsomw4u000701rygtqpew4t"
  }.
 *
 */
export const ALLIANCE_DETAIL_QUERY = gql`
  query($id: ID!) {
    alliance(id: $id) {
      ...AllianceFragment
    }
  }
  ${AllianceFragment}
`;

export const ALLIANCE_CREATE_MUTATION = gql`
  mutation($data: AllianceCreateInput!) {
    allianceCreate(data: $data) {
      id
    }
  }
`;

/**
 * Update an Alliance.
 */
export const ALLIANCE_UPDATE_MUTATION = gql`
  mutation($data: AllianceUpdateInput!) {
    allianceUpdate(data: $data) {
      id
      status
    }
  }
`;

/**
 * Update an Alliance Approval.
 */
export const ALLIANCE_APPROVAL_UPDATE_MUTATION = gql`
  mutation($approval: AllianceApprovalUpdateInput!) {
    allianceApprovalUpdate(data: $approval) {
      id
    }
  }
`;

/**
 * Update an Alliance Approval and a Alliance.
 */
export const ALLIANCE_APPROVAL_MUTATION = gql`
  mutation($alliance: AllianceUpdateInput!, $approval: AllianceApprovalUpdateInput!) {
    allianceUpdate(data: $alliance) {
      id
    }
    allianceApprovalUpdate(data: $approval) {
      id
    }
  }
`;

export const ALLIANCE_DELETE_MUTATION = gql`
  mutation($data: AllianceDeleteInput!) {
    allianceDelete(data: $data) {
      success
    }
  }
`;

export const C2GUSERROLE_CREATE_MUTATION = gql`
  mutation($data: C2GAllianceRoleCreateInput!) {
    c2GAllianceRoleCreate(data: $data) {
      id
    }
  }
`;

export const C2GUSERROLE_UPDATE_MUTATION = gql`
  mutation($data: C2GAllianceRoleUpdateInput!) {
    c2GAllianceRoleUpdate(data: $data) {
      id
    }
  }
`;

export const C2GUSERROLE_DELETE_MUTATION = gql`
  mutation($data: C2GAllianceRoleDeleteInput!) {
    c2GAllianceRoleDelete(data: $data) {
      success
    }
  }
`;

export const ALLIANCE_MEMBERS_QUERY = gql`
  query($allianceId: ID!) {
    alliance(id: $allianceId) {
      id
      clientCompany {
        id
        name
        companyUserRelation {
          count
          items {
            ...CompanyUserFragment
          }
        }
      }
      partnerCompany {
        id
        name
        companyUserRelation {
          count
          items {
            ...CompanyUserFragment
          }
        }
      }
      allianceUserAllianceRelation {
        items {
          id
          companyUser {
            id
            user {
              ...UserFragment
            }
            company {
              id
            }
          }
          role {
            id
            name
          }
        }
      }
    }
  }
  ${CompanyUserFragment}
  ${UserFragment}
`;

/* Mutation to assign a new role to a member */
export const ALLIANCE_USER_UPDATE_MUTATION = gql`
  mutation($data: AllianceUserUpdateInput!) {
    allianceUserUpdate(data: $data) {
      id
    }
  }
`;

export const ALLIANCE_MEMBERS_WITH_ROLE_QUERY = gql`
  query($data: AllianceUserFilter) {
    allianceUsersList(filter: $data) {
      count
      items {
        id
        alliance {
          id
        }
        role {
          id
          name
        }
        companyUser {
          id
          user {
            id
            firstName
            lastName
            email
          }
          company {
            id
            name
          }
        }
      }
    }
  }
`;

export const CREATE_ALLIANCE_USER_MUTATION = gql`
  mutation($data: AllianceUserCreateInput!) {
    allianceUserCreate(data: $data) {
      id
    }
  }
`;

export const CREATE_ALLIANCE_MEMBER_INVITATION_MUTATION = gql`
  mutation($data: AllianceMemberInvitationCreateInput!) {
    allianceMemberInvitationCreate(data: $data) {
      id
    }
  }
`;

export const DELETE_ALLIANCE_USER_MUTATION = gql`
  mutation($data: AllianceUserDeleteInput!) {
    allianceUserDelete(data: $data) {
      success
    }
  }
`;

export const ALLIANCE_COMMENTS_QUERY = gql`
  query($id: ID!) {
    alliance(id: $id) {
      id
      comments {
        items {
          ...CommentFragment
        }
        count
      }
    }
  }
  ${CommentFragment}
`;

export const ALLIANCE_REACTIVATE_QUERY = gql`
  mutation($data: AllianceUpdateInput!) {
    allianceUpdate(data: $data) {
      id
      status
    }
  }
`;

export const CREATE_ALLIANCE_DELETED_MUTATION = gql`
  mutation($data: AllianceDeletedCreateInput!) {
    allianceDeletedCreate(data: $data) {
      id
      alliance {
        id
      }
    }
  }
`;

export const DELETED_ALLIANCE_DELETED_MUTATION = gql`
  mutation($data: AllianceDeletedDeleteInput!) {
    allianceDeletedDelete(data: $data) {
      success
    }
  }
`;

export const ALLIANCE_DELETED_LIST_QUERY = gql`
  query($filter: AllianceDeletedFilter) {
    allianceDeletedsList(filter: $filter) {
      items {
        alliance {
          id
          name
        }
        id
      }
      count
    }
  }
`;

import gql from 'graphql-tag';

export const META_QUERY = gql`
  query User($userId: ID) {
    metasList(filter: { user: { id: { equals: $userId } } }) {
      count
      items {
        name
        value
      }
    }
  }
`;

export const CREATE_META_MUTATION = gql`
  mutation CreateMeta($data: MetaCreateInput!) {
    metaCreate(data: $data) {
      id
      value
      name
    }
  }
`;

export const UPDATE_META_MUTATION = gql`
  mutation UpdateMeta($data: MetaUpdateInput!) {
    metaUpdate(data: $data) {
      id
      value
      name
    }
  }
`;

export const CREATE_USER = gql`
  mutation CreateUser(
    $metaData: MetaCreateInput!
    $userData: UserInformationCreateInput!
    $companyData: CompanyInformationCreateInput!
    $allianceData: AllianceInformationCreateInput!
  ) {
    metaCreate(data: $metaData) {
      id
      value
      name
    }
    userInformationCreate(data: $userData) {
      id
    }
    companyInformationCreate(data: $companyData) {
      id
    }
    allianceInformationCreate(data: $allianceData) {
      id
    }
  }
`;

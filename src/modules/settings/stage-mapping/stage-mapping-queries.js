import gql from 'graphql-tag';
import { UserFragment } from '../../auth/queries';
import { CompanyFragment } from '../company-management/Company.queries';

/**
 * Create.
 */
export const STAGE_MAPPING_CREATE_MUTATION = gql`
  mutation($data: DealStageCreateInput!) {
    dealStageCreate(data: $data) {
      id
    }
  }
`;

export const STAGE_MAPPING_UPDATE_MUTATION = gql`
  mutation($data: DealStageUpdateInput!) {
    dealStageUpdate(data: $data) {
      id
    }
  }
`;

export const STAGE_MAPPING_VALUES_CREATE_MUTATION = gql`
  mutation($data: DealStageValueCreateInput!) {
    dealStageValueCreate(data: $data) {
      id
    }
  }
`;

const AllianceFragment = gql`
  fragment AllianceFragment on Alliance {
    id
    name
  }
`;

export const STAGE_MAPPING_LIST_QUERY = gql`
  query($data: DealStageFilter) {
    dealStagesList(filter: $data) {
      count
      items {
        id
        name
        winProbability
        entranceCriteria
        exitCriteria
        description
        dealStageDealStageValuesRelation {
          items {
            id
            value
            createdBy {
              ...UserFragment
            }
            company {
              ...CompanyFragment
            }
            alliance {
              ...AllianceFragment
            }
          }
        }
      }
    }
  }
  ${UserFragment}
  ${CompanyFragment}
  ${AllianceFragment}
`;

export const STAGE_MAPPING_DELETE_MUTATION = gql`
  mutation($data: DealStageValueDeleteInput!) {
    dealStageValueDelete(data: $data) {
      success
    }
  }
`;
/**
 *
 */
export const DEAL_STAGE_LIST_QUERY = gql`
  query {
    dealStagesList {
      items {
        id
        description
        exitCriteria
        winProbability
        name
        entranceCriteria
      }
    }
  }
`;
/**
 *
 */
export const DEAL_STAGE_VALUES_LIST_QUERY = gql`
  query($filter: DealStageValueFilter) {
    dealStageValuesList(filter: $filter) {
      items {
        value
        id
        dealStageDealStageValuesRelation {
          id
          name
        }
      }
    }
  }
`;

export const DEAL_STAGES_LIST_QUERY = gql`
  query {
    dealStagesList {
      items {
        name
      }
    }
  }
`;

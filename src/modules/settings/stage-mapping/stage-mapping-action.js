import {
  STAGE_MAPPING_DELETE_MUTATION,
  STAGE_MAPPING_VALUES_CREATE_MUTATION,
  DEAL_STAGE_LIST_QUERY,
  DEAL_STAGE_VALUES_LIST_QUERY,
  DEAL_STAGES_LIST_QUERY,
} from './stage-mapping-queries';
import sessionStore, { APOLLO_CLIENT, NEW_SESSION_EVENT } from '../../../shared/SessionStore';
import { error, log } from '@cobuildlab/pure-logger';
import Flux from '@cobuildlab/flux-state';
import {
  STAGE_MAPPING_ERROR_EVENT,
  STAGE_MAPPING_DETAIL_EVENT,
  STAGE_MAPPING_UPDATE_EVENT,
} from './stage-mapping-store';

/**
 * Get dealStageList and filter for id on dealStageValues.
 *
 * @returns {Promise<void|*>}
 * @param companyId
 */
export const fetchStageMapping = async (companyId) => {
  const client = sessionStore.getState(APOLLO_CLIENT);

  let response;
  const filter = { company: { id: { equals: companyId } } };

  // Get dealStageValuesList
  try {
    response = await client.mutate({
      mutation: DEAL_STAGE_VALUES_LIST_QUERY,
      variables: { filter },
    });
  } catch (e) {
    error('fetchStageMapping:dealStageValues', e);
    return Flux.dispatchEvent(STAGE_MAPPING_DETAIL_EVENT, e);
  }

  const { dealStageValuesList } = response.data;

  // get dealStagesList
  try {
    response = await client.mutate({
      mutation: DEAL_STAGE_LIST_QUERY,
    });
  } catch (e) {
    error('fetchStageMapping:dealStagesList', e);
    return Flux.dispatchEvent(STAGE_MAPPING_DETAIL_EVENT, e);
  }
  const { dealStagesList } = response.data;

  response.data.dealStagesList = sanitizeDealStageList(dealStagesList, dealStageValuesList);

  log('fetchStageMapping', response);
  Flux.dispatchEvent(STAGE_MAPPING_DETAIL_EVENT, response.data);
  return response.data;
};

/**
 * Find values for stages.
 *
 * @param {object}dealStagesList
 * @param dealStageValuesList
 * @returns {*}
 */
const sanitizeDealStageList = (dealStagesList, dealStageValuesList) => {
  dealStagesList.items.forEach((dealStage) => {
    dealStage.values = dealStageValuesList.items.filter(
      (dealStageValue) => dealStageValue.dealStageDealStageValuesRelation.id === dealStage.id,
    );
  });

  return dealStagesList;
};

/**
 * Create value for each dealStage.
 *
 * @param client
 * @param dealStage - : deal Stage List [ {description, winCriteria , ...}].
 * @param values - : deal Stage Values List [ ].
 * @param companyId
 * @param allianceId
 * @returns {Promise<void|*>}
 */
export const createDealStageValue = async (client, dealStage, values, companyId, allianceId) => {
  if (values.length !== 0) {
    for (let i = 0, j = values.length; i < j; i++) {
      let response;

      const data = {
        alliance: { connect: { id: allianceId } },
        value: values[i],
        company: { connect: { id: companyId } },
        dealStageDealStageValuesRelation: { connect: { id: dealStage.id } },
      };

      try {
        response = await client.mutate({
          mutation: STAGE_MAPPING_VALUES_CREATE_MUTATION,
          variables: { data },
        });
      } catch (e) {
        error('updateStageMapping', e);
        return Flux.dispatchEvent(STAGE_MAPPING_ERROR_EVENT, e);
      }
      console.log('DEBUG:updateStageMapping:createDealValue', response);
    }
  }

  return dealStage;
};

/**
 * Fetch Deal Stage List.
 *
 * @returns {Promise<void|*>}Promise.
 */
export const fetchDealStage = async () => {
  const client = sessionStore.getState(APOLLO_CLIENT);

  let response;
  try {
    response = await client.mutate({
      mutation: DEAL_STAGES_LIST_QUERY,
    });
  } catch (e) {
    error('fetchStageMapping:dealStagesList', e);
    return Flux.dispatchEvent(STAGE_MAPPING_DETAIL_EVENT, e);
  }

  Flux.dispatchEvent(STAGE_MAPPING_DETAIL_EVENT, response.data);

  return response.data;
};

/**
 * Relation values with deal stage.
 *
 * @param stageMappingData
 * @param originalStageMappingValues
 * @returns {Promise<void>}
 */
export const updateStageMapping = async (stageMappingData, originalStageMappingValues) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { selectedAlliance, user } = sessionStore.getState(NEW_SESSION_EVENT);
  const companyId = user.companyUserRelation.items[0].company.id;
  const allianceId = selectedAlliance.id;

  for (let i = 0, j = stageMappingData.length; i < j; i++) {
    const dealStage = stageMappingData[i];

    const _originalStageMappingValues = originalStageMappingValues[i].map((item) => {
      return item.value;
    });
    const createNewValues = dealStage.values.filter(
      (val) => !_originalStageMappingValues.includes(val),
    ); // compare both list and return list of new values
    const deleteValues = originalStageMappingValues[i].filter(
      (val) => !dealStage.values.includes(val.value),
    ); // compare both list and return list of delete values
    let response;

    if (deleteValues.length !== 0) {
      for (let x = 0, y = deleteValues.length; x < y; x++) {
        try {
          response = await client.mutate({
            mutation: STAGE_MAPPING_DELETE_MUTATION,
            variables: { data: { id: deleteValues[x].id, force: false } },
          });
        } catch (e) {
          error('updateStageMapping', e);
          return Flux.dispatchEvent(STAGE_MAPPING_ERROR_EVENT, e);
        }

        log('updateStageMapping===deleteData', response);
      }
    }

    if (createNewValues.length !== 0) {
      await createDealStageValue(client, dealStage, createNewValues, companyId, allianceId);
    }
  }
  Flux.dispatchEvent(STAGE_MAPPING_UPDATE_EVENT);
};

import React from 'react';
import { Card, Loader, Heading } from '@8base/boost';
import invitationStore, {
  ALLIANCE_INVITATION_ERROR_EVENT,
  ALLIANCE_MEMBER_INVITATION_UPDATED_EVENT,
  ALLIANCE_MEMBER_INVITATION_DETAIL_EVENT,
} from './invitations.store';
import {
  acceptAllianceMemberInvitation,
  fetchAllianceMemberInvitation,
} from './invitations.actions';
import View from '@cobuildlab/react-flux-state';
import { PropTypes } from 'prop-types';
import * as toast from 'components/toast/Toast';
import { fetchSession } from '../../auth/auth.actions';
import sessionStore, { NEW_SESSION_EVENT } from '../../../shared/SessionStore';

/**
 * Redirect to this view from emails to automatically accept the alliance
 * invitation
 */
class AllianceMemberInvitationAcceptView extends View {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      allianceMemberInvitation: {},
    };
  }

  componentDidMount() {
    const { match } = this.props;
    if (!match.params.id) {
      toast.error('Invitation ID missing');
      return this.redirectOnError();
    }

    this.subscribe(invitationStore, ALLIANCE_INVITATION_ERROR_EVENT, (error) => {
      toast.error(error.message);
      this.redirectOnError();
    });

    this.subscribe(
      invitationStore,
      ALLIANCE_MEMBER_INVITATION_DETAIL_EVENT,
      this.autoAcceptInvitation,
    );

    this.subscribe(invitationStore, ALLIANCE_MEMBER_INVITATION_UPDATED_EVENT, async (data) => {
      const user = sessionStore.getState(NEW_SESSION_EVENT).user;
      await fetchSession();
      toast.success('Alliance Membership Invitation Accepted');
      const id = this.state.allianceMemberInvitation.alliance.id;

      // If user profile is incomplete, redirect to user profile
      if (!user.firstName || !user.lastName) {
        this.props.history.push('/settings/user-profile');
      } else {
        // Else go to Alliance details view
        this.props.history.push(`/settings/alliance-management/${id}`);
      }
    });

    fetchAllianceMemberInvitation(match.params.id);
  }

  redirectOnError = () => {
    this.props.history.push('/invitations/alliance-member-Invitations');
  };

  autoAcceptInvitation = (actionData) => {
    const { allianceMemberInvitation } = actionData;
    if (!allianceMemberInvitation) {
      toast.error('Invitation not found');
      return this.redirectOnError();
    }

    this.setState({ allianceMemberInvitation }, () =>
      setTimeout(() => acceptAllianceMemberInvitation(allianceMemberInvitation), 2000),
    );
  };

  render() {
    const { loading } = this.state;
    let content = <></>;

    if (loading) {
      content = (
        <div className="invitation-message">
          <Loader stretch />
          <p>Accepting Invitation</p>
        </div>
      );
    }

    return (
      <React.Fragment>
        <Card.Header>
          <Heading type="h4" text="Accepting Alliance Membership Invitation" />
        </Card.Header>
        <Card.Body borderRadius="all" style={{ padding: 20, textAlign: 'center' }}>
          {content}
        </Card.Body>
      </React.Fragment>
    );
  }
}

AllianceMemberInvitationAcceptView.propTypes = {
  history: PropTypes.object.isRequired,
};

export default AllianceMemberInvitationAcceptView;

import gql from 'graphql-tag';
import { AllianceInvitationFragment, AllianceMemberInvitationFragment } from '../../auth/queries';

/**
 * Updates the Alliance Mutation Entity
 */
export const UPDATE_ALLIANCE_INVITATION_MUTATION = gql`
  mutation($data: AllianceInvitationUpdateInput!) {
    allianceInvitationUpdate(data: $data) {
      id
    }
  }
`;

/**
 * Create a CompanyUser Relationship
 */
export const CREATE_COMPANY_USER_MUTATION = gql`
  mutation($data: CompanyUserCreateInput!) {
    companyUserCreate(data: $data) {
      id
    }
  }
`;

/**
 * Delete an Invitation
 */
export const ALLIANCE_INVITATION_DELETE_MUTATION = gql`
  mutation($data: AllianceInvitationDeleteInput!) {
    allianceInvitationDelete(data: $data) {
      success
    }
  }
`;

export const ALLIANCE_INVITATION_QUERY = gql`
  query($id: ID!) {
    allianceInvitation(id: $id) {
      ...AllianceInvitationFragment
    }
  }
  ${AllianceInvitationFragment}
`;

export const ALLIANCE_MEMBER_INVITATIONS_QUERY = gql`
  query($filter: AllianceMemberInvitationFilter!) {
    allianceMemberInvitationsList(filter: $filter) {
      count
      items {
        ...AllianceMemberInvitationFragment
      }
    }
  }
  ${AllianceMemberInvitationFragment}
`;

export const ALLIANCE_MEMBER_INVITATION_QUERY = gql`
  query($id: ID!) {
    allianceMemberInvitation(id: $id) {
      ...AllianceMemberInvitationFragment
    }
  }
  ${AllianceMemberInvitationFragment}
`;

export const UPDATE_ALLIANCE_MEMBER_INVITATION_MUTATION = gql`
  mutation($data: AllianceMemberInvitationUpdateInput!) {
    allianceMemberInvitationUpdate(data: $data) {
      id
    }
  }
`;

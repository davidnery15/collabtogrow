import React from 'react';
import { Card, Table, Heading } from '@8base/boost';
import { ListCardBody } from 'components/card/ListCardBody';
import * as toast from 'components/toast/Toast';
import invitationStore, {
  ALLIANCE_INVITATION_ERROR_EVENT,
  ALLIANCE_INVITATION_NO_COMPANY_ERROR_EVENT,
  ALLIANCE_INVITATION_UPDATED_EVENT,
} from './invitations.store';
import * as R from 'ramda';
import { acceptAllianceInvitation } from './invitations.actions';
import View from '@cobuildlab/react-flux-state';
import YesNoDialog from '../../../components/dialogs/YesNoDialog';
import sessionStore, {
  INVITATIONS_EVENT,
  SESSION_ERROR,
  NEW_SESSION_EVENT,
} from '../../../shared/SessionStore';
import { PropTypes } from 'prop-types';
import { fetchInvitations } from '../../auth/auth.actions';
import { ACCEPT_ALLIANCE_INVITATION } from './index';
import Moment from 'react-moment';
import Status from '../../../components/Status';
import { ActionButton } from '../../../components/buttons/ActionButton';
import ErrorDialog from 'components/dialogs/ErrorDialog';
import { fetchSession } from '../../auth/auth.actions';
import { sortByPendingInvitation } from './invitations-utils';
import { SelectCompany } from './components/SelectCompany';
import { getMyCompanies } from '../company-management/company.actions';
import { canAcceptAllianceInvitation } from './invitations-permissions';

/**
 * List All the Alliances Invitations
 */
class AllianceInvitationListView extends View {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: true,
      invitation: null,
      yesNoDialogIsOpen: false,
      yesNoDialogTitle: '',
      yesNoDialogDescription: '',
      isGonnaAccept: false,
      noCompanyErrorDialogIsOpen: false,
      isOpen: false,
      companyId: null,
    };
    this.user = sessionStore.getState(NEW_SESSION_EVENT).user;
    this.companyUsers = getMyCompanies();
  }

  componentDidMount() {
    const { history } = this.props;

    this.subscribe(sessionStore, SESSION_ERROR, (data) => {
      toast.error(data.message);
      this.setState({
        loading: false,
      });
    });

    this.subscribe(invitationStore, ALLIANCE_INVITATION_NO_COMPANY_ERROR_EVENT, (data) => {
      this.setState({ noCompanyErrorDialogIsOpen: true });
    });

    this.subscribe(invitationStore, ALLIANCE_INVITATION_ERROR_EVENT, (data) => {
      toast.error(data.message);
      this.setState({
        loading: false,
      });
    });

    this.subscribe(sessionStore, INVITATIONS_EVENT, (data) => {
      this.setState({
        data: data.allianceInvitationsList.items.sort(sortByPendingInvitation),
        loading: false,
      });
    });

    this.subscribe(invitationStore, ALLIANCE_INVITATION_UPDATED_EVENT, async (data) => {
      await fetchSession();
      const id = this.state.invitation.alliance.id;
      toast.success('Alliance invitation successfully accepted');
      history.push(`/settings/alliance-management/${id}`);
    });

    fetchInvitations();
  }

  onSelectForAccept = (invitation, name) => {
    if (this.companyUsers.length > 1) {
      this.setState({
        isOpen: true,
        invitation,
      });
    } else {
      this.setState({
        invitation,
        isGonnaAccept: true,
        yesNoDialogIsOpen: true,
        yesNoDialogTitle: name,
        yesNoDialogDescription: ACCEPT_ALLIANCE_INVITATION,
      });
    }
  };

  onYes = () => {
    this.setState(
      {
        yesNoDialogIsOpen: false,
        loading: true,
      },
      () => {
        if (this.state.isGonnaAccept)
          acceptAllianceInvitation(this.state.invitation, this.companyUsers[0]);
      },
    );
  };
  onAccept = () => {
    const { invitation, companyId } = this.state;
    let companySelected = this.companyUsers.find((companyUser) => {
      return companyUser.company.id === companyId;
    });
    this.setState(
      {
        isOpen: false,
        loading: true,
      },
      () => setTimeout(() => acceptAllianceInvitation(invitation, companySelected), 2000),
    );
  };
  onClose = () => {
    this.setState({
      yesNoDialogIsOpen: false,
    });
  };

  onNoCompanyErrorDialogOk = () => {
    this.setState({ noCompanyErrorDialogIsOpen: false });
    this.props.history.push('/settings/company-management/create');
  };
  onChangeCompany = (companyId) => {
    this.setState({
      companyId: companyId,
    });
  };
  onClose = () => {
    this.setState({
      isOpen: false,
    });
  };

  render() {
    const {
      yesNoDialogIsOpen,
      yesNoDialogTitle,
      yesNoDialogDescription,
      noCompanyErrorDialogIsOpen,
      isOpen,
      companyId,
    } = this.state;
    const tableHeaders = ['Alliance Name', 'Sent to', 'Status', 'Date of Response'];
    const columnSize = `repeat(${tableHeaders.length}, 1fr) 200px`;
    let selector = <></>;
    if (this.companyUsers.length > 1) {
      selector = (
        <SelectCompany
          isOpen={isOpen}
          onClose={this.onClose}
          companyUsers={this.companyUsers}
          onChange={this.onChangeCompany}
          onAccept={this.onAccept}
          companyId={companyId}
          title={'Select Your Company'}
        />
      );
    }

    return (
      <React.Fragment>
        <Card.Header>
          <Heading type="h4" text="My Alliances" />
        </Card.Header>
        <ListCardBody>
          <Table>
            <Table.Header columns={columnSize}>
              {tableHeaders.map((header, index) => (
                <Table.HeaderCell key={index}>{header}</Table.HeaderCell>
              ))}
              <Table.HeaderCell />
            </Table.Header>
            <Table.Body
              loading={this.state.loading}
              data={this.state.data}
              className="card-body-list">
              {(invitation) => {
                const _canAcceptAllianceInvitation = canAcceptAllianceInvitation(
                  this.user,
                  invitation,
                );

                return (
                  <Table.BodyRow columns={columnSize} key={invitation.id}>
                    <Table.BodyCell>
                      {R.pathOr('Unititled', ['alliance', 'name'], invitation)}
                    </Table.BodyCell>
                    <Table.BodyCell>{R.pathOr('', ['email'], invitation)}</Table.BodyCell>
                    <Table.BodyCell>
                      <Status status={R.pathOr('', ['status'], invitation)} />
                    </Table.BodyCell>
                    <Table.BodyCell>
                      {invitation.dateOfResponse === null ? (
                        ''
                      ) : (
                        <Moment format="MMMM Do, YYYY">
                          {new Date(invitation.dateOfResponse)}
                        </Moment>
                      )}
                    </Table.BodyCell>
                    <Table.BodyCell>
                      {_canAcceptAllianceInvitation ? (
                        <ActionButton
                          text={'Accept'}
                          onClick={() =>
                            this.onSelectForAccept(invitation, invitation.alliance.name)
                          }
                        />
                      ) : null}
                    </Table.BodyCell>
                  </Table.BodyRow>
                );
              }}
            </Table.Body>
          </Table>
        </ListCardBody>
        <YesNoDialog
          isOpen={yesNoDialogIsOpen}
          onYes={this.onYes}
          onNo={this.onClose}
          onClose={this.onClose}
          text={yesNoDialogDescription}
          title={yesNoDialogTitle}
        />
        <ErrorDialog
          isOpen={noCompanyErrorDialogIsOpen}
          onOk={this.onNoCompanyErrorDialogOk}
          text={'You must create a Company in order to accept Alliance Invitations'}
          okText={'Create Company'}
          title={'Error accepting the Alliance Invitation'}
        />
        {selector}
      </React.Fragment>
    );
  }
}

// // TODO:
AllianceInvitationListView.propTypes = {
  history: PropTypes.object.isRequired,
};

export default AllianceInvitationListView;

export const ACCEPT_ALLIANCE_INVITATION =
  'Are you sure you want to Accept this Alliance Invitation?';
export const REJECT_ALLIANCE_INVITATION =
  'Are you sure you want to Reject this Alliance Invitation?';
export const DELETE_ALLIANCE_INVITATION =
  'Are you sure you want to Delete this Alliance Invitation?';

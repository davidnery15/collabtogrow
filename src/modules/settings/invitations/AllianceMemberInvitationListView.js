import React from 'react';
import { Card, Table, Heading } from '@8base/boost';
import { ListCardBody } from '../../../components/card/ListCardBody';
import * as toast from '../../../components/toast/Toast';
import invitationStore, {
  ALLIANCE_INVITATION_ERROR_EVENT,
  ALLIANCE_MEMBER_INVITATION_UPDATED_EVENT,
} from './invitations.store';
import * as R from 'ramda';
import { acceptAllianceMemberInvitation } from './invitations.actions';
import View from '@cobuildlab/react-flux-state';
import YesNoDialog from '../../../components/dialogs/YesNoDialog';
import sessionStore, {
  INVITATIONS_EVENT,
  SESSION_ERROR,
  NEW_SESSION_EVENT,
} from '../../../shared/SessionStore';
import { PropTypes } from 'prop-types';
import { fetchInvitations } from '../../auth/auth.actions';
import Moment from 'react-moment';
import Status from '../../../components/Status';
import { onErrorMixin } from '../../../shared/mixins';
import { ActionButton } from '../../../components/buttons/ActionButton';
import { fetchSession } from '../../auth/auth.actions';
import { sortByPendingInvitation } from './invitations-utils';
import { canAcceptAllianceMemberInvitation } from './invitations-permissions';

/**
 * List All the Alliances Member Invitations
 */
class AllianceMemberInvitationListView extends View {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      invitations: [],
      invitation: null,
      acceptModalIsOpen: false,
    };
    this.onError = onErrorMixin.bind(this);
  }

  componentDidMount() {
    const { history } = this.props;
    this.user = sessionStore.getState(NEW_SESSION_EVENT).user;

    this.subscribe(sessionStore, SESSION_ERROR, this.onError);

    this.subscribe(invitationStore, ALLIANCE_INVITATION_ERROR_EVENT, this.onError);

    this.subscribe(sessionStore, INVITATIONS_EVENT, (data) => {
      this.setState({
        invitations: data.allianceMemberInvitationsList.items.sort(sortByPendingInvitation),
        loading: false,
      });
    });

    this.subscribe(invitationStore, ALLIANCE_MEMBER_INVITATION_UPDATED_EVENT, async (data) => {
      await fetchSession();
      const id = this.state.invitation.alliance.id;
      toast.success('Alliance Membership invitation successfully accepted');
      history.push(`/settings/alliance-management/${id}`);
    });

    fetchInvitations();
  }

  onSelectForAccept = (invitation) => {
    this.setState({
      invitation,
      acceptModalIsOpen: true,
    });
  };

  onYesAccept = () => {
    this.setState(
      {
        acceptModalIsOpen: false,
        loading: true,
      },
      () => {
        acceptAllianceMemberInvitation(this.state.invitation);
      },
    );
  };

  onCloseAccept = () => {
    this.setState({
      acceptModalIsOpen: false,
    });
  };

  render() {
    const { acceptModalIsOpen, invitations, loading } = this.state;
    const tableHeaders = ['Company Name', 'Alliance Name', 'Sent to', 'Status', 'Date of Response'];
    const columnSize = `repeat(${tableHeaders.length}, 1fr) 200px`;

    return (
      <React.Fragment>
        <Card.Header>
          <Heading type="h4" text="Alliance Membership" />
        </Card.Header>
        <ListCardBody>
          <Table>
            <Table.Header columns={columnSize}>
              {tableHeaders.map((header, index) => (
                <Table.HeaderCell key={index}>{header}</Table.HeaderCell>
              ))}
              <Table.HeaderCell />
            </Table.Header>
            <Table.Body loading={loading} data={invitations} className="card-body-list">
              {(invitation) => {
                const _canAcceptAllianceMemberInvitation = canAcceptAllianceMemberInvitation(
                  this.user,
                  invitation,
                );

                return (
                  <Table.BodyRow columns={columnSize} key={invitation.id}>
                    <Table.BodyCell>
                      {R.pathOr('Unititled', ['company', 'name'], invitation)}
                    </Table.BodyCell>
                    <Table.BodyCell>
                      {R.pathOr('Unititled', ['alliance', 'name'], invitation)}
                    </Table.BodyCell>
                    <Table.BodyCell>{R.pathOr('', ['email'], invitation)}</Table.BodyCell>
                    <Table.BodyCell>
                      <Status status={R.pathOr('', ['status'], invitation)} />
                    </Table.BodyCell>
                    <Table.BodyCell>
                      {invitation.dateOfResponse === null ? (
                        'No date'
                      ) : (
                        <Moment format="MMMM Do, YYYY">
                          {new Date(invitation.dateOfResponse)}
                        </Moment>
                      )}
                    </Table.BodyCell>
                    <Table.BodyCell>
                      {_canAcceptAllianceMemberInvitation ? (
                        <ActionButton
                          text={'Accept'}
                          onClick={() => this.onSelectForAccept(invitation)}
                        />
                      ) : null}
                    </Table.BodyCell>
                  </Table.BodyRow>
                );
              }}
            </Table.Body>
          </Table>
        </ListCardBody>
        <YesNoDialog
          isOpen={acceptModalIsOpen}
          onYes={this.onYesAccept}
          onNo={this.onCloseAccept}
          onClose={this.onCloseAccept}
          text={'Are you sure you want to Accept this Alliance Membership Invitation?'}
          title={'Accept Invitation'}
        />
      </React.Fragment>
    );
  }
}

AllianceMemberInvitationListView.propTypes = {
  history: PropTypes.object.isRequired,
};

export default AllianceMemberInvitationListView;

import React from 'react';
import { Card, Loader, Heading, Column, Paragraph } from '@8base/boost';
import invitationStore, {
  ALLIANCE_INVITATION_ERROR_EVENT,
  ALLIANCE_INVITATION_NO_COMPANY_ERROR_EVENT,
  ALLIANCE_INVITATION_UPDATED_EVENT,
  ALLIANCE_INVITATION_DETAIL_EVENT,
} from './invitations.store';
import { acceptAllianceInvitation, fetchAllianceInvitation } from './invitations.actions';
import View from '@cobuildlab/react-flux-state';
import { PropTypes } from 'prop-types';
import * as toast from 'components/toast/Toast';
import CenterParagraph from '../../../components/CenterParagraph';
import { ActionButton } from '../../../components/buttons/ActionButton';
import businessCase from 'images/wizard/Business_Case_Graphic.svg';
import ErrorDialog from 'components/dialogs/ErrorDialog';
import { fetchSession } from '../../auth/auth.actions';
import { SelectCompany } from './components/SelectCompany';
import { getMyCompanies } from '../company-management/company.actions';

/**
 * Redirect to this view from emails to automatically accept the alliance
 * invitation
 */
class AllianceInvitationAcceptView extends View {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      allianceInvitation: {},
      noCompanyErrorDialogIsOpen: false,
      isOpen: false,
      companyUsers: [],
      options: [],
      allianceAccepted: false,
      companyId: null,
    };
  }

  componentDidMount() {
    this.subscribe(invitationStore, ALLIANCE_INVITATION_ERROR_EVENT, (error) => {
      toast.error(error.message);
      this.redirectOnError();
    });

    this.subscribe(invitationStore, ALLIANCE_INVITATION_NO_COMPANY_ERROR_EVENT, (data) => {
      this.setState({ noCompanyErrorDialogIsOpen: true });
    });

    this.subscribe(invitationStore, ALLIANCE_INVITATION_DETAIL_EVENT, (data) => {
      const { allianceInvitation } = data;
      const filteredCompanies = getMyCompanies();

      if (filteredCompanies.length === 1) {
        this.AcceptInvitation(allianceInvitation, filteredCompanies[0]);
      } else if (filteredCompanies.length > 1) {
        this.setState({
          companyUsers: filteredCompanies,
          allianceInvitation,
          isOpen: true,
        });
      } else if (filteredCompanies.length === 0) {
        this.AcceptInvitation(allianceInvitation, null);
      }
    });

    this.subscribe(invitationStore, ALLIANCE_INVITATION_UPDATED_EVENT, async (data) => {
      await fetchSession();
      toast.success('Alliance Invitation Accepted');
      this.setState({
        loading: false,
        allianceAccepted: true,
      });
    });

    this.getInvitationData();
  }

  redirectOnError = () => {
    this.props.history.push('/invitations/alliance-Invitations');
  };

  getInvitationData = () => {
    const id = this.props.match.params.id;
    if (!id) {
      toast.error('Invitation ID missing');
      return this.redirectOnError();
    }

    fetchAllianceInvitation(id);
  };

  AcceptInvitation = (allianceInvitation, company) => {
    if (!allianceInvitation) {
      toast.error('Invitation not found');
      return this.redirectOnError();
    }

    this.setState({ allianceInvitation }, () =>
      setTimeout(() => acceptAllianceInvitation(allianceInvitation, company), 2000),
    );
  };

  onAccept = () => {
    const { allianceInvitation, companyUsers, companyId } = this.state;
    let companySelected = companyUsers.find((companyRelation) => {
      return companyRelation.company.id === companyId;
    });
    this.setState({
      isOpen: false,
    });
    this.AcceptInvitation(allianceInvitation, companySelected);
  };

  goToAllianceManagement = () => {
    const id = this.state.allianceInvitation.alliance.id;
    this.props.history.push(`/settings/alliance-management/${id}`);
  };

  onNoCompanyErrorDialogOk = () => {
    this.setState({ noCompanyErrorDialogIsOpen: false });
    this.props.history.push('/settings/company-management/create');
  };
  onChangeCompany = (companyId) => {
    this.setState({
      companyId: companyId,
    });
  };
  onClose = () => {
    this.setState({
      isOpen: false,
    });
    this.props.history.push('/invitations/alliance-invitations');
  };

  render() {
    const {
      loading,
      noCompanyErrorDialogIsOpen,
      allianceAccepted,
      isOpen,
      companyUsers,
      companyId,
    } = this.state;
    let content = <></>;
    let selector = <></>;

    if (loading) {
      content = <Loader stretch />;
    }

    if (allianceAccepted) {
      content = (
        <Column alignItems={'center'}>
          <Paragraph
            style={{
              opacity: '0.9',
              color: '#323C47',
              fontSize: 16,
              fontWeight: 600,
            }}>
            You have accepted the Alliance Invitation!
          </Paragraph>
          <img style={{ width: 212 }} src={businessCase} alt="CollabToGrow" />
          <CenterParagraph>Navigate to your Alliance to start Collaborating!</CenterParagraph>
          <ActionButton onClick={this.goToAllianceManagement} text="Alliance Details" />
        </Column>
      );
    }
    if (companyUsers.length >= 1) {
      selector = (
        <SelectCompany
          isOpen={isOpen}
          companyUsers={companyUsers}
          onChange={this.onChangeCompany}
          onAccept={this.onAccept}
          companyId={companyId}
          title={'Select Your Company'}
          onClose={this.onClose}
        />
      );
    }

    return (
      <React.Fragment>
        <Card.Header>
          <Heading type="h4" text="Accepting Alliance Invitation" />
        </Card.Header>
        <Card.Body borderRadius="all" style={{ padding: 20, textAlign: 'center' }}>
          {content}
        </Card.Body>
        <ErrorDialog
          isOpen={noCompanyErrorDialogIsOpen}
          onOk={this.onNoCompanyErrorDialogOk}
          text={'You must create a Company in order to accept Alliance Invitations'}
          okText={'Create Company'}
          title={'Error accepting the Alliance Invitation'}
        />
        {selector}
      </React.Fragment>
    );
  }
}

AllianceInvitationAcceptView.propTypes = {
  history: PropTypes.object.isRequired,
};

export default AllianceInvitationAcceptView;

import { areEmailsEqual } from '../../../shared/utils';
import { ALLIANCE_INVITATION_PENDING } from '../../../shared/status';

/**
 * canAcceptAllianceInvitation
 * @param  {User} user
 * @param  {Invitation} invitation
 * @return {Boolean}
 */
export const canAcceptAllianceInvitation = (user, invitation) => {
  if (!areEmailsEqual(user.email, invitation.email)) return false;

  if (invitation.status === ALLIANCE_INVITATION_PENDING) return true;
};

/**
 * canAcceptAllianceMemberInvitation
 * @param  {User} user
 * @param  {Invitation} invitation
 * @return {Boolean}
 */
export const canAcceptAllianceMemberInvitation = (user, invitation) => {
  if (!areEmailsEqual(user.email, invitation.email)) return false;

  if (invitation.status === ALLIANCE_INVITATION_PENDING) return true;
};

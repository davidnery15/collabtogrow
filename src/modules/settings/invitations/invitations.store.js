import Flux from '@cobuildlab/flux-state';

/**
 * Event that triggers a Alliance Invitation Error
 * @type {string}
 */
export const ALLIANCE_INVITATION_ERROR_EVENT = 'onAllianceInivtationError';

/**
 * Event that triggers a Alliance Invitation Error (When you try to accept an invitation but you have no company)
 * @type {string}
 */
export const ALLIANCE_INVITATION_NO_COMPANY_ERROR_EVENT = 'onAllianceInivtationNoCompanyError';

/**
 * Event that triggers a Alliance invitation Update
 * @type {string}
 */
export const ALLIANCE_INVITATION_UPDATED_EVENT = 'onAllianceInvitationUpdated';

/**
 * Event that triggers a Alliance member invitation Update
 * @type {string}
 */
export const ALLIANCE_MEMBER_INVITATION_UPDATED_EVENT = 'onAllianceMemberInvitationUpdated';

/**
 * Event that triggers a Alliance member invitation List
 * @type {string}
 */
export const ALLIANCE_MEMBER_INVITATION_LIST_EVENT = 'onAllianceMemberInvitationList';

/**
 * Event that triggers a Alliance member invitation Detail
 * @type {string}
 */
export const ALLIANCE_MEMBER_INVITATION_DETAIL_EVENT = 'onAllianceMemberInvitationDetail';

/**
 * Event that triggers a Alliance Deleted
 * @type {string}
 */
export const ALLIANCE_INVITATION_DELETED_EVENT = 'onAllianceInvitationDeleted';

/**
 * Event that triggers a Alliance invitation detail
 * @type {string}
 */
export const ALLIANCE_INVITATION_DETAIL_EVENT = 'onAllianceInvitationDetail';

/**
 * Hold the Alliance Inivtation Data
 */
class AllianceInvitationStore extends Flux.DashStore {
  constructor() {
    super();
    this.addEvent(ALLIANCE_INVITATION_ERROR_EVENT);
    this.addEvent(ALLIANCE_INVITATION_NO_COMPANY_ERROR_EVENT);
    this.addEvent(ALLIANCE_INVITATION_UPDATED_EVENT);
    this.addEvent(ALLIANCE_INVITATION_DELETED_EVENT);
    this.addEvent(ALLIANCE_MEMBER_INVITATION_UPDATED_EVENT);
    this.addEvent(ALLIANCE_MEMBER_INVITATION_LIST_EVENT);
    this.addEvent(ALLIANCE_MEMBER_INVITATION_DETAIL_EVENT);
    this.addEvent(ALLIANCE_INVITATION_DETAIL_EVENT);
  }
}

export default new AllianceInvitationStore();

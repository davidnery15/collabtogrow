import Flux from '@cobuildlab/flux-state';
import {
  ALLIANCE_INVITATION_ERROR_EVENT,
  ALLIANCE_INVITATION_NO_COMPANY_ERROR_EVENT,
  ALLIANCE_INVITATION_UPDATED_EVENT,
  ALLIANCE_MEMBER_INVITATION_UPDATED_EVENT,
  ALLIANCE_INVITATION_DELETED_EVENT,
  ALLIANCE_INVITATION_DETAIL_EVENT,
  ALLIANCE_MEMBER_INVITATION_DETAIL_EVENT,
} from './invitations.store';
import sessionStore, { APOLLO_CLIENT, NEW_SESSION_EVENT } from '../../../shared/SessionStore';
import { areEmailsEqual } from '../../../shared/utils';
import {
  UPDATE_ALLIANCE_INVITATION_MUTATION,
  UPDATE_ALLIANCE_MEMBER_INVITATION_MUTATION,
  CREATE_COMPANY_USER_MUTATION,
  ALLIANCE_INVITATION_DELETE_MUTATION,
  ALLIANCE_INVITATION_QUERY,
  ALLIANCE_MEMBER_INVITATION_QUERY,
} from './invitations.queries';
import { fetchMembersAction } from '../company-management/company.actions';
import { IntegrityError } from '../../../shared/errors';
import { ALLIANCE_UPDATE_MUTATION } from '../alliance-management/alliance-queries';
import { fetchAlliance } from '../alliance-management/alliance-actions';
import {
  ALLIANCE_INVITATION_ACCEPTED,
  ALLIANCE_MEMBER_INVITATION_ACCEPTED,
  ALLIANCE_INVITATION_REJECTED,
  ALLIANCE_INVITATION_CANCELLED,
} from '../../../shared/status';
import {
  ALLIANCE_ADMINISTRATOR,
  COMPANY_ADMINISTRATOR,
  ALLIANCE_COLLABORATOR,
  COMPANY_PORTFOLIO_OWNER,
  COMPANY_MEMBER,
} from '../../../shared/roles';
import { fetchRoles } from '../../dashboard/dashboard-actions';
import { createAllianceMember } from '../alliance-management/alliance-actions';
import { log, error } from '@cobuildlab/pure-logger';
import {
  canAcceptAllianceInvitation,
  canAcceptAllianceMemberInvitation,
} from './invitations-permissions';

/**
 * Accepts an Alliance Invitation.
 *
 * @param invitation - The Alliance Invitation to be accepted.
 * @param companyUser
 */
export const acceptAllianceInvitation = async (invitation, companyUser) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { user } = sessionStore.getState(NEW_SESSION_EVENT);

  if (invitation.status === ALLIANCE_INVITATION_ACCEPTED) {
    return Flux.dispatchEvent(
      ALLIANCE_INVITATION_ERROR_EVENT,
      new IntegrityError('Alliance Invitation Already Accepted'),
    );
  }

  if (!areEmailsEqual(user.email, invitation.email)) {
    return Flux.dispatchEvent(
      ALLIANCE_INVITATION_ERROR_EVENT,
      new IntegrityError('Invitations can only be accepted by their owner'),
    );
  }

  if (!canAcceptAllianceInvitation(user, invitation)) {
    return Flux.dispatchEvent(
      ALLIANCE_INVITATION_ERROR_EVENT,
      new IntegrityError(`Permision Denied. Can't accept Alliance Invitation`),
    );
  }

  // Fetch the User company, to check if it has one
  if (!companyUser) {
    return Flux.dispatchEvent(
      ALLIANCE_INVITATION_NO_COMPANY_ERROR_EVENT,
      new IntegrityError('You must create a company in order to accept alliance invitations'),
    );
  }

  const companyInformation = companyUser.role.name === COMPANY_ADMINISTRATOR;

  if (companyInformation === false)
    return Flux.dispatchEvent(
      ALLIANCE_INVITATION_ERROR_EVENT,
      new IntegrityError('Only the Owner of a Company can Accept Invitations'),
    );
  // Update Invitation to status ACCEPTED
  const invitationData = {
    id: invitation.id,
    status: ALLIANCE_INVITATION_ACCEPTED,
    dateOfResponse: new Date(),
  };

  let response;
  try {
    response = await client.mutate({
      mutation: UPDATE_ALLIANCE_INVITATION_MUTATION,
      variables: { data: invitationData },
    });
  } catch (e) {
    error('acceptAllianceInvitation', e);
    return Flux.dispatchEvent(ALLIANCE_INVITATION_ERROR_EVENT, e);
  }

  // get the Alliance ID of the Invitation
  const allianceId = invitation.alliance.id;
  const roles = await fetchRoles();
  const allianceAdministratorRole = roles.find((role) => role.name === ALLIANCE_ADMINISTRATOR);
  const companyId = companyUser.company.id;
  const allianceData = {
    id: allianceId,
    // Join the Company to the Alliance
    partnerCompany: {
      connect: { id: companyId },
    },
    // And the User as an Administrator to the Alliance
    allianceUserAllianceRelation: {
      create: [
        {
          companyUser: { connect: { id: companyUser.id } },
          role: { connect: { id: allianceAdministratorRole.id } },
        },
      ],
    },
  };
  try {
    await client.mutate({
      mutation: ALLIANCE_UPDATE_MUTATION,
      variables: { data: allianceData },
    });
  } catch (e) {
    error('acceptAllianceInvitation', e);
    console.log(e);
    return Flux.dispatchEvent(ALLIANCE_INVITATION_ERROR_EVENT, e);
  }

  // Cancel Every Other Invitation to that Alliance
  const allianceFetch = await fetchAlliance(allianceId);
  const { allianceInvitationRelation } = allianceFetch.alliance;
  const otherInvitations = allianceInvitationRelation.items.filter(
    (allianceInvitation) => invitation.id !== allianceInvitation.id,
  );
  otherInvitations.forEach(async (otherInvitation) => {
    try {
      await client.mutate({
        mutation: UPDATE_ALLIANCE_INVITATION_MUTATION,
        variables: {
          data: {
            id: otherInvitation.id,
            status: ALLIANCE_INVITATION_CANCELLED,
            dateOfResponse: new Date(),
          },
        },
      });
    } catch (e) {
      error('acceptAllianceInvitation', e);
      return Flux.dispatchEvent(ALLIANCE_INVITATION_ERROR_EVENT, e);
    }
  });

  try {
    await addPortfolioOwnersAsAllianceCollaborator(allianceId, companyId, roles);
  } catch (e) {
    error('addPortfolioOwnersAsAllianceCollaborator', e);
    return Flux.dispatchEvent(ALLIANCE_INVITATION_ERROR_EVENT, e);
  }

  // Update Alliance with the partnerCompany
  Flux.dispatchEvent(ALLIANCE_INVITATION_UPDATED_EVENT, response.data);
  return response.data;
};

/**
 * Rejects an Alliance Invitation.
 *
 * @param id - The id of the Alliance Invitation to be rejected.
 */
export const rejectAllianceInvitation = async ({ id }) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const data = {
    id,
    status: ALLIANCE_INVITATION_REJECTED,
    dateOfResponse: new Date(),
  };
  let response;
  try {
    response = await client.mutate({
      mutation: UPDATE_ALLIANCE_INVITATION_MUTATION,
      variables: { data },
    });
  } catch (e) {
    error('rejectAllianceInvitation', e);
    return Flux.dispatchEvent(ALLIANCE_INVITATION_ERROR_EVENT, e);
  }
  log('rejectAllianceInvitation', response);
  Flux.dispatchEvent(ALLIANCE_INVITATION_UPDATED_EVENT, response.data);
  return response.data;
};

/**
 * Add portfolio owners of the company as alliance collaborators of the alliance
 * Used on alliance creation & allianceInvitation accept to add the company
 * portfolio owners as alliance collaborator of that alliance.
 *
 * @param allianceId
 * @param companyId
 * @param roles -   to avoid another query on roles
 */
export const addPortfolioOwnersAsAllianceCollaborator = async (allianceId, companyId, roles) => {
  log('addPortfolioOwnersAsAllianceCollaborator', allianceId, companyId, roles);
  const client = sessionStore.getState(APOLLO_CLIENT);
  const allianceCollaboratorRole = roles.find((role) => role.name === ALLIANCE_COLLABORATOR);
  const {
    companyUsersList: { items: members },
  } = await fetchMembersAction(companyId);

  log('MembersOfCompany', members);
  const membersToAdd = members.filter((member) => member.role.name === COMPANY_PORTFOLIO_OWNER);
  log('MembersToAdd', membersToAdd);

  const createMemberPromise = async (member) => {
    const allianceData = {
      id: allianceId,
      allianceUserAllianceRelation: {
        create: [
          {
            companyUser: { connect: { id: member.id } },
            role: { connect: { id: allianceCollaboratorRole.id } },
          },
        ],
      },
    };

    let response;
    try {
      response = await client.mutate({
        mutation: ALLIANCE_UPDATE_MUTATION,
        variables: { data: allianceData },
      });
      log('addPortfolioOwnerAsAllianceCollaborator', response);
    } catch (e) {
      throw e;
    }

    return response;
  };

  const responses = await Promise.all(membersToAdd.map(createMemberPromise));

  return responses;
};

/**
 * Delete an Alliance Invitation.
 *
 * @returns {Promise<void>}
 * @param invitation
 */
export const deleteAllianceInvitation = async (invitation) => {
  const client = sessionStore.getState(APOLLO_CLIENT);

  if (invitation.status === ALLIANCE_INVITATION_ACCEPTED)
    return Flux.dispatchEvent(
      ALLIANCE_INVITATION_ERROR_EVENT,
      new IntegrityError("You can't delete a Invitation that has been Accepted"),
    );

  let response;

  try {
    response = await client.mutate({
      mutation: ALLIANCE_INVITATION_DELETE_MUTATION,
      variables: { data: { id: invitation.id } },
    });
  } catch (e) {
    error('deleteAllianceInvitation', e);
    return Flux.dispatchEvent(ALLIANCE_INVITATION_ERROR_EVENT, e);
  }

  log('deleteAllianceInvitation', response);
  Flux.dispatchEvent(ALLIANCE_INVITATION_DELETED_EVENT, response.data);
  return response.data;
};

/**
 * Fetch a single Alliance Invitation.
 *
 * @param  {string}  id -   The invitation id.
 * @returns {Promise}
 */
export const fetchAllianceInvitation = async (id) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  let response;
  try {
    response = await client.query({
      query: ALLIANCE_INVITATION_QUERY,
      variables: { id },
      fetchPolicy: 'network-only',
    });
  } catch (e) {
    error('fetchAllianceInvitation', e);
    return Flux.dispatchEvent(ALLIANCE_INVITATION_ERROR_EVENT, e);
  }

  log('fetchAllianceInvitation', response);
  Flux.dispatchEvent(ALLIANCE_INVITATION_DETAIL_EVENT, response.data);
  return response.data;
};

/**
 * Fetch a single Alliance Membership Invitation.
 *
 * @param  {string}  id -   The invitation id.
 * @returns {Promise}
 */
export const fetchAllianceMemberInvitation = async (id) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  console.log('fetchAllianceMemberInvitation', id);
  let response;
  try {
    response = await client.query({
      query: ALLIANCE_MEMBER_INVITATION_QUERY,
      variables: { id },
      fetchPolicy: 'network-only',
    });
  } catch (e) {
    error('fetchAllianceMemberInvitation', e);
    return Flux.dispatchEvent(ALLIANCE_INVITATION_ERROR_EVENT, e);
  }

  log('fetchAllianceMemberInvitation', response);
  Flux.dispatchEvent(ALLIANCE_MEMBER_INVITATION_DETAIL_EVENT, response.data);
  return response.data;
};

/**
 * Create a company user.
 *
 * @param  {string}  companyId
 * @param  {string}  roleId
 * @param  {string}  userId
 */
export const createCompanyMember = async (companyId, roleId, userId) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  log('createCompanyMember: ', companyId, roleId, userId);

  const companyUserData = {
    company: {
      connect: { id: companyId },
    },
    role: {
      connect: { id: roleId },
    },
    user: {
      connect: { id: userId },
    },
  };

  let companyUserResponse;
  try {
    companyUserResponse = await client.mutate({
      mutation: CREATE_COMPANY_USER_MUTATION,
      variables: { data: companyUserData },
    });
  } catch (e) {
    error('createCompanyMember', e);
    Flux.dispatchEvent(ALLIANCE_INVITATION_ERROR_EVENT, e);
    throw e;
  }

  return companyUserResponse.data;
};

/**
 * Accepts an Alliance Member Invitation.
 *
 * @param invitation - The Alliance Member Invitation to be accepted.
 */
export const acceptAllianceMemberInvitation = async (invitation) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { user } = sessionStore.getState(NEW_SESSION_EVENT);
  const { companyUserRelation } = user;
  const companyId = invitation.company.id;
  const companyUsers = companyUserRelation.items;
  // To find if the user belongs to the company of the invitation
  const sameCompanyUser = companyUsers.find(({ company }) => company.id === companyId);

  if (invitation.status === ALLIANCE_MEMBER_INVITATION_ACCEPTED) {
    return Flux.dispatchEvent(
      ALLIANCE_INVITATION_ERROR_EVENT,
      new IntegrityError('Alliance Membership Invitation Already Accepted'),
    );
  }

  if (!areEmailsEqual(user.email, invitation.email)) {
    return Flux.dispatchEvent(
      ALLIANCE_INVITATION_ERROR_EVENT,
      new IntegrityError('Invitations can only be accepted by their owner'),
    );
  }

  if (!canAcceptAllianceMemberInvitation(user, invitation)) {
    return Flux.dispatchEvent(
      ALLIANCE_INVITATION_ERROR_EVENT,
      new IntegrityError(`Permision Denied. Can't accept Alliance Membership Invitation`),
    );
  }

  // Update Invitation to status ACCEPTED
  const invitationData = {
    id: invitation.id,
    status: ALLIANCE_MEMBER_INVITATION_ACCEPTED,
    dateOfResponse: new Date(),
  };

  let response;
  try {
    response = await client.mutate({
      mutation: UPDATE_ALLIANCE_MEMBER_INVITATION_MUTATION,
      variables: { data: invitationData },
    });
  } catch (e) {
    error('acceptAllianceMemberInvitation', e);
    return Flux.dispatchEvent(ALLIANCE_INVITATION_ERROR_EVENT, e);
  }

  let companyUserId;
  if (!sameCompanyUser) {
    // set the invitation company to the user
    const roles = await fetchRoles();
    const companyRole = roles.find((role) => role.name === COMPANY_MEMBER);
    const companyRoleId = companyRole.id;

    let companyUserResponse;
    try {
      companyUserResponse = await createCompanyMember(companyId, companyRoleId, user.id);
    } catch (e) {
      error('setUserCompany', e);
      return Flux.dispatchEvent(ALLIANCE_INVITATION_ERROR_EVENT, e);
    }
    log('setUserCompany', companyUserResponse);
    companyUserId = companyUserResponse.companyUserCreate.id;
  } else companyUserId = sameCompanyUser.id;

  // get the Alliance and role ID of the Invitation
  const allianceId = invitation.alliance.id;
  const allianceRoleId = invitation.role.id;
  try {
    await createAllianceMember(companyUserId, allianceRoleId, allianceId);
  } catch (e) {
    error('acceptAllianceMemberInvitation', e);
    return Flux.dispatchEvent(ALLIANCE_INVITATION_ERROR_EVENT, e);
  }

  log('acceptAllianceMemberInvitation', response);
  Flux.dispatchEvent(ALLIANCE_MEMBER_INVITATION_UPDATED_EVENT, response.data);
  return response.data;
};

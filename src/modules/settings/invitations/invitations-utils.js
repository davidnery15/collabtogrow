/**
 * Sort function. Show pending invitations first
 * @param {Invitation} invitation
 */
export const sortByPendingInvitation = ({ status }) => (status === 'PENDING' ? -1 : 1);

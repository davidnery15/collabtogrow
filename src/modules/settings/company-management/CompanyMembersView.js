import React from 'react';
import * as toast from 'components/toast/Toast';
import * as R from 'ramda';
import { PropTypes } from 'prop-types';
import View from '@cobuildlab/react-flux-state';
import { Card, Table, Dropdown, Menu, Heading, SelectField } from '@8base/boost';
import { DropdownBodyOnTable } from 'components/dropdown/DropdownBodyOnTable';
import { ViewCardBody } from 'components/card/ViewCardBody';
import sessionStore, { ROLES_EVENT, NEW_SESSION_EVENT } from '../../../shared/SessionStore';
import CompanyStore, {
  COMPANY_ERROR_EVENT,
  COMPANY_LIST_MEMBERS_EVENT,
  COMPANY_TRANSFER_EVENT,
  COMPANY_LEAVE_EVENT,
  COMPANY_ROLE_CHANGE_EVENT,
} from './company.store';
import ModalWithInputs from '../../../components/ModalWithInputs';
import {
  fetchMembersAction,
  leaveCompanyAction,
  transferOwnerAction,
  changeMemberRoleAction,
} from './company.actions';
import YesNoDialog from '../../../components/dialogs/YesNoDialog';
import { getCompanyRoles } from '../../../shared/roles';
import { fetchRoles } from '../../dashboard/dashboard-actions';
import { ActionButton } from '../../../components/buttons/ActionButton';
import { onErrorMixin } from '../../../shared/mixins';
import { ActionButtonClose } from '../../../components/buttons/ActionButtonClose';

/**
 * List All the Members
 */
export default class CompanyMembersView extends View {
  constructor(props) {
    super(props);
    this.state = {
      members: [],
      loading: true,
      member: '',
      memberId: 0,
      companyUserId: 0,
      removeModalIsOpen: false,
      transferModalIsOpen: false,
      changeRoleModalIsOpen: false,
      email: '',
      role: null,
      newRole: null,
      roles: [],
    };
    this.onError = onErrorMixin.bind(this);
  }

  componentDidMount() {
    this.subscribe(CompanyStore, COMPANY_ERROR_EVENT, this.onError);

    this.subscribe(sessionStore, ROLES_EVENT, (data) => {
      // Filter the roles to only include the ones that are related to company
      const validRoles = getCompanyRoles();
      const filteredRoles = data.filter(
        (item) => validRoles.find((validRole) => validRole.name === item.name) !== undefined,
      );

      this.setState({ roles: filteredRoles });
    });

    this.subscribe(CompanyStore, COMPANY_LIST_MEMBERS_EVENT, (state) => {
      const user = sessionStore.getState(NEW_SESSION_EVENT).user;

      // Not sure why the next few lines were there
      // Filter out the currently logged in user
      // const filteredMembers = state.companyUsersList.items.filter(
      //   (member) => member.user.id !== user.id,
      // );

      // Find own user to set the companyUserId to the state
      const ownUser = state.companyUsersList.items.find((member) => member.user.id === user.id);

      this.setState({
        members: state.companyUsersList.items,
        companyUserId: ownUser.id,
        loading: false,
      });
    });

    this.subscribe(CompanyStore, COMPANY_ROLE_CHANGE_EVENT, () => {
      toast.success('Role Changed Successfully');
      fetchMembersAction(this.props.match.params.id);
    });

    this.subscribe(CompanyStore, COMPANY_LEAVE_EVENT, () => {
      toast.success('Member Removed');
      fetchMembersAction(this.props.match.params.id);
    });

    this.subscribe(CompanyStore, COMPANY_TRANSFER_EVENT, () => {
      toast.success('You transferred the ownership successfully');
      const { match } = this.props;
      this.props.history.push(`/settings/company-management/${match.params.id}`);
    });

    fetchMembersAction(this.props.match.params.id);
    fetchRoles();
  }

  onChange = (name, value) => {
    this.setState({ [name]: value });
  };

  onChangeRole = (value) => {
    this.setState({ role: value });
  };

  onNewRole = (value) => {
    this.setState({ newRole: value });
  };

  onSelectForRemove = (id) => {
    this.setState({
      memberId: id,
      removeModalIsOpen: true,
    });
  };

  onSelectForTransfer = (id) => {
    this.setState({
      memberId: id,
      transferModalIsOpen: true,
    });
  };

  onSelectForChangeRole = (member) => {
    this.setState({
      member,
      changeRoleModalIsOpen: true,
    });
  };

  onYesRemove = () => {
    this.setState(
      {
        removeModalIsOpen: false,
        loading: true,
      },
      () => leaveCompanyAction(this.state.memberId),
    );
  };

  onYesTransfer = () => {
    this.setState(
      {
        transferModalIsOpen: false,
        loading: true,
      },
      () => transferOwnerAction(this.state.companyUserId, this.state.memberId),
    );
  };

  onYesChangeRole = () => {
    this.setState(
      {
        changeRoleModalIsOpen: false,
        loading: true,
      },
      () => {
        changeMemberRoleAction(this.state.member.id, this.state.newRole);
      },
    );
  };

  onClose = () => {
    this.setState({
      email: '',
      role: null,
      member: '',
      newRole: null,
      removeModalIsOpen: false,
      transferModalIsOpen: false,
      changeRoleModalIsOpen: false,
    });
  };

  render() {
    const { removeModalIsOpen, transferModalIsOpen, changeRoleModalIsOpen, loading } = this.state;
    const { history } = this.props;
    const { user } = sessionStore.getState(NEW_SESSION_EVENT);

    const topButtons = !loading ? (
      <div className="company-icons">
        <ActionButtonClose onClick={history.goBack} />
      </div>
    ) : null;

    return (
      <React.Fragment>
        <Card.Header>
          <Heading type="h4" text="Member Management" />
          {topButtons}
        </Card.Header>
        <ViewCardBody borderRadius="all" style={{ padding: 0, textAlign: 'center' }}>
          <Table>
            <Table.Header columns="repeat(5, 1fr)">
              <Table.HeaderCell>First Name</Table.HeaderCell>
              <Table.HeaderCell>Last Name</Table.HeaderCell>
              <Table.HeaderCell>Email</Table.HeaderCell>
              <Table.HeaderCell>Role</Table.HeaderCell>
              <Table.HeaderCell>Options</Table.HeaderCell>
            </Table.Header>
            <Table.Body loading={this.state.loading} data={this.state.members}>
              {(member) => (
                <Table.BodyRow columns="repeat(5, 1fr)" key={member.id}>
                  <Table.BodyCell>{R.pathOr('', ['user', 'firstName'], member)}</Table.BodyCell>
                  <Table.BodyCell>{R.pathOr('', ['user', 'lastName'], member)}</Table.BodyCell>
                  <Table.BodyCell>{R.pathOr('', ['user', 'email'], member)}</Table.BodyCell>
                  <Table.BodyCell>{R.pathOr('', ['role', 'name'], member)}</Table.BodyCell>
                  <Table.BodyCell>
                    {member.user.id !== user.id && (
                      <Dropdown defaultOpen={false}>
                        <Dropdown.Head>
                          <ActionButton onClick={() => {}} text="Options" />
                        </Dropdown.Head>
                        <DropdownBodyOnTable>
                          {({ closeDropdown }) => (
                            <Menu>
                              <Menu.Item
                                onClick={() => {
                                  closeDropdown();
                                  this.onSelectForChangeRole(member);
                                }}>
                                Change Role
                              </Menu.Item>
                              <Menu.Item
                                onClick={() => {
                                  closeDropdown();
                                  this.onSelectForRemove(member.id);
                                }}>
                                Remove Member
                              </Menu.Item>
                              <Menu.Item
                                onClick={() => {
                                  closeDropdown();
                                  this.onSelectForTransfer(member.id);
                                }}>
                                Transfer Ownership
                              </Menu.Item>
                            </Menu>
                          )}
                        </DropdownBodyOnTable>
                      </Dropdown>
                    )}
                  </Table.BodyCell>
                </Table.BodyRow>
              )}
            </Table.Body>
          </Table>
        </ViewCardBody>
        <YesNoDialog
          isOpen={removeModalIsOpen}
          onYes={this.onYesRemove}
          onNo={this.onClose}
          onClose={this.onClose}
          text={'Are you sure that you want to remove this user from the Company?'}
          title={'Remove Member'}
        />
        <YesNoDialog
          isOpen={transferModalIsOpen}
          onYes={this.onYesTransfer}
          onNo={this.onClose}
          onClose={this.onClose}
          text={'Are you sure that you want to Transfer Ownership of the Company to this user?'}
          title={'Transfer Ownership'}
        />

        {/* Change role Modal */}
        <ModalWithInputs
          isOpen={changeRoleModalIsOpen}
          onYes={this.onYesChangeRole}
          onNo={this.onClose}
          onClose={this.onClose}
          yesText={'Change Role'}
          noText={'Cancel'}
          text={'Select the new Role for this member'}
          title={'Role Change'}>
          <React.Fragment>
            <SelectField
              label="Role"
              input={{
                name: 'role',
                value: this.state.newRole,
                onChange: (value) => this.onNewRole(value),
              }}
              meta={{}}
              placeholder="Select Role"
              options={this.state.roles.map((rol) => {
                return { label: rol.name, value: rol.id };
              })}
            />
          </React.Fragment>
        </ModalWithInputs>
      </React.Fragment>
    );
  }
}

CompanyMembersView.propTypes = {
  history: PropTypes.object.isRequired,
};

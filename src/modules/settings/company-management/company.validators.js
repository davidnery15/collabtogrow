import * as utils from '../../../shared/validators';
import { ValidationError } from '../../../shared/errors';

/**
 * Validator for the company form
 * @param {Object} data fields from the company form
 */
export const createCompanyValidator = (data) => {
  console.log('CompanyFormValidator', data);
  let errorMessages = [];

  if (!utils.isValidString(data.name)) {
    errorMessages.push("The Company Name can't be empty.");
    // throw new Error("The Company Name can't be empty.");
  }

  if (!utils.isValidString(data.description)) {
    errorMessages.push("The Company Description can't be empty.");
    // throw new Error("The Company Description can't be empty.");
  }

  if (!utils.isValidString(data.missionAndGoals)) {
    errorMessages.push("The Company Mission and Goals can't be empty.");
    // throw new Error("The Company Mission and Goals can't be empty.");
  }

  if (!utils.isValidString(data.website, true)) {
    errorMessages.push('The Website URL is not valid');
    // throw new Error('The Website URL is not valid');
  }

  if (!data.country) {
    errorMessages.push("The Country can't be empty.");
    // throw new Error("The Country can't be empty.");
  }

  if (!utils.isValidString(data.zipCode)) {
    errorMessages.push("The Zip Code can't be empty.");
    // throw new Error("The Zip Code can't be empty.");
  }

  if (!utils.isValidString(data.state)) {
    errorMessages.push("The State can't be empty.");
    // throw new Error("The State can't be empty.");
  }

  if (!utils.isValidString(data.city)) {
    errorMessages.push("The City can't be empty.");
    // throw new Error("The City can't be empty.");
  }

  if (!utils.isValidString(data.address1)) {
    errorMessages.push("The Address 1  can't be empty.");
    // throw new Error("The Address 1  can't be empty.");
  }

  if (!utils.isValidString(data.address2, true)) {
    errorMessages.push('The Address 2 is not valid');
    // throw new Error('The Address 2 is not valid');
  }

  if (!utils.isValidString(data.phone)) {
    errorMessages.push("The Phone can't be empty.");
    // throw new Error("The Phone can't be empty.");
  }

  if (!utils.isValidNumber(data.annualRevenue)) {
    errorMessages.push("The Annual Revenue can't be empty.");
    // throw new Error("The Annual Revenue can't be empty.");
  }

  if (!utils.isValidString(data.numberOfEmployees)) {
    errorMessages.push("The Number of Employees can't be empty.");
    // throw new Error("The Number of Employees can't be empty.");
  }

  if (!data.industry) {
    errorMessages.push("The Industry can't be empty.");
    // throw new Error("The Industry can't be empty.");
  }

  if (!utils.isValidString(data.parentCompany, true)) {
    errorMessages.push('The Parent Company Name is not valid');
    // throw new Error('The Parent Company Name is not valid');
  }

  const companyStatus = ['public', 'private'];
  if (!utils.isValidString(data.companyStatus) || !companyStatus.includes(data.companyStatus)) {
    errorMessages.push('The Company Status is not valid');
    // throw new Error('The Company Status is not valid');
  }

  if (data.companyStatus === 'public' && !utils.isValidString(data.tickerSymbol)) {
    errorMessages.push("The Ticker Symbol can't be empty.");
    // throw new Error("The Ticker Symbol can't be empty.");
  }
  if (errorMessages.length !== 0) throw new ValidationError(errorMessages);
};

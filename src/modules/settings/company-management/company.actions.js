import Flux from '@cobuildlab/flux-state';
import {
  COMPANY_FORM_DATA_QUERY,
  CREATE_COMPANY_MUTATION,
  COMPANY_DETAIL_QUERY,
  EDIT_COMPANY_MUTATION,
  LEAVE_COMPANY_MUTATION,
  COMPANY_LIST_MEMBERS_QUERY,
  TRANSFER_OWNER_MUTATION,
  UPDATE_ROLE_MUTATION,
  COMPANY_LIST_QUERY,
} from './Company.queries';
import {
  COMPANY_ERROR_EVENT,
  COMPANY_FORM_DATA_EVENT,
  COMPANY_CREATE_EVENT,
  COMPANY_DETAIL_EVENT,
  COMPANY_EDIT_EVENT,
  COMPANY_LEAVE_EVENT,
  COMPANY_LIST_MEMBERS_EVENT,
  COMPANY_TRANSFER_EVENT,
  COMPANY_ROLE_CHANGE_EVENT,
  COMPANY_LIST_EVENT,
} from './company.store';
import { createCompanyValidator } from './company.validators';
import sessionStore, { APOLLO_CLIENT, NEW_SESSION_EVENT } from '../../../shared/SessionStore';
import { fetchRoles } from '../../dashboard/dashboard-actions';
import {
  sanitize8BaseReference,
  sanitize8BaseDocumentCreate,
  sanitize8BaseDocumentUpdate,
} from '../../../shared/utils';
import { IntegrityError } from '../../../shared/errors';
import { COMPANY_ADMINISTRATOR } from '../../../shared/roles';
import { updateMeta } from '../../../modules/dashboard/dashboard-actions';
import { META_ALLIANCE_SELECTED } from '../../../shared/index';
import { log, error } from '@cobuildlab/pure-logger';

/**
 * Fetches data for the company creation and edit form
 * @return {Promise<void>}
 */
export const fetchCompanyFormDataAction = async () => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const user = sessionStore.getState(NEW_SESSION_EVENT).user;

  let response;

  try {
    response = await client.query({
      query: COMPANY_FORM_DATA_QUERY,
      variables: { userId: user.id },
      fetchPolicy: 'network-only',
    });
  } catch (e) {
    console.error('fetchCompanyFormDataAction', e);
    return Flux.dispatchEvent(COMPANY_ERROR_EVENT, e);
  }

  console.log('fetchCompanyFormData', response);
  return Flux.dispatchEvent(COMPANY_FORM_DATA_EVENT, response.data);
};

/**
 * Creates a New Company
 * @param {Object} data object with all the company information
 * @return {Promise<void>}
 */
export const createCompanyAction = async (data) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const user = sessionStore.getState(NEW_SESSION_EVENT).user;
  const roles = await fetchRoles();
  const role = roles.find((role) => role.name === COMPANY_ADMINISTRATOR);

  delete data.id;
  delete data.pictureUrl;

  data.annualRevenue = String(data.annualRevenue);
  data.country = { connect: { id: data.country } };
  data.industry = { connect: { id: data.industry } };
  data.companyUserRelation = {
    create: [{ user: { connect: { id: user.id } }, role: { connect: { id: role.id } } }],
  };

  try {
    createCompanyValidator(data);
  } catch (err) {
    console.error('createCompany Validator', err);
    return Flux.dispatchEvent(COMPANY_ERROR_EVENT, err);
  }

  sanitize8BaseDocumentCreate(data, 'logoFile');

  let response;
  try {
    response = await client.mutate({
      mutation: CREATE_COMPANY_MUTATION,
      variables: { data },
    });
  } catch (e) {
    console.error('createCompany', e);
    return Flux.dispatchEvent(COMPANY_ERROR_EVENT, e);
  }

  console.log('createCompany Response', response);
  Flux.dispatchEvent(COMPANY_CREATE_EVENT, response.data);
  return response.data;
};

/**
 * Fetch a single Company
 * @param {String} id of the company to fetch
 * @return {Promise<void>}
 */
export const fetchCompanyAction = async (id) => {
  const client = sessionStore.getState(APOLLO_CLIENT);

  let response;

  try {
    response = await client.query({
      query: COMPANY_DETAIL_QUERY,
      variables: { id },
      fetchPolicy: 'network-only',
    });
  } catch (e) {
    console.error('fetchCompany', e);
    return Flux.dispatchEvent(COMPANY_ERROR_EVENT, e);
  }

  console.log('fetchCompany', response);
  Flux.dispatchEvent(COMPANY_DETAIL_EVENT, response.data);
  return response.data;
};
export const fetchCompanyListAction = async (page = 1, first = 20) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const user = sessionStore.getState(NEW_SESSION_EVENT).user;
  const skip = (page - 1) * first;
  const filter = {
    companyUserRelation: {
      some: { user: { id: { equals: user.id } } },
    },
  };

  let response;
  try {
    response = await client.query({
      query: COMPANY_LIST_QUERY,
      variables: { data: filter, skip, first },
      fetchPolicy: 'network-only',
    });
  } catch (e) {
    console.error('FetchCompanies', e);
    return Flux.dispatchEvent(COMPANY_ERROR_EVENT);
  }

  console.log('fetchCompanies', response);
  Flux.dispatchEvent(COMPANY_LIST_EVENT, response.data);
};

/**
 * Update a Company
 * @param {Object} data The Updated Company
 * @return {Promise<void>}
 */
export const editCompanyAction = async (data) => {
  const client = sessionStore.getState(APOLLO_CLIENT);

  delete data.__typename;
  delete data.companyUserRelation;
  delete data.pictureUrl;

  data.annualRevenue = String(data.annualRevenue);
  data.country = data.country.id;
  data.industry = data.industry.id;

  sanitize8BaseReference(data, 'country');
  sanitize8BaseReference(data, 'industry');
  sanitize8BaseDocumentUpdate(data, 'logoFile');

  try {
    createCompanyValidator(data);
  } catch (err) {
    console.error('EditCompany Validator', err);
    return Flux.dispatchEvent(COMPANY_ERROR_EVENT, err);
  }

  let response;
  console.log('EditCompany Data', data);
  try {
    response = await client.mutate({
      mutation: EDIT_COMPANY_MUTATION,
      variables: { data },
    });
  } catch (e) {
    console.error('EditCompany', e, data);
    return Flux.dispatchEvent(COMPANY_ERROR_EVENT, e);
  }

  console.log('EditCompany', response);
  Flux.dispatchEvent(COMPANY_EDIT_EVENT, response.data);
  return response.data;
};

/**
 * Leave the company action
 * @param {String} companyUserId id of the relation
 * @returns {Promise<void>}
 */
export const leaveCompanyAction = async (companyUserId) => {
  const client = sessionStore.getState(APOLLO_CLIENT);

  let response;

  try {
    response = await client.mutate({
      mutation: LEAVE_COMPANY_MUTATION,
      variables: { data: { id: companyUserId, force: true } },
    });
  } catch (e) {
    console.error('leaveCompany', e);
    return Flux.dispatchEvent(COMPANY_ERROR_EVENT, e);
  }

  try {
    await updateMeta(META_ALLIANCE_SELECTED, '');
  } catch (e) {
    console.error('Update META_ALLIANCE_SELECTED', e);
    return Flux.dispatchEvent(COMPANY_ERROR_EVENT, e);
  }

  console.log('leaveCompany', response);
  Flux.dispatchEvent(COMPANY_LEAVE_EVENT, response.data);
  return response.data;
};

/**
 * Fetches the Members from my Company
 * @return {Promise<void>}
 */
export const fetchMyCompanyMembersAction = async () => {
  const user = sessionStore.getState(NEW_SESSION_EVENT).user;
  const companyId = user.companyUserRelation.items[0].company.id;
  if (!companyId)
    return Flux.dispatchEvent(
      COMPANY_ERROR_EVENT,
      new IntegrityError(`You don't belong to a Company yet!`),
    );
  return await fetchMembersAction(companyId);
};

/**
 * Fetches the Members for the Company
 * @param {String} id of the company
 * @return {Promise<void>}
 */
export const fetchMembersAction = async (id) => {
  log('fetchMembers', id);
  const client = sessionStore.getState(APOLLO_CLIENT);

  let response;
  try {
    response = await client.query({
      query: COMPANY_LIST_MEMBERS_QUERY,
      variables: { id },
      fetchPolicy: 'cache-first',
    });
  } catch (e) {
    error('fetchMembers', e);
    return Flux.dispatchEvent(COMPANY_ERROR_EVENT, e);
  }
  log('fetchMembers:response', response.data);
  Flux.dispatchEvent(COMPANY_LIST_MEMBERS_EVENT, response.data);
  return response.data;
};

/**
 * Action to transfer the ownership of a company
 * @param companyUserId The id of your own company user relation
 * @param memberId The id of the company user relation of the new owner
 * @returns {Promise<void>}
 */
export const transferOwnerAction = async (companyUserId, memberId) => {
  const client = sessionStore.getState(APOLLO_CLIENT);

  const roles = await fetchRoles();
  const collaboratorRole = roles.find((role) => role.name === 'Company Collaborator');
  const ownerRole = roles.find((role) => role.name === COMPANY_ADMINISTRATOR);

  let data1 = {};
  let data2 = {};

  data1.id = companyUserId;
  data2.id = memberId;

  data1.role = {
    connect: { id: collaboratorRole.id },
  };

  data2.role = {
    connect: { id: ownerRole.id },
  };

  console.log('transferOwnerAction data 1', data1);
  console.log('transferOwnerAction data 2', data2);

  let response;

  try {
    response = await client.mutate({
      mutation: TRANSFER_OWNER_MUTATION,
      variables: { data1, data2 },
    });
  } catch (e) {
    console.error('transferOwner', e);
    return Flux.dispatchEvent(COMPANY_ERROR_EVENT, e);
  }

  console.log('transferOwner', response);
  Flux.dispatchEvent(COMPANY_TRANSFER_EVENT, response.data);
  return response.data;
};

/**
 * Change the role of a member
 * @param memberId
 * @param newRole
 *  @return {Promise<void>}
 */
export const changeMemberRoleAction = async (memberId, newRole) => {
  const client = sessionStore.getState(APOLLO_CLIENT);

  const data = {};
  data.id = memberId;
  data.role = {
    connect: { id: newRole },
  };

  let response;

  try {
    response = await client.mutate({
      mutation: UPDATE_ROLE_MUTATION,
      variables: { data },
    });
  } catch (e) {
    console.error('changeMemberRoleAction', e);
    return Flux.dispatchEvent(COMPANY_ERROR_EVENT, e);
  }

  console.log('changeMemberRoleAction', response);
  Flux.dispatchEvent(COMPANY_ROLE_CHANGE_EVENT, response.data);
  return response.data;
};

/**
 * Get all the user's company where the user is Administrator
 *
 */
export const getMyCompanies = () => {
  const user = sessionStore.getState(NEW_SESSION_EVENT).user;
  const companies = user.companyUserRelation;
  const filteredCompanies = companies.items.filter((company) => {
    return company.role.name === COMPANY_ADMINISTRATOR;
  });

  return filteredCompanies;
};

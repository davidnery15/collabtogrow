import React from 'react';
import { Card, Heading, Loader } from '@8base/boost';
import { DetailViewCardBody } from 'components/card/DetailViewCardBody';
import * as R from 'ramda';
import PropTypes from 'prop-types';
import * as toast from 'components/toast/Toast';
import View from '@cobuildlab/react-flux-state';
import { withRouter } from 'react-router-dom';
import sessionStore, { NEW_SESSION_EVENT } from '../../../shared/SessionStore';
import CompanyStore, {
  COMPANY_DETAIL_EVENT,
  COMPANY_ERROR_EVENT,
  COMPANY_LEAVE_EVENT,
} from './company.store';
import CompanyDetailTable from './components/CompanyDetailTable';
import YesNoDialog from '../../../components/dialogs/YesNoDialog';
import { fetchCompanyAction, leaveCompanyAction } from './company.actions';
import CompanyModel from './company.model';
import { onErrorMixin } from '../../../shared/mixins';
import { canEditCompanyPermission } from '../../auth/company-permissions';
import { getCurrencyOnSession } from '../../../shared/alliance-utils';
import { DangerButton } from '../../../components/buttons/DangerButton';
import { TransparentButtonFontAwesome } from '../../../components/buttons/TransparentButtonFontAwesome';
import { ActionButtonClose } from '../../../components/buttons/ActionButtonClose';
import { CardFooter } from '../../../components/new-ui/card/CardFooter';

export const COMPANY_DOCUMENT = 'companyDocument';

class CompanyDetailView extends View {
  constructor(props) {
    super(props);
    this.state = {
      data: R.clone(CompanyModel),
      loading: true,
      isOwner: false,
      companyUserId: 0,
      leaveModalIsOpen: false,
    };
    this.onError = onErrorMixin.bind(this);
  }

  componentDidMount() {
    const { match } = this.props;
    if (!match.params.id) return toast.error('Company ID missing');

    this.subscribe(CompanyStore, COMPANY_ERROR_EVENT, this.onError);
    this.subscribe(CompanyStore, COMPANY_DETAIL_EVENT, (state) => {
      const user = sessionStore.getState(NEW_SESSION_EVENT).user;
      const companyUser = state.company.companyUserRelation.items.find(
        (companyInformation) => companyInformation.user.id === user.id,
      );

      if (!companyUser) {
        toast.error('You are not part of this Company');
        return this.props.history.push('/settings');
      }
      const companyUserId = companyUser.id;

      localStorage.setItem(COMPANY_DOCUMENT, JSON.stringify(state.company.logoFile));

      this.setState({
        data: R.clone(state.company),
        isOwner: canEditCompanyPermission(user, state.company),
        companyUserId,
        loading: false,
      });
    });

    this.subscribe(CompanyStore, COMPANY_LEAVE_EVENT, () => {
      toast.success('You left the Company');
      this.props.history.push('/settings');
      window.location.reload();
    });

    fetchCompanyAction(match.params.id);
  }
  componentWillUnmount() {
    super.componentWillUnmount();
    localStorage.removeItem(COMPANY_DOCUMENT);
  }

  onLeaveCompany = (id) => {
    this.setState({
      companyId: id,
      leaveModalIsOpen: true,
    });
  };

  onYesLeave = () => {
    this.setState(
      {
        leaveModalIsOpen: false,
        loading: true,
      },
      () => leaveCompanyAction(this.state.companyUserId),
    );
  };

  onClose = () => {
    this.setState({
      leaveModalIsOpen: false,
    });
  };

  render() {
    const { data, loading, users, isOwner, leaveModalIsOpen } = this.state;
    const currency = getCurrencyOnSession();
    let content = (
      <CompanyDetailTable
        currency={currency}
        data={data}
        users={users}
        isDisabled
        onClickEdit={() =>
          this.props.history.push(`/settings/company-management/edit/${this.state.data.id}`)
        }
      />
    );
    if (loading === true) content = <Loader stretch />;
    let buttonsBottom = '';

    buttonsBottom = (
      <>
        <div className="positionBottonRight">
          {!loading ? (
            <DangerButton
              text={'Leave Company'}
              fontAwesomeIcon={'door-open'}
              onClick={() => this.onLeaveCompany(data.id)}
            />
          ) : null}
        </div>
      </>
    );

    return (
      <React.Fragment>
        <Card.Header>
          <Heading type="h4" text={data.name} style={{ marginRight: '10px' }} />
          <>
            <div className="company-icons">
              {isOwner && !loading ? (
                <TransparentButtonFontAwesome
                  text={'Manage Members'}
                  fontAwesomeIcon={'users-cog'}
                  onClick={() =>
                    this.props.history.push(
                      `/settings/company-management/members/${this.state.data.id}`,
                    )
                  }
                />
              ) : null}
              <ActionButtonClose onClick={() => this.props.history.goBack()} />
            </div>
          </>
        </Card.Header>
        <DetailViewCardBody>{content}</DetailViewCardBody>
        <YesNoDialog
          isOpen={leaveModalIsOpen}
          onYes={this.onYesLeave}
          onNo={this.onClose}
          onClose={this.onClose}
          text={'Are you sure you want to Leave this Company?'}
          title={'Leave Company'}
        />
        <CardFooter>{buttonsBottom}</CardFooter>
      </React.Fragment>
    );
  }
}

CompanyDetailView.propTypes = {
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default withRouter(CompanyDetailView);

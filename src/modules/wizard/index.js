import welcomeImage from 'images/wizard/Welcome_Graphic.svg';
import personalImage from 'images/wizard/Personal_Information_Graphic.svg';
import companyImage from 'images/wizard/Company_Information_Graphic.svg';
import allianceImage from 'images/wizard/Alliance_Graphic.svg';
import usersImage from 'images/wizard/User_Permissions_Graphic.svg';
import businessImage from 'images/wizard/Business_Case_Graphic.svg';
import Welcome from './inner-screens/Welcome';
import UserInformationView from './inner-screens/UserInformationView';
import CompanyInformation from './inner-screens/CompanyInformation';
import AllianceInformation from './inner-screens/AllianceInformation';
import UserInvitations from './inner-screens/UserInvitations';
import ThankYou from './inner-screens/ThankYou';

export const SCREENS = [
  {
    progressName: 'Welcome',
    header: 'Welcome',
    layout: '1fr 1fr 1fr',
    progressBarValue: '0',
    image: welcomeImage,
    component: Welcome,
  },
  {
    progressName: 'User Information',
    header: 'Personal',
    layout: '1fr 40px 1fr',
    progressBarValue: '20',
    image: personalImage,
    component: UserInformationView,
  },
  {
    progressName: 'Company Information',
    header: 'Company',
    layout: '1fr 40px 1fr',
    progressBarValue: '40',
    image: companyImage,
    component: CompanyInformation,
  },
  {
    progressName: 'Alliance Information',
    header: 'Alliance',
    layout: '1fr 40px 1fr',
    progressBarValue: '60',
    image: allianceImage,
    component: AllianceInformation,
  },
  {
    progressName: 'Alliance Membership Invitations',
    header: 'Users',
    layout: '1fr',
    progressBarValue: '80',
    image: usersImage,
    component: UserInvitations,
  },
  {
    progressName: "Let's Begin",
    header: 'Welcome',
    layout: '1fr 1fr 1fr',
    progressBarValue: '100',
    image: businessImage,
    component: ThankYou,
  },
];

export const NUMBER_OF_WIZARD_SCREENS = SCREENS.length;

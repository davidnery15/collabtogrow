import Flux from '@cobuildlab/flux-state';
import {
  UPDATE_COMPANY_INFORMATION_MUTATION,
  CREATE_COMPANY_INFORMATION_MUTATION,
  COMPANY_INFORMATION_QUERY,
} from './company.queries';
import {
  WIZARD_ERROR,
  COMPANY_INFORMATION_UPDATE_EVENT,
  COMPANY_INFORMATION_CREATE_EVENT,
  COMPANY_INFORMATION_EVENT,
} from './WizardStore';
import sessionStore, { APOLLO_CLIENT, NEW_SESSION_EVENT } from '../../shared/SessionStore';
import { createCompanyValidator } from '../settings/company-management/company.validators';
import { fetchRoles } from '../dashboard/dashboard-actions';
import {
  sanitize8BaseReference,
  sanitize8BaseDocumentCreate,
  sanitize8BaseDocumentUpdate,
} from '../../shared/utils';
import { COMPANY_ADMINISTRATOR } from '../../shared/roles';
import { IntegrityError } from '../../shared/errors';

/**
 * Create Company
 * @param {CompanyInformation} company The User company information
 */
export const createCompany = async (company) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const user = sessionStore.getState(NEW_SESSION_EVENT).user;

  const roles = await fetchRoles();
  const role = roles.find((role) => role.name === COMPANY_ADMINISTRATOR);
  delete company.id;
  delete company.pictureUrl;

  company.annualRevenue = String(company.annualRevenue);
  company.companyUserRelation = {
    create: [{ user: { connect: { id: user.id } }, role: { connect: { id: role.id } } }],
  };

  console.log('wizardCreateCompany', company);
  try {
    createCompanyValidator(company);
  } catch (err) {
    console.error('CreateCompanyValidator', err);
    return Flux.dispatchEvent(WIZARD_ERROR, err);
  }

  sanitize8BaseReference(company, 'country');
  sanitize8BaseReference(company, 'industry');
  sanitize8BaseDocumentCreate(company, 'logoFile');
  let result;
  try {
    result = await client.mutate({
      mutation: CREATE_COMPANY_INFORMATION_MUTATION,
      variables: { data: company },
    });
  } catch (e) {
    console.error('createCompany', e);
    return Flux.dispatchEvent(
      WIZARD_ERROR,
      new IntegrityError(
        'An Error Occur trying to create the Company. The Name of the Company Already may already exist on the Database.',
      ),
    );
  }

  console.log('createCompany', result);

  return Flux.dispatchEvent(COMPANY_INFORMATION_CREATE_EVENT, result);
};

/**
 * Update Company Information
 * @param {CompanyInformation} companyInformation The User company information
 */
export const updateCompanyInformation = async (companyInformation) => {
  const client = sessionStore.getState(APOLLO_CLIENT);

  delete companyInformation.__typename;
  delete companyInformation.companyUserRelation;
  delete companyInformation.pictureUrl;

  companyInformation.annualRevenue = String(companyInformation.annualRevenue);

  console.log('updateCompanyInformation', companyInformation);
  sanitize8BaseReference(companyInformation, 'country');
  sanitize8BaseReference(companyInformation, 'industry');
  sanitize8BaseDocumentUpdate(companyInformation, 'logoFile');

  try {
    createCompanyValidator(companyInformation);
  } catch (err) {
    console.error('UpdateCompanyValidator', err);
    return Flux.dispatchEvent(WIZARD_ERROR, err);
  }

  let result;

  try {
    result = await client.mutate({
      mutation: UPDATE_COMPANY_INFORMATION_MUTATION,
      variables: { data: companyInformation },
    });
  } catch (e) {
    console.error('updateCompanyInformation', e);
    return Flux.dispatchEvent(WIZARD_ERROR, e);
  }

  console.log('updateCompanyInformation', result);

  Flux.dispatchEvent(COMPANY_INFORMATION_UPDATE_EVENT, result);
  return result;
};

/**
 * Fetches a Company Information by Id
 * @param client
 * @param id
 * @return {Promise<void>}
 */
export const fetchCompanyInformation = async () => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const user = sessionStore.getState(NEW_SESSION_EVENT).user;
  let response;
  try {
    response = await client.query({
      query: COMPANY_INFORMATION_QUERY,
      variables: { userId: user.id },
      fetchPolicy: 'network-only',
    });
  } catch (e) {
    return Flux.dispatchEvent(WIZARD_ERROR, e);
  }
  console.log('fetchCompanyInformation', response.data);
  Flux.dispatchEvent(COMPANY_INFORMATION_EVENT, response.data);
  return response.data;
};

import React from 'react';
import { Grid, InputField, SelectField } from '@8base/boost';
import PropTypes from 'prop-types';
import DeleteIcon from '../../../components/DeleteIcon';

const UserInvitation = (props) => {
  const { email, onChangeEmail, roles, role, onChangeRole, onDelete } = props;
  return (
    <div className="users-divider">
      <Grid.Layout columns="1fr 40px 1fr 40px">
        <Grid.Box>
          <InputField
            stretch
            label="Email Address"
            placeholder="Email Address"
            input={{
              name: 'email',
              type: 'email',
              value: email,
              onChange: onChangeEmail,
            }}
          />
        </Grid.Box>
        <Grid.Box />
        <Grid.Box>
          <SelectField
            label="Role"
            input={{
              name: 'role',
              value: role,
              onChange: onChangeRole,
            }}
            meta={{}}
            placeholder="Select Role"
            options={roles.map((rol) => {
              return { label: rol.name, value: rol.id };
            })}
          />
        </Grid.Box>
        <Grid.Box alignItems="center" justifyContent="center">
          <DeleteIcon onClick={onDelete} />
        </Grid.Box>
      </Grid.Layout>
    </div>
  );
};

UserInvitation.propTypes = {
  email: PropTypes.string.isRequired,
  onChangeEmail: PropTypes.func.isRequired,
  roles: PropTypes.array.isRequired,
  role: PropTypes.string.isRequired,
  onChangeRole: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
};

export default UserInvitation;

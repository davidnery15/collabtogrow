import React from 'react';
import { Route } from 'react-router-dom';
import { LeftWizardSection } from './LeftWizardSection';
import { RightWizardSection } from './RightWizardSection';

const WizardRoute = ({
  selectScreen,
  screens,
  currentScreen,
  currentScreenIndex,
  nextScreen,
  prevScreen,
  ...rest
}) => (
  <Route
    {...rest}
    render={(props) => (
      <React.Fragment>
        <LeftWizardSection
          sections={screens}
          currentScreen={currentScreenIndex}
          selectScreen={selectScreen}
        />
        <RightWizardSection
          screen={currentScreen}
          currentScreen={currentScreenIndex}
          nextScreen={nextScreen}
          prevScreen={prevScreen}
        />
      </React.Fragment>
    )}
  />
);

export default WizardRoute;

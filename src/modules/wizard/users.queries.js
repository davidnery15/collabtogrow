import gql from 'graphql-tag';

export const CREATE_USER_INVITATION_MUTATION = gql`
  mutation CreateUsernvitation($data: UserInvitationCreateInput!) {
    userInvitationCreate(data: $data) {
      id
    }
  }
`;

import gql from 'graphql-tag';
import { CompanyFragment } from '../settings/company-management/Company.queries';

export const CREATE_COMPANY_INFORMATION_MUTATION = gql`
  mutation($data: CompanyCreateInput!) {
    companyCreate(data: $data) {
      id
    }
  }
`;

export const UPDATE_COMPANY_INFORMATION_MUTATION = gql`
  mutation($data: CompanyUpdateInput!) {
    companyUpdate(data: $data) {
      id
    }
  }
`;

/**
 * company Information Query
 * {
 *   "userId":  ""
 * }
 */
export const COMPANY_INFORMATION_QUERY = gql`
  query($userId: ID) {
    companyUsersList(filter: { user: { id: { equals: $userId } } }) {
      items {
        company {
          ...CompanyFragment
        }
        role {
          id
          name
        }
      }
      count
    }
    countriesList {
      count
      items {
        id
        name
      }
    }
    statesList(filter: { country: { name: { equals: "United States" } } }) {
      items {
        id
        name
      }
    }
    industriesList {
      count
      items {
        id
        name
      }
    }
  }
  ${CompanyFragment}
`;

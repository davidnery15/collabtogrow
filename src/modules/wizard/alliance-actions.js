import {
  ALLIANCE_INFORMATION_QUERY,
  UPDATE_ALLIANCE_INFORMATION_MUTATION,
  CREATE_ALLIANCE_INFORMATION_MUTATION,
} from './alliance.queries';
import Flux from '@cobuildlab/flux-state';
import {
  WIZARD_ERROR,
  ALLIANCE_INFORMATION_EVENT,
  ALLIANCE_INFORMATION_UPDATE_EVENT,
  ALLIANCE_INFORMATION_CREATE_EVENT,
} from './WizardStore';
import { APOLLO_CLIENT, NEW_SESSION_EVENT } from '../../shared/SessionStore';
import sessionStore from '../../shared/SessionStore';
import { isValidString } from '../../shared/validators';
import { fetchCompanyInformation } from './company.actions';
import { createAllianceWizardValidator } from '../settings/alliance-management/alliance-validators';
import { createDefaultInitiativeAndActions } from '../settings/alliance-management/alliance-actions';
import { ALLIANCE_ADMINISTRATOR, COMPANY_ADMINISTRATOR } from '../../shared/roles';
import { fetchRoles } from '../dashboard/dashboard-actions';
import { IntegrityError } from '../../shared/errors';
import { error, log } from '@cobuildlab/pure-logger';

/**
 * Create an Alliance
 * - Sets the User logged id as the owner
 * - Sets the client company if the User logged in Has one
 */
export const createAllianceWizard = async (alliance) => {
  log('createAllianceWizard', alliance);
  const client = sessionStore.getState(APOLLO_CLIENT);
  const user = sessionStore.getState(NEW_SESSION_EVENT).user;
  const roles = await fetchRoles();
  const allianceAdministratorRole = roles.find((role) => role.name === ALLIANCE_ADMINISTRATOR);
  // delete id
  delete alliance.id;

  // Invitation Email
  const invitationEmail = alliance.invitationEmail;
  delete alliance.invitationEmail;

  try {
    createAllianceWizardValidator(alliance);
  } catch (err) {
    error('createAllianceWizard', err);
    return Flux.dispatchEvent(WIZARD_ERROR, err);
  }

  // Client Company if the User has one
  const { companyUsersList } = await fetchCompanyInformation();
  const companyInformation = companyUsersList.items.find(
    (companyInformation) => companyInformation.role.name === COMPANY_ADMINISTRATOR,
  );

  if (companyInformation === undefined)
    return Flux.dispatchEvent(
      WIZARD_ERROR,
      new IntegrityError('You need to own a Company to Create an Alliance'),
    );

  alliance.clientCompany = {
    connect: { id: companyInformation.company.id },
  };
  // I'm the Owner of this alliance
  alliance.owner = { connect: { id: user.id } };
  alliance.allianceUserAllianceRelation = {
    create: [
      {
        companyUser: {
          connect: {
            id: user.companyUserRelation.items[0].id,
          },
        },
        role: {
          connect: {
            id: allianceAdministratorRole.id,
          },
        },
      },
    ],
  };
  // also the Business Case in one off
  alliance.businessCase = {
    create: {
      owner: { connect: { id: user.id } },
      expectedCosts: ['0'],
      anticipatedCosts: ['0'],
      expectedRevenues: ['0'],
    },
  };

  //Currency
  alliance.currency = {
    connect: {
      id: alliance.currency,
    },
  };

  let result;
  try {
    result = await client.mutate({
      mutation: CREATE_ALLIANCE_INFORMATION_MUTATION,
      variables: { data: alliance },
    });
  } catch (e) {
    error('createAllianceWizard', e);
    return Flux.dispatchEvent(WIZARD_ERROR, e);
  }

  // If there is a invitationEmail create the allianceInvitationRelation
  const allianceId = result.data.allianceCreate.id;
  if (isValidString(invitationEmail)) {
    const allianceData = {
      id: allianceId,
      allianceInvitationRelation: {
        create: {
          email: invitationEmail,
        },
      },
    };

    try {
      await client.mutate({
        mutation: UPDATE_ALLIANCE_INFORMATION_MUTATION,
        variables: { data: allianceData },
      });
    } catch (e) {
      error('createAllianceInvitation', e, allianceData);
      return Flux.dispatchEvent(WIZARD_ERROR, e);
    }
  }

  try {
    await createDefaultInitiativeAndActions(allianceId);
  } catch (e) {
    error('createDefaultInitiativeAndActions', e);
    return Flux.dispatchEvent(WIZARD_ERROR, e);
  }

  log('createAllianceWizard', result);
  return Flux.dispatchEvent(ALLIANCE_INFORMATION_CREATE_EVENT, result);
};

/**
 * Update an Alliance
 */
export const updateAllianceInformation = async (allianceInformation) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  // Invitation Email
  if (isValidString(allianceInformation.invitationEmail)) {
    allianceInformation.allianceInvitationRelation = {
      create: {
        email: allianceInformation.invitationEmail,
      },
    };
  }
  allianceInformation.currency = {
    connect: {
      id: allianceInformation.currency,
    },
  };

  delete allianceInformation.invitationEmail;

  let result;
  try {
    result = await client.mutate({
      mutation: UPDATE_ALLIANCE_INFORMATION_MUTATION,
      variables: { data: allianceInformation },
    });
  } catch (e) {
    console.error('updateAllianceInformation', e);
    return Flux.dispatchEvent(WIZARD_ERROR, e);
  }

  console.log('updateAllianceInformation', result);
  return Flux.dispatchEvent(ALLIANCE_INFORMATION_UPDATE_EVENT, result);
};

/**
 * Fetches a Alliance Information by Id
 * @param client
 * @param id
 * @return {Promise<void>}
 */
export const fetchAllianceInformation = async () => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const user = sessionStore.getState(NEW_SESSION_EVENT).user;
  let response;
  try {
    response = await client.query({
      query: ALLIANCE_INFORMATION_QUERY,
      variables: { userId: user.id },
    });
  } catch (e) {
    return Flux.dispatchEvent(WIZARD_ERROR, e);
  }
  console.log('fetchAllianceInformation', response.data);
  return Flux.dispatchEvent(ALLIANCE_INFORMATION_EVENT, response.data);
};

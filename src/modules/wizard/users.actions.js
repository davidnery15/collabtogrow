import Flux from '@cobuildlab/flux-state';
import { WIZARD_ERROR, CREATE_USER_INVITATION_EVENT } from './WizardStore';
import sessionStore, { APOLLO_CLIENT, NEW_SESSION_EVENT } from '../../shared/SessionStore';
import { isValidString } from '../../shared/validators';
import { fetchCompanyInformation } from './company.actions';
import { UPDATE_ALLIANCE_INFORMATION_MUTATION } from './alliance.queries';
import { log } from '@cobuildlab/pure-logger';
import { IntegrityError } from '../../shared/errors';
import { createUserInvitationsValidator } from './users.validators';
import { COMPANY_ADMINISTRATOR } from '../../shared/roles';

/**
 * Create the User Invitations
 * @param usersInvitations The information for the invitations
 * @return {Promise<void>}
 */
export const createUserInvitations = async (usersInvitations) => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);

  if (!selectedAlliance || !isValidString(selectedAlliance.id)) {
    return Flux.dispatchEvent(
      WIZARD_ERROR,
      new IntegrityError('Permission Error. No Alliance Selected'),
    );
  }
  const allianceId = selectedAlliance.id;

  try {
    createUserInvitationsValidator(usersInvitations);
  } catch (err) {
    console.error('createUserInvitations', err);
    return Flux.dispatchEvent(WIZARD_ERROR, err);
  }

  const { companyUsersList } = await fetchCompanyInformation();
  if (companyUsersList.count === 0) {
    //No company to Add the Invitations
    Flux.dispatchEvent(WIZARD_ERROR, new IntegrityError("You don't belong to a Company yet"));
    return;
  }

  const companyUser = companyUsersList.items.find((companyUser) => {
    log('createUserInvitations', companyUser);
    return companyUser.role.name === COMPANY_ADMINISTRATOR;
  });
  if (companyUser === undefined) {
    // You don't own any company yet
    Flux.dispatchEvent(WIZARD_ERROR, new IntegrityError('You must be the owner of the Company'));
    return;
  }
  const companyId = companyUser.company.id;

  usersInvitations = usersInvitations.map((invitation) => ({
    email: invitation.email,
    role: { connect: { id: invitation.role } },
    company: { connect: { id: companyId } },
  }));

  const data = {
    id: allianceId,
    allianceMemberInvitationAllianceRelation: {
      create: usersInvitations,
    },
  };
  console.log('createUserInvitations data', data);

  let result;
  try {
    result = await client.mutate({
      mutation: UPDATE_ALLIANCE_INFORMATION_MUTATION,
      variables: { data },
    });
  } catch (e) {
    console.error('createUserInvitations', e);
    return Flux.dispatchEvent(WIZARD_ERROR, e);
  }

  console.log('createUserInvitations', result);
  Flux.dispatchEvent(CREATE_USER_INVITATION_EVENT, result);
  return result;
};

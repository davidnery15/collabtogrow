import React from 'react';
import { Button, Grid, Card, Loader } from '@8base/boost';
import { CardFooter } from '../components/CardFooter';
import UserInvitation from '../components/UserInvitation';
import PropTypes from 'prop-types';
import wizardStore, { CREATE_USER_INVITATION_EVENT } from '../WizardStore';
import sessionStore, { ROLES_EVENT } from '../../../shared/SessionStore';
import * as toast from 'components/toast/Toast';
import { WIZARD_ERROR } from '../WizardStore';
import { fetchRoles } from '../../dashboard/dashboard-actions';
import { createUserInvitations } from '../users.actions';
import CenterParagraph from '../../../components/CenterParagraph';
import H3 from '../../../components/H3';
import { getAllianceRoles } from 'shared/roles';
import View from '@cobuildlab/react-flux-state';
import { ActionButton } from '../../../components/buttons/ActionButton';
import * as R from 'ramda';
import PlusButton from 'components/PlusButton';

const getEmptyUser = () => {
  return {
    email: '',
    role: null,
  };
};

class UserInvitations extends View {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
      companies: [],
      roles: [],
      loading: true,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    this.subscribe(wizardStore, WIZARD_ERROR, (err) => {
      this.setState({
        loading: false,
      });
      toast.error(err.message);
    });

    this.subscribe(sessionStore, ROLES_EVENT, (data) => {
      // Filter the roles to only include the ones that are related to Alliance
      const validRoles = getAllianceRoles();
      const filteredRoles = data.filter(
        (item) => validRoles.find((validRole) => validRole.name === item.name) !== undefined,
      );

      this.setState({ roles: filteredRoles, loading: false });
    });

    this.subscribe(wizardStore, CREATE_USER_INVITATION_EVENT, () => {
      this.props.nextScreen();
    });
    fetchRoles();
  }

  handleChange(value, e, i) {
    const users = this.state.users;
    users[i][e.target.name] = value;
    this.setState({ users });
  }

  handleSubmit() {
    if (this.state.loading) return;

    this.setState({ loading: true });

    const data = R.clone(this.state);

    if (data.users.length === 0) {
      return this.props.nextScreen();
    }

    delete data.roles;
    delete data.loading;

    const users = data.users.concat();

    createUserInvitations(users);
  }

  addNewUser = () => {
    const users = this.state.users;
    users.push(getEmptyUser());
    this.setState({ users });
  };

  onChangeRole = (i, value) => {
    const users = this.state.users;
    users[i].role = value;
    this.setState({ users });
  };

  onChangeEmail = (i, value) => {
    const users = this.state.users;
    users[i].email = value;
    this.setState({ users });
  };

  onDelete = (index) => {
    const users = this.state.users;
    users.splice(index, 1);
    this.setState({ users });
  };

  render() {
    const { layout, prevScreen } = this.props;
    const { loading } = this.state;

    if (loading) return <Loader stretch />;

    return (
      <React.Fragment>
        <Card.Body style={{ paddingRight: '35px' }}>
          <Grid.Layout columns={layout}>
            {this.state.users.map((user, i) => (
              <UserInvitation
                key={i}
                email={user.email}
                onChangeEmail={(value) => this.onChangeEmail(i, value)}
                roles={this.state.roles}
                role={user.role}
                onChangeRole={(value) => this.onChangeRole(i, value)}
                onDelete={() => this.onDelete(i)}
              />
            ))}
            <Card.Section padding="none" style={{ paddingTop: 5 }}>
              <H3>Invite Users to your Alliance</H3>

              <CenterParagraph>
                Here you are able to invite members of your team to join your Alliance on the
                platform and collaborate.
              </CenterParagraph>

              <div className="add-hover">
                <PlusButton text="Add New User" onClick={this.addNewUser} />
              </div>
            </Card.Section>
          </Grid.Layout>
        </Card.Body>
        <CardFooter>
          <ActionButton onClick={() => this.handleSubmit()} text="Continue" />
          <Button type="button" onClick={() => prevScreen()} color="neutral" variant="outlined">
            {'Previous'}
          </Button>
        </CardFooter>
      </React.Fragment>
    );
  }
}

UserInvitations.propTypes = {
  layout: PropTypes.string.isRequired,
  currentScreen: PropTypes.number.isRequired,
  prevScreen: PropTypes.func.isRequired,
  nextScreen: PropTypes.func.isRequired,
};

export default UserInvitations;

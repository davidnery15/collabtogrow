import React, { Fragment } from 'react';
import {
  Card,
  Button,
  Grid,
  Label,
  InputField,
  TextAreaField,
  Loader,
  Text,
  Row,
  SelectField,
} from '@8base/boost';
import { CardFooter } from '../components/CardFooter';
import wizardStore, {
  WIZARD_ERROR,
  ALLIANCE_INFORMATION_CREATE_EVENT,
  ALLIANCE_INFORMATION_UPDATE_EVENT,
  ALLIANCE_INFORMATION_EVENT,
} from '../WizardStore';
import PropTypes from 'prop-types';
import sessionStore, { SESSION_ERROR, NEW_SESSION_EVENT } from '../../../shared/SessionStore';
import {
  createAllianceWizard,
  updateAllianceInformation,
  fetchAllianceInformation,
} from '../alliance-actions';
import View from '@cobuildlab/react-flux-state';
import CenterParagraph from '../../../components/CenterParagraph';
import { ActionButton } from '../../../components/buttons/ActionButton';
import * as R from 'ramda';
import { onErrorMixin } from '../../../shared/mixins';
import { fetchSession } from '../../auth/auth.actions';
import { ACCEPT_ALLIANCE_INVITATION } from 'modules/settings/invitations';
import { acceptAllianceInvitation } from 'modules/settings/invitations/invitations.actions';
import ErrorDialog from 'components/dialogs/ErrorDialog';
import YesNoDialog from 'components/dialogs/YesNoDialog';
import invitationStore, {
  ALLIANCE_INVITATION_ERROR_EVENT,
  ALLIANCE_INVITATION_NO_COMPANY_ERROR_EVENT,
  ALLIANCE_INVITATION_UPDATED_EVENT,
} from 'modules/settings/invitations/invitations.store';
import Status from 'components/Status';
import { DetailViewCardBody } from 'components/card/DetailViewCardBody';
import DetailValue from 'components/DetailValue';
import { updateMeta } from '../../../modules/dashboard/dashboard-actions';
import { META_STEP_NAME } from '../../../shared';
import { NUMBER_OF_WIZARD_SCREENS } from '../';
import { withRouter } from 'react-router-dom';
import AllianceStore, {
  ALLIANCE_FORM_DATA_EVENT,
} from '../../settings/alliance-management/alliance-store';
import { fetchAllianceFormData } from '../../settings/alliance-management/alliance-actions';
import { getMyCompanies } from '../../settings/company-management/company.actions';

/**
 * AllianceInformation view
 */
class AllianceInformation extends View {
  constructor(props) {
    super(props);
    this.state = {
      data: { name: '', description: '', invitationEmail: '', currency: {} },
      allianceInvitations: [],
      invitation: {},
      yesNoDialogIsOpen: false,
      yesNoDialogTitle: false,
      yesNoDialogDescription: false,
      noCompanyErrorDialogIsOpen: false,
      loading: true,
      currencies: [],
    };

    this.onError = onErrorMixin.bind(this);
    this.companyUsers = getMyCompanies();
  }

  componentDidMount() {
    const { items: allianceInvitations } = sessionStore.getState(
      NEW_SESSION_EVENT,
    ).allianceInvitationsList;

    this.subscribe(wizardStore, WIZARD_ERROR, this.onError);
    this.subscribe(sessionStore, SESSION_ERROR, this.onError);
    this.subscribe(invitationStore, ALLIANCE_INVITATION_ERROR_EVENT, this.onError);

    this.subscribe(invitationStore, ALLIANCE_INVITATION_NO_COMPANY_ERROR_EVENT, (data) => {
      this.setState({ noCompanyErrorDialogIsOpen: true, loading: false });
    });

    this.subscribe(sessionStore, NEW_SESSION_EVENT, (data) => {
      this.setState({
        allianceInvitations: data.allianceInvitationsList.items,
      });
    });
    this.subscribe(AllianceStore, ALLIANCE_FORM_DATA_EVENT, (state) => {
      this.setState({
        currencies: state.currenciesList.items,
      });
    });

    this.subscribe(invitationStore, ALLIANCE_INVITATION_UPDATED_EVENT, async () => {
      await updateMeta(META_STEP_NAME, NUMBER_OF_WIZARD_SCREENS + 1);
      await fetchSession();
      this.props.history.push('/welcome');
    });

    this.subscribe(wizardStore, ALLIANCE_INFORMATION_EVENT, (data) => {
      const { items } = data.alliancesList;

      // if the user has alliances get the first on list to edit
      if (items.length) {
        const alliance = items[0];
        alliance.currency = alliance.currency.id;

        return this.setState({ data: alliance, loading: false });
      }

      this.setState({ loading: false });
    });

    this.subscribe(wizardStore, ALLIANCE_INFORMATION_CREATE_EVENT, () => {
      this.props.nextScreen();
    });

    this.subscribe(wizardStore, ALLIANCE_INFORMATION_UPDATE_EVENT, () => {
      this.props.nextScreen();
    });

    // if the user has alliance invitations set the invitations
    if (allianceInvitations.length) {
      return this.setState({ loading: false, allianceInvitations });
    }

    fetchAllianceFormData();
    fetchAllianceInformation();
  }

  handleChange = (name, value) => {
    const { data } = this.state;
    data[name] = value;
    this.setState({ data });
  };

  handleSubmit = () => {
    if (this.state.loading) return;

    const { allianceInvitations, data } = this.state;

    this.setState({ loading: true });

    if (allianceInvitations.length) {
      return this.props.nextScreen();
    }

    const allianceData = R.clone(data);
    if (allianceData.id) {
      const { id, name, description, invitationEmail, currency } = allianceData;
      return updateAllianceInformation({ id, name, description, invitationEmail, currency });
    }

    createAllianceWizard(allianceData);
  };

  onSelectForAccept = (invitation, name) => {
    this.setState({
      invitation,
      yesNoDialogIsOpen: true,
      yesNoDialogTitle: name,
      yesNoDialogDescription: ACCEPT_ALLIANCE_INVITATION,
    });
  };

  onAccept = () => {
    this.setState(
      {
        yesNoDialogIsOpen: false,
        loading: true,
      },
      () => {
        const invitation = R.clone(this.state.invitation);
        acceptAllianceInvitation(invitation, this.companyUsers[0]);
      },
    );
  };

  onClose = () => {
    this.setState({
      yesNoDialogIsOpen: false,
    });
  };

  onNoCompanyErrorDialogOk = () => {
    this.setState({ noCompanyErrorDialogIsOpen: false });
  };

  render() {
    const { layout, currentScreen, prevScreen } = this.props;
    const {
      data,
      loading,
      allianceInvitations,
      yesNoDialogIsOpen,
      yesNoDialogTitle,
      yesNoDialogDescription,
      noCompanyErrorDialogIsOpen,
      currencies,
    } = this.state;
    const { name, description, invitationEmail, allianceInvitationRelation, currency } = data;
    const isInvitationDisabled = allianceInvitationRelation && allianceInvitationRelation.length;
    let content = null;

    if (loading) return <Loader stretch />;

    if (allianceInvitations.length) {
      content = (
        <Grid.Box>
          <DetailViewCardBody>
            {allianceInvitations.map((invitation, index) => {
              const name = invitation.alliance.name;
              const status = invitation.alliance.status;
              const ownerName = invitation.alliance.owner
                ? `${invitation.alliance.owner.firstName} ${invitation.alliance.owner.lastName}`
                : '';
              const clientCompanyName = invitation.alliance.clientCompany
                ? invitation.alliance.clientCompany.name
                : '';
              const invitationFooter =
                invitation.status !== 'PENDING' ? (
                  <Status status={invitation.status} />
                ) : (
                  <ActionButton
                    text={'Accept invitation'}
                    onClick={() => this.onSelectForAccept(invitation, invitation.alliance.name)}
                  />
                );

              return (
                <Fragment key={index}>
                  <table className="details">
                    <tbody>
                      <tr>
                        <th>Name</th>
                        <td>
                          <DetailValue text={name} />
                        </td>
                      </tr>
                      <tr>
                        <th>Status</th>
                        <td>
                          <Status status={status} />
                        </td>
                      </tr>
                      <tr>
                        <th>Owner</th>
                        <td>
                          <DetailValue text={ownerName} />
                        </td>
                      </tr>
                      <tr>
                        <th>Client Company</th>
                        <td>
                          <DetailValue text={clientCompanyName} />
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <Row offsetY={'md'} justifyContent={'center'}>
                    {invitationFooter}
                  </Row>
                </Fragment>
              );
            })}
          </DetailViewCardBody>
        </Grid.Box>
      );
    }

    if (!allianceInvitations.length) {
      content = (
        <Card.Body style={{ paddingRight: '35px' }}>
          <Grid.Layout columns={layout}>
            <Grid.Box>
              <InputField
                stretch
                label="Alliance"
                placeholder="Alliance Name"
                input={{
                  name: 'name',
                  value: name,
                  onChange: (value, e) => this.handleChange(e.target.name, value),
                }}
              />
              <TextAreaField
                style={{ width: '100%', height: 152 }}
                placeholder="Alliance Description"
                label="Description"
                input={{
                  name: 'description',
                  value: description,
                  onChange: (value, e) => this.handleChange(e.target.name, value),
                }}
              />
              <SelectField
                label="Currency"
                input={{
                  name: 'currency',
                  value: currency,
                  onChange: (value, e) => this.handleChange('currency', value),
                }}
                meta={{}}
                placeholder="Currency of your Alliance"
                options={currencies.map((currency) => {
                  return { label: currency.shortName, value: currency.id };
                })}
              />
            </Grid.Box>
            <Grid.Box />
            <Grid.Box>
              <Label>
                <CenterParagraph>
                  Invite your Alliance Partner to the <strong>Succeed</strong> platform.
                </CenterParagraph>
                <Text>
                  Enter the email address for the main contact person for your Alliance Partner:
                </Text>
              </Label>
              <br />
              <InputField
                disabled={isInvitationDisabled}
                stretch
                label="Email Address"
                placeholder="Email Address"
                input={{
                  name: 'invitationEmail',
                  type: 'email',
                  value: invitationEmail,
                  onChange: (value, e) => this.handleChange(e.target.name, value),
                }}
              />
            </Grid.Box>
          </Grid.Layout>
        </Card.Body>
      );
    }

    return (
      <React.Fragment>
        {content}
        <CardFooter>
          <ActionButton onClick={() => this.handleSubmit()} text="Continue" />
          {currentScreen > 0 ? (
            <Button type="button" onClick={(e) => prevScreen()} color="neutral" variant="outlined">
              {'Previous'}
            </Button>
          ) : (
            ''
          )}
        </CardFooter>
        <YesNoDialog
          isOpen={yesNoDialogIsOpen}
          onYes={this.onAccept}
          onNo={this.onClose}
          onClose={this.onClose}
          text={yesNoDialogDescription}
          title={yesNoDialogTitle}
        />
        <ErrorDialog
          isOpen={noCompanyErrorDialogIsOpen}
          onOk={this.onNoCompanyErrorDialogOk}
          text={'You must create a Company in order to accept Alliance Invitations'}
          okText={'Continue'}
          title={'Error accepting the Alliance Invitation'}
        />
      </React.Fragment>
    );
  }
}

AllianceInformation.propTypes = {
  layout: PropTypes.string.isRequired,
  currentScreen: PropTypes.number.isRequired,
  nextScreen: PropTypes.func.isRequired,
  prevScreen: PropTypes.func.isRequired,
};

export default withRouter(AllianceInformation);

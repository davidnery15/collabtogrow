import React from 'react';
import { Grid, Loader, Card, Button } from '@8base/boost';
import { CardFooter } from '../components/CardFooter';
import sessionStore, { NEW_SESSION_EVENT } from './../../../shared/SessionStore';
import wizardStore, {
  WIZARD_ERROR,
  COMPANY_INFORMATION_EVENT,
  COMPANY_INFORMATION_CREATE_EVENT,
  COMPANY_INFORMATION_UPDATE_EVENT,
} from '../WizardStore';
import PropTypes from 'prop-types';
import {
  updateCompanyInformation,
  fetchCompanyInformation,
  createCompany,
} from '../company.actions';
import { updateStateFromObject } from '../../../shared/utils';
import View from '@cobuildlab/react-flux-state';
import { COMPANY_ADMINISTRATOR } from '../../../shared/roles';
import { onChangeMixin } from '../../../shared/mixins';
import CompanyForm from '../../settings/company-management/components/CompanyForm';
import CompanyModel from '../../settings/company-management/company.model';
import * as R from 'ramda';
import { ActionButton } from '../../../components/buttons/ActionButton';
import { onErrorMixin } from '../../../shared/mixins';

class CompanyInformation extends View {
  constructor(props) {
    super(props);
    this.state = {
      data: R.clone(CompanyModel),
      countries: [],
      usaStates: [],
      industries: [],
      loading: true,
      iAmOwner: false, // Whether if I'm owner of the current Company
      isDisabled: true,
    };

    this.onChange = onChangeMixin.bind(this);
    this.onError = onErrorMixin.bind(this);
  }

  componentDidMount() {
    this.user = sessionStore.getState(NEW_SESSION_EVENT).user;

    this.subscribe(wizardStore, WIZARD_ERROR, this.onError);

    this.subscribe(wizardStore, COMPANY_INFORMATION_EVENT, (data) => {
      const { companyUsersList, countriesList, statesList, industriesList } = data;
      let logoFile = null;
      if (companyUsersList.items.length > 0) {
        if (companyUsersList.items[0].company.logoFile) {
          logoFile = companyUsersList.items[0].company.logoFile; // Used To save the logoFile and don't lose it in updateStateFromObject. Used to show the logo file in update company wizard
        }
      }
      console.log(logoFile);
      // If I belong to a Company, but I'm not the Owner I can't do anything
      let company;
      if (
        companyUsersList.count > 0 &&
        companyUsersList.items[0].role.name !== COMPANY_ADMINISTRATOR
      ) {
        company = updateStateFromObject(this.state.data, companyUsersList.items[0].company);
        company.logoFile = logoFile;
        return this.setState({
          loading: false,
          countries: countriesList.items,
          usaStates: statesList.items,
          industries: industriesList.items,
          data: company,
        });
      }

      // If i belong to a Company and I'm the Owner then I can Edit From here
      if (
        companyUsersList.count > 0 &&
        companyUsersList.items[0].role.name === COMPANY_ADMINISTRATOR
      ) {
        company = updateStateFromObject(this.state.data, companyUsersList.items[0].company);
        company.logoFile = logoFile;
        this.setState({ data: company });
      }

      // If I don't belong to any company, then I can Edit anyway to create One
      this.setState({
        iAmOwner: true,
        countries: countriesList.items,
        usaStates: statesList.items,
        industries: industriesList.items,
        loading: false,
        isDisabled: false,
      });
    });

    this.subscribe(wizardStore, COMPANY_INFORMATION_UPDATE_EVENT, () => {
      this.props.nextScreen();
    });

    this.subscribe(wizardStore, COMPANY_INFORMATION_CREATE_EVENT, () => {
      this.props.nextScreen();
    });

    fetchCompanyInformation();
  }

  handleSubmit = () => {
    if (this.state.loading) return;

    const data = R.clone(this.state.data);
    if (data.companyStatus === 'private') delete data.tickerSymbol;
    this.setState(
      {
        loading: true,
      },
      () => {
        if (data.id === null) {
          delete data.id;
          createCompany(data);
        } else {
          updateCompanyInformation(data);
        }
      },
    );
  };

  render() {
    const { prevScreen } = this.props;
    const { loading, iAmOwner, isDisabled } = this.state;

    if (loading === true) {
      return <Loader stretch />;
    }

    const footerComponent = (
      <CardFooter>
        <ActionButton onClick={(e) => this.handleSubmit()} text="Continue" />
        <Button type="button" onClick={(e) => prevScreen()} color="neutral" variant="outlined">
          {'Previous'}
        </Button>
      </CardFooter>
    );

    if (iAmOwner === false) {
      return (
        <React.Fragment>
          <Card.Body style={{ paddingRight: '35px' }}>
            <Grid.Layout>
              <Grid.Box>
                You already belong to the Company named: <strong> {this.state.data.name}, </strong>{' '}
                ask the owner to edit this Company Information.
                <br />
                <br />
                Or you can choose to <strong>Leave</strong> the Company from your{' '}
                <strong>Settings Menu</strong> and create a New one.
              </Grid.Box>
            </Grid.Layout>
          </Card.Body>
          {footerComponent}
        </React.Fragment>
      );
    }

    return (
      <React.Fragment>
        <Card.Body style={{ paddingRight: '35px' }}>
          <CompanyForm
            isDisabled={isDisabled}
            data={this.state.data}
            onChange={this.onChange}
            countries={this.state.countries}
            usaStates={this.state.usaStates}
            industries={this.state.industries}
          />
        </Card.Body>
        {footerComponent}
      </React.Fragment>
    );
  }
}

CompanyInformation.propTypes = {
  currentScreen: PropTypes.number.isRequired,
  nextScreen: PropTypes.func.isRequired,
  prevScreen: PropTypes.func.isRequired,
};

export default CompanyInformation;

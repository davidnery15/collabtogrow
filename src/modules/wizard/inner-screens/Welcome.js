import React from 'react';
import { Grid, Card, Paragraph } from '@8base/boost';
import { EmptyCardFooter } from '../components/CardFooter';
import PropTypes from 'prop-types';
import collabLogo from 'images/logos/collabtogrow_main_logo.svg';
import CenterParagraph from '../../../components/CenterParagraph';
import { ActionButton } from '../../../components/buttons/ActionButton';

/**
 * Welcome screen of the Wizard
 * @param {layout, currentScreen, prevScreen, nextScreen} props
 */
const Welcome = (props) => {
  const { layout, nextScreen } = props;
  const handleSubmit = () => {
    nextScreen(false);
  };
  return (
    <React.Fragment>
      <Card.Body style={{ paddingRight: '35px' }}>
        <Grid.Layout style={{ height: 500 }} columns={layout}>
          <Grid.Box />
          <Grid.Box style={{ width: '100%' }}>
            <div style={{ textAlign: 'center', margin: 'auto' }}>
              <img style={{ width: 333 }} src={collabLogo} alt="CollabToGrow" />
              <CenterParagraph>
                Welcome to the collabtogrow™ <strong>Succeed</strong> Platform.
              </CenterParagraph>
              <Paragraph style={{ width: 500, fontSize: 13 }}>
                Let’s get started by completing some initial information about yourself, your
                company, and your alliance.
              </Paragraph>
              <ActionButton
                onClick={handleSubmit}
                text="Begin"
                style={{ float: 'none', margin: '35px auto 0px auto' }}
              />
            </div>
          </Grid.Box>
          <Grid.Box />
        </Grid.Layout>
      </Card.Body>
      <EmptyCardFooter />
    </React.Fragment>
  );
};

Welcome.propTypes = {
  layout: PropTypes.string.isRequired,
  currentScreen: PropTypes.number.isRequired,
  nextScreen: PropTypes.func.isRequired,
  prevScreen: PropTypes.func.isRequired,
};

export default Welcome;

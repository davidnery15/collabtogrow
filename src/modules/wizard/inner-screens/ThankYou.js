import React, { Component } from 'react';
import { Grid, Card, Button, Paragraph, Loader } from '@8base/boost';
import { EmptyCardFooter } from '../components/CardFooter';
import PropTypes from 'prop-types';
import businessCase from 'images/wizard/Business_Case_Graphic.svg';
import { withRouter } from 'react-router-dom';
import CenterParagraph from '../../../components/CenterParagraph';
import { fetchSession } from '../../auth/auth.actions';
import { NUMBER_OF_WIZARD_SCREENS } from 'modules/wizard';
import { updateMeta } from 'modules/dashboard/dashboard-actions';
import { META_STEP_NAME } from 'shared';

class ThankYou extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
    };
  }

  goToAlliances = async () => {
    this.setState({ loading: true });

    /**
     * force last STEP to avoid redirection to wizard if step is not last
     * this happens when the wizard steps are skiped by clicking the
     * LeftWizardSection menu
     * // The timeout if to avoid outdated query data
     */
    await updateMeta(META_STEP_NAME, NUMBER_OF_WIZARD_SCREENS + 1);
    await fetchSession();

    this.props.history.push('/welcome');
  };

  render() {
    const { layout } = this.props;
    const { loading } = this.state;

    if (loading) return <Loader stretch />;

    return (
      <React.Fragment>
        <Card.Body style={{ paddingRight: '35px' }}>
          <Grid.Layout style={{ height: 500 }} columns={layout}>
            <Grid.Box />
            <Grid.Box style={{ width: '100%' }}>
              <div style={{ textAlign: 'center', margin: 'auto' }}>
                <Paragraph
                  style={{
                    opacity: '0.9',
                    color: '#323C47',
                    fontSize: 16,
                    fontWeight: 600,
                    paddingRight: '0px',
                  }}>
                  Thank you for the info! Now, just one more thing.
                </Paragraph>
                <img
                  style={{ width: 212, margin: '26px 0' }}
                  src={businessCase}
                  alt="CollabToGrow"
                />
                <CenterParagraph>
                  Navigate to your Alliances to start Collaborating!
                </CenterParagraph>
                <Button
                  onClick={this.goToAlliances}
                  style={{ float: 'none', margin: '28px auto 5px auto' }}
                  type="button">
                  {'Alliances'}
                </Button>
              </div>
            </Grid.Box>
            <Grid.Box />
          </Grid.Layout>
        </Card.Body>
        <EmptyCardFooter />
      </React.Fragment>
    );
  }
}

ThankYou.propTypes = {
  layout: PropTypes.string.isRequired,
  history: PropTypes.object.isRequired,
};

export default withRouter(ThankYou);

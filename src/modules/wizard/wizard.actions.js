import {
  CREATE_PHONE_MUTATION,
  DELETE_PHONE_MUTATION,
  UPDATE_USER_MUTATION,
  USER_INFORMATION_QUERY,
  UPDATE_PHONE_MUTATION,
} from './queries';
import Flux from '@cobuildlab/flux-state';
import { USER_INFORMATION_EVENT, USER_UPDATE_EVENT, WIZARD_ERROR } from './WizardStore';
import sessionStore, { APOLLO_CLIENT, NEW_SESSION_EVENT } from '../../shared/SessionStore';
import { updateUserPhonesValidator, updateUserValidator } from './users.validators';
import { log, error } from '@cobuildlab/pure-logger';
import { sanitize8BaseDocumentUpdate } from '../../shared/utils';

type User = {
  id: string,
  firstName: string,
  lastName: string,
};

type UserInformation = {
  id: string,
  country: string,
};

type PhoneType = {
  id: string,
  name: string,
};

type Phone = {
  id: string,
  phone: string,
  type: PhoneType,
};

/**
 * Fetches a User information by Id
 * @param {User} user The User of the system
 * @param {UserInformation} userInformation The User additional information
 * @param {Array<Phone>} phones The phones
 * @param {Array<Phone>} originalPhones The Original phones
 */
export const updateUser = async (
  user: User,
  userInformation: UserInformation,
  phones: Array<Phone>,
  originalPhones,
) => {
  // Country
  if (userInformation.country !== null)
    userInformation.country = { connect: { id: userInformation.country } };
  else delete userInformation.country;

  // Picture
  sanitize8BaseDocumentUpdate(user, 'avatar');

  const data = { user, userInformation };

  try {
    updateUserValidator(data);
  } catch (err) {
    error('UpdateUserValidator', err);
    return Flux.dispatchEvent(WIZARD_ERROR, err);
  }

  log('updateUser Phones', phones);

  try {
    updateUserPhonesValidator(phones);
  } catch (err) {
    console.error('UpdateUserPhonesValidator', err);
    return Flux.dispatchEvent(WIZARD_ERROR, err);
  }

  const client = sessionStore.getState(APOLLO_CLIENT);

  let userResult;

  try {
    userResult = await client.mutate({
      mutation: UPDATE_USER_MUTATION,
      variables: data,
    });
  } catch (e) {
    console.error('updateUser', e);
    return Flux.dispatchEvent(WIZARD_ERROR, e);
  }

  log('updateUser:userResult:', userResult);

  let phonesData;
  try {
    phonesData = await updatePhones(user, phones, originalPhones);
  } catch (e) {
    error('update Phone', e, data);
    return Flux.dispatchEvent(WIZARD_ERROR, e);
  }

  Flux.dispatchEvent(USER_UPDATE_EVENT, {
    user: userResult,
    phones: phonesData,
  });

  return { user: userResult, phones: phonesData };
};

/**
 * Fetches a User information by Id
 * @return {Promise<void>}
 */
export const fetchUserInformation = async (): void => {
  const client = sessionStore.getState(APOLLO_CLIENT);
  const user = sessionStore.getState(NEW_SESSION_EVENT).user;
  let response;
  try {
    response = await client.query({
      query: USER_INFORMATION_QUERY,
      variables: { userId: user.id },
      fetchPolicy: 'network-only',
    });
  } catch (e) {
    console.error('fetchUserInformation', e);
    return Flux.dispatchEvent(WIZARD_ERROR, e);
  }
  console.log('fetchUserInformation', response.data);
  return Flux.dispatchEvent(USER_INFORMATION_EVENT, response.data);
};

/**
 * Create, Update, Delete phone
 * @param {User} user The User of the system
 * @param {Array<Phone>} phones The phones
 * @param {Array<Phone>} originalPhones The Original phones
 *
 * return {dataPhone}
 */

export const updatePhones = async (user, phones, originalPhones): void => {
  log('updatePhones', phones, originalPhones);

  const client = sessionStore.getState(APOLLO_CLIENT);
  const toBeDeleted = [],
    toBeUpdated = [],
    toBeCreated = [];
  let phonesData = [];

  phones.forEach((data) => {
    if (data.id === null) toBeCreated.push(data);
    else {
      const originalPhone = originalPhones.find((original) => original.id === data.id);
      if (originalPhone.phone !== data.phone) toBeUpdated.push(data);
      else phonesData.push(data);
    }
  });

  originalPhones.forEach((data) => {
    const originalPhone = phones.find((original) => original.id === data.id);
    if (originalPhone === undefined) toBeDeleted.push(data);
  });

  toBeCreated.forEach(async (phone) => {
    let phoneResult, data;
    data = {
      phone: phone.phone,
      type: { connect: { id: phone.type.id } },
      user: { connect: { id: user.id } },
    };
    try {
      phoneResult = await client.mutate({ mutation: CREATE_PHONE_MUTATION, variables: { data } });
    } catch (e) {
      error('update Phone', e, data);
      return Flux.dispatchEvent(WIZARD_ERROR, e);
    }
    phonesData.push(phoneResult.data.phoneCreate);
  });

  toBeDeleted.forEach(async (phone) => {
    const data = { id: phone.id };
    try {
      await client.mutate({
        mutation: DELETE_PHONE_MUTATION,
        variables: { data },
      });
    } catch (e) {
      error('deletePhone', e, data);
      return Flux.dispatchEvent(WIZARD_ERROR, e);
    }
  });

  toBeUpdated.forEach(async (phone) => {
    let phoneResult, data;
    data = {
      id: phone.id,
      phone: phone.phone,
      type: { connect: { id: phone.type.id } },
    };
    try {
      phoneResult = await client.mutate({
        mutation: UPDATE_PHONE_MUTATION,
        variables: { data: data },
      });
    } catch (e) {
      error('updatePhone', e, data);
      return Flux.dispatchEvent(WIZARD_ERROR, e);
    }
    phonesData.push(phoneResult.data.phoneUpdate);
  });

  return phonesData;
};

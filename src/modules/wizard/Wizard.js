import React, { Component } from 'react';
import { Grid } from '@8base/boost';
import { LeftWizardSection } from './components/LeftWizardSection';
import { RightWizardSection } from './components/RightWizardSection';
import { SCREENS } from './index';
import styled from '@emotion/styled';
import './css/wizard.css';
import { withApollo } from 'react-apollo';
import PropTypes from 'prop-types';
import sessionStore, { NEW_SESSION_EVENT } from '../../shared/SessionStore';
import { APOLLO_CLIENT } from './../../shared/SessionStore';
import { META_STEP_NAME } from '../../shared';
import { updateMeta } from '../../modules/dashboard/dashboard-actions';
import { fetchSession } from '../../modules/auth/auth.actions';
import Flux from '@cobuildlab/flux-state';
import * as R from 'ramda';

const MainLayout = styled(Grid.Layout)({
  backgroundColor: 'white',
  boxShadow: '1px 1px 5px darkgrey',
  borderRadius: 5,
  maxWidth: 1122,
  margin: '0 auto',
  transition: 'all .5s',
});

class Wizard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentScreen: 0,
    };
    this.selectScreen = this.selectScreen.bind(this);
    this.nextScreen = this.nextScreen.bind(this);
    this.prevScreen = this.prevScreen.bind(this);
  }

  componentWillMount() {
    //Placed this Process here to avoid the the blink of the first page of the wizard between steps
    const { computedMatch } = this.props;
    if (computedMatch.params.step) this.selectScreen(parseInt(computedMatch.params.step));
  }

  componentWillUnmount() {
    this.apolloClientSubscription.unsubscribe();
  }

  componentDidMount() {
    this.apolloClientSubscription = sessionStore.subscribe(APOLLO_CLIENT, (state) => {});

    // To avoid the dispatch cycle when redirected here
    setTimeout(() => Flux.dispatchEvent(APOLLO_CLIENT, this.props.client), 1000);
  }

  selectScreen(selection) {
    this.setState({ currentScreen: selection });
  }

  async nextScreen(callUpdateMeta = true) {
    const currentScreen = R.clone(this.state.currentScreen);
    if (callUpdateMeta) {
      await updateMeta(META_STEP_NAME, currentScreen + 1);
      await fetchSession();
    }
    this.setState({ currentScreen: currentScreen + 1 });
  }

  prevScreen() {
    this.setState({ currentScreen: this.state.currentScreen - 1 });
  }

  render() {
    const current = SCREENS[this.state.currentScreen];
    const allianceInvitationsList = sessionStore.getState(NEW_SESSION_EVENT)
      .allianceInvitationsList;

    return (
      <MainLayout columns="minmax(260px, 286px) auto">
        <LeftWizardSection
          sections={SCREENS}
          currentScreen={this.state.currentScreen}
          selectScreen={this.selectScreen}
        />
        <RightWizardSection
          screen={current}
          currentScreen={this.state.currentScreen}
          nextScreen={this.nextScreen}
          prevScreen={this.prevScreen}
          allianceInvitationsList={allianceInvitationsList}
        />
      </MainLayout>
    );
  }
}

Wizard.propTypes = {
  client: PropTypes.object.isRequired,
  computedMatch: PropTypes.object,
};
Wizard.defaultProps = {
  computedMatch: PropTypes.object,
};

export default withApollo(Wizard);

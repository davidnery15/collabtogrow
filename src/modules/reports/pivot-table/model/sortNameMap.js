const SORT_NAME_MAP = {
  owner: 'owner.firstName',
  createdBy: 'createdBy.firstName',
  assignedTo: 'assignedTo.firstName',
  requestedBy: 'requestedBy.firstName',
  company: 'company.name',
  source: 'source.name',
};
export { SORT_NAME_MAP };

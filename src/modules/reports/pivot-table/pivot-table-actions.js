import Flux from '@cobuildlab/flux-state';
import { PIVOT_TABLE_FILTERS_EVENT } from './pivot-table-store';

export const savePivotTableFilters = (data) => {
  Flux.dispatchEvent(PIVOT_TABLE_FILTERS_EVENT, data);
};

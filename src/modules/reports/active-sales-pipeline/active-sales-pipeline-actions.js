import Flux from '@cobuildlab/flux-state';
import { ACTIVE_SALES_PIPELINE_FILTERS_EVENT } from './active-sales-pipeline-store';

export const saveActiveSalesPipelineFilters = (data) => {
  Flux.dispatchEvent(ACTIVE_SALES_PIPELINE_FILTERS_EVENT, data);
};

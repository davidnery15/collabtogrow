import sessionStore, { NEW_SESSION_EVENT } from '../../../shared/SessionStore';
import { fetchInitiativeList } from '../../management/initiative/initiative-actions';
import { fetchAlliance } from '../../settings/alliance-management/alliance-actions';
import {
  BALANCED_SCORECARD_DATA_EVENT,
  BALANCED_SCORECARD_ERROR_EVENT,
} from './balanced-scorecard-store';
import { setDefaultKPIs } from './balanced-scorecard-utils';
import { fetchIdeas } from '../../management/idea/idea-actions';
import { fetchInvestmentItems } from '../../management/investment-item/investment-item-actions';
import { fetchAllDealsData } from '../../management/deal/deal-actions';
import Flux from '@cobuildlab/flux-state';
import { chartsMonths } from '../utils';
import {
  BOOKINGS_CLIENT_TYPE,
  BOOKINGS_PARTNER_TYPE,
  JOINT_INNOVATION_IDEAS_TYPE,
  JOINT_INITIATIVES_TYPE,
  JOINT_TRAINING_INVESTMENT_TYPE,
  JOINT_REFERENCEABLE_CLIENTS_TYPE,
  PIPELINE_CLIENT_TYPE,
  PIPELINE_PARTNER_TYPE,
  JOINT_PROPOSALS_ISSUED_TYPE,
  JOINT_PROPOSALS_WON_TYPE,
  CONTRIBUTIONS_CLIENT_TYPE,
  CONTRIBUTIONS_PARTNER_TYPE,
} from '../../settings/alliance-management/allianceKPIs/allianceKPIs-model';
import moment from 'moment';

/**
 * Fetches the Initiative & ideas list.
 *
 * @returns {Promise}
 * @param year
 */
export const fetchBalancedScorecardData = async (year = moment().year()) => {
  const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
  const allianceId = selectedAlliance.id;

  const yearDealsFilter = {
    year,
    fieldName: 'closeDate',
    isDateTime: false,
  };

  const yearContributionsFilter = {
    year,
    fieldName: 'contributionDate',
    isDateTime: false,
  };

  const yearApprovedFilter = {
    year,
    fieldName: 'dateOfResponse',
    isDateTime: true,
  };

  let responses;
  try {
    responses = await Promise.all([
      fetchAlliance(allianceId),
      //TODO: Use query filter instead of first 1000
      // TODO: This is on the Deals List too
      fetchInitiativeList('', 1, 1000, null, yearApprovedFilter),
      fetchIdeas('', 1, 1000, null, yearApprovedFilter),
      fetchInvestmentItems('', 1, 1000, null, null, yearContributionsFilter),
      fetchAllDealsData('', yearDealsFilter),
    ]);
  } catch (e) {
    Flux.dispatchEvent(BALANCED_SCORECARD_ERROR_EVENT, e);
    throw e;
  }

  const [
    { alliance },
    {
      initiativesList: { items: initiatives },
    },
    {
      itemsList: { items: ideas },
    },
    {
      itemsList: { items: investmentItems },
    },
    {
      dealDataList: { items: deals },
    },
  ] = responses;

  const {
    allianceKPIAllianceRelation: { items: kpis },
  } = alliance;
  const contributions = investmentItems.filter((item) => item.contribution);

  setDefaultKPIs(kpis, year);
  const data = {
    kpis,
    initiatives,
    ideas,
    contributions,
    deals,
    alliance,
  };

  Flux.dispatchEvent(BALANCED_SCORECARD_DATA_EVENT, data);
  return data;
};

export const createChartData = (monthByMonthCurrent, monthByMonthAccumulator, type) => {
  return chartsMonths().map((opt, index) => {
    if (type === JOINT_INNOVATION_IDEAS_TYPE) {
      return {
        ...opt,
        ideas: monthByMonthCurrent[index].toString(),
        accumulatorIdeas: monthByMonthAccumulator[index],
      };
    }
    if (type === JOINT_INITIATIVES_TYPE) {
      return {
        ...opt,
        initiatives: monthByMonthCurrent[index].toString(),
        accumulatorInitiatives: monthByMonthAccumulator[index],
      };
    }

    if (type === JOINT_TRAINING_INVESTMENT_TYPE) {
      return {
        ...opt,
        investment: monthByMonthCurrent[index].toString(),
        accumulatorInvestment: monthByMonthAccumulator[index],
      };
    }

    if (type === PIPELINE_PARTNER_TYPE || type === PIPELINE_CLIENT_TYPE) {
      return {
        ...opt,
        pipeline: monthByMonthCurrent[index].toString(),
        accumulatorPipeline: monthByMonthAccumulator[index],
      };
    }

    if (type === JOINT_PROPOSALS_ISSUED_TYPE) {
      return {
        ...opt,
        issued: monthByMonthCurrent[index].toString(),
        accumulatorIssued: monthByMonthAccumulator[index],
      };
    }

    if (type === JOINT_PROPOSALS_WON_TYPE) {
      return {
        ...opt,
        won: monthByMonthCurrent[index].toString(),
        accumulatorWon: monthByMonthAccumulator[index],
      };
    }

    if (type === BOOKINGS_CLIENT_TYPE || type === BOOKINGS_PARTNER_TYPE) {
      return {
        ...opt,
        booked: monthByMonthCurrent[index].toString(),
        accumulatorBooked: monthByMonthAccumulator[index],
      };
    }

    if (type === CONTRIBUTIONS_CLIENT_TYPE || type === CONTRIBUTIONS_PARTNER_TYPE) {
      return {
        ...opt,
        contribution: monthByMonthCurrent[index].toString(),
        accumulatorContribution: monthByMonthAccumulator[index],
      };
    }

    if (type === JOINT_REFERENCEABLE_CLIENTS_TYPE) {
      return {
        ...opt,
        referenceable: monthByMonthCurrent[index].toString(),
        accumulatorReferenceable: monthByMonthAccumulator[index],
      };
    }
  });
};

import styled from '@emotion/styled';

const DetailCardFooter = styled.div`
  padding: 16px 24px !important;
`;

export { DetailCardFooter };

import React from 'react';
import { Row, Column, Card, SelectField, Loader, Grid } from '@8base/boost';
import {
  KPITitle,
  KPIProgress,
  ScorecardSection,
  ScorecardDate,
  DetailCardFooter,
  ScorecardTarget,
  BalanceScorecardChartLegend,
} from './';
import { getBalancedScorecardData, getKPIName } from '../balanced-scorecard-utils';
import { KPI_FIELDS } from '../../reports-model';
import { KPICharts } from './KPICharts';
import View from '@cobuildlab/react-flux-state';
import { onErrorMixin } from '../../../../shared/mixins';
import balancedScorecardStore, {
  BALANCED_SCORECARD_DATA_EVENT,
  BALANCED_SCORECARD_ERROR_EVENT,
} from '../balanced-scorecard-store';
import { fetchBalancedScorecardData } from '../balanced-scorecard-actions';
import { DealList } from './DealList';
import {
  CONTRIBUTIONS_CLIENT_TYPE,
  CONTRIBUTIONS_PARTNER_TYPE,
} from '../../../settings/alliance-management/allianceKPIs/allianceKPIs-model';
import { ContributionList } from './ContributionList';
import moment from 'moment';
import * as R from 'ramda';
import { KPI_WITH_CURRENCY_TYPES } from '../../../settings/alliance-management/allianceKPIs/allianceKPIs-model';
import { getCurrencyOnSession } from '../../../../shared/alliance-utils';
import sessionStore, { NEW_SESSION_EVENT } from '../../../../shared/SessionStore';

class ScorecardFinancialDetail extends View {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      financialKPIs: [],
      selectedFinancialKPI: null,
      selectedKPIYear: moment().year(),
      kpiYears: [],
    };

    this.onError = onErrorMixin.bind(this);
    this.selectedAlliance = sessionStore.getState(NEW_SESSION_EVENT).selectedAlliance;
  }

  componentDidMount() {
    this.subscribe(balancedScorecardStore, BALANCED_SCORECARD_ERROR_EVENT, this.onError);

    this.subscribe(balancedScorecardStore, BALANCED_SCORECARD_DATA_EVENT, async (data) => {
      const year = R.clone(this.state.selectedKPIYear);
      const { kpis, initiatives, ideas, contributions, deals, alliance } = data;
      const { financialKPIs, kpiYears } = await getBalancedScorecardData(
        kpis,
        initiatives,
        ideas,
        contributions,
        deals,
        alliance,
        year,
      );

      const selectedFinancialKPI = financialKPIs.length ? financialKPIs[0] : null;
      this.setState({
        kpiYears,
        financialKPIs,
        selectedFinancialKPI,
        selectedKPIYear: year,
        loading: false,
      });
    });

    fetchBalancedScorecardData(R.clone(this.state.selectedKPIYear));
  }

  onSelectedKPI = (selectedFinancialKPI) => {
    this.setState({ selectedFinancialKPI });
  };

  onSelectedYearChange = (year) => {
    this.setState({ selectedKPIYear: year, loading: true }, () => {
      fetchBalancedScorecardData(R.clone(this.state.selectedKPIYear));
    });
  };

  render() {
    const { loading, selectedFinancialKPI, financialKPIs, kpiYears, selectedKPIYear } = this.state;
    const currency = getCurrencyOnSession();
    let content = <></>;
    let list = <></>;

    if (loading)
      content = (
        <Row growChildren gap="lg" offsetY="lg" offsetX="lg">
          <Loader stretch />
        </Row>
      );

    if (!loading && selectedFinancialKPI) {
      const kpiCurrency = KPI_WITH_CURRENCY_TYPES.includes(selectedFinancialKPI.type)
        ? currency
        : null;

      const kpiName = getKPIName(this.selectedAlliance, selectedFinancialKPI.type);

      content = (
        <Card.Body padding="none">
          <Row>
            <Column style={{ width: '50%' }} alignItems="stretch">
              <KPICharts selectedKPI={selectedFinancialKPI} currency={kpiCurrency} />
              <div style={{ paddingLeft: '20%' }}>
                <BalanceScorecardChartLegend
                  name={`${kpiName} - Cumulative Actuals `}
                  color={'#3EB7F9'}
                />
                <BalanceScorecardChartLegend
                  name={`${kpiName} Cumulative Target `}
                  color={'#70D34C'}
                />
              </div>
            </Column>
            <Column
              style={{ width: '50%', borderLeft: '1px solid #e9eff4', minHeight: '465px' }}
              alignItems="stretch">
              {financialKPIs.map((kpi, i) => {
                const selected = kpi.name === selectedFinancialKPI.name;
                return (
                  <ScorecardSection
                    selected={selected}
                    onClick={() => this.onSelectedKPI(kpi)}
                    key={i}>
                    <Row growChildren>
                      <Column alignItems="start">
                        <KPITitle text={kpi.name} selected={selected} />
                      </Column>
                      <Column alignItems="end">
                        <Row>
                          <ScorecardTarget
                            currency={kpiCurrency}
                            kpiType={kpi.type}
                            fullYearTarget={kpi.fullYearTarget}
                            proRataTarget={kpi.proRataTarget}
                            kpiCurrent={kpi.current}
                          />
                        </Row>
                      </Column>
                    </Row>
                  </ScorecardSection>
                );
              })}

              <DetailCardFooter>
                <ScorecardDate date={new Date()} kpiName={selectedFinancialKPI.name} />
                <KPIProgress
                  kpiType={selectedFinancialKPI.type}
                  kpiTarget={selectedFinancialKPI.fullYearTarget}
                  current={selectedFinancialKPI.current}
                  text="Full Year"
                  currency={kpiCurrency}
                />
                <KPIProgress
                  kpiType={selectedFinancialKPI.type}
                  kpiTarget={selectedFinancialKPI.proRataTarget}
                  current={selectedFinancialKPI.current}
                  text="Pro Rata"
                  currency={kpiCurrency}
                />
              </DetailCardFooter>
            </Column>
          </Row>
        </Card.Body>
      );

      list =
        selectedFinancialKPI.type === CONTRIBUTIONS_CLIENT_TYPE ||
        selectedFinancialKPI.type === CONTRIBUTIONS_PARTNER_TYPE ? (
            <ContributionList selectedKPI={selectedFinancialKPI} />
          ) : (
            <DealList currency={kpiCurrency} selectedKPI={selectedFinancialKPI} />
          );
    }

    return (
      <>
        <Card>
          <Card.Header>
            <SelectField
              input={{
                name: 'optionKPI',
                value: 'financial',
                onChange: (value) => {
                  this.props.history.push(`/reports/balanced-scorecard/${value}/`);
                },
              }}
              meta={{}}
              options={KPI_FIELDS}
              style={{ width: '156px', height: '37px' }}
            />
            <Grid.Box direction="row" justifySelf="end" alignSelf="center">
              {kpiYears.length ? (
                <SelectField
                  style={{ width: '150px', alignSelf: 'end', marginRight: '15px' }}
                  input={{
                    name: 'yearFilter',
                    value: selectedKPIYear,
                    onChange: (year) => this.onSelectedYearChange(year),
                  }}
                  placeholder={'Select a Year'}
                  options={kpiYears.map((year) => {
                    return { label: year, value: year };
                  })}
                />
              ) : null}
            </Grid.Box>
          </Card.Header>
          {content}
        </Card>
        {list}
      </>
    );
  }
}

// ScorecardFinancialDetail.propTypes = {
//   kpi: PropTypes.object.isRequired,
// };

export { ScorecardFinancialDetail };

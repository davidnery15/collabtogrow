import React from 'react';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';
import { Card, Heading, Table } from '@8base/boost';
import { ListCardBody } from 'components/card/ListCardBody';
import { Link } from 'react-router-dom';
import { CurrencyTextField } from '../../../../shared/components/CurrencyTextField';

const StyledHeading = styled(Heading)`
  height: 28px;
  color: #323c47;
  font-family: Poppins;
  font-size: 20px;
  line-height: 30px;
`;

const ContributionList = ({ selectedKPI }) => {
  const { name: KPIname, contributions } = selectedKPI;
  return (
    <>
      <Card style={{ marginTop: '35px', marginBottom: '35px' }}>
        <Card.Header>
          <StyledHeading type="h4" text={KPIname} />
        </Card.Header>
        <Card.Body>
          <ListCardBody>
            <Table>
              <Table.Header className="justify-center-column" columns="4fr 4fr 4fr 4fr 4fr 4fr 4fr">
                <Table.HeaderCell className="name-column">Name</Table.HeaderCell>
                <Table.HeaderCell>Description</Table.HeaderCell>
                <Table.HeaderCell>Unit Type</Table.HeaderCell>
                <Table.HeaderCell>Unit Value</Table.HeaderCell>
                <Table.HeaderCell>Unit Monetization Factor</Table.HeaderCell>
                <Table.HeaderCell>Calculated Value</Table.HeaderCell>
                <Table.HeaderCell>Unit Description</Table.HeaderCell>
              </Table.Header>
              <Table.Body data={contributions} className="card-body-list">
                {(item, index) => {
                  const { contribution } = item;
                  const {
                    id,
                    description,
                    name,
                    unitType,
                    unitQuantity,
                    unitValueDescription,
                    calculatedValue,
                    unitMonetizationFactor,
                  } = contribution;

                  return (
                    <>
                      <Table.BodyRow columns="4fr 4fr 4fr 4fr 4fr 4fr 4fr" key={index}>
                        <Table.BodyCell className="justify-center-row">
                          <Link className="item-name" to={`/management/contribution/${id}/`}>
                            {name}
                          </Link>
                        </Table.BodyCell>
                        <Table.BodyCell className="justify-center-row">
                          {description}
                        </Table.BodyCell>
                        <Table.BodyCell className="justify-center-row">{unitType}</Table.BodyCell>
                        <Table.BodyCell className="justify-center-row">
                          {unitQuantity}
                        </Table.BodyCell>
                        <Table.BodyCell className="justify-center-row">
                          <CurrencyTextField value={unitMonetizationFactor} />
                        </Table.BodyCell>
                        <Table.BodyCell className="justify-center-row">
                          <CurrencyTextField value={calculatedValue} />
                        </Table.BodyCell>
                        <Table.BodyCell className="justify-center-row">
                          {unitValueDescription}
                        </Table.BodyCell>
                      </Table.BodyRow>
                    </>
                  );
                }}
              </Table.Body>
            </Table>
          </ListCardBody>
        </Card.Body>
      </Card>
    </>
  );
};

ContributionList.propTypes = {
  selectedKPI: PropTypes.object.isRequired,
};

export { ContributionList };

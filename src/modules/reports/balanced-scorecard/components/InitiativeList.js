import React from 'react';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';
import { Card, Heading, Table } from '@8base/boost';
import { ListCardBody } from 'components/card/ListCardBody';
import { Link } from 'react-router-dom';

const StyledHeading = styled(Heading)`
  height: 28px;
  width: 277px;
  color: #323c47;
  font-family: Poppins;
  font-size: 20px;
  line-height: 30px;
`;

const InitiativeList = ({ selectedKPI }) => {
  const { name: KPIname, initiatives } = selectedKPI;
  console.log('IdeaList', selectedKPI);
  return (
    <>
      <Card style={{ marginTop: '35px' }}>
        <Card.Header>
          <StyledHeading type="h4" text={KPIname} />
        </Card.Header>
        <Card.Body>
          <ListCardBody>
            <Table>
              <Table.Header className="justify-center-column" columns="3fr 3fr 3fr 3fr 3fr 3fr">
                <Table.HeaderCell className="name-column">Name</Table.HeaderCell>
                <Table.HeaderCell>Description</Table.HeaderCell>
                <Table.HeaderCell>Budget Utilized</Table.HeaderCell>
                <Table.HeaderCell>Forecasted End Date</Table.HeaderCell>
                <Table.HeaderCell>Owner</Table.HeaderCell>
                <Table.HeaderCell>Status</Table.HeaderCell>
              </Table.Header>
              <Table.Body data={initiatives} className="card-body-list">
                {(initiative, index) => {
                  const {
                    id,
                    owner,
                    description,
                    status,
                    name,
                    budgetUtilized,
                    forecastedEndDate,
                  } = initiative;
                  const username = owner ? `${owner.firstName} ${owner.lastName}` : '';

                  return (
                    <>
                      <Table.BodyRow columns="3fr 3fr 3fr 3fr 3fr 3fr" key={initiative.id}>
                        <Table.BodyCell className="justify-center-row">
                          <Link className="item-name" to={`/management/initiative/${id}/`}>
                            {name}
                          </Link>
                        </Table.BodyCell>
                        <Table.BodyCell className="justify-center-row">
                          {description}
                        </Table.BodyCell>
                        <Table.BodyCell className="justify-center-row">
                          {budgetUtilized}
                        </Table.BodyCell>
                        <Table.BodyCell className="justify-center-row">
                          {forecastedEndDate}
                        </Table.BodyCell>
                        <Table.BodyCell className="justify-center-row">{username}</Table.BodyCell>
                        <Table.BodyCell className="justify-center-row">{status}</Table.BodyCell>
                      </Table.BodyRow>
                    </>
                  );
                }}
              </Table.Body>
            </Table>
          </ListCardBody>
        </Card.Body>
      </Card>
    </>
  );
};

InitiativeList.propTypes = {
  selectedKPI: PropTypes.object.isRequired,
};

export { InitiativeList };

import React from 'react';
import { Heading, Row, Text } from '@8base/boost';
import PropTypes from 'prop-types';
import moment from 'moment';

const ScorecardDate = ({ kpiName, date }) => {
  const _date = moment(date).format('LL');

  return (
    <>
      <Heading type="h5" kind="secondary" weight="semibold">
        {_date}
      </Heading>
      <Row offsetBottom="lg">
        <Text color="DARK_GRAY3">{`${kpiName} % Completed`}</Text>
      </Row>
    </>
  );
};

ScorecardDate.propTypes = {
  date: PropTypes.string.isRequired,
  kpiName: PropTypes.string.isRequired,
};

export { ScorecardDate };

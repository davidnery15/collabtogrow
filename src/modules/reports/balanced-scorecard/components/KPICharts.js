import { Bar, BarChart, Tooltip, XAxis, YAxis } from 'recharts';
import React from 'react';
import PropTypes from 'prop-types';
import { Text } from '@8base/boost';
import styled from '@emotion/styled';
import { createChartData } from '../balanced-scorecard-actions';
import {
  JOINT_INNOVATION_IDEAS_TYPE,
  JOINT_INITIATIVES_TYPE,
  JOINT_TRAINING_INVESTMENT_TYPE,
  JOINT_REFERENCEABLE_CLIENTS_TYPE,
  PIPELINE_PARTNER_TYPE,
  PIPELINE_CLIENT_TYPE,
  JOINT_PROPOSALS_WON_TYPE,
  JOINT_PROPOSALS_ISSUED_TYPE,
  BOOKINGS_CLIENT_TYPE,
  BOOKINGS_PARTNER_TYPE,
  CONTRIBUTIONS_CLIENT_TYPE,
  CONTRIBUTIONS_PARTNER_TYPE,
} from '../../../settings/alliance-management/allianceKPIs/allianceKPIs-model';
import { numberFormatter } from '../../utils';
import BigInt from 'big-integer';
import { KPIChartToolTip } from './';

const StyleTitle = styled(Text)`
  opacity: 0.9;
  color: #323c47;
  font-family: Poppins;
  font-size: 16px;
  font-weight: 600;
  margin: 20px 0px 20px 10px !important;
`;

const KPICharts = ({ selectedKPI, currency }) => {
  const { name, monthByMonthCurrent, type } = selectedKPI;
  let { monthByMonthCumulativeTarget: monthByMonthAccumulator } = selectedKPI;
  monthByMonthAccumulator = monthByMonthAccumulator
    ? monthByMonthAccumulator
    : new Array(12).fill(0);

  const data = createChartData(monthByMonthCurrent, monthByMonthAccumulator, type);
  let dataKey;
  let dataKeyAccumulator;
  if (type === JOINT_INNOVATION_IDEAS_TYPE) {
    dataKey = 'ideas';
    dataKeyAccumulator = 'accumulatorIdeas';
  } else if (type === JOINT_INITIATIVES_TYPE) {
    dataKey = 'initiatives';
    dataKeyAccumulator = 'accumulatorInitiatives';
  } else if (type === JOINT_TRAINING_INVESTMENT_TYPE) {
    dataKey = 'investment';
    dataKeyAccumulator = 'accumulatorInvestment';
  } else if (type === JOINT_REFERENCEABLE_CLIENTS_TYPE) {
    dataKey = 'referenceable';
    dataKeyAccumulator = 'accumulatorReferenceable';
  } else if (type === PIPELINE_PARTNER_TYPE || type === PIPELINE_CLIENT_TYPE) {
    dataKey = 'pipeline';
    dataKeyAccumulator = 'accumulatorPipeline';
  } else if (type === JOINT_PROPOSALS_ISSUED_TYPE) {
    dataKey = 'issued';
    dataKeyAccumulator = 'accumulatorIssued';
  } else if (type === JOINT_PROPOSALS_WON_TYPE) {
    dataKey = 'won';
    dataKeyAccumulator = 'accumulatorWon';
  } else if (type === BOOKINGS_CLIENT_TYPE || type === BOOKINGS_PARTNER_TYPE) {
    dataKey = 'booked';
    dataKeyAccumulator = 'accumulatorBooked';
  } else if (type === CONTRIBUTIONS_CLIENT_TYPE || type === CONTRIBUTIONS_PARTNER_TYPE) {
    dataKey = 'contribution';
    dataKeyAccumulator = 'accumulatorContribution';
  }

  return (
    <>
      <StyleTitle>{name}</StyleTitle>

      <BarChart width={525} height={350} data={data}>
        <XAxis dataKey="name" stroke="#8884d8" />
        <YAxis
          domain={[
            0,
            () =>
              BigInt(data[11][dataKey]).greater(data[11][dataKeyAccumulator])
                ? data[11][dataKey]
                : data[11][dataKeyAccumulator],
          ]}
          allowDecimals={false}
          tickFormatter={(value) =>
            currency ? `${currency.symbol} ${numberFormatter(value, 1)}` : value
          }
        />
        <Tooltip content={<KPIChartToolTip currency={currency} />} />

        <Bar type="monotone" dataKey={dataKey} fill="#3EB7F9" barSize={30} />
        <Bar type="monotone" dataKey={dataKeyAccumulator} fill="#70D34C" barSize={30} />
      </BarChart>
    </>
  );
};

KPICharts.propTypes = {
  selectedKPI: PropTypes.object.isRequired,
  currency: PropTypes.object.isRequired,
};

export { KPICharts };

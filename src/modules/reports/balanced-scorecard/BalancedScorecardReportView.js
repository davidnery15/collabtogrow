import React from 'react';
import { Row, Column, Loader, Card, SelectField, Grid, Heading } from '@8base/boost';
import { getCurrencyOnSession } from '../../../shared/alliance-utils';
import View from '@cobuildlab/react-flux-state';
import { Scorecard } from './components';
import { onErrorMixin } from '../../../shared/mixins';
import balancedScorecardStore, {
  BALANCED_SCORECARD_DATA_EVENT,
  BALANCED_SCORECARD_ERROR_EVENT,
} from './balanced-scorecard-store';
import { fetchBalancedScorecardData } from './balanced-scorecard-actions';
import { getBalancedScorecardData } from './balanced-scorecard-utils';
import moment from 'moment';
import * as R from 'ramda';

class BalancedScorecardReportView extends View {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      financialKPIs: [],
      selectedFinancialKPI: null,
      operationalKPIs: [],
      selectedOperationalKPI: null,
      relationshipKPIs: [],
      selectedRelationshipKPI: null,
      strategyKPIs: [],
      selectedStrategyKPI: null,
      selectedKPIYear: moment().year(),
      kpiYears: [],
    };

    this.onError = onErrorMixin.bind(this);
  }

  componentDidMount() {
    this.subscribe(balancedScorecardStore, BALANCED_SCORECARD_ERROR_EVENT, this.onError);

    this.subscribe(balancedScorecardStore, BALANCED_SCORECARD_DATA_EVENT, async (data) => {
      const year = R.clone(this.state.selectedKPIYear);

      const { kpis, initiatives, ideas, contributions, deals, alliance } = data;

      const {
        strategyKPIs,
        relationshipKPIs,
        operationalKPIs,
        financialKPIs,
        kpiYears,
      } = await getBalancedScorecardData(
        kpis,
        initiatives,
        ideas,
        contributions,
        deals,
        alliance,
        year,
      );

      const selectedStrategyKPI = strategyKPIs.length ? strategyKPIs[0] : null;
      const selectedRelationshipKPI = relationshipKPIs.length ? relationshipKPIs[0] : null;
      const selectedOperationalKPI = operationalKPIs.length ? operationalKPIs[0] : null;
      const selectedFinancialKPI = financialKPIs.length ? financialKPIs[0] : null;

      this.setState({
        strategyKPIs,
        selectedStrategyKPI,
        relationshipKPIs,
        selectedRelationshipKPI,
        selectedOperationalKPI,
        operationalKPIs,
        financialKPIs,
        selectedFinancialKPI,
        kpiYears,
        loading: false,
      });
    });

    fetchBalancedScorecardData(R.clone(this.state.selectedKPIYear));
  }

  onSelectedYearChange = (year) => {
    this.setState({ selectedKPIYear: year, loading: true }, () => {
      fetchBalancedScorecardData(R.clone(this.state.selectedKPIYear));
    });
  };

  onSelectedFinancialKPI = (selectedFinancialKPI) => {
    this.setState({ selectedFinancialKPI });
  };

  onSelectedOperationalKPI = (selectedOperationalKPI) => {
    this.setState({ selectedOperationalKPI });
  };

  onSelectedRelationshipKPI = (selectedRelationshipKPI) => {
    this.setState({ selectedRelationshipKPI });
  };

  onSelectedStrategyKPI = (selectedStrategyKPI) => {
    this.setState({ selectedStrategyKPI });
  };

  onDetailClick = (routeTab) => {
    this.props.history.push(`/reports/balanced-scorecard/${routeTab}`);
  };

  render() {
    const {
      selectedFinancialKPI,
      selectedOperationalKPI,
      selectedRelationshipKPI,
      selectedStrategyKPI,
      relationshipKPIs,
      operationalKPIs,
      financialKPIs,
      strategyKPIs,
      loading,
      kpiYears,
      selectedKPIYear,
    } = this.state;
    const currency = getCurrencyOnSession();

    if (loading)
      return (
        <Row growChildren gap="lg" offsetY="lg" offsetX="lg">
          <Loader stretch />
        </Row>
      );

    return (
      <>
        <Card>
          <Card.Header>
            <Grid.Layout
              columns="300px auto 350px"
              areas={[['left', 'center', 'right']]}
              style={{ width: '100%' }}>
              <Grid.Box area="left" justifyContent="center">
                <Heading type="h4" text="Balanced Scorecard" />
              </Grid.Box>
              <Grid.Box area="center" />
              <Grid.Box area="right" direction="row" justifySelf="end" alignSelf="center">
                {kpiYears.length ? (
                  <SelectField
                    style={{ width: '150px', alignSelf: 'end', marginRight: '15px' }}
                    input={{
                      name: 'yearFilter',
                      value: selectedKPIYear,
                      onChange: (year) => this.onSelectedYearChange(year),
                    }}
                    placeholder={'Select a Year'}
                    options={kpiYears.map((year) => {
                      return { label: year, value: year };
                    })}
                  />
                ) : null}
              </Grid.Box>
            </Grid.Layout>
          </Card.Header>
        </Card>

        <Row growChildren gap="lg" offsetY="lg">
          <Column style={{ width: '50%' }} alignItems="stretch">
            <Scorecard
              currency={currency}
              header="Financial"
              kpis={financialKPIs}
              selectedKPI={selectedFinancialKPI}
              onSectionClick={this.onSelectedFinancialKPI}
              onDetailClick={() => this.onDetailClick('financial')}
            />
          </Column>
          <Column style={{ width: '50%' }} alignItems="stretch">
            <Scorecard
              currency={currency}
              header="Operational"
              kpis={operationalKPIs}
              selectedKPI={selectedOperationalKPI}
              onSectionClick={this.onSelectedOperationalKPI}
              onDetailClick={() => this.onDetailClick('operational')}
            />
          </Column>
        </Row>
        <Row growChildren gap="lg" offsetBottom="lg">
          <Column style={{ width: '50%' }} alignItems="stretch">
            <Scorecard
              currency={currency}
              header="Relationship"
              kpis={relationshipKPIs}
              selectedKPI={selectedRelationshipKPI}
              onSectionClick={this.onSelectedRelationshipKPI}
              onDetailClick={() => this.onDetailClick('relationship')}
            />
          </Column>
          <Column style={{ width: '50%' }} alignItems="stretch">
            <Scorecard
              header="Strategy"
              kpis={strategyKPIs}
              selectedKPI={selectedStrategyKPI}
              onSectionClick={this.onSelectedStrategyKPI}
              onDetailClick={() => this.onDetailClick('strategy')}
            />
          </Column>
        </Row>
      </>
    );
  }
}

export { BalancedScorecardReportView };

import Flux from '@cobuildlab/flux-state';

/**
 * Event that triggers data for Balanced Scorecards
 */
export const BALANCED_SCORECARD_DATA_EVENT = 'onBalancedScorecardData';

export const BALANCED_SCORECARD_ERROR_EVENT = 'onBalancedScorecardError';

/**
 * Hold the Active Sales Pipeline data
 */
class BalancedScorecardStore extends Flux.DashStore {
  constructor() {
    super();
    this.addEvent(BALANCED_SCORECARD_DATA_EVENT);
    this.addEvent(BALANCED_SCORECARD_ERROR_EVENT);
  }
}

const balancedScorecardStore = new BalancedScorecardStore();
export default balancedScorecardStore;

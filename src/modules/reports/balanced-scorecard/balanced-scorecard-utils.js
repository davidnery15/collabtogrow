import { INITIATIVE_APPROVAL_APPROVED, IDEA_APPROVAL_APPROVED } from '../../../shared/status';
import { TRAINING_UNIT_TYPE } from '../../../shared/item-types';
import {
  PERIODICITY_TYPE,
  JOINT_INITIATIVES_TYPE,
  JOINT_INNOVATION_IDEAS_TYPE,
  JOINT_REFERENCEABLE_CLIENTS_TYPE,
  JOINT_TRAINING_INVESTMENT_TYPE,
  JOINT_PROPOSALS_WON_TYPE,
  JOINT_PROPOSALS_ISSUED_TYPE,
  PIPELINE_CLIENT_TYPE,
  BOOKINGS_CLIENT_TYPE,
  CONTRIBUTIONS_CLIENT_TYPE,
  PIPELINE_PARTNER_TYPE,
  BOOKINGS_PARTNER_TYPE,
  CONTRIBUTIONS_PARTNER_TYPE,
  KPI_WITH_CURRENCY_TYPES,
  KPI_WITHOUT_CURRENCY_TYPES,
} from '../../settings/alliance-management/allianceKPIs/allianceKPIs-model';
import {
  DEAL_STAGE_CLOSED_WON,
  PROPOSAL_ISSUED_STAGES,
  DEAL_ACTIVE_STAGES,
} from '../../management/deal/deal-model';
import { calculateValueBigInt } from '../../../shared/utils';
import { GREEN_COLOR, RED_COLOR, YELLOW_COLOR } from '../../../shared/colors';
import * as R from 'ramda';
import numbro from 'numbro';
import moment from 'moment';
import BigInt from 'big-integer';
import { getKPIYears } from '../../settings/alliance-management/allianceKPIs/allianceKPIs-actions';
import { allianceKPIsYearModel } from '../../settings/alliance-management/allianceKPIs/allianceKPIs-model';

/**
 * To get the month for the selected kpi year for fullYear & proRata.
 *
 * @param {number} year - The kpi selected year.
 * @returns {number} The actual month.
 */
export const getActualKPIMonth = (year) => {
  const actualYear = moment().year();
  if (year < actualYear) return 12;
  if (year > actualYear) return 1;

  return moment().month() + 1;
};

/**
 * Get kpi color from progress.
 *
 * @param  {number} progress - 0 to 100.
 * @returns {string} The color.
 */
export const getKPIColor = (progress) => {
  if (progress < 70) return RED_COLOR;
  if (progress >= 90) return GREEN_COLOR;

  return YELLOW_COLOR;
};

/**
 * Get kpi theme color from progress.
 *
 * @param  {number} progress - 0 to 100.
 * @returns {string} The theme color name.
 */
export const getKPIColorThemeName = (progress) => {
  if (progress < 70) return 'RED';
  if (progress >= 90) return 'GREEN';

  return 'YELLOW';
};

/**
 * Get kpi from KPILIst.
 *
 * @param {KPIs} kpis
 * @param kpiType
 * @param kpiYear
 * @param {string} type
 * @returns {KPI}
 */
export const getKPIFromList = (kpis, kpiType, kpiYear) => {
  const kpi = kpis.find(({ type, year }) => type === kpiType && year === kpiYear);

  return kpi;
};

/**
 * Get KPI monthByMonth cumulative target.
 *
 * @param {object} kpi - The kpi to get the data.
 * @returns {Array} The cumulativeMonthByMonthTarget.
 */
export const getCumulativeMonthByMonthTarget = (kpi) => {
  const { type, monthByMonth, targetDistributionType, target } = kpi;

  const getPeriodicityMonthByMonthTarget = (kpi) => {
    const { type, target } = kpi;
    // Dont use BigInt for without currency kpis
    if (KPI_WITHOUT_CURRENCY_TYPES.includes(type)) {
      const monthValue = target / 12;
      const monthByMonthTarget = Array.from({ length: 12 }, (e) =>
        monthValue.toFixed(2).toString(),
      );

      return monthByMonthTarget;
    }

    const monthValue = BigInt(target)
      .divide(12)
      .toString();
    const monthByMonthTarget = Array.from({ length: 12 }, (e) => monthValue);

    return monthByMonthTarget;
  };

  const monthByMonthCumulativeTarget =
    targetDistributionType === PERIODICITY_TYPE
      ? getPeriodicityMonthByMonthTarget(kpi)
      : R.clone(monthByMonth);

  monthByMonthCumulativeTarget.forEach((value, i) => {
    // to avoid big int divide lost
    if (i === 11) {
      monthByMonthCumulativeTarget[i] = target.toString();
    }

    // Cumulative ints
    if (i !== 11 && KPI_WITHOUT_CURRENCY_TYPES.includes(type)) {
      const currentMonthTarget = monthByMonthCumulativeTarget[i];
      const pastMonthCumulativeTarget = monthByMonthCumulativeTarget[i - 1] || 0;
      const currentMonthCumulativeTarget =
        Number(currentMonthTarget) + Number(pastMonthCumulativeTarget);
      monthByMonthCumulativeTarget[i] = currentMonthCumulativeTarget.toString();
    }
    // cumulative big ints
    if (i !== 11 && KPI_WITH_CURRENCY_TYPES.includes(type)) {
      const currentMonthTarget = monthByMonthCumulativeTarget[i];
      const pastMonthCumulativeTarget = monthByMonthCumulativeTarget[i - 1] || '0';
      monthByMonthCumulativeTarget[i] = BigInt(currentMonthTarget)
        .add(pastMonthCumulativeTarget)
        .toString();
    }
  });

  return R.clone(monthByMonthCumulativeTarget);
};

/**
 * Get KPI FullYear And ProRata.
 *
 * @param {KPI} kpi - The kpi to get the data.
 * @param month - The month from getActualKPIMonth func.
 * @returns {fullYear, proRata}
 */
export const getKPIFullYearAndProRata = (kpi, month) => {
  if (!kpi) return { fullYear: '0', proRata: '0' };

  const getPeriodicityProRata = (kpi) => {
    const { type, target } = kpi;
    // dont divide if its the last month, just return the fullTarget
    if (month === 12) return BigInt(target).toString();

    // Dont use BigInt for without currency kpis
    if (KPI_WITHOUT_CURRENCY_TYPES.includes(type)) {
      const floatProRata = (target / 12) * month;
      return floatProRata.toFixed(2).toString();
    }

    return BigInt(target)
      .divide(12)
      .multiply(month)
      .toString();
  };

  const fullYear = BigInt(kpi.target).toString();

  const proRata =
    kpi.targetDistributionType === PERIODICITY_TYPE
      ? getPeriodicityProRata(kpi, month)
      : kpi.monthByMonth.slice(0, month).reduce(
        (a, b) =>
          BigInt(a)
            .add(b)
            .toString(),
        '0',
      );

  return { fullYear, proRata };
};

/**
 * Calc total monetized value of items.
 *
 * @param  {Items} items
 * @param  {string} itemName - Item model name.
 * @returns {number} TotalMonetizedValue.
 */
export const getItemsMonetizedValue = (items, itemName) => {
  const totalMonetizedValue = items.reduce((total, item) => {
    const { unitQuantity, unitMonetizationFactor } = item[itemName];
    const calculatedValue = calculateValueBigInt(unitQuantity, unitMonetizationFactor);

    return BigInt(total)
      .add(calculatedValue)
      .toString();
  }, '0');

  return totalMonetizedValue;
};

/**
 * Get monthByMonthCurrent total items From groupByRamdaObject.
 *
 * @param groupByRamdaObject
 * @param {object} groupByObject - [description].
 * @returns {Array} Array of months.
 */
const getMonthByMonthCurrentFromGroupBy = (groupByRamdaObject) => {
  // fill with 12 empty arrays
  const monthByMonthCurrent = Array.from({ length: 12 }, (e) => '0');

  monthByMonthCurrent.forEach((item, i) => {
    const monthItems = groupByRamdaObject[`${i}`] || [];
    const monthItemsLength = monthItems.length;
    const pastMonthItemsLength = monthByMonthCurrent[i - 1] || '0';
    monthByMonthCurrent[i] = BigInt(monthItemsLength)
      .add(pastMonthItemsLength)
      .toString();
  });

  return monthByMonthCurrent;
};

/**
 * Get monthByMonthCurrent monetizedValue From groupByRamdaObject.
 *
 * @param groupByRamdaObject
 * @param {object} groupByObject - [description].
 * @returns {Array} Array of months.
 */
const getMonthByMonthCurrentMonetizedFromGroupBy = (groupByRamdaObject) => {
  // fill with 12 empty arrays
  const monthByMonthCurrent = Array.from({ length: 12 }, (e) => '0');
  const monthByMonthForecastActuals = Array.from({ length: 12 }, (e) => '0');
  monthByMonthCurrent.forEach((item, i) => {
    const monthItems = groupByRamdaObject[`${i}`] || [];
    const monthItemsMonetized = getItemsMonetizedValue(monthItems, 'contribution');
    const pastMonthMonetized = monthByMonthCurrent[i - 1] || '0';
    monthByMonthForecastActuals[i] = R.clone(monthItemsMonetized);
    monthByMonthCurrent[i] = BigInt(monthItemsMonetized)
      .add(pastMonthMonetized)
      .toString();
  });

  return { monthByMonthCurrent, monthByMonthForecastActuals };
};

/**
 * Get monthByMonthCurrent total amount From groupByRamdaObject.
 *
 * @param groupByRamdaObject
 * @param {object} groupByObject - [description].
 * @returns {Array} Array of months.
 */
const getMonthByMonthCurrentAmountFromGroupBy = (groupByRamdaObject) => {
  // fill with 12 empty arrays
  const monthByMonthCurrent = Array.from({ length: 12 }, (e) => '0');
  const monthByMonthForecastActuals = Array.from({ length: 12 }, (e) => '0');
  monthByMonthCurrent.forEach((item, i) => {
    const monthItems = groupByRamdaObject[`${i}`] || [];
    const monthItemsAmount = monthItems.reduce(
      (total, { amount }) =>
        BigInt(total)
          .add(amount)
          .toString(),
      '0',
    );
    const pastMonthAmount = monthByMonthCurrent[i - 1] || '0';
    monthByMonthForecastActuals[i] = R.clone(monthItemsAmount);
    monthByMonthCurrent[i] = BigInt(monthItemsAmount)
      .add(pastMonthAmount)
      .toString();
  });

  return { monthByMonthCurrent, monthByMonthForecastActuals };
};

/**
 * Count the total/approved initiatives & ideas for the Strategy scorecard.
 *
 * @param {KPIs} kpis
 * @param {Initiatives} initiatives
 * @param {Ideas} ideas
 * @param year
 * @returns {Scorecard}
 */
export const getStrategyScorecardData = async (kpis, initiatives, ideas, year) => {
  // With both approvals approved
  const approvedInitiatives = initiatives.filter(({ initiativeApprovalInitiativeRelation }) => {
    if (!initiativeApprovalInitiativeRelation.items.length) return false;
    for (const { status } of initiativeApprovalInitiativeRelation.items) {
      if (status !== INITIATIVE_APPROVAL_APPROVED) return false;
    }

    return true;
  });
  // With both approvals approved
  const approvedIdeas = ideas.filter(({ idea: { ideaApprovalRelation } }) => {
    if (!ideaApprovalRelation.items.length) return false;
    for (const { status } of ideaApprovalRelation.items) {
      if (status !== IDEA_APPROVAL_APPROVED) return false;
    }

    return true;
  });

  const approvedInitiativesCount = approvedInitiatives.length;
  const approvedIdeasCount = approvedIdeas.length;

  // set approvedInitiatives approvedDate
  const approvedInitiativesWithApprovedDate = approvedInitiatives.map((initiative) => {
    const { initiativeApprovalInitiativeRelation } = initiative;
    const { dateOfResponse } = initiativeApprovalInitiativeRelation.items.reduce(
      (a, b) =>
        // get the greater dateOfResponse
        moment(a.dateOfResponse).isSameOrAfter(moment(b.dateOfResponse)) ? a : b,
      { dateOfResponse: null },
    );

    initiative.approvedDate = dateOfResponse;
    return initiative;
  });

  // set approvedIdeas approvedDate
  const approvedIdeasWithApprovedDate = approvedIdeas.map((item) => {
    const { idea } = item;
    const { ideaApprovalRelation } = idea;
    const { dateOfResponse } = ideaApprovalRelation.items.reduce(
      (a, b) =>
        // get the greater dateOfResponse
        moment(a.dateOfResponse).isSameOrAfter(moment(b.dateOfResponse)) ? a : b,
      { dateOfResponse: null },
    );

    item.approvedDate = dateOfResponse;
    return item;
  });

  // groupBy approvedDate
  const byApprovedDate = R.groupBy(({ approvedDate }) => {
    if (!moment(approvedDate).isValid()) return null;
    const approvedMonth = moment(approvedDate).month();
    return approvedMonth;
  });
  // groupBy
  const approvedInitiativesByApprovedDate = byApprovedDate(approvedInitiativesWithApprovedDate);
  const approvedIdeasByApprovedDate = byApprovedDate(approvedIdeasWithApprovedDate);
  // monthByMonthCurrents
  const monthByMonthApprovedInitiatives = getMonthByMonthCurrentFromGroupBy(
    approvedInitiativesByApprovedDate,
  );
  const monthByMonthApprovedIdeas = getMonthByMonthCurrentFromGroupBy(approvedIdeasByApprovedDate);

  const month = getActualKPIMonth(year);
  const joinInitiativesKPI = getKPIFromList(kpis, JOINT_INITIATIVES_TYPE, year);
  const joinIdeasKPI = getKPIFromList(kpis, JOINT_INNOVATION_IDEAS_TYPE, year);

  const { fullYear: fullYearInitiatives, proRata: proRataInitiatives } = getKPIFullYearAndProRata(
    joinInitiativesKPI,
    month,
  );
  const { fullYear: fullYearIdeas, proRata: proRataIdeas } = getKPIFullYearAndProRata(
    joinIdeasKPI,
    month,
  );

  const initiativesKPIData = {
    type: JOINT_INITIATIVES_TYPE,
    name: JOINT_INITIATIVES_TYPE,
    current: BigInt(approvedInitiativesCount).toString(),
    monthByMonthCurrent: monthByMonthApprovedInitiatives,
    monthByMonthCumulativeTarget: getCumulativeMonthByMonthTarget(joinInitiativesKPI),
    fullYearTarget: fullYearInitiatives,
    proRataTarget: proRataInitiatives,
    initiatives: approvedInitiatives,
  };

  const ideasKPIData = {
    type: JOINT_INNOVATION_IDEAS_TYPE,
    name: JOINT_INNOVATION_IDEAS_TYPE,
    current: BigInt(approvedIdeasCount).toString(),
    monthByMonthCurrent: monthByMonthApprovedIdeas,
    monthByMonthCumulativeTarget: getCumulativeMonthByMonthTarget(joinIdeasKPI),
    fullYearTarget: fullYearIdeas,
    proRataTarget: proRataIdeas,
    ideas: approvedIdeas,
  };

  const dataKPIs = [initiativesKPIData, ideasKPIData].filter(
    ({ fullYearTarget }) => fullYearTarget > 0,
  );

  return dataKPIs;
};

/**
 * Count the total referenceableDeals
 Get the total unit monetized value of unitType === TRAINING contributions.
 *
 * @param {KPIs} kpis
 * @param {Deals} deals
 * @param contributions
 * @param year
 * @returns {Scorecard}
 */
export const getRelationshipScorecardData = async (kpis, deals, contributions, year) => {
  const referenceableDeals = deals.filter(({ dealReferencable }) => dealReferencable === 'Y');
  const referenceableDealsCount = referenceableDeals.length;
  const trainingContributions = contributions.filter(
    ({ contribution: { unitType } }) => unitType === TRAINING_UNIT_TYPE,
  );

  const jointTrainingInvestment = getItemsMonetizedValue(trainingContributions, 'contribution');
  // groupBy closeDate for deals
  const byCloseDate = R.groupBy(({ closeDate }) => {
    const closeMonth = moment(closeDate).month();
    return closeMonth;
  });
  // groupBy contributionDate for contributions
  const byContributionDate = R.groupBy(({ contribution: { contributionDate } }) => {
    const contributionMonth = moment(contributionDate || undefined).month();
    return contributionMonth;
  });

  // groupBy
  const referenceableDealsByCloseDate = byCloseDate(referenceableDeals);
  const trainingContributionsByContributionDate = byContributionDate(trainingContributions);
  // monthByMonthCurrents
  const monthByMonthReferenceableDeals = getMonthByMonthCurrentFromGroupBy(
    referenceableDealsByCloseDate,
  );
  const {
    monthByMonthCurrent: monthByMonthJointTrainingInvestment,
  } = getMonthByMonthCurrentMonetizedFromGroupBy(trainingContributionsByContributionDate);

  const month = getActualKPIMonth(year);
  const referenceableDealsKPI = getKPIFromList(kpis, JOINT_REFERENCEABLE_CLIENTS_TYPE, year);
  const trainingContributionsKPI = getKPIFromList(kpis, JOINT_TRAINING_INVESTMENT_TYPE, year);

  const {
    fullYear: fullYearReferenceableDeals,
    proRata: proRataReferenceableDeals,
  } = getKPIFullYearAndProRata(referenceableDealsKPI, month);
  const {
    fullYear: fullYearTrainingContributions,
    proRata: proRataTrainingContributions,
  } = getKPIFullYearAndProRata(trainingContributionsKPI, month);

  const referenceableDealsKPIData = {
    type: JOINT_REFERENCEABLE_CLIENTS_TYPE,
    name: JOINT_REFERENCEABLE_CLIENTS_TYPE,
    current: BigInt(referenceableDealsCount).toString(),
    monthByMonthCurrent: monthByMonthReferenceableDeals,
    monthByMonthCumulativeTarget: getCumulativeMonthByMonthTarget(referenceableDealsKPI),
    fullYearTarget: fullYearReferenceableDeals,
    proRataTarget: proRataReferenceableDeals,
    deals: referenceableDeals,
  };

  const trainingContributionsKPIData = {
    type: JOINT_TRAINING_INVESTMENT_TYPE,
    name: JOINT_TRAINING_INVESTMENT_TYPE,
    current: jointTrainingInvestment,
    monthByMonthCurrent: monthByMonthJointTrainingInvestment,
    monthByMonthCumulativeTarget: getCumulativeMonthByMonthTarget(trainingContributionsKPI),
    fullYearTarget: fullYearTrainingContributions,
    proRataTarget: proRataTrainingContributions,
    contributions: trainingContributions,
  };

  const dataKPIs = [referenceableDealsKPIData, trainingContributionsKPIData].filter(
    ({ fullYearTarget }) => fullYearTarget > 0,
  );

  return dataKPIs;
};

/**
 * Count the total closedWon & proposalIssued deals
 Get the total amount of client & partner deals (Pipeline).
 *
 * @param {KPIs} kpis
 * @param {Deals} deals
 * @param {Alliance} alliance
 * @param year
 * @returns {Scorecard}
 */
export const getOperationalScorecardData = async (kpis, deals, alliance, year) => {
  const { clientCompany, partnerCompany } = alliance;
  const clientCompanyId = clientCompany ? clientCompany.id : null;
  const partnerCompanyId = partnerCompany ? partnerCompany.id : null;

  const activeDeals = deals.filter(({ stage }) => DEAL_ACTIVE_STAGES.includes(stage));
  const clientPipelineDeals = activeDeals.filter(
    ({ company }) => company && company.id === clientCompanyId,
  );
  const clientPipeline = clientPipelineDeals.reduce(
    (total, { amount }) =>
      BigInt(total)
        .add(amount)
        .toString(),
    '0',
  );
  const partnerPipelineDeals = activeDeals.filter(
    ({ company }) => company && company.id === partnerCompanyId,
  );
  const partnerPipeline = partnerPipelineDeals.reduce(
    (total, { amount }) =>
      BigInt(total)
        .add(amount)
        .toString(),
    '0',
  );
  const closedWonDeals = deals.filter(({ stage }) => stage === DEAL_STAGE_CLOSED_WON);
  const proposalDeals = deals.filter(({ stage }) => PROPOSAL_ISSUED_STAGES.includes(stage));

  const closedWonDealsCount = closedWonDeals.length;
  const proposalDealsCount = proposalDeals.length;

  // groupBy closeDate for deals
  const byCloseDate = R.groupBy(({ closeDate }) => {
    const closeMonth = moment(closeDate).month();
    return closeMonth;
  });
  // groupBy
  const clientPipelineDealsByCloseDate = byCloseDate(clientPipelineDeals);
  const partnerPipelineDealsByCloseDate = byCloseDate(partnerPipelineDeals);
  const closedWonDealsByCloseDate = byCloseDate(closedWonDeals);
  const proposalDealsByCloseDate = byCloseDate(proposalDeals);
  // monthByMonthCurrents
  const {
    monthByMonthCurrent: monthByMonthClientPipelineDeals,
  } = getMonthByMonthCurrentAmountFromGroupBy(clientPipelineDealsByCloseDate);
  const {
    monthByMonthCurrent: monthByMonthPartnerPipelineDeals,
  } = getMonthByMonthCurrentAmountFromGroupBy(partnerPipelineDealsByCloseDate);
  const monthByMonthClosedWonDeals = getMonthByMonthCurrentFromGroupBy(closedWonDealsByCloseDate);
  const monthByMonthProposalDeals = getMonthByMonthCurrentFromGroupBy(proposalDealsByCloseDate);

  const month = getActualKPIMonth(year);
  const closedWonDealsKPI = getKPIFromList(kpis, JOINT_PROPOSALS_WON_TYPE, year);
  const proposalDealsKPI = getKPIFromList(kpis, JOINT_PROPOSALS_ISSUED_TYPE, year);
  const pipelineClientKPI = getKPIFromList(kpis, PIPELINE_CLIENT_TYPE, year);
  const pipelinePartnerKPI = getKPIFromList(kpis, PIPELINE_PARTNER_TYPE, year);

  const {
    fullYear: fullYearClosedWonDeals,
    proRata: proRataClosedWonDeals,
  } = getKPIFullYearAndProRata(closedWonDealsKPI, month);
  const {
    fullYear: fullYearProposalDeals,
    proRata: proRataProposalDeals,
  } = getKPIFullYearAndProRata(proposalDealsKPI, month);
  const {
    fullYear: fullYearClientPipeline,
    proRata: proRataClientPipeline,
  } = getKPIFullYearAndProRata(pipelineClientKPI, month);
  const {
    fullYear: fullYearPartnerPipeline,
    proRata: proRataPartnerPipeline,
  } = getKPIFullYearAndProRata(pipelinePartnerKPI, month);

  const clientPipelineName = getKPIName(alliance, PIPELINE_CLIENT_TYPE);
  const clientPipelineKPIData = {
    type: PIPELINE_CLIENT_TYPE,
    name: clientPipelineName,
    current: clientPipeline,
    monthByMonthCurrent: monthByMonthClientPipelineDeals,
    monthByMonthCumulativeTarget: getCumulativeMonthByMonthTarget(pipelineClientKPI),
    fullYearTarget: fullYearClientPipeline,
    proRataTarget: proRataClientPipeline,
    deals: clientPipelineDeals,
  };

  const partnerPipelineName = getKPIName(alliance, PIPELINE_PARTNER_TYPE);
  const partnerPipelineKPIData = {
    type: PIPELINE_PARTNER_TYPE,
    name: partnerPipelineName,
    current: partnerPipeline,
    monthByMonthCurrent: monthByMonthPartnerPipelineDeals,
    monthByMonthCumulativeTarget: getCumulativeMonthByMonthTarget(pipelinePartnerKPI),
    fullYearTarget: fullYearPartnerPipeline,
    proRataTarget: proRataPartnerPipeline,
    deals: partnerPipelineDeals,
  };

  const closedWonDealsKPIData = {
    type: JOINT_PROPOSALS_WON_TYPE,
    name: JOINT_PROPOSALS_WON_TYPE,
    current: BigInt(closedWonDealsCount).toString(),
    monthByMonthCurrent: monthByMonthClosedWonDeals,
    monthByMonthCumulativeTarget: getCumulativeMonthByMonthTarget(closedWonDealsKPI),
    fullYearTarget: fullYearClosedWonDeals,
    proRataTarget: proRataClosedWonDeals,
    deals: closedWonDeals,
  };

  const proposalDealsKPIData = {
    type: JOINT_PROPOSALS_ISSUED_TYPE,
    name: JOINT_PROPOSALS_ISSUED_TYPE,
    current: BigInt(proposalDealsCount).toString(),
    monthByMonthCurrent: monthByMonthProposalDeals,
    monthByMonthCumulativeTarget: getCumulativeMonthByMonthTarget(proposalDealsKPI),
    fullYearTarget: fullYearProposalDeals,
    proRataTarget: proRataProposalDeals,
    deals: proposalDeals,
  };

  const dataKPIs = [
    clientPipelineKPIData,
    partnerPipelineKPIData,
    closedWonDealsKPIData,
    proposalDealsKPIData,
  ].filter(({ fullYearTarget }) => fullYearTarget > 0);

  return dataKPIs;
};

/**
 * Get the client/partner (BOOKED REVENUE) & Contributions monetizedValue.
 *
 * @param {KPIs} kpis
 * @param {Deals} deals
 * @param {Contributions} contributions
 * @param {Alliance} alliance
 * @param clientMembers
 * @param partnerMembers
 * @param year
 * @returns {Scorecard}
 */
export const getFinancialScorecardData = async (kpis, deals, contributions, alliance, year) => {
  const { clientCompany, partnerCompany } = alliance;
  const clientCompanyId = clientCompany ? clientCompany.id : null;
  const partnerCompanyId = partnerCompany ? partnerCompany.id : null;

  const closedWonDeals = deals.filter(({ stage }) => stage === DEAL_STAGE_CLOSED_WON);
  const clientRevenueDeals = closedWonDeals.filter(
    ({ company }) => company && company.id === clientCompanyId,
  );
  const clientRevenue = clientRevenueDeals.reduce(
    (total, { amount }) =>
      BigInt(total)
        .add(amount)
        .toString(),
    '0',
  );
  const partnerRevenueDeals = closedWonDeals.filter(
    ({ company }) => company && company.id === partnerCompanyId,
  );
  const partnerRevenue = partnerRevenueDeals.reduce(
    (total, { amount }) =>
      BigInt(total)
        .add(amount)
        .toString(),
    '0',
  );
  const clientContributions = contributions.filter(
    ({ contribution: { source } }) => source && source.id === clientCompanyId,
  );
  const partnerContributions = contributions.filter(
    ({ contribution: { source } }) => source && source.id === partnerCompanyId,
  );
  const clientContributionsValue = getItemsMonetizedValue(clientContributions, 'contribution');
  const partnerContributionsValue = getItemsMonetizedValue(partnerContributions, 'contribution');

  // groupBy closeDate for deals
  const byCloseDate = R.groupBy(({ closeDate }) => {
    const closeMonth = moment(closeDate).month();
    return closeMonth;
  });
  // groupBy contributionDate for contributions
  const byContributionDate = R.groupBy(({ contribution: { contributionDate } }) => {
    const contributionMonth = moment(contributionDate || undefined).month();
    return contributionMonth;
  });
  // groupBy
  const clientRevenueDealsByCloseDate = byCloseDate(clientRevenueDeals);
  const partnerRevenueDealsByCloseDate = byCloseDate(partnerRevenueDeals);
  const clientContributionsByContributionDate = byContributionDate(clientContributions);
  const partnerContributionsByContributionDate = byContributionDate(partnerContributions);
  // monthByMonthCurrents
  const {
    monthByMonthCurrent: monthByMonthClientRevenueDeals,
    monthByMonthForecastActuals: monthByMonthForecastClientRevenueDeals,
  } = getMonthByMonthCurrentAmountFromGroupBy(clientRevenueDealsByCloseDate);
  const {
    monthByMonthCurrent: monthByMonthPartnerRevenueDeals,
    monthByMonthForecastActuals: monthByMonthForecastPartnerRevenueDeals,
  } = getMonthByMonthCurrentAmountFromGroupBy(partnerRevenueDealsByCloseDate);
  const {
    monthByMonthCurrent: monthByMonthClientContributions,
    monthByMonthForecastActuals: monthByMonthForecastClientContributions,
  } = getMonthByMonthCurrentMonetizedFromGroupBy(clientContributionsByContributionDate);
  const {
    monthByMonthCurrent: monthByMonthPartnerContributions,
    monthByMonthForecastActuals: monthByMonthForecastPartnerContributions,
  } = getMonthByMonthCurrentMonetizedFromGroupBy(partnerContributionsByContributionDate);

  const month = getActualKPIMonth(year);
  const revenueClientKPI = getKPIFromList(kpis, BOOKINGS_CLIENT_TYPE, year);
  const revenuePartnerKPI = getKPIFromList(kpis, BOOKINGS_PARTNER_TYPE, year);
  const contributionsClientKPI = getKPIFromList(kpis, CONTRIBUTIONS_CLIENT_TYPE, year);
  const contributionsPartnerKPI = getKPIFromList(kpis, CONTRIBUTIONS_PARTNER_TYPE, year);

  const {
    fullYear: fullYearClientRevenue,
    proRata: proRataClientRevenue,
  } = getKPIFullYearAndProRata(revenueClientKPI, month);
  const {
    fullYear: fullYearPartnerRevenue,
    proRata: proRataPartnerRevenue,
  } = getKPIFullYearAndProRata(revenuePartnerKPI, month);
  const {
    fullYear: fullYearClientContributions,
    proRata: proRataClientContributions,
  } = getKPIFullYearAndProRata(contributionsClientKPI, month);
  const {
    fullYear: fullYearPartnerContributions,
    proRata: proRataPartnerContributions,
  } = getKPIFullYearAndProRata(contributionsPartnerKPI, month);

  const clientRevenueName = getKPIName(alliance, BOOKINGS_CLIENT_TYPE);
  const clientRevenueKPIData = {
    type: BOOKINGS_CLIENT_TYPE,
    name: clientRevenueName,
    current: clientRevenue,
    monthByMonthCurrent: monthByMonthClientRevenueDeals,
    monthByMonthForecastActuals: monthByMonthForecastClientRevenueDeals,
    monthByMonthCumulativeTarget: getCumulativeMonthByMonthTarget(revenueClientKPI),
    fullYearTarget: fullYearClientRevenue,
    proRataTarget: proRataClientRevenue,
    deals: clientRevenueDeals,
  };

  const partnerRevenueName = getKPIName(alliance, BOOKINGS_PARTNER_TYPE);
  const partnerRevenueKPIData = {
    type: BOOKINGS_PARTNER_TYPE,
    name: partnerRevenueName,
    current: partnerRevenue,
    monthByMonthCurrent: monthByMonthPartnerRevenueDeals,
    monthByMonthForecastActuals: monthByMonthForecastPartnerRevenueDeals,
    monthByMonthCumulativeTarget: getCumulativeMonthByMonthTarget(revenuePartnerKPI),
    fullYearTarget: fullYearPartnerRevenue,
    proRataTarget: proRataPartnerRevenue,
    deals: partnerRevenueDeals,
  };

  const clientContributionsName = getKPIName(alliance, CONTRIBUTIONS_CLIENT_TYPE);
  const clientContributionsKPIData = {
    type: CONTRIBUTIONS_CLIENT_TYPE,
    name: clientContributionsName,
    current: clientContributionsValue,
    monthByMonthCurrent: monthByMonthClientContributions,
    monthByMonthForecastActuals: monthByMonthForecastClientContributions,
    monthByMonthCumulativeTarget: getCumulativeMonthByMonthTarget(contributionsClientKPI),
    fullYearTarget: fullYearClientContributions,
    proRataTarget: proRataClientContributions,
    contributions: clientContributions,
  };

  const partnerContributionsName = getKPIName(alliance, CONTRIBUTIONS_PARTNER_TYPE);
  const partnerContributionsKPIData = {
    type: CONTRIBUTIONS_PARTNER_TYPE,
    name: partnerContributionsName,
    current: partnerContributionsValue,
    monthByMonthCurrent: monthByMonthPartnerContributions,
    monthByMonthForecastActuals: monthByMonthForecastPartnerContributions,
    monthByMonthCumulativeTarget: getCumulativeMonthByMonthTarget(contributionsPartnerKPI),
    fullYearTarget: fullYearPartnerContributions,
    proRataTarget: proRataPartnerContributions,
    contributions: partnerContributions,
  };

  // not filtered for forecasting, filter by fullYearTarget > 0
  // for Balanced Scorecard
  const dataKPIs = [
    clientRevenueKPIData,
    partnerRevenueKPIData,
    clientContributionsKPIData,
    partnerContributionsKPIData,
  ];

  return dataKPIs;
};

/**
 * Get Balanced Scorecad Strategy, Relationship, Operational & Financial KPIs.
 *
 * @param {KPIs} kpis
 * @param {Initiatves} initiatives
 * @param {Ideas} ideas
 * @param {Decisions} decisions
 * @param deals
 * @param alliance
 * @param clientMembers
 * @param partnerMembers
 * @param year
 * @param {Contributions} contributions
 * @returns {StrategyKPIs, RelationshipKPIs}
 */
export const getBalancedScorecardData = async (
  kpis,
  initiatives,
  ideas,
  contributions,
  deals,
  alliance,
  year = moment().year(),
) => {
  const [
    strategyKPIs,
    relationshipKPIs,
    operationalKPIs,
    financialKPIsNotFiltered,
  ] = await Promise.all([
    getStrategyScorecardData(kpis, initiatives, ideas, year),
    getRelationshipScorecardData(kpis, deals, contributions, year),
    getOperationalScorecardData(kpis, deals, alliance, year),
    getFinancialScorecardData(kpis, deals, contributions, alliance, year),
  ]);
  // filter financialKPIs
  const financialKPIs = financialKPIsNotFiltered.filter(({ fullYearTarget }) => fullYearTarget > 0);

  const { businessCase } = alliance;
  const { kpiYears } = getKPIYears(alliance, businessCase);
  return { strategyKPIs, relationshipKPIs, operationalKPIs, financialKPIs, kpiYears };
};

/**
 * Get the progress based on current and targetKPI.
 *
 * @param  {number} current
 * @param  {number} kpiTarget
 * @param  {string} kpiType
 * @returns {number} Progress.
 */
export const getKPIProgress = (current, kpiTarget, kpiType) => {
  if (KPI_WITHOUT_CURRENCY_TYPES.includes(kpiType)) {
    return Math.trunc((current * 100) / kpiTarget);
  }

  if (BigInt(kpiTarget).isZero()) return 0;

  const intNumber = parseInt(
    BigInt(current)
      .multiply(100)
      .divide(kpiTarget)
      .toString(),
  );
  const progress = Math.trunc(intNumber);

  return progress;
};

/**
 *  GetKPI Progress Text.
 *
 * @param  {Currency}  currency
 * @param  {string}  type
 * @param  {number}  current
 * @param  {number}  target
 * @param  {boolean} [averageCurrent=true]
 * @param  {boolean} [averageTarget=true]
 * @returns {string}
 */
export const getKPIProgressText = (
  currency,
  type,
  current,
  target,
  averageCurrent = true,
  averageTarget = true,
) => {
  const withCurrencyType = KPI_WITH_CURRENCY_TYPES.includes(type) ? true : false;

  const kpiCurency = withCurrencyType ? currency : null;
  const symbol = kpiCurency ? kpiCurency.symbol : '';
  const kpiCurrent = withCurrencyType
    ? numbro(current.toString()).format({
      thousandSeparated: !averageCurrent,
      average: averageCurrent,
    })
    : current;
  const kpiTarget = withCurrencyType
    ? numbro(target.toString()).format({
      thousandSeparated: !averageTarget,
      average: averageTarget,
    })
    : target;
  const progressText = `${symbol}${kpiCurrent}/${symbol}${kpiTarget}`;

  return progressText;
};

/**
 *  GetKPI Target Text.
 *
 * @param  {Currency}  currency
 * @param  {string}  type
 * @param  {number}  target
 * @returns {string}
 */
export const getKPITargetText = (currency, type, target) => {
  const kpiCurency = KPI_WITH_CURRENCY_TYPES.includes(type) ? currency : null;
  const symbol = kpiCurency ? kpiCurency.symbol : '';
  const kpiTarget = numbro(target.toString()).format({
    thousandSeparated: true,
  });
  const targetText = `${symbol}${kpiTarget}`;

  return targetText;
};

/**
 * Get KPI Name based on company name.
 *
 * @param {Company} company
 * @param alliance
 * @param {string} kpiType
 * @returns {string} KpiName.
 */
export const getKPIName = (alliance, kpiType) => {
  const { clientCompany, partnerCompany } = alliance;

  if (kpiType.includes('- Client')) {
    if (!clientCompany) return kpiType;
    return kpiType.replace('- Client', `- ${clientCompany.name}`);
  }

  if (kpiType.includes('- Partner')) {
    if (!partnerCompany) return kpiType;
    return kpiType.replace('- Partner', `- ${partnerCompany.name}`);
  }

  return kpiType;
};

/**
 * Set default KPIs for a specific year if none is already set (for balanced scorecard & forecast reports).
 *
 * @param {Array} kpis - The kpis list.
 * @param {Array} kpiYear - The kpi selected year.
 */
export const setDefaultKPIs = (kpis, kpiYear) => {
  const kpi = kpis.find(({ year }) => year === kpiYear);

  if (!kpi) kpis.push.apply(kpis, allianceKPIsYearModel(kpiYear));
};

import styled from '@emotion/styled';
import { Card } from '@8base/boost';

export const ReportHeader = styled(Card.Header)`
  display: grid;
  grid-template-columns: 1fr 1fr;
`;

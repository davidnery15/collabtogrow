import styled from '@emotion/styled';

const DateFieldsContainer = styled('div')`
  display: grid;
  grid-auto-flow: column;
  grid-column-gap: 12px;
`;

export { DateFieldsContainer };

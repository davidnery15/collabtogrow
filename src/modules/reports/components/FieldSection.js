import styled from '@emotion/styled';

const FieldSection = styled('div')`
  margin-bottom: 22px;
`;

export { FieldSection };

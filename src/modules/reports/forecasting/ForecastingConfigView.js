import React from 'react';
import * as R from 'ramda';
import { Query } from 'react-apollo';
import { Form, Field } from 'react-final-form';
import arrayMutators from 'final-form-arrays';
import { FieldArray } from 'react-final-form-arrays';
import { Card, Heading, SelectField } from '@8base/boost';
import View from '@cobuildlab/react-flux-state';

import sessionStore, { NEW_SESSION_EVENT } from 'shared/SessionStore';
import { TransparentButtonWithIcon } from 'components/buttons/TransparentButtonWithIcon';
import { ViewCardBody } from 'components/card/ViewCardBody';
import { ActionButton } from 'components/buttons/ActionButton';
import { INITIATIVE_FULL_LIST_QUERY } from '../reports-queries';
import {
  FieldSection,
  FieldSectionHeader,
  FieldSectionText,
  DeleteButton,
  FilterRow,
  FilterFieldsRow,
  ReportsCard,
} from '../components';
import { validateReportsConfig } from '../utils';
import BudgetVsForecastStore, { BUDGET_VS_FORECAST_FILTERS_EVENT } from './forecasting-store';
import { saveBudgetVsForecastFilters } from './forecasting-actions';
import { FIELDS_LIST } from './forecasting-model';

/**
 * These fieldsOptionsByFilters and fieldsDictByFilters are used only for the 'filters' field.
 */
const fieldsOptionsByFilters = FIELDS_LIST.map((field) => ({
  label: field.displayName,
  value: field.id,
}));
const fieldsDictByFilters = FIELDS_LIST.reduce((dict, field) => {
  return {
    ...dict,
    [field.id]: field,
  };
}, {});

class ForecastingConfigView extends View {
  constructor(props) {
    super(props);

    this.state = {
      data: BudgetVsForecastStore.getState(BUDGET_VS_FORECAST_FILTERS_EVENT),
    };

    this.selectedAlliance = sessionStore.getState(NEW_SESSION_EVENT).selectedAlliance;
  }

  componentDidMount() {
    this.subscribe(BudgetVsForecastStore, BUDGET_VS_FORECAST_FILTERS_EVENT, (data) => {
      this.setState({ data });
    });
  }

  onSubmit = (data) => {
    const { history } = this.props;
    saveBudgetVsForecastFilters(data);
    history.push('/reports/forecasting/report');
  };

  render() {
    const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);

    const queryArgs = {
      filter: {
        AND: [
          {
            alliance:
              selectedAlliance && selectedAlliance.id
                ? { id: { equals: selectedAlliance.id } }
                : undefined,
          },
          {
            forecastingYears: {
              some: {
                id: {
                  is_not_empty: true,
                },
              },
            },
          },
        ],
      },
    };

    return (
      <Form
        onSubmit={this.onSubmit}
        subscription={{ submitting: true, error: true, pristine: true }}
        mutators={{
          ...arrayMutators,
        }}
        initialValues={this.state.data}
        validate={validateReportsConfig}
        render={({ form: { change }, handleSubmit, submitting, validating }) => {
          return (
            <>
              <form onSubmit={handleSubmit}>
                <ReportsCard stretch withFooter>
                  <Card.Header>
                    <Heading type="h4" text={'Forecasting'} />
                  </Card.Header>
                  <ViewCardBody
                    style={{ textAlign: 'left', padding: '25px 24px' }}
                    className="card-body-report-with-footer">
                    <FieldSection>
                      <FieldSectionHeader weight="semibold" text="Organization Filter" />
                      <Field
                        name="initiative"
                        subscription={{ active: true, value: true, error: true }}
                        render={({ input: { value, onChange }, ...rest }) => (
                          <>
                            {value ? (
                              <FilterRow withDeleteButton>
                                <Query
                                  query={INITIATIVE_FULL_LIST_QUERY}
                                  variables={queryArgs}
                                  skip={!selectedAlliance || !selectedAlliance.id}>
                                  {({ data, loading }) => {
                                    const initiativesOptions = R.pathOr(
                                      [],
                                      ['initiativesList', 'items'],
                                      data,
                                    ).map((initiative) => ({
                                      label: initiative.name,
                                      value: initiative.id,
                                    }));
                                    return (
                                      <SelectField
                                        input={{
                                          value: value.id,
                                          onChange: (value) =>
                                            onChange({
                                              id: value,
                                            }),
                                        }}
                                        label="Filter by"
                                        placeholder="Select Initiative"
                                        options={[
                                          { label: 'All initiatives', value: 0 },
                                          ...initiativesOptions,
                                        ]}
                                        loading={loading}
                                        {...rest}
                                      />
                                    );
                                  }}
                                </Query>
                                <DeleteButton
                                  text={''}
                                  iconName={'Delete'}
                                  onClick={() => onChange(null)}
                                  iconSize={'md'}
                                />
                              </FilterRow>
                            ) : (
                              <>
                                <FieldSectionText text="No organization filters have been applied to this report, currently the report is capturing data from the entire alliance." />
                                <TransparentButtonWithIcon
                                  text={'Add Organization Filter'}
                                  iconName={'Add'}
                                  onClick={() => onChange({ id: 0 })}
                                />
                              </>
                            )}
                          </>
                        )}
                      />
                    </FieldSection>
                    <FieldSection>
                      <FieldSectionHeader weight="semibold" text="Filter" />
                      <FieldArray name="filters">
                        {({ fields }) => {
                          return (
                            <>
                              {fields.length > 0 ? (
                                fields.map((name, fieldIndex) => (
                                  <Field
                                    key={fieldIndex}
                                    fieldIndex={fieldIndex}
                                    name={name}
                                    component={FilterFieldsRow}
                                    deleteFilter={() => fields.remove(fieldIndex)}
                                    fieldsDict={fieldsDictByFilters}
                                    selectedFilters={fields.value}
                                    fieldsOptions={fieldsOptionsByFilters}
                                    changeFormValue={change}
                                    withDeleteButton
                                  />
                                ))
                              ) : (
                                <FieldSectionText text="No filters have been applied to this report." />
                              )}
                              {fields.length < fieldsOptionsByFilters.length ? (
                                <TransparentButtonWithIcon
                                  text={'Add Filter'}
                                  iconName={'Add'}
                                  onClick={() => fields.push({})}
                                />
                              ) : null}
                            </>
                          );
                        }}
                      </FieldArray>
                    </FieldSection>
                  </ViewCardBody>
                  <Card.Footer style={{ padding: '13px 23px' }}>
                    <ActionButton
                      text="Run"
                      style={{ width: '151px' }}
                      type="submit"
                      disabled={submitting || validating}
                    />
                  </Card.Footer>
                </ReportsCard>
              </form>
            </>
          );
        }}
      />
    );
  }
}

export { ForecastingConfigView };

import React from 'react';
import * as R from 'ramda';
import { Heading, Button, Icon, Grid, Dropdown, Loader } from '@8base/boost';
import View from '@cobuildlab/react-flux-state';
import { Query } from 'react-apollo';

import { Menu } from '../../../shared/components/Menu';
import { getCurrencyOnSession } from '../../../shared/alliance-utils';
import { ViewCardBody } from '../../../components/card/ViewCardBody';
import { ReportHeader, ReportsCard } from '../components';
import { BUDGET_VS_FORECAST_QUERY } from '../reports-queries';
import sessionStore, { NEW_SESSION_EVENT } from '../../../shared/SessionStore';

import {
  generateBudgetVsForecastFilters,
  getInitiativesListData,
  convertInitiativesListToExcelData,
} from './forecasting-utils';
import { generateRequestMonthsInfo } from '../utils';
import BudgetVsForecastStore, { BUDGET_VS_FORECAST_FILTERS_EVENT } from './forecasting-store';
import { ForecastingTable } from './ForecastingTable';

class ForecastingReportView extends View {
  constructor(props) {
    super(props);
    const config = BudgetVsForecastStore.getState(BUDGET_VS_FORECAST_FILTERS_EVENT);

    this.state = {
      queryArgs: this.buildRequestArgs(config),
      config,
    };

    this.selectedAlliance = sessionStore.getState(NEW_SESSION_EVENT).selectedAlliance;
  }

  componentDidMount() {
    this.subscribe(BudgetVsForecastStore, BUDGET_VS_FORECAST_FILTERS_EVENT, this.onConfigChange);
  }

  onConfigChange = (config) => {
    this.setState({
      queryArgs: this.buildRequestArgs(config),
      config,
    });
  };

  render() {
    const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
    const { queryArgs } = this.state;

    return (
      <ReportsCard stretch>
        <Query
          query={BUDGET_VS_FORECAST_QUERY}
          variables={queryArgs}
          skip={!selectedAlliance || !selectedAlliance.id}>
          {this.renderContent}
        </Query>
      </ReportsCard>
    );
  }

  renderContent = ({ data, loading }) => {
    const { config } = this.state;
    if (loading) {
      return <Loader stretch />;
    }

    const { initiativesList } = data;
    const currency = getCurrencyOnSession();

    const dateFilter = R.pathOr([], ['filters'], config).find(
      (filter) => filter.fieldId === 'id-2',
    );

    const currentInitiative = R.path(['initiative'], config);

    const requestMonthsInfo = generateRequestMonthsInfo(dateFilter);
    const initiativesListData = getInitiativesListData(initiativesList.items, requestMonthsInfo);

    const reportName = 'Forecasting';

    return (
      <>
        <ReportHeader>
          <Heading type="h4" text={reportName} />
          {/* Temporarily commented out, these functions are in development right now
          <SearchBox /> */}
          <Grid.Box direction="row" alignItems="center" justifyContent="flex-end">
            <Dropdown defaultOpen={false}>
              <Dropdown.Head>
                <Button variant="outlined" color="GRAY4" squared>
                  <Icon name="More" />
                </Button>
              </Dropdown.Head>
              <Dropdown.Body pin="right">
                {({ closeDropdown }) => (
                  <Menu>
                    <Menu.Item
                      onClick={() => {
                        const fileName = `${reportName} ${new Date().toISOString()}`;
                        convertInitiativesListToExcelData(
                          initiativesListData,
                          currentInitiative,
                          fileName,
                        );
                        closeDropdown();
                      }}>
                      Export to Excel
                    </Menu.Item>
                  </Menu>
                )}
              </Dropdown.Body>
            </Dropdown>
          </Grid.Box>
        </ReportHeader>

        <ViewCardBody style={{ padding: 0 }} className="card-body-report">
          <ForecastingTable
            currency={currency}
            config={config}
            initiativesList={initiativesListData}
          />
        </ViewCardBody>
      </>
    );
  };

  buildRequestArgs(config) {
    const args = {
      filter: {
        AND: [],
      },
    };

    const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);

    const filtersList = generateBudgetVsForecastFilters(config.filters);

    if (Array.isArray(config.filters)) {
      args.filter.AND.push(...filtersList);
    }

    if (config.initiative && config.initiative.id) {
      args.filter.AND.push({
        id: {
          equals: config.initiative.id,
        },
      });
    }

    args.filter.AND.push({
      alliance:
        selectedAlliance && selectedAlliance.id
          ? { id: { equals: selectedAlliance.id } }
          : undefined,
    });

    args.filter.AND.push({
      forecastingYears: {
        some: {
          id: {
            is_not_empty: true,
          },
        },
      },
    });

    return args;
  }
}

export { ForecastingReportView };

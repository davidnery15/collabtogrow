import React from 'react';
import * as R from 'ramda';
import styled, { css } from 'styled-components';
import { PropTypes } from 'prop-types';
import { NoData } from '@8base/boost';

import { CurrencyVarianceField } from 'components/text/CurrencyVarianceField';
import { CurrencyFormat } from 'shared/components/CurrencyFormat';
import {
  ReportGroupSection,
  ReportGroupTitle,
  ReportTable,
  ReportTableTH as TH,
  ReportTableTR as TR,
  ReportTableTD as TD,
  ReportTableFooterTD as FooterTD,
} from '../components';
import { MONTHS } from '../reports-model';

const TitleWrap = styled.div`
  padding-left: 26px;
  width: 132px;
`;

const NumberWrap = styled.div`
  padding-right: 32px;
  padding-left: 21px;
  text-align: right;
  white-space: nowrap;
  ${({ withBrackets }) =>
    withBrackets &&
    css`
      padding-right: 27px;
    `}
  ${({ textWeight }) =>
    css`
      font-weight: ${textWeight ? textWeight : 'normal'};
    `}
`;

const AmountWrap = styled.div`
  padding-right: 24px;
  padding-left: 21px;
  text-align: right;
  white-space: nowrap;
  ${({ withBrackets }) =>
    withBrackets &&
    css`
      padding-right: 19px;
    `}
  ${({ textWeight }) =>
    css`
      font-weight: ${textWeight ? textWeight : 'normal'};
    `}
`;

const rowTitlesDict = {
  budget: 'Budget',
  yTDActuals: 'YTD Actuals',
  variance: 'Variance',
  forecasting: 'Forecast',
};

class ForecastingTable extends React.PureComponent {
  render() {
    const { initiativesList, config } = this.props;
    const selectedInitiative = R.pathOr('', ['initiative', 'id'], config);

    if (initiativesList.length === 0) {
      return <NoData />;
    }

    return (
      <>
        {initiativesList.map((initiative) => this.renderTable(initiative))}
        {!selectedInitiative && this.renderTotalTable(initiativesList)}
      </>
    );
  }

  renderTotalTable = (initiativesList) => {
    const monthTotalAmounts = initiativesList.reduce((accum, initiative) => {
      initiative.forecastingsInSpecifiedRange.forEach(({ budget }, index) => {
        accum[index] = (accum[index] || 0) + (budget || 0);
      });
      return accum;
    }, []);

    return (
      <ReportGroupSection>
        <ReportGroupTitle>{'Total Budget'}</ReportGroupTitle>
        <ReportTable>
          {this.renderTableHeader(initiativesList[0].forecastingsInSpecifiedRange, 'INITIATIVES')}
          <tbody>
            {initiativesList.map(({ forecastingsInSpecifiedRange, name }) => {
              return this.renderTableRow(forecastingsInSpecifiedRange, 'budget', name);
            })}
          </tbody>
          {this.renderTableFooter(monthTotalAmounts)}
        </ReportTable>
      </ReportGroupSection>
    );
  };

  renderTable = (initiative) => {
    const { name, id, forecastingsInSpecifiedRange } = initiative;
    return (
      <ReportGroupSection key={id}>
        <ReportGroupTitle>{name}</ReportGroupTitle>
        <ReportTable>
          {this.renderTableHeader(forecastingsInSpecifiedRange, 'OVERVIEW')}
          <tbody>
            {Object.keys(rowTitlesDict).map((rowName) => {
              return this.renderTableRow(forecastingsInSpecifiedRange, rowName);
            })}
          </tbody>
        </ReportTable>
      </ReportGroupSection>
    );
  };

  renderTableRow = (rowData, rowName, initiativeName) => {
    const { currency } = this.props;
    const title = initiativeName ? initiativeName : rowTitlesDict[rowName];

    const rowAmount = rowData.reduce(
      (accum, item) => (item[rowName] ? accum + item[rowName] : accum),
      0,
    );

    return (
      <TR>
        <TD>
          <TitleWrap>{title}</TitleWrap>
        </TD>
        <TD rightBorder>
          <AmountWrap textWeight={600} withBrackets={rowAmount < 0}>
            {rowName === 'variance' ? (
              <CurrencyVarianceField textWeight={600} value={rowAmount} currency={currency} />
            ) : (
              <CurrencyFormat {...currency} value={rowAmount} displayType="text" decimalScale={2} />
            )}
          </AmountWrap>
        </TD>
        {rowData.map((item, key) => (
          <TD key={key}>
            <NumberWrap withBrackets={item[rowName] < 0}>
              {rowName === 'variance' ? (
                <CurrencyVarianceField
                  value={item[rowName] ? item[rowName] : 0}
                  currency={currency}
                />
              ) : (
                <CurrencyFormat
                  {...currency}
                  value={item[rowName] ? item[rowName] : 0}
                  displayType="text"
                  decimalScale={2}
                />
              )}
            </NumberWrap>
          </TD>
        ))}
      </TR>
    );
  };

  renderTableHeader = (forecastingsList, title) => {
    return (
      <thead>
        <TR>
          <TH>
            <TitleWrap>{title}</TitleWrap>
          </TH>
          <TH rightBorder></TH>
          {forecastingsList.map(({ month, year }, key) => {
            return (
              <TH key={key}>
                <NumberWrap textWeight={'bold'}>{`${MONTHS[month - 1]}-${String(year).substring(
                  2,
                  4,
                )}`}</NumberWrap>
              </TH>
            );
          })}
        </TR>
      </thead>
    );
  };

  renderTableFooter = (monthTotalAmounts) => {
    const totalAmont = monthTotalAmounts.reduce((total, monthAmount) => total + monthAmount, 0);

    const { currency } = this.props;
    return (
      <tfoot>
        <tr>
          <FooterTD>
            <TitleWrap>Total</TitleWrap>
          </FooterTD>
          <FooterTD total rightBorder>
            <AmountWrap>
              <CurrencyFormat
                {...currency}
                value={totalAmont}
                displayType="text"
                decimalScale={2}
              />
            </AmountWrap>
          </FooterTD>
          {monthTotalAmounts.map((monthAmount, key) => (
            <FooterTD total key={key}>
              <NumberWrap>
                <CurrencyFormat
                  {...currency}
                  value={monthAmount}
                  displayType="text"
                  decimalScale={2}
                />
              </NumberWrap>
            </FooterTD>
          ))}
        </tr>
      </tfoot>
    );
  };
}

ForecastingTable.propTypes = {
  currency: PropTypes.object.isRequired,
  initiativesList: PropTypes.array.isRequired,
  config: PropTypes.object.isRequired,
};

export { ForecastingTable };

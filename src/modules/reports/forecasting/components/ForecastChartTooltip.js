import React from 'react';
import PropTypes from 'prop-types';
import { Paper, Text } from '@8base/boost';
import { CurrencyTextField } from '../../../../shared/components/CurrencyTextField';

/**
 * ForecastChartTooltip: custom tooltip for the recharts library.
 *
 * @returns {Function} The rendered component.
 */
const ForecastChartTooltip = ({ active, payload, currency, label }) => {
  if (active) {
    const budgetValue = payload[0].value;
    const actualValue = payload[1].value;
    const forecastValue = payload[2].value;

    return (
      <Paper padding={'md'}>
        <Text>{label}</Text>
        <br />
        <Text color={'PRIMARY'}>
          {`Budget: `}
          <CurrencyTextField currency={currency} value={budgetValue} />
        </Text>
        <br />
        <Text color={'GREEN'}>
          {`Actual: `}
          <CurrencyTextField currency={currency} value={actualValue} />
        </Text>
        <br />
        <Text color={'BLUE_2'}>
          {`Forecast: `}
          <CurrencyTextField currency={currency} value={forecastValue} />
        </Text>
      </Paper>
    );
  }

  return null;
};

ForecastChartTooltip.defaultProps = {
  currency: null,
};

ForecastChartTooltip.propTypes = {
  active: PropTypes.bool.isRequired,
  payload: PropTypes.array.isRequired,
  currency: PropTypes.object,
  label: PropTypes.string.isRequired,
};

export { ForecastChartTooltip };

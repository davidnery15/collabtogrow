import { Bar, BarChart, Tooltip, XAxis, YAxis, ResponsiveContainer } from 'recharts';
import React from 'react';
import { Text } from '@8base/boost';
import styled from '@emotion/styled';
import PropTypes from 'prop-types';

const StyleTitle = styled(Text)`
  height: 23px;
  width: 273px;
  opacity: 0.9;
  color: #323c47;
  font-family: Poppins;
  font-size: 16px;
  font-weight: 600;
  line-height: 25px;
`;

const StyledContainer = styled('div')`
  position: relative;
  margin-bottom: 20px;
`;

export const ForecastReportChart = ({ data }) => {
  return (
    <>
      <StyledContainer>
        <StyleTitle>Budget vs Forecast</StyleTitle>
      </StyledContainer>
      <div style={{ width: '100%', height: '400px' }}>
        <ResponsiveContainer>
          <BarChart data={data}>
            <XAxis dataKey="name" stroke="#8884d8" />
            <YAxis allowDecimals={false} />
            <Tooltip />
            <Bar type="monotone" dataKey={'budget'} fill="#2BA1FF" barSize={30} />
            <Bar type="monotone" dataKey={'ytdActual'} fill="#70D34C" barSize={30} />
            <Bar type="monotone" dataKey={'contribution'} fill="#0096B8" barSize={30} />
            <Bar type="monotone" dataKey={'booking'} fill="#E54034" barSize={30} />
          </BarChart>
        </ResponsiveContainer>
      </div>
    </>
  );
};

ForecastReportChart.propTypes = {
  data: PropTypes.object.isRequired,
};

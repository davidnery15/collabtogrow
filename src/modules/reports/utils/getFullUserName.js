const getFullUserName = ({ firstName, lastName }) => {
  return `${firstName}${firstName ? ' ' : ''}${lastName}`;
};
export { getFullUserName };

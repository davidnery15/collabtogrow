import moment from 'moment';

const chartsMonths = () => {
  const months = [];
  for (let i = 0, j = 12; i < j; i++) {
    const data = {
      name: moment()
        .month(i)
        .format('MMM YY'),
    };
    months.push(data);
  }
  return months;
};

export { chartsMonths };

const getOptionsFromArray = (array) => {
  return array.map((arrayItem) => ({ label: arrayItem, value: arrayItem }));
};

export { getOptionsFromArray };

const getFullNameWithEmail = ({ firstName, lastName, email }) => {
  return `${firstName}${firstName ? ' ' : ''}${lastName}${lastName ? ' - ' : ''}${email}`;
};
export { getFullNameWithEmail };

import * as R from 'ramda';

const getFullCompanyName = (company) => {
  const name = R.pathOr('Company not specified', ['name'], company);
  const website = R.pathOr('', ['website'], company);

  return `${name}${website ? ' - ' : ''}${website}`;
};
export { getFullCompanyName };

export const randomHEX = () =>
  `#${(
    '000000' +
    Math.random()
      .toString(16)
      .slice(2, 8)
  ).slice(-6)}`;

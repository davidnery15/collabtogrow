import * as R from 'ramda';

const initiativesToString = (initiatives) => {
  const initiativesList = R.pathOr([], ['items'], initiatives);
  return initiativesList.map(({ name }) => `- ${name};`).join(' \n');
};

export { initiativesToString };

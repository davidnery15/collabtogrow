import React from 'react';
import { Heading, Loader, Row } from '@8base/boost';
import * as R from 'ramda';
import View from '@cobuildlab/react-flux-state';
import BigInt from 'big-integer';
import { CurrencyFormat } from 'shared/components/CurrencyFormat';
import { getCurrencyOnSession } from 'shared/alliance-utils';
import { randomHEX } from '../utils';
import moment from 'moment';

import {
  Break,
  ChartCard,
  ReportHeader,
  GeneralInfo,
  GeneralInfoItemSales,
  GeneralInfoName,
  GeneralInfoValue,
} from '../components';
import { MONTHS } from '../reports-model';
import { SalesPipelineTrendsDataHandler } from './SalesPipelineTrendsDataHandler';
import { SalesPipelineTrendsChart } from './SalesPipelineTrendsChart';

const clientCompanySelector = 'clientCompanyMonthSnapshots';
const partnerCompanySelector = 'partnerCompanyMonthSnapshots';

class ChartView extends View {
  state = {
    selected: this.props.requestedData.enhancedData[partnerCompanySelector].length
      ? partnerCompanySelector
      : clientCompanySelector,
  };

  onChangeSelected = () => {
    this.setState((currentState) => ({
      selected:
        currentState.selected === clientCompanySelector
          ? partnerCompanySelector
          : clientCompanySelector,
    }));
  };

  dataForChart = ({ data, monthsInfo, group }) => {
    const xAxis = Object.entries(monthsInfo.monthNumberDict)
      .map(([year, months]) => Object.keys(months).map((key) => `${MONTHS[key - 1]}-${year}`))
      .flat();

    return data.reduce(
      (acc, snapshot) => {
        if (!acc.formattedData.length) {
          return {
            formattedData: snapshot.map((month, i) => ({
              name: xAxis[i],
              [month[group]]: month.amount,
            })),
            lines: [{ color: randomHEX(), name: snapshot[0][group] }],
          };
        }

        return {
          formattedData: snapshot.map((month, i) => ({
            ...acc.formattedData[i],
            [month[group]]: month.amount,
          })),
          lines: [...acc.lines, { color: randomHEX(), name: snapshot[0][group] }],
        };
      },
      { formattedData: [], lines: [] },
    );
  };

  dataForGeneralInfo = (data) => {
    const { selected } = this.state;
    const currentMonth = moment().month() + 1;

    const clientStagesAverage = data[clientCompanySelector].length
      ? data[clientCompanySelector].map((stageSnapshots) => {
        const average = stageSnapshots.reduce((acc, snapshot) => {
          if (!acc) return snapshot;
          acc.amount = BigInt(acc.amount)
            .add(snapshot.amount)
            .toString();

          return acc;
        }, null);

        average.average = BigInt(average.amount)
          .divide(currentMonth)
          .toString();

        return average;
      })
      : [];

    const partnerStagesAverage = data[partnerCompanySelector].length
      ? data[partnerCompanySelector].map((stageSnapshots) => {
        const average = stageSnapshots.reduce((acc, snapshot) => {
          if (!acc) return snapshot;
          acc.amount = BigInt(acc.amount)
            .add(snapshot.amount)
            .toString();

          return acc;
        }, null);

        average.average = BigInt(average.amount)
          .divide(currentMonth)
          .toString();

        return average;
      })
      : [];

    const selectedCompanyAverage =
      selected === clientCompanySelector ? clientStagesAverage : partnerStagesAverage;

    return selectedCompanyAverage.map((item) => {
      return {
        name: `Average Stage: ${item.stage} - ${item.company.name}`,
        value: item.average,
      };
    });
  };

  renderContent = () => {
    const {
      sharedData: { clientCompany, partnerCompany },
      requestedData: { enhancedData, groupBy, requestMonthsInfo },
      loading,
    } = this.props;
    if (loading) {
      return <Loader stretch />;
    }

    const { formattedData, lines } = this.dataForChart({
      data: R.path([this.state.selected], enhancedData),
      monthsInfo: requestMonthsInfo,
      group: groupBy[1],
    });

    const generalInfo = this.dataForGeneralInfo(R.clone(enhancedData));

    const currency = getCurrencyOnSession();

    const selectedCompanyName =
      this.state.selected === clientCompanySelector ? clientCompany.name : partnerCompany.name;

    return (
      <>
        <ReportHeader>
          <Heading type="h4" text={'Sales Pipeline Trends'} />
        </ReportHeader>
        <GeneralInfo>
          <Row flexWrap="wrap" alignItems="start">
            {generalInfo.map((el, index) => {
              const breakRow = (index + 1) % 3 === 0;

              return (
                <>
                  <GeneralInfoItemSales key={index}>
                    <GeneralInfoName>{el.name}</GeneralInfoName>
                    <GeneralInfoValue>
                      {el.value === '0' ? (
                        'N/A'
                      ) : (
                        <CurrencyFormat {...currency} value={el.value} displayType="text" />
                      )}
                    </GeneralInfoValue>
                  </GeneralInfoItemSales>
                  {breakRow ? <Break /> : null}
                </>
              );
            })}
          </Row>
        </GeneralInfo>
        <SalesPipelineTrendsChart
          data={formattedData}
          onChangeSelected={this.onChangeSelected}
          selectedCompanyName={selectedCompanyName}
          lineList={lines}
          currency={currency}
          noSwitcher={
            !enhancedData[clientCompanySelector].length ||
            !enhancedData[partnerCompanySelector].length
          }
        />
      </>
    );
  };

  render() {
    return (
      <ChartCard loading={this.props.loading} stretch>
        {this.renderContent()}
      </ChartCard>
    );
  }
}

export const SalesPipelineTrendsChartsView = SalesPipelineTrendsDataHandler(ChartView);

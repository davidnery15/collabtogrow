import Flux from '@cobuildlab/flux-state';
import { SALES_PIPELINE_TRENDS_FILTERS_EVENT } from './sales-pipeline-trends-store';

export const saveSalesPipelineTrendsFilters = (data) => {
  Flux.dispatchEvent(SALES_PIPELINE_TRENDS_FILTERS_EVENT, data);
};

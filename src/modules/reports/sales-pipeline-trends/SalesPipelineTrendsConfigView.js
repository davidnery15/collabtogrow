import React from 'react';
import { Form, Field } from 'react-final-form';
import arrayMutators from 'final-form-arrays';
import { FieldArray } from 'react-final-form-arrays';
import { Card, Heading, SelectField } from '@8base/boost';
import View from '@cobuildlab/react-flux-state';

import AllianceStore, {
  ALLIANCE_LIST_MEMBERS_EVENT,
} from '../../settings/alliance-management/alliance-store';
import { TransparentButtonWithIcon } from '../../../components/buttons/TransparentButtonWithIcon';
import { ViewCardBody } from '../../../components/card/ViewCardBody';
import { ActionButton } from '../../../components/buttons/ActionButton';
import { fetchCurrentAllianceMembersAction } from '../../settings/alliance-management/alliance-actions';
import {
  FieldSection,
  FieldSectionHeader,
  FieldSectionText,
  DeleteButton,
  FilterRow,
  FilterFieldsRow,
  GroupFieldsRow,
  ReportsCard,
} from '../components';
import { getOptionsFromArray, validateReportsConfig } from '../utils';
import SalesPipelineTrendsStore, {
  SALES_PIPELINE_TRENDS_FILTERS_EVENT,
} from './sales-pipeline-trends-store';
import { saveSalesPipelineTrendsFilters } from './sales-pipeline-trends-actions';
import { FILTER_FIELDS_LIST, GROUPING_FIELDS_LIST } from './sales-pipeline-trends-model';
import { fetchAllDealsData } from '../../management/deal/deal-actions';
import dealStore, { ALL_DEAL_DATA_LIST_EVENT } from '../../management/deal/deal-store';

/**
 * These fieldsOptionsByFilters and fieldsDictByFilters are used only for the 'filters' field.
 */
const fieldsOptionsByFilters = FILTER_FIELDS_LIST.map((field) => ({
  label: field.displayName,
  value: field.id,
}));
const fieldsDictByFilters = FILTER_FIELDS_LIST.reduce((dict, field) => {
  return {
    ...dict,
    [field.id]: field,
  };
}, {});

/**
 * These fieldsOptionsByGroups and fieldsDictByGroup are used only for the 'groups' field.
 */
const fieldsOptionsByGroups = GROUPING_FIELDS_LIST.map((field) => ({
  label: field.displayName,
  value: field.name,
}));
const fieldsDictByGroup = GROUPING_FIELDS_LIST.reduce((dict, field) => {
  return {
    ...dict,
    [field.name]: field,
  };
}, {});

class SalesPipelineTrendsConfigView extends View {
  constructor(props) {
    super(props);

    this.state = {
      data: SalesPipelineTrendsStore.getState(SALES_PIPELINE_TRENDS_FILTERS_EVENT),
      clientCompany: null,
      partnerCompany: null,
      dealsList: [],
    };
  }

  componentDidMount() {
    this.subscribe(SalesPipelineTrendsStore, SALES_PIPELINE_TRENDS_FILTERS_EVENT, (data) => {
      this.setState({ data });
    });

    this.subscribe(AllianceStore, ALLIANCE_LIST_MEMBERS_EVENT, (data) => {
      this.setState({
        clientCompany: data.clientCompany,
        partnerCompany: data.partnerCompany,
      });
    });

    this.subscribe(dealStore, ALL_DEAL_DATA_LIST_EVENT, (state) => {
      const { items } = state.dealDataList;
      this.setState({
        dealsList: items,
      });
    });

    fetchCurrentAllianceMembersAction();
    fetchAllDealsData();
  }

  onSubmit = (data) => {
    const { history } = this.props;
    saveSalesPipelineTrendsFilters(data);
    history.push('/reports/sales-pipeline-trends/report');
  };

  render() {
    const { clientCompany, partnerCompany, dealsList } = this.state;

    const organiationFilterOptions =
      clientCompany && partnerCompany
        ? [
          {
            label: clientCompany.name,
            value: clientCompany.id,
          },
          {
            label: partnerCompany.name,
            value: partnerCompany.id,
          },
        ]
        : [];

    const stageOptions = getOptionsFromArray(
      Array.from(new Set(dealsList.map((deal) => deal.stage))),
    );

    return (
      <Form
        onSubmit={this.onSubmit}
        subscription={{ submitting: true, error: true, pristine: true }}
        mutators={{
          ...arrayMutators,
        }}
        initialValues={this.state.data}
        validate={validateReportsConfig}
        render={({ form: { change }, handleSubmit, submitting, validating }) => {
          return (
            <>
              <form onSubmit={handleSubmit}>
                <ReportsCard stretch withFooter>
                  <Card.Header>
                    <Heading type="h4" text={'Sales Pipeline Trends'} />
                  </Card.Header>
                  <ViewCardBody
                    style={{ textAlign: 'left', padding: '25px 24px' }}
                    className="card-body-report-with-footer">
                    <FieldSection>
                      <FieldSectionHeader weight="semibold" text="Organization Filter" />
                      <Field
                        name="organization"
                        subscription={{ active: true, value: true, error: true }}
                        render={({ input: { value, onChange }, ...rest }) => (
                          <>
                            {value ? (
                              <FilterRow withDeleteButton>
                                <SelectField
                                  input={{
                                    value: value.id,
                                    onChange: (value) =>
                                      onChange({
                                        id: value,
                                      }),
                                  }}
                                  label="Filter by"
                                  placeholder="Select Company"
                                  options={organiationFilterOptions}
                                  {...rest}
                                />
                                <DeleteButton
                                  text={''}
                                  iconName={'Delete'}
                                  onClick={() => onChange(null)}
                                  iconSize={'md'}
                                />
                              </FilterRow>
                            ) : (
                              <>
                                <FieldSectionText text="No organization filters have been applied to this report, currently the report is capturing data from the entire alliance." />
                                <TransparentButtonWithIcon
                                  text={'Add Organization Filter'}
                                  iconName={'Add'}
                                  onClick={() => onChange({})}
                                />
                              </>
                            )}
                          </>
                        )}
                      />
                    </FieldSection>
                    <FieldSection>
                      <FieldSectionHeader weight="semibold" text="Filter" />
                      <FieldArray name="filters">
                        {({ fields }) => {
                          return (
                            <>
                              {fields.length > 0 ? (
                                fields.map((name, fieldIndex) => (
                                  <Field
                                    key={fieldIndex}
                                    fieldIndex={fieldIndex}
                                    name={name}
                                    component={FilterFieldsRow}
                                    deleteFilter={() => fields.remove(fieldIndex)}
                                    fieldsDict={fieldsDictByFilters}
                                    selectedFilters={fields.value}
                                    fieldsOptions={fieldsOptionsByFilters}
                                    stageOptions={stageOptions}
                                    changeFormValue={change}
                                    withDeleteButton
                                  />
                                ))
                              ) : (
                                <FieldSectionText text="No filters have been applied to this report." />
                              )}
                              {fields.length < fieldsOptionsByFilters.length ? (
                                <TransparentButtonWithIcon
                                  text={'Add Filter'}
                                  iconName={'Add'}
                                  onClick={() => fields.push({})}
                                />
                              ) : null}
                            </>
                          );
                        }}
                      </FieldArray>
                    </FieldSection>
                    <FieldSection>
                      <FieldSectionHeader weight="semibold" text="Group" />
                      <FieldArray name="groups">
                        {({ fields }) => {
                          return (
                            <>
                              {fields.length > 0 ? (
                                fields.map((name, fieldIndex) => {
                                  return (
                                    <Field
                                      key={fieldIndex}
                                      name={name}
                                      subscription={{ active: true, value: true, error: true }}
                                      component={GroupFieldsRow}
                                      deleteFilter={() => fields.remove(fieldIndex)}
                                      fieldsDict={fieldsDictByGroup}
                                      selectedGroups={fields.value}
                                      fieldIndex={fieldIndex}
                                      fieldsOptions={fieldsOptionsByGroups}
                                      changeFormValue={change}
                                    />
                                  );
                                })
                              ) : (
                                <FieldSectionText text="No groups have been applied to this report." />
                              )}
                              {fields.length < 1 ? (
                                <TransparentButtonWithIcon
                                  text={'Add Group'}
                                  iconName={'Add'}
                                  onClick={() => fields.push({ sort: 'ASC' })}
                                />
                              ) : null}
                            </>
                          );
                        }}
                      </FieldArray>
                    </FieldSection>
                  </ViewCardBody>
                  <Card.Footer style={{ padding: '13px 23px' }}>
                    <ActionButton
                      text="Run"
                      style={{ width: '151px' }}
                      type="submit"
                      disabled={submitting || validating}
                    />
                  </Card.Footer>
                </ReportsCard>
              </form>
            </>
          );
        }}
      />
    );
  }
}

SalesPipelineTrendsConfigView.displayName = 'SalesPipelineTrendsConfigView';

export { SalesPipelineTrendsConfigView };

import React from 'react';
import { Card, Grid, SecondaryNavigation } from '@8base/boost';
import { Route, Switch, Redirect } from 'react-router-dom';
import '../settings/css/settings.css';
import AllianceInvitationListView from '../settings/invitations/AllianceInvitationListView';
import AllianceMemberInvitationListView from '../settings/invitations/AllianceMemberInvitationListView';
import AllianceInvitationAcceptView from '../settings/invitations/AllianceInvitationAcceptView';
import AllianceMemberInvitationAcceptView from '../settings/invitations/AllianceMemberInvitationAcceptView';
import { LinkItem } from '../../components/link/LinkItem';
import { PropTypes } from 'prop-types';

const InvitationsView = (props) => {
  const { location } = props;
  return (
    <Grid.Layout columns="215px 1fr">
      <Grid.Box>
        <SecondaryNavigation>
          <LinkItem
            to={'/invitations/alliance-invitations'}
            label={'My Alliances'}
            location={location}
            url={'alliance-invitations'}
          />
          <LinkItem
            to={'/invitations/alliance-member-invitations'}
            label={'Alliance Membership'}
            location={location}
            url={'alliance-member-invitations'}
          />
        </SecondaryNavigation>
      </Grid.Box>
      <Grid.Box>
        <Card stretch>
          <Switch>
            <Route
              path="/invitations/alliance-invitations"
              component={AllianceInvitationListView}
            />
            <Route
              path="/invitations/alliance-member-invitations"
              component={AllianceMemberInvitationListView}
            />
            <Route
              path="/invitations/accept-alliance-invitation/:id"
              component={AllianceInvitationAcceptView}
            />
            <Route
              path="/invitations/accept-alliance-member-invitation/:id"
              component={AllianceMemberInvitationAcceptView}
            />
            <Redirect to="/invitations/alliance-member-invitations" />
          </Switch>
        </Card>
      </Grid.Box>
    </Grid.Layout>
  );
};

InvitationsView.propTypes = {
  location: PropTypes.any.isRequired,
};

export default InvitationsView;

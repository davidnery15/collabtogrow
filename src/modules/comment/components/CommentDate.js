import React from 'react';
import { PropTypes } from 'prop-types';
import { Text } from '@8base/boost';
import moment from 'moment';
import styled from '@emotion/styled';

// Component checks if there's a value present to render
// or else returns "None"

const StyledDate = styled(Text)`
  width: 177.97px;
  color: #989898;
  font-family: Poppins;
  font-size: 12px;
`;

const CommentDate = ({ date, fontSize }) => {
  const dateFormat = date ? moment(date).format('LLL') : '';
  return <StyledDate text={dateFormat} />;
};

CommentDate.propTypes = {
  date: PropTypes.object,
  fontSize: PropTypes.number,
};

export { CommentDate };

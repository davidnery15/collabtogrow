import React from 'react';
import { compose } from 'recompose';
import { Dropdown, Menu, Avatar, Card } from '@8base/boost';
import { withLogout } from '@8base/app-provider';
import * as R from 'ramda';
import { PropTypes } from 'prop-types';
import sessionStore, { NEW_SESSION_EVENT } from '../shared/SessionStore';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router';
import DropdownUserText from './DropdownUserText';
import styled from '@emotion/styled';

/**
 *
 * @return {*}
 * @constructor
 */

const StyledAvatar = styled(Avatar)({
  height: '62px',
  width: '62px',
});

const UserDropdown = (props) => {
  const user = sessionStore.getState(NEW_SESSION_EVENT).user;
  const { items } = user.companyUserRelation;
  const companyName = items.length !== 0 ? items[0].company.name : null;
  const role = items.length !== 0 ? items[0].role.name : null;

  return (
    <Dropdown defaultOpen={false}>
      <Dropdown.Head>
        <Avatar
          src={R.path(['avatar', 'downloadUrl'], user)}
          firstName={R.path(['firstName'], user)}
          lastName={R.path(['lastName'], user)}
          size="sm"
        />
      </Dropdown.Head>
      <Dropdown.Body pin="right" offset="md">
        {({ closeDropdown }) => (
          <Menu>
            <Card.Header
              onClick={() => {
                closeDropdown();
                props.history.push('/settings/user-profile');
              }}
              style={{ cursor: 'pointer', height: '115px', minWidth: '269px' }}
              padding={'md'}>
              <StyledAvatar
                src={R.path(['avatar', 'downloadUrl'], user)}
                firstName={R.path(['firstName'], user)}
                lastName={R.path(['lastName'], user)}
              />
              {user.lastName && user.firstName && (
                <div style={{ marginLeft: 10 }}>
                  <DropdownUserText
                    text={`${user.firstName} ${user.lastName}`}
                    fontSize={14}
                    color={'#323C47'}
                    fontWeight={700}
                  />
                  <DropdownUserText text={companyName} fontSize={12} />
                  <DropdownUserText text={role} fontSize={12} />
                  <DropdownUserText text={user.email} fontSize={13} color={'#323C47'} />
                </div>
              )}
            </Card.Header>
            <Menu.Item>
              <Link onClick={closeDropdown} to="/settings/user-profile">
                My Account
              </Link>
            </Menu.Item>
            <Menu.Item
              onClick={() => {
                props.logout({
                  returnTo: `${window.location.origin}/home`,
                });
                closeDropdown();
              }}>
              Log Out
            </Menu.Item>
          </Menu>
        )}
      </Dropdown.Body>
    </Dropdown>
  );
};

UserDropdown.propTypes = {
  logout: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
};

export default compose(
  withLogout,
  withRouter,
)(UserDropdown);

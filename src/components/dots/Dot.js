import React from 'react';
import { PropTypes } from 'prop-types';
import './dots.css';

/**
 *
 * @param props
 * @return {*}
 * @constructor
 */
const Dot = ({ active = false }) => {
  return <span className={active ? 'active' : ''} />;
};

Dot.propTypes = {
  active: PropTypes.bool,
};

export { Dot };

import React from 'react';
import { PropTypes } from 'prop-types';
import { InputField } from '@8base/boost';
import numeral from 'numeral';

const NUMERAL_FORMAT = '$ 0,0[.]00';

/**
 * @deprecated Use FileInputField instead
 */
class MoneyInputField extends React.Component {
  render() {
    const { onChange, label } = this.props;
    const value = numeral(this.props.value).format(NUMERAL_FORMAT);

    return (
      <InputField
        style={{ width: '90%' }}
        label={label}
        input={{
          value,
          onChange: (value, e) => {
            onChange(numeral(value).value());
          },
        }}
      />
    );
  }
}

MoneyInputField.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.any.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default MoneyInputField;

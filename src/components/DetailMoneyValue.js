import React from 'react';
import { PropTypes } from 'prop-types';
import LightGreyText from './LightGreyText';

// Component checks if there's a valid array of number value(s) to render
// or else returns "None"
const DetailMoneyValue = ({ value, fontSize }) => {
  if (value && value.length > 0) {
    return (
      <span style={fontSize ? { fontSize: fontSize } : {}}>
        {'$' +
          parseFloat(value[0])
            .toFixed(2)
            .replace(/\d(?=(\d{3})+\.)/g, '$&,')}
      </span>
    );
  }
  return <LightGreyText text="None" />;
};

DetailMoneyValue.propTypes = {
  value: PropTypes.array.isRequired,
  fontSize: PropTypes.number,
};

export default DetailMoneyValue;

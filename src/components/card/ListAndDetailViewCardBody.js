import React from 'react';
import { Card } from '@8base/boost';
import { PropTypes } from 'prop-types';

/**
 * @deprecated Use ListCardBody/ViewCardBody
 *
 * Use this instead of Card.Body for List/Detail Views,
 * Add any css Padding, margin on this component
 * @param {Component} children the children component
 */
const ListAndDetailViewCardBody = ({ children, ...rest }) => {
  return (
    <Card.Body
      borderRadius="all"
      style={{ minHeight: '69vh', maxHeight: '54vh', padding: '0px', textAlign: 'center' }}
      {...rest}>
      {children}
    </Card.Body>
  );
};

ListAndDetailViewCardBody.propTypes = {
  children: PropTypes.any.isRequired,
};

export { ListAndDetailViewCardBody };

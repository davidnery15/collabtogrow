import React from 'react';
import { Card } from '@8base/boost';
import { PropTypes } from 'prop-types';

/**
 * Use this instead of Card.Body for Detail Views,
 * Add any css Padding, margin on this component
 * @param {Component} children the children component
 */
const DetailViewCardBody = ({ children, ...rest }) => {
  return (
    <Card.Body borderRadius="all" className="card-body" {...rest}>
      {children}
    </Card.Body>
  );
};

DetailViewCardBody.propTypes = {
  children: PropTypes.any.isRequired,
};

export { DetailViewCardBody };

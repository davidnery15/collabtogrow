import React from 'react';
import sessionStore from '../shared/SessionStore';
import { NEW_SESSION_EVENT, UPDATE_META_EVENT } from '../shared/SessionStore';
import { META_STEP_NAME, META_ALLIANCE_SELECTED } from '../shared';
import { NUMBER_OF_WIZARD_SCREENS } from '../modules/wizard';
import { NavPlate } from '../components/NavPlate';
import { NavItem } from '../components/NavItem';
import View from '@cobuildlab/react-flux-state';

const WIZARD_MENU = null;

const USER_MENU = (
  <NavPlate color={'PRIMARY'}>
    <NavItem icon="SpeedMeter" to="/dashboard" label="Dashboard" />
    <NavItem icon="Group" to="/invitations" label="Invitations" />
    <NavItem icon="Settings" to="/settings" label="Settings" />
  </NavPlate>
);

const USER_MENU_WITH_ALLIANCE = (
  <NavPlate color={'PRIMARY'}>
    <NavItem icon="SpeedMeter" to="/dashboard" label="Dashboard" />
    <NavItem icon="Planet" to="/active-items" label="Active Items" />
    <NavItem icon="Diagram" to="/management" label="Collaborate" />
    <NavItem icon="Table" to="/reports" label="Reports" />
    <NavItem icon="Group" to="/invitations" label="Invitations" />
    <NavItem icon="Settings" to="/settings" label="Settings" />
  </NavPlate>
);

/**
 * Renders the Menu of the App
 */
class Menu extends View {
  constructor(props) {
    super(props);
    const user = sessionStore.getState(NEW_SESSION_EVENT).user;
    const metaList = user.metaRelation.items;
    const metaStep = metaList.find((meta) => meta.name === META_STEP_NAME);
    const metaAlliance = metaList.find((meta) => meta.name === META_ALLIANCE_SELECTED);
    this.state = {
      step: metaStep === undefined ? 0 : parseInt(metaStep.value),
      alliance: metaAlliance === undefined ? null : metaAlliance.value,
    };
  }

  componentDidMount() {
    this.subscribe(sessionStore, UPDATE_META_EVENT, (state) => {
      if (state.name === META_STEP_NAME) this.setState({ step: parseInt(state.value) });
      if (state.name === META_ALLIANCE_SELECTED) this.setState({ alliance: state.value });
    });
  }

  render() {
    const { step, alliance } = this.state;
    if (step < NUMBER_OF_WIZARD_SCREENS - 2) return WIZARD_MENU;
    if (alliance) return USER_MENU_WITH_ALLIANCE;
    return USER_MENU;
  }
}

export default Menu;

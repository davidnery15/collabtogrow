import React from 'react';
import PropTypes from 'prop-types';
import './file-icon.css';
import logoPdf from '../images/icons/2000px-PDF_file_icon.png';
import logoImg from '../images/icons/img_148071.png';
import styled from '@emotion/styled';
import { Text } from '@8base/boost';
import IconPreview from '../components/buttons/IconPreview';

import docx from '../images/icons/file/docx_16x1.svg';
import doc from '../images/icons/file/docx_16x1.svg';
import accdb from '../images/icons/file/accdb_16x1.svg';
import csv from '../images/icons/file/csv_16x1.svg';
import dotx from '../images/icons/file/dotx_16x1.svg';
import mpp from '../images/icons/file/mpp_16x1.svg';
import onepkg from '../images/icons/file/onepkg_16x1.svg';
import mpt from '../images/icons/file/mpt_16x1.svg';
import odp from '../images/icons/file/odp_16x1.svg';
import ods from '../images/icons/file/ods_16x1.svg';
import odt from '../images/icons/file/odt_16x1.svg';
import one from '../images/icons/file/one_16x1.svg';
import onetoc from '../images/icons/file/onetoc_16x1.svg';
import potx from '../images/icons/file/potx_16x1.svg';
import ppsx from '../images/icons/file/ppsx_16x1.svg';
import pptx from '../images/icons/file/pptx_16x1.svg';
import pub from '../images/icons/file/pub_16x1.svg';
import vsdx from '../images/icons/file/vsdx_16x1.svg';
import vssx from '../images/icons/file/vssx_16x1.svg';
import vstx from '../images/icons/file/vstx_16x1.svg';
import xls from '../images/icons/file/xls_16x1.svg';
import xlsx from '../images/icons/file/xlsx_16x1.svg';
import xltx from '../images/icons/file/xltx_16x1.svg';
import xsn from '../images/icons/file/xsn_16x1.svg';

// Component receives a "file" array and depending on the 'file.filename'
// chooses which file icon to render or returns "None"
const FileIcon = ({ index, file, width = 22, field, onClickDelete, nameInput }) => {
  //width should be 16 or 48

  const StyleLabel = styled(Text)({
    width: '118px!important',
    height: '44px!important',
    whiteSpace: 'nowrap!important',
    textOverflow: 'ellipsis!important',
    overflow: 'hidden!important',
    display: 'block',
    paddingRight: '10px',
  });

  const Div = styled('div')({
    width: 44,
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    overflow: 'hidden',
  });

  //default style
  let style = { width: width, display: 'inline-block', transform: 'translateY(2px)' };
  const className = 'file-icon';

  //default image icon
  let imgSource = `https://icons-for-free.com/free-icons/png/512/2529990.png`;

  // Check if there is even a file being received
  if ((file && file.filename) || (file && file.name)) {
    const fileName = file.name !== undefined ? file.name : file.filename;
    const type = fileName.split('.').pop();
    // supportedOfficeTypes is declared to make sure the image
    // is available in the Microsoft sharepoint site for us to use
    const supportedOfficeTypes = [
      'docx',
      'doc',
      'xlsx',
      'xls',
      'csv',
      'pptx',
      'potx',
      'odt',
      'one',
      'ods',
      'odp',
      'accdb',
      'dotx',
      'pub',
      'vstx',
      'xltx',
      'mpp',
      'onepkg',
      'ppsx',
      'vsdx',
      'mpt',
      'onetoc',
      'vssx',
      'xsn',
    ];

    const variable = [
      { name: 'docx', value: docx },
      { name: 'doc', value: doc },
      { name: 'xlsx', value: xlsx },
      { name: 'xls', value: xls },
      { name: 'csv', value: csv },
      { name: 'pptx', value: pptx },
      { name: 'potx', value: potx },
      { name: 'odt', value: odt },
      { name: 'one', value: one },
      { name: 'ods', value: ods },
      { name: 'odp', value: odp },
      { name: 'accdb', value: accdb },
      { name: 'dotx', value: dotx },
      { name: 'pub', value: pub },
      { name: 'vstx', value: vstx },
      { name: 'xltx', value: xltx },
      { name: 'mpp', value: mpp },
      { name: 'onepkg', value: onepkg },
      { name: 'ppsx', value: ppsx },
      { name: 'vsdx', value: vsdx },
      { name: 'mpt', value: mpt },
      { name: 'onetoc', value: onetoc },
      { name: 'vssx', value: vssx },
      { name: 'xsn', value: xsn },
    ];

    let result = variable.filter((data) => data.name === type);

    // MS Office Files Icons
    if (supportedOfficeTypes.includes(type)) {
      imgSource = result[0].value;
    } else {
      imgSource = logoImg;
    }

    // PDF File Icon
    if (type === 'pdf') {
      imgSource = logoPdf;
      style = { width: width, display: 'inline-block', transform: 'translateY(4px)' };
    }

    // Default Component Return
    return (
      <>
        <div className={className}>
          <Div>
            <img style={style} src={imgSource} alt={`${type} icon`} />
          </Div>
          <div style={{ width: '66px' }}>
            <StyleLabel text={fileName} />
          </div>
          <IconPreview
            file={file}
            index={index}
            field={field}
            onClickDelete={onClickDelete}
            nameInput={nameInput}
          />
        </div>
      </>
    );
  }
  //else return "None"
  return null;
};

FileIcon.propTypes = {
  file: PropTypes.object.isRequired,
  field: PropTypes.string,
  nameInput: PropTypes.string,
  index: PropTypes.number,
  onClickDelete: PropTypes.func,
};

FileIcon.defaultProps = {
  field: {},
  nameInput: null,
  index: null,
  onClickDelete: null,
};

export default FileIcon;

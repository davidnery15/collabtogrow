import React from 'react';
import { Dialog, Grid } from '@8base/boost';
import PropTypes from 'prop-types';
import FileViewer from 'react-file-viewer';
import { TransparentButtonFontAwesome } from '../buttons/TransparentButtonFontAwesome';
import { b64toBlob, downloadFile } from '../../shared/utils';

class PreviewDialog extends React.Component {
  tempLink: HTMLAnchorElement;
  constructor(props) {
    super(props);
    this.filename = null;
  }

  handleDownloadClick(file) {
    if (file.image) {
      this.filename = file.name;
      fetch(file.image)
        .then((res) => res.blob())
        .then((blob) => downloadFile(blob, this.tempLink, this.filename));
    } else {
      this.filename = file.name;
      downloadFile(b64toBlob(file.base64, file.type), this.tempLink, this.filename);
    }
  }

  render() {
    const { isOpen, file, onClose } = this.props;

    let open = false;

    if (file !== null && isOpen !== false) {
      open = true;
    }

    let content, preview;
    let type, url, name;

    if (file !== null && file.downloadUrl !== undefined) {
      type = file.filename.split('.').pop();
      url = file.downloadUrl;
      name = file.filename;
      preview = <FileViewer fileType={type.toLowerCase()} filePath={url} />;
    } else if (file !== null) {
      type = file !== null ? file.name.split('.').pop() : null;
      name = file.name;
      preview = <FileViewer fileType={type.toLowerCase()} filePath={file.image} />;
    }

    content = (
      <Dialog size="xl" isOpen={open}>
        <Dialog.Header title={name} onClose={onClose} />
        <div className="icon-download">
          <TransparentButtonFontAwesome
            text={'Download'}
            fontAwesomeIcon={'download'}
            onClick={() => this.handleDownloadClick(file)}
          />
        </div>
        <Dialog.Body>
          <Grid.Layout gap="xl">
            <Grid.Box>{preview}</Grid.Box>
          </Grid.Layout>
        </Dialog.Body>
      </Dialog>
    );
    return <>{content}</>;
  }
}

PreviewDialog.propTypes = {
  onYes: PropTypes.func.isRequired,
  onClose: PropTypes.func,
  isOpen: PropTypes.bool.isRequired,
  file: PropTypes.any.isRequired,
  type: PropTypes.string.isRequired,
};

export default PreviewDialog;

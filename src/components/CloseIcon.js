import React from 'react';
import PropTypes from 'prop-types';
import './file-icon.css';

const CloseIcon = ({ onClick, style = {}, className = '' }) => {
  return <div style={style} onClick={onClick} className={`close-icon ${className}`} />;
};

CloseIcon.propTypes = {
  onClick: PropTypes.func,
  style: PropTypes.object,
  className: PropTypes.string,
};

export default CloseIcon;

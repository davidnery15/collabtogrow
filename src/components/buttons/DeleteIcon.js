import React from 'react';
import { PropTypes } from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import ClickableLabel from '../ClickableLabel';

/**
 * Delete icon button for form removable inputs
 */
const DeleteIcon = ({ onClick }) => {
  return <ClickableLabel text={<FontAwesomeIcon icon="minus-circle" />} onClick={onClick} />;
};

DeleteIcon.propTypes = {
  onClick: PropTypes.func.isRequired,
};

export default DeleteIcon;

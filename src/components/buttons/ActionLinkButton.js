import React from 'react';
import { PropTypes } from 'prop-types';
import { Link } from 'react-router-dom';
import { Button } from '@8base/boost';

/**
 *
 * @param props
 * @return {*}
 * @constructor
 */
const ActionLinkButton = ({ url, textValue = '' }) => {
  return (
    <div className="action-link-button">
      <Link to={url}>
        <Button style={{ borderRadius: '50px' }}>{textValue}</Button>
      </Link>
    </div>
  );
};

ActionLinkButton.propTypes = {
  url: PropTypes.string,
  textValue: PropTypes.string,
};

export { ActionLinkButton };

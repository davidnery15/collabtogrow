import React from 'react';
import { PropTypes } from 'prop-types';
import collaborateIcon from '../../images/icons/collab-chat-icon.svg';
import rellationsIcon from '../../images/icons/related-items-icon.svg';
import { ActionButtonClose } from './ActionButtonClose';
import { TransparentButtonSvg } from './TransparentButtonSvg';

/**
 *
 * @param props
 * @return {*}
 * @constructor
 * @deprecated DO NOT USE
 */

const ActionButtonTop = ({ onClickClosed, onClickRelated, onClickCollaborated, ...rest }) => {
  return (
    <div className="top-icons">
      <TransparentButtonSvg
        onClick={onClickRelated}
        text={'Related Items'}
        iconSvg={rellationsIcon}
      />
      <TransparentButtonSvg onClick={onClickCollaborated} iconSvg={collaborateIcon} />
      <ActionButtonClose onClick={onClickClosed} />
    </div>
  );
};

ActionButtonTop.propTypes = {
  onClickClosed: PropTypes.func.isRequired,
  onClickRelated: PropTypes.func.isRequired,
  onClickCollaborated: PropTypes.func.isRequired,
};

export { ActionButtonTop };

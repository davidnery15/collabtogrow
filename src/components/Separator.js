import styled from '@emotion/styled';

const Separator = styled(`hr`)({
  borderTop: '1px solid #E3E9EF',
  width: '100%',
});

export { Separator };

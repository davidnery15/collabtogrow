import React from 'react';
import PropTypes from 'prop-types';
import './../../file-icon.css';
import LightGreyText from './../../LightGreyText';
import logoPdf from '../../../images/icons/2000px-PDF_file_icon.png';
import logoImg from '../../../images/icons/img_148071.png';
import styled from '@emotion/styled';

// Component receives a "file" array and depending on the 'file.filename'
// chooses which file icon to render or returns "None"
const IconComponents = ({ file, width = 16 }) => {
  //width should be 16 or 48
  const Div = styled('div')({
    width: 34,
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    overflow: 'hidden',
  });

  //default style
  let style = { width: width, display: 'inline-block', transform: 'translateY(2px)' };
  const className = 'file-icon';

  //default image icon
  let imgSource = `https://icons-for-free.com/free-icons/png/512/2529990.png`;

  // Check if there is even a file being received
  if ((file && file.filename) || (file && file.name)) {
    const fileName = file.name !== undefined ? file.name : file.filename;
    const type = fileName.split('.').pop();
    // supportedOfficeTypes is declared to make sure the image
    // is available in the Microsoft sharepoint site for us to use
    const supportedOfficeTypes = [
      'docx',
      'doc',
      'xlsx',
      'xls',
      'csv',
      'pptx',
      'potx',
      'odt',
      'one',
      'ods',
      'odp',
      'accdb',
      'dotx',
      'pub',
      'vstx',
      'xltx',
      'mpp',
      'onepkg',
      'ppsx',
      'vsdx',
      'mpt',
      'onetoc',
      'vssx',
      'xsn',
      'csv',
    ];

    // MS Office Files Icons
    if (supportedOfficeTypes.includes(type)) {
      imgSource = `https://static2.sharepointonline.com/files/fabric/assets/brand-icons/document/svg/${type}_${width}x1.svg`;
    } else {
      imgSource = logoImg;
    }

    // PDF File Icon
    if (type === 'pdf') {
      imgSource = logoPdf;
      style = { width: width, display: 'inline-block', transform: 'translateY(4px)' };
    }

    // Default Component Return
    return (
      <>
        <div className={className}>
          <Div>
            <img style={style} src={imgSource} alt={`${type} icon`} />
          </Div>
        </div>
      </>
    );
  }
  //else return "None"
  return <LightGreyText text="None" />;
};

IconComponents.propTypes = {
  file: PropTypes.object.isRequired,
  width: PropTypes.number,
};

export default IconComponents;

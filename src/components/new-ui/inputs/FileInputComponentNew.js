import React from 'react';
import { Row, Column, Button } from '@8base/boost';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { PropTypes } from 'prop-types';
import FileIcon from '../../FileIcon';
import { FileInput } from '@8base/file-input';
import '../forms.css';

class FileInputComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      file: [],
    };

    this.onChangeInput = this.onChangeInput.bind(this);
  }

  componentDidMount(): void {
    this.setState({ file: this.props.field });
  }

  componentWillReceiveProps(nextProps, nextContext) {
    this.setState({ file: nextProps.field });
  }

  onChangeInput(field, base64, file, localKey, maxFiles) {
    let listFile = JSON.parse(localStorage.getItem(localKey));
    if (listFile === null || maxFiles === 1) {
      listFile = [];
    }

    if (listFile.length >= maxFiles) return;

    let object = {
      name: file.name,
      type: file.type,
      base64: base64,
    };
    listFile.push(object);
    this.setState({
      file: listFile,
    });

    localStorage.setItem(localKey, JSON.stringify(listFile));
  }

  render() {
    const { onChange, field, nameInput, maxFiles, text, localKey, onClickDelete } = this.props;
    const { file } = this.state;
    return (
      <FileInput
        onChange={(picture, file, ...rest) => {
          if (maxFiles > 1) {
            picture.forEach((value, key) => {
              const reader = new FileReader();
              reader.addEventListener(
                'load',
                () => {
                  this.onChangeInput(nameInput, reader.result, file[key], localKey, maxFiles);
                },
                false,
              );
              reader.readAsDataURL(file[key]);
            });
            onChange(nameInput, picture);
          } else {
            const reader = new FileReader();
            reader.addEventListener(
              'load',
              () => {
                this.onChangeInput(nameInput, reader.result, file, localKey, maxFiles);
              },
              false,
            );
            reader.readAsDataURL(file);
            onChange(nameInput, picture);
          }
        }}
        value={field}
        maxFiles={maxFiles}>
        {({ pick }) => (
          <Row stretch alignItems="center">
            <Column>
              {Array.isArray(file) ? (
                file.map((val, i) => (
                  <>
                    {i < maxFiles && (
                      <FileIcon
                        key={i}
                        index={i}
                        file={val}
                        field={localKey}
                        onClickDelete={onClickDelete}
                      />
                    )}
                  </>
                ))
              ) : (
                <>
                  <FileIcon
                    index={0}
                    file={file}
                    field={localKey}
                    onClickDelete={onClickDelete}
                    nameInput={nameInput}
                  />
                </>
              )}
              <Button
                className="fileInput"
                type="button"
                onClick={pick}
                color="neutral"
                variant="outlined"
                disabled={file !== null && file.length >= maxFiles ? true : false}
                title={file !== null && file.length >= maxFiles ? 'Max Documents' + maxFiles : ''}>
                <FontAwesomeIcon icon="file-upload" />
                &nbsp; {file && file.length > 0 ? 'Replace Document' : text}
              </Button>
              {file !== null && file.length === maxFiles && (
                <span style={{ color: 'red' }}>Max Documents {maxFiles}</span>
              )}
            </Column>
          </Row>
        )}
      </FileInput>
    );
  }
}

FileInputComponent.propTypes = {
  onChange: PropTypes.func.isRequired,
  field: PropTypes.object,
  nameInput: PropTypes.string.isRequired,
  maxFiles: PropTypes.number.isRequired,
  text: PropTypes.string.isRequired,
  localKey: PropTypes.string.isRequired,
  onClickDelete: PropTypes.func,
};

FileInputComponent.defaultProps = {
  field: null,
  onClickDelete: null,
};

export default FileInputComponent;

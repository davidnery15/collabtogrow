import React from 'react';
import { PropTypes } from 'prop-types';
import styled from '@emotion/styled';
import FileViewer from 'react-file-viewer';
import IconComponents from './IconComponents';

/**
 * Use this instead of Card.Body for Create/Edit Views,
 * Add any css Padding, margin on this component
 * @param {Component} children the children component
 */

const StyledGridBox = styled('div')`
  box-sizing: border-box;
  height: 155px;
  width: 136px;
  border: 1px solid #e9eff4;
  border-radius: 4px 4px 0 0;
  background-color: #ffffff;
  box-shadow: inset 0 0 8px 0 #e9eff4, 0 1px 4px 0 #e9eff4;
`;

const StyledGridBox2 = styled('div')`
  height: 38px;
  border: 1px solid #e9eff4;
`;

const StyledGridBox3 = styled('div')`
  width: 134px !important;
  height: 90px !important;
  overflow-y: hidden;
  border: 1px solid #e9eff4;
  margin-top: 16px;
`;

const StyledTruncatedText = styled('p')`
  width: 80px;
  margin-left: 23px;
  margin-top: 8px;
  white-space: nowrap;
  overflow: hidden;
`;

class FileInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
    };
  }

  componentWillReceiveProps(nextProps: Readonly<P>, nextContext: any): void {
    let file = Array.isArray(nextProps.file) ? nextProps.file[0] : nextProps.file;
    fetch(file.downloadUrl)
      .then((res) => res.blob())
      .then((blob) => {
        let value = {
          image: file.downloadUrl,
          name: file.filename,
          type: file.filename
            .split('.')
            .pop()
            .toLowerCase(),
        };
        this.setState({ data: value });
      });
  }

  render() {
    const { data } = this.state;
    return (
      <StyledGridBox>
        <StyledGridBox2>
          <IconComponents file={this.props.file} />
          <StyledTruncatedText style={{ 'text-overflow': 'ellipsis' }}>
            {data.name}
          </StyledTruncatedText>
          <StyledGridBox3>
            {data.type !== undefined && (
              <FileViewer
                fileType={data.type}
                filePath={data.image}
                style={{
                  width: '115px!important',
                  height: '114px!important',
                  'margin-top': '15px!important',
                  'padding-bottom': '12px!important',
                  'overflow-y': 'hidden!important',
                }}
              />
            )}
          </StyledGridBox3>
        </StyledGridBox2>
      </StyledGridBox>
    );
  }
}

FileInput.propTypes = {
  file: PropTypes.object.isRequired,
};

export default FileInput;

import React from 'react';
import { Button, Row } from '@8base/boost';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { PropTypes } from 'prop-types';

/**
 *
 * @param props
 * @return {*}
 * @constructor
 */
const ActionButton = ({ fontAwesomeIcon = '', onClick, text = '', ...rest }) => {
  const icon = fontAwesomeIcon ? <FontAwesomeIcon icon={fontAwesomeIcon} /> : null;

  return (
    <Row style={{ width: '130px', margin: '0px 3px' }}>
      <Button stretch onClick={onClick} {...rest}>
        {icon}
        {text}
      </Button>
    </Row>
  );
};

ActionButton.propTypes = {
  fontAwesomeIcon: PropTypes.string,
  onClick: PropTypes.func.isRequired,
  text: PropTypes.string,
};

ActionButton.defaultProps = {
  fontAwesomeIcon: '',
  text: '',
};

export { ActionButton };

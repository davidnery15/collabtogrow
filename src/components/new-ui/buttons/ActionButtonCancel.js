import React from 'react';
import { Button } from '@8base/boost';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { PropTypes } from 'prop-types';

/**
 *
 * @param props
 * @return {*}
 * @constructor
 */
const ActionButtonCancel = ({ fontAwesomeIcon = '', onClick, text = '', ...rest }) => {
  const icon = fontAwesomeIcon ? <FontAwesomeIcon icon={fontAwesomeIcon} /> : null;

  return (
    <Button
      color="neutral"
      variant="outlined"
      style={{ opacity: '0.8' }}
      onClick={onClick}
      {...rest}>
      {icon}
      {text}
    </Button>
  );
};

ActionButtonCancel.propTypes = {
  fontAwesomeIcon: PropTypes.string,
  onClick: PropTypes.func.isRequired,
  text: PropTypes.string,
};

ActionButtonCancel.defaultProps = {
  fontAwesomeIcon: '',
  text: '',
};

export { ActionButtonCancel };

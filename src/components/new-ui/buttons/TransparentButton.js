import React from 'react';
import { Button, Row, Text } from '@8base/boost';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { PropTypes } from 'prop-types';

/**
 *
 * @param props
 * @return {*}
 * @constructor
 */
const TransparentButton = ({ fontAwesomeIcon = '', onClick, text = '', ...rest }) => {
  const icon = fontAwesomeIcon ? <FontAwesomeIcon icon={fontAwesomeIcon} /> : null;

  return (
    <Row style={{ width: '130px' }}>
      <Button onClick={onClick} color={'neutral'} variant={'outlined'} {...rest}>
        <Text disabled className="fontWeighButton">
          {icon}
          {text}
        </Text>
      </Button>
    </Row>
  );
};

TransparentButton.propTypes = {
  fontAwesomeIcon: PropTypes.string,
  onClick: PropTypes.func.isRequired,
  text: PropTypes.string,
};

TransparentButton.defaultProps = {
  fontAwesomeIcon: '',
  text: '',
};

export { TransparentButton };

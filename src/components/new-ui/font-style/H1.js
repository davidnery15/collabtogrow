import React from 'react';
import styled from '@emotion/styled';
import { PropTypes } from 'prop-types';

const StyledH1 = styled('h1')`
  font-family: Poppins;
  font-weight: 500;
  font-size: 32px;
  letter-spacing: 0.5px;
  line-height: 48px;
  color: #323c47;
  & first-letter {
    text-transform: capitalize;
  }
`;

const H1 = ({ children }) => {
  return <StyledH1>{children}</StyledH1>;
};

H1.propTypes = {
  children: PropTypes.any.isRequired,
};

export { H1 };

import React from 'react';
import { Text } from '@8base/boost';
import styled from '@emotion/styled';
import { PropTypes } from 'prop-types';

const StyledSubtitle = styled(Text)`
  font-family: Poppins;
  font-weight: 600;
  font-size: 14px;
  letter-spacing: 0.5px;
  line-height: 20px;
  color: #323c47;
  & first-letter {
    text-transform: capitalize;
  }
`;

const Subtitle = ({ children }) => {
  return <StyledSubtitle>{children}</StyledSubtitle>;
};

Subtitle.propTypes = {
  children: PropTypes.any.isRequired,
};

export { Subtitle };

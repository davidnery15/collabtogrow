import React from 'react';
import styled from '@emotion/styled';
import { PropTypes } from 'prop-types';

const StyledLink = styled('a')`
  font-family: Poppins;
  font-weight: 400;
  font-size: 13px;
  letter-spacing: 0.2px;
  line-height: 20px;
  color: #0874f9;
  text-decoration: none;
  & first-letter {
    text-transform: capitalize;
  }
`;

const LinkFont = ({ children }) => {
  return <StyledLink>{children}</StyledLink>;
};

LinkFont.propTypes = {
  children: PropTypes.any.isRequired,
};

export { LinkFont };

import React from 'react';
import styled from '@emotion/styled';
import { PropTypes } from 'prop-types';

const StyledH2 = styled('h2')`
  font-family: Poppins;
  font-weight: 400;
  font-size: 18px;
  letter-spacing: 0.5px;
  line-height: 28px;
  color: #323c47;
  & first-letter {
    text-transform: capitalize;
  }
`;

const H2 = ({ children }) => {
  return <StyledH2>{children}</StyledH2>;
};

H2.propTypes = {
  children: PropTypes.any.isRequired,
};

export { H2 };

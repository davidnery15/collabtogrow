import React from 'react';
import styled from '@emotion/styled';
import { PropTypes } from 'prop-types';

const StyledForm = styled.div`
  padding: 15px 20px !important;
  border: solid #e9eff4 !important;
  border-width: 0px 1px 1px 1px !important;
`;

const BorderForm = ({ children }) => {
  return <StyledForm>{children}</StyledForm>;
};

BorderForm.propTypes = {
  children: PropTypes.any.isRequired,
};

export { BorderForm };

import React from 'react';
import styled from '@emotion/styled';
import { PropTypes } from 'prop-types';

const StyledFlexTop = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  align-content: center;
  width: 70%;
  margin: 0 auto;
  padding: 10px 10px;
  border: 1px solid rgb(241, 241, 241);
`;

const FlexTopDiv = ({ children }) => {
  return <StyledFlexTop>{children}</StyledFlexTop>;
};

FlexTopDiv.propTypes = {
  children: PropTypes.any.isRequired,
};

export { FlexTopDiv };

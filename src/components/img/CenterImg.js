import React from 'react';
import { PropTypes } from 'prop-types';

const CenterImg = ({ width = 80, alt = 'img', src, ...rest }) => {
  return (
    <img
      style={{
        display: 'block',
        marginLeft: 'auto',
        marginRight: 'auto',
      }}
      src={src}
      width={width}
      alt={alt}
      {...rest}
    />
  );
};

CenterImg.propTypes = {
  width: PropTypes.string,
  src: PropTypes.string.isRequired,
  alt: PropTypes.string,
};

export default CenterImg;

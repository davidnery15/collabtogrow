import React from 'react';
import { PropTypes } from 'prop-types';
import styled from '@emotion/styled';

const NotificationCircle = styled('span')`
  position: absolute;
  top: 0px;
  right: -2px;
  height: 4px;
  width: 4px;
  background-color: #3eb7f9;
  border-radius: 50%;
`;

const HeaderIcon = ({ url, hasNotifications = false, ...rest }) => {
  const RelativeContainer = styled('div')`
    cursor: pointer;
    position: relative;
    display: inline;
    width: 24px;
    height: 24px;
    &:hover {
      opacity: 0.8;
    }
  `;

  return (
    <>
      &nbsp; &nbsp;
      <RelativeContainer {...rest}>
        <img src={url} width={24} alt="icon" />
        {hasNotifications ? <NotificationCircle /> : null}
      </RelativeContainer>
      &nbsp; &nbsp;
    </>
  );
};

HeaderIcon.propTypes = {
  url: PropTypes.string,
  hasNotifications: PropTypes.bool,
};

export default HeaderIcon;

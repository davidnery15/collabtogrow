import React from 'react';
import { PropTypes } from 'prop-types';
import LightGreyText from './LightGreyText';

// Component checks if there's a value present to render
// or else returns unassignedText
const UserDetailValue = ({ user, fontSize, unassignedText = 'Unassigned' }) => {
  if (user && user.firstName) {
    return (
      <span style={fontSize ? { fontSize: fontSize } : {}}>
        {`${user.firstName} ${user.lastName}`}
      </span>
    );
  }
  return <LightGreyText fontSize={fontSize} text={unassignedText} />;
};

UserDetailValue.propTypes = {
  user: PropTypes.object,
  fontSize: PropTypes.number,
  unassignedText: PropTypes.string,
};

export default UserDetailValue;

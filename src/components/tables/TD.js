import React from 'react';
import styled, { css } from 'styled-components';
import { PropTypes } from 'prop-types';

const StyledTD = styled(`td`)`
  height: 30px;
  color: #323c47;
  font-family: Poppins;
  font-size: 13px;
  line-height: 20px;
  min-width: 77px;
  max-width: 77px;
  text-align: center;
  vertical-align: middle;
  border-bottom: 1px solid #e9eff4;
  padding: 5px;
  ${(props) =>
    props.border &&
    css`
      border-left: 1px solid #e9eff4;
    `}
  ${(props) =>
    props.textAlignLeft &&
    css`
      text-align: left;
    `}
`;

const TD = ({ children, ...rest }) => {
  return <StyledTD {...rest}>{children}</StyledTD>;
};

TD.propTypes = {
  children: PropTypes.any.isRequired,
  border: PropTypes.bool,
  textAlignLeft: PropTypes.bool,
};

TD.defaultProps = {
  border: false,
  textAlignLeft: false,
};

export { TD };

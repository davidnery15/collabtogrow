import React from 'react';
import styled, { css } from 'styled-components';
import { PropTypes } from 'prop-types';

const StyledTH = styled(`th`)`
 min-width: 77px;
 color: #7E828B;
 font-family: Poppins;
 font-size: 12px;
 font-weight: bold;
 letter-spacing: 0.5px;
 line-height: 18px;
 height: 48px;
 background-color: #F4F7F9;
 text-transform: uppercase;
 text-align: center;
 vertical-align: middle;
 padding:5px;
   ${(props) =>
    props.border &&
     css`
       border-left: 1px solid #e9eff4;
     `}
   ${(props) =>
    props.textAlign === `left` &&
     css`
       text-align: left;
     `}
}
`;

const TH = ({ children, ...rest }) => {
  return <StyledTH {...rest}>{children}</StyledTH>;
};

TH.propTypes = {
  children: PropTypes.any.isRequired,
  border: PropTypes.bool,
  textAlign: PropTypes.bool,
};

TH.defaultProps = {
  border: false,
  textAlign: 'center',
};

export { TH };

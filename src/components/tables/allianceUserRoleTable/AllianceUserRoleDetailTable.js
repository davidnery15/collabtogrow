import React from 'react';
import PropTypes from 'prop-types';
import AllianceUserRoleDetailColumn from './AllianceUserRoleDetailColumn';
import { TableDetail } from '../../new-ui/table/TableDetail';
import { TablePosition } from '../../new-ui/div/TablePosition';
import { HorizontalLineText } from '../../text/HorizontalLineText';
import { HorizontalLine } from '../../new-ui/text/HorizontalLine';
import { BoderDetailView } from '../../new-ui/div/BorderDetailView';
import { Grid, Table } from '@8base/boost';
import { HeaderText } from '../../new-ui/text/HeaderText';

/**
 * Detail View Table For The Action Entity
 */
const AllianceUserRoleDetailTable = (props) => {
  const { data: users } = props;

  let content = null;

  if (users.length) {
    content = (
      <>
        {users.map((user, index) => (
          <AllianceUserRoleDetailColumn key={index} data={user} />
        ))}
      </>
    );
  }

  return (
    <>
      <HorizontalLine>
        <HorizontalLineText>DETAILS OF THE ALLIANCE SER</HorizontalLineText>
      </HorizontalLine>
      <>
        <BoderDetailView>
          <Grid.Layout columns="50% 50%" areas={[['left', 'right']]} style={{ width: '100%' }}>
            <Grid.Box area="left">
              <div style={{ position: 'absolute', top: '21px', left: '25px' }}>
                <HeaderText>DETAILS OF THE ALLIANCE SER</HeaderText>
              </div>
            </Grid.Box>
          </Grid.Layout>
        </BoderDetailView>
        <TablePosition>
          <TableDetail>
            <Table.Header className="justify-center-column" columns="3fr 3fr">
              <Table.HeaderCell>NAME</Table.HeaderCell>
              <Table.HeaderCell>ROLE</Table.HeaderCell>
            </Table.Header>
            <tbody>{content}</tbody>
          </TableDetail>
        </TablePosition>
      </>
    </>
  );
};

AllianceUserRoleDetailTable.propTypes = {
  data: PropTypes.array.isRequired,
};

export default AllianceUserRoleDetailTable;

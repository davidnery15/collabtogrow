import React from 'react';
import styled from 'styled-components';
import { PropTypes } from 'prop-types';

const StyledHr = styled.hr`
  border: solid 1px #edf0f2;
  margin: 20px ${(props) => props.marginHorizontal}px;
`;

/**
 * Hr with marginHorizontal to ignore padding
 * @param {Number} [marginHorizontal=0 }] negative to ignore parent's padding
 */
const Hr = ({ marginHorizontal = 0 }) => {
  return <StyledHr marginHorizontal={marginHorizontal} />;
};

Hr.propTypes = {
  marginHorizontal: PropTypes.number,
};

export default Hr;

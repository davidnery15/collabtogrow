import React from 'react';
import styled, { css } from 'styled-components';
import { PropTypes } from 'prop-types';

const StyledHeader = styled(`h4`)`
  color: #323c47;
  font-size: 13px;
  font-weight: 600;
  line-height: 18px;
  font-family: Poppins;
  ${(props) =>
    props.textAlignRight &&
    css`
      text-align: right;
    `}
`;

const H4Bold = ({ children, ...rest }) => {
  return <StyledHeader {...rest}>{children}</StyledHeader>;
};

H4Bold.propTypes = {
  children: PropTypes.any.isRequired,
  textAlignRight: PropTypes.bool,
};

H4Bold.defaultProps = {
  textAlignRight: false,
};

export { H4Bold };

import React from 'react';
import { Heading } from '@8base/boost';
import styled from '@emotion/styled';
import { PropTypes } from 'prop-types';

const StyledHeader = styled(Heading)({
  fontSize: 17,
  marginTop: 30,
  marginLeft: 25,
  position: 'relative',
  float: 'left',
});

const H4 = ({ text }) => {
  return (
    <>
      <StyledHeader text={text} />
    </>
  );
};

H4.propTypes = {
  text: PropTypes.string.isRequired,
  status: PropTypes.string,
};

export default H4;

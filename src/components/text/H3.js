import React from 'react';
import styled, { css } from 'styled-components';
import { PropTypes } from 'prop-types';

const StyledHeader = styled(`h3`)`
  color: #323c47;
  font-family: Poppins;
  font-size: 14px;
  font-weight: 600;
  letter-spacing: 0.5px;
  line-height: 21px;
  margin: 10px 10px 10px 10px;
  ${(props) =>
    props.textAlignLeft &&
    css`
      text-align: left;
    `}
  ${(props) =>
    props.textAlignRight &&
    css`
      text-align: right;
    `}
`;

const H3 = ({ children, ...rest }) => {
  return <StyledHeader {...rest}>{children}</StyledHeader>;
};

H3.propTypes = {
  children: PropTypes.any.isRequired,
  textAlignLeft: PropTypes.bool,
  textAlignRight: PropTypes.bool,
};

H3.defaultProps = {
  textAlignLeft: false,
  textAlignRight: false,
};

export { H3 };

import React from 'react';
import styled from 'styled-components';
import { PropTypes } from 'prop-types';
import { Text } from '@8base/boost';
import { GREEN_COLOR, RED_COLOR } from '../../shared/colors';
import CurrencyFormat from 'react-currency-format';

const StyledCurrencyFormat = styled(CurrencyFormat)`
  color: ${(props) => props.color} !important;
`;

const CurrencyVarianceField = (props) => {
  let { currency, value, textWeight } = props;
  const color = value < 0 ? RED_COLOR : GREEN_COLOR;

  if (currency === null || currency === undefined) {
    currency = {};
    currency.symbol = '$';
    currency.decimalSeparator = ',';
    currency.thousandSeparator = '.';
  }
  if (value === null || value === undefined) {
    value = 0;
  }
  return (
    <Text weight={textWeight}>
      {value < 0 ? '(' : ''}
      <StyledCurrencyFormat
        color={color}
        value={Math.abs(value)}
        displayType={'text'}
        thousandSeparator={currency.thousandSeparator}
        decimalSeparator={currency.decimalSeparator}
        prefix={`${currency.symbol} `}
        decimalScale={2}
      />
      {value < 0 ? ')' : ''}
    </Text>
  );
};

CurrencyVarianceField.propTypes = {
  currency: PropTypes.object,
  value: PropTypes.number,
  textWeight: PropTypes.string,
};

CurrencyVarianceField.defaultProps = {
  currency: null,
  value: null,
  textWeight: 'normal',
};

export { CurrencyVarianceField };

import styled from '@emotion/styled';

const Content = styled(`div`)({
  margin: '10px 10px',
  padding: '10px 10px',
});

export default Content;

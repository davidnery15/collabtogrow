import React from 'react';
import { PropTypes } from 'prop-types';

/**
 * Delete icon button for form removable inputs
 */
const DeleteIcon = ({ onClick }) => {
  return (
    <div className="clickable-label" style={{ height: '100%' }}>
      <img
        style={{ marginTop: '25px' }}
        src="http://cdn.onlinewebfonts.com/svg/img_417342.png"
        alt="test"
        onClick={onClick}
      />
    </div>
  );
};

DeleteIcon.propTypes = {
  onClick: PropTypes.func.isRequired,
};

export default DeleteIcon;

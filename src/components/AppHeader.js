import React from 'react';
import { Link } from 'react-router-dom';
import styled from '@emotion/styled';
import { Button, Grid, Loader, ButtonGroup } from '@8base/boost';
import UserDropdown from './UserDropdown.js';
import logo from '../images/logos/collabtogrow_main_logo.svg';
import AllianceDropdown from '../shared/components/AllianceDropdown';
import View from '@cobuildlab/react-flux-state';
import sessionStore, { NEW_SESSION_EVENT } from '../shared/SessionStore';
import { updateMeta } from '../modules/dashboard/dashboard-actions';
import { META_ALLIANCE_SELECTED } from '../shared/index';
import { isValidString } from '../shared/validators';
import { openActiveItems } from '../modules/active-items/active-items-actions';
import NOTIFICATIONS_ICON from '../images/icons/notifications.svg';
import CHAT_ICON from '../images/icons/Chat.svg';
import HeaderIcon from './img/HeaderIcon';
import CompanyHeaderButton from './buttons/CompanyHeaderButton';
import {
  fetchNotifications,
  openNotificationsSideBar,
} from '../modules/notifications/notifications-actions';
import notificationsStore, {
  NOTIFICATIONS_EVENT,
} from '../modules/notifications/notifications-store';
import { toggleChatSideBar } from '../modules/chat/chat-actions';
import { fetchSession } from '../modules/auth/auth.actions.js';
import chatStore, { CHAT_UNREADS_EVENT } from '../modules/chat/chat-store';

const HeaderTag = styled(Grid.Layout)({
  height: '6rem',
  padding: '0 2rem',
  backgroundColor: '#fff',
  borderBottom: '1px solid #D0D7DD',
});

const HeaderLogo = styled('img')({
  width: '175px',
});

class AppHeader extends View {
  constructor(props) {
    super(props);
    this.state = {
      alliances: [],
      company: null,
      selected: null,
      loading: false,
      hasNotifications: false,
      hasUnreadsChannels: false,
    };
  }

  async componentDidMount() {
    this.subscribe(sessionStore, NEW_SESSION_EVENT, this.setSession);
    this.subscribe(notificationsStore, NOTIFICATIONS_EVENT, (notifications) =>
      this.setState({ hasNotifications: notifications.length > 0 }),
    );

    this.subscribe(chatStore, CHAT_UNREADS_EVENT, (unreadsChannelsCount) => {
      this.setState({
        hasUnreadsChannels: unreadsChannelsCount > 0,
      });
    });

    // setSession on first load
    this.setSession(sessionStore.getState(NEW_SESSION_EVENT));

    const { selectedAlliance } = sessionStore.getState(NEW_SESSION_EVENT);
    if (selectedAlliance) fetchNotifications();
  }

  setSession = (newSessionEventData) => {
    const { userAlliances, user } = newSessionEventData;
    const { companyUserRelation } = user;
    const company = companyUserRelation.count !== 0 ? companyUserRelation.items[0].company : null;

    const newState = {
      alliances: userAlliances,
      company,
    };
    // Picking the Selected previously
    const allianceSelected = user.metaRelation.items.find(
      (meta) => meta.name === META_ALLIANCE_SELECTED,
    );

    if (isValidString(allianceSelected.value)) {
      const alliance = userAlliances.find((alliance) => alliance.id === allianceSelected.value);
      if (alliance) {
        newState.selected = alliance.name;
      }
    } else {
      if (userAlliances && userAlliances.length) {
        this.onSelect(userAlliances[0], false);
      }
    }
    this.setState(newState);
  };

  onSelect = async (alliance, hasAlliance = false) => {
    this.setState({
      selected: alliance.name,
      loading: true,
    });

    await updateMeta(META_ALLIANCE_SELECTED, alliance.id);

    if (hasAlliance) window.location.reload();
    else {
      await fetchSession();
      this.setState({ loading: false });
    }
  };

  onActiveItems = () => openActiveItems();
  onClickNotifications = () => openNotificationsSideBar();
  onClickChat = () => toggleChatSideBar();

  render() {
    const {
      alliances,
      selected,
      loading,
      company,
      hasNotifications,
      hasUnreadsChannels,
    } = this.state;
    let content = (
      <AllianceDropdown items={alliances} selected={selected} onSelect={this.onSelect} />
    );

    if (loading) content = <Loader size={'sm'} />;

    return (
      <Grid.Box
        style={{
          position: 'fixed',
          width: '100%',
          zIndex: '2000',
        }}
        area="header">
        <HeaderTag columns="350px auto 350px" gap="sm">
          <Grid.Box justifyContent="center" justifySelf="start">
            <Link to="/">
              <HeaderLogo src={logo} alt="CollabToGrow logo" />
            </Link>
          </Grid.Box>
          <Grid.Box justifyContent="center" alignItems="center">
            {content}
          </Grid.Box>
          <Grid.Box direction="row" alignItems="center" justifySelf="end">
            {company && (
              <HeaderIcon
                url={CHAT_ICON}
                onClick={this.onClickChat}
                hasNotifications={hasUnreadsChannels}
              />
            )}
            <HeaderIcon
              url={NOTIFICATIONS_ICON}
              onClick={this.onClickNotifications}
              hasNotifications={hasNotifications}
            />
            &nbsp;&nbsp;&nbsp;
            <ButtonGroup>
              <CompanyHeaderButton company={company} />
              <Button
                style={{ padding: '0 10px', boxShadow: 'none' }}
                color="neutral"
                variant="outlined">
                <UserDropdown />
              </Button>
            </ButtonGroup>
          </Grid.Box>
        </HeaderTag>
      </Grid.Box>
    );
  }
}

export { AppHeader };

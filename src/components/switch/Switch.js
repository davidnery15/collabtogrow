import styled from '@emotion/styled';
import { Switch as BoostSwitch } from '@8base/boost';

const Switch = styled(BoostSwitch)`
  height: 30px;
  & div:first-of-type {
    height: 26px;
    width: 55px;
    &:before {
      height: 22px;
      width: 22px;
    }
  }
  & div:last-of-type:not(:first-of-type) {
    margin-left: 10px;
    font-size: 12px;
  }
  & input:checked + div:before {
    left: 31px;
  }
`;

export { Switch };

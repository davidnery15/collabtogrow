import React from 'react';
import { Text } from '@8base/boost';
import { PropTypes } from 'prop-types';
import styled from '@emotion/styled';

const StyledText = styled(Text)({
  fontSize: 16,
  color: '#a6e50f',
});

const ToastSuccess = ({ text = '' }) => {
  return (
    <>
      <StyledText weight="bold">{'YEP!'}</StyledText>
      &nbsp;&nbsp;
      <StyledText>{text}</StyledText>
    </>
  );
};

ToastSuccess.propTypes = {
  text: PropTypes.string.isRequired,
};

export default ToastSuccess;

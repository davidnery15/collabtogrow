import React from 'react';
import { toast } from 'react-toastify';
import { ToastSuccess, ToastError, ToastWarn, ToastInfo } from './components';
import ToastErrors from './components/ToastErrors';

const autoClose = 15000;

export const success = (text) => {
  toast.success(<ToastSuccess text={text} />, { autoClose });
};

export const error = (text) => {
  toast.error(<ToastError text={text} />, { autoClose });
};
export const errors = (arr) => {
  toast.error(<ToastErrors arr={arr} />, { autoClose });
};

export const info = (text) => {
  toast.info(<ToastInfo text={text} />, { autoClose });
};
export const warn = (text) => {
  toast.warn(<ToastWarn text={text} />, { autoClose });
};

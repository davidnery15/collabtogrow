import React from 'react';
import FileIcon from '../FileIcon';
import { PropTypes } from 'prop-types';
import LightGreyText from '../LightGreyText';

const DocumentsFileComponent = ({ data, localKey, fontSize }) => {
  let documentStorage;
  if (JSON.parse(localStorage.getItem(localKey)) !== null) {
    documentStorage = JSON.parse(localStorage.getItem(localKey));
  } else if (data && data.items) {
    if (data.items.items !== undefined) {
      documentStorage = data.items.items;
    } else {
      documentStorage = data.items;
    }
  } else {
    documentStorage = data;
  }

  return (
    <>
      {Array.isArray(documentStorage) ? (
        documentStorage.length > 0 ? (
          documentStorage.map((value, index) => (
            <FileIcon key={index} index={index} file={value} field={localKey} />
          ))
        ) : (
          <LightGreyText fontSize={fontSize} text="None" />
        )
      ) : documentStorage !== '' && documentStorage !== null ? (
        <FileIcon key={0} index={0} file={documentStorage} field={localKey} />
      ) : (
        <LightGreyText fontSize={fontSize} text="None" />
      )}
    </>
  );
};

DocumentsFileComponent.propTypes = {
  data: PropTypes.object,
  localKey: PropTypes.string.isRequired,
  fontSize: PropTypes.number,
};

DocumentsFileComponent.defaultProps = {
  fontSize: null,
  data: null,
};

export default DocumentsFileComponent;

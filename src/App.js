import React from 'react';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import { AppProvider } from '@8base/app-provider';
import { AsyncContent, createTheme, BoostProvider } from '@8base/boost';
import { toast, ToastContainer } from 'react-toastify';
import ProtectedRoute from './shared/components/ProtectedRoute';
import { TOAST_SUCCESS_MESSAGE } from './shared/constants';
import { setLoginRedirectUri } from './shared/utils';
import { MainPlate } from './components/MainPlate';
import { ContentPlate } from './components/ContentPlate';
import { Auth } from './routes/auth';
import Wizard from './modules/wizard/Wizard';
import DashboardView from './modules/dashboard/DashboardView';
import SettingsView from './modules/settings/SettingsView';
import HomeView from './modules/home/HomeView';
import ManagementView from './modules/management/ManagementView';
import ActiveItemsView from './modules/active-items/ActiveItemsView';
import CommentsSideBar from './modules/comment/CommentsSideBar';
import RelatedItemSideBar from './modules/related-item/components/RelatedItemSideBar';
import Session from './shared/components/Session';
import { library } from '@fortawesome/fontawesome-svg-core';
import Menu from './components/Menu';
import { auth0WebClient } from './modules/auth/auth.actions';
import {
  faPaperPlane,
  faUsersCog,
  faEdit,
  faDoorOpen,
  faPencilAlt,
  faCaretDown,
  faCheck,
  faFileUpload,
  faMinusCircle,
  faComment,
  faComments,
  faProjectDiagram,
  faSearch,
  faTasks,
  faTimes,
  faClipboardList,
  faPlus,
  faEye,
  faDownload,
  faEyeSlash,
} from '@fortawesome/free-solid-svg-icons';
import InvitationsView from './modules/invitations/InvitationsView';
import { NotificationsSideBar } from './modules/notifications/NotificationsSideBar';
import { ReportsView } from './modules/reports/ReportsView';
import ChatView from './modules/chat/ChatView';
import { GREEN_COLOR, RED_COLOR, YELLOW_COLOR, AMBER_COLOR } from './shared/colors';
import { Welcome } from './modules/home/components/Welcome';
import { info } from './components/toast/Toast';

//Font Awesome Icons
library.add(
  faProjectDiagram,
  faPaperPlane,
  faComment,
  faComments,
  faUsersCog,
  faEdit,
  faDoorOpen,
  faSearch,
  faPencilAlt,
  faCaretDown,
  faCheck,
  faFileUpload,
  faMinusCircle,
  faTasks,
  faTimes,
  faClipboardList,
  faPlus,
  faEye,
  faDownload,
  faEyeSlash,
);

const { REACT_APP_8BASE_API_ENDPOINT } = process.env;

const collabTheme = createTheme({
  /** Change the pallet of the color. */
  COLORS: {
    GREEN: GREEN_COLOR,
    RED: RED_COLOR,
    YELLOW: YELLOW_COLOR,
    PRIMARY: '#3EB7F9',
    GRAY4: '#939EA7',
    AMBER: AMBER_COLOR,
    BLUE_2: '#0096B8',
  },
  /** Change the custom components styles if it needed. */
  components: {
    dividerInner: {
      root: {
        backgroundColor: '#edf0f2',
      },
    },
    input: {
      // root: {
      //   borderColor: 'gray',
      // },
      modifiers: {
        hasError: {
          borderColor: 'red',
        },
      },
    },
    navigation: {
      modifiers: {
        color: {
          PRIMARY: {
            backgroundColor: '#008AAB',
          },
        },
      },
    },
    navigationItem: {
      modifiers: {
        color: {
          PRIMARY: {
            backgroundColor: '#008AAB',
            '&:hover, &.active': {
              backgroundColor: '#3EB7F9',
            },
          },
        },
      },
    },
    navigationItemLabel: {
      root: {
        color: 'white',
      },
    },
    // button: ({ COLORS, SIZES }) => ({
    //   root: {
    //     fontSize: SIZES.SMALL_FONT_SIZE,
    //   },
    //   modifiers: {
    //     disabled: {
    //       backgroundColor: COLORS.RED,
    //     },
    //   },
    // }),
  },
});
// TODO: Add below navigationPlate color to the new "createTheme"
// const collabTheme = merge(defaultTheme, {
//   COLORS: {
//     PRIMARY_BUTTON_BACKGROUND_COLOR: "#3EB7F9"
//   },
//   navigationPlate: {
//     modifiers: {
//       color: {
//         PRIMARY: {
//           backgroundColor: "#008AAB"
//         }
//       }
//     }
//   },
//   navigationItem: {
//     modifiers: {
//       color: {
//         PRIMARY: {
//           backgroundColor: "#008AAB",
//           "&:hover, &.active": {
//             backgroundColor: "#3EB7F9"
//           }
//         }
//       }
//     }
//   }
// });

class App extends React.PureComponent {
  componentDidMount = () => {
    setLoginRedirectUri();

    window.toastRefreshPage = () => {
      info('A new version of the platform is available. Please refresh your browser!.');
    };
  };

  renderContent = () => {
    return (
      <AsyncContent stretch>
        <Switch>
          <Route path="/auth" component={Auth} />
          <Route path="/home" component={HomeView} />
          <Route>
            <CommentsSideBar>
              <RelatedItemSideBar>
                <NotificationsSideBar>
                  <Session>
                    <ChatView />
                    <MainPlate>
                      <Menu />
                      <ContentPlate>
                        <Switch>
                          <ProtectedRoute exact path="/dashboard" component={DashboardView} />
                          <ProtectedRoute
                            exact
                            path="/wizard/:step(0|1|2|3|4|5)?"
                            component={Wizard}
                          />
                          <ProtectedRoute path="/management" component={ManagementView} />
                          <ProtectedRoute path="/reports" component={ReportsView} />
                          <ProtectedRoute path="/active-items" component={ActiveItemsView} />
                          <ProtectedRoute path="/settings" component={SettingsView} />
                          <ProtectedRoute path="/invitations" component={InvitationsView} />
                          <ProtectedRoute path="/welcome" component={Welcome} />
                          <Redirect to={`/welcome`} />
                        </Switch>
                      </ContentPlate>
                    </MainPlate>
                  </Session>
                </NotificationsSideBar>
              </RelatedItemSideBar>
            </CommentsSideBar>
          </Route>
          <Route render={() => <Redirect to={'/home'} />} />
        </Switch>
      </AsyncContent>
    );
  };

  onRequestSuccess = ({ operation }) => {
    const message = operation.getContext()[TOAST_SUCCESS_MESSAGE];
    if (message) {
      toast.success(message);
    }
  };

  render() {
    return (
      <BrowserRouter>
        <BoostProvider theme={collabTheme}>
          <AppProvider
            uri={REACT_APP_8BASE_API_ENDPOINT}
            authClient={auth0WebClient}
            onRequestSuccess={this.onRequestSuccess}>
            {this.renderContent}
          </AppProvider>
          <ToastContainer position={toast.POSITION.BOTTOM_LEFT} />
        </BoostProvider>
      </BrowserRouter>
    );
  }
}

export { App };
